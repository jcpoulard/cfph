

-- 16-02-2021
ALTER TABLE `student_level_history` ADD `program` INT(11) NULL DEFAULT NULL AFTER `student_id`, ADD INDEX `fk_student_level_history_program` (`program`);
ALTER TABLE `student_level_history` ADD CONSTRAINT `fk_student_level_history_program` FOREIGN KEY (`program`) REFERENCES `program`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;






-- 26-11-2020
INSERT INTO auth_item(name, type) VALUES('fi-generalconfig-update','2');


-- 13-11-2020
INSERT INTO `rpt_custom_cat` (`id`, `categorie_name`, `cat`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'Admission', 'adm', '2020-11-13 00:00:00', '', '', '');

-- --------------------------------
ALTER TABLE `taxes` ADD `valeur_fixe` INT NOT NULL DEFAULT '0' AFTER `taxe_value`;


-- 25-09-2020
INSERT INTO auth_item(name, type) VALUES('inscription-postulantincome-view','2');
INSERT INTO auth_item(name, type) VALUES('inscription-postulantincome-delete','2');

-- 19-09-2020
ALTER TABLE `postulant_income` ADD `fee` INT(11) NULL AFTER `devise`, ADD INDEX `fk_postulant_income_fees_label` (`fee`);
ALTER TABLE `postulant_income` ADD CONSTRAINT `fk_postulant_income_fees_label` FOREIGN KEY (`fee`) REFERENCES `fees_label`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

DELETE FROM `postulant_income` WHERE postulant NOT in (SELECT id FROM postulant );
ALTER TABLE `postulant_income` ADD  CONSTRAINT `fk_postulant_income_postulant` FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;






-- ------------------------------------------------------------------------------------------------------------------------
# modification pour gerer les inscriptions 
ALTER TABLE `postulant` ADD `is_working` BOOLEAN NULL AFTER `phone`;
ALTER TABLE `postulant` ADD `name_working` TEXT NULL AFTER `is_working`, ADD `adress_working` TEXT NULL AFTER `name_working`;
ALTER TABLE postulant DROP FOREIGN KEY fk_postulant_previous_program;
ALTER TABLE `postulant` CHANGE `previous_program` `last_class` VARCHAR(64) NULL;

CREATE TABLE `postulant_parent` ( `id` INT NOT NULL AUTO_INCREMENT , `mother_name` VARCHAR(128) NULL , `father_name` VARCHAR(128) NULL , `address_in_haiti` TEXT NULL , `address_foreign` TEXT NULL , `phone_mother` VARCHAR(16) NULL , `phone_father` VARCHAR(16) NULL , `occupation_mother` VARCHAR(64) NULL , `occupation_father` VARCHAR(64) NULL , `occupation_mother_address` TEXT NULL , `occupation_father_address` TEXT NULL , `occupation_mother_phone` VARCHAR(16) NULL , `occupation_father_phone` VARCHAR(16) NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(32) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(32) NULL , PRIMARY KEY (`id`), INDEX (`mother_name`), INDEX (`father_name`)) ENGINE = InnoDB;
ALTER TABLE `postulant_parent` ADD `postulant` INT NOT NULL AFTER `id`, ADD INDEX (`postulant`);
ALTER TABLE `postulant_parent` ADD CONSTRAINT `fk_postulant` FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
    
CREATE TABLE `postulant_liable_person` ( `id` INT NOT NULL AUTO_INCREMENT , `postulant` INT NULL , `last_name` VARCHAR(64) NULL , `first_name` VARCHAR(64) NULL , `phone` VARCHAR(16) NULL , `address` TEXT NULL , `occupation` VARCHAR(64) NULL , `occupation_address` TEXT NULL , `occupation_phone` VARCHAR(16) NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`postulant`), INDEX (`last_name`), INDEX (`first_name`)) ENGINE = InnoDB;
ALTER TABLE `postulant_liable_person` ADD CONSTRAINT `fk_postulant_person_liable` FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE `postulant_health` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `postulant_health` ADD `postulant` INT NULL AFTER `id`, ADD `blood_group` VARCHAR(8) NULL AFTER `postulant`, ADD `is_permanent_decease` BOOLEAN NULL AFTER `blood_group`, ADD `permanent_decease` TEXT NULL AFTER `is_permanent_decease`, ADD `is_regulary_drug` BOOLEAN NULL AFTER `permanent_decease`, ADD `regulary_drug` TEXT NULL AFTER `is_regulary_drug`, ADD `create_date` DATETIME NULL AFTER `regulary_drug`, ADD `create_by` VARCHAR(64) NULL AFTER `create_date`, ADD `update_date` DATETIME NULL AFTER `create_by`, ADD `update_by` VARCHAR(64) NULL AFTER `update_date`, ADD INDEX (`postulant`);

CREATE TABLE `postulant_social` ( `id` INT NOT NULL AUTO_INCREMENT , `postulant` INT NULL , `civil_state` VARCHAR(32) NULL , `is_leave_outside` BOOLEAN NULL , `foreign_country` VARCHAR(32) NULL , `is_prison` BOOLEAN NULL , `prison_place` VARCHAR(32) NULL , `is_religion` BOOLEAN NULL , `religion` VARCHAR(32) NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`postulant`), INDEX (`civil_state`)) ENGINE = InnoDB;
ALTER TABLE `postulant_social` ADD CONSTRAINT `fk_postulant_social` FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

RENAME TABLE `enrollment_income` TO `postulant_income`;
ALTER TABLE postulant_income DROP FOREIGN KEY fk_enrollment_income_program;
ALTER TABLE `postulant_income` DROP `apply_level`;

ALTER TABLE `postulant`
  DROP `blood_group`,
  DROP `health_state`,
  DROP `person_liable`,
  DROP `person_liable_phone`,
  DROP `person_liable_adresse`,
  DROP `person_liable_relation`;

ALTER TABLE `postulant` ADD `cohorte` INT NULL AFTER `academic_year`, ADD INDEX (`cohorte`);
ALTER TABLE `postulant` ADD `work_phone` VARCHAR(16) NULL AFTER `adress_working`;
ALTER TABLE `postulant` ADD `shift` INT NULL AFTER `apply_for_program`, ADD INDEX (`shift`);
ALTER TABLE `postulant_income` ADD `devise` INT NULL AFTER `amount`, ADD INDEX (`devise`);
ALTER TABLE `postulant_income` ADD CONSTRAINT `fk_postulant_income_devise` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `postulant_income` CHANGE `comments` `comments` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `postulant` CHANGE `date_updated` `date_updated` DATETIME NULL, CHANGE `update_by` `update_by` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `postulant_income` CHANGE `date_updated` `date_updated` DATETIME NULL, CHANGE `update_by` `update_by` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

# New update 
ALTER TABLE `postulant` CHANGE `status` `status` INT(11) NULL;

# Grades for entrance examination 
CREATE TABLE `postulant_grades` ( `id` INT NOT NULL AUTO_INCREMENT , `postulant` INT NOT NULL , `program` INT NOT NULL , `academic_year` INT NOT NULL , `grade_value` FLOAT NOT NULL , `date_create` DATETIME NULL , `create_by` VARCHAR(64) NULL , `date_update` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`postulant`), INDEX (`program`), INDEX (`academic_year`)) ENGINE = InnoDB; 

ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`program`) REFERENCES `program`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `postulant` ADD `recommadation_letter` BOOLEAN NULL AFTER `cohorte`, ADD `exam_date` DATE NULL AFTER `recommadation_letter`, ADD `no_inscription` VARCHAR(32) NULL AFTER `exam_date`;
ALTER TABLE `postulant` CHANGE `recommadation_letter` `recommandation_letter` TINYINT(1) NULL DEFAULT NULL;
 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'school_name', 'Nom établissement', 'Collège Canado-Haitien', 'Saisir le nom de l''établissement', 'Enter the name of the school', 'sys', '0000-00-00 00:00:00', '2016-05-31 00:00:00', '', ''),
(2, 'School_acronym', 'Acronyme établissement', '', 'Acronyme du nom de l''école. Placer entre parenthèse. ', 'Acronym for school name. Place within parenthesis. ', 'sys', '2015-09-19 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(3, 'school_address', 'Adresse établissement', '148,ave Jean Paul II  Port-au-Prince (Haiti)', 'Adresse de l''école', 'School address', 'sys', '0000-00-00 00:00:00', '2016-05-31 00:00:00', '', ''),
(4, 'school_phone_number', 'Tél établissement', '50922089042 / 50922089043', 'Saisir les numéros de téléphone, s''il y a plusieurs numéros, utiliser un "/" pour les séparer.', 'Enter the phone number, if there are multiple phone numbers, use a "/" to separated them.', 'sys', NULL, '2016-11-09 00:00:00', NULL, NULL),
(5, 'school_director_name', 'Nom Directeur', 'Frère Augustin Nelson, s.c', 'Saisir le nom du Directeur de l''école.', 'Enter the name of the director of the school.', 'sys', NULL, '2016-05-31 00:00:00', NULL, NULL),
(6, 'school_email_address', 'Email établissement', 'cchcanado@gmail.com', 'Saisir l''adresse email de l''école.', 'Enter school email address.', 'sys', NULL, '2016-05-31 00:00:00', NULL, NULL),
(7, 'school_site_web', 'Site web établissement', 'www.collegecanadohaitien.org', 'Saisir URL du site web de l''école. (Exemple: http:://logipam.com)', 'Enter the URL of the school site web (Example: http:://logipam.com)', 'sys', NULL, '2016-05-31 00:00:00', NULL, NULL),
(8, 'facebook_page', 'Page facebook', 'LOGIPAM', 'Adresse de la page facebook de l''établissement (Exemple : facebook.com/logipam -> Ecrivez : logipam) ', 'Facebook address of the institution. Example : facebook.com/logipam -> type logipam) ', 'sys', '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(9, 'twitter_page', 'Compte twitter', 'LOGIPAM', 'Nom d''utilisateur twitter de l''établissement.', 'Twitter username of the institution.', 'sys', '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(10, 'youtube_page', 'Page youtube', 'https://youtube.com', 'Adresse de la Chaine YouTube de l''institution.', 'Address of the YouTube Channel of the instutition.', 'sys', '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(11, 'devise_school', 'Devise établissement', 'Plus haut, plus loin... plus fort !', 'La phrase devise de l''établissement.', 'The motto phrase of the institution.', 'sys', '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(12, 'slogan', 'Autre Devise établissement', 'Dirigé par les Frères  du Sacré-Coeur', 'La phrase devise de l''établissement.', 'The motto phrase of the institution.', 'sys', NULL, '2016-05-31 00:00:00', 'admin', NULL),
(13, 'automatic_code', 'Code automatique', '0', '0 : pour inserrer le code de la personne manuellement; 1 : pour laisser à SIGES le soin de fournir le code automatiquement', '0 : to insert person''s code manually 1 : to let SIGES provides the code automatically', NULL, NULL, NULL, NULL, NULL),
(14, 'default_vacation', 'Vacation par défaut', 'Matin', 'Vacation  par defaut.', 'Default Shift.', 'acad', '2015-05-20 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(15, 'success_mention', 'Mention pour réussite', 'Succès', 'Message pour la mention de réussite', 'Message for success notification. ', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(16, 'failure_mention', 'Mention pour échec', 'Echec', 'Message en cas d''échec d''un élève.', 'Message for failed student.', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(17, 'course_shift', 'Cours & Vacations', '0', '0: Permettre le choix des cours dans une seule vacation; 1: Permettre le choix des cours dans plusieurs vacations', '', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL);


-- ---------------------------------------------------

RENAME TABLE `canado`.`users` TO `canado`.`user`;

drop table if exists `auth_assignment`;
drop table if exists `auth_item_child`;
drop table if exists `auth_item`;
drop table if exists `auth_rule`;

create table `auth_rule`
(
`name` varchar(64) not null,
`data` text,
`created_at` integer,
`updated_at` integer, primary key (`name`)
) engine InnoDB;

create table `auth_item`
(
`name` varchar(64) not null,
`type` integer not null,
`description` text,
`rule_name` varchar(64),
`data` text,
`created_at` integer,
`updated_at` integer,
primary key (`name`),
foreign key (`rule_name`) references `auth_rule` (`name`) on delete set null on update cascade,
key `type` (`type`)
) engine InnoDB;

create table `auth_item_child`
(
`parent` varchar(64) not null,
`child` varchar(64) not null,
primary key (`parent`, `child`),
foreign key (`parent`) references `auth_item` (`name`) on delete cascade on update cascade,
foreign key (`child`) references `auth_item` (`name`) on delete cascade on update cascade
) engine InnoDB;

create table `auth_assignment`
(
`item_name` varchar(64) not null,
`user_id` varchar(64) not null,
`created_at` integer,
primary key (`item_name`, `user_id`),
foreign key (`item_name`) references `auth_item` (`name`) on delete cascade on update cascade
) engine InnoDB;

ALTER TABLE `auth_assignment` CHANGE `user_id` `user_id` BIGINT NOT NULL;
ALTER TABLE `auth_assignment` CHANGE `user_id` `user_id` VARCHAR(64) NOT NULL;

ALTER TABLE `auth_assignment` ADD CONSTRAINT `auth_assignment_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
--Ajout auto-increment dans la table shift 
ALTER TABLE `shifts` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
-- Ajout auto-increment dans la table program 
ALTER TABLE `program` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
-- Ajout auto-increment dans la table subject 
ALTER TABLE `subjects` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
-- Mettre leaving_date a NULL
ALTER TABLE `student_other_info` CHANGE `leaving_date` `leaving_date` DATETIME NULL;

-- Multiple alter dans la tabl student_other_info
ALTER TABLE `student_other_info` CHANGE `health_state` `health_state` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `father_full_name` `father_full_name` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `mother_full_name` `mother_full_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `person_liable` `person_liable` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `date_updated` `date_updated` DATETIME NULL;

ALTER TABLE `courses` ADD `reference_id` INT(11) NULL COMMENT 'id kou li ranplase a ' AFTER `old_new`;
-- Alter table relation to add auto-increment
ALTER TABLE `relations` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

-- table events pour le planning 

CREATE TABLE `events` ( `id` INT NOT NULL AUTO_INCREMENT , `course` INT NULL , `description` TEXT NULL , `date_start` DATETIME NULL , `date_end` DATETIME NULL , `repeat_each_day` BOOLEAN NULL , `color` VARCHAR(32) NULL , `create_by` VARCHAR(32) NULL , `update_by` VARCHAR(32) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`course`)) ENGINE = InnoDB;
ALTER TABLE `events` ADD FOREIGN KEY (`course`) REFERENCES `courses`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- table holydays pour le planning 

CREATE TABLE `holydays` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NOT NULL , `description` TEXT NULL , `is_no_planning` BOOLEAN NULL , `description_no_planning` TEXT NULL , `date_start` DATETIME NOT NULL , `date_end` DATETIME NOT NULL , `all_day` BOOLEAN NULL , `color` VARCHAR(32) NULL , `repeat_each_day` BOOLEAN NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , `create_by` VARCHAR(32) NULL , `update_by` VARCHAR(32) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;



ALTER TABLE `contact_info` ADD CONSTRAINT `fk_contact_info_persons` FOREIGN KEY (`person`) REFERENCES `persons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `contact_info` ADD CONSTRAINT `fk_contact_info_relations` FOREIGN KEY (`contact_relationship`) REFERENCES `relations`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `events` ADD `time_start` TIME NULL AFTER `date_end`, ADD `time_end` TIME NULL AFTER `time_start`;


ALTER TABLE `user` ADD `person_id` INT(11) NOT NULL AFTER `id`, ADD INDEX (`person_id`) ;
ALTER TABLE `user` ADD CONSTRAINT `fk_user_persons` FOREIGN KEY (`person_id`) REFERENCES `persons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `user` CHANGE `email` `email` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `user` ADD `is_parent` INT(11) NULL DEFAULT NULL AFTER `person_id`;

ALTER TABLE `events` ADD `course_date_start` DATETIME NOT NULL AFTER `course`;
ALTER TABLE `events` ADD `course_date_end` DATETIME NOT NULL AFTER `course_date_start`, ADD `weekday` INT NOT NULL AFTER `course_date_end`;



ALTER TABLE `fees` CHANGE `level` `program` INT(11) NOT NULL; 

ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_program` FOREIGN KEY (`program`) REFERENCES `program`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_academicperiods` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_feeslabel` FOREIGN KEY (`fee`) REFERENCES `fees_label`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_persons` FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_fees` FOREIGN KEY (`fee_period`) REFERENCES `fees`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_paymentmethod` FOREIGN KEY (`payment_method`) REFERENCES `payment_method`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'currency_name_symbol', 'Nom et symbole monétaire', 'Gourde/HTG', 'Le nom et le symbol de la devise utilisée par l''école', 'Currency name and symbol used', 'econ', NULL, '2016-05-31 00:00:00', 'SIGES', NULL);



ALTER TABLE `events` ADD `lesson_complete` BOOLEAN NOT NULL DEFAULT TRUE AFTER `color`;

ALTER TABLE `payment_method` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `reservation` (
`id` int(11) NOT NULL,
  `postulant_student` int(11) NOT NULL,
  `is_student` tinyint(2) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `already_checked` tinyint(2) NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_reservation_payment_method` (`payment_method`), ADD KEY `fk_reservation_academic_year` (`academic_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
ADD CONSTRAINT `fk_reservation_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `fk_reservation_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE;


ALTER TABLE `billings` ADD `reservation_id` INT(11) NULL AFTER `balance`, ADD INDEX (`reservation_id`) ;
ALTER TABLE `billings` ADD CONSTRAINT `fk_biiling_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `scholarship_holder` ADD CONSTRAINT `fk_scholarship_holder_person` FOREIGN KEY (`student`) REFERENCES `canado`.`persons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `scholarship_holder` ADD CONSTRAINT `fk_scholarship_holder_acad` FOREIGN KEY (`academic_year`) REFERENCES `canado`.`academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_devises` FOREIGN KEY (`devise`) REFERENCES `canado`.`devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


ALTER TABLE `scholarship_holder` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `partners` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;






INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
('total_payroll', 'Nombre de payroll par année', '12', 'Nombre de payroll pour l\'annee', 'Number of payroll per year', 'econ', NULL, '2016-05-31 00:00:00', 'SIGES', NULL);

ALTER TABLE `payroll_settings` ADD `devise` INT(11) NULL AFTER `amount`, ADD INDEX (`devise`) ;

ALTER TABLE `payroll_settings` ADD CONSTRAINT `fk_payroll_settings_devises` FOREIGN KEY (`devise`) REFERENCES `canado`.`devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `taxes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;



ALTER TABLE `payroll_settings` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `loan_of_money` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `mails` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `menfp_grades` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `other_incomes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `other_incomes_description` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `passing_grades` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT; 
ALTER TABLE `payroll` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payroll_setting_taxes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pending_balance` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `person_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `postulant` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `products` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `qualifications` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `raise_salary` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `record_infraction` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `record_presence` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `relations` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `return_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `room_has_person` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT; 
ALTER TABLE `rpt_custom` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sale_transaction` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `scalendar` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sections` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sellings` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `stocks` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `stock_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_documents` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `menfp_decision` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payroll_settings` CHANGE `as` `as_` INT(2) NULL DEFAULT '0' COMMENT '0: employee; 1: teacher';



ALTER TABLE `balance` ADD `devise` INT(11) NOT NULL AFTER `balance`, ADD INDEX (`devise`) ;
ALTER TABLE `balance` ADD CONSTRAINT `fk_balance_devises` FOREIGN KEY (`devise`) REFERENCES `canado`.`devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `pending_balance` ADD `devise` INT(11) NOT NULL AFTER `balance`, ADD INDEX (`devise`) ;
ALTER TABLE `pending_balance` ADD CONSTRAINT `fk_pending_balance_devises` FOREIGN KEY (`devise`) REFERENCES `canado`.`devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `reservation` ADD `devise` INT(11) NOT NULL AFTER `amount`, ADD INDEX (`devise`) ;
ALTER TABLE `reservation` ADD CONSTRAINT `fk_reservation_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
