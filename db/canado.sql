-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2017 at 12:55 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `canado`
--

-- --------------------------------------------------------

--
-- Table structure for table `academicperiods`
--

CREATE TABLE `academicperiods` (
  `id` int(11) NOT NULL,
  `name_period` varchar(45) NOT NULL,
  `weight` double DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `is_year` tinyint(1) DEFAULT NULL,
  `previous_academic_year` int(11) NOT NULL DEFAULT '0',
  `year` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academicperiods`
--

INSERT INTO `academicperiods` (`id`, `name_period`, `weight`, `checked`, `date_start`, `date_end`, `is_year`, `previous_academic_year`, `year`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, '2017-218', NULL, 0, '0000-00-00', '0000-00-00', 1, 0, 1, '2017-05-29 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accounting`
--

CREATE TABLE `accounting` (
  `id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `expenses` double NOT NULL,
  `incomes` double NOT NULL,
  `new_balance` double NOT NULL,
  `month` int(3) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `create_by` varchar(128) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arrondissements`
--

CREATE TABLE `arrondissements` (
  `id` int(11) NOT NULL,
  `arrondissement_name` varchar(45) NOT NULL,
  `departement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `average_by_period`
--

CREATE TABLE `average_by_period` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `sum` double NOT NULL,
  `average` double NOT NULL,
  `place` int(11) NOT NULL,
  `reportcard_ref` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE `balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bareme`
--

CREATE TABLE `bareme` (
  `id` int(11) NOT NULL,
  `min_value` double NOT NULL,
  `max_value` double NOT NULL,
  `percentage` double NOT NULL,
  `compteur` int(11) NOT NULL,
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old bareme; 1: new bareme in use',
  `date_created` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `fee_period` int(11) NOT NULL,
  `amount_to_pay` float NOT NULL,
  `amount_pay` float NOT NULL,
  `balance` float DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_pay` date NOT NULL,
  `payment_method` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `fee_totally_paid` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: paiement partiel; 1: paiement total ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `charge_description`
--

CREATE TABLE `charge_description` (
  `id` int(11) NOT NULL,
  `description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `charge_paid`
--

CREATE TABLE `charge_paid` (
  `id` int(11) NOT NULL,
  `id_charge_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `city_name` varchar(45) NOT NULL,
  `arrondissement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `arrondissement`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Carrefour', 1, NULL, NULL, NULL, NULL),
(2, 'Cite-Soleil', 1, NULL, NULL, NULL, NULL),
(3, 'Delmas', 1, NULL, NULL, NULL, NULL),
(4, 'Tabarre', 1, NULL, NULL, NULL, NULL),
(5, 'Gressier', 1, NULL, NULL, NULL, NULL),
(6, 'Kenscoff', 1, NULL, NULL, NULL, NULL),
(7, 'Petion-Ville', 1, NULL, NULL, NULL, NULL),
(8, 'Port-Au-Prince', 1, NULL, NULL, NULL, NULL),
(9, 'Borgne', 2, NULL, NULL, NULL, NULL),
(10, 'Port-Margot', 2, NULL, NULL, NULL, NULL),
(11, 'Cap-Haitien', 3, NULL, NULL, NULL, NULL),
(12, 'Limonade', 3, NULL, NULL, NULL, NULL),
(13, 'Quartier-Morin', 3, NULL, NULL, NULL, NULL),
(14, 'Bahon', 4, NULL, NULL, NULL, NULL),
(15, 'Grande-Riviere Du Nord', 4, NULL, NULL, NULL, NULL),
(16, 'Acul-Du-Nord', 5, NULL, NULL, NULL, NULL),
(17, 'Milot', 5, NULL, NULL, NULL, NULL),
(18, 'Plaine-Du-Nord', 5, NULL, NULL, NULL, NULL),
(19, 'Bas-Limbe', 6, NULL, NULL, NULL, NULL),
(20, 'Limbe', 6, NULL, NULL, NULL, NULL),
(21, 'Pilate', 7, NULL, NULL, NULL, NULL),
(22, 'Plaisance', 7, NULL, NULL, NULL, NULL),
(23, 'La Victoire', 8, NULL, NULL, NULL, NULL),
(24, 'Pignon', 8, NULL, NULL, NULL, NULL),
(25, 'Ranquitte', 8, NULL, NULL, NULL, NULL),
(26, 'Dondon', 8, NULL, NULL, NULL, NULL),
(27, 'Saint-Raphael', 8, NULL, NULL, NULL, NULL),
(28, 'Arcahaie', 9, NULL, NULL, NULL, NULL),
(29, 'Cabaret', 9, NULL, NULL, NULL, NULL),
(30, 'Cornillon', 10, NULL, NULL, NULL, NULL),
(31, 'Croix Des Bouquets', 10, NULL, NULL, NULL, NULL),
(32, 'Thomazeau', 10, NULL, NULL, NULL, NULL),
(33, 'Fonds-Verrettes', 10, NULL, NULL, NULL, NULL),
(34, 'Ganthier', 10, NULL, NULL, NULL, NULL),
(35, 'Anse-A-Galets', 11, NULL, NULL, NULL, NULL),
(36, 'Pointe-A-Raquette', 11, NULL, NULL, NULL, NULL),
(37, 'Grand-Goave', 12, NULL, NULL, NULL, NULL),
(38, 'Leogane', 12, NULL, NULL, NULL, NULL),
(39, 'Petit-Goave', 12, NULL, NULL, NULL, NULL),
(40, 'Desdunes', 13, NULL, NULL, NULL, NULL),
(41, 'Dessalines', 13, NULL, NULL, NULL, NULL),
(42, 'Grande-Saline', 13, NULL, NULL, NULL, NULL),
(43, 'Petite-Riviere De L\'Artibonite', 13, NULL, NULL, NULL, NULL),
(44, 'Ennery', 14, NULL, NULL, NULL, NULL),
(45, 'Gonaives', 14, NULL, NULL, NULL, NULL),
(46, 'L\'Estere', 14, NULL, NULL, NULL, NULL),
(47, 'Gros-Morne', 15, NULL, NULL, NULL, NULL),
(48, 'Anse-Rouge', 15, NULL, NULL, NULL, NULL),
(49, 'Terre-Neuve', 15, NULL, NULL, NULL, NULL),
(50, 'Marmelade', 16, NULL, NULL, NULL, NULL),
(51, 'Saint-Michel De L\'Attalaye', 16, NULL, NULL, NULL, NULL),
(52, 'La Chapelle', 17, NULL, NULL, NULL, NULL),
(53, 'Saint-Marc', 17, NULL, NULL, NULL, NULL),
(54, 'Verrettes', 17, NULL, NULL, NULL, NULL),
(55, 'Cerca-La-Source', 18, NULL, NULL, NULL, NULL),
(56, 'Thomassique', 18, NULL, NULL, NULL, NULL),
(57, 'Cerca-Carvajal', 19, NULL, NULL, NULL, NULL),
(58, 'Hinche', 19, NULL, NULL, NULL, NULL),
(59, 'Maissade', 19, NULL, NULL, NULL, NULL),
(60, 'Thomonde', 19, NULL, NULL, NULL, NULL),
(61, 'Belladere', 20, NULL, NULL, NULL, NULL),
(62, 'Lascahobas', 20, NULL, NULL, NULL, NULL),
(63, 'Savanette', 20, NULL, NULL, NULL, NULL),
(64, 'Boucan-Carre', 21, NULL, NULL, NULL, NULL),
(65, 'Mirebalais', 21, NULL, NULL, NULL, NULL),
(66, 'Saut-D\'Eau', 21, NULL, NULL, NULL, NULL),
(67, 'Anse D\'Hainault', 22, NULL, NULL, NULL, NULL),
(68, 'Les Irois', 22, NULL, NULL, NULL, NULL),
(69, 'Dame-Marie', 22, NULL, NULL, NULL, NULL),
(70, 'Corail', 23, NULL, NULL, NULL, NULL),
(71, 'Roseaux', 23, NULL, NULL, NULL, NULL),
(72, 'Beaumont', 23, NULL, NULL, NULL, NULL),
(73, 'Pestel', 23, NULL, NULL, NULL, NULL),
(74, 'Abricots', 24, NULL, NULL, NULL, NULL),
(75, 'Bonbon', 24, NULL, NULL, NULL, NULL),
(76, 'Jeremie', 24, NULL, NULL, NULL, NULL),
(77, 'Chambellan', 24, NULL, NULL, NULL, NULL),
(78, 'Moron', 24, NULL, NULL, NULL, NULL),
(79, 'Anse-A-Veau', 25, NULL, NULL, NULL, NULL),
(80, 'Arnaud', 25, NULL, NULL, NULL, NULL),
(81, 'L\'Asile', 25, NULL, NULL, NULL, NULL),
(82, 'Petit-Trou De Nippes', 25, NULL, NULL, NULL, NULL),
(83, 'Plaisance Du Sud', 25, NULL, NULL, NULL, NULL),
(84, 'Baraderes', 26, NULL, NULL, NULL, NULL),
(85, 'Grand-Boucan', 26, NULL, NULL, NULL, NULL),
(86, 'Fonds-Des-Negres', 27, NULL, NULL, NULL, NULL),
(87, 'Miragoane', 27, NULL, NULL, NULL, NULL),
(88, 'Paillant', 27, NULL, NULL, NULL, NULL),
(89, 'Petite Riviere De Nippes', 27, NULL, NULL, NULL, NULL),
(90, 'Ferrier', 28, NULL, NULL, NULL, NULL),
(91, 'Fort-Liberte', 28, NULL, NULL, NULL, NULL),
(92, 'Perches', 28, NULL, NULL, NULL, NULL),
(93, 'Capotille', 29, NULL, NULL, NULL, NULL),
(94, 'Mont-Organise', 29, NULL, NULL, NULL, NULL),
(95, 'Ouanaminthe', 29, NULL, NULL, NULL, NULL),
(96, 'Sainte-Suzanne', 30, NULL, NULL, NULL, NULL),
(97, 'Terrier-Rouge', 30, NULL, NULL, NULL, NULL),
(98, 'Caracol', 30, NULL, NULL, NULL, NULL),
(99, 'Trou-Du-Nord', 30, NULL, NULL, NULL, NULL),
(100, 'Carice', 31, NULL, NULL, NULL, NULL),
(101, 'Mombin Crochu', 31, NULL, NULL, NULL, NULL),
(102, 'Vallieres', 31, NULL, NULL, NULL, NULL),
(103, 'Baie-De-Henne', 32, NULL, NULL, NULL, NULL),
(104, 'Bombardopolis', 32, NULL, NULL, NULL, NULL),
(105, 'Jean Rabel', 32, NULL, NULL, NULL, NULL),
(106, 'Mole Saint-Nicolas', 32, NULL, NULL, NULL, NULL),
(107, 'Bassin Bleu', 33, NULL, NULL, NULL, NULL),
(108, 'Chansolme', 33, NULL, NULL, NULL, NULL),
(109, 'La Tortue', 33, NULL, NULL, NULL, NULL),
(110, 'Port-De-Paix', 33, NULL, NULL, NULL, NULL),
(111, 'Anse-A-Foleur', 34, NULL, NULL, NULL, NULL),
(112, 'Saint-Louis Du Nord', 34, NULL, NULL, NULL, NULL),
(113, 'Aquin', 35, NULL, NULL, NULL, NULL),
(114, 'Cavaillon', 35, NULL, NULL, NULL, NULL),
(115, 'Saint-Louis Du Sud', 35, NULL, NULL, NULL, NULL),
(116, 'Chardonnieres', 36, NULL, NULL, NULL, NULL),
(117, 'Les Anglais', 36, NULL, NULL, NULL, NULL),
(118, 'Tiburon', 36, NULL, NULL, NULL, NULL),
(119, 'Coteaux', 37, NULL, NULL, NULL, NULL),
(120, 'Port-A-Piment', 37, NULL, NULL, NULL, NULL),
(121, 'Roche-A-Bateau', 37, NULL, NULL, NULL, NULL),
(122, 'Camp-Perrin', 38, NULL, NULL, NULL, NULL),
(123, 'Maniche', 38, NULL, NULL, NULL, NULL),
(124, 'Cayes', 38, NULL, NULL, NULL, NULL),
(125, 'Ile-A-Vache', 38, NULL, NULL, NULL, NULL),
(126, 'Chantal', 38, NULL, NULL, NULL, NULL),
(127, 'Torbeck', 38, NULL, NULL, NULL, NULL),
(128, 'Port-Salut', 39, NULL, NULL, NULL, NULL),
(129, 'Arniquet', 39, NULL, NULL, NULL, NULL),
(130, 'Saint-Jean Du Sud', 39, NULL, NULL, NULL, NULL),
(131, 'Bainet', 40, NULL, NULL, NULL, NULL),
(132, 'Cotes De Fer', 40, NULL, NULL, NULL, NULL),
(133, 'Anse-A-Pitre', 41, NULL, NULL, NULL, NULL),
(134, 'Belle-Anse', 41, NULL, NULL, NULL, NULL),
(135, 'Grand-Gosier', 41, NULL, NULL, NULL, NULL),
(136, 'Thiotte', 41, NULL, NULL, NULL, NULL),
(137, 'Jacmel', 42, NULL, NULL, NULL, NULL),
(138, 'La Vallee De Jacmel', 42, NULL, NULL, NULL, NULL),
(139, 'Cayes-Jacmel', 42, NULL, NULL, NULL, NULL),
(140, 'Marigot', 42, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `id` int(11) NOT NULL,
  `person` int(11) NOT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `contact_relationship` int(11) DEFAULT NULL,
  `profession` varchar(100) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `one_more` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `shift` int(11) NOT NULL,
  `weight` float DEFAULT NULL COMMENT 'Weight : Le coefficient du cours',
  `debase` tinyint(1) NOT NULL DEFAULT '0',
  `optional` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: not optional; 1: optional(reportcard will not take care about))',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old ; 1: new, in use',
  `passing_grade` float DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field`
--

CREATE TABLE `custom_field` (
  `id` int(11) NOT NULL,
  `field_name` varchar(64) NOT NULL,
  `field_label` varchar(45) DEFAULT NULL,
  `field_type` varchar(45) DEFAULT 'text',
  `value_type` varchar(16) DEFAULT NULL,
  `field_option` text,
  `field_related_to` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_data`
--

CREATE TABLE `custom_field_data` (
  `id` bigint(20) NOT NULL,
  `field_link` int(11) NOT NULL,
  `field_data` text,
  `object_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `decision_finale`
--

CREATE TABLE `decision_finale` (
  `id` bigint(20) NOT NULL,
  `student` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `general_average` float DEFAULT NULL,
  `mention` varchar(45) DEFAULT NULL,
  `comments` varchar(128) DEFAULT NULL,
  `is_move_to_next_year` tinyint(1) DEFAULT NULL,
  `current_level` int(11) DEFAULT NULL,
  `next_level` int(11) DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department_has_person`
--

CREATE TABLE `department_has_person` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department_in_school`
--

CREATE TABLE `department_in_school` (
  `id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devises`
--

CREATE TABLE `devises` (
  `id` int(11) NOT NULL,
  `devise_name` varchar(45) NOT NULL,
  `devise_symbol` varchar(45) NOT NULL,
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE `employee_info` (
  `id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `country_of_birth` varchar(45) NOT NULL,
  `university_or_school` varchar(45) DEFAULT NULL,
  `number_of_year_of_study` int(11) DEFAULT NULL,
  `field_study` int(11) DEFAULT NULL,
  `qualification` int(11) DEFAULT NULL,
  `job_status` int(11) DEFAULT NULL,
  `permis_enseignant` varchar(45) NOT NULL,
  `leaving_date` date DEFAULT NULL,
  `comments` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_income`
--

CREATE TABLE `enrollment_income` (
  `id` int(11) NOT NULL,
  `postulant` int(11) NOT NULL,
  `apply_level` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(11) NOT NULL,
  `evaluation_name` varchar(64) NOT NULL,
  `weight` float DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_by_year`
--

CREATE TABLE `evaluation_by_year` (
  `id` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `evaluation_date` date NOT NULL,
  `last_evaluation` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `examen_menfp`
--

CREATE TABLE `examen_menfp` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `amount` float NOT NULL,
  `devise` int(11) DEFAULT NULL,
  `date_limit_payment` date DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fees_label`
--

CREATE TABLE `fees_label` (
  `id` int(11) NOT NULL,
  `fee_label` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: other fees, no pending; 1: tuition fees, pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_study`
--

CREATE TABLE `field_study` (
  `id` int(11) NOT NULL,
  `field_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_config`
--

CREATE TABLE `general_config` (
  `id` int(11) NOT NULL,
  `item_name` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `item_value` text,
  `description` text,
  `english_comment` text,
  `category` varchar(12) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `grade_value` float DEFAULT NULL,
  `validate` tinyint(4) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homework`
--

CREATE TABLE `homework` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `limit_date_submission` date NOT NULL,
  `given_date` date NOT NULL,
  `attachment_ref` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homework_submission`
--

CREATE TABLE `homework_submission` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `homework_id` int(11) NOT NULL,
  `date_submission` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `attachment_ref` varchar(100) NOT NULL,
  `grade_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `infraction_type`
--

CREATE TABLE `infraction_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `deductible_value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE `job_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `label_category_for_billing`
--

CREATE TABLE `label_category_for_billing` (
  `id` int(11) NOT NULL,
  `category` varchar(220) NOT NULL,
  `income_expense` varchar(3) NOT NULL COMMENT 'ri: income; di: expense'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL COMMENT 'School level (calling classes dans le system scolaire Haitien)',
  `level_name` varchar(45) NOT NULL,
  `short_level_name` varchar(45) NOT NULL,
  `previous_level` int(11) DEFAULT NULL,
  `section` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level_has_person`
--

CREATE TABLE `level_has_person` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loan_of_money`
--

CREATE TABLE `loan_of_money` (
  `id` int(11) NOT NULL,
  `loan_date` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payroll_month` int(3) NOT NULL,
  `deduction_percentage` int(3) NOT NULL,
  `solde` double NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `number_of_month_repayment` int(5) NOT NULL,
  `remaining_month_number` int(5) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` bigint(20) NOT NULL,
  `description` longtext NOT NULL COMMENT 'Format json : prendra en compte nom utilisateur- action- donnees modifies',
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(11) NOT NULL,
  `sender` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `receivers` text CHARACTER SET utf8,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `is_read` int(1) DEFAULT NULL,
  `id_sender` int(11) DEFAULT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` int(11) DEFAULT NULL,
  `is_my_send` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_decision`
--

CREATE TABLE `menfp_decision` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `total_grade` double NOT NULL,
  `average` double NOT NULL,
  `mention` varchar(100) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_grades`
--

CREATE TABLE `menfp_grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `menfp_exam` int(11) NOT NULL,
  `grade` double NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `code` varchar(12) NOT NULL,
  `duration` int(11) NOT NULL,
  `in_use` tinyint(4) NOT NULL DEFAULT '1',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `subject`, `program`, `code`, `duration`, `in_use`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 1, 'M-001', 150, 1, '2017-05-30 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes`
--

CREATE TABLE `other_incomes` (
  `id` int(11) NOT NULL,
  `id_income_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `income_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes_description`
--

CREATE TABLE `other_incomes_description` (
  `id` int(11) NOT NULL,
  `income_description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `activity_field` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `passing_grades`
--

CREATE TABLE `passing_grades` (
  `id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  `academic_period` int(11) NOT NULL,
  `minimum_passing` float NOT NULL,
  `level_or_course` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: level; 1: course',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `method_name` varchar(45) NOT NULL,
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_payroll_set2` int(11) DEFAULT NULL COMMENT 'recois l''id_payrollsetting professeur s''il est a la fois employe et professeur',
  `payroll_month` int(3) NOT NULL,
  `payroll_date` date NOT NULL,
  `missing_hour` int(11) NOT NULL,
  `taxe` double NOT NULL,
  `net_salary` double NOT NULL,
  `payment_date` date NOT NULL,
  `cash_check` varchar(45) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_settings`
--

CREATE TABLE `payroll_settings` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `an_hour` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:no; 1:yes',
  `number_of_hour` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `as` int(2) DEFAULT '0' COMMENT '0: employee; 1: teacher',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old setting; 1: new setting',
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_setting_taxes`
--

CREATE TABLE `payroll_setting_taxes` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_taxe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pending_balance`
--

CREATE TABLE `pending_balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not yet; 1: paid',
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `blood_group` varchar(10) NOT NULL,
  `birthday` date DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL,
  `is_student` tinyint(1) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nif_cin` varchar(100) NOT NULL,
  `cities` int(11) DEFAULT NULL,
  `citizenship` varchar(45) NOT NULL,
  `mother_first_name` varchar(55) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `identifiant` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `matricule` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `paid` tinyint(2) DEFAULT NULL COMMENT 'for admission list. 0: not yet paid; 1: already paid; NULL: left admission list ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `last_name`, `first_name`, `gender`, `blood_group`, `birthday`, `id_number`, `is_student`, `adresse`, `phone`, `email`, `nif_cin`, `cities`, `citizenship`, `mother_first_name`, `identifiant`, `matricule`, `paid`, `date_created`, `date_updated`, `create_by`, `update_by`, `active`, `image`, `comment`) VALUES
(1, 'PIERRE LOUIS', 'stephan', '0', 'A+', NULL, '', 1, '', '', 'steph@gmail.com', '0998323566', NULL, 'Ayitien', '', '', '', NULL, NULL, '2017-05-27 00:00:00', '', '', 2, NULL, 'Yes'),
(2, 'RICHE', 'Alyne', '1', 'A+', NULL, '', 1, '', '', '', '09123566543', NULL, 'Ayitien', '', '', '', NULL, NULL, '2017-05-29 00:00:00', '', '', 1, 'documents/photo_upload/alyneriche2.jpeg', 'sghmn'),
(3, 'Normile', 'Etienne', '0', 'B+', NULL, '', 0, '', '', '', '345', NULL, 'ys', '', '', '', NULL, NULL, NULL, '', '', 1, '', 'asfa');

-- --------------------------------------------------------

--
-- Table structure for table `persons_has_titles`
--

CREATE TABLE `persons_has_titles` (
  `persons_id` int(11) NOT NULL,
  `titles_id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_history`
--

CREATE TABLE `person_history` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `entry_hire_date` datetime DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `disable_date` datetime NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `profil` varchar(32) NOT NULL,
  `job_status_name` varchar(100) NOT NULL,
  `last_level_name` varchar(70) DEFAULT NULL,
  `academic_year` varchar(70) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `postulant`
--

CREATE TABLE `postulant` (
  `id` int(11) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `cities` int(11) DEFAULT NULL,
  `adresse` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `health_state` varchar(255) NOT NULL,
  `person_liable` varchar(100) NOT NULL,
  `person_liable_phone` varchar(65) NOT NULL,
  `person_liable_adresse` varchar(255) NOT NULL,
  `person_liable_relation` int(11) DEFAULT NULL,
  `apply_for_level` int(11) NOT NULL,
  `previous_level` int(11) NOT NULL,
  `previous_school` varchar(255) NOT NULL,
  `school_date_entry` date NOT NULL,
  `last_average` double DEFAULT NULL,
  `status` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `stock_alert` int(11) NOT NULL DEFAULT '0',
  `is_forsale` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `label`, `description`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Mecanique ajustable', 'xcvbn rtyjk', '2017-05-30 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL,
  `qualification_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `raise_salary`
--

CREATE TABLE `raise_salary` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `raising_date` date NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_infraction`
--

CREATE TABLE `record_infraction` (
  `id` bigint(20) NOT NULL,
  `student` int(20) NOT NULL,
  `infraction_type` int(11) NOT NULL,
  `record_by` varchar(64) NOT NULL,
  `incident_date` date NOT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `incident_description` text NOT NULL,
  `decision_description` text,
  `value_deduction` float DEFAULT NULL,
  `general_comment` text,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_presence`
--

CREATE TABLE `record_presence` (
  `id` int(11) NOT NULL,
  `student` int(20) NOT NULL,
  `room` int(11) DEFAULT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `date_record` date NOT NULL,
  `presence_type` int(11) NOT NULL,
  `comments` text,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `relation_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_history`
--

CREATE TABLE `return_history` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `return_amount` float NOT NULL,
  `return_quantity` int(11) NOT NULL,
  `date_return` datetime NOT NULL,
  `return_by` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  `short_room_name` varchar(45) NOT NULL,
  `shift` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `room_has_person`
--

CREATE TABLE `room_has_person` (
  `id` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rpt_custom`
--

CREATE TABLE `rpt_custom` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `academic_year` int(11) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sale_transaction`
--

CREATE TABLE `sale_transaction` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `amount_sale` float NOT NULL,
  `discount` float DEFAULT NULL,
  `amount_receive` float NOT NULL,
  `amount_balance` float NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scalendar`
--

CREATE TABLE `scalendar` (
  `id` int(11) NOT NULL,
  `c_title` varchar(255) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_all_day_event` smallint(6) NOT NULL,
  `color` varchar(200) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_holder`
--

CREATE TABLE `scholarship_holder` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `partner` int(11) DEFAULT NULL,
  `fee` int(11) DEFAULT NULL COMMENT 'Do not let NULL value if it is not a whole scholarship please specify',
  `percentage_pay` double NOT NULL,
  `is_internal` tinyint(1) NOT NULL DEFAULT '0',
  `academic_year` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `section_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sellings`
--

CREATE TABLE `sellings` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `id_products` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `selling_date` datetime NOT NULL,
  `client_name` varchar(128) DEFAULT NULL,
  `sell_by` varchar(64) DEFAULT NULL,
  `amount_receive` float DEFAULT NULL,
  `amount_selling` float DEFAULT NULL,
  `amount_balance` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `unit_selling_price` float DEFAULT NULL,
  `is_return` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  `user_id` int(11) NOT NULL,
  `last_activity` datetime NOT NULL,
  `last_ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(45) NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `shift_name`, `time_start`, `time_end`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Matin', '09:00:00', '01:45:00', NULL, '2017-05-30 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `acquisition_date` date NOT NULL,
  `buiying_price` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `is_donation` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock_history`
--

CREATE TABLE `stock_history` (
  `id` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `buying_date` date NOT NULL,
  `buying_price` float NOT NULL,
  `selling_price` float NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `id` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `file_name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_has_courses`
--

CREATE TABLE `student_has_courses` (
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `is_pass` tinyint(4) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_other_info`
--

CREATE TABLE `student_other_info` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `school_date_entry` date DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `previous_school` varchar(255) DEFAULT NULL,
  `previous_level` varchar(45) DEFAULT NULL,
  `apply_for_level` varchar(45) DEFAULT NULL,
  `health_state` varchar(255) NOT NULL,
  `father_full_name` varchar(45) NOT NULL,
  `mother_full_name` varchar(100) NOT NULL,
  `person_liable` varchar(100) NOT NULL,
  `person_liable_phone` varchar(65) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(45) NOT NULL,
  `short_subject_name` varchar(5) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `short_subject_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Formation & Metier', 'F&M', '2017-05-29 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_average`
--

CREATE TABLE `subject_average` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `average` double NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(100) NOT NULL,
  `update_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `taxe_description` varchar(120) NOT NULL,
  `employeur_employe` int(2) DEFAULT NULL COMMENT '0: employe; 1: employeur',
  `taxe_value` double NOT NULL,
  `particulier` int(1) NOT NULL DEFAULT '0' COMMENT '0: for general taxes; 1: for a particular tax ',
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `title_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `person_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `profil` int(11) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `is_parent` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `last_ip` varchar(100) NOT NULL,
  `last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academicperiods`
--
ALTER TABLE `academicperiods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_periods_academic_periods1_idx` (`year`);

--
-- Indexes for table `accounting`
--
ALTER TABLE `accounting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`,`create_by`);

--
-- Indexes for table `arrondissements`
--
ALTER TABLE `arrondissements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `arrondissement_name` (`arrondissement_name`),
  ADD KEY `departement` (`departement`);

--
-- Indexes for table `average_by_period`
--
ALTER TABLE `average_by_period`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`student`),
  ADD KEY `fk_average_by_period_eval_by_y` (`evaluation_by_year`),
  ADD KEY `fk_average_by_period_person` (`student`);

--
-- Indexes for table `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `bareme`
--
ALTER TABLE `bareme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_biiling_student_idx` (`student`),
  ADD KEY `fk_payment_method_idx` (`payment_method`),
  ADD KEY `fk_billings_fee_period` (`fee_period`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `charge_description`
--
ALTER TABLE `charge_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `charge_paid`
--
ALTER TABLE `charge_paid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_charge_description` (`id_charge_description`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_UNIQUE` (`city_name`),
  ADD KEY `fk_cities_arrondissement_idx` (`arrondissement`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_contact_info_idx` (`person`),
  ADD KEY `fk_relationship_idx` (`contact_relationship`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_couse_subject_idx` (`module`),
  ADD KEY `fk_course_teacher_idx` (`teacher`),
  ADD KEY `room_idx` (`room`),
  ADD KEY `fk_course_period_academic_idx` (`academic_year`),
  ADD KEY `fk_course_shift_idx` (`shift`);

--
-- Indexes for table `custom_field`
--
ALTER TABLE `custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_custom_field_idx` (`field_link`),
  ADD KEY `fk_object_id_idx` (`object_id`);

--
-- Indexes for table `decision_finale`
--
ALTER TABLE `decision_finale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_decision_idx` (`student`),
  ADD KEY `fk_academic_year_decision_idx` (`academic_year`),
  ADD KEY `fk_current_level_idx` (`current_level`),
  ADD KEY `fk_next_level_idx` (`next_level`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_name_UNIQUE` (`department_name`);

--
-- Indexes for table `department_has_person`
--
ALTER TABLE `department_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `employee` (`employee`);

--
-- Indexes for table `department_in_school`
--
ALTER TABLE `department_in_school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devises`
--
ALTER TABLE `devises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `devise_name_UNIQUE` (`devise_name`),
  ADD UNIQUE KEY `devise_symbol_UNIQUE` (`devise_symbol`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_qualification_idx` (`qualification`),
  ADD KEY `fk_employee_job_status_idx` (`job_status`),
  ADD KEY `fk_employee_field_of_study_idx` (`field_study`),
  ADD KEY `fk_employee_person` (`employee`);

--
-- Indexes for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postulant` (`postulant`),
  ADD KEY `apply_level` (`apply_level`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `payment_method` (`payment_method`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_evaluation_year_evaluation_idx` (`evaluation`),
  ADD KEY `fk_evaluation_year_academic_year_idx` (`academic_year`);

--
-- Indexes for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `subject` (`subject`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `devise` (`devise`),
  ADD KEY `fee` (`fee`);

--
-- Indexes for table `fees_label`
--
ALTER TABLE `fees_label`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_study`
--
ALTER TABLE `field_study`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `field_name_UNIQUE` (`field_name`);

--
-- Indexes for table `general_config`
--
ALTER TABLE `general_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_name_UNIQUE` (`item_name`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grades_student_idx` (`student`),
  ADD KEY `fk_grades_course_idx` (`course`);

--
-- Indexes for table `homework`
--
ALTER TABLE `homework`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course` (`course`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `homework_submission`
--
ALTER TABLE `homework_submission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `homework_id` (`homework_id`);

--
-- Indexes for table `infraction_type`
--
ALTER TABLE `infraction_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `job_status`
--
ALTER TABLE `job_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_UNIQUE` (`status_name`);

--
-- Indexes for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_name_UNIQUE` (`level_name`),
  ADD KEY `fk_levels_levels1_idx` (`previous_level`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `level_has_person`
--
ALTER TABLE `level_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`level`);

--
-- Indexes for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from` (`sender`(191),`subject`(191),`is_read`),
  ADD KEY `id_sender` (`id_sender`);

--
-- Indexes for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `menfp_exam` (`menfp_exam`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`),
  ADD KEY `fk_mod_subject_idx` (`subject`),
  ADD KEY `fk_mod_program_idx` (`program`);

--
-- Indexes for table `other_incomes`
--
ALTER TABLE `other_incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_income_description` (`id_income_description`);

--
-- Indexes for table `other_incomes_description`
--
ALTER TABLE `other_incomes_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passing_grades`
--
ALTER TABLE `passing_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_level_passing_grade_idx` (`level`),
  ADD KEY `fk_academic_period_passing_idx` (`academic_period`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `method_name_UNIQUE` (`method_name`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_paroll_set` (`id_payroll_set`);

--
-- Indexes for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`),
  ADD KEY `fk_payroll_setting_academicperiods` (`academic_year`);

--
-- Indexes for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`id_payroll_set`),
  ADD KEY `id_taxe` (`id_taxe`);

--
-- Indexes for table `pending_balance`
--
ALTER TABLE `pending_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pending_balance_persons` (`student`),
  ADD KEY `fk_pending_balance_academicperiods` (`academic_year`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persons_cities1_idx` (`cities`);

--
-- Indexes for table `persons_has_titles`
--
ALTER TABLE `persons_has_titles`
  ADD PRIMARY KEY (`persons_id`,`titles_id`,`academic_year`),
  ADD KEY `fk_persons_has_titles_titles1_idx` (`titles_id`),
  ADD KEY `fk_persons_has_titles_persons1_idx` (`persons_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `person_history`
--
ALTER TABLE `person_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `postulant`
--
ALTER TABLE `postulant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities` (`cities`),
  ADD KEY `person_liable_relation` (`person_liable_relation`),
  ADD KEY `apply_for_level` (`apply_for_level`),
  ADD KEY `previous_level` (`previous_level`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_name` (`product_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label_UNIQUE` (`label`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qualification_name_UNIQUE` (`qualification_name`);

--
-- Indexes for table `raise_salary`
--
ALTER TABLE `raise_salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`);

--
-- Indexes for table `record_infraction`
--
ALTER TABLE `record_infraction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indx_stud_infrac` (`student`),
  ADD KEY `infraction_type` (`infraction_type`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `record_presence`
--
ALTER TABLE `record_presence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `room` (`room`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relation_name_UNIQUE` (`relation_name`);

--
-- Indexes for table `return_history`
--
ALTER TABLE `return_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_room_shift_idx` (`shift`);

--
-- Indexes for table `room_has_person`
--
ALTER TABLE `room_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`room`);

--
-- Indexes for table `rpt_custom`
--
ALTER TABLE `rpt_custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaction` (`id_transaction`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `scalendar`
--
ALTER TABLE `scalendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scalendar_academic_year` (`academic_year`);

--
-- Indexes for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scholarship_holder_student` (`student`),
  ADD KEY `fk_scholarship_holder_academicperiods` (`academic_year`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sellings`
--
ALTER TABLE `sellings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_products` (`id_products`),
  ADD KEY `transac` (`transaction_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_stock` (`id_stock`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_documents_persons` (`id_student`);

--
-- Indexes for table `student_has_courses`
--
ALTER TABLE `student_has_courses`
  ADD PRIMARY KEY (`student`,`course`),
  ADD KEY `fk_persons_has_courses_courses1_idx` (`course`),
  ADD KEY `fk_persons_has_courses_persons1_idx` (`student`);

--
-- Indexes for table `student_other_info`
--
ALTER TABLE `student_other_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_other_info` (`student`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_average`
--
ALTER TABLE `subject_average`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`course`),
  ADD KEY `fk_subject_average_evaluation_byyear` (`evaluation_by_year`),
  ADD KEY `fk_subject_average_course` (`course`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title_name_UNIQUE` (`title_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `person_idx` (`person_id`),
  ADD KEY `profil` (`profil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academicperiods`
--
ALTER TABLE `academicperiods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `accounting`
--
ALTER TABLE `accounting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `arrondissements`
--
ALTER TABLE `arrondissements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balance`
--
ALTER TABLE `balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bareme`
--
ALTER TABLE `bareme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `charge_description`
--
ALTER TABLE `charge_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `charge_paid`
--
ALTER TABLE `charge_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `contact_info`
--
ALTER TABLE `contact_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom_field`
--
ALTER TABLE `custom_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `decision_finale`
--
ALTER TABLE `decision_finale`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department_has_person`
--
ALTER TABLE `department_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department_in_school`
--
ALTER TABLE `department_in_school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `devises`
--
ALTER TABLE `devises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees_label`
--
ALTER TABLE `fees_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `field_study`
--
ALTER TABLE `field_study`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_config`
--
ALTER TABLE `general_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `homework`
--
ALTER TABLE `homework`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `homework_submission`
--
ALTER TABLE `homework_submission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `infraction_type`
--
ALTER TABLE `infraction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `job_status`
--
ALTER TABLE `job_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'School level (calling classes dans le system scolaire Haitien)';
--
-- AUTO_INCREMENT for table `level_has_person`
--
ALTER TABLE `level_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `fk_course_academicyear` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_course_module` FOREIGN KEY (`module`) REFERENCES `module` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_course_room` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_course_shift` FOREIGN KEY (`shift`) REFERENCES `shifts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_course_teacher` FOREIGN KEY (`teacher`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `fk_grades_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_grades_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `fk_mod_program` FOREIGN KEY (`program`) REFERENCES `program` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mod_subject` FOREIGN KEY (`subject`) REFERENCES `subjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `student_has_courses`
--
ALTER TABLE `student_has_courses`
  ADD CONSTRAINT `fk_persons_has_courses_courses1` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persons_has_courses_persons1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
