
-- 23/12/2019
ALTER TABLE `year_migration_check` ADD `marge_echec` INT NOT NULL AFTER `appreciations_grid`;



-- xx-yy-
ALTER TABLE `year_migration_check` DROP `period`, DROP `student`, DROP `evaluation`, DROP `passing_grade`, DROP `reportcard_observation`;

ALTER TABLE `year_migration_check` ADD `payroll_setting` INT(11) NOT NULL DEFAULT '0' AFTER `taxes`;
 


-- 06/10/2019
-- 'Nombre de succès par groupe et par année'
INSERT INTO `rpt_custom` ( `title`, `data`, `parameters`, `academic_year`, `categorie`, `variables`, `setup_variable`, `create_by`, `create_date`) VALUES( 'Nombre de succès par groupe et par année', 'SELECT DISTINCT p.first_name AS \'Prénom\', p.last_name AS \'Nom\',pr.short_name AS \'Programme\', if(slh.level=3,\'Spécialisation\',slh.level) AS \'Niveau\', sh.shift_name AS \'Vacation\',r.room_name AS \'Groupe\' FROM student_level_history slh INNER JOIN persons p ON (slh.student_id = p.id) INNER JOIN academicperiods ap ON (slh.academic_year = ap.id) INNER JOIN student_other_info soi ON (soi.student = p.id) INNER JOIN program pr ON (soi.apply_for_program = pr.id) INNER JOIN shifts sh ON (soi.apply_shift = sh.id) INNER JOIN rooms r ON (slh.room = r.id) WHERE p.is_student=1 AND p.active in(1,2) AND is_pass=1 AND slh.room ={%groupe%} AND slh.academic_year={%annee%} ORDER BY pr.short_name ASC, shift_name ASC, slh.level ASC, last_name ASC,first_name ASC\r\n', '', NULL, 1, 'groupe,annee', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, room_name FROM rooms order BY room_name ASC\",\"dynamic-combo1\":\"SELECT id, name_period FROM academicperiods WHERE is_year=1 order BY date_end DESC\"}}', '_super_', '2019-10-06 19:09:13');



-- 08/08/2019

INSERT INTO auth_item(name, type) VALUES('fi-margeechec-create','2');
INSERT INTO auth_item(name, type) VALUES('fi-margeechec-update','2');
INSERT INTO auth_item(name, type) VALUES('fi-margeechec-delete','2');
INSERT INTO auth_item(name, type) VALUES('fi-margeechec-index','2');


-- Structure de la table `marge_echec`
CREATE TABLE `marge_echec` (
  `id` int(11) NOT NULL,
  `program` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `quantite_module` int(11) NOT NULL,
  `borne_sup` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Index pour la table `marge_echec`
ALTER TABLE `marge_echec`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_marge_echec_program` (`program`),
  ADD KEY `fk_marge_echec_academicperiod` (`academic_year`);

-- AUTO_INCREMENT pour la table `marge_echec`
ALTER TABLE `marge_echec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Contraintes pour la table `marge_echec`
ALTER TABLE `marge_echec`
  ADD CONSTRAINT `fk_marge_echec_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_marge_echec_program` FOREIGN KEY (`program`) REFERENCES `program` (`id`) ON UPDATE CASCADE;


--------------------------------------------------------------------------------------
-- 02/08/2019

ALTER TABLE `student_level_history` ADD `nbr_hold_module` INT(5) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `student_level_history` ADD `is_pass` INT(5) NULL DEFAULT NULL COMMENT '0: echec ; 1: succes' AFTER `nbr_hold_module`;


-- -----------------------------------------------------------------------------------------

-- 29/07/2019

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'rencontredirection_dateheure', 'Date et heure de la rencontre avec la direction pour une prise de decision', '15/08/2019 A 10H 00', 'Date et heure de la rencontre avec la direction pour une prise de decision', '', '', '', '', NULL, NULL);


-- -------------------------------------------------------------------------------------------

-- 18/07/2019

INSERT INTO auth_item(name, type) VALUES('fi-studenthascourses-decision','2');
INSERT INTO auth_item(name, type) VALUES('fi-studenthascourses-decisionlist','2');
INSERT INTO auth_item(name, type) VALUES('fi-studenthascourses-rollbackdecision','2');

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'first_year_use', 'Première année d\'utilisation', '0', 'Pour selectionner les etudiants selon le niveau ou pas', '', '', '', '', NULL, NULL);



CREATE TABLE `student_level_history` (
  `student_id` int(11) NOT NULL,
  `level` int(5) NOT NULL,
  `room` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `status` int(5) DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `student_level_history`
  ADD PRIMARY KEY (`student_id`,`academic_year`),
  ADD KEY `fk_student_level_history_academicperiod` (`academic_year`);

ALTER TABLE `student_level_history`
  ADD CONSTRAINT `fk_student_level_history_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_level_history_persons` FOREIGN KEY (`student_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

-- ----------------------------------------------------------------------



-- 12/04/2019
INSERT INTO auth_item(name, type) VALUES('fi-grades-update_validatedG','2');



-- ----------------------------------------------------------------------

INSERT INTO auth_item(name, type) VALUES('fi-grades-teacher_create','2');
INSERT INTO auth_item(name, type) VALUES('fi-grades-teacher_update','2');
INSERT INTO auth_item(name, type) VALUES('fi-grades-teacher_delete','2');

-- 27-mars-19
INSERT INTO auth_item(name, type) VALUES('portal-menu','2');

INSERT INTO auth_item(name, type) VALUES('portal-save_publish','2');
INSERT INTO auth_item(name, type) VALUES('portal-delete','2');



-- -------------------------------------------------------------------------------------------------
INSERT INTO auth_item(name, type) VALUES('fi-grades-transcriptnotes','2');

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'transcript_note_text', 'Texte pour relevé de notes', 'A l\'approche par compétence la réussite à un module est sanctionnée par Succès ou Echec', 'Texte pour relevé de notes', '', '', '', '', NULL, NULL);



ALTER TABLE `rooms` ADD `shift` INT(11) NOT NULL AFTER `short_room_name`, ADD INDEX `Shift` (`shift`);

ALTER TABLE `rooms` ADD  CONSTRAINT `fk_rooms_shift` FOREIGN KEY (`shift`) REFERENCES `shifts`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



INSERT INTO auth_item(name, type) VALUES('fi-grades-setreprise','2');

--
-- Structure de la table `reprise_grades`
--

CREATE TABLE `reprise_grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `grade_value_1` float DEFAULT NULL,
  `grade_value_2` float DEFAULT NULL,
  `grade_value_3` float DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `reprise_grades`
--
ALTER TABLE `reprise_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `course` (`course`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `reprise_grades`
--
ALTER TABLE `reprise_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `reprise_grades`
--
ALTER TABLE `reprise_grades`
  ADD CONSTRAINT `fk_reprise_grades_courses` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reprise_grades_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

-- ------------------------------------------------------------------------------------









INSERT INTO auth_item(name, type) VALUES('rbac-user-usernamelist','2');

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'main_shift_color', 'Couleur principale des ID carte Jour,Median,Soir,Staff', '#228b22,#DA4543,#38569C,#D95D10', '', '', NULL, NULL, NULL, NULL, NULL);

INSERT INTO auth_item(name, type) VALUES('idcards-idcard-print','2');

INSERT INTO `user`(`person_id`, `is_parent`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`) VALUES ( NULL, NULL, 'logipam', 'RPLSQG9Owj-c3x42vbT1t6adwj0AUBYO', '$2y$13$WVbF31qnCr/f15mzCV1p/u.1PdLh1Lmam1yUKa2aDV1lbM9tLeYsa', NULL, 'admin@logipam.com', 10);
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-create','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-index','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-deletecard','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-uploadphotos','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-listephotos','2');







INSERT INTO auth_item(name, type) VALUES('billings-billingsservicesitems-view','2');

INSERT INTO auth_item(name, type) VALUES('billings-feeservices-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-feeservices-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-feeservices-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-feeservices-index','2');


INSERT INTO auth_item(name, type) VALUES('billings-billingsservices-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-billingsservices-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-billingsservices-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-billingsservices-index','2');
INSERT INTO auth_item(name, type) VALUES('billings-billingsservices-view','2');


-- Table structure for table `fee_services`
CREATE TABLE `fee_services` (
`id` int(11) NOT NULL,
  `service_description` int(11) NOT NULL,
  `price` double NOT NULL,
  `devise` int(11) NOT NULL,
  `old_new` tinyint(2) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for table `fee_services`
ALTER TABLE `fee_services`
 ADD PRIMARY KEY (`id`), ADD KEY `service_id` (`service_description`), ADD KEY `devise` (`devise`);

-- AUTO_INCREMENT for table `fee_services`
ALTER TABLE `fee_services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Constraints for table `fee_services`
ALTER TABLE `fee_services`
ADD CONSTRAINT `fk_fee_services_devise` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_fee_services_other_income_description` FOREIGN KEY (`service_description`) REFERENCES `other_incomes_description` (`id`) ON UPDATE CASCADE;



INSERT INTO `label_category_for_billing` (`id`, `category`, `income_expense`) VALUES (NULL, 'Services', 'ri');

-- Table structure for table `billings_services`
CREATE TABLE `billings_services` (
`id` int(11) NOT NULL,
  `student_id` int(11) DEFAULT NULL,
  `fee` int(11) NOT NULL,
  `price` double NOT NULL,
  `request_date` datetime NOT NULL,
  `is_delivered` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: not yet delivered; 1: delivered',
  `delivered_date` datetime DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- Indexes for table `billings_services`
ALTER TABLE `billings_services`
 ADD PRIMARY KEY (`id`), ADD KEY `created_by` (`created_by`), ADD KEY `updated_by` (`updated_by`), ADD KEY `student_id` (`student_id`), ADD KEY `fee` (`fee`);

-- AUTO_INCREMENT for table `billings_services`
ALTER TABLE `billings_services`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

-- Constraints for table `billings_services`
ALTER TABLE `billings_services`
ADD CONSTRAINT `fk_billings_services_createdBy_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_billings_services_fee_services` FOREIGN KEY (`fee`) REFERENCES `fee_services` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_billings_services_studentId_persons` FOREIGN KEY (`student_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_billings_services_updatedBy_user` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE;





-- Table structure for table `billings_services_items`
CREATE TABLE `billings_services_items` (
  `billings_services_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `promotion` varchar(20) NOT NULL,
  `option_program` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for table `billings_services_items`
ALTER TABLE `billings_services_items`
 ADD PRIMARY KEY (`billings_services_id`);

-- Constraints for table `billings_services_items`
ALTER TABLE `billings_services_items`
ADD CONSTRAINT `pk_fk_billings_services_items_billings_services` FOREIGN KEY (`billings_services_id`) REFERENCES `billings_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `postulant` CHANGE `status` `status` INT(11) NULL;

# Grades for entrance examination 
CREATE TABLE `postulant_grades` ( `id` INT NOT NULL AUTO_INCREMENT , `postulant` INT NOT NULL , `program` INT NOT NULL , `academic_year` INT NOT NULL , `grade_value` FLOAT NOT NULL , `date_create` DATETIME NULL , `create_by` VARCHAR(64) NULL , `date_update` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`postulant`), INDEX (`program`), INDEX (`academic_year`)) ENGINE = InnoDB; 

ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`postulant`) REFERENCES `postulant`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `postulant_grades` ADD FOREIGN KEY (`program`) REFERENCES `program`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;







ALTER TABLE `payroll` CHANGE `date_created` `date_created` DATETIME NOT NULL, CHANGE `date_updated` `date_updated` DATETIME NOT NULL;

-- Table structure for table `payroll_plusvalue`
CREATE TABLE `payroll_plusvalue` (
`id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` double NOT NULL,
  `devise` int(11) NOT NULL,
  `acad` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for table `payroll_plusvalue`
ALTER TABLE `payroll_plusvalue`
 ADD PRIMARY KEY (`id`), ADD KEY `plusvalue_person` (`person_id`), ADD KEY `plusvalue_acad` (`acad`), ADD KEY `devise` (`devise`);

-- AUTO_INCREMENT for table `payroll_plusvalue`
ALTER TABLE `payroll_plusvalue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- Constraints for table `payroll_plusvalue`
ALTER TABLE `payroll_plusvalue`
ADD CONSTRAINT `fk_payroll_plusvalue_academic` FOREIGN KEY (`acad`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_payroll_plusvalue_devise` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_payroll_plusvalue_persons` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;



INSERT INTO auth_item(name, type) VALUES('billings-payrollplusvalue-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollplusvalue-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollplusvalue-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollplusvalue-index','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollplusvalue-view','2');







ALTER TABLE `courses` DROP FOREIGN KEY `fk_course_room`;

ALTER TABLE `student_level` ADD `room` INT(11) NOT NULL AFTER `level`, ADD INDEX (`room`) ;

-- run it when as soon as everyone is in a group
-- ALTER TABLE `student_level`
-- ADD CONSTRAINT `fk_student_level_rooms` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


INSERT INTO auth_item(name, type) VALUES('fi-grades-pdfgrade','2');




ALTER TABLE `payroll` ADD `loan_rate` DOUBLE NULL DEFAULT '0' AFTER `plus_value`;


-- Table structure for table `loan_elements`
CREATE TABLE `loan_elements` (
`id` int(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `amount` double NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for table `loan_elements`
ALTER TABLE `loan_elements`
 ADD PRIMARY KEY (`id`), ADD KEY `loan_id` (`loan_id`);
-- AUTO_INCREMENT for table `loan_elements`
ALTER TABLE `loan_elements`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- Constraints for table `loan_elements`
ALTER TABLE `loan_elements`
ADD CONSTRAINT `fk_loan_elements_loan_of_money` FOREIGN KEY (`loan_id`) REFERENCES `loan_of_money` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'loan_payment_method', 'Methode de paiement des prets', '2', '1: montant a payer automatique + mois debutant le remboursement; 2: montant a payer non automatique + mois de remboursement selectif', '', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL);


ALTER TABLE `payroll` ADD `gross_salary` DOUBLE NOT NULL AFTER `taxe`;

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'payroll_setting_twice', 'Nombre de configuration possible pour une personne', '1', '1: une seule configuration(employe); 2: deux configuration(employe, professeur)', '', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL);

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'programs_leaders', 'Nom du poste des responsables de programme', 'Responsable Programme', 'Nom du poste des responsables de programme', '', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL);




-- Table structure for table `programs_leaders`
CREATE TABLE `programs_leaders` (
`id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `old_new` tinyint(2) NOT NULL DEFAULT '1',
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Indexes for table `programs_leaders`
ALTER TABLE `programs_leaders`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_programs_leaders_persons` (`person_id`), ADD KEY `fk_programs_leaders_program` (`program_id`);

-- AUTO_INCREMENT for table `programs_leaders`
ALTER TABLE `programs_leaders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Constraints for table `programs_leaders`
ALTER TABLE `programs_leaders`
ADD CONSTRAINT `fk_programs_leaders_persons` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_programs_leaders_program` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON UPDATE CASCADE;






INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'course_room', 'Cours & Salles', '0', '1: Permettre le choix des cours dans une seule salle; 0: Permettre le choix des cours dans plusieurs salle', '', 'acad', '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL);


INSERT INTO auth_item(name, type) VALUES('fi-studentlevel-viewonlineusers','2');


ALTER TABLE `user` ADD `last_ip` VARCHAR(225) NULL DEFAULT NULL COMMENT 'add by logipam for session' AFTER `status`, ADD `last_activity` DATETIME NULL DEFAULT NULL COMMENT 'add by logipam for session' AFTER `last_ip`;

ALTER TABLE `grades` ADD `date_validation` DATETIME NULL DEFAULT NULL AFTER `date_updated`, ADD `validated_by` VARCHAR(100) NULL DEFAULT NULL AFTER `date_validation`;

INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES ( 'display_course_weight', 'Afficher poids cours', '0', '0: ne pas afficher le poids du cours lors de la creation; 1: afficher le poids du cours lors de la creation', '0: do not display course weight when creating course; 1: display course weight when creating course', 'sys', NULL, NULL, NULL, NULL);




ALTER TABLE `stockroom_restock` ADD `quantity_before` INT(11) NULL DEFAULT NULL AFTER `stockroom_product_id`;

ALTER TABLE `stockroom_restock` ADD `quantity_after` INT(11) NULL DEFAULT NULL AFTER `quantity_restock`;


ALTER TABLE `stockroom_products_label` ADD `category` TINYINT NULL DEFAULT NULL AFTER `product_description`;

ALTER TABLE `stockroom_inventory` CHANGE `is_bad_quantity_removed` `quantity_bad_removed` INT(11) NOT NULL DEFAULT '0';

ALTER TABLE `stockroom_raw_material` CHANGE `weight` `nature` VARCHAR(100) NOT NULL;

ALTER TABLE `stockroom_raw_material` CHANGE `price` `price` DOUBLE NULL DEFAULT NULL;

-- Table structure for table `stockroom_restock`
CREATE TABLE `stockroom_restock` (
`id` int(11) NOT NULL,
  `stockroom_product_id` int(11) NOT NULL,
  `quantity_restock` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `restock_date` date NOT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- Indexes for table `stockroom_restock`
ALTER TABLE `stockroom_restock`
 ADD PRIMARY KEY (`id`), ADD KEY `stockroom_product_id` (`stockroom_product_id`), ADD KEY `created_by` (`created_by`), ADD KEY `updated_by` (`updated_by`);
-- AUTO_INCREMENT for table `stockroom_restock`
ALTER TABLE `stockroom_restock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- Constraints for table `stockroom_restock`
ALTER TABLE `stockroom_restock`
ADD CONSTRAINT `fk_stockroom_restock_updatedby_user` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_restock_createdby_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_restock_stockroom_has_product` FOREIGN KEY (`stockroom_product_id`) REFERENCES `stockroom_has_products` (`id`) ON UPDATE CASCADE;

-- stockroomrestock
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-delete','2');

ALTER TABLE `stockroom_inventory` CHANGE `quantity_new` `quantity_lost` INT(11) NOT NULL DEFAULT '0';


ALTER TABLE `stockroom_inventory` CHANGE `quantity_good` `quantity_good` INT(11) NOT NULL DEFAULT '0', CHANGE `quantity_bad` `quantity_bad` INT(11) NOT NULL DEFAULT '0', CHANGE `opened_quantity` `opened_quantity` INT(11) NOT NULL DEFAULT '0', CHANGE `closed_quantity` `closed_quantity` INT(11) NOT NULL DEFAULT '0', CHANGE `created_by` `created_by` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `updated_by` `updated_by` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `date_created` `date_created` DATE NULL, CHANGE `date_updated` `date_updated` DATE NULL, CHANGE `comment` `comment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `stockroom_inventory` CHANGE `created_by` `created_by` INT(11) NULL, CHANGE `updated_by` `updated_by` INT(11) NULL;

ALTER TABLE `stockroom_inventory` ADD CONSTRAINT `fk_stockroom_inventory_user_created_by` FOREIGN KEY (`created_by`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `stockroom_inventory` ADD CONSTRAINT `fk_stockroom_inventory_user_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `stockroom_stock_usage` ADD `comment` VARCHAR(255) NULL ;

ALTER TABLE `stockroom_stock_usage` CHANGE `quantity_recieved` `quantity_recieved` INT(11) NULL, CHANGE `recieved_by` `recieved_by` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `taken_date` `taken_date` DATE NULL, CHANGE `return_date` `return_date` DATE NULL;

ALTER TABLE `stockroom_raw_material` CHANGE `weight` `weight` DOUBLE NULL, CHANGE `longueur` `longueur` DOUBLE NULL, CHANGE `square` `surface` DOUBLE NULL, CHANGE `volume` `volume` DOUBLE NULL, CHANGE `note` `note` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;



INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES ( 'stockroom_automaticCode', 'Codes Automatiques (Magasin)', '1', '0: on va saisir les codes; 1: les codes sont automatiques', '0: users enter the codes; 1: codes are automatic', 'sys', NULL, NULL, NULL, NULL);


INSERT INTO auth_item(name, type) VALUES('Magasinier','1');
INSERT INTO auth_item(name, type) VALUES('Magasinier manager','1');


ALTER TABLE `titles` CHANGE `title_name` `title_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES ( 'bonis_set', 'Methode de calcul du bonis', '1', '0: ne pas tenir compte des payrolls sur les 12 mois de l''annee; 1: tenir compte des payrolls sur les 12 mois de l''annee.', '0: do not care about payrolls for the year; 1: care about payrolls for the year.', 'sys', NULL, NULL, NULL, NULL);

INSERT INTO `taxes` (`id`, `taxe_description`, `employeur_employe`, `taxe_value`, `particulier`, `academic_year`) VALUES (8, 'BONIS', 0, 0, 0, 1);





ALTER TABLE `rooms` DROP `level`, DROP `shift`;
-- ALTER TABLE `rooms` ADD `level` INT(11) NOT NULL AFTER `short_room_name`, ADD `shift` INT(11) NULL DEFAULT NULL AFTER `level`, ADD INDEX ( `shift`);

-- ALTER TABLE `rooms` ADD CONSTRAINT `fk_rooms_shifts` FOREIGN KEY (`shift`) REFERENCES `shifts`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `student_other_info` ADD `apply_shift` INT(11) NULL AFTER `apply_for_program`;
ALTER TABLE `student_other_info` ADD CONSTRAINT `fk_stud_other_info_apply_shift` FOREIGN KEY (`apply_shift`) REFERENCES `shifts`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



INSERT INTO auth_item(name, type) VALUES('rbac-user-passwordreset','2');

INSERT INTO auth_item(name, type) VALUES('Administration recettes','1');

INSERT INTO auth_item(name, type) VALUES('Administration dépenses','1');

INSERT INTO auth_item(name, type) VALUES('Administration études','1');

INSERT INTO auth_item(name, type) VALUES('billings-payroll-print','2');

ALTER TABLE `payroll` CHANGE `cash_check` `cash_check` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `payroll` DROP `frais`;

ALTER TABLE `payroll_settings` CHANGE `assurance_name` `frais` DOUBLE NULL DEFAULT '0';


-- ALTER TABLE `payroll` ADD `frais` DOUBLE NOT NULL DEFAULT '0' AFTER `plus_value`;

ALTER TABLE `payroll` ADD `plus_value` DOUBLE NULL DEFAULT '0' AFTER `net_salary`;

ALTER TABLE `payroll_settings` ADD `assurance_value` DOUBLE NULL DEFAULT NULL AFTER `assurance_name`;

INSERT INTO auth_item(name, type) VALUES('fi-studentlevel-create','2');
INSERT INTO auth_item(name, type) VALUES('fi-studentlevel-update','2');
INSERT INTO auth_item(name, type) VALUES('fi-studentlevel-delete','2');
INSERT INTO auth_item(name, type) VALUES('fi-studentlevel-index','2');


ALTER TABLE `fees` ADD `level` INT(5) NOT NULL AFTER `academic_period`;


INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'max_level_cycle', 'Niveau maximal pour boucle un cycle', '2', 'Niveau maximal pour boucle un cycle d''etudes.', 'Maximun level to complete... ', 'sys', NULL, NULL, NULL, NULL);


CREATE TABLE `student_level` (
  `student_id` int(11) NOT NULL,
  `level` int(5) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: uncompleted; 1: complited'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
ALTER TABLE `student_level`
 ADD PRIMARY KEY (`student_id`);

--
ALTER TABLE `student_level`
ADD CONSTRAINT `fk_student_level_persons` FOREIGN KEY (`student_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;







ALTER TABLE `other_incomes` ADD `devise` INT(11) NOT NULL AFTER `amount`, ADD INDEX (`devise`) ;
ALTER TABLE `other_incomes` ADD CONSTRAINT `fk_other_incomes_devise` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `other_incomes` ADD CONSTRAINT `fk_other_income_other_income_description` FOREIGN KEY (`id_income_description`) REFERENCES `other_incomes_description`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO auth_item(name, type) VALUES('Administrateur systeme','1');

INSERT INTO auth_item(name, type) VALUES('billings-otherincomedescription-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincomedescription-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincomedescription-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincomedescription-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincomedescription-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-otherincome-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincome-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincome-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincome-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-otherincome-index','2');



INSERT INTO `label_category_for_billing` (`id`, `category`, `income_expense`) VALUES
(1, 'Donations and grants', 'ri'),
(2, 'Other income', 'ri'),
(3, 'Rent expenses', 'di'),
(4, 'Amenities and services', 'di'),
(5, 'Staff', 'di'),
(6, 'Tax', 'di'),
(7, 'Other expenses', 'di');


INSERT INTO auth_item(name, type) VALUES('billings-chargedescription-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargedescription-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargedescription-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargedescription-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargedescription-index','2');

ALTER TABLE `charge_paid` ADD `devise` INT(11) NOT NULL AFTER `amount`, ADD INDEX (`devise`) ;
ALTER TABLE `charge_paid` ADD CONSTRAINT `fk_charge_devise` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO auth_item(name, type) VALUES('billings-chargepaid-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargepaid-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargepaid-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargepaid-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-chargepaid-index','2');






INSERT INTO auth_item(name, type) VALUES('billings-taxes-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-taxes-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-taxes-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-taxes-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-taxes-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-payroll_settings-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll_settings-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll_settings-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll_settings-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-masscreate','2');
INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-index_exempt','2');
INSERT INTO auth_item(name, type) VALUES('billings-scholarshipholder-exempt','2');

INSERT INTO auth_item(name, type) VALUES('billings-loanofmoney-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-loanofmoney-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-loanofmoney-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-loanofmoney-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-loanofmoney-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-payroll-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-index','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-receipt','2');

INSERT INTO auth_item(name, type) VALUES('billings-billings-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-billings-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-billings-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-billings-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-billings-index','2');
INSERT INTO auth_item(name, type) VALUES('billings-payroll-print','2');

INSERT INTO auth_item(name, type) VALUES('billings-payrollsettings-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollsettings-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollsettings-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollsettings-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-payrollsettings-index','2');

INSERT INTO auth_item(name, type) VALUES('billings-partners-create','2');
INSERT INTO auth_item(name, type) VALUES('billings-partners-update','2');
INSERT INTO auth_item(name, type) VALUES('billings-partners-delete','2');
INSERT INTO auth_item(name, type) VALUES('billings-partners-view','2');
INSERT INTO auth_item(name, type) VALUES('billings-partners-index','2');

INSERT INTO auth_item(name, type) VALUES('rbac-user-usersdisabled','2');





INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
( 'max_amount_loan', 'Montant maximum d''un prêt', '1000', 'Le montant maximum que l''on peut donner comme prêter a un employé.', 'Maximun loan can be granted to an employee. ', 'sys', NULL, NULL, NULL, NULL);

ALTER TABLE `loan_of_money` ADD `devise` INT(11) NOT NULL AFTER `amount`, ADD INDEX (`devise`) ;
ALTER TABLE `loan_of_money` ADD CONSTRAINT `fk_loan_devise` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


INSERT INTO auth_item(name, type) VALUES('Planning-events-movelessonchoose','2');

ALTER TABLE `payment_method` ADD `is_default` TINYINT NOT NULL DEFAULT '0' COMMENT '0: it is not the default method; 1: it is the default method; ' AFTER `description`;

ALTER TABLE `program` ADD `short_name` VARCHAR(100) NULL DEFAULT NULL AFTER `label`;






ALTER TABLE `reservation` ADD `devise` INT(11) NOT NULL AFTER `amount`, ADD INDEX (`devise`) ;
ALTER TABLE `reservation` ADD CONSTRAINT `fk_reservation_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `pending_balance` ADD `devise` INT(11) NOT NULL AFTER `balance`, ADD INDEX (`devise`) ;
ALTER TABLE `pending_balance` ADD CONSTRAINT `fk_pending_balance_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `balance` ADD `devise` INT(11) NOT NULL AFTER `balance`, ADD INDEX (`devise`) ;
ALTER TABLE `balance` ADD CONSTRAINT `fk_balance_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



ALTER TABLE `loan_of_money` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `mails` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `menfp_decision` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `menfp_grades` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `other_incomes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `other_incomes_description` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `passing_grades` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT; 
ALTER TABLE `payroll` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payroll_setting_taxes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pending_balance` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `person_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `postulant` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `products` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `qualifications` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `raise_salary` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `record_infraction` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;
ALTER TABLE `record_presence` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `relations` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `return_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `room_has_person` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT; 
ALTER TABLE `rpt_custom` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sale_transaction` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `scalendar` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sections` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sellings` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `stocks` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `stock_history` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `student_documents` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payroll_settings` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payroll_settings` CHANGE `as` `as_` INT(2) NULL DEFAULT '0' COMMENT '0: employee; 1: teacher';


ALTER TABLE `taxes` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
('total_payroll', 'Nombre de payroll par année', '12', 'Nombre de payroll pour l\'annee', 'Number of payroll per year', 'econ', NULL, '2016-05-31 00:00:00', 'SIGES', NULL);

ALTER TABLE `payroll_settings` ADD `devise` INT(11) NULL AFTER `amount`, ADD INDEX (`devise`) ;

ALTER TABLE `payroll_settings` ADD CONSTRAINT `fk_payroll_settings_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



ALTER TABLE `partners` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `scholarship_holder` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_devises` FOREIGN KEY (`devise`) REFERENCES `devises`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE TABLE `reservation` (
`id` int(11) NOT NULL,
  `postulant_student` int(11) NOT NULL,
  `is_student` tinyint(2) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `already_checked` tinyint(2) NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_reservation_payment_method` (`payment_method`), ADD KEY `fk_reservation_academic_year` (`academic_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
ADD CONSTRAINT `fk_reservation_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `fk_reservation_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE;


ALTER TABLE `billings` ADD `reservation_id` INT(11) NULL AFTER `balance`, ADD INDEX (`reservation_id`) ;
ALTER TABLE `billings` ADD CONSTRAINT `fk_biiling_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `scholarship_holder` ADD CONSTRAINT `fk_scholarship_holder_person` FOREIGN KEY (`student`) REFERENCES `canado`.`persons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `scholarship_holder` ADD CONSTRAINT `fk_scholarship_holder_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `payment_method` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;




ALTER TABLE `fees` CHANGE `level` `program` INT(11) NOT NULL; 

ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_program` FOREIGN KEY (`program`) REFERENCES `program`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_academicperiods` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `fees` ADD CONSTRAINT `fk_fees_feeslabel` FOREIGN KEY (`fee`) REFERENCES `fees_label`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_persons` FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_fees` FOREIGN KEY (`fee_period`) REFERENCES `fees`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE; ALTER TABLE `billings` ADD CONSTRAINT `fk_billings_paymentmethod` FOREIGN KEY (`payment_method`) REFERENCES `payment_method`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

INSERT INTO `general_config` ( `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
('currency_name_symbol', 'Nom et symbole monétaire', 'Gourde/HTG', 'Le nom et le symbol de la devise utilisée par l''école', 'Currency name and symbol used', 'econ', NULL, '2016-05-31 00:00:00', 'SIGES', NULL);



ALTER TABLE `user` ADD `is_parent` INT(11) NULL DEFAULT NULL AFTER `person_id`;

ALTER TABLE `user` CHANGE `email` `email` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `user` ADD `person_id` INT(11) NULL AFTER `id`, ADD INDEX (`person_id`) ;
ALTER TABLE `user` ADD CONSTRAINT `fk_user_persons` FOREIGN KEY (`person_id`) REFERENCES `persons`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

 INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
('administration_signature_text', 'Definir le texte sous la ligne de la signature de la direction', 'Directeur', 'Definir le texte sous la ligne de signature de la direction', 'Define the text which is under the administration signature line', 'acad', NULL, '2017-06-02 00:00:00', NULL, NULL);
 
 
 INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
('reportcard_format', 'Format bulletin', '1', '1: ;  2: ', '1: Basic ; 2: ', 'acad', NULL, '2017-06-02 00:00:00', NULL, NULL);

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES ('include_course_withoutGrade', 'Inclure les cours qui n\'ont pas de note', '0', '0: non ; 1: oui ', '0: no ; 1: yes', 'acad', NULL, '2017-06-02 00:00:00', NULL, NULL);

INSERT INTO `general_config` (`item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES ('average_base', 'Moyenne de base', '100', 'La base de la moyenne: 10 ou 100.', 'Base average: 10 or 100.', 'acad', NULL, '2017-06-02 00:00:00', NULL, NULL);



