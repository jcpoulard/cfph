/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Dec 13, 2018
 */

CREATE TABLE `cms_bank_img` ( `id` INT NOT NULL AUTO_INCREMENT , `image_name` VARCHAR(255) NOT NULL , `is_in_use` BOOLEAN NOT NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(255) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(255) NOT NULL , INDEX (`id`), INDEX (`image_name`)) ENGINE = InnoDB;
ALTER TABLE `cms_bank_img` CHANGE `update_by` `update_by` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

CREATE TABLE `cms_program_extra` ( `id` INT NOT NULL AUTO_INCREMENT , `program_id` INT NOT NULL , `image` VARCHAR(255) NULL , `description_programme` TEXT NULL , `pre_requis` TEXT NULL , `why` TEXT NULL , `is_publish` BOOLEAN NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(255) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(255) NULL , PRIMARY KEY (`id`), INDEX (`program_id`)) ENGINE = InnoDB;
ALTER TABLE `cms_program_extra` ADD FOREIGN KEY (`program_id`) REFERENCES `program`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `cms_menu` ( `id` INT NOT NULL AUTO_INCREMENT , `code` VARCHAR(12) NOT NULL , `nom_menu` VARCHAR(255) NOT NULL , `is_publish` BOOLEAN NOT NULL DEFAULT TRUE , PRIMARY KEY (`id`), UNIQUE (`code`), UNIQUE (`nom_menu`)) ENGINE = InnoDB;

--
-- Dumping data for table `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`) VALUES
(1, 'carrou', 'Carrousel', 1),
(2, 'prog', 'Programme', 1),
(3, 'news', 'Nouvelles', 1),
(4, 'event', 'Evenements', 1);

-- 
-- Pour la gestion des liens vers les ressources depuis le backend  
--
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'backend_url', 'Url administration site web', 'http://localhost/cfph', 'localhost/cfph en local ou https://siges.us/cfph en ligne ou tout autres adresses en ligne du server. ', 'localhost/cfph en local ou https://siges.us/cfph en ligne ou tout autres adresses en ligne du server. ', 'sys', '2019-01-16 00:00:00', NULL, 'LOGIPAM', NULL);

-- Table carrousel 
CREATE TABLE `cms_carrousel` ( `id` INT NOT NULL AUTO_INCREMENT , `titre_image` TEXT NOT NULL , `short_description` TEXT NOT NULL , `image` TEXT NOT NULL , `is_publish` BOOLEAN NOT NULL , `create_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- Table article (news) 
CREATE TABLE `cms_article` ( `id` INT NOT NULL AUTO_INCREMENT , `titre` TEXT NOT NULL , `featured_image` TEXT NOT NULL , `desctiption` LONGTEXT NOT NULL , `menu_link` VARCHAR(12) NULL , `is_publish` BOOLEAN NOT NULL , `publish_at` DATE NULL , `create_by` VARCHAR(64) NOT NULL , `create_date` DATETIME NOT NULL , `update_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `cms_article` CHANGE `desctiption` `description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- Mise a jour de la table cms_menu pour prendre en compte la possibilite d'avoir des menus dynamiques 
ALTER TABLE `cms_menu` ADD `is_system` BOOLEAN NOT NULL DEFAULT TRUE AFTER `is_publish`, ADD `create_by` VARCHAR(64) NULL AFTER `is_system`, ADD `create_date` DATETIME NULL AFTER `create_by`, ADD `update_by` VARCHAR(64) NULL AFTER `create_date`, ADD `update_date` DATETIME NULL AFTER `update_by`;
ALTER TABLE `cms_menu` ADD `can_have_article` BOOLEAN NULL AFTER `is_system`;
UPDATE `cms_menu` SET `can_have_article` = '1' WHERE `cms_menu`.`id` = 3;
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'menu', 'Menu', '1', '1', NULL, NULL, NULL, NULL, NULL);
ALTER TABLE `cms_menu` ADD `publish_until` DATETIME NULL AFTER `can_have_article`;
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'salon', 'Salon des Compétences', '1', '1', '1', '2019-06-30 08:00:00', NULL, NULL, NULL, NULL);
UPDATE `cms_menu` SET `nom_menu` = 'Article' WHERE `cms_menu`.`id` = 3;
ALTER TABLE `cms_carrousel` ADD `label_visible` BOOLEAN NULL AFTER `is_publish`;
ALTER TABLE `cms_carrousel` CHANGE `label_visible` `label_visible` TINYINT(1) NULL DEFAULT '1';
Update `cms_carrousel` SET label_visible = 1;

-- 22 janvier 2019 
UPDATE `cms_menu` SET `code` = 'art' WHERE `cms_menu`.`id` = 3;
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'news', 'Nouvelles', '1', '0', '1', NULL, 'LOGIPAM', '2019-01-22 00:00:00', NULL, NULL);
UPDATE `cms_menu` SET `can_have_article` = '0' WHERE `cms_menu`.`id` = 3;

-- 23 janvier 2019 
UPDATE `cms_menu` SET `is_publish` = '0' WHERE `cms_menu`.`id` = 5;
-- Creation table gallerie categorie
CREATE TABLE `cms_album_cat` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_gallerie` VARCHAR(255) NOT NULL , `description` TEXT NOT NULL , `is_publish` BOOLEAN NOT NULL , `create_date` DATETIME NOT NULL , `create_by` VARCHAR(64) NOT NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), UNIQUE (`nom_gallerie`)) ENGINE = InnoDB;
-- Creation de la table albmum 
CREATE TABLE `cms_image_album` ( `id` INT NOT NULL AUTO_INCREMENT , `album_id` INT NOT NULL , `image_name` VARCHAR(255) NOT NULL , `desctiption` TEXT NOT NULL , `image_title` VARCHAR(255) NOT NULL , `is_publish` BOOLEAN NOT NULL , `publish_at` DATETIME NULL , `create_date` DATETIME NOT NULL , `create_by` VARCHAR(64) NOT NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`album_id`), INDEX (`image_name`)) ENGINE = InnoDB;
ALTER TABLE `cms_image_album` ADD FOREIGN KEY (`album_id`) REFERENCES `cms_album_cat`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'gal', 'Gallerie photos', '1', '1', '0', NULL, 'LOGIPAM', '2019-01-23 00:00:00', NULL, NULL);
ALTER TABLE `cms_image_album` CHANGE `desctiption` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- 24 janvier 2019
ALTER TABLE `cms_album_cat` ADD `is_system` BOOLEAN NULL AFTER `description`;
INSERT INTO `cms_album_cat` (`id`, `nom_gallerie`, `description`, `is_system`, `is_publish`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Salon des compétences', 'Gallerie  photo dédié au salon des compétences', NULL, 1, '2019-01-24 09:01:54', 'LOGIPAM', NULL, NULL);
UPDATE `cms_album_cat` SET `is_system` = '1' WHERE `cms_album_cat`.`id` = 1;
ALTER TABLE `cms_image_album` ADD `label_visible` BOOLEAN NULL AFTER `publish_at`;

-- 28 janvier 2019 
CREATE TABLE `cms_events` ( `id` INT NOT NULL AUTO_INCREMENT , `titre_event` VARCHAR(255) NOT NULL , `date_start` DATE NOT NULL , `date_end` DATE NULL , `time_start` TIME NOT NULL , `time_end` TIME NOT NULL , `one_day_event` BOOLEAN NOT NULL , `location` VARCHAR(255) NOT NULL , `description` TEXT NOT NULL , `publish_at` DATETIME NULL , `is_publish` BOOLEAN NOT NULL , `create_date` DATETIME NOT NULL , `create_by` VARCHAR(64) NOT NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `cms_events` ADD `menu_link` VARCHAR(32) NULL AFTER `publish_at`;
UPDATE `cms_menu` SET `nom_menu` = 'Evénements' WHERE `cms_menu`.`id` = 4;
-- 30 janvier 2019 
ALTER TABLE `cms_events` ADD FOREIGN KEY (`menu_link`) REFERENCES `cms_menu`(`code`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- 2 fevrier 2019 
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'saen', 'Services aux entreprises', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-02 00:00:00', NULL, NULL);
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'ppro', 'Perfectionnement professionnel', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-02 00:00:00', NULL, NULL);
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'ipra', 'Infos Pratiques', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-02 00:00:00', NULL, NULL);
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'vetu', 'Vie étudiante', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-02 00:00:00', NULL, NULL);

-- 4 fevrier 2019 
UPDATE `cms_menu` SET `is_system` = '0' WHERE `cms_menu`.`id` = 6;
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'cextern', 'Coopération externe', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-04 00:00:00', NULL, NULL);
-- 4 fevrier 2019 3:06 PM
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'admission', 'Admission', '1', '0', '1', '2019-02-04 00:00:00', 'LOGIPAM', '2019-02-04 00:00:00', NULL, NULL);
INSERT INTO `cms_menu` (`id`, `code`, `nom_menu`, `is_publish`, `is_system`, `can_have_article`, `publish_until`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'about', 'A propos de CANADO', '1', '0', '1', NULL, 'LOGIPAM', '2019-02-04 00:00:00', NULL, NULL);

-- 5 fevrier 2019 
CREATE TABLE `cms_config` ( `id` INT NOT NULL AUTO_INCREMENT , `parameter_name` VARCHAR(255) NOT NULL , `parameter_label` VARCHAR(255) NOT NULL , `parameter_value` LONGTEXT NULL , `parameter_type` VARCHAR(255) NOT NULL , `parameter_warning` TEXT NULL , `create_by` VARCHAR(64) NOT NULL , `create_date` DATETIME NOT NULL , `update_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
INSERT INTO `cms_config` (`id`, `parameter_name`, `parameter_label`, `parameter_value`, `parameter_type`, `parameter_warning`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'is_salon_visible', 'Afficher salon des compétences', NULL, 'boolean', 'Afficher le salon des compétences entraine la disparition du menu A la une. Voulez vous vraiment afficher le menu Salon des compétences. ', 'LOGIPAM', '2019-02-05 00:00:00', NULL, NULL);
INSERT INTO `cms_config` (`id`, `parameter_name`, `parameter_label`, `parameter_value`, `parameter_type`, `parameter_warning`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'titre_salon', 'Titre du salon des compétences', NULL, 'textarea', NULL, 'LOGIPAM', '2019-02-05 00:00:00', NULL, NULL);
INSERT INTO `cms_config` (`id`, `parameter_name`, `parameter_label`, `parameter_value`, `parameter_type`, `parameter_warning`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'theme_salon', 'Thème du salon des compétences', NULL, 'textarea', NULL, 'LOGIPAM', '2019-02-05 00:00:00', NULL, NULL);

-- 8 fevrier 2019 
INSERT INTO `cms_config` (`id`, `parameter_name`, `parameter_label`, `parameter_value`, `parameter_type`, `parameter_warning`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'is_inscription_visible', "Afficher les informations d\'inscription sur le site web", NULL, 'boolean', 'Les paramètres d inscription seront affichés sur le site web. Voulez vous continuer ?', 'lOGIPAM', '2019-02-08 00:00:00', NULL, NULL);
INSERT INTO `cms_config` (`id`, `parameter_name`, `parameter_label`, `parameter_value`, `parameter_type`, `parameter_warning`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES (NULL, 'text_inscription', 'Texte information inscription', NULL, 'textarea', NULL, 'lOGIPAM', '2019-02-08 00:00:00', NULL, NULL);
UPDATE `cms_config` SET `parameter_label` = 'Afficher les informations pour inscription sur le site web ' WHERE `cms_config`.`id` = 4;
UPDATE `cms_config` SET `parameter_warning` = 'Les paramètres pour inscription seront affichés sur le site web. Voulez vous continuer ?' WHERE `cms_config`.`id` = 4;
ALTER TABLE `cms_config` ADD `boolean_warning` TEXT NULL AFTER `parameter_warning`;
UPDATE `cms_config` SET `parameter_warning` = 'Afficher le salon des compétences entraine la disparition du menu <<A la une>>. Voulez vous vraiment afficher le menu Salon des compétences? ' WHERE `cms_config`.`id` = 1;
UPDATE `cms_config` SET `boolean_warning` = 'Ne plus afficher le salon des compétences entraine la disparation du menu <<Salon des compétences>> et la restauration du menu <<A la une>>. Voulez vous continuer ?' WHERE `cms_config`.`id` = 1;
UPDATE `cms_config` SET `boolean_warning` = 'Voulez vous vraiment ne plus afficher les informations sur les inscriptions ?' WHERE `cms_config`.`id` = 4;

-- 29 mars 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_portal_relay', 'Email du Portail', 'jcpoulard@gmail.com', 'Eamil de relais pour la gestion de la communication email du portail', NULL, NULL, '2019-03-29 00:00:00', NULL, 'LOGIPAM', NULL);

-- 30 mars 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_host_relay', 'Hote de l\'email de relai', 'smtp.gmail.com', 'Hote de l\'email de relai', NULL, 'sys', '2019-03-30 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_password_relay', 'Mot de passe email relai', NULL, 'Mot de passe email relai', NULL, 'sys', '2019-03-30 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_port_relay', 'Port de l\'email de relai', '465', 'Port de l\'email de relai', NULL, 'sys', '2019-03-30 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_secret_key', 'Clé secret de l\'email', '37 v0u5, h0mm35 prec13ux gneraux1ntrp1de5qui 1n5en51bl35vos pr0pr35m4lheur54v3z r355u5c17l4lib3rt3nlui prod16u4n770u7vo7r3s4n6', 'Clé secret de l\'email. A ne pas changer sous aucun prétexte. ', NULL, 'sys', '2019-03-30 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_iv', 'IV pour l\'email', '32', 'IV pour l\'email. Ne pas modifier en aucun cas.', NULL, 'sys', '2019-03-30 00:00:00', NULL, 'LOGIPAM', NULL);

-- 06 avril 2019 debut inscription 
ALTER TABLE `postulant` ADD `email` VARCHAR(255) NOT NULL AFTER `no_inscription`, ADD UNIQUE `k_email_unik` (`email`);
ALTER TABLE `postulant` ADD `postulant_password` TEXT NOT NULL AFTER `email`;
ALTER TABLE `postulant` ADD `validation_code` VARCHAR(32) NOT NULL AFTER `postulant_password`, ADD UNIQUE (`validation_code`);
ALTER TABLE `postulant` ADD `is_validation_expire` INT NOT NULL AFTER `validation_code`;
ALTER TABLE `postulant` CHANGE `birthday` `birthday` DATE NULL;
ALTER TABLE `postulant` CHANGE `adresse` `adresse` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `postulant` CHANGE `phone` `phone` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `apply_for_program` `apply_for_program` INT(11) NULL, CHANGE `previous_school` `previous_school` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `school_date_entry` `school_date_entry` DATE NULL, CHANGE `academic_year` `academic_year` INT(11) NULL;
ALTER TABLE `postulant` CHANGE `create_by` `create_by` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

-- 12 avril 2019 
ALTER TABLE `postulant` ADD `program_second` INT NULL AFTER `apply_for_program`;
ALTER TABLE `postulant_liable_person` ADD `email_responsable` VARCHAR(255) NULL AFTER `occupation_phone`, ADD `relation_responsable` INT NULL AFTER `email_responsable`;

-- 13 avril 2018 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'frais_inscription', 'Frais d\'inscription', '1000', 'Frais d\'inscription pour les postulants', NULL, NULL, '2019-04-13 00:00:00', NULL, 'LOGIPAM', NULL);

-- 24 avril 2019 
UPDATE `general_config` SET `item_value` = '2000' WHERE `general_config`.`id` = 46;

-- 14 mai 2019 
CREATE TABLE `system_log` ( `id` INT NOT NULL AUTO_INCREMENT , `nom_du_log` TEXT NOT NULL , `valeur_du_log` TEXT NOT NULL , `date_creation` DATETIME NOT NULL , `creer_par` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

