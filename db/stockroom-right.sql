


-- local
INSERT INTO auth_item(name, type) VALUES('stockrooms-local-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-local-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-local-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-local-delete','2');

-- marque
INSERT INTO auth_item(name, type) VALUES('stockrooms-marque-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-marque-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-marque-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-marque-delete','2');

-- stockroomproductslabel
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomproductslabel-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomproductslabel-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomproductslabel-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomproductslabel-delete','2');

-- stockroomequipment
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomequipment-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomequipment-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomequipment-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomequipment-delete','2');

-- stockroomrawmaterial
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrawmaterial-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrawmaterial-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrawmaterial-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrawmaterial-delete','2');

-- stockroomfurniture
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomfurniture-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomfurniture-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomfurniture-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomfurniture-delete','2');


-- stockroomhasproducts
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-delete','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-restock','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomhasproducts-dashboard','2');

-- stockroomstockusage
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomstockusage-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomstockusage-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomstockusage-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomstockusage-view','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomstockusage-delete','2');

-- stockroominventory
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroominventory-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroominventory-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroominventory-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroominventory-view','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroominventory-dashboard','2');


-- stockroomrestock
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-index','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-create','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-update','2');
INSERT INTO auth_item(name, type) VALUES('stockrooms-stockroomrestock-delete','2');

