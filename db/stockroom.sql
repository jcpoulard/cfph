-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 15, 2018 at 07:07 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `canado_blank`
--

-- --------------------------------------------------------

--
-- Table structure for table `local`
--

CREATE TABLE `local` (
`id` int(11) NOT NULL,
  `local_label` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `marque`
--

CREATE TABLE `marque` (
`id` int(11) NOT NULL,
  `marque_label` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom`
--

CREATE TABLE `stockroom` (
`id` int(11) NOT NULL,
  `stockroom_code` varchar(100) NOT NULL,
  `stockroom_name` varchar(255) NOT NULL,
  `stockroom_local` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_equipment`
--

CREATE TABLE `stockroom_equipment` (
`id` int(11) NOT NULL,
  `code` varchar(200) NOT NULL COMMENT 'automatic',
  `product_label` int(11) NOT NULL,
  `marque` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_furniture`
--

CREATE TABLE `stockroom_furniture` (
`id` int(11) NOT NULL,
  `code` varchar(200) NOT NULL COMMENT 'automatic',
  `product_label` int(11) NOT NULL,
  `marque` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `serial` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_has_products`
--

CREATE TABLE `stockroom_has_products` (
`id` int(11) NOT NULL,
  `stockroom_id` int(11) NOT NULL,
  `stockroom_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `quantity_alert` int(11) NOT NULL,
  `state` tinyint(4) DEFAULT NULL COMMENT '0: bad; 1: good; 2: damaged',
  `status` tinyint(4) DEFAULT NULL COMMENT '0: removed; 1: in use; 2: in repair',
  `is_equipment` tinyint(2) NOT NULL,
  `things_out` tinyint(4) NOT NULL DEFAULT '0',
  `localisation` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_inventory`
--

CREATE TABLE `stockroom_inventory` (
`id` int(11) NOT NULL,
  `stockroom_product_id` int(11) NOT NULL,
  `quantity_lost` int(11) NOT NULL DEFAULT '0',
  `quantity_good` int(11) NOT NULL DEFAULT '0',
  `quantity_bad` int(11) NOT NULL DEFAULT '0',
  `opened_quantity` int(11) NOT NULL DEFAULT '0',
  `closed_quantity` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `inventory_date` date NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `quantity_bad_removed` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stockroom_products_label`
--

CREATE TABLE `stockroom_products_label` (
`id` int(11) NOT NULL,
  `product_description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_raw_material`
--

CREATE TABLE `stockroom_raw_material` (
`id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL COMMENT 'automatic',
  `product_label` int(11) NOT NULL,
  `nature` varchar(100) NOT NULL,
  `longueur` double DEFAULT NULL,
  `surface` double DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `note` text,
  `price` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `stockroom_restock`
--

CREATE TABLE `stockroom_restock` (
`id` int(11) NOT NULL,
  `stockroom_product_id` int(11) NOT NULL,
  `quantity_restock` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `restock_date` date NOT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stockroom_stock_usage`
--

CREATE TABLE `stockroom_stock_usage` (
`id` int(11) NOT NULL,
  `stockroom_product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_user` int(11) NOT NULL,
  `stockkeeper` int(11) NOT NULL,
  `is_return` tinyint(4) NOT NULL,
  `quantity_recieved` int(11) DEFAULT NULL,
  `recieved_by` varchar(255) DEFAULT NULL,
  `taken_date` date DEFAULT NULL,
  `return_date` date DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Indexes for table `local`
--
ALTER TABLE `local`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marque`
--
ALTER TABLE `marque`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockroom`
--
ALTER TABLE `stockroom`
 ADD PRIMARY KEY (`id`), ADD KEY `stockroom_local` (`stockroom_local`);

--
-- Indexes for table `stockroom_equipment`
--
ALTER TABLE `stockroom_equipment`
 ADD PRIMARY KEY (`id`), ADD KEY `product_label` (`product_label`), ADD KEY `marque` (`marque`);

--
-- Indexes for table `stockroom_furniture`
--
ALTER TABLE `stockroom_furniture`
 ADD PRIMARY KEY (`id`), ADD KEY `product_label` (`product_label`), ADD KEY `marque` (`marque`);

--
-- Indexes for table `stockroom_has_products`
--
ALTER TABLE `stockroom_has_products`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `stockroom_id_2` (`stockroom_id`,`stockroom_product`,`is_equipment`), ADD KEY `stockroom_id` (`stockroom_id`);

--
-- Indexes for table `stockroom_inventory`
--
ALTER TABLE `stockroom_inventory`
 ADD PRIMARY KEY (`id`), ADD KEY `stockroom_product_id` (`stockroom_product_id`), ADD KEY `created_by` (`created_by`), ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `stockroom_products_label`
--
ALTER TABLE `stockroom_products_label`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `product_description` (`product_description`);

--
-- Indexes for table `stockroom_raw_material`
--
ALTER TABLE `stockroom_raw_material`
 ADD PRIMARY KEY (`id`), ADD KEY `product_label` (`product_label`);

--
-- Indexes for table `stockroom_restock`
--
ALTER TABLE `stockroom_restock`
 ADD PRIMARY KEY (`id`), ADD KEY `stockroom_product_id` (`stockroom_product_id`), ADD KEY `created_by` (`created_by`), ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `stockroom_stock_usage`
--
ALTER TABLE `stockroom_stock_usage`
 ADD PRIMARY KEY (`id`), ADD KEY `product_user` (`product_user`), ADD KEY `stockkeeper` (`stockkeeper`), ADD KEY `stockroom_product_id` (`stockroom_product_id`);

--
-- AUTO_INCREMENT for table `local`
--
ALTER TABLE `local`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `marque`
--
ALTER TABLE `marque`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom`
--
ALTER TABLE `stockroom`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_equipment`
--
ALTER TABLE `stockroom_equipment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_furniture`
--
ALTER TABLE `stockroom_furniture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_has_products`
--
ALTER TABLE `stockroom_has_products`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_inventory`
--
ALTER TABLE `stockroom_inventory`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stockroom_products_label`
--
ALTER TABLE `stockroom_products_label`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_raw_material`
--
ALTER TABLE `stockroom_raw_material`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `stockroom_restock`
--
ALTER TABLE `stockroom_restock`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stockroom_stock_usage`
--
ALTER TABLE `stockroom_stock_usage`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `stockroom`
--
ALTER TABLE `stockroom`
ADD CONSTRAINT `fk_stockroom_local` FOREIGN KEY (`stockroom_local`) REFERENCES `local` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_equipment`
--
ALTER TABLE `stockroom_equipment`
ADD CONSTRAINT `fk_stockroom_equipment_marque` FOREIGN KEY (`marque`) REFERENCES `marque` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_equipment_stockroom_product_label` FOREIGN KEY (`product_label`) REFERENCES `stockroom_products_label` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_furniture`
--
ALTER TABLE `stockroom_furniture`
ADD CONSTRAINT `fk_stockroom_furniture_stockroom_marque` FOREIGN KEY (`marque`) REFERENCES `marque` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_furniture_stockroom_product_label` FOREIGN KEY (`product_label`) REFERENCES `stockroom_products_label` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_has_products`
--
ALTER TABLE `stockroom_has_products`
ADD CONSTRAINT `stockroom_has_product_stockroom` FOREIGN KEY (`stockroom_id`) REFERENCES `stockroom` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_inventory`
--
ALTER TABLE `stockroom_inventory`
ADD CONSTRAINT `fk_stockroom_inventory_stockroom_has_product` FOREIGN KEY (`stockroom_product_id`) REFERENCES `stockroom_has_products` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_inventory_user_created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_inventory_user_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_raw_material`
--
ALTER TABLE `stockroom_raw_material`
ADD CONSTRAINT `fk_stockroom_raw_material_product_label` FOREIGN KEY (`product_label`) REFERENCES `stockroom_products_label` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_restock`
--
ALTER TABLE `stockroom_restock`
ADD CONSTRAINT `fk_stockroom_restock_createdby_user` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_restock_stockroom_has_product` FOREIGN KEY (`stockroom_product_id`) REFERENCES `stockroom_has_products` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_restock_updatedby_user` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `stockroom_stock_usage`
--
ALTER TABLE `stockroom_stock_usage`
ADD CONSTRAINT `fk_stockroom_stock_usage_persons_productUser` FOREIGN KEY (`product_user`) REFERENCES `persons` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_stock_usage_persons_stockkeeper` FOREIGN KEY (`stockkeeper`) REFERENCES `persons` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_stockroom_stock_usage_stockroom_has_product` FOREIGN KEY (`stockroom_product_id`) REFERENCES `stockroom_has_products` (`id`) ON UPDATE CASCADE;

ALTER TABLE `stockroom_products_label` ADD `category` TINYINT NULL DEFAULT NULL AFTER `product_description`;
ALTER TABLE `stockroom_raw_material` CHANGE `nature` `nature` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `stockroom_restock` ADD `quantity_before` INT(11) NULL DEFAULT NULL AFTER `stockroom_product_id`;

ALTER TABLE `stockroom_restock` ADD `quantity_after` INT(11) NULL DEFAULT NULL AFTER `quantity_restock`;