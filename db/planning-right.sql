INSERT INTO auth_item(name, type) VALUES('planning-events-create','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-index','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-chrono','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-markascomplete','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-delete','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-update','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-list','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-coursesummary','2');
INSERT INTO auth_item(name, type) VALUES('planning-holidays-index','2');
INSERT INTO auth_item(name, type) VALUES('planning-holidays-view','2');
INSERT INTO auth_item(name, type) VALUES('planning-holidays-create','2');
INSERT INTO auth_item(name, type) VALUES('planning-holidays-update','2');
INSERT INTO auth_item(name, type) VALUES('planning-holidays-delete','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-movelesson','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-movelessonchoose','2');
-- Teacher attendance 
INSERT INTO auth_item(name, type) VALUES('planning-teacherattendance-attendancegrid','2');
INSERT INTO auth_item(name, type) VALUES('planning-teacherattendance-create','2');
INSERT INTO auth_item(name, type) VALUES('planning-teacherattendance-update','2');
INSERT INTO auth_item(name, type) VALUES('planning-teacherattendance-delete','2');
INSERT INTO auth_item(name, type) VALUES('planning-events-deleteevent','2');

-- Add report_to to know at what date a lesson was moving to 
ALTER TABLE `events` ADD `report_to` DATETIME NULL AFTER `initial_date_course_end`;

-- Student attendance planning-studentattendance-create
INSERT INTO auth_item(name, type) VALUES('planning-studentattendance-create','2');
INSERT INTO auth_item(name, type) VALUES('planning-studentattendance-attendancegrid','2');
INSERT INTO auth_item(name, type) VALUES('planning-studentattendance-update','2');
INSERT INTO auth_item(name, type) VALUES('planning-studentattendance-delete','2');



