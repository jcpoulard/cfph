/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Nov 22, 2017
 */

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `is_migrate` tinyint(1) NOT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `migrate_by` varchar(255) DEFAULT NULL,
  `type_migration` varchar(255) DEFAULT NULL,
  `date_upload` datetime DEFAULT NULL,
  `date_migrate` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `file_name` (`file_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migration`
--
ALTER TABLE `migration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `persons` CHANGE `nif_cin` `nif_cin` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `persons` CHANGE `citizenship` `citizenship` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `persons` CHANGE `comment` `comment` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
ALTER TABLE `persons` CHANGE `blood_group` `blood_group` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;