<?php

namespace app\modules\idcards\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\idcards\models\Idcard;
use app\modules\idcards\models\SrcIdcard;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcShifts;

use app\modules\fi\models\SrcRooms;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use kartik\mpdf\Pdf; // For PDF 

/**
 * IdcardController implements the CRUD actions for Idcard model.
 */
class IdcardController extends Controller
{
   
    public $layout = "/inspinia"; 
    
    public $printing_date = false;
    public $idShift=null;
    public $section_id=null;
    public $room_id=null;
    public $category = null;
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Idcard models.
     * @return mixed
     */
   public function actionIndex()
    {
        if(Yii::$app->user->can('idcards-idcard-index'))
         {
       $acad = Yii::$app->session['currentId_academic_year'];
       $this->printing_date = false;
       
       
       
       $main_shift_color = infoGeneralConfig('main_shift_color');
        $main_shift_color=explode(",",substr($main_shift_color,0));  //  0: jour,  1:  median,   2:  soir
        
        $color_shift='#446da3';  //default
            
       $filename = '';      
       $program= 0;
	        $shift= 0;
	        $room = 0;
	        $level = 0;
                 $single_print =1;
                 $this->category = null;
                
                $model=new SrcIdcard();
             $searchModel = new SrcIdcard();
                         
             $dataProvider = $searchModel->searchByLevelProgramShiftGroup(null,null,null,null,$acad);
		
		
	        
	if($model->load(Yii::$app->request->post()) ) 
	 {
	            // $dbTrans = Yii::app->db->beginTransaction();
	       
               if(isset($_POST['SrcIdcard']['category_']))
                    $this->category = $_POST['SrcIdcard']['category_'];
               
               $model->category_ = $this->category;

                //$this->category= 0: elev, 1: prof, 2:emp  

                if( ($this->category== 0) && ($this->category!=null) ) 
                {
            
	                    if(isset($_POST['SrcIdcard']['level']))
		              { $level= $_POST['SrcIdcard']['level'];
		              
		                 $model->level=$level; 
		                 
		                 if(Yii::$app->session['idcard_level']!=$level)
                                 {
                                     Yii::$app->session['idcard_level'] = $level;
                                     
                                     $program= 0;
                                     Yii::$app->session['idcard_program']= $program;
		                     $model->program=$program;
                                     
                                     $shift=0;
                                          Yii::$app->session['idcard_shift'] = $shift;
                                           $model->shift=$shift;
                                           
                                           $room = 0;
                                           Yii::$app->session['idcard_room'] = $room;
                                            $model->room=$room;
                                  }
                                elseif(isset($_POST['SrcIdcard']['program']))
                                   {  
                                      $program= $_POST['SrcIdcard']['program'];
                                      
                                      $model->program=$program;
                                  
                                      if(Yii::$app->session['idcard_program']!= $program)
                                       {
                                          Yii::$app->session['idcard_program'] = $program;
                                         
                                          $shift=0;
                                          Yii::$app->session['idcard_shift'] = $shift;
                                           $model->shift=$shift;
                                           
                                           $room = 0;
                                           Yii::$app->session['idcard_room'] = $room;
                                            $model->room=$room;
                                          
                                        }
                                      elseif(isset($_POST['SrcIdcard']['shift'])) 
                                        {
                                             $shift= $_POST['SrcIdcard']['shift'];
                                             
                                              $model->shift=$shift; 
                                              
                                             if(Yii::$app->session['idcard_shift']!=$shift)
                                              {
                                                Yii::$app->session['idcard_shift'] = $shift;
                                                 
                                               
                                                    $room = 0;
                                                    Yii::$app->session['idcard_room'] = $room;
                                                     $model->room=$room;
                                                     
                                              }
                                            elseif(isset($_POST['SrcIdcard']['room'])) 
                                              {  
                                            
                                                    $room = $_POST['SrcIdcard']['room'];
                                                    
                                                    $model->room=$room;
                                                    
                                                   if(Yii::$app->session['idcard_room']!=$room)
                                                    {
                                                        Yii::$app->session['idcard_room'] = $room;
                                                         $model->room=$room;
                                                    }
                                                    
                                              }
                                            
                                         }
                                      }
                                     
                                     
		                      }
		           
                        }
                        
                        
              if( ($this->category== 0) && ($this->category!=null) ) //elev
                  {

                     if( ($model->level==0)||($model->level==null) ) //tout elev
                      {
                         $dataProvider = $searchModel->searchByLevelProgramShiftGroup(null,null,null,null,$acad); 

                             $single_print =1;
                             $filename = strtr( Yii::t('app','students'), pa_daksan() );

                       }
                     else
                       {      
                           
                            if( (($model->level!=null)&&($model->level!=0))&&( ($model->program!=null)&&($model->program!=0))&&( ($model->shift!=null)&&($model->shift!=0) )&&( ($model->room!=null)&&($model->room!=0) ) )
                              {    $dataProvider = $searchModel->searchByLevelProgramShiftGroup($model->level,$model->program,$model->shift,$model->room,$acad);
                                 
                                   $prog = SrcProgram::findOne($model->program)->label;
                                   $level = $model->level;
                                   $shiftName = SrcShifts::findOne($model->shift)->shift_name;
                                   $room = SrcRooms::findOne($model->room)->room_name;
                                   
                                   if(strtr( $shiftName,pa_daksan() )=='Matin')
                                     $color_shift = $main_shift_color[0];
                                    elseif(strtr( $shiftName,pa_daksan() )=='Median')
                                        $color_shift = $main_shift_color[1];
                                        elseif(strtr( $shiftName,pa_daksan() )=='Soir')
                                            $color_shift = $main_shift_color[2];
                                   
                                  $single_print =0;
                                          
                              $filename = strtr( $prog, pa_daksan() ).'_'.$level.'_'.strtr( $shiftName, pa_daksan() ).'_'.strtr( $room, pa_daksan() );  // program-level-shift-room
                              }
                            elseif( (($model->level!=null)&&($model->level!=0))&&( ($model->program!=null)&&($model->program!=0))&&( ($model->shift!=null)&&($model->shift!=0) )&&( ($model->room==null)||($model->room==0) ) )
                               {    $dataProvider = $searchModel->searchByLevelProgramShiftGroup($model->level,$model->program,$model->shift,null,$acad);
                                  
                                   $prog = SrcProgram::findOne($model->program)->label;
                                   $level = $model->level;
                                   $shiftName = SrcShifts::findOne($model->shift)->shift_name;
                                   
                                    if(strtr( $shiftName,pa_daksan() )=='Matin')
                                     $color_shift = $main_shift_color[0];
                                    elseif(strtr( $shiftName,pa_daksan() )=='Median')
                                        $color_shift = $main_shift_color[1];
                                        elseif(strtr( $shiftName,pa_daksan() )=='Soir')
                                            $color_shift = $main_shift_color[2];
                                 
                                        $single_print =0;
                                        
                               $filename = strtr( $prog, pa_daksan() ).'_'.$level.'_'.strtr( $shiftName, pa_daksan() );  // program-level-shift
                              }
                            elseif( (($model->level!=null)&&($model->level!=0))&&( ($model->program!=null)&&($model->program!=0))&&( ($model->shift==null)||($model->shift==0) )&&( ($model->room==null)||($model->room==0) ) )
                               {   $dataProvider = $searchModel->searchByLevelProgramShiftGroup($model->level,$model->program,null,null,$acad);
                                 
                                    $prog = SrcProgram::findOne($model->program)->label;
                                   $level = $model->level;
                                   
                                   $single_print =1;
                                   
                                   
                               $filename = strtr( $prog, pa_daksan() ).'_'.$level;  // program-level-shift-room
                              }
                            elseif( (($model->level!=null)&&($model->level!=0))&&( ($model->program==null)||($model->program==0))&&( ($model->shift==null)||($model->shift==0) )&&( ($model->room==null)||($model->room==0) ) )
                               {    $dataProvider = $searchModel->searchByLevelProgramShiftGroup($model->level,null,null,null,$acad);
                                
                                   $level = $model->level;
                                   
                                   $single_print =1;
                                   
                              $filename = strtr( Yii::t('app','Level'), pa_daksan() ).'_'.$level;  // program-level-shift-room
                              }
                            //elseif( (($model->level==null)||($model->level==0))&&( ($model->program==null)||($model->program==0))&&( ($model->shift==null)||($model->shift==0) )&&( ($model->room==null)||($model->room==0) ) )
                                // { $dataProvider = $searchModel->searchByLevelProgramShiftGroup(null,null,null,null,$acad);
                                 //   $single_print =1;
                                // }
                               
                           }
                  }
               elseif( ($this->category== 1) ) //teacher
                 {
                       $dataProvider = $searchModel->searchForAllTeachers($acad);
                             $single_print =0;
                             $filename = strtr( Yii::t('app','teachers'), pa_daksan() );
                 }
                elseif( ($this->category== 2) ) //employee
                  {
                    $dataProvider = $searchModel->searchForAllEmployees($acad);
                    $single_print =0;
                    $filename = strtr( Yii::t('app','employees'), pa_daksan() );
                    
                            
                  }
                   
                           
                       
    if(isset($_POST['print']))
     {
       if(Yii::$app->user->can('idcards-idcard-print')) 
        {
                                
          if( ($this->category!=3) )
           {
             //gade si se yon elev pou pran vacation an
             if( ($this->category==0)&&($this->category!=null) )
               {
               
                   
                   $shift='';
                   

                   if(isset($modelPerson->studentOtherInfo0[0]->apply_shift))
                      $shift=$modelPerson->studentOtherInfo0[0]->applyShift->shift_name; 

                    
                   
         
                    
                 if(strtr( $shift,pa_daksan() )=='Matin')
                     $color_shift = $main_shift_color[0];
                    elseif(strtr( $shift,pa_daksan() )=='Median')
                        $color_shift = $main_shift_color[1];
                        elseif(strtr( $shift,pa_daksan() )=='Soir')
                            $color_shift = $main_shift_color[2];
           }
         elseif( ($this->category==1)||($this->category==2) )
             $color_shift = $main_shift_color[3];
                        
                        
                        
                        
                        
                $css = '
 
.main_content {   
		
		text-align: center;
		align: center;
                
                
              }
 
.firstLayer {  
		
		width:666px;
		
		 
	   }
           

	

.content {
	      
			  
	
        }
		


.top_line {
              height:3px;
              color: '.$color_shift.';
              margin-bottom:5px;
              margin-top:8px;
          }
                  
.id_header {

       
           }


.logo {
	   /*  border:1px solid green; */
	        width:140px;
                padding-left:3px;
                float:left;
                
		
	  }
        

.schoolName {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;  */
	         width:165px;
                 text-align:center;
			 font-size:1em; 
			 font-weight:bold;
                         font-style:italic;
			 float:left;
                   margin-left:25px;
			 
         }	  

.schoolName1 {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;   */
	         width:165px;
                 text-align:center;
			 font-size:0.9em; 
			 font-weight:bold;
                        /* font-style:italic; */
			 float:left;
			 margin-left:25px; 
         }	  

.foto {
	      
		width:100px;  
                height:100px;
	        margin-left:0px;
              
          
	}		   

.foto_div {
	       border:1px solid #e2e0e0;  /* #AAA9A9;   */
		width:100px;  
                height:100px;
	       margin-left:10px;
               margin-bottom:-2px;
               float:left;
               object-fit: contain;
          
	}

.barcode {
	   /* border:1px solid brown; */
	   width:100px; 
           height:15px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode1 {
	   /* border:1px solid brown; */
	   width:100px; 
           height:28px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode_img {    
             height: 26px; 
             
         }
         


.label_ {
          font-size:6px; 
          font-weight:bold;
          font-style:italic;
          padding:0px;
          margin-bottom:-5px;
          float:left;
        }
        
.text_info {
          text-indent: 3px;
     }

.profile_  {
             color:white;
        font-size:9px; 
          font-weight:bold;
          font-style:italic;
    }

.info {
	   
            width:209px;
	   height:70px; /* height:75px; *//* content 1 */
           float:left;
           text-align: left;
           margin-left:9px;
          margin-bottom:-13px;
	   
    }	   

.block_info {
	    
             width:215px;
	   float:left;
           margin-left:9px;
           
            
	   
    }	 
    
.head_info {
	    
           background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            width:215px;
	   height:13px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:-7px;
           margin-bottom:0px;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
	   
    }	 
    
.foot_info {
	    
            
            color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
            width:215px;
	   height:15px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:14px;
           margin-bottom:-34px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
 
.shift_year {
	   color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
	   width:100px; 
           height:15px; 
           float:left;
           text-align: left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }    
.barcode_foot_info {
	    
            
            /* border:1px solid brown; */
	   width:100px; 
	   height:25px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:0px;
           margin-bottom:-14px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
    

.foot1 {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            height:25px;
            padding-top:1px;
            padding-bottom:1px;
            margin-bottom:2px;
             border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
      }	
      
.foot {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            margin-bottom:2px;
      }	
 
.break { page-break-before: always; } 
.clear_ { clear: both; } 

  ';
                                 $selection=(array)Yii::$app->request->post('selection');
                                if($selection!=null) 
                                 {
                                    $content = $this->renderPartial('printcards',['array_card'=>$selection]);
                                    $pdf = new Pdf();
                                        $pdf->filename = $filename."-".date('Y-m-d h:i:s').'.pdf';
                                        $pdf->content = Pdf::MODE_CORE;
                                        $pdf->mode = Pdf::MODE_BLANK;
                                        //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                        $pdf->defaultFontSize = 10;
                                        $pdf->defaultFont = 'helvetica';
                                        $pdf->format = array('57','88');
                                        $pdf->orientation = Pdf::ORIENT_LANDSCAPE; // ORIENT_PORTRAIT;
                                        $pdf->destination = Pdf::DEST_DOWNLOAD;// DEST_BROWSER;
                                        $pdf->cssInline =  $css;
                                        $pdf->marginLeft = 0.0;
                                        $pdf->marginTop = 0.0;
                                        $pdf->marginRight = 0.0;
                                        $pdf->marginBottom = 0.0;
                                     //   $pdf->marginHeader = ;
                                     //   $pdf->marginFooter = ;

                                        $pdf->content = $content;
                                        $pdf->options = ['title' => Yii::t('app','ID cards')];
                                        $pdf->methods = [
                                                'SetHeader'=>[''],
                                                'SetFooter'=>[''],
                                        ];

                                        $pdf->options = [
                                                'title' => Yii::t('app','ID cards'),
                                                'autoScriptToLang' => true,
                                                'ignore_invalid_utf8' => true,
                                                'tabSpaces' => 4
                                        ];

                                    // return the pdf output as per the destination setting
                                     
                                    foreach($selection as $idcard){
                                        $carte = SrcIdcard::findOne($idcard);
                                        $carte->is_print = 1;
                                        $carte->date_print = date("y-m-d h:i:s");
                                        $carte->save();
                                    }
                                    return $pdf->render();

                                 }
                                else
                                    {
					    	Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',   // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You must check at least a student.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
					}
                                
                               }
                             elseif( ($this->category==3) )   //visitors
                                {   
                                       $color_shift = $main_shift_color[3];
                        
                        
                        
                        
                        
                $css = '
 
.main_content {   
		
		text-align: center;
		align: center;
                
                
              }
 
.firstLayer {  
		
		width:666px;
		
		 
	   }
           

	

.content {
	      
			  
	
        }
		


.top_line {
              height:3px;
              color: '.$color_shift.';
              margin-bottom:3px;
              margin-top:15px;
          }
                  
.id_header {

       align:center;
           }


.logo {
	   /*  border:1px solid green; */
	        width:190px;
       }
        

.logo_div {
	   /*  border:1px solid green; */
	        
                
           align:center;     
		
	  }

        

.schoolName1 {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;   */
	         
                 text-align:center;
			 font-size:0.6em; 
			 font-weight:bold;
                        /* font-style:italic; */
			 
         }	  

.barcode1 {
	   /* border:1px solid brown; */
	   width:100px; 
           height:28px; 
           padding:2px;
           
	   
    }


.label_ {
          font-size:6px; 
          font-weight:bold;
          font-style:italic;
          padding:0px;
          margin-bottom:-5px;
          float:left;
        }
        
.text_info {
          text-indent: 3px;
     }



.info {
	  text-align: center;
           font-weight:bold;
          font-size:24px;
          margin-top:30px;
	   
    }
 .number {
	  text-align: center;
           font-weight:bold;
          font-size:24px;
          align: center;
	   
    }
.number_bg {
	  width:55px;
          color:white;
          background-color:#ff3d3d;
           border-radius: 65px;
           margin:auto;
	   
    }

.block_info {
	    
             width:215px;
             height:152px;
	   float:left;
           
           
            
	   
    }	 

.barcode_foot_info {
	    
            
            /* border:1px solid brown; */
	    
	   height:55px; 
           
           text-align: center;
           margin-bottom:-12px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
    

.foot1 {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            height:35px;
            padding-top:1px;
            margin-bottom:2px;
             border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
      }	
      
.break { page-break-before: always; } 
.clear_ { clear: both; } 

  ';
                                       
                                           $content = $this->renderPartial('visitorscard',['number_of_cards'=>$model->number_of_cards, 'from_number'=>$model->from_number]);
                                           $pdf = new Pdf();
                                               $pdf->filename = Yii::t('app','Visitors')."-".date('Y-m-d h:i:s').'.pdf';
                                               $pdf->content = Pdf::MODE_CORE;
                                               $pdf->mode = Pdf::MODE_BLANK;
                                               //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                               $pdf->defaultFontSize = 10;
                                               $pdf->defaultFont = 'helvetica';
                                               $pdf->format = array('57','88');
                                               $pdf->orientation = Pdf::ORIENT_PORTRAIT; // ORIENT_LANDSCAPE; // 
                                               $pdf->destination = Pdf::DEST_DOWNLOAD;// DEST_BROWSER;
                                               $pdf->cssInline =  $css;
                                               $pdf->marginLeft = 0.0;
                                               $pdf->marginTop = 0.0;
                                               $pdf->marginRight = 0.0;
                                               $pdf->marginBottom = 0.0;
                                            //   $pdf->marginHeader = ;
                                            //   $pdf->marginFooter = ;

                                               $pdf->content = $content;
                                               $pdf->options = ['title' => Yii::t('app','Visitors card')];
                                               $pdf->methods = [
                                                       'SetHeader'=>[''],
                                                       'SetFooter'=>[''],
                                               ];

                                               $pdf->options = [
                                                       'title' => Yii::t('app','Visitors card'),
                                                       'autoScriptToLang' => true,
                                                       'ignore_invalid_utf8' => true,
                                                       'tabSpaces' => 4
                                               ];

                                           // return the pdf output as per the destination setting

                                           return $pdf->render();



                                }
                                
                                
                          }
                
         }
      
                          
                          
                          
                          
                         
              }
        
         

               
                
                return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'model'=>$model,
                    'single_print'=>$single_print,
                ]);

         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
    }

    /**
     * Displays a single Idcard model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Idcard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      if(Yii::$app->user->can('idcards-idcard-create'))
         { 
        
        $model = new SrcIdcard();

      //  if ($model->load(Yii::$app->request->post()) && $model->save()) {
       //     return $this->redirect(['view', 'id' => $model->id]);
      //  } else {
            return $this->render('create', [
                'model' => $model,
            ]);
       // }
            
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing Idcard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {                                                                                                                                                                                           
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Idcard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

     
    
    public function actionUploadphotos(){
        
       if(Yii::$app->user->can('idcards-idcard-uploadphotos'))
         {
        $model = new SrcIdcard();
	$modelIdcard = new SrcIdcard(); 
	$modelPerson = new SrcPersons();
	$is_update=null;
	$image_name ='';
                                 
        $fileName = 'file';
          $uploadPath = 'documents/photo_upload'; 
       
          if (isset($_FILES[$fileName])) {
            
              $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            
        
                  $pers_id  = $file->name;
               //load model person sa
                  $modelPerson = SrcPersons::findOne($pers_id);
                  $imageName = strtolower(  strtolower(strtr( trim(str_replace(" ","",$modelPerson->first_name) ), pa_daksan() ) ).strtolower(strtr( trim(str_replace(" ","",$modelPerson->last_name) ), pa_daksan() ) )  ).$modelPerson->id;
                  
                  $file->name = $imageName.'.'.$file->extension;


	//pa delete ni komante liy sa lap fe kod la pa mache			              	 
        print_r('<br/><br/><br/>*****************************************************************************************');          
  					//delete sil la deja
  				$file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$file->name;
                                     if(file_exists($file_to_delete))
                                     { unlink($file_to_delete);
                                      
                                           $model = SrcIdcard::find()->where('person_id='.$modelPerson->id)->all();
                                             foreach($model as $modelID)
                                                {
                                                    if($modelID->id ==null)
                                                      {   $model = new SrcIdcard();
                                                          $is_update=0;
                                                          
                                                      }
                                                    else 
                                                      { $model = $modelID;
                                                         $is_update=1;
                                                         
                                                      }
                                                      
                                                }
  						 
                                         }
                                     
            
                  if ($file->saveAs($uploadPath . '/' . $file->name)) 
                    {
                   
                     $file_path = $uploadPath . '/' . $file->name;
                      // jpg  change the dimension 750, 450 to your desired values
                     $img = $this->resize_imagejpg($file_path, 200, 200);
                      // again for jpg
                      imagejpeg($img, $uploadPath . '/' . $file->name);
                     // $img = resize_imagejpg($file_path, 200, 200);
                  
                     
                                        if($is_update==0) //se pa foto wap ranplase
  					   {						   
  					   //cheche moun lan nan persons
  						
  						$model->person_id = $modelPerson->id;
  						$model->prenom = trim(str_replace(" ","",$modelPerson->first_name) );
  						$model->nom = trim(str_replace(" ","",$modelPerson->last_name) );
  						$model->sexe = $modelPerson->gender;
						
  						$model->date_ajout = date('Y-m-d h:m:s');
                          // $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                           //$model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                        
                                                   $model->image_name = strtolower( $file->name ); 
                         
  						 if($model->save())
  						 {
  							 //delete fiche ak ansyen non
  							 if( ($modelPerson->image!=null)&&($modelPerson->image!=$model->image_name) )
  							 {
                                                               $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$file->name;
                                                            
                                                               if(file_exists($file_to_delete))
                                                                  unlink($file_to_delete);
							  
  							 } 
							 
  							 $modelPerson->image = strtolower($model->image_name);
  							 $modelPerson->save();
						     
  						 } 
  					   }
  					  elseif($is_update==1)  //ranplase foto
  					  {
  						  //cheche moun lan nan persons
  						 $modelIDcard = SrcIdcard::findOne($model->id);
					
  						$modelIDcard->prenom = trim(str_replace(" ","",$modelPerson->first_name) );
  						$modelIDcard->nom = trim(str_replace(" ","",$modelPerson->last_name) );
  						$modelIDcard->sexe = $modelPerson->gender;
						
  						// $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                           //$model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                         
  						 $modelIDcard->save();
						 
  					  }
                     
                         
                
                 
                  
                  echo \yii\helpers\Json::encode($file);
              }
                
           
            
              //Print file data
              //print_r($file);

            
      }else{
        
          return $this->renderAjax('create', [
                  'model' => $model,
              ]); 
        
        
           }

          return false;
       
       
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
    }
   
    public function actionPrinthtml($id){
       if(Yii::$app->user->can('idcards-idcard-print')) 
          { 
           return $this->render('printcards',[]);
          }
       } 
    
    
  
    public function actionPrintidcard($id){
      
        $main_shift_color = infoGeneralConfig('main_shift_color');
        $main_shift_color=explode(",",substr($main_shift_color,0));  //  0: jour,  1:  median,   2:  soir
        
        $color_shift='#446da3';  //default
        
        
        if(Yii::$app->user->can('idcards-idcard-print')) 
          { 
                
              $modelIdcard=SrcIdcard::findOne($id);
              
               $person = $modelIdcard->person_id;
              
         	  $modelPerson=SrcPersons::findOne($person);
          
                  $filename = strtr( trim(str_replace(" ","",$modelPerson->first_name) ), pa_daksan() ).'_'.strtr( trim(str_replace(" ","",$modelPerson->last_name) ), pa_daksan() );
              
            //gade si se yon elev pou pran vacation an
         if($modelPerson->is_student==1)
           {
               
                   
                   $shift='';
                   

                   if(isset($modelPerson->studentOtherInfo0[0]->apply_shift))
                      $shift=$modelPerson->studentOtherInfo0[0]->applyShift->shift_name; 

                    
                   
         
                    
                 if(strtr( $shift,pa_daksan() )=='Matin')
                     $color_shift = $main_shift_color[0];
                    elseif(strtr( $shift,pa_daksan() )=='Median')
                        $color_shift = $main_shift_color[1];
                        elseif(strtr( $shift,pa_daksan() )=='Soir')
                            $color_shift = $main_shift_color[2];
           }
         else
             $color_shift = $main_shift_color[3];
                        
                        
                        
                        
                        
                $css = '
 
.main_content {   
		
		text-align: center;
		align: center;
                
                
              }
 
.firstLayer {  
		
		width:666px;
		
		 
	   }
           

	

.content {
	      
			  
	
        }
		


.top_line {
              height:3px;
              color: '.$color_shift.';
              margin-bottom:5px;
              margin-top:8px;
          }
                  
.id_header {

       
           }


.logo {
	   /*  border:1px solid green; */
	        width:140px;
                padding-left:3px;
                float:left;
		
	  }
        

.schoolName {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;  */
	         width:165px;
                 text-align:center;
			 font-size:1em; 
			 font-weight:bold;
                         font-style:italic;
			 float:left;
			 margin-left:25px;
         }	  

.schoolName1 {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;   */
	         width:165px;
                 text-align:center;
			 font-size:0.9em; 
			 font-weight:bold;
                        /* font-style:italic; */
			 float:left;
			 margin-left:25px;
                         
         }	  

.foto {
	      
		width:100px;  
                height:100px;
	        margin-left:0px;
              
          
	}		   

.foto_div {
	       border:1px solid #e2e0e0;  /* #AAA9A9;   */
		width:100px;  
                height:100px;
	       margin-left:10px;
               margin-bottom:-2px;
               float:left;
               object-fit: contain;
          
	}

.barcode {
	   /* border:1px solid brown; */
	   width:100px; 
           height:15px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode1 {
	   /* border:1px solid brown; */
	   width:100px; 
           height:28px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode_img {    
             height: 26px; 
             
         }
         


.label_ {
          font-size:6px; 
          font-weight:bold;
          font-style:italic;
          padding:0px;
          margin-bottom:-5px;
          float:left;
        }
        
.text_info {
          text-indent: 3px;
     }

.profile_  {
             color:white;
        font-size:9px; 
          font-weight:bold;
          font-style:italic;
    }

.info {
	   
            width:209px;
	   height:70px; /* height:75px; *//* content 1 */
           float:left;
           text-align: left;
           margin-left:9px;
          margin-bottom:-13px;
	   
    }	   

.block_info {
	    
             width:215px;
	   float:left;
           margin-left:9px;
           
            
	   
    }	 
    
.head_info {
	    
           background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            width:215px;
	   height:13px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:-7px;
           margin-bottom:0px;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
	   
    }	 
    
.foot_info {
	    
            
            color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
            width:215px;
	   height:15px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:14px;
           margin-bottom:-34px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
 
.shift_year {
	   color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
	   width:100px; 
           height:15px; 
           float:left;
           text-align: left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }    
.barcode_foot_info {
	    
            
            /* border:1px solid brown; */
	   width:90px; 
	   height:25px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:0px;
           margin-bottom:-14px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
    

.foot1 {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            height:25px;
            padding-top:1px;
            padding-bottom:1px;
            margin-bottom:2px;
             border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
      }	
      
.foot {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            margin-bottom:2px;
      }	
 
.break { page-break-before: always; } 
.clear_ { clear: both; } 

  ';
                
                
                

// $this->layout = "pdf_layout";
                $content = $this->renderPartial('printcards',['array_card'=>[$id] ]);
                $pdf = new Pdf();
                $pdf->filename = $filename."-".date('Y-m-d h:i:s').'.pdf';
                                        $pdf->content = Pdf::MODE_CORE;
                                        $pdf->mode = Pdf::MODE_BLANK;
                                        //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                        $pdf->defaultFontSize = 10;
                                        $pdf->defaultFont = 'helvetica';
                                        $pdf->format = array('57','88');
                                        $pdf->orientation = Pdf::ORIENT_LANDSCAPE; // ORIENT_PORTRAIT; ORIENT_LANDSCAPE
                                        $pdf->destination = Pdf::DEST_DOWNLOAD;// DEST_BROWSER;
                                        $pdf->cssInline =  $css;
                                        $pdf->marginLeft = 0.0;
                                        $pdf->marginTop = 0.0;
                                        $pdf->marginRight = 0.0;
                                        $pdf->marginBottom = 0.0;
                                     //   $pdf->marginHeader = ;
                                     //   $pdf->marginFooter = ;

                                        $pdf->content = $content;
                                        $pdf->options = ['title' => Yii::t('app','ID cards')];
                                        $pdf->methods = [
                                                'SetHeader'=>[''],
                                                'SetFooter'=>[''],
                                        ];

                                        $pdf->options = [
                                                'title' => Yii::t('app','ID cards'),
                                                'autoScriptToLang' => true,
                                                'ignore_invalid_utf8' => true,
                                                'tabSpaces' => 4
                                        ];

                // return the pdf output as per the destination setting
                return $pdf->render();
          
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }
    

 public function actionPrintacard($id){
      
        $main_shift_color = infoGeneralConfig('main_shift_color');
        $main_shift_color=explode(",",substr($main_shift_color,0));  //  0: jour,  1:  median,   2:  soir
        
        $color_shift='#446da3';  //default
        
        
        if(Yii::$app->user->can('idcards-idcard-print')) 
          { 
                
              $modelIdcard=SrcIdcard::find()->where('person_id='.$id)->all();
              $card_id = 0;
              
              foreach ($modelIdcard as $modIdcard)
                   $card_id =$modIdcard->id;
              
               $person = $id;
              
         	  $modelPerson=SrcPersons::findOne($person);
          
                  $filename = strtr( trim(str_replace(" ","",$modelPerson->first_name) ), pa_daksan() ).'_'.strtr( trim(str_replace(" ","",$modelPerson->last_name) ), pa_daksan() );
              
            //gade si se yon elev pou pran vacation an
         if($modelPerson->is_student==1)
           {
               
                   
                   $shift='';
                   

                   if(isset($modelPerson->studentOtherInfo0[0]->apply_shift))
                      $shift=$modelPerson->studentOtherInfo0[0]->applyShift->shift_name; 

                    
                   
         
                    
                 if(strtr( $shift,pa_daksan() )=='Matin')
                     $color_shift = $main_shift_color[0];
                    elseif(strtr( $shift,pa_daksan() )=='Median')
                        $color_shift = $main_shift_color[1];
                        elseif(strtr( $shift,pa_daksan() )=='Soir')
                            $color_shift = $main_shift_color[2];
           }
         else
             $color_shift = $main_shift_color[3];
                        
                        
                        
                        
                        
                $css = '
 
.main_content {   
		
		text-align: center;
		align: center;
                
                
              }
 
.firstLayer {  
		
		width:666px;
		
		 
	   }
           

	

.content {
	      
			  
	
        }
		


.top_line {
              height:3px;
              color: '.$color_shift.';
              margin-bottom:5px;
              margin-top:8px;
          }
                  
.id_header {

       
           }


.logo {
	   /*  border:1px solid green; */
	        width:140px;
                padding-left:3px;
                float:left;
                
		
	  }
        

.schoolName {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;  */
	         width:165px;
                 text-align:center;
			 font-size:1em; 
			 font-weight:bold;
                         font-style:italic;
			 float:left;
                   margin-left:25px;
			 
         }	  

.schoolName1 {   /* border:1px solid green;  
                border-bottom:1px solid #EC3237;   */
	         width:165px;
                 text-align:center;
			 font-size:0.9em; 
			 font-weight:bold;
                        /* font-style:italic; */
			 float:left;
			 margin-left:25px; 
         }	  
	  

.foto {
	      
		width:100px;  
                height:100px;
	        margin-left:0px;
              
          
	}		   

.foto_div {
	       border:1px solid #e2e0e0;  /* #AAA9A9;   */
		width:100px;  
                height:100px;
	       margin-left:10px;
               margin-bottom:-2px;
               float:left;
               object-fit: contain;
          
	}

.barcode {
	   /* border:1px solid brown; */
	   width:100px; 
           height:15px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode1 {
	   /* border:1px solid brown; */
	   width:100px; 
           height:28px; 
           float:left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }
    
.barcode_img {    
             height: 26px; 
             
         }
         


.label_ {
          font-size:6px; 
          font-weight:bold;
          font-style:italic;
          padding:0px;
          margin-bottom:-5px;
          float:left;
        }
        
.text_info {
          text-indent: 3px;
     }

.profile_  {
             color:white;
        font-size:9px; 
          font-weight:bold;
          font-style:italic;
    }

.info {
	   
            width:209px;
	   height:70px; /* height:75px; *//* content 1 */
           float:left;
           text-align: left;
           margin-left:9px;
          margin-bottom:-13px;
	   
    }	   

.block_info {
	    
             width:215px;
	   float:left;
           margin-left:9px;
           
            
	   
    }	 
    
.head_info {
	    
           background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            width:215px;
	   height:13px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:-7px;
           margin-bottom:0px;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
	   
    }	 
    
.foot_info {
	    
            
            color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
            width:215px;
	   height:15px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:14px;
           margin-bottom:-34px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
 
.shift_year {
	   color:#EC3237;  /* wouj logo canado */
            font-weight:bold;
	   width:100px; 
           height:15px; 
           float:left;
           text-align: left;
           margin-left:6px;
           margin-bottom:0px;
           padding:2px;
           
	   
    }    
.barcode_foot_info {
	    
            
            /* border:1px solid brown; */
	   width:90px; 
	   height:25px; 
           float:left;
           text-align: center;
           margin-left:9px;
           margin-top:0px;
           margin-bottom:-14px;
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
	   
    }	 
    

.foot1 {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            height:25px;
            padding-top:1px;
            padding-bottom:1px;
            margin-bottom:2px;
             border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
      }	
      
.foot {  
            background-color:'.$color_shift.'; 
            color:white;
            font-weight:bold;
            text-align:center; 
            font-size:9px;
	    font-style: italic;
            width:666px;
            margin-bottom:2px;
      }	
 
.break { page-break-before: always; } 
.clear_ { clear: both; } 

  ';
                
                
                

// $this->layout = "pdf_layout";
                $content = $this->renderPartial('printcards',['array_card'=>[$card_id] ]);
                $pdf = new Pdf();
                $pdf->filename = $filename."-".date('Y-m-d h:i:s').'.pdf';
                                        $pdf->content = Pdf::MODE_CORE;
                                        $pdf->mode = Pdf::MODE_BLANK;
                                        //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                        $pdf->defaultFontSize = 10;
                                        $pdf->defaultFont = 'helvetica';
                                        $pdf->format = array('57','88');
                                        $pdf->orientation = Pdf::ORIENT_LANDSCAPE; // ORIENT_PORTRAIT;
                                        $pdf->destination = Pdf::DEST_DOWNLOAD;// DEST_BROWSER;
                                        $pdf->cssInline =  $css;
                                        $pdf->marginLeft = 0.0;
                                        $pdf->marginTop = 0.0;
                                        $pdf->marginRight = 0.0;
                                        $pdf->marginBottom = 0.0;
                                     //   $pdf->marginHeader = ;
                                     //   $pdf->marginFooter = ;

                                        $pdf->content = $content;
                                        $pdf->options = ['title' => Yii::t('app','ID cards')];
                                        $pdf->methods = [
                                                'SetHeader'=>[''],
                                                'SetFooter'=>[''],
                                        ];

                                        $pdf->options = [
                                                'title' => Yii::t('app','ID cards'),
                                                'autoScriptToLang' => true,
                                                'ignore_invalid_utf8' => true,
                                                'tabSpaces' => 4
                                        ];

                // return the pdf output as per the destination setting
                return $pdf->render();
                
          
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
          
          
          
          
          
          
    }
    


    
    /**
     * Finds the Idcard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Idcard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcIdcard::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function resize_imagejpg($file, $w, $h) 
     {
        list($width, $height) = getimagesize($file);
        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($w, $h);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $w, $h, $width, $height);
        return $dst;
    }
    
    public function actionListephotos(){
        $searchModel = new SrcIdcard();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        echo $this->renderAjax('lisfoto',['model'=>$searchModel, 'id_card'=>$dataProvider,]); 
    }
    
    
    public function actionDeletecard($id)
	{
	
        if(Yii::$app->user->can('idcards-idcard-deletecard'))
         {
        $model = $this->findModel($id); 
            $filename = Yii::$app->request->baseUrl.'/documents/photo_upload/'.$model->image_name; 
            if(is_file($filename)){
                if(unlink($filename)){
                    $model->delete();
					//retire non photo a nan tab persons
		      $modelPersons=new Persons();
                     $pers = Persons::findOne($model->person_id);
							 $pers->image = '';
							 $pers->save();					
                    return $this->redirect(array('create'));
                }
            }else {
               
			  $model->delete();
               return $this->redirect(array('create'));
            }
            
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
		   
	}
	
    
    
    
    
    
    
    
}
