<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\idcards\models\Idcard */

$this->title = Yii::t('app', 'Create Idcard');

?>

<?= $this->render("//layouts/idcardLayout") ?>
<div class="row">
     
    <div class="col-lg-8">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>   
</div>    
<div class="wrapper wrapper-content idcard-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
