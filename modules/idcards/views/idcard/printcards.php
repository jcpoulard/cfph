 <?php

use app\modules\idcards\models\SrcIdcard;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersonsHasTitles;
use app\modules\fi\models\SrcTitles;
?>
<?php
       $modelPerson= new SrcPersons;
         
        //Extract school address
        $school_name = infoGeneralConfig('school_name');
        //Extract devise school
        $school_acronym = infoGeneralConfig('school_acronym');

        //Extract school address
        $school_address = infoGeneralConfig('school_address');
        //Extract  email address 
        $school_email_address = infoGeneralConfig('school_email_address');
        //Extract site_web 
        $school_site_web = infoGeneralConfig('school_site_web');   
						   
        //Extract Phone Number
        $school_phone_number = infoGeneralConfig('school_phone_number');
							
        

				            
                                                
        //$secondary_color = infoGeneralConfig('secondary_color');
        //$balance_color = infoGeneralConfig('balance_color');
        //$line_color = infoGeneralConfig('line_color');
	
        $acad = Yii::$app->session['currentId_academic_year'];
        
        $acadModel = SrcAcademicperiods::findOne($acad);
        
        $path_logo= Yii::$app->request->BaseUrl."/img/Logo_Cana_Reel_G.png";
        $path_siyati= Yii::$app->request->BaseUrl."/img/siyati.png";
        
        
      if($school_site_web=='')
        $school_site_web =$school_email_address;
     
      $color_shift ='';
      $path_foto='';
      $total_cards = 0;
      $total_cards = sizeof($array_card);
      
    foreach ($array_card as $id_card)
      {
        $total_cards = $total_cards-1;
        
        $modelIdcard=SrcIdcard::findOne($id_card);
		 
         $student = $modelIdcard->person_id;
	
         
         	  $modelStudent=SrcPersons::findOne($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
		  $pers_nif_cin = $modelStudent->nif_cin;
              
                   
                   $program ='';
                   $program_short_name ='';
                   $level='';
                   $shift='';
                   $room='';
                   
                $title_name="";

            //gade si se yon elev 
        if($modelStudent->is_student==1)       
           {
                   if(isset($modelStudent->studentOtherInfo0[0]->apply_shift))
                      $shift=$modelStudent->studentOtherInfo0[0]->applyShift->shift_name; 

                   if(isset($modelStudent->studentLevel->room)&&($modelStudent->studentLevel->room!=0))
                      $room= $modelStudent->studentLevel->room0->room_name; 

                        if(isset($modelStudent->studentOtherInfo0[0]->apply_for_program)) 
                        { $program = $modelStudent->studentOtherInfo0[0]->applyForProgram->label;
                          $program_short_name = $modelStudent->studentOtherInfo0[0]->applyForProgram->short_name;
                        }

                    if(isset($modelStudent->studentLevel->level) )
                                 $level= $modelStudent->studentLevel->level;
         
            }
         else
         {       //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = $modelPerson->isEmployeeOrTeacher($student, $acad);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=SrcPersonsHasTitles::find()->where(['persons_id'=>$student])->andWhere(['academic_year'=>$acad])->all();
		                      
		                    	
                    if($title!=null)
                      {
                        foreach($title as $title)
		          {
                            $modelTitle = SrcTitles::findOne($title->titles_id);
                        
                             $title_name= $modelTitle->title_name; 
                          }
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
	 
      
     $path_foto=Yii::$app->request->BaseUrl."/documents/photo_upload/".$modelIdcard->image_name;
       
 ?>
     
    <div class="main_content">

      <span class=""><hr class="top_line"/></span>
      
      <div class="id_header">

           <div class="logo"><img src="<?= $path_logo ?>" /></div> 
           <div class="schoolName1"><?= $school_name ?></div>

      </div>
      <div class="content">

            <div class="foto_div"><img src="<?= $path_foto ?>" class="foto"  /></div> 
            <div class="block_info">   <div class="head_info"><?php if($modelStudent->is_student==1) 
           { echo $program_short_name.' / '.$shift; }else{ echo Yii::t('app', 'STAFF'); } ?>  </div>           
                 <div class="info"><div class="label_" ><br/><?php if($modelStudent->is_student==1)      
           { echo  Yii::t('app', 'Student Name');}else{ echo Yii::t('app', 'Employee Name'); } ?></div>
                              <div class="text_info"><?= $first_name ?>  <strong><?= $last_name ?></strong><br/> </div>
                              <div class="label_" ><?php if($modelStudent->is_student==1)      
                                                        { echo  Yii::t('app', 'Title'); } else{ echo  Yii::t('app', 'Title'); } ?><br/></div>
                              <div class="text_info"><?php if($modelStudent->is_student==1)      
                                                        { echo  Yii::t('app', 'Level').' '.$level; }else{ echo  $title_name; } ?><br/>   </div>
                              <div class="label_" ><?= Yii::t('app', 'Id Number') ?><br/></div>
                              <div class="text_info"><?= $pers_cod ?>   </div>
                              
             </div>   </div>
            <div class="clear_"></div>
            <div style="width:66%; float:left; margin-bottom: -4px;">
                <div class="shift_year" > 
                    <?php      
                            echo  '[ '.$acadModel->name_period.' ]';  ?>
                </div>
                <div style="width:47%; float:left; margin-top: -15px;  margin-bottom: 0px;">
                    <img width="100px" src="<?= $path_siyati ?>" />
                </div>
            </div>
            
            <div class="barcode_foot_info"> <?php
                            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                            echo '<img class="barcode1" src="data:image/png;base64,' . base64_encode($generator->getBarcode($student, $generator::TYPE_CODE_39)) . '"  >';
                        ?> </div>
            <div class="foot1"><?= $school_address.'<br/>'.$school_site_web.' - '.$school_phone_number ?></div>
            

      </div>
        
                                
 </div> 

<?php     
     
     $modelIdcard->is_print =1;
     $modelIdcard->date_print = date('Y-m-d');
     $modelIdcard->save();
     
     
if($total_cards!=0)
{ echo '<div class="break"></div> ';
}
      


}      
?>