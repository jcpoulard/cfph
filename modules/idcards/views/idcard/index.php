<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\idcards\models\Idcard;
use app\modules\idcards\models\SrcIdcard;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcCourses;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\idcards\models\SrcIdcard */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ready to print');
$this->params['breadcrumbs'][] = $this->title;

            $program= Yii::$app->session['idcard_program'];
	        $shift= Yii::$app->session['idcard_shift'];
	        $room = Yii::$app->session['idcard_room'];
                
$modelCourse = new SrcCourses;
	       
				
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<?= $this->render("//layouts/idcardLayout") ?>


<div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content idcard-index">
 
    <?php $form = ActiveForm::begin(); ?>
    
    
<?php
    $modelShift_ = new SrcShifts();
    
    
 ?>
	 <div class="row">
             
             <div class="col-lg-2">
                 
                  <?php 
					 
					 
						
          
			 echo $form->field($model, 'category_')->widget(Select2::classname(), [
			                                   'data'=>array(null=>Yii::t('app','All'),'0'=>Yii::t('app','Students'),'1'=>Yii::t('app','Teachers'),'2'=>Yii::t('app','Employees'),'3'=>' ==> [ '.Yii::t('app','Visitors').' ] <==' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>[ //'placeholder'=>Yii::t('app', ' --  select category  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);			
						
					 
	           ?>  
             </div>  
 <?php
 
 if( ($model->category_!= 3))
  {
    if( ($model->category_== 0) && ($model->category_!=null) )
     {
 ?>            
        <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

     <div class="col-lg-2" style="margin-left:18px; padding:0px;">
      <?php
                    //return program_id or null value
             $program = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program==null)
		       {
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       	
		       	     if(isset($model->program))
		       	       $program = $model->program;
		       	     else
		       	        $program = 0; 
		       	$data_shift = [];
		       }
		     else
		       {
		       	  $data_program = ArrayHelper::map(SrcProgram::find()->where('id='.$program)->all(),'id','label' );
		       	  
		       	  Yii::$app->session['idcards_program'] = $program; 
		       	  
		       	    
                  $data_shift = $modelShift_->getShiftinprogram($program,$acad);
		       	  
		       	  
		       	}
        
           ?>
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'submit()', /* '
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=idcards&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcidcard-shift").html(data);	
                                       //chanje course 
                                        $("#srcidcard-room").html("");
                                        	
                                        });', 
                                     * */
                                    
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         
            //  if($model->shift!='')

             //   {      
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                 // }
               
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'submit()', /*'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=idcards&shift='.'"+$(this).val()+"&program='.'"+$("#srcidcard-program").val()+"&level='.'"+$("#srcidcard-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcidcard-room").html(data);	
                                       //chanje course 
                                      //  $("#srcidcard-course").html("");
                                        	
                                        });',
                                      * 
                                      */
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        //$data_room = [];
              //if($model->room!='')
               // {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                        // ->all(),'room0.id','room0.room_name' );
                        
                     $from = 'idcards';
                     $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift,$model->program,$model->level,$acad);
                  //}      
                 ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>'submit()',], /*'
                                        $.post("../../fi/courses/courseinroomforgrades?from=idcards&room='.'"+$(this).val()+"&shift='.'"+$("#srcgrades-shift").val()+"&program='.'"+$("#srcgrades-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcidcard-course").html(data);	
                                        	
                                        });',
  
                       ],  */
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  <?php  //       $data_course = [];
         //     if($model->course[0]!='')
        //        {  $modelCourse_ = new SrcCourses();
                  /*	$data_course = ArrayHelper::map(SrcCourses::find()->joinWith(['module0'])->where(['room'=>$model->room,'shift'=>$model->shift,'program'=>$model->program])
                         ->all(),'id','module0.subject0.subject_name' );
                   */
               
                   
           //     $data_course = $modelCourse_->getCourseinroomforgrades($model->room[0],$model->shift[0],$model->program[0],$acad);
          //        }      
                 ?> 
     
      </div>
<?php
   

    }
    
?>
             
    </div>
    
  
<br/>  
  <div class="col-md-14 table-responsive">

<?php 


if($single_print ==1)
   {
     
     ?>


				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Gender'); ?></th>
				            <th><?= Yii::t('app','Printing date'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 





    $dataIdCard = $dataProvider->getModels();
    
          foreach($dataIdCard as $idcard)
           {  
           	
           	   echo '  <tr >
                                                    <td >'.$idcard->person->id_number.' </td>
                                                    <td >'.$idcard->person->first_name.' </td>
                                                    <td >'.$idcard->person->last_name.' </td>
                                                    <td >'.$idcard->person->genders.' </td>
                                                    <td >'.Yii::$app->formatter->asDate($idcard->date_print).' </td>';
                                                    
                             
                                                 echo '  <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                         if(Yii::$app->user->can('idcards-idcard-print')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-print"></span>', Yii::getAlias('@web').'/index.php/idcards/idcard/printidcard?id='.$idcard->id.'&wh=prt', [
                                    'title' => Yii::t('app', 'Print'),
                        ]); 
                                                              }
                                                              
                                                        
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        
<?php

   }
 elseif($single_print ==0)
   {
 
     ?> 
         
    
    <?= GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-striped table-bordered table-hover dataTables-example'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['id'] ];
                                                 },
         ],
         [
             'attribute'=>'person_id',
			 'label'=>Yii::t("app","Id Number"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->person->id_number;
				 }
			  ],
            
            
            
        
        [
             'attribute'=>'prenom',
			 'label'=>Yii::t("app","First Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->prenom;
				 }
			  ],
            
            
            
         [
             'attribute'=>'nom',
			 'label'=>Yii::t("app","Last Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->nom;
				 }
			  ],
			  
			  
	      [
             'attribute'=>'sexe',
			 'label'=>Yii::t("app","Gender"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->getGender();
				 }
			  ],
			  
			  
	     
             
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
        <?php 
    
    
 if($dataProvider->getModels()!=null)
   {   
    ?>
    


    
    <div class="">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton( Yii::t('app', 'Print'), ['name' => 'print', 'class' => 'btn btn-success']) ?>

           
            
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

<?php
   }
     
     
     
   }
 
   
 }
else if( ($model->category_== 3) )  //visitors
  {  
    
  ?>
      <div class="row">
        <div class="col-lg-3">
            <?= $form->field($model, 'number_of_cards')->textInput(['maxlength' => true]) ?>
        </div>
          <div class="col-lg-3">
            <?= $form->field($model, 'from_number')->textInput(['maxlength' => true]) ?>
        </div>
        
    </div>
    <div class="">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton( Yii::t('app', 'Print'), ['name' => 'print', 'class' => 'btn btn-success']) ?>

           
            
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>  
      
 <?php   
  }
   
   
 
?>
                 </div>

  <?php ActiveForm::end(); ?>
</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Grades List'},
                    {extend: 'pdf', title: 'Grades List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
    
