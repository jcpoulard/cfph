<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\idcards\models\Idcard */
/* @var $form yii\widgets\ActiveForm */


?>



<div class="idcard-form">

    <?php $form = ActiveForm::begin(); ?>

   <div class="row" style="padding-left:80px;"> <center>
    
    <div id="dropZon" class="col-lg-11">
        <div class='row'>
        
        
        <?php
            echo \kato\DropZone::widget([
                   'options' => [
                       'url'=>Yii::getAlias('@web').'/index.php/idcards/idcard/uploadphotos',
                       'maxFilesize' => '2',
                       'acceptedFiles'=>'.jpeg,.jpg,.png,.JPEG,.JPG, .PNG', // '.JPG', // 
                       'addRemoveLinks'=>false,
                       'dictDefaultMessage'=>'<i class="fa fa-up"></i> '.Yii::t('app','Drag and Drop your image here to upload'),'dictFallbackMessage'=>Yii::t('app','The browser do not support the process. :('),'dictFallbackText'=>Yii::t('app','Please use an alternative upload process!'),'dictInvalidFileType'=>Yii::t('app','Type of files incorrect !'),'dictFileTooBig'=>Yii::t('app','File too big!'),'dictResponseError'=>Yii::t('app','Oops ! Something goes wrong!'),'dictCancelUpload'=>Yii::t('app','Delete'),'dictCancelUploadConfirmation'=>Yii::t('app','Do you want to cancel the upload?'),'dictRemoveFile'=>Yii::t('app','Delete'),'dictMaxFilesExceeded'=>Yii::t('app','File size exceeded'),
                   ],
                   'clientEvents' => [
                       'complete' => "function(file){"
                       . "console.log(file);"
                       . "var valeur = 1;
                        $.get(\"".Yii::getAlias('@web')."/index.php/idcards/idcard/listephotos\",{},function(data){
                        $('#lis_foto').html(data);
                        
                        });"
                       . "}",
                       
                   ],
               ]);

            ?>
        <!--
            
        -->
        
        </div>
        <div class='row' id='lis_foto'>
            
        </div>
            
             </center>
    </div>
    <div class="col-lg-1">
        
    </div>

    
    

    

    <?php ActiveForm::end(); 
    
    $basePath = Yii::getAlias('@web');
    ?>
    
    
    <input id="basepath" name="basepath" type="hidden" value="'.$basePath.'" />

</div>


  
        
<?php 
       $baseUrl=Yii::$app->request->baseUrl;
$script = <<< JS
       
   $(document).ready(function(){
        
    var base_path = document.getElementById("basepath");
        
   $.get("$baseUrl/index.php/idcards/idcard/listephotos",{},function(data){
                $('#lis_foto').html(data);
            });
        $('#dropZon').mouseover(function(){
            $.get("$baseUrl/index.php/idcards/idcard/listephotos",{},function(data){
                $('#lis_foto').html(data);
            });
           //alert('Test li sou li');
        });
   });
   
       
  
JS;
$this->registerJs($script);



 
?>