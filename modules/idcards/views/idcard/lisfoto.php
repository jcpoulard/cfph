<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 
use yii\widgets\Pjax;


?>

<div class="col-md-14 table-responsive"> 
 <center> <table class='table table-striped table-bordered table-hover dataTables-example' style='width:60%;' >
    <thead>
        <tr >
    <th colspan="6"><?= Yii::t('app','Liste photo')?></th>
    
        </tr>
    
    </thead>
    <tbody>
        <?php  $id_card = $id_card->getModels();
		       $i =1;
            foreach($id_card as $ci){ 
                 $string_image_location = "<img src=".Yii::$app->request->baseUrl."/documents/photo_upload/".$ci->image_name.">";
                ?>
        <tr>
            <td  style="width:8%;" ><?= $i; ?></td>
			<td  style="width:25%;" ><?= $ci->prenom; ?></td>
                        <td  style="width:23%;" ><?= $ci->nom; ?></td>
			<td><div class="avatar " style="width:23%;">

					        <?php

					            $path = Yii::$app->request->baseUrl.'/documents/photo_upload/'.$ci->image_name;
                                
                             ?>
					<img src="<?= $path ?>" class="img img-rounded img-responsive avatar" alt="" > 

				</div>
			</td>
			<td  style="width:12%" ><?= $model->getIdNumberForList($ci->person_id); ?></td>
			
            <td><?= Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/idcards/idcard/deletecard?id='.$ci->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ; ?></td>
            
        </tr>
        <?php 
               $i++;
            }   
        ?>
    </tbody>
    
  </table>
  </center>
</div>

<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Idcard List'},
                    {extend: 'pdf', title: 'Idcard List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);
            



?>
    