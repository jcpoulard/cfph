<?php

namespace app\modules\idcards\models;

use Yii;


use app\modules\fi\models\SrcPersons;

/**
 * This is the model class for table "idcard".
 *
 * @property integer $id
 * @property integer $person_id
 * @property string $prenom
 * @property string $nom
 * @property string $sexe
 * @property string $image_name
 * @property string $date_ajout
 * @property integer $is_print
 * @property string $date_print
 *
 * @property Persons $person
 */
class Idcard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'idcard';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id' ], 'required'],
            [['person_id', 'is_print'], 'integer'],
            [['person_id','date_ajout', 'date_print'], 'safe'],
            [['prenom', 'nom', 'sexe', 'image_name'], 'string', 'max' => 200],
            [['person_id'], 'unique'],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'person_id' => Yii::t('app', 'Person ID'),
            'prenom' => Yii::t('app', 'Prenom'),
            'nom' => Yii::t('app', 'Nom'),
            'sexe' => Yii::t('app', 'Sexe'),
            'image_name' => Yii::t('app', 'Image Name'),
            'date_ajout' => Yii::t('app', 'Date Ajout'),
            'is_print' => Yii::t('app', 'Is Print'),
            'date_print' => Yii::t('app', 'Date Print'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'person_id']);
    }
}
