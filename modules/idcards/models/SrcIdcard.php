<?php

namespace app\modules\idcards\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\idcards\models\Idcard;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcPersonsHasTitles;
use app\modules\fi\models\PersonsHasTitles;

/**
 * SrcIdcard represents the model behind the search form about `app\modules\idcards\models\Idcard`.
 */
class SrcIdcard extends Idcard
{
   
    public $category_;
    public $number_of_cards;
    public $from_number=1;
     public $program; 
   public $shift;
   public $room;
   public $level; 
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id', 'is_print','number_of_cards','from_number'], 'integer'],
            [['prenom', 'nom', 'sexe', 'image_name', 'date_ajout', 'date_print'], 'safe'],
        ];
    }
    
    
     public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'program' =>Yii::t('app','Program'),
			               'shift' =>Yii::t('app','Shift'),
			               'room' =>Yii::t('app','Room'),
			               'level' =>Yii::t('app','Level'),
			               'category_' =>Yii::t('app','Category'),
                                       'number_of_cards'=>Yii::t('app','Number of cards'),
                                       'from_number'=>Yii::t('app','From number'),
		));
	}		


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcIdcard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'pagination' => [
                       'pageSize' => 1000000,
                    ],
             'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'person_id' => $this->person_id,
            'date_ajout' => $this->date_ajout,
            'is_print' => $this->is_print,
            'date_print' => $this->date_print,
        ]);

        $query->andFilterWhere(['like', 'prenom', $this->prenom])
            ->andFilterWhere(['like', 'nom', $this->nom])
            ->andFilterWhere(['like', 'sexe', $this->sexe])
            ->andFilterWhere(['like', 'image_name', $this->image_name]);

        return $dataProvider;
    }
    
  
    
 public function searchAllStudents()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->alias='i';
                
                $criteria->join='inner join persons p on(p.id = i.person_id) ';
                
                $criteria->condition = 'p.is_student=1';

		$criteria->compare('i.id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('sexe',$this->sexe,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('date_ajout',$this->date_ajout,true);
		$criteria->compare('is_print',$this->is_print);
		$criteria->compare('date_print',$this->date_print,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria,
		));
	}
	
  
        
  public function searchForAllTeachers($acad)
	{
           
           $query_employee = SrcIdcard::find()->orderBy(['nom'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('i')
                ->joinWith(['person','person.personsHasTitles',  ])
                ->distinct(true)
                ->where('is_student=0 AND active in(1,2) AND academic_year='.$acad)
                ->all();        
      
           $query = SrcIdcard::find()->orderBy(['nom'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('i')
                ->joinWith(['person','person.courses',  ])
                ->distinct(true)
                ->where('is_student=0 AND active in(1,2) AND academic_year='.$acad)
                ->andWhere(['not in', 'person_id',$query_employee]);
                   
	
	 
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;
                
                
	}
	
        
      
   public function searchForAllEmployees($acad)
	{    
           $query = SrcIdcard::find()->orderBy(['nom'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('i')
                ->joinWith(['person','person.personsHasTitles',  ])
                ->distinct(true)
                ->where('is_student=0 AND active in(1,2) AND academic_year='.$acad);
                   
	
	 
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;
        
	
    }      
  
	
            
        
public function searchByLevelProgramShiftGroup($le,$prog,$sh,$ro,$acad)
{
	$query = SrcIdcard::find()->orderBy(['nom'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('i')
                ->distinct(true);
                   
	
	if( ($le!=null)&&($prog==null)&&($sh==null)&&($ro==null) )
	{
		 $query ->joinWith(['person','person.studentLevel','person.studentOtherInfo0','person.courses0',  ])
                ->where([ 'level'=>$le, 'academic_year'=>$acad]);
		
		
	}
	elseif( ($le!=null)&&($prog!=null)&&($sh==null)&&($ro==null) )
	{
		
                 $query ->joinWith(['person','person.studentLevel','person.studentOtherInfo0','person.courses0',  ])
                ->where([ 'level'=>$le,'apply_for_program'=>$prog, 'academic_year'=>$acad]);
		
	}
	elseif( ($le!=null)&&($prog!=null)&&($sh!=null)&&($ro==null) )
	{
		$query ->joinWith(['person','person.studentLevel','person.studentOtherInfo0','person.courses0',  ])
                ->where([ 'level'=>$le,'apply_for_program'=>$prog, 'apply_shift'=>$sh, 'academic_year'=>$acad]);
                 
	}
	elseif( ($le!=null)&&($prog!=null)&&($sh!=null)&&($ro!=null) )
	{
		
                
                $query ->joinWith(['person','person.studentLevel','person.studentOtherInfo0','person.courses0',  ])
                ->where([ 'level'=>$le,'apply_for_program'=>$prog, 'apply_shift'=>$sh, 'student_level.room'=>$ro, 'academic_year'=>$acad]);
                
	}
	
	/**/
	
	$dataProvider = new ActiveDataProvider([
		        'query' => $query,
		        'pagination' => [
		            'pageSize' => 10000,
		        ],
		    ]);
       
        return $dataProvider;
	
}




  
  public function getIdNumber()
			{
			
				$modelPerson = SrcPersons::findOne($this->person_id);
				
				return $modelPerson->id_number;
					
					
			}
	

public function getIdNumberForList($person_id)
			{
				$modelPerson = SrcPersons::findOne($person_id);
				
				return $modelPerson->id_number;
					
			}

			
public function getGender()
    {

            switch($this->sexe)
            {
                    case 0:
                            return Yii::t('app','Male');

                    case 1:
                            return Yii::t('app','Female');

                    }
    }	
	  
    
    
    
    
    
    
}
