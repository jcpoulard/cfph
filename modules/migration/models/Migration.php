<?php

namespace app\modules\migration\models;

use Yii;

/**
 * This is the model class for table "migration".
 *
 * @property integer $id
 * @property string $file_name
 * @property integer $is_migrate
 * @property integer $is_delete
 * @property string $migrate_by
 * @property string $type_migration
 * @property string $date_upload
 * @property string $date_migrate
 * @property string $date_delete
 */
class Migration extends \yii\db\ActiveRecord
{
    public $migrate_for_who;
    public $level;
    public $program;
    public $shift;
    public $room;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'migration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name', 'is_migrate', 'is_delete'], 'required'],
            [['is_migrate', 'is_delete'], 'integer'],
            //[['date_upload', 'date_migrate', 'date_delete'], 'safe'],
            [['file_name', 'migrate_by', 'type_migration'], 'string', 'max' => 255],
            [['file_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'file_name' => Yii::t('app', 'File Name'),
            'is_migrate' => Yii::t('app', 'Is Migrate'),
            'is_delete' => Yii::t('app', 'Is Delete'),
            'migrate_by' => Yii::t('app', 'Migrate By'),
            'type_migration' => Yii::t('app', 'Type Migration'),
            'date_upload' => Yii::t('app', 'Date Upload'),
            'date_migrate' => Yii::t('app', 'Date Migrate'),
            'date_delete' => Yii::t('app', 'Date Delete'),
        ];
    }
}
