<?php

namespace app\modules\migration\models;
use app\modules\fi\models\Cities;

use Yii;

/**
 * This is the model class for table "persons".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $gender
 * @property string $blood_group
 * @property string $birthday
 * @property string $id_number
 * @property integer $is_student
 * @property string $adresse
 * @property string $phone
 * @property string $email
 * @property string $nif_cin
 * @property integer $cities
 * @property string $citizenship
 * @property integer $paid
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 * @property integer $active
 * @property string $image
 * @property string $comment
 *
 * @property Balance[] $balances
 * @property Billings[] $billings
 * @property ContactInfo[] $contactInfos
 * @property Courses[] $courses
 * @property DepartmentHasPerson[] $departmentHasPeople
 * @property EmployeeInfo[] $employeeInfos
 * @property Grades[] $grades
 * @property LoanOfMoney[] $loanOfMoneys
 * @property PayrollSettings[] $payrollSettings
 * @property PendingBalance[] $pendingBalances
 * @property Cities $cities0
 * @property PersonsHasTitles[] $personsHasTitles
 * @property ScholarshipHolder[] $scholarshipHolders
 * @property StudentAttendance[] $studentAttendances
 * @property StudentHasCourses[] $studentHasCourses
 * @property StudentLevel $studentLevel
 * @property StudentOtherInfo[] $studentOtherInfos
 * @property TeacherAttendance[] $teacherAttendances
 * @property User[] $users
 */
class NewPersons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name'], 'required'],
            [['birthday', 'date_created', 'date_updated'], 'safe'],
            [['is_student', 'cities', 'paid', 'active'], 'integer'],
            [['last_name', 'gender', 'phone', 'email', 'citizenship', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['first_name'], 'string', 'max' => 120],
            [['blood_group'], 'string', 'max' => 10],
            [['id_number', 'image'], 'string', 'max' => 50],
            [['adresse', 'comment'], 'string', 'max' => 255],
            [['nif_cin'], 'string', 'max' => 100],
            [['cities'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['cities' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'last_name' => Yii::t('app', 'Last Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'gender' => Yii::t('app', 'Gender'),
            'blood_group' => Yii::t('app', 'Blood Group'),
            'birthday' => Yii::t('app', 'Birthday'),
            'id_number' => Yii::t('app', 'Id Number'),
            'is_student' => Yii::t('app', 'Is Student'),
            'adresse' => Yii::t('app', 'Adresse'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nif_cin' => Yii::t('app', 'Nif Cin'),
            'cities' => Yii::t('app', 'Cities'),
            'citizenship' => Yii::t('app', 'Citizenship'),
            'paid' => Yii::t('app', 'Paid'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
            'active' => Yii::t('app', 'Active'),
            'image' => Yii::t('app', 'Image'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalances()
    {
        return $this->hasMany(Balance::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillings()
    {
        return $this->hasMany(Billings::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactInfos()
    {
        return $this->hasMany(ContactInfo::className(), ['person' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['teacher' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartmentHasPeople()
    {
        return $this->hasMany(DepartmentHasPerson::className(), ['employee' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeInfos()
    {
        return $this->hasMany(EmployeeInfo::className(), ['employee' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(Grades::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanOfMoneys()
    {
        return $this->hasMany(LoanOfMoney::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayrollSettings()
    {
        return $this->hasMany(PayrollSettings::className(), ['person_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendingBalances()
    {
        return $this->hasMany(PendingBalance::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities0()
    {
        return $this->hasOne(Cities::className(), ['id' => 'cities']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonsHasTitles()
    {
        return $this->hasMany(PersonsHasTitles::className(), ['persons_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScholarshipHolders()
    {
        return $this->hasMany(ScholarshipHolder::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAttendances()
    {
        return $this->hasMany(StudentAttendance::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentHasCourses()
    {
        return $this->hasMany(StudentHasCourses::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentLevel()
    {
        return $this->hasOne(StudentLevel::className(), ['student_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentOtherInfos()
    {
        return $this->hasMany(StudentOtherInfo::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherAttendances()
    {
        return $this->hasMany(TeacherAttendance::className(), ['teacher' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['person_id' => 'id']);
    }
}
