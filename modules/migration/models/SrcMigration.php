<?php

namespace app\modules\migration\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\migration\models\Migration;

/**
 * SrcMigration represents the model behind the search form about `app\modules\migration\models\Migration`.
 */
class SrcMigration extends Migration
{
    
    public $file;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_migrate', 'is_delete'], 'integer'],
           // [['file_name', 'migrate_by', 'type_migration', 'date_upload', 'date_migrate', 'date_delete'], 'safe'],
           [['file'], 'file', 'extensions' => 'csv,xls,xlsx'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Migration::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_migrate' => $this->is_migrate,
            'is_delete' => $this->is_delete,
            'date_upload' => $this->date_upload,
            'date_migrate' => $this->date_migrate,
            'date_delete' => $this->date_delete,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'migrate_by', $this->migrate_by])
            ->andFilterWhere(['like', 'type_migration', $this->type_migration]);

        return $dataProvider;
    }
}
