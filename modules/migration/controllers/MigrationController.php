<?php

namespace app\modules\migration\controllers;

use Yii;
use app\modules\migration\models\Migration;
use app\modules\migration\models\SrcMigration;
use app\modules\migration\models\NewPersons;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json; 
use app\modules\fi\models\Persons;
use app\modules\fi\models\Rooms;
use app\modules\fi\models\Cities;
use app\modules\fi\models\StudentLevel;
use app\modules\fi\models\StudentOtherInfo;
use yii\helpers\Html; 
use app\modules\rbac\models\User; //pou tou kreye user depi ou ajoute/modifye moun
use app\modules\rbac\models\form\Signup;
/**
 * MigrationController implements the CRUD actions for Migration model.
 */
class MigrationController extends Controller
{
    
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Migration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SrcMigration();
        $model = new Migration(); 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(isset($_POST['btnSave'])){
           $model->file = UploadedFile::getInstance($model,'file'); 
           $model->file->saveAs(Yii::getAlias('documents/reportcard/test.csv'));
          // echo json_encode($file_name);
           
           
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
        ]);
    }
    /*
    public function actionUpload(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $model = new Migration();
        if(isset($_POST['btnSave'])){
           $model->file = UploadedFile::getInstance($model,'file_name'); 
           $model->file->saveAs(Yii::getAlias('documents/reportcard/test.csv'));
          // echo json_encode($file_name);
           
           
        }
        
        return $this->renderAjax('index', [
                'model' => $model,
            ]);
        
        
        
    }
     * 
     */
    
    
    public function actionUpload(){
        $fileName = 'file';
        $uploadPath = 'uploads';
        
        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
           // print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database
                    $model = new Migration(); 
                    $model->file_name = $file->name;
                    $model->is_migrate = 0; 
                    $model->is_delete = 0; 
                    $model->date_upload = date('Y-m-d h:m:s');
                    $model->migrate_by = Yii::$app->user->identity->username;
                    $model->save();
                    
                echo \yii\helpers\Json::encode($file);
            }
    }else{
        return $this->render('upload'); 
    }

    return false;
    }
    
    public function actionGetFileInfo(){
       
       $file_info = Migration::find()->where(['is_migrate'=>0])->all();
       $i=1; 
       echo "<h2>".Yii::t('app','Click on a file name to start the migration process.')."</h2>";
       echo "<table class='table table-striped table-bordered table-hover dataTables-example'>"
       . "<thead>"
           . "<tr>"
               . "<th>".Yii::t('app','#')."</th>"
               . "<th>".Yii::t('app','File name')."</th>"
               . "<th>".Yii::t('app','Date upload')."</th>"
               . "<th>".Yii::t('app','Upload by')."</th>"
               . "<th></th>"
           . "</tr>"
       . "</thead>"
               . "<tbody>"; 
               foreach($file_info as $f){
                   
                 echo "<tr>"
                   . "<td>".$i."</td>"
                   . "<td><a href='migrate?id=".$f->id."'>".$f->file_name."</a></td>"
                   . "<td>".Yii::$app->formatter->asDatetime($f->date_upload)."</td>"
                   . "<td>".$f->migrate_by."</td>"
                   . "<td class='efase'>".
                         Html::a('<i class="fa fa-trash"></i>', ["delete?id=$f->id"], ['class' => '','title'=>Yii::t('app','Delete file'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this file ? By deleting this file you will remove it phycally on the server !'),
                'method' => 'post',
                ]
            ])
                    . "</td>"
                   . "</tr>";
                 $i++; 
               }
               
        echo  "</tbody>"
       . "</table>";
       
       // echo Json::encode($file_info); 
    }
    
    public function actionMigrate($id){
       ini_set('max_execution_time', 3000);
        $is_save = FALSE;
        $model = new Migration();
        // Cherche le tableur dans la base 
        $file_name = Migration::findOne($id)->file_name;
        $modelMigration = Migration::findOne($id);
        $inputFile = 'uploads/'.$file_name; 
        // Recupere les elements du tableur et transforme les en objet
        try{
            $inputFileType = \PHPExcel_IOFactory::identify($inputFile); 
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPhpExcel = $objReader->load($inputFile); 
        } catch (Exception $e){
            die('Error');
        }
        // Demarrer la migreation de donnees
        if(isset($_POST['btnMigrate'])){
            
            if(isset($_POST['migrate_for_who'])){
                $migrate_for_who = $_POST['migrate_for_who'];
                $totalLigne = $_POST['totalLigne']; 
                // Pour migrer les employées 
                if($migrate_for_who==0){
                        // ON itilialise la transaction 
                        $dbTrans = Yii::$app->db->beginTransaction(); 
                    for($i=1;$i<$totalLigne;$i++){
                        if($_POST['last_name'.$i]){
                            //Recupere les information de la grille d'importation 
                            $last_name = $_POST['last_name'.$i];
                            $first_name = $_POST['first_name'.$i];
                            if(isset($_GET['sex'.$i])){
                                $gender = $_POST['sex'.$i];
                            }else{
                                $gender = NULL;
                            }
                            
                            $person = new NewPersons();
                            $person->last_name = $last_name; 
                            $person->first_name = $first_name; 
                            $person->gender = $gender; 
                            $person->active = 2; 
                            $person->is_student = 0; 
                            $person->create_by =  "Migration Tool";
                            // Forme le code si la fonction code automatique est active
                            $automatic_code = infoGeneralConfig('automatic_code');
                            if($automatic_code==1){
                               $cf='';
                               $cl='';
                               $explode_firstname = explode(" ",substr($person->first_name,0));
	            
                                if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
                                 { 
                                    $cf = strtoupper(  substr(strtr($explode_firstname[0],pa_daksan()), 0,1).substr(strtr($explode_firstname[1],pa_daksan()), 0,1));

                                 }
                                else
                                 {  
                                    $cf = strtoupper( substr(strtr($explode_firstname[0],pa_daksan()), 0,2) );
                                }
	
				$explode_lastname = explode(" ",substr($person->last_name,0));
	            
                                if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                 { 
                                    $cl = strtoupper(  substr(strtr($explode_lastname[0],pa_daksan() ), 0,1).substr(strtr($explode_lastname[1],pa_daksan() ), 0,1) );

                                 }
                                else
                                 {  
                                    $cl = strtoupper( substr(strtr( $explode_lastname[0],pa_daksan() ), 0,2)  );
                                 }
                            }
                            
                            if($person->save()){
                                $code_ = $cf.$cl.$person->id;
				$person->id_number = $code_;
                                
                                //Creation de l'utilisateur 
                                $explode_lastname=explode(" ",substr($person->last_name,0));
            
				if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                    $username = strtr( strtolower( $explode_lastname[0]), pa_daksan() ).'_'.strtr( strtolower( $explode_lastname[1]), pa_daksan() ).$person->id;
                                else
                                    $username = strtr(strtolower( $explode_lastname[0]), pa_daksan() ).$person->id;
                                $password = "password";
                                $user = new Signup();
                                $user->username = $username;
                                $user->person_id = $person->id;
                                $user->is_parent = 0;
                                $user->password = $password;
                                $user->signup_from_person();
                                $person->save();
                                
                            }
                        }
                    }
                    // Cleanup the process : mark file as delete and migrate in the database
                    $modelMigration->is_migrate = 1; 
                    $modelMigration->is_delete = 1; 
                    $modelMigration->type_migration = Yii::t('app','Employees');
                    $modelMigration->date_migrate = date('Y-m-d h:m:s');
                    $modelMigration->save(); 
                    
                    $file_to_delete = '/uploads/'.$file_name;
                    // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
                    shell_exec('rm  "'. getcwd().$file_to_delete.'"');
                    $is_save = TRUE; 
                    if(!$is_save){
                        /**
                     * Start flash 
                     */
                    Yii::$app->getSession()->setFlash('danger', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' => 36000,
                        'icon' => 'glyphicon glyphicon-remove-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode( Yii::t('app','Somethings goes wrong during migration process !') ),
                        'title' => Html::encode(Yii::t('app','Error') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
                    /*
                     * End Flash 
                     */
                        $dbTrans->rollback();
                        return $this->redirect("migrate?id=$id");
                    }else{
                        $dbTrans->commit();
                        return  $this->redirect("upload");
                    }
                }elseif ($migrate_for_who==1) { // Student migration 
                    
                    if(isset($_POST['program'])){
                        $program = $_POST['program'];
                    }
                    if(isset($_POST['level'])){
                        $level = $_POST['level'];
                    }
                    if(isset($_POST['shift'])){
                        $shift = $_POST['shift'];
                    }
                    // ON itilialise la transaction 
                        $dbTrans = Yii::$app->db->beginTransaction();
                        for($i=1;$i<$totalLigne;$i++){
                            if($_POST['last_name'.$i]){
                                //Recupere les information de la grille d'importation 
                            $last_name = $_POST['last_name'.$i];
                            $first_name = $_POST['first_name'.$i];
                            if(isset($_POST['sex'.$i])){
                                $gender = $_POST['sex'.$i];
                            }else{
                                $gender = NULL;
                            }
                            $person = new NewPersons();
                            $person->last_name = $last_name; 
                            $person->first_name = $first_name; 
                            $person->gender = $gender; 
                            $person->active = 2; 
                            $person->is_student = 1; 
                            $person->create_by =  "Migration Tool";
                            // Forme le code si la fonction code automatique est active
                            $automatic_code = infoGeneralConfig('automatic_code');
                            if($automatic_code==1){
                               $cf='';
                               $cl='';
                               $explode_firstname = explode(" ",substr($person->first_name,0));
	            
                                if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
                                 { 
                                    $cf = strtoupper(  substr(strtr($explode_firstname[0],pa_daksan()), 0,1).substr(strtr($explode_firstname[1],pa_daksan()), 0,1));

                                 }
                                else
                                 {  
                                    $cf = strtoupper( substr(strtr($explode_firstname[0],pa_daksan()), 0,2) );
                                }
	
				$explode_lastname = explode(" ",substr($person->last_name,0));
	            
                                if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                 { 
                                    $cl = strtoupper(  substr(strtr($explode_lastname[0],pa_daksan() ), 0,1).substr(strtr($explode_lastname[1],pa_daksan() ), 0,1) );

                                 }
                                else
                                 {  
                                    $cl = strtoupper( substr(strtr( $explode_lastname[0],pa_daksan() ), 0,2)  );
                                 }
                            }
                            if($person->save()){
                                $code_ = $cf.$cl.$person->id;
				$person->id_number = $code_;
                                
                                //Creation de l'utilisateur 
                                $explode_lastname=explode(" ",substr($person->last_name,0));
            
				if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                    $username = strtr( strtolower( $explode_lastname[0]), pa_daksan() ).'_'.strtr( strtolower( $explode_lastname[1]), pa_daksan() ).$person->id;
                                else
                                    $username = strtr(strtolower( $explode_lastname[0]), pa_daksan() ).$person->id;
                                
                                $password = "password";
                                $user = new Signup();
                                $user->username = $username;
                                $user->person_id = $person->id;
                                $user->is_parent = 0;
                                $user->password = $password;
                                $user->signup_from_person();
                                //Ajoute l'etudiant dans un level
                                $modelStudentLevel= new StudentLevel;
                                $modelStudentLevel->student_id = $person->id;
                                $modelStudentLevel->level = $level; 
                                $modelStudentLevel->status = 0; 
                                $modelStudentLevel->save(); 
                                // Ajouter l'etudiant dans un programme
                                $modelStudentOtherInfo = new StudentOtherInfo();
                                $modelStudentOtherInfo->student = $person->id; 
                                $modelStudentOtherInfo->apply_for_program = $program; 
                                $modelStudentOtherInfo->apply_shift = $shift;
                                $modelStudentOtherInfo->save(); 
                                // Attribuer un droit guest a l'etudiant cree
                                $modelNewUser = new User;
                                $itilizate = $modelNewUser->findByPersonId($person->id);
				$command = Yii::$app->db->createCommand();					                                    
                                $command->insert('auth_assignment', [
                                'item_name'=>"guest",
                                'user_id'=>$itilizate->id,
                                  ])->execute();
                                $person->save();
                                
                            }
                            
                            }
                        }
                        // Cleanup the process : mark file as delete and migrate in the database
                        $modelMigration->is_migrate = 1; 
                        $modelMigration->is_delete = 1; 
                        $modelMigration->type_migration = Yii::t('app','Student');
                        $modelMigration->date_migrate = date('Y-m-d h:m:s');
                        $modelMigration->save(); 
                        $file_to_delete = '/uploads/'.$file_name;
                        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
                        shell_exec('rm  "'. getcwd().$file_to_delete.'"');
                        $is_save = TRUE;
                        
                        if(!$is_save){
                        /**
                     * Start flash 
                     */
                    Yii::$app->getSession()->setFlash('danger', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' => 36000,
                        'icon' => 'glyphicon glyphicon-remove-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode( Yii::t('app','Somethings goes wrong during migration process !') ),
                        'title' => Html::encode(Yii::t('app','Error') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
                    /*
                     * End Flash 
                     */
                        $dbTrans->rollback();
                        return $this->redirect("migrate?id=$id");
                    }else{
                        $dbTrans->commit();
                        return  $this->redirect("upload");
                    }
                    
                }
                
            }
            
            
            
        }
        
        return $this->render('migrate',
                ['objPhpExcel'=>$objPhpExcel,
                 'fileName'=>$file_name,
                 'model'=>$model]); 
    }

    /**
     * Displays a single Migration model.
     * @param integer $id
     * @return mixed
     */
    /*
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
     * 
     */

    /**
     * Creates a new Migration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
    public function actionCreate()
    {
        $model = new Migration();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
     * 
     */

    /**
     * Updates an existing Migration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
     * 
     */

    /**
     * Deletes an existing Migration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $file_name = $this->findModel($id)->file_name; 
        $file_to_delete = '/uploads/'.$file_name;
        // Supprimer le fichier CSV (Marche sur MACOS et Linux seulement... mais la non supression du fichier n'empeche pas a l'operation de continuer) 
        shell_exec('rm  "'. getcwd().$file_to_delete.'"');
        $this->findModel($id)->delete();
        
        return $this->redirect(['upload']);
    }

    /**
     * Finds the Migration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Migration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Migration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
  public function actionTest(){
      return $this->render('test');
  }
    
}
