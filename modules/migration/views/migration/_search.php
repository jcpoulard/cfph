<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\migration\models\SrcMigration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="migration-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'file_name') ?>

    <?= $form->field($model, 'is_migrate') ?>

    <?= $form->field($model, 'is_delete') ?>

    <?= $form->field($model, 'migrate_by') ?>

    <?php // echo $form->field($model, 'type_migration') ?>

    <?php // echo $form->field($model, 'date_upload') ?>

    <?php // echo $form->field($model, 'date_migrate') ?>

    <?php // echo $form->field($model, 'date_delete') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
