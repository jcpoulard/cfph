<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\fi\models\SrcPersons;
use kartik\select2\Select2;
use app\modules\fi\models\SrcProgram;
use yii\helpers\ArrayHelper;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\Rooms;

// Get the sheet 
$sheet = $objPhpExcel->getSheet(0); 
$highestRow = $sheet->getHighestRow(); 
$highestColumn = $sheet->getHighestColumn();
// Tableau pour determiner les sexes de maniere intelligente
$female_array = ['Female','Feminin','Fi','Fanm','Féminin','F','f','1','female','fille','fanm','mujer','femme','chica','Femme']; 
$male_array = ['Male','','Masculin','Gason','Garçon','M','m','male','masculin','gason','garçon','Hombre','hombre','homme','Homme','h','H'];

?>



<div class="row">
  
    <div class="col-lg-1">
        
    </div>
    <?php if(!in_array($highestColumn,['A','B','C'])) { ?>
    
    <span><i class="fa fa-magic"></i> <?= Yii::t('app','File format incorrect ! The migration is stoping, please make sure your Excel file have at least 2 columns and at most 3 columns.'); ?></span>
    <br/>
    <a class="btn btn-lg btn-primary" href="upload?wh=mgr"><i class="fa fa-database"></i> <?= Yii::t('app','Return to upload page')?></a>
    <?php }  else { ?>
    
    <div class="col-lg-10">
        <?php 
          $form = ActiveForm::begin(['options'=> ['enctype'=> 'multipart/form-data']]);
            
        ?>
        
        <h2><i class="fa fa-magic"></i> <?= Yii::t('app','Start migration process for the file {fileName}',['fileName'=>$fileName]) ?></h2>
        <div class="row">
            <div class="col-lg-3">
                <?php
                    $list = [0 =>Yii::t('app','Employees'), 1 =>Yii::t('app','Students')];
                    echo $form->field($model, 'migrate_for_who')->radioList($list,['name'=>'migrate_for_who' ]);
                ?>
            </div>
            <div>
                <div class="col-lg-2 edityan">
                   <?php        
         
                   
                    echo $form->field($model, 'level')->widget(Select2::classname(), [
                           'data'=>loadLevels(),
                           'size' => Select2::MEDIUM,
                           'theme' => Select2::THEME_CLASSIC,
                           'language'=>'fr',
                           'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),'name'=>'level'],
                           'pluginOptions'=>[
                                 'allowclear'=>true,
                             ],
			]);
                ?> 
                </div>
                <div class="col-lg-2 edityan">
                   <?php
                        echo $form->field($model, 'program')->widget(Select2::classname(), [

                           'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                           'size' => Select2::MEDIUM,
                           'theme' => Select2::THEME_CLASSIC,
                           'language'=>'fr',
                           'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                       'name'=>'program',

                                 ],
                           'pluginOptions'=>[
                                 'allowclear'=>true,
                             ],
                       ]);
                    ?>
                </div>
                <div class="col-lg-2 edityan">
                   <?php
                        echo $form->field($model, 'shift')->widget(Select2::classname(), [

                           'data'=>ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ),
                           'size' => Select2::MEDIUM,
                           'theme' => Select2::THEME_CLASSIC,
                           'language'=>'fr',
                           'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                       'name'=>'shift',

                                 ],
                           'pluginOptions'=>[
                                 'allowclear'=>true,
                             ],
                       ]);
                    ?>
                </div>
                
            </div>
        </div>
        
        <div class="row" id="boutonAksyon">
            <div class="col-lg-3"></div>
            <div class="col-lg-2">
                <button class="btn btn-success" name="btnMigrate" type="submit"><?= Yii::t('app','Migrate')?></button>
            </div>
            <div class="col-lg-2">
                <a class="btn btn-primary" href="upload" ><?= Yii::t('app','Back to upload')?></a>
            </div>
            <div class="col-lg-2">
                <a class="btn btn-warning"><?= Yii::t('app','View list of files')?></a>
            </div>
            <div class="col-lg-3"></div>
        </div>
        <p>
            
        </p>
        <div class="row">
        <table class="table table-striped table-bordered table-hover dataTables-example"> <!-- table table-striped table-bordered table-hover dataTables-example -->
            <thead>
                <tr>
                    <th><?= Yii::t('app','#'); ?></th>
                    <th><?= Yii::t('app','Last name'); ?></th>
                    <th><?= Yii::t('app','First name'); ?></th>
                    <?php if(!in_array($highestColumn,['C'])){ } else{ ?>
                    <th><?= Yii::t('app','Gender'); ?></th>
                    <?php }?>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $i = 1; 
                    for($row = 1; $row <= $highestRow; $row++){
                        $rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
                        if($row==1){
                            continue; 
                        }
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td class="inputfield"><input name="last_name<?= $i; ?>" value="<?= $rowData[0][0]; ?>"/></td>
                    <td class="inputfield"><input name="first_name<?= $i; ?>" value="<?= $rowData[0][1]; ?>"/></td>
                    <?php if(!in_array($highestColumn,['C'])) { } else{ ?>
                    
                    <?php
                            if(in_array($rowData[0][2],$male_array)){
                                $sex = 0;
                        ?>
                    <td class="inputfield">
                        <select name="sex<?= $i; ?>">
                            <option value="<?= $sex; ?>"><?= Yii::t('app','Male'); ?></option>
                            <option value="1"><?= Yii::t('app','Female');?></option>
                        </select>
                    </td>  
                    
                            <?php } elseif(in_array($rowData[0][2],$female_array)){ $sex = 1; ?>
                    <td class="inputfield">
                        <select name="sex<?= $i; ?>">
                            <option value="<?= $sex; ?>"><?= Yii::t('app','Female'); ?></option>
                            <option value="0"><?= Yii::t('app','Male');?></option>
                        </select>
                    </td>  
                            <?php } ?>
                    
                    <?php }?>
                </tr>
                <?php 
                    
                    $i++;     
                    }
                ?>
            </tbody>
        </table>
        </div>
        <input type="hidden" name="totalLigne" value="<?= $i; ?>">
        <?php ActiveForm::end(); ?>
    </div>
    <?php } ?>
    <div class="col-lg-1">
        
    </div>
    
</div>
<p></p><p></p><p></p>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;

$script = <<< JS
      $('#migration-migrate_for_who').change(function(){
        
        var valeur = $('#migration-migrate_for_who').find(":checked").val();
        
        if(valeur==0){
                $('.edityan').hide();
                $('#boutonAksyon').show();
            }else{
                $('.edityan').show();
                $('#boutonAksyon').hide();
            }
        
        
   });
        
   $(document).ready(function(){
        var valeur = $('#migration-migrate_for_who').find(":checked").val();
        
        if(valeur==0 || valeur == null){
        
                $('.edityan').hide();
                $('#boutonAksyon').show();
            }else{
                $('.edityan').show();
                $('#boutonAksyon').hide();
            }
        $('.field-migration-program').hide(); 
        $('.field-migration-shift').hide(); 
        $('.field-migration-room').hide(); 
        $('#boutonAksyon').hide();
       // $('.edityan').hide();
        $('#migration-level').change(function(){
                $('.field-migration-program').show();
            });
        
        $('#migration-program').change(function(){
                $(".field-migration-shift").show();
                
            });
        
        $('#migration-shift').change(function(){
                //$(".field-migration-shift").show();
                $('#boutonAksyon').show();
            });
   });      
JS;
$this->registerJs($script);
?>
