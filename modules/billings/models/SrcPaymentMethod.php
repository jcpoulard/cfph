<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\PaymentMethod;

/**
 * SrcPaymentMethod represents the model behind the search form about `app\modules\billings\models\PaymentMethod`.
 */
class SrcPaymentMethod extends PaymentMethod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_default'], 'integer'],
            [['method_name', 'description', 'date_create', 'date_update', 'create_by', 'update_by'], 'safe'],
            [['method_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcPaymentMethod::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_default' => $this->is_default,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'method_name', $this->method_name]);

        return $dataProvider;
    }
    
    
    
 
public function isAlreadyDefined()
	  {
	  	$sql='SELECT id FROM payment_method WHERE is_default=1';

	
				  $is_there = Yii::$app->db->createCommand($sql)->queryAll();
				  
            if($is_there!=null)
               return $is_there;
            else
               return null;
           
                        
           
           
	  	}   
    
    
    
     public function getIsDefault(){
    switch ($this->is_default){
        case 0:
            return Yii::t('app','No');
            break;
        case 1:
            return Yii::t('app','Yes');
            break;
       
    }
   }
    
    
    
    
    
    
    
    
}
