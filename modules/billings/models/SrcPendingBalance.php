<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\PendingBalance;

/**
 * SrcPendingBalance represents the model behind the search form about `app\modules\billings\models\PendingBalance`.
 */
class SrcPendingBalance extends PendingBalance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'is_paid', 'academic_year'], 'integer'],
            [['balance'], 'number'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PendingBalance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'balance' => $this->balance,
            'is_paid' => $this->is_paid,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
        ]);

        return $dataProvider;
    }
}
