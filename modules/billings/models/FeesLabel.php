<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "fees_label".
 *
 * @property integer $id
 * @property string $fee_label
 * @property integer $status
 */
class FeesLabel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fees_label';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fee_label'], 'required'],
            [['fee_label'], 'unique'],
            [['status'], 'integer'],
            [['fee_label'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fee_label' => Yii::t('app', 'Fee Description'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
