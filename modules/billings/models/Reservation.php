<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\billings\models\PaymentMethod;

/**
 * This is the model class for table "reservation".
 *
 * @property integer $id
 * @property integer $postulant_student
 * @property integer $is_student
 * @property double $amount
 * @property integer $payment_method
 * @property string $payment_date
 * @property integer $already_checked
 * @property string $comments
 * @property integer $academic_year
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Billings[] $billings
 * @property Academicperiods $academicYear
 * @property PaymentMethod $paymentMethod
 */
class Reservation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reservation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant_student', 'is_student', 'amount', 'payment_method', 'payment_date', 'comments', 'academic_year', 'date_created', 'date_updated', 'create_by', 'update_by'], 'required'],
            [['postulant_student', 'is_student', 'payment_method', 'already_checked', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['payment_date', 'date_created', 'date_updated'], 'safe'],
            [['comments'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['payment_method'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::className(), 'targetAttribute' => ['payment_method' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant_student' => Yii::t('app', 'Postulant Student'),
            'is_student' => Yii::t('app', 'Is Student'),
            'amount' => Yii::t('app', 'Amount'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'already_checked' => Yii::t('app', 'Already Checked'),
            'comments' => Yii::t('app', 'Comments'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillings()
    {
        return $this->hasMany(Billings::className(), ['reservation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['id' => 'payment_method']);
    }
}
