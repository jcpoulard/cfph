<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\fi\models\SrcAcademicPeriods;


/**
 * SrcFees represents the model behind the search form about `app\modules\billings\models\Fees`.
 */
class SrcFees extends Fees
{
     public $globalSearch;
     
    public $id_fee;
    public $fee_name;
	public $fee_label;
	public $program_lname; 
	public $devise_lname;
	public $devise_symbol; 
	public $period_academic_lname; 
	public $amount_pay;
	

     
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'program', 'academic_period','level', 'fee', 'devise', 'checked'], 'integer'],
            [['amount'], 'number'],
            [['date_limit_payment', 'globalSearch', 'description', 'date_create', 'date_update', 'create_by', 'update_by'], 'safe'],
            [['program','fee','level', 'academic_period'], 'unique', 'targetAttribute' => ['program','fee','level', 'academic_period'],  'message' => Yii::t('app', 'This combinaison "program - fee - level - academic_period" has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcFees::find()->orderBy(['program'=>SORT_ASC,'level'=>SORT_ASC,'date_limit_payment'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'program' => $this->program,
            'academic_period' => $this->academic_period,
            'level'=>  $this->level,
            'fee' => $this->fee,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'date_limit_payment' => $this->date_limit_payment,
            'checked' => $this->checked,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
  public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;
      $acad = Yii::$app->session['currentId_academic_year'];

    $query = SrcFees::find()->where('academic_period='.$acad)->orderBy(['program'=>SORT_ASC,'level'=>SORT_ASC,'date_limit_payment'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }
     
    $query->joinWith(['fee0', 'program0','devise0']);
    $query->orFilterWhere(['like', 'program.label', $this->globalSearch])
            ->orFilterWhere(['like', 'fees_label.fee_label', $this->globalSearch])
            ->orFilterWhere(['like', 'devises.devise_name', $this->globalSearch])
            ->orFilterWhere(['like', 'date_limit_payment', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
  
     
 

public function searchByProgram($program,$acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('academicPeriod','level0', 'fee0');
		
		$criteria->alias='f';
		
		$criteria->condition='fee0.status=1 AND level='.$level.' AND academic_period ='.$acad ;

		$criteria->compare('id',$this->id);
		$criteria->compare('level',$this->level);
		$criteria->compare('fee',$this->fee);
		$criteria->compare('academic_period',$this->academic_period);
		$criteria->compare('fee0.fee_label',$this->fee_name,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
                $criteria->compare('date_limit_payment',$this->date_limit_payment,true);
                $criteria->compare('f.checked',$this->checked);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('level0.level_name', $this->level_lname, true); 
		$criteria->compare('academicPeriod.name_period', $this->period_academic_lname, true); 
		
		$criteria->order='level0.level_name ASC, date_limit_payment ASC' ;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        ),
		));
	}

         
/*   */       
        
public function checkDateLimitPayment($current_date,$acad)//date_du_jour >= date_limt_payment AND checked=0
        {
        	$query = SrcFees::find()                             
  	              ->select(['fees.id', 'fees.level','fees.program','devises.devise_symbol','fees_label.fee_label', 'fees.amount','fees.date_limit_payment'])
  	              ->distinct(['true'])
  	              ->joinWith(['fee0','devise0','billings','academicPeriod','program0'])
  	              ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
  	              ->where(['fees_label.status'=>1])
  	              ->andWhere(['<=','date_limit_payment',$current_date])
  	              ->andWhere(['fees.checked'=>0])
  	              ->andWhere(['academic_period'=>$acad]);
  	              
  	              
  	      //1-yon fre ki nan billing balans >0 epi fee_totally_paid=0,         
  	       
  	      
  	     $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 500,
             ],
           ]);
  	     	
  	 return $dataProvider;    
  	 
		         	
        }
      
    
      public function checkDateLimitPaymentByProgram($current_date, $program,$acad)//date_du_jour >= date_limt_payment 
        {
        	$criteria=new CDbCriteria;
		$criteria->with= array('academicPeriod','level0', 'fee0');
		
		$criteria->alias='f';

$siges_structure = infoGeneralConfig('siges_structure_session');
	     
        if($siges_structure==1)
	       $criteria->condition = 'fee0.status=1 AND level=:level AND (date_limit_payment<:cDate OR date_limit_payment=:cDate)  AND academicPeriod.year=:acad';
	    elseif($siges_structure==0)
	       $criteria->condition = 'fee0.status=1 AND level=:level AND (date_limit_payment<:cDate OR date_limit_payment=:cDate)  AND academic_period=:acad';
	       
		$criteria->params = array( ':level'=>$level,':cDate'=>$current_date,':acad'=>$acad );
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('level',$this->level,true);
		$criteria->compare('fee',$this->fee,true);
		$criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('fee0.fee_label',$this->fee_name,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
                $criteria->compare('date_limit_payment',$this->date_limit_payment,true);
                $criteria->compare('f.checked',$this->checked,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('level0.level_name', $this->level_lname, true); 
		$criteria->compare('academicPeriod.name_period', $this->period_academic_lname, true); 

		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
			'pagination'=>array(
        			'pageSize'=>500,
    			),  
                        
		));

        	
        }
      
 
public function getFeeStatus($fee_period_id)
 {
 	$sql='SELECT fl.status FROM fees f inner join fees_label fl on(f.fee=fl.id) WHERE f.id ='.$fee_period_id;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
           foreach($result as $r)
            return $r["status"];
            
 	}
 	
    
//return 		
public function getFeeInfoByProgramAcademicperiodChecked($program,$level, $acad, $condition_checked)
	{   	    
       $sql='SELECT f.id, f.level, f.fee, f.academic_period, f.date_limit_payment, f.checked, f.amount, f.devise, f.description, f.date_create, f.date_update, f.create_by, f.update_by  FROM fees f INNER JOIN fees_label fl ON(f.fee = fl.id) WHERE fl.status=1 AND level ='.$level.' AND academic_period='.$acad.' AND f.id NOT IN( SELECT fee_period FROM billings b INNER JOIN fees f1 ON(f1.id=b.fee_period) INNER JOIN fees_label fl1 ON(f1.fee = fl1.id) WHERE  fl1.status=1 AND  f1.level='.$level.' AND f1.academic_period='.$acad.' ) ORDER BY date_limit_payment ASC';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
            return $result;
            
 }


      
public function searchFeesToExempt($student,$program,$acad) 
  {
  	$level= 0;
  	
  	if($student=='')
  	   $student=0;
  	else 
  	    $level= getLevelByStudentId($student);
  	
  	if($program=='')
  	   $program=0;
  	     	
  	     $query = SrcFees::find()                             
  	              ->select(['fees.id', 'devises.devise_symbol','fees_label.fee_label', 'fees.amount','fees.date_limit_payment'])
  	              ->distinct(['true'])
  	              ->joinWith(['fee0','devise0','billings'])
  	              ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
  	              ->where(['billings.fee_totally_paid'=>0])
    // ->orWhere('billings.balance >0')
  	              ->andWhere(['billings.academic_year'=>$acad])
  	              ->andWhere(['fees.program'=>$program])
  	              ->andWhere(['fees.level'=>$level])
  	              ->andWhere(['academic_period'=>$acad])
  	              ->andWhere(['billings.student'=>$student]);
  	             // ->andWhere(['billings.academic_year'=>$acad]);
  	             // ->orWhere('billings.balance >0 AND billings.fee_totally_paid=0 AND billings.academic_year='.$acad);
  	              
  	      //1-yon fre ki nan billing balans >0 epi fee_totally_paid=0,         
  	       
  	      
  	     $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100,
             ],
           ]);
  	     	
  	 return $dataProvider;     	 
  }

public function searchPastFeesToExempt($student,$program,$level,$acad) 
  {
  	
  	if($program=='')
  	   $program=0;
  	     	
  	     $query = SrcFees::find()                             
  	              ->select(['fees.id', 'devises.devise_symbol','fees_label.fee_label', 'fees.amount','fees.date_limit_payment'])
  	              ->distinct(['true'])
  	              ->joinWith(['fee0','devise0','billings'])
  	              ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
  	              ->where(['billings.fee_totally_paid'=>0])
    // ->orWhere('billings.balance >0')
  	              ->andWhere(['billings.academic_year'=>$acad])
  	              ->andWhere(['fees.program'=>$program])
  	              ->andWhere(['fees.level'=>$level])
  	              ->andWhere(['academic_period'=>$acad])
  	              ->andWhere(['billings.student'=>$student]);
  	             // ->andWhere(['billings.academic_year'=>$acad]);
  	             // ->orWhere('billings.balance >0 AND billings.fee_totally_paid=0 AND billings.academic_year='.$acad);
  	              
  	      //1-yon fre ki nan billing balans >0 epi fee_totally_paid=0,         
  	       
  	      
  	     $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100,
             ],
           ]);
  	     	
  	 return $dataProvider;     	 
  }


 public function getFeeAmount($student,$acad,$amount)
  {
  	$modelAcad = new SrcAcademicPeriods();
  	$previous_year= $modelAcad->getPreviousAcademicYear($acad);
  	
	  if($amount==0)
	    {
	    	 //gad si elev la gen balans ane pase ki poko peye
				$modelPendingBal=SrcPendingBalance::find()
				                                    ->select(['id', 'balance'])
				                                    ->where('student='.$student.' AND is_paid=0 AND academic_year='.$previous_year)
				                                    ->All();
				//si gen pending, ajoutel nan lis apeye a			
				if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
				 {
				   foreach($modelPendingBal as $bal)
					 {	
					 	return $bal->balance;
					 }
				 }
			   else
			     return 0;	
	      }
	   else
	     return $amount;
	
	
	}

 public function getTotalAmountPayOnFee($stud,$fee_id)
      {   
      	 $acad=Yii::$app->session['currentId_academic_year']; 
      	 
      	
      	     $tot_amount =0;
      	     
      	     //al nan billings fe total kob li peye pou fe sa nan acad sa
      	        $bill_info_= Yii::$app->db->createCommand('SELECT SUM(amount_pay) as total_amount FROM billings WHERE  student='.$stud.' AND fee_period='.$fee_id.' AND academic_year='.$acad)->queryAll();
      	     
      	        if($bill_info_!=null)
			      { 
			      	foreach($bill_info_ as $bill)
			      	 {
			      	   if($bill['total_amount']!=null)
			      	     $tot_amount = $bill['total_amount'];
			           else
	      	            $tot_amount =0;
			      	
			      	 }
			      	 
			      }
                else
	      	      $tot_amount = 0;
	      	       	      	       
      	      
			    
			    
			     return $tot_amount;
      	
      	}    

public function getAmountPayOnFee($student,$fee_id)
      {    
      	 $currency_symbol = Yii::$app->session['currencySymbol'];
      	
      	    // $tot_amount =  $currency_symbol." ".numberAccountingFormat( SrcFees::getTotalAmountPayOnFee($stud,$fee_id) );
      	     $modelFee___ = new SrcFees;
      	     $modelFee = SrcFees::findOne($fee_id);
      	     $tot_amount =  $modelFee->devise0->devise_symbol." ".numberAccountingFormat( $modelFee___->getTotalAmountPayOnFee($student,$fee_id) );
      	     
      	     return $tot_amount;
      	
      	}   


    public function getAmount(){
           
           
            //return $currency_symbol.' '.numberAccountingFormat($this->amount); //retirel le w aktive devise nan form lan
            return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
        }
 
        public function getDateLimit(){
            if($this->date_limit_payment=='0000-00-00')
                return Yii::t('app','No date payment limit');
            else
                return $this->date_limit_payment;
        }
        
        public function getFeeName(){
        	   //return Yii::t('app',$this->fee0->fee_label).' '.$this->program0->label.' '.$currency_symbol.' '.numberAccountingFormat($this->amount);
            return Yii::t('app',$this->fee0->fee_label).' ('.$this->program0->short_name.') '.$this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
            
        }
        
        public function getSimpleFeeName(){
            return Yii::t('app',$this->fee0->fee_label).' '.$this->program0->label;
        }


 
public function getLevel(){
           
           if($this->level==null)
             return Yii::t('app','N/A');
           else
             return $this->level;
            
        }


	
//return TRUE if payment is already started and FALSE if not
public function isPaymentStarted($fee,$acad){
            $sql='SELECT id, student FROM billings b WHERE b.amount_pay<>0 AND b.fee_period ='.$fee.' AND b.academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
           if($result !=null)
              return true;
            else              
              return false;    
        }

//return 		
public function getFeeInfoByLevelAcademicperiodChecked($program, $acad, $condition_checked)
	{   	    
       $sql='SELECT f.id, f.level, f.fee, f.academic_period, f.date_limit_payment, f.checked, f.amount, f.devise, f.description, f.date_create, f.date_update, f.create_by, f.update_by  FROM fees f INNER JOIN fees_label fl ON(f.fee = fl.id) WHERE fl.status=1 AND program ='.$program.' AND academic_period='.$acad.' AND f.id NOT IN( SELECT fee_period FROM billings b INNER JOIN fees f1 ON(f1.id=b.fee_period) INNER JOIN fees_label fl1 ON(f1.fee = fl1.id) WHERE  fl1.status=1 AND  f1.program='.$program.' AND f1.academic_period='.$acad.' ) ORDER BY date_limit_payment ASC';
		  
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
            return $result;
            
 }
   
    
    
    
    
    
    
    
    
    
    
}
