<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\Payroll;
use app\modules\billings\models\SrcDevises;


use app\modules\fi\models\SrcPersons;

use kartik\mpdf\Pdf;

use kartik\widgets\Alert;

 

/**
 * SrcPayroll represents the model behind the search form about `app\modules\billings\models\Payroll`.
 */
class SrcPayroll extends Payroll
{
    public $full_name;
     public $first_name;
     public $last_name;
     public $person_id;
     
     public $amount;
     public $missing_hour;
     public $group;
	public $group_payroll;
	public $an_hour;
	public $number_of_hour;
	public $to_pay;
     public $frais_p;
	
	public $id_payroll;
	
	public $depensesItems;
     
       public $year;
     
      public $devise;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_payroll_set', 'id_payroll_set2', 'payroll_month', 'missing_hour'], 'integer'],
            [['payroll_date','first_name', 'cash_check', 'last_name','full_name', 'payment_date', 'cash_check', 'date_created', 'date_updated', 'created_by', 'updated_by'], 'safe'],
            [['taxe', 'gross_salary', 'net_salary', 'plus_value', 'loan_rate'], 'number'],
            
            [['id_payroll_set','payroll_month','payment_date'], 'unique', 'targetAttribute' => ['id_payroll_set','payroll_month','payment_date'],  'message' => Yii::t('app', 'This combinaison "payroll setting - payroll month - payment date" has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**/public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                      [
                        'person_id'=>Yii::t('app','Person'), 
                        'group'=> Yii::t('app','By Group'), 
                        'group_payroll'=> Yii::t('app','Group Payroll'),
                        'taxe'=>Yii::t('app','Include taxe'),
                        'missing_hour'=> Yii::t('app','Missing hour'),
                        'depensesItems'=> Yii::t('app','Depenses Items'),
                                 
                        ]
                    );
           
	}


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcPayroll::find()
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payroll_month'=>SORT_DESC,'payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_payroll_set' => $this->id_payroll_set,
            'id_payroll_set2' => $this->id_payroll_set2,
            'payroll_month' => $this->payroll_month,
            'payroll_date' => $this->payroll_date,
            'missing_hour' => $this->missing_hour,
            'taxe' => $this->taxe,
            'net_salary' => $this->net_salary,
            'plus_value' => $this->plus_value,
            'loan_rate'=> $this->loan_rate,
            'payment_date' => $this->payment_date,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'cash_check', $this->cash_check])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
    



    public function searchByMonthYear($month,$year)
    {
        if(in_array($month,[0,13,14]) )
         {
         	$query = SrcPayroll::find()
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('payroll_month='.$month.' AND YEAR(payroll_date)='.$year);
           }
        else
        {
        	$query = SrcPayroll::find()
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('MONTH(payroll_date)='.$month.' AND YEAR(payroll_date)='.$year);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

                return $dataProvider;
    }
    

public function searchByMonthYearForReport($month,$year)
    {
        $query = null;
        
       if( (! in_array($month,[0,13,14]) )||($month=='') )
        {    
            if( ($month=='') )
             {       
                  $query = SrcPayroll::find()
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('payroll_month=-1 ');
             }
            else   
             {  $query = SrcPayroll::find()->distinct(true)
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('payroll_month='.$month.' AND YEAR(payroll_date)='.$year);
             }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

                return $dataProvider;
    }
    
  
    
public function searchBonisYearForReport($month,$year)
    {
        $query = null;
        
           
            if( ($month==-1) )
             {       
                  $query = SrcPayroll::find()
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('payroll_month=-1 ');
             }
            elseif( $month==0)   
             {  $query = SrcPayroll::find()->distinct(true)
                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
		              ->orderBy(['payment_date'=>SORT_DESC,'persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC, ])
		              ->where('payroll_month=0 AND YEAR(payroll_date)='.$year.' AND id_payroll_set in(select id_payroll_set from payroll p where payroll_month in(1,2,3,4,5,6,7,8,9,10,11,12) and YEAR(payroll_date)='.$year.')');
             }
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

                return $dataProvider;
    }
 
    
    
public function search_($acad)
	{
		$query = SrcPayroll::find()
		              ->joinWith(['idPayrollSet'])
		              ->orderBy(['payroll_settings.id'=>SORT_ASC, 'payroll_month'=>SORT_ASC])
		              ->where(['payroll_settings.academic_year'=>$acad]);

        // add conditions that should always apply here

      return  $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
 
 
	}


public function searchByMonth($month_, $payroll_date, $acad)  //il fo cree colonne academic_year ds la table BILLINGS
	{
		// Warning: Please modify the following id to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('idPayrollSet');
		
		$criteria->join='left join payroll_settings ps on(ps.id=pl.id_payroll_set) left join persons p on(p.id=ps.person_id)';
		
		$criteria->alias='pl';
		$criteria->condition='pl.payroll_month='.$month_.' AND pl.payroll_date=\''.$payroll_date.'\' AND ps.old_new=1 AND  ps.old_new=1 AND idPayrollSet.academic_year='.$acad;
		
		$criteria->compare('pl.id',$this->id);
		$criteria->compare('id_payroll_set',$this->id_payroll_set);
		$criteria->compare('p.first_name',$this->first_name,true);
		$criteria->compare('p.last_name',$this->last_name,true);
		$criteria->compare('pl.fullName',$this->full_name,true);
		$criteria->compare('idPayrollSet.amount',$this->amount);
		$criteria->compare('payroll_month',$this->payroll_month);
		$criteria->compare('payroll_date',$this->payroll_date,true);
		$criteria->compare('idPayrollSet.number_of_hour',$this->number_of_hour);
		$criteria->compare('missing_hour',$this->missing_hour);
		$criteria->compare('taxe',$this->taxe,true);
		$criteria->compare('net_salary',$this->gross_salary);
		$criteria->compare('net_salary',$this->net_salary);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('cash_check',$this->cash_check,true);
		
		$criteria->order='idPayrollSet.id DESC, payroll_month ASC';

	    
		//$criteria->compare('id',$this->id_bill, true); 

		return new CActiveDataProvider($this, array(
			'pagination'=>[
        			'pageSize'=>10000000,
    			],
			'criteria'=>$criteria,
		));
	}
	


public function searchByPersonId($person_id, $acad)  
	{
		$query = SrcPayroll::find()
		               ->joinWith(['idPayrollSet','idPayrollSet.person'])
		               ->orderBy(['payroll_month'=>SORT_ASC,'payroll_settings.id'=>SORT_DESC])
		               ->where('person_id='.$person_id.' AND academic_year='.$acad);

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);


	}

public function searchForBonis($person_id, $acad)  
	{
		$query = SrcPayroll::find()
		               ->joinWith(['idPayrollSet','idPayrollSet.person'])
		               ->orderBy(['payroll_month'=>SORT_ASC,'payroll_settings.id'=>SORT_DESC])
		               ->where('payroll_month NOT IN(0,13,14) AND payroll_settings.person_id='.$person_id.' AND payroll_settings.academic_year='.$acad);

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);


	}
	


public function searchByMonthPersonId($month_, $payroll_date, $person_id, $acad)  //il fo cree colonne academic_year ds la table BILLINGS
	{
		// Warning: Please modify the following id to remove attributes that
		// should not be searched.

		$query = SrcPayroll::find()
		                  ->joinWith(['idPayrollSet','idPayrollSet.person'])
		                  ->orderBy(['payroll_settings.id'=>SORT_DESC,'payroll_month'=>SORT_ASC])
		                  ->where('payroll.payroll_date=\''.$payroll_date.'\' AND payroll.payroll_month='.$month_.' AND payroll_settings.person_id='.$person_id.' AND payroll_settings.academic_year='.$acad);
		                  
		
		
			    
		//$criteria->compare('id',$this->id_bill, true); 

		return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
	}
	
    
    



    
public function isDone($month){
     
    	$acad=Yii::$app->session['currentId_academic_year']; 
    	
    	$query = SrcPayroll::find()
		                  ->joinWith(['idPayrollSet','idPayrollSet.person'])
		                  ->where('payroll_settings.academic_year='.$acad.' AND payroll.payroll_month='.$month.' AND MONTH(payroll.payroll_date)='.$month );
		                  
		            
    return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);   
   
    }
    
    
    
public function isDoneForOnes($month, $pers, $acad){
     
    	$query = SrcPayroll::find()
		                  ->joinWith(['idPayrollSet','idPayrollSet.person'])
		                  ->where('payroll_settings.academic_year='.$acad.' AND payroll.payroll_month='.$month.' AND MONTH(payroll.payroll_date)='.$month.' AND payroll_settings.person_id='.$pers );
			
			
			return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);    
   
    }

    
    
public function getInfoLastPayroll(){
	 
    $acad=Yii::$app->session['currentId_academic_year'];
    
	   $query = SrcPayroll::find()
		                  ->distinct('true')
		                  ->joinWith(['idPayrollSet','idPayrollSet.person'])
		                  ->orderBy(['payroll.payroll_month'=>SORT_DESC,'payroll.payroll_date'=>SORT_DESC])
		                  ->select(['payroll.id', 'payroll.id_payroll_set', 'payroll.id_payroll_set2', 'payroll.missing_hour', 'payroll.payroll_month', 'payroll.payroll_date', 'payroll_settings.number_of_hour', 'payroll.taxe', 'payroll.gross_salary','payroll.net_salary','payroll.plus_value','payroll.loan_rate','payroll_settings.frais', 'payroll.payment_date', 'payroll.cash_check'])
                                  ->where(['payroll_settings.academic_year'=>$acad]);
			
           
    return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000,
    			],
        ]);    

	
}    
    
    


public function getInfoLastPayrollForOne($pers){
	           
	$acad=Yii::$app->session['currentId_academic_year'];
        
   $bill = new SrcPayroll();
          $result= $bill->find()
		                      ->joinWith(['idPayrollSet'])
		                      ->select(['payroll.id', 'payroll.id_payroll_set','payroll.id_payroll_set2','payroll.missing_hour', 'payroll.payroll_month', 'payroll.payroll_date', 'payroll_settings.number_of_hour', 'payroll.taxe', 'payroll.gross_salary','payroll.net_salary','payroll.plus_value','payroll.loan_rate','payroll_settings.frais', 'payroll.payment_date', 'payroll.cash_check',])
		                      ->distinct('true')
		                      ->orderBy(['payroll.payroll_month'=>SORT_DESC, 'payroll.payroll_date'=>SORT_DESC])
		                      ->where(['payroll_settings.person_id'=>$pers,'payroll_settings.academic_year'=>$acad]);
           
                        
          $dataProvider = new ActiveDataProvider([
            'query' => $result,
            'pagination' => [
            'pageSize' => 10000,
        ],
        ]);

    return $dataProvider;
	
}

public function getInfoAllPayrollForOne($pers,$acad){
	           
	
   $bill = new SrcPayroll();
          $result= $bill->find()
		                      ->joinWith(['idPayrollSet'])
		                      ->select(['payroll.id', 'payroll.id_payroll_set','payroll.id_payroll_set2','payroll.missing_hour', 'payroll.payroll_month', 'payroll.payroll_date', 'payroll_settings.number_of_hour', 'payroll.taxe', 'payroll.gross_salary','payroll.net_salary','payroll.plus_value','payroll.loan_rate','payroll_settings.frais', 'payroll.payment_date', 'payroll.cash_check',])
		                      ->distinct('true')
		                      ->orderBy(['payroll.payroll_month'=>SORT_ASC])
		                      ->where(['payroll_settings.person_id'=>$pers,'payroll_settings.academic_year'=>$acad]);
           
                        
          $dataProvider = new ActiveDataProvider([
            'query' => $result,
            'pagination' => [
            'pageSize' => 10000,
        ],
        ]);

    return $dataProvider;
	
}

    
    
public function getInfoLastPayrollByPerson($person_id)
 {
       $acad=Yii::$app->session['currentId_academic_year'];
    
    $query = SrcPayroll::find()
	                 ->alias('p')
	                 ->distinct('true')
	                 ->joinWith(['idPayrollSet',])
	                 ->select(['p.id', 'p.id_payroll_set', 'p.id_payroll_set2', 'p.missing_hour', 'p.payroll_month', 'p.payroll_date', 'payroll_settings.number_of_hour', 'p.taxe', 'p.gross_salary','p.net_salary','p.plus_value','p.loan_rate','payroll_settings.frais', 'p.payment_date', 'p.cash_check']) 
	                 ->orderBy(['p.payroll_month'=>SORT_DESC, 'p.payroll_date'=>SORT_DESC])
	                ->where('payroll_settings.person_id='.$person_id.' AND payroll_settings.academic_year='.$acad);
	
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;

  
  }



public function getInfoLastPayrollByPayroll_group($grouppayroll)
 {
      $acad=Yii::$app->session['currentId_academic_year'];	
      
    $query = SrcPayroll::find()
		                  ->distinct('true')
		                  ->joinWith(['idPayrollSet','idPayrollSet.person'])
		                  ->orderBy(['payroll.payroll_month'=>SORT_DESC,'payroll.payroll_date'=>SORT_DESC])
		                  ->select(['payroll.id', 'payroll.id_payroll_set', 'payroll.id_payroll_set2', 'payroll.missing_hour', 'payroll.payroll_month', 'payroll.payroll_date', 'payroll_settings.number_of_hour', 'payroll.taxe', 'payroll.gross_salary','payroll.net_salary','payroll.plus_value','payroll.loan_rate','payroll_settings.frais', 'payroll.payment_date', 'payroll.cash_check'])
		                  ->where('payroll_settings.as_='.$grouppayroll.' AND payroll_settings.academic_year='.$acad);
			
			  
     $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;


   

  
  }

    
    



public function getDatePastPayrollByPerson($id_payroll_set, $month){

	$acad=Yii::$app->session['currentId_academic_year'];
	if($month!='')
	{
	     $sql = 'SELECT DISTINCT p.payroll_date FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE p.payroll_month='.$month.' AND ps.academic_year='.$acad.' AND ps.id='.$id_payroll_set.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	  }
	else
	  return null;

}


//return 1 si l gen payroll pou li deja; 0 si poko genyen
public function isPayrollAlreadyDone($id_payroll_set,$acad){

	
	     $sql = 'SELECT DISTINCT p.payroll_date FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' AND ps.id='.$id_payroll_set.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return 1;
        else
           return 0;
	 

}

public function getDatePastPayrollByGroup($month){

	$acad=Yii::$app->session['currentId_academic_year'];
	if($month!='')
	{
	     $sql = 'SELECT DISTINCT p.payroll_date FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE p.payroll_month='.$month.' AND ps.academic_year='.$acad.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	  }
	else
	  return null;

}


     
public function getDatePastPayroll($month){

	$acad=Yii::$app->session['currentId_academic_year'];
	if($month!='')
	{
	     $sql = 'SELECT DISTINCT p.payroll_date FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE p.payroll_month='.$month.' AND ps.academic_year='.$acad.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	  }
	else
	  return null;

}


                 
public function getMonthPastPayrollByPerson($id_payroll_set){
	
	$acad=Yii::$app->session['currentId_academic_year'];
	if($id_payroll_set!='')
	{
	     $sql = 'SELECT DISTINCT p.payroll_month FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' AND ps.id='.$id_payroll_set.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	  }
	else
	  return null;

	
   }
   

   
   
public function getMonthPastPayrollByPayroll_group($grouppayroll){
	
	$acad=Yii::$app->session['currentId_academic_year'];
	
	if($grouppayroll==1)//employee
	   $group=0;
	elseif($grouppayroll==2)//teacher
	    $group=1;
	    
	$sql = 'SELECT DISTINCT p.payroll_month FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' AND ps.as='.$group.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::app()->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	
   }
    
   
   
public function getMonthPastPayroll(){
	
	$acad=Yii::$app->session['currentId_academic_year'];
	
	
	    
	$sql = 'SELECT DISTINCT p.payroll_month FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' order by p.payroll_month ASC, p.payroll_date ASC';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
           return $info__p;
        else
           return null;
	
   }
       

 public function getPlusValue(){
          
          if(isset($this->idPayrollSet->devise0->devise_symbol))
             return $this->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($this->plus_value);
        }
        
 public function getLoanRate(){
          
          if(isset($this->idPayrollSet->devise0->devise_symbol))
             return $this->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($this->loan_rate);
        }
        
  
 
  
    
                  
public function getTimeMissing($person,$numb_hour){
	                                                      
     $acad=Yii::$app->session['currentId_academic_year'];
        $missTime = 0;
        $nbr_time=0;
      	
		//pran enfo denye setting-payroll la
		$sql = 'SELECT number_of_hour, an_hour FROM payroll_settings  WHERE old_new=1 AND person_id='.$person.' AND academic_year='.$acad.' order by id DESC';
		$command = Yii::$app->db->createCommand($sql);
        $info_setting_p = $command->queryAll(); 
									       	   
		  if($info_setting_p!=null) 
			{ 
			 
			 $employee_teacher=$modelPers->isEmployeeTeacher($person, $acad);
	  
			 if($employee_teacher) 
			   {
			   	     $compt=0;
			   	     
			   	     foreach($info_setting_p as $info_)
					  {
					      $compt++;
					      
					      if($info_['an_hour']==1)
			                {     
						      $nbr_time = $info_['number_of_hour'];					  	   	    
			                }
					     
					     if($compt==2)
					        break;
					        
					   }
			   	     
			   	     
			     }
			  elseif(!$employee_teacher)
			    {

				 foreach($info_setting_p as $info_)
				  {
				      if($info_['an_hour']==1)
			           {
			              $nbr_time = $info_['number_of_hour'];
					      
			             }
			             					  	   	    
				     break;
				   }
				
			    }
			
			   			   
			}
													  	   	  
                                        
        if($numb_hour >0)
           { 
	           	if( $nbr_time > 0 )
	           	  {
	           	   $missTime = $nbr_time - $numb_hour;
	           	
	           	  }
	           	
                
             }
                  
            
       return $missTime; 
       
        
    }
    
    
    
   
        

   
/**
         * 
         * @param float $monthlySalary 
         * @param //array $bareme 5 values like this array(array(0, 60000, 0),array(60000, 240000, 0.1),array(240000, 480000, 0.15),array(480000, 1000000, 0.25), array(1000000, , 0.3))
        * @return array $salaire 
         */
     public function getIriCharge_new($monthlySalary,$bareme){
            $netSalary = 0;
            $deduction = 0;
            $eder_marc_poulard_prosper =-1;
                        
            $salaire = array(); // $salaire['month_salary_net'] : salaire net mensuel, $salaire['month_iri']: deduction IRI mensuel, $salaire['salary_year']: salaire net annuel, $salaire['iri_year']: deduction IRI annuelle
            $annualSalary = 12*$monthlySalary;
           
            //detemine nan ki enteval sale sa ye
            for($i=0; $i< 4 ; $i++)
             {
             	if(($bareme[$i][0]< $annualSalary) && ($annualSalary<=$bareme[$i][1]))
             	  {
             	  	  $eder_marc_poulard_prosper = $i;
             	  	    break;
             	  	}
             	}
            
             if($eder_marc_poulard_prosper == -1)
                 $eder_marc_poulard_prosper = 4;
            
            
            switch($eder_marc_poulard_prosper)
               {
               	
               	      case 0:
               	             $deduction = 0;
               	             $netSalary = $annualSalary - $deduction;
               	              break;
               	    
               	      case 1:
               	               $deduction = $deduction +(  ( $annualSalary - $bareme[1][0] ) * $bareme[1][2]/100  );
               	             
               	                 $netSalary = $annualSalary - $deduction;
               	           
               	               break;
               	              
               	      case 2:
               	                $deduction = $deduction +(  ( $annualSalary - $bareme[2][0] ) * $bareme[2][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[1][1] - $bareme[1][0] ) * $bareme[1][2]/100  );
               	              
               	                 $netSalary = $annualSalary - $deduction;
               	                 
               	                 
               	                break;
               	              
               	      case 3:
               	                $deduction = $deduction +(  ( $annualSalary - $bareme[3][0] ) * $bareme[3][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[2][1] - $bareme[2][0] ) * $bareme[2][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[1][1] - $bareme[1][0] ) * $bareme[1][2]/100  );
               	             
               	                 $netSalary = $annualSalary - $deduction;
               	              
               	                 break;
               	              
               	      case 4:
               	              $deduction = $deduction +(  ( $annualSalary - $bareme[4][0] ) * $bareme[4][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[3][1] - $bareme[3][0] ) * $bareme[3][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[2][1] - $bareme[2][0] ) * $bareme[2][2]/100  );
               	                
               	                $deduction = $deduction +(  ( $bareme[1][1] - $bareme[1][0] ) * $bareme[1][2]/100  );
               	             
               	                 $netSalary = $annualSalary - $deduction;
                               
                                  break;
               	
               	}
            
                          
            
            $salaire['month_salary_net'] = round(($netSalary/12),2); // Salaire mensuel net
            $salaire['month_iri'] = round(($deduction/12),2); // Deduction IRI mensuel
            $salaire['salary_year'] = round($netSalary,2); // Salaire annuel
            $salaire['iri_year'] = round($deduction,2); // Deduction IRI annuel        
            
            
            
            return $salaire;
        }
        
        
        
        
        
                            
public function getLoanDeduction($person,$amount,$numb_hour,$missing_hour,$net_salary,$frais,$plus_value,$taxe,$assurance,$month){
	                                                      
     $acad=Yii::$app->session['currentId_academic_year'];
     //$currency_name = Yii::app()->session['currencyName'];
	  //     $currency_symbol = Yii::app()->session['currencySymbol']; 
	  
	  $modelPers = new SrcPersons;

         $loan = 0;
         $gross_salary =0;
          $diff_salary = 0;
         //nbr d'hre
         $nbr_time=0;
         $pay_time= 0;
         $assurance_= 0;
      
if( ($month!='')&&($month!=0)  )
	{				
		//pran enfo denye setting-payroll la
		$sql = 'SELECT number_of_hour,amount,assurance_value, an_hour FROM payroll_settings  WHERE old_new=1 AND person_id='.$person.' AND academic_year='.$acad.' order by id DESC';
		$command = Yii::$app->db->createCommand($sql);
        $info_setting_p = $command->queryAll(); 
									       	   
		  if($info_setting_p!=null) 
			{ 
			 
			 $employee_teacher=$modelPers->isEmployeeTeacher($person, $acad);
	  
			 if($employee_teacher) 
			   {
			   	     $compt=0;
			   	     
			   	     foreach($info_setting_p as $info_)
					  {
					      $compt++;
					      
					      if($info_['an_hour']==1)
			                {     
						      $pay_time= $info_['amount'];
							  $nbr_time = $info_['number_of_hour'];	
							  $assurance_= $info_['assurance_value'];	
							  
							  if(($missing_hour!='')&&( $missing_hour!=0))
					           { 	           	      	
					           	  $diff_salary = $diff_salary + round( ($pay_time * $missing_hour), 2);
						          
					             }			  	   	    
			                }
					     
					     if($compt==2)
					        break;
					        
					   }
			   	     
			   	     
			     }
			  elseif(!$employee_teacher)
			    {

				 foreach($info_setting_p as $info_)
				  {
				      if($info_['an_hour']==1)
			           {
			             $pay_time= $info_['amount'];
					      $nbr_time = $info_['number_of_hour'];
					      $assurance_= $info_['assurance_value'];
					      
					      if(($missing_hour!='')&&( $missing_hour!=0))
					           { 	           	      	
					           	  $diff_salary = $diff_salary + round( ($pay_time * $missing_hour), 2);
						          
					             }
					      
			             }
			             					  	   	    
				     break;
				   }
				
			    }
			
			   
			}
			
          
          
			 
        $gross_salary = $amount;
   
          $loan = round( ($gross_salary + $frais + $plus_value - $diff_salary - $taxe - $assurance), 2)- $net_salary;   
	     
	     }
	     
            return $loan;  
    }
 
                             
public function getMissingHourDeduction($devise,$person,$missing_hour){
	                                                      
     $acad=Yii::$app->session['currentId_academic_year'];
     //$currency_name = Yii::app()->session['currencyName'];
	       //$currency_symbol = Yii::app()->session['currencySymbol']; 
       
       $modelPers = new SrcPersons;
         $gross_salary_deduction = 0;
         //nbr d'hre
         $nbr_time=0;
         $pay_time= 0;
         $devis_symbol = '';
         
         $modelDevise = SrcDevises::findOne($devise);
         $devis_symbol = $modelDevise->devise_symbol;
      
    
		//pran enfo denye setting-payroll la
		$sql = 'SELECT number_of_hour,amount, an_hour FROM payroll_settings  WHERE devise='.$devise.' and old_new=1 AND person_id='.$person.' AND academic_year='.$acad.' order by id DESC';
		$command = Yii::$app->db->createCommand($sql);
        $info_setting_p = $command->queryAll(); 
									       	   
		  if($info_setting_p!=null) 
			{ 
			 
			 $employee_teacher=$modelPers->isEmployeeTeacher($person, $acad);
	  
			 if($employee_teacher) 
			   {
			   	     $compt=0;
			   	     
			   	     foreach($info_setting_p as $info_)
					  {
					      $compt++;
					      
					      if($info_['an_hour']==1)
			                {     
						      $pay_time= $info_['amount'];
							  $nbr_time = $info_['number_of_hour'];					  	   	    
			                }
					     
					     if($compt==2)
					        break;
					        
					   }
			   	     
			   	     
			     }
			  elseif(!$employee_teacher)
			    {

				 foreach($info_setting_p as $info_)
				  {
				      if($info_['an_hour']==1)
			           {
			             $pay_time= $info_['amount'];
					      $nbr_time = $info_['number_of_hour'];
					      
			             }
			             					  	   	    
				     break;
				   }
				
			    }
			
			   
			}
													  	   	  
      if($nbr_time!=0)
        $gross_salary_deduction =round(  (($pay_time * $missing_hour) ), 2);         
           
           return $devis_symbol.' '.numberAccountingFormat($gross_salary_deduction);  
    }
 


 public function getNumberHour(){
        
        if(($this->number_of_hour!=0)&&($this->number_of_hour!=''))
             return $this->number_of_hour;
        else
            return  Yii::t('app','N/A');
                
        }


//for index and update
 public function getGrossSalaryInd($devise, $person_id,$month,$year)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
         
         //$currency_name = Yii::$app->session['currencyName'];
	      // $currency_symbol = Yii::$app->session['currencySymbol'];
	      $modelPay_set = new SrcPayrollSettings;
	      $modelPers = new SrcPersons;
	        
         $id_payroll_set ='';
         $id_payroll_set2 ='';
         
         $devise_symbol = '';
         //jwenn id_payroll_set apati de payroll la
         $sql = 'SELECT id_payroll_set, id_payroll_set2, devise FROM payroll p inner join payroll_settings ps on(ps.id = p.id_payroll_set) where ps.devise='.$devise.' and ps.person_id='.$person_id.' AND p.payroll_month='.$month.' AND YEAR(p.payment_date)='.$year;		   	     
			
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  $id_payroll_set = $info['id_payroll_set'];
          	        $id_payroll_set2 =$info['id_payroll_set2'];
          	        
          	        $modelDevise =  SrcDevises::findOne($info['devise']);
          	        $devise_symbol = $modelDevise->devise_symbol;
          	         break;
          	     }
          
          }
        
           
         $person_id = $modelPay_set->getPersonIdByIdPayrollSetting($id_payroll_set);

	     $employee_teacher=$modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	       if(($id_payroll_set2 =='')||($id_payroll_set2 ==NULL))
		   	         $condition ='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	      else
		   	          $condition ='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.') AND payroll_settings.academic_year='.$acad;
		   	     
		   	      //check if it is a timely salary 
                   $model_ = new SrcPayrollSettings;
                     $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
			            $total_gross_salary = $total_gross_salary + $gross_salary;
			          
			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $number_of_hour = $amount->number_of_hour;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	 //calculate $gross_salary by hour if it's a timely salary person 
								     if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
					                   
						            }
					           
					            $total_gross_salary = $total_gross_salary + $gross_salary;
					          
					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $devise_symbol.' '.numberAccountingFormat($total_gross_salary);
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		           
		           
		            $condition ='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	     
		   	      //check if it is a timely salary 
                   $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();

                   
                                         
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';$gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
                          break;
                      }
                      
                 return  $devise_symbol.' '.numberAccountingFormat($gross_salary);     
                      		           
		     }     
 
  }


//for index and update
public function getGrossSalaryIndex_value($devise,$person_id,$month,$year)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
         
         //$currency_name = Yii::app()->session['currencyName'];
	     //  $currency_symbol = Yii::app()->session['currencySymbol']; 
	     $modelPay_set = new SrcPayrollSettings;
	      $modelPers = new SrcPersons;
	      
	      
         $id_payroll_set ='';
         $id_payroll_set2 ='';
         
         //jwenn id_payroll_set apati de payroll la
         $sql = 'SELECT id_payroll_set, id_payroll_set2 FROM payroll p inner join payroll_settings ps on(ps.id = p.id_payroll_set) where ps.devise='.$devise.' and ps.person_id='.$person_id.' AND p.payroll_month='.$month.' AND YEAR(p.payment_date)='.$year;		   	     
			
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  $id_payroll_set = $info['id_payroll_set'];
          	        $id_payroll_set2 =$info['id_payroll_set2'];
          	         break;
          	     }
          
          }
        
           
         $person_id = $modelPay_set->getPersonIdByIdPayrollSetting($id_payroll_set);

	     $employee_teacher=$modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	      if(($id_payroll_set2 =='')||($id_payroll_set2 ==NULL))
		   	         $condition ='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	      else
		   	          $condition ='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.') AND payroll_settings.academic_year='.$acad;
		   	        
		   	     
		   	      //check if it is a timely salary 
                   
                     $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
			            $total_gross_salary = $total_gross_salary + $gross_salary;
			          
			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $number_of_hour = $amount->number_of_hour;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	 //calculate $gross_salary by hour if it's a timely salary person 
								     if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
					                   
						            }
					           
					            $total_gross_salary = $total_gross_salary + $gross_salary;
					          
					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $total_gross_salary;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		           $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;

                   
                    $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';$gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
                          break;
                      }
                      
                 return  $gross_salary;     
                      		           
		     }     
 
 
  }

//for index and update
public function getGrossSalaryPerson_idMonthAcad($devise,$person_id,$month,$acad)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
         
        // $currency_name = Yii::app()->session['currencyName'];
	    //   $currency_symbol = Yii::app()->session['currencySymbol'];
	    $modelPay_set = new SrcPayrollSettings;
	      $modelPers = new SrcPersons;
	      
	       
         $id_payroll_set ='';
         $id_payroll_set2 ='';
         //jwenn id_payroll_set apati de payroll la
         $sql = 'SELECT id_payroll_set, id_payroll_set2 FROM payroll p inner join payroll_settings ps on(ps.id = p.id_payroll_set) where ps.devise='.$devise.' and ps.person_id='.$person_id.' AND p.payroll_month='.$month.' AND ps.academic_year='.$acad;		   	     
			
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  $id_payroll_set = $info['id_payroll_set'];
          	        $id_payroll_set2 =$info['id_payroll_set2'];
          	         break;
          	     }
          
          }
        
           
         $person_id = $modelPay_set->getPersonIdByIdPayrollSetting($id_payroll_set);

	     $employee_teacher=$modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	       if(($id_payroll_set2 =='')||($id_payroll_set2 ==NULL))
		   	         $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	      else
		   	          $condition='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.') AND payroll_settings.academic_year='.$acad;
		   	     
		   	      //check if it is a timely salary 
                   $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
			            $total_gross_salary = $total_gross_salary + $gross_salary;
			          
			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $number_of_hour = $amount->number_of_hour;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	 //calculate $gross_salary by hour if it's a timely salary person 
								     if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
					                   
						            }
					           
					            $total_gross_salary = $total_gross_salary + $gross_salary;
					          
					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $total_gross_salary;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		           $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;

                   
                     $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';$gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
                          break;
                      }
                      
                 return  $gross_salary;     
                      		           
		     }     
 
 
  }




//for create
 public function getGrossSalary($devise,$person_id)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
          $model_ = new SrcPayrollSettings;
          $modelPers = new SrcPersons;
         
         //$currency_name = Yii::$app->session['currencyName'];
	      // $currency_symbol = Yii::app()->session['currencySymbol']; 
         //$criteria = new CDbCriteria(array('alias'=>'ps', 'order'=>'ps.id DESC', 'condition'=>'ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) '));
                
                
                 //check if it is a timely salary 
                  
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
			            $total_gross_salary = $total_gross_salary + $gross_salary;
			          
			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $number_of_hour = $amount->number_of_hour;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	 //calculate $gross_salary by hour if it's a timely salary person 
								     if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
					                   
						            }
					           
					            $total_gross_salary = $total_gross_salary + $gross_salary;
					          
					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $total_gross_salary;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		          $gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $number_of_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	//calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						         
						        }
			                   
			             
				            }
			           
                          break;
                      }
                      
                 return  $gross_salary;     
                      		           
		     }     
 
  }

//for create
 public function getFrais_($devise,$person_id)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
          $model_ = new SrcPayrollSettings;
          $modelPers = new SrcPersons;
         
         //$currency_name = Yii::$app->session['currencyName'];
	      // $currency_symbol = Yii::app()->session['currencySymbol']; 
         //$criteria = new CDbCriteria(array('alias'=>'ps', 'order'=>'ps.id DESC', 'condition'=>'ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) '));
                
                
                 //check if it is a timely salary 
                  
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                    $frais=0;

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $frais = $frais + $amount->frais;
			               	   
			                }
			           			          
			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	  $frais = $frais + $amount->frais;
					               	   
					                 }
					           					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $frais;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		          $gross_salary = 0;
		          $frais = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $frais = $frais + $amount->frais;
			               	  
			                 }
			           
			           
                          break;
                      }
                      
                 return  $frais;     
                      		           
		     }     
 
  }

//for index and update
public function getFraisIndex_value($devise,$person_id,$month,$year)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
         
         //$currency_name = Yii::app()->session['currencyName'];
	     //  $currency_symbol = Yii::app()->session['currencySymbol']; 
	     $modelPay_set = new SrcPayrollSettings;
	      $modelPers = new SrcPersons;
	      
	      
         $id_payroll_set ='';
         $id_payroll_set2 ='';
         
         //jwenn id_payroll_set apati de payroll la
         $sql = 'SELECT id_payroll_set, id_payroll_set2 FROM payroll p inner join payroll_settings ps on(ps.id = p.id_payroll_set) where ps.devise='.$devise.' and ps.person_id='.$person_id.' AND p.payroll_month='.$month.' AND YEAR(p.payment_date)='.$year;		   	     
			
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  $id_payroll_set = $info['id_payroll_set'];
          	        $id_payroll_set2 =$info['id_payroll_set2'];
          	         break;
          	     }
          
          }
        
        $frais=0;
           
         $person_id = $modelPay_set->getPersonIdByIdPayrollSetting($id_payroll_set);

	     $employee_teacher=$modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	      if(($id_payroll_set2 =='')||($id_payroll_set2 ==NULL))
		   	         $condition ='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	      else
		   	          $condition ='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.') AND payroll_settings.academic_year='.$acad;
		   	        
		   	     
		   	      //check if it is a timely salary 
                   
                     $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	     $frais = $frais + $amount->frais;
			               	  
			                 }
			          			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $frais = $frais + $amount->frais;
					                 }
					         					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $frais;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		           $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;

                   
                    $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';$gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $frais = $frais + $amount->frais;
			                 }
			           			           
                          break;
                      }
                      
                 return  $frais;     
                      		           
		     }     
 
 
  }

//for index and update
public function getFraisPerson_idMonthAcad($devise,$person_id,$month,$acad)
  {
         $acad=Yii::$app->session['currentId_academic_year'];
         
        // $currency_name = Yii::app()->session['currencyName'];
	    //   $currency_symbol = Yii::app()->session['currencySymbol'];
	    $modelPay_set = new SrcPayrollSettings;
	      $modelPers = new SrcPersons;
	      
	       
         $id_payroll_set ='';
         $id_payroll_set2 ='';
         //jwenn id_payroll_set apati de payroll la
         $sql = 'SELECT id_payroll_set, id_payroll_set2 FROM payroll p inner join payroll_settings ps on(ps.id = p.id_payroll_set) where ps.devise='.$devise.' and ps.person_id='.$person_id.' AND p.payroll_month='.$month.' AND ps.academic_year='.$acad;		   	     
			
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  $id_payroll_set = $info['id_payroll_set'];
          	        $id_payroll_set2 =$info['id_payroll_set2'];
          	         break;
          	     }
          
          }
        
        $frais = 0;
           
         $person_id = $modelPay_set->getPersonIdByIdPayrollSetting($id_payroll_set);

	     $employee_teacher=$modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	       if(($id_payroll_set2 =='')||($id_payroll_set2 ==NULL))
		   	         $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;
		   	      else
		   	          $condition='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.') AND payroll_settings.academic_year='.$acad;
		   	     
		   	      //check if it is a timely salary 
                   $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	 
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $frais = $frais + $amount->frais;
			                 }
			           			          	                           
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                    
			                     if(($amount!=null))
					               {  
					               	   $frais = $frais + $amount->frais;
					                 }
					           
					          	                          
                               }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $frais;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   { 
		           $condition='payroll_settings.id ='.$id_payroll_set.' AND payroll_settings.academic_year='.$acad;

                   
                     $pay_set = SrcPayrollSettings::find()
                                            ->orderBy(['payroll_settings.id'=>SORT_DESC])
                                           ->where($condition)
                                           ->all();
                     
                     $compt=0;
                     $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';$gross_salary = 0;
		              
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     
	                     if(($amount!=null))
			               {  
			               	   $frais = $frais + $amount->frais;
			                 }
			          			           
                          break;
                      }
                      
                 return  $frais;     
                      		           
		     }     
 
 
  }


 public function getAssurance()
  {
        $modelPers = new SrcPersons;
        
         $acad=Yii::$app->session['currentId_academic_year'];
         
         $assurance = 0;
         
         $devise = $this->idPayrollSet->devise;
         
          $modelDevise = SrcDevises::findOne($devise);
         
         $person_id = $this->idPayrollSet->person_id;
                   
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               $assurance = $assurance + $amount->assurance_value;
			               	          	                           
                         }
                        elseif(($as_teach==0)&&($as==1))
                            {  
                            	if(($amount!=null))
					               $assurance = $assurance + $amount->assurance_value;
					                                
                              }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $modelDevise->devise_symbol.' '.numberAccountingFormat($assurance);
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   {     
                 foreach($pay_set as $amount)
                   {   
                   	  $as = $amount->as_;
                   	  
   	                     if(($amount!=null))
			               $assurance = $assurance + $amount->assurance_value;
			          			           
                          break;
                      }
                      
                 return  $modelDevise->devise_symbol.' '.numberAccountingFormat($assurance);     
                      		           
		     }     
 
  }

 public function getAssurance_numb()
  {
        $modelPers = new SrcPersons;
        
         $acad=Yii::$app->session['currentId_academic_year'];
         
         $assurance = 0;
         
         $devise = $this->idPayrollSet->devise;
         
          $modelDevise = SrcDevises::findOne($devise);
         
         $person_id = $this->idPayrollSet->person_id;
                   
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               $assurance = $assurance + $amount->assurance_value;
			               	          	                           
                         }
                        elseif(($as_teach==0)&&($as==1))
                            {  
                            	if(($amount!=null))
					               $assurance = $assurance + $amount->assurance_value;
					                                
                              }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $assurance;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   {     
                 foreach($pay_set as $amount)
                   {   
                   	  $as = $amount->as_;
                   	  
   	                     if(($amount!=null))
			               $assurance = $assurance + $amount->assurance_value;
			          			           
                          break;
                      }
                      
                 return  $assurance;     
                      		           
		     }     
 
  }  
  
 public function getFrais()
  {
        $modelPers = new SrcPersons;
        
         $acad=Yii::$app->session['currentId_academic_year'];
         
         $frais = 0;
         
         $devise = $this->idPayrollSet->devise;
         
          $modelDevise = SrcDevises::findOne($devise);
         
         $person_id = $this->idPayrollSet->person_id;
                   
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               $frais = $frais + $amount->frais;
			               	          	                           
                         }
                        elseif(($as_teach==0)&&($as==1))
                            {  
                            	if(($amount!=null))
					               $frais = $frais + $amount->frais;
					                                
                              }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $modelDevise->devise_symbol.' '.numberAccountingFormat($frais);
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   {     
                 foreach($pay_set as $amount)
                   {   
                   	  $as = $amount->as_;
                   	  
   	                     if(($amount!=null))
			               $frais = $frais + $amount->frais;
			          			           
                          break;
                      }
                      
                 return  $modelDevise->devise_symbol.' '.numberAccountingFormat($frais);     
                      		           
		     }     
 
  }

 public function getFrais_numb()
  {
        $modelPers = new SrcPersons;
        
         $acad=Yii::$app->session['currentId_academic_year'];
         
         $frais = 0;
         
         $devise = $this->idPayrollSet->devise;
         
          $modelDevise = SrcDevises::findOne($devise);
         
         $person_id = $this->idPayrollSet->person_id;
                   
                     $pay_set = SrcPayrollSettings::find()
                                     ->alias('ps')
                                     ->orderBy(['ps.id'=>SORT_DESC])
                                     ->where('ps.devise='.$devise.' and ps.old_new=1 AND ps.person_id='.$person_id.' AND ps.academic_year='.$acad.' AND ps.person_id IN( SELECT id FROM persons p WHERE p.id='.$person_id.' AND p.active IN(1, 2)) ')
                                     ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $total_gross_salary=0;
                    $timely_salary = '';

	  $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
	  
		 if($employee_teacher) 
		   {
		   	     $compt=0;
                  
                 foreach($pay_set as $amount)
                   {   
                   	  $compt++;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               $frais = $frais + $amount->frais;
			               	          	                           
                         }
                        elseif(($as_teach==0)&&($as==1))
                            {  
                            	if(($amount!=null))
					               $frais = $frais + $amount->frais;
					                                
                              }
                       
                        if($compt==2)
                           break;
                       
                      }
                 
                 return $frais;
		   	     
		   	 }
		  elseif(!$employee_teacher)
		   {     
                 foreach($pay_set as $amount)
                   {   
                   	  $as = $amount->as_;
                   	  
   	                     if(($amount!=null))
			               $frais = $frais + $amount->frais;
			          			           
                          break;
                      }
                      
                 return  $frais;     
                      		           
		     }     
 
  }

	 public function getTaxe(){
           
           $modelPS = SrcPayrollSettings::findOne($this->id_payroll_set);
           
           $modelDevise = SrcDevises::findOne($modelPS->devise);
                
            return $modelDevise->devise_symbol.' '.numberAccountingFormat($this->taxe);
        }


	 public function getNetSalary(){
           
           $modelPS = SrcPayrollSettings::findOne($this->id_payroll_set);
           
           $modelDevise = SrcDevises::findOne($modelPS->devise);
                
            return $modelDevise->devise_symbol.' '.numberAccountingFormat($this->net_salary );
        }


  public function getAnHour(){
            switch($this->an_hour)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
               
            }
        }
        
   public function getAnHourValue(){
            return array(
                0=>Yii::t('app','No'),
                1=>Yii::t('app','Yes'),
                             
            );            
        } 		
      
   public function searchPersonsForShowingPayroll($condition, $acad)
	{      
			$query = SrcPayroll::find()
			                     ->joinWith(['idPayrollSet','idPayrollSet.person'])
			                     ->select(['payroll.id', 'payroll.id_payroll_set', 'payroll.id_payroll_set2', 'payroll_settings.person_id', 'persons.last_name', 'persons.first_name', 'concat(persons.first_name,persons.last_name) as full_name', 'persons.id_number', 'persons.birthday', 'persons.gender', 'payroll_settings.amount', 'payroll_settings.an_hour', 'payroll_settings.academic_year', 'payroll.gross_salary', 'payroll.net_salary', 'payroll.payroll_date', 'payroll.payment_date', 'payroll.cash_check', 'payroll.taxe','payroll.plus_value','payroll.loan_rate','payroll_settings.frais', 'payroll_settings.number_of_hour', 'payroll.missing_hour', 'payroll.payroll_month'])
			                     ->where($condition)
			                     ->andWhere(['payroll_settings.academic_year'=>$acad])
			                     ->orderBy(['payroll.payment_date'=>SORT_DESC, 'payroll.payroll_date'=>SORT_DESC, 'persons.last_name'=>SORT_ASC ]);
   
   
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000,
        ],
    ]);


     
    
    return $dataProvider;
			
	 	
    }



  public function searchPersonsForUpdatePayroll($condition, $month, $payroll_date, $acad)
	{      
			$criteria = new CDbCriteria;
			
			
			//$criteria->with=array('idPayrollSet');
		$criteria->alias = 'pl';
		 $criteria->join = 'left join payroll_settings ps on(ps.id=pl.id_payroll_set) left join persons p on(p.id=ps.person_id)';
		 
		 $criteria->condition = $condition.' AND ps.academic_year='.$acad.' AND pl.payroll_month='.$month.' AND pl.payroll_date=\''.$payroll_date.'\'' ;
			   
            $criteria->select = 'pl.id, pl.id_payroll_set,pl.id_payroll_set2, ps.person_id, p.last_name , p.first_name, p.id_number, p.birthday, p.gender, ps.amount, ps.an_hour, ps.academic_year, pl.gross_salary,pl.net_salary, pl.payroll_date, pl.payment_date, pl.cash_check, pl.taxe,pl.plus_value,pl.loan_rate,ps.frais,ps.number_of_hour, pl.missing_hour,pl.payroll_month';
                       // $criteria->alias = 'courses c';
			             
			
						 $criteria->order = 'p.last_name ASC';
			//$criteria->limit = '100';
		     
		    		 
    return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=> 10000,
    			),
				
		'criteria'=>$criteria,
		
		
    ));
 	
    }
    
   public function searchPersonsInPayrollForReports($condition, $month, $acad)
	{      
			$criteria = new CDbCriteria;
			
			
			//$criteria->with=array('idPayrollSet');
		$criteria->alias = 'pl';
		 $criteria->join = 'left join payroll_settings ps on(ps.id=pl.id_payroll_set) left join persons p on(p.id=ps.person_id)';
		 
		 $criteria->condition = $condition.' AND ps.academic_year='.$acad.' AND pl.payroll_month='.$month ;
			   
            $criteria->select = 'pl.id, pl.id_payroll_set,pl.id_payroll_set2, ps.person_id, p.last_name , p.first_name, p.id_number, p.birthday, p.gender, ps.amount, ps.an_hour, ps.academic_year, pl.gross_salary,pl.net_salary, pl.payroll_date, pl.payment_date, pl.cash_check, pl.taxe,pl.plus_value,pl.loan_rate,ps.frais,ps.number_of_hour, pl.missing_hour,pl.payroll_month';
                       // $criteria->alias = 'courses c';
			             
			
						 $criteria->order = 'p.last_name ASC';
			//$criteria->limit = '100';
		     
		    		 
    return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=> 10000,
    			),
				
		'criteria'=>$criteria,
		
		
    ));
 	
    }
    

 public function getYearFromAcad($month, $acad)
	{      
		$sql = 'SELECT YEAR(p.payroll_date) as year FROM payroll p left join payroll_settings ps on(ps.id=p.id_payroll_set) where ps.old_new = 1 AND ps.academic_year ='.$acad.' AND p.payroll_month ='.$month;
		
		$command = Yii::app()->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  return $info['year'];
          	         break;
          	     }
          
          }
        else
           return null;
           	
    }
    

  public function getPayrollGroup(){
            return [
                1=>Yii::t('app','Employees'), // Les personnes dans person qui ont un poste [done] 
                2=>Yii::t('app','Teachers'), // Les professeurs uniquement sans un poste [done]
                
            ];            
        }
        

// Return the key=>value  of a month in long format 
public function getLongMonthValue(){
     return array(
        1=>Yii::t('app','January'),
        2=>Yii::t('app','February'),
        3=>Yii::t('app','March'),
        4=>Yii::t('app','April'),
        5=>Yii::t('app','May'),
        6=>Yii::t('app','June'),
        7=>Yii::t('app','July'),
        8=>Yii::t('app','August'),
        9=>Yii::t('app','September'),
        10=>Yii::t('app','October'),
        11=>Yii::t('app','November'),
        12=>Yii::t('app','December'),
        13=>Yii::t('app','13 eme'),
        14=>Yii::t('app','14 eme'),
        0=>Yii::t('app','BONIS'),
        
        
        );  
    }
 
 
// Return the name of a month in long format 
public function getSelectedLongMonth($month){
    switch ($month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
       case 13:
            return Yii::t('app','13 eme');
            break;
       case 14:
            return Yii::t('app','14 eme');
            break;
       case 0:
            return Yii::t('app','BONIS');
            break;
    }
   }
   
 // Return the key=>value  of a month in long format 
public function getIdLongMonth($month){
     switch ($month){
        case Yii::t('app','January'):
            return 1;
            break;
        case Yii::t('app','February'):
            return 2;
            break;
        case Yii::t('app','March'):
            return 3;
            break;
        case Yii::t('app','April'):
            return 4;
            break;
        case Yii::t('app','May'):
            return 5;
            break;
        case Yii::t('app','June'):
            return 6;
            break;
        case Yii::t('app','July'):
            return 7;
            break;
        case Yii::t('app','August'):
            return 8;
            break;
        case Yii::t('app','September'):
            return 9;
            break;
        case Yii::t('app','October'):
            return 10;
            break;
        case Yii::t('app','November'):
            return 11;
            break;
        case Yii::t('app','December'):
            return 12;
            break;
       case Yii::t('app','13 eme'):
            return 13;
            break;
      case Yii::t('app','14 eme'):
            return 14;
            break;
      case Yii::t('app','BONIS'):
            return 0;
            break;

    }

 }
   
  

// Return the name of a month in long format 
public function getLongMonth(){
    switch ($this->payroll_month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
         case 13:
            return Yii::t('app','13 eme');
            break;
         case 14:
            return Yii::t('app','14 eme');
            break;
         case 0:
            return Yii::t('app','BONIS');
            break; 
           


    }
   }
  
   
 
public function anyAfterDoneForOnes($devise, $month,$date_payroll, $pers,$acad){
     
  if($month!='')
	{
	     
	     $sql = 'SELECT p.id FROM payroll p INNER JOIN payroll_settings ps ON(p.id_payroll_set=ps.id) WHERE ps.devise='.$devise.' and ps.academic_year='.$acad.' AND payroll_date >\''.$date_payroll.'\' AND YEAR(payroll_date) >= YEAR(\''.$date_payroll.'\') AND ps.person_id='.$pers;
		$info__p = Yii::$app->db->createCommand($sql)->queryAll(); 
        
        return $info__p;
        
	  }
	else
	  return null;

   
   
    }

     
 
 
 
 //************************  getSelectedLongMonthValueByPerson($person_id)  ******************************/	
 public function getSelectedLongMonthValueByPerson($person_id)
	{    
         $code = array(); 
         $modelPayroll = new SrcPayroll;
         
         $nbre_payroll = infoGeneralConfig('total_payroll');  
         
           if($person_id!='')
           {
            $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $last_payroll_month=0;
	 				  
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$last_payroll_month = $p->payroll_month;
						     	 break;
						     	 
						     }
						     
						} 
					  
					 if($last_payroll_month!=$nbre_payroll)
					  { 
						  for($i=($last_payroll_month + 1); $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name=$modelPayroll->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
					   }
					  else
					  {
					  	 for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name=$modelPayroll->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
					  	}
				     
				   }
				 else
				  {
				  	 for($i=1; $i<=$nbre_payroll; $i++)
					   {			   
						   $month_name=$modelPayroll->getSelectedLongMonth($i);
						   
						   $code[$i]= $month_name; 	  
							     
					     }  
				  	}
			   
             }
			else
			  {
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name=$modelPayroll->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  	}
			
			//pou BONIS
			$code[0]= Yii::t('app', 'BONIS'); 
			  	 	
		return $code;
         
	}   
	
	
//************************  getPastLongMonthValueByPayroll_group($grouppayroll)  ******************************/	
 public function getPastLongMonthValueByPayroll_group($grouppayroll)
	{    
         $code = array();   
         $modelPayroll=new SrcPayroll;
         
         $payroll=$modelPayroll->getMonthPastPayrollByPayroll_group($grouppayroll);
		  
			
			if($payroll!=null)
			 {
				 foreach($payroll as $p)
			       {	 
					     	if($p['payroll_month']==0)
					     	  {
					     	  	//pou BONIS
			                       $code[0]= Yii::t('app', 'BONIS');
					     	   }
					     	else
					     	   $code[$p['payroll_month']] = $modelPayroll->getSelectedLongMonth($p['payroll_month']);
				     
					}  
			  }
			
			  	 	
		return $code;
         
	}


//************************  getSelectedLongMonthValue()  ******************************/	
 public function getSelectedLongMonthValue()
	{    
         $code = array(); 
         $modelPayroll=new SrcPayroll;  
         
          $nbre_payroll = infoGeneralConfig('total_payroll');  
          
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name=$modelPayroll->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  
			//pou BONIS
			$code[0]= Yii::t('app', 'BONIS');
			  	 	
		return $code;
         
	}
        
 //************************  getSelectedLongMonthValue()  ******************************/	
 public function getSelectedLongMonthValueReport()
   {    
         $code = array(); 
         $modelPayroll=new SrcPayroll;  
         
          $nbre_payroll = infoGeneralConfig('total_payroll');  
          
			  	 for($i=1; $i<=12; $i++)
				   {			   
					   $month_name=$modelPayroll->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  
			//pou BONIS
			//$code[0]= Yii::t('app', 'BONIS');
			  	 	
		return $code;
         
	}
	
	
//************************  getPastLongMonthValueByPerson($id_payroll_set)  ******************************/	
 public function getPastLongMonthValueByPerson($id_payroll_set)
	{    
         $code = array();   
         
          $modelPayroll=new SrcPayroll;  
          
         $payroll=$modelPayroll->getMonthPastPayrollByPerson($id_payroll_set);
		        
			
			if($payroll!=null)
			 {
				 foreach($payroll as $p)
			       {	 
					    if($p['payroll_month']==0)
					      {
					            //pou BONIS
			                   $code[0]= Yii::t('app', 'BONIS');	
					       }
					    else
					     	$code[$p['payroll_month']] = $modelPayroll->getSelectedLongMonth($p['payroll_month']);
				     
					} 
			  }
			
			  	 	
		return $code;
         
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
}
