<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\billings\models\SrcPayrollSettingTaxes;

/**
 * This is the model class for table "taxes".
 *
 * @property integer $id
 * @property string $taxe_description
 * @property integer $employeur_employe
 * @property double $taxe_value
 * @property integer $particulier
 * @property integer $valeur_fixe
 * @property integer $academic_year
 *
 * @property PayrollSettingTaxes[] $payrollSettingTaxes
 */
class Taxes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'taxe_description', 'taxe_value', 'academic_year'], 'required'],
            [['id', 'employeur_employe', 'particulier', 'valeur_fixe', 'academic_year'], 'integer'],
            [['taxe_value'], 'number'],
            [['taxe_description'], 'string', 'max' => 120],
            
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'taxe_description' => Yii::t('app', 'Taxe Description'),
            'employeur_employe' => Yii::t('app', 'Employeur Employe'),
            'taxe_value' => Yii::t('app', 'Taxe Value'),
            'particulier' => Yii::t('app', 'Particulier'),
            'valeur_fixe'=>Yii::t('app', 'Valeur fixe'),
            'academic_year' => Yii::t('app', 'Academic Year'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayrollSettingTaxes()
    {
        return $this->hasMany(SrcPayrollSettingTaxes::className(), ['id_taxe' => 'id']);
    }
}
