<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\LoanOfMoney;
use app\modules\billings\models\SrcPayroll;

/**
 * This is the model class for table "loan_elements".
 *
 * @property integer $id
 * @property integer $loan_id
 * @property integer $month
 * @property double $amount
 * @property double $loan_rate
 * @property integer $is_paid
 *
 * @property LoanOfMoney $loan
 */
class LoanElements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
     
  public $loan_rate;
  
  
    public static function tableName()
    {
        return 'loan_elements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_id', 'month', 'amount'], 'required'],
            [['id','loan_id', 'month', 'is_paid'], 'integer'],
            [['amount','loan_rate'], 'number'],
            [['loan_id','month'], 'unique', 'targetAttribute' => ['loan_id','month'],  'message' => Yii::t('app', 'This combinaison "loan_id - month " has already been taken.')],
            [['loan_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcLoanOfMoney::className(), 'targetAttribute' => ['loan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'loan_id' => Yii::t('app', 'Loan ID'),
            'month' => Yii::t('app', 'Month'),
            'amount' => Yii::t('app', 'Amount'),
            'loan_rate' => Yii::t('app', 'Loan Rate'),
            'is_paid' => Yii::t('app', 'Is Paid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoan()
    {
        return $this->hasOne(SrcLoanOfMoney::className(), ['id' => 'loan_id']);
    }
    
 
 public function getAmount(){
          
            return $this->loan->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
        }
        
 public function getLoanRate($pers,$month,$acad)
      {
          $loan_rate=0;
           //al pran loan_rate payroll la
           $modelPayroll = new SrcPayroll;
          $model_pay_one = $modelPayroll->isDoneForOnes($month, $pers, $acad);
          if($model_pay_one->getModels()!=null)
           {
           	    foreach($model_pay_one->getModels() as $pay_r)
           	     {
           	     	 return $pay_r->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($pay_r->loan_rate);
           	     	}
           	   
           	}
          else
            return Yii::t('app','N/A');
        }

    	 
    	 
public function searchElementsByLoan_id($loan_id) 
  {
  	$query = LoanElements::find()->where(['loan_id'=>$loan_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

       return $dataProvider;
  	
  	
  	} 
 

 // Return the key=>value  of a month in long format 
public function getLongMonthValue(){
     return array(
        1=>Yii::t('app','January'),
        2=>Yii::t('app','February'),
        3=>Yii::t('app','March'),
        4=>Yii::t('app','April'),
        5=>Yii::t('app','May'),
        6=>Yii::t('app','June'),
        7=>Yii::t('app','July'),
        8=>Yii::t('app','August'),
        9=>Yii::t('app','September'),
        10=>Yii::t('app','October'),
        11=>Yii::t('app','November'),
        12=>Yii::t('app','December'),
        13=>Yii::t('app','13 eme'),
        14=>Yii::t('app','14 eme'),
        );  
    }
   

// Return the name of a month in long format 
public function getSelectedLongMonth($month){
    switch ($month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
        case 13:
            return Yii::t('app','13 eme');
            break;

        case 14:
            return Yii::t('app','14 eme');
            break;

    }
   }
   
  // Return the name of a month in long format 
public function getLongMonth(){
    switch ($this->month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
         case 13:
            return Yii::t('app','13 eme');
            break;
         case 14:
            return Yii::t('app','14 eme');
            break;


    }
   }

                  
// Return the name of a month in long format 
public function getElementPaid(){
    switch ($this->is_paid){
        case 0:
            return Yii::t('app','No');
            break;
        case 1:
            return Yii::t('app','Yes');
            break;
       
    }
   }

	
   
    
    
    
    
 
    
    
}
