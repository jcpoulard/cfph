<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\SrcOtherIncomes;

/**
 * SrcOtherIncomes represents the model behind the search form about `app\modules\billings\models\OtherIncomes`.
 */
class SrcOtherIncomes extends OtherIncomes
{
     public $recettesItems;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_income_description', 'devise', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['income_date', 'description', 'date_created', 'date_updated', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
         $acad=Yii::$app->session['currentId_academic_year'];
        
        $query = SrcOtherIncomes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_income_description' => $this->id_income_description,
            'amount' => $this->amount,
            'devise' => $this->devise, 
            'income_date' => $this->income_date,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['academic_year'=>$acad]);

        return $dataProvider;
    }
    
    
public function getTotalAmountOtherIncome($devise,$acad)
   {
        $t = 0;
    	  $sql='SELECT SUM(amount) as total_amount FROM other_incomes  WHERE devise='.$devise.' AND academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
          if($result!=null)
          { foreach($result as $r)
             $t = $r["total_amount"]; 
           }
           
           return $t;
   
    }



    
    
    
    
    
    
}
