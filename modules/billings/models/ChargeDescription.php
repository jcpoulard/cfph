<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\billings\models\ChargePaid;
use app\modules\billings\models\SrcChargePaid;

/**
 * This is the model class for table "charge_description".
 *
 * @property integer $id
 * @property string $description
 * @property integer $category
 * @property string $comment
 *
 * @property ChargePaid[] $chargePas
 */
class ChargeDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'charge_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'category', 'comment'], 'required'],
            [['category'], 'integer'],
            [['description'], 'string', 'max' => 65],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'category' => Yii::t('app', 'Category'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChargePas()
    {
        return $this->hasMany(SrcChargePaid::className(), ['id_charge_description' => 'id']);
    }
}
