<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "payroll_setting_taxes".
 *
 * @property integer $id
 * @property integer $id_payroll_set
 * @property integer $id_taxe
 *
 * @property PayrollSettings $idPayrollSet
 * @property Taxes $idTaxe
 */
class PayrollSettingTaxes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_setting_taxes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_payroll_set', 'id_taxe'], 'required'],
            [['id', 'id_payroll_set', 'id_taxe'], 'integer'],
            // [['','',''], 'unique', 'targetAttribute' => ['','',''],  'message' => Yii::t('app', 'This combinaison "program - fee - academic_period" has already been taken.')]
            //[['id_payroll_set'], 'exist', 'skipOnError' => true, 'targetClass' => PayrollSettings::className(), 'targetAttribute' => ['id_payroll_set' => 'id']],
            //[['id_taxe'], 'exist', 'skipOnError' => true, 'targetClass' => Taxes::className(), 'targetAttribute' => ['id_taxe' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_payroll_set' => Yii::t('app', 'Id Payroll Set'),
            'id_taxe' => Yii::t('app', 'Id Taxe'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPayrollSet()
    {
        return $this->hasOne(PayrollSettings::className(), ['id' => 'id_payroll_set']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTaxe()
    {
        return $this->hasOne(Taxes::className(), ['id' => 'id_taxe']);
    }
}
