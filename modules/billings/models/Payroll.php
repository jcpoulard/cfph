<?php

namespace app\modules\billings\models;

use Yii;

use app\modules\billings\models\SrcPayrollSettings;

/**
 * This is the model class for table "payroll".
 *
 * @property integer $id
 * @property integer $id_payroll_set
 * @property integer $id_payroll_set2
 * @property integer $payroll_month
 * @property string $payroll_date
 * @property integer $missing_hour
 * @property double $taxe
 * @property double $gross_salary
 * @property double $net_salary
 * @property double $plus_value
 * @property double $loan_rate
 * @property string $payment_date
 * @property string $cash_check
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 *
 * @property PayrollSettings $idPayrollSet
 */
class Payroll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_payroll_set', 'payroll_month', 'payroll_date', 'gross_salary', 'net_salary', 'payment_date'], 'required'],
            [['id', 'id_payroll_set', 'id_payroll_set2', 'payroll_month', 'missing_hour'], 'integer'],
            [['payroll_date', 'payment_date', 'date_created', 'date_updated'], 'safe'],
            [['taxe', 'gross_salary', 'net_salary', 'plus_value', 'loan_rate'], 'number'],
            [['cash_check'], 'string', 'max' => 45],
            [['created_by', 'updated_by'], 'string', 'max' => 65],
            [['id_payroll_set'], 'exist', 'skipOnError' => true, 'targetClass' => PayrollSettings::className(), 'targetAttribute' => ['id_payroll_set' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_payroll_set' => Yii::t('app', 'Id Payroll Set'),
            'id_payroll_set2' => Yii::t('app', 'Id Payroll Set2'),
            'payroll_month' => Yii::t('app', 'Payroll Month'),
            'payroll_date' => Yii::t('app', 'Payroll Date'),
            'missing_hour' => Yii::t('app', 'Missing Hour'),
            'taxe' => Yii::t('app', 'Taxe'),
            'gross_salary' => Yii::t('app', 'Gross Salary'),
            'net_salary' => Yii::t('app', 'Net Salary'),
            'plus_value' => Yii::t('app', 'Plus Value'),
            'loan_rate' => Yii::t('app', 'Loan Rate'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'cash_check' => Yii::t('app', 'Cash Check'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPayrollSet()
    {
        return $this->hasOne(SrcPayrollSettings::className(), ['id' => 'id_payroll_set']);
    }
}
