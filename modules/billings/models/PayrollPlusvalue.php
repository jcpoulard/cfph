<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcDevises;


/**
 * This is the model class for table "payroll_plusvalue".
 *
 * @property integer $id
 * @property integer $person_id
 * @property integer $month
 * @property double $amount
 * @property integer $devise
 * @property integer $acad
 * @property string $created_date 
 * @property string $updated_date 
 * @property string $created_by 
 * @property string $updated_by 
 *
 * @property Devises $devise0
 * @property Academicperiods $acad0
 * @property Persons $person
 */
class PayrollPlusvalue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_plusvalue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'month', 'amount','devise', 'acad'], 'required'],
            [['person_id', 'month','devise', 'acad'], 'integer'],
            [['amount'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
		    [['created_by', 'updated_by'], 'string', 'max' => 255],
		    [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']],
            [['acad'], 'exist', 'skipOnError' => true, 'targetClass' => SrcAcademicperiods::className(), 'targetAttribute' => ['acad' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'person_id' => Yii::t('app', 'Person ID'),
            'month' => Yii::t('app', 'Month'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'acad' => Yii::t('app', 'Acad'),
            'created_date' => Yii::t('app', 'Created Date'), 
		           'updated_date' => Yii::t('app', 'Updated Date'), 
		           'created_by' => Yii::t('app', 'Created By'), 
		           'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcad0()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'acad']);
    }

   /** 
    * @return \yii\db\ActiveQuery 
	*/ 
	public function getDevise0() 
	{ 
		return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
	}
		   
  /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'person_id']);
    }
}
