<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\Billings;
use app\modules\fi\models\SrcAcademicperiods;
 
/**
 * SrcBillings represents the model behind the search form about `app\modules\billings\models\Billings`.
 */
class SrcBillings extends Billings
{
   public $globalSearch;
   
   public $id_fee;
   public $fee_label; 
   public $exempt_fees;
   
   public $date_limit_payment;
   
   public $recettesItems;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'fee_period', 'academic_year', 'reservation_id','payment_method', 'fee_totally_paid'], 'integer'],
            [['amount_to_pay', 'amount_pay', 'balance'], 'number'],
            [['exempt_fees','num_chekdepo','date_pay', 'globalSearch', 'comments', 'date_created', 'date_updated', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
  public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'recettesItems' =>Yii::t('app','Recettes Items'),
                            'exempt_fees' => Yii::t('app','Exempt fees'),
		));
	}		


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcBillings::find()->where('payment_method IS NOT NULL');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'fee_period' => $this->fee_period,
            'amount_to_pay' => $this->amount_to_pay,
            'amount_pay' => $this->amount_pay,
            'balance' => $this->balance,
            'reservation_id' => $this->reservation_id, 
            'academic_year' => $this->academic_year,
            'date_pay' => $this->date_pay,
            'payment_method' => $this->payment_method,
            'num_chekdepo'=> $this->num_chekdepo,
            'fee_totally_paid' => $this->fee_totally_paid,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
 
  public function searchByAcad($acad)
    {
        $query = SrcBillings::find()->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',])->where('payment_method IS NOT NULL AND academic_year='.$acad)->orderBy(['date_pay'=>SORT_DESC,'fees.date_limit_payment'=>SORT_ASC])->groupBy(['student']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
        return $dataProvider;
    }
 

 
 public function searchByMonth($condition,$month,$year,$acad)
    {
        $query = SrcBillings::find()->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',])->where('payment_method IS NOT NULL AND '.$condition.' and MONTH(date_pay)='.$month.' and YEAR(date_pay)='.$year.' and academic_year='.$acad)->orderBy(['date_pay'=>SORT_DESC,'fees.date_limit_payment'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
              
        return $dataProvider;
    }
    
    
  public function searchForView($student,$acad)
    {
       $modelAcad = SrcAcademicperiods::findOne($acad); 
      
        $query = SrcBillings::find()->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',])
                ->where('payment_method IS NOT NULL AND student='.$student.' AND ( (billings.date_created>=\''.$modelAcad->date_start.'\' AND billings.date_created<=\''.$modelAcad->date_end.'\') OR (billings.date_updated>=\''.$modelAcad->date_start.'\' AND billings.date_updated<=\''.$modelAcad->date_end.'\') )')->orderBy(['date_pay'=>SORT_DESC,'fees.date_limit_payment'=>SORT_ASC]);

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000,
        ],
        ]);
        
        //$query->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',]);
        //$query->andFilterWhere(['student'=> $student])
         //     ->andFilterWhere(['date_pay>='.$modelAcad->date_start.' and date_pay<='.$modelAcad->date_end]);
               // ->andFilterWhere(['<=', 'date_pay',$modelAcad->date_end]);
          //  ->andFilterWhere(['academic_year'=> $acad]);   // date_pay nan acad,pou jwenn payment anreta yo

        return $dataProvider;
    }
    
    public function searchLastYearForView($student,$acad)
    {
       $modelAcad = SrcAcademicperiods::findOne($acad); 
      
        $query = SrcBillings::find()->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',])
                ->where('payment_method IS NOT NULL AND student='.$student.' AND ( (billings.date_pay>=\''.$modelAcad->date_start.'\' AND billings.date_pay<=\''.$modelAcad->date_end.'\')  )')->orderBy(['date_pay'=>SORT_DESC,'fees.date_limit_payment'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000,
        ],
        ]);
        
        //$query->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',]);
        //$query->andFilterWhere(['student'=> $student])
         //     ->andFilterWhere(['date_pay>='.$modelAcad->date_start.' and date_pay<='.$modelAcad->date_end]);
               // ->andFilterWhere(['<=', 'date_pay',$modelAcad->date_end]);
          //  ->andFilterWhere(['academic_year'=> $acad]);   // date_pay nan acad,pou jwenn payment anreta yo

        return $dataProvider;
    }

    

  
    
public function searchForBalance($id_stud) 
	{
					
		$query = SrcBillings::find()->where('((amount_to_pay=balance)) AND student='.$id_stud);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000,
        ],
        ]);
        
        $query->joinWith(['student0','feePeriod','paymentMethod', 'feePeriod.fee0', 'feePeriod.devise0']);
        

        return $dataProvider;
		
	}


	
public function searchForBalance2($id_stud) 
	{
					
		$query = SrcBillings::find()->where('((balance > 0) AND (amount_to_pay<>balance) ) AND fee_totally_paid=0 AND student='.$id_stud)->orderBy(['billings.id'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000,
        ],
        ]);
        
        $query->joinWith(['student0','feePeriod','paymentMethod', 'feePeriod.fee0', 'feePeriod.devise0']);
        

        return $dataProvider;
		
	}
	
public function lastTransactionDate()	
	{
					
		$sql = "SELECT DATE_FORMAT(MAX(date_created),'%Y/%m/%d') AS denyedatecreate, DATE_FORMAT(MAX(date_updated),'%Y/%m/%d') AS denyedatupdate FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE amount_pay<>0";
			    $result = Yii::$app->db->createCommand($sql)->queryAll();
        

        return $result;
		
	}	   
  public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;

    $query = SrcBillings::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }
     
    $query->joinWith(['paymentMethod','academicYear','feePeriod', 'feePeriod.fee0', 'feePeriod.devise0','student0',]);
    $query->orFilterWhere(['like', 'fees_label.fee_label', $this->globalSearch])
            ->orFilterWhere(['like', 'payment_method.method_name', $this->globalSearch])
            ->orFilterWhere(['like', 'devises.devise_name', $this->globalSearch])
            ->orFilterWhere(['like', 'persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'persons.last_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    
  }    
    
    
   public function checkForBalance($stud, $fee_period, $status, $acad_year){
            
			$bill = new SrcBillings();
		        $result= $bill->find()
		                      ->orderBy(['billings.id'=>SORT_DESC])
		                      ->limit(1)
		                      ->select(['billings.id','billings.amount_pay','billings.balance'])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where(['fees_label.status'=>$status])
		                      ->andWhere(['student'=>$stud])
		                      ->andWhere(['billings.fee_period'=>$fee_period])
		                      ->andWhere(['billings.academic_year'=>$acad_year])
		                      ->all();
		        
		        
		
						return $result; 
        }
    
    
//return a integer (id)
public function getLastTransactionIDWithoutDevise($student, $condition_fee_status,$acad)
	  {
	  	  
	  	  $bill = new SrcBillings();
		        $result= $bill->find()
		                      ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
		                      ->select(['billings.id','fees.date_limit_payment',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where($condition_fee_status)
		                      ->andWhere(['student'=>$student])
		                       ->andWhere(['<>','billings.amount_pay',0])
		                      ->andWhere(['<>','billings.amount_to_pay','billings.balance'])
		                      ->andWhere(['billings.academic_year'=>$acad])
		                      ->all();
		   
           $last_payment_date = null;
           $last_id = null;
           
           if($result!=null)
             { 
             	 
             	 foreach($result as $r)
             	  { 
             	  	if($last_payment_date==null)
             	  	  { $last_payment_date=  $r->date_limit_payment;
             	  	     $last_id = $r->id;
             	  	   }
             	  	   if(($last_payment_date) < ($r->date_limit_payment))
             	  	     { $last_payment_date =  $r->date_limit_payment;
             	  	        $last_id = $r->id;
             	  	     }
             	  	   else
             	  	     {   if( ($last_id) < ($r->id) )
             	  	     	       $last_id = $r->id;
             	  	     	
             	  	     	}
             	   
               	  }
               	  
               	 return $last_id;
             	  
             }
           else
             return  null;
             
            
     }


     
//return a integer (id)
public function getLastTransactionID($student, $condition_fee_status, $currency_id, $acad)
	  {
	  	  
	  	  $bill = new SrcBillings();
		        $result= $bill->find()
		                      ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
		                      ->select(['billings.id','fees.date_limit_payment',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where($condition_fee_status)
		                      ->andWhere(['student'=>$student])
		                      ->andWhere(['devise'=>$currency_id])
		                       ->andWhere(['<>','billings.amount_pay',0])
		                      ->andWhere(['<>','billings.amount_to_pay','billings.balance'])
		                      ->andWhere(['billings.academic_year'=>$acad])
		                      ->all();
		   
           $last_payment_date = null;
           $last_id = null;
           
           if($result!=null)
             { 
             	 
             	 foreach($result as $r)
             	  { 
             	  	if($last_payment_date==null)
             	  	  { $last_payment_date=  $r->date_limit_payment;
             	  	     $last_id = $r->id;
             	  	   }
             	  	   if(($last_payment_date) < ($r->date_limit_payment))
             	  	     { $last_payment_date =  $r->date_limit_payment;
             	  	        $last_id = $r->id;
             	  	     }
             	  	   else
             	  	     {   if( ($last_id) < ($r->id) )
             	  	     	       $last_id = $r->id;
             	  	     	
             	  	     	}
             	   
               	  }
               	  
               	 return $last_id;
             	  
             }
           else
             return  null;
             
            
     }
     
     
     //return a integer (id)
public function getLastTransactionIDForGuest($student, $condition_fee_status, $acad)
        
	  {
	  	  
	  	  $bill = new SrcBillings();
		        $result= $bill->find()
		                      ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
		                      ->select(['billings.id','fees.date_limit_payment',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where($condition_fee_status)
		                      ->andWhere(['student'=>$student])
		                       ->andWhere(['<>','billings.amount_pay',0])
		                      ->andWhere(['<>','billings.amount_to_pay','billings.balance'])
		                      ->andWhere(['billings.academic_year'=>$acad])
		                      ->all();
		   
           $last_payment_date = null;
           $last_id = null;
           
           if($result!=null)
             { 
             	 
             	 foreach($result as $r)
             	  { 
             	  	if($last_payment_date==null)
             	  	  { $last_payment_date=  $r->date_limit_payment;
             	  	     $last_id = $r->id;
             	  	   }
             	  	   if(($last_payment_date) < ($r->date_limit_payment))
             	  	     { $last_payment_date =  $r->date_limit_payment;
             	  	        $last_id = $r->id;
             	  	     }
             	  	   else
             	  	     {   if( ($last_id) < ($r->id) )
             	  	     	       $last_id = $r->id;
             	  	     	
             	  	     	}
             	   
               	  }
               	  
               	 return $last_id;
             	  
             }
           else
             return  null;
             
            
     }

     //return a integer (id)
public function getLastTransactionIDForStudentInfo($student, $condition_fee_status, $acad)
 {
         $last_payment_date = null;
           $last_id = null;
           
        $modDevise = SrcBillings::find()->all();
               
          if($modDevise!=null)
           {   
               foreach ($modDevise as $devise)
                 {
	  	     $bill = new SrcBillings();
		        $result= $bill->find()
		                      ->orderBy(['fees.date_limit_payment'=>SORT_ASC])
		                      ->select(['billings.id','fees.date_limit_payment',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where($condition_fee_status)
		                      ->andWhere(['devise'=>$devise])
		                      ->andWhere(['student'=>$student])
		                       ->andWhere(['<>','billings.amount_pay',0])
		                      ->andWhere(['<>','billings.amount_to_pay','billings.balance'])
		                      ->andWhere(['billings.academic_year'=>$acad])
		                      ->all();
		   
          
           
           if($result!=null)
             { 
             	 
             	 foreach($result as $r)
             	  { 
             	  	if($last_payment_date==null)
             	  	  { $last_payment_date=  $r->date_limit_payment;
             	  	     $last_id = $r->id;
             	  	   }
             	  	   if(($last_payment_date) < ($r->date_limit_payment))
             	  	     { $last_payment_date =  $r->date_limit_payment;
             	  	        $last_id = $r->id;
             	  	     }
             	  	   else
             	  	     {   if( ($last_id) < ($r->id) )
             	  	     	       $last_id = $r->id;
             	  	     	
             	  	     	}
             	   
               	  }
               	  
               	 
             	  
             }
          
           
              
                 }
           
           
           
           
         }
             
       return $last_id;
       
     }

   
    
   public function getAmountToPay(){
           
           $currency_name = Yii::$app->session['currencyName'];
	       $currency_symbol = Yii::$app->session['currencySymbol']; 
           
            //return $currency_symbol.' '.numberAccountingFormat($this->amount_to_pay); //retirel le w aktive devise nan form lan
            return $this->feePeriod->devise0->devise_symbol.' '.numberAccountingFormat($this->amount_to_pay);
        }
    
public function getAmountPay(){
           
           $currency_name = Yii::$app->session['currencyName'];
	       $currency_symbol = Yii::$app->session['currencySymbol']; 
           
            //return $currency_symbol.' '.numberAccountingFormat($this->amount_pay); //retirel le w aktive devise nan form lan
            return $this->feePeriod->devise0->devise_symbol.' '.numberAccountingFormat($this->amount_pay);
        }    
    
 public function getBalance(){
           
           $currency_name = Yii::$app->session['currencyName'];
	       $currency_symbol = Yii::$app->session['currencySymbol']; 
           
            //return $currency_symbol.' '.numberAccountingFormat($this->balance); //retirel le w aktive devise nan form lan
            return $this->feePeriod->devise0->devise_symbol.' '.numberAccountingFormat($this->balance);
        }   
    
    
    
//return id_fee, fee_name
	public function searchFullPaidFeeByStudentId($student, $status, $acad)
	  {
	  	    $level= getLevelByStudentId($student);
	    
	  	  $query_fee_in_scholarship = SrcScholarshipHolder::find()->select('fee')
	                ->where(['student'=>$student])
	                ->andWhere(['percentage_pay'=>100])
	                ->andWhere(['partner'=>NULL])
	                ->andWhere(['academic_year'=>$acad]);
	                
	  	              $bill = new SrcFees();
		        $result= $bill->find()
		                      ->joinWith(['fee0'])
		                      ->select(['fees.id as id_fee','fees_label.fee_label',])
		                      ->where(['fees_label.status'=>$status])
		                      ->andWhere(['level'=>$level])
		                      ->andWhere(['in','fees.id',$query_fee_in_scholarship])
		                      ->all();
           
                        
           return $result;
           
	  	}

public function searchPastFullPaidFeeByStudentId($student,$level, $status, $acad)
	  {
	  	   
	    
	  	  $query_fee_in_scholarship = SrcScholarshipHolder::find()->select('fee')
	                ->where(['student'=>$student])
	                ->andWhere(['percentage_pay'=>100])
	                ->andWhere(['partner'=>NULL])
	                ->andWhere(['academic_year'=>$acad]);
	                
	  	              $bill = new SrcFees();
		        $result= $bill->find()
		                      ->joinWith(['fee0'])
		                      ->select(['fees.id as id_fee','fees_label.fee_label',])
		                      ->where(['fees_label.status'=>$status])
		                      ->andWhere(['level'=>$level])
		                      ->andWhere(['in','fees.id',$query_fee_in_scholarship])
		                      ->all();
           
                        
           return $result;
           
	  	}
                
                
//return id_fee, fee_name
	public function searchPaidFeesByStudentId($student, $status, $acad)
	  {
	  	  
	  	  //$sql = "SELECT DISTINCT fee_period as id_fee, fl.fee_label FROM billings INNER JOIN fees f ON(fee_period = f.id) INNER JOIN fees_label fl ON(f.fee=fl.id)  WHERE fl.status=".$status." AND student=".$student." AND balance <= 0 AND academic_period=".$acad;
										
          $bill = new SrcBillings();
          $result= $bill->find()
		                      ->select(['billings.fee_period as id_fee','fees_label.fee_label',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where(['fees_label.status'=>$status])
		                      ->andWhere(['billings.student'=>$student])
		                      ->andWhere(['fees.academic_period'=>$acad])
		                      ->andWhere('billings.balance <= 0 and fee_totally_paid=1')
		                      ->all();
           
                        
           return $result;

           
	  	} 

//return id_fee, fee_name
	public function searchPaidFeesByStudentIdDevise($student, $status,$devise, $acad)
	  {
	  	  
	  	  //$sql = "SELECT DISTINCT fee_period as id_fee, fl.fee_label FROM billings INNER JOIN fees f ON(fee_period = f.id) INNER JOIN fees_label fl ON(f.fee=fl.id)  WHERE fl.status=".$status." AND student=".$student." AND balance <= 0 AND academic_period=".$acad;
										
          $bill = new SrcBillings();
          $result= $bill->find()
		                      ->select(['billings.fee_period as id_fee','fees_label.fee_label',])
		                      ->joinWith(['feePeriod','feePeriod.fee0'])
		                      ->where(['fees_label.status'=>$status])
		                      ->andWhere(['billings.student'=>$student])
		                      ->andWhere(['fees.devise'=>$devise])
		                      ->andWhere(['fees.academic_period'=>$acad])
		                      ->andWhere(['<=','billings.balance',0])
		                      ->all();
           
                        
           return $result;

           
	  	}

//return id_fee, fee_name
	public function searchPendingFeesByStudentId($program, $level, $acad)
	  {
										
          $bill = new SrcFees();
          $result= $bill->find()
		                      ->select(['fees.id as id_fee','fees_label.fee_label','fees.amount'])
		                      ->joinWith(['fee0'])
		                      ->where(['fees_label.status'=>1])
		                      ->andWhere(['fees.program'=>$program])
		                      ->andWhere(['fees.level'=>$level])
		                      ->andWhere(['fees.academic_period'=>$acad])
		                      ->all();
           
                        
           return $result;
           
	  	}


public function searchTodayIncomeFee($date_)
	{
										
          $bill = new SrcBillings();
          $result= $bill->find()
		                      ->joinWith(['feePeriod.fee0'])
		                      ->where(['fees_label.status'=>1])
		                      ->andWhere('(date_created=\''.$date_.'\') OR(date_updated=\''.$date_.'\')')
                                      ->groupBy('student')
		                      ->all();
           
                        
           return $result;
           
	  	}
	

public function getPreviousFeeBalance($stud, $program,$date_limit_fee, $acad_year){
            
		     $fee_period =0; 
		  
		   $level= getLevelByStudentId($stud);
		      		      
		  $fee = new SrcFees();
          $result= $fee->find()
                              ->orderBy(['fees.date_limit_payment'=>SORT_DESC])
                              ->limit(1)
		                      ->select(['fees.id'])
		                      ->joinWith(['fee0'])
		                      ->where(['<','date_limit_payment',$date_limit_fee])
		                      ->andWhere(['fees.program'=>$program])
		                      ->andWhere(['fees.level'=>$level])
		                      ->andWhere(['fees.academic_period'=>$acad_year])
		                      ->all();
           
           if($result!=null)
             {  foreach($result as $r)
                 {  $fee_period = $r->id;
                   break;
                 }
             }
           
	  
		$bill = new SrcBillings;
		        $result1=$bill->find()
		                              ->orderBy(['billings.id'=>SORT_DESC])
		                              ->limit(1)
				                      ->select(['billings.id','billings.amount_pay','billings.balance'])
				                      ->joinWith(['feePeriod','feePeriod.fee0'])
				                      ->where(['billings.student'=>$stud])
				                      ->andWhere(['billings.fee_period'=>$fee_period])
				                      ->andWhere(['billings.academic_year'=>$acad_year])
				                      ->all();
		        
		        
		        
								
						
						
						return $result1;
						
						
        } 

        public function getPreviousFeeBalanceByDevise($stud, $program,$date_limit_fee,$devise, $acad_year){
            
		     $fee_period =0; 
		  
		   $level= getLevelByStudentId($stud);
		      		      
		  $fee = new SrcFees();
          $result= $fee->find()
                              ->orderBy(['fees.date_limit_payment'=>SORT_DESC])
                              ->limit(1)
		                      ->select(['fees.id'])
		                      ->joinWith(['fee0'])
		                      ->where(['<','date_limit_payment',$date_limit_fee])
		                      ->andWhere(['fees.program'=>$program])
                                      ->andWhere(['fees.devise'=>$devise])
		                      ->andWhere(['fees.level'=>$level])
		                      ->andWhere(['fees.academic_period'=>$acad_year])
		                      ->all();
           
           if($result!=null)
             {  foreach($result as $r)
                 {  $fee_period = $r->id;
                   break;
                 }
             }
           
	  
		$bill = new SrcBillings;
		        $result1=$bill->find()
		                              ->orderBy(['billings.id'=>SORT_DESC])
		                              ->limit(1)
				                      ->select(['billings.id','billings.amount_pay','billings.balance'])
				                      ->joinWith(['feePeriod','feePeriod.fee0'])
				                      ->where(['billings.student'=>$stud])
				                      ->andWhere(['billings.fee_period'=>$fee_period])
				                      ->andWhere(['billings.academic_year'=>$acad_year])
				                      ->all();
		        
		        
		        
								
						
						
						return $result1;
						
						
        } 
        
        
 //return 0: false; 1: true
  public function isChekDepoUnique($num_chekdepo,$devise)
     {
          if($num_chekdepo!=null)
          {
           $bill = new SrcBillings();
           $sql = "SELECT b.id FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE devise=".$devise." AND num_chekdepo='".$num_chekdepo."'";
			    $result = Yii::$app->db->createCommand($sql)->queryAll();
           
             if($result==null)          
               return 0;
             else
                 return 1;
             
          }
          else 
            {
              return 0;
              }
              
              
    }
    
//pou scholarship
public function isFeeAlreadyUse($stud,$fee_check, $acad)
	  {
	  	  if($fee_check==NULL)//gade si elev la gentan nan tab billings lan pou ane acad
			  { $sql = "SELECT b.id FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE b.student=".$stud." AND f.academic_period=".$acad." AND b.academic_year=".$acad;
			  }
		  else            //gade si elev la gen tranzaksyon sou fre sa
			 {  $sql = "SELECT b.id FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE b.student=".$stud." AND f.id=".$fee_check." AND f.academic_period=".$acad." AND b.academic_year=".$acad;
			 }
          
	  	  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                        
           return $result;
           
	  	}

 //return id , amount_pay               
public function isFeeAlreadyUse_new($stud,$fee_check, $acad)
	  {
	  	  if($fee_check==NULL)//gade si elev la gentan nan tab billings lan pou ane acad
			  { $sql = "SELECT b.id, amount_pay FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE b.student=".$stud." AND f.academic_period=".$acad." AND b.academic_year=".$acad;
			  }
		  else            //gade si elev la gen tranzaksyon sou fre sa
			 {  $sql = "SELECT b.id, amount_pay FROM billings b INNER JOIN fees f ON(f.id=b.fee_period) WHERE b.student=".$stud." AND f.id=".$fee_check." AND f.academic_period=".$acad." AND b.academic_year=".$acad;
			 }
          
	  	  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                        
           foreach($result as $r)
              return $r;
           
	  	}

//return a double as balance		
public function updateMainBalance($student, $devise, $acad)
	{   	    
       $sql='SELECT SUM(b.balance) as sum_balance FROM billings b inner join fees f on(f.id=b.fee_period) WHERE b.student ='.$student.' AND devise='.$devise.' AND b.academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
           foreach($result as $r)
            return $r["sum_balance"];
            
 }

	//return a integer (id)
public function getLastTransactionIdByFeeId($student, $fee_id, $acad)
	  {
	  	  
	  	  $sql = "SELECT  b.id, f.date_limit_payment FROM billings b INNER JOIN fees f ON(b.fee_period= f.id) WHERE f.id=".$fee_id." AND b.student=".$student." AND (b.amount_to_pay <> b.balance) AND b.academic_year=".$acad.' ORDER BY f.date_limit_payment ASC';
										
          $command = Yii::$app->db->createCommand($sql);
 
           $result = $command->queryAll();
           
           $last_payment_date = null;
           $last_id = null;
           
           if($result!=null)
             {
             	 
             	 foreach($result as $r)
             	  { 
             	  	if($last_payment_date==null)
             	  	  { $last_payment_date=  $r['date_limit_payment'];
             	  	     $last_id = $r['id'];
             	  	   }
             	  	   if($last_payment_date < $r['date_limit_payment'])
             	  	     { $last_payment_date=  $r['date_limit_payment'];
             	  	        $last_id = $r['id'];
             	  	     }
             	  	   else
             	  	     {   if($last_id < $r['id'])
             	  	     	       $last_id = $r['id'];
             	  	     	
             	  	     	}
             	   
               	  }
               	  
               	 return $last_id;
             	  
             }
           else
             return  null;
             
            
     }


//return a double-> sum_amount_pay
 public function searchTuitionFeeByLevelDevise($level, $devise, $acad)
	  {
	  	  
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND level='.$level.' AND devise='.$devise.' AND b.academic_year='.$acad.' group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                        
        if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
	  	} 
             
                
//return a double-> sum_amount_pay
 public function searchTuitionFeeByProgram($program, $devise, $acad)
	  {
	  $modelAcad = SrcAcademicperiods::findOne($acad); 	  
	  									
          $bill = new SrcBillings();
         //$sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND program='.$program.' AND devise='.$devise.' AND b.academic_year='.$acad.' group by devise order by devise';
	$sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND program='.$program.' AND devise='.$devise.' AND (b.date_pay>=\''.$modelAcad->date_start.'\' AND b.date_pay<=\''.$modelAcad->date_end.'\') group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                        
        if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
	  	} 
                
//return a double-> sum_amount_to_pay
 public function searchTuitionFeeExpectedByProgram($program, $devise, $acad)
	  {
	  	  
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_to_pay) as sum_amount_to_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND program='.$program.' AND devise='.$devise.' AND b.academic_year='.$acad.' group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                        
           if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_to_pay"];
             
           }
         }
         else
               return 0;

           
	  	} 
                
   public function searchTuitionFeeAllProgram($acad)
     {
        $modelAcad = SrcAcademicperiods::findOne($acad); 
      
        
           $bill = new SrcBillings();
         //$sql='SELECT SUM(amount_pay) as sum_amount_pay, devise FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND  b.academic_year='.$acad.' group by devise order by devise';
	$sql='SELECT SUM(amount_pay) as sum_amount_pay, devise FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND ( b.date_pay >=\''.$modelAcad->date_start.'\' AND b.date_pay <=\''.$modelAcad->date_end.'\') group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                       
           return $result;
    }

       public function searchTuitionFeeExpectedAllProgram($acad)
     {
           
           $bill = new SrcBillings();
         $sql='SELECT SUM(amount_to_pay) as sum_amount_to_pay, devise FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) WHERE fl.status=1 AND  b.academic_year='.$acad.' group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
                       
           return $result;
    }

 //return a double-> sum_amount_pay
 public function searchTuitionFeeByProgramAndMonth($program, $month, $devise, $acad)    
     {
	  	  
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee)  WHERE fl.status=1 AND program='.$program.' AND devise='.$devise.' AND MONTH(date_pay)='.$month.' AND b.academic_year='.$acad.' group by devise order by devise';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
   } 
     
  
   //return a double-> sum_amount_pay
 public function getTotalIncomeInShiftByProgramDevise($shift, $program, $devise, $acad)    
     {
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) inner join student_other_info soi on(soi.student=b.student) WHERE fl.status=1 AND apply_shift='.$shift.' AND program='.$program.' AND devise='.$devise.' AND b.academic_year='.$acad.'  AND f.academic_period='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
   } 
   
   
 public function getWholeIncomeFee($devise, $acad)    
    {
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) inner join student_other_info soi on(soi.student=b.student) WHERE fl.status=1 AND devise='.$devise.' AND b.academic_year='.$acad.'  AND f.academic_period='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
   }     
  
 public function getDailyWholeIncomeFee($devise, $date_du_jour)    
    {
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) inner join student_other_info soi on(soi.student=b.student) WHERE fl.status=1 AND devise='.$devise.' AND ( (b.date_created=\''.$date_du_jour.'\') OR(b.date_updated=\''.$date_du_jour.'\') )';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
   }   
   
 public function getTotalIncomeFeeByShift($shift, $devise, $acad)   
      {
	  									
          $bill = new SrcBillings();
         $sql='SELECT SUM(amount_pay) as sum_amount_pay FROM billings b inner join fees f on(f.id=b.fee_period) inner join fees_label fl on(fl.id=f.fee) inner join student_other_info soi on(soi.student=b.student) WHERE fl.status=1 AND apply_shift='.$shift.' AND devise='.$devise.' AND b.academic_year='.$acad.'  AND f.academic_period='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         if($result!=null)
         {
           foreach($result as $r)
           { 
               return $r["sum_amount_pay"];
             
           }
         }
         else
               return 0;

           
   } 
   
   
   
   
   
   
   
    
}
