<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcDevises;

/**
 * This is the model class for table "payroll_settings".
 *
 * @property integer $id
 * @property integer $person_id
 * @property double $amount
 * @property integer $devise
 * @property integer $an_hour
 * @property integer $number_of_hour
 * @property double $frais 
 * @property double $assurance_value 
 * @property integer $academic_year
 * @property integer $as_
 * @property integer $old_new
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 *
 * @property Payroll[] $payrolls
 * @property PayrollSettingTaxes[] $payrollSettingTaxes
 * @property Devises $devise0 
 * @property Academicperiods $academicYear
 * @property Persons $person
 */
class PayrollSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payroll_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'person_id', 'amount', 'number_of_hour', 'academic_year', 'date_created', 'date_updated', 'created_by', 'updated_by'], 'required'],
            [['id', 'person_id', 'an_hour', 'number_of_hour', 'academic_year', 'as_', 'old_new'], 'integer'],
            [['amount', 'assurance_value', 'frais'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 65],
           // [['','',''], 'unique', 'targetAttribute' => ['','',''],  'message' => Yii::t('app', 'This combinaison "program - fee - academic_period" has already been taken.')]
           // [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']],
           // [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => SrcAcademicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            //[['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'person_id' => Yii::t('app', 'Persons'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'an_hour' => Yii::t('app', 'An Hour'),
            'number_of_hour' => Yii::t('app', 'Number Of Hour'),
            'frais' => Yii::t('app', 'Frais'),
            'assurance_value' => Yii::t('app', 'Assurance Value'), 
		    'academic_year' => Yii::t('app', 'Academic Year'),
            'as_' => Yii::t('app', 'As'),
            'old_new' => Yii::t('app', 'Old New'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayrolls()
    {
        return $this->hasMany(SrcPayroll::className(), ['id_payroll_set' => 'id']);
    }

   /**
		    * @return \yii\db\ActiveQuery
		    */
	public function getDevise0() 
	 { 
		 return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
	 } 
		 
   /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayrollSettingTaxes()
    {
        return $this->hasMany(PayrollSettingTaxes::className(), ['id_payroll_set' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'person_id']);
    }
}
