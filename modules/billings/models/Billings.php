<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;
use app\modules\billings\models\PaymentMethod;
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\billings\models\Reservation;




/**
 * This is the model class for table "billings".
 *
 * @property integer $id
 * @property integer $student
 * @property integer $fee_period
 * @property double $amount_to_pay
 * @property double $amount_pay
 * @property double $balance
  * @property Reservation $reservation
 * @property integer $academic_year
 * @property string $date_pay
 * @property integer $payment_method
 * @property string $comments
 * @property string $num_chekdepo
 * @property integer $fee_totally_paid
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 *
 * @property PaymentMethod $paymentMethod
 * @property Academicperiods $academicYear
 * @property Fees $feePeriod
 * @property Persons $student0
 */
class Billings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'fee_period', 'amount_to_pay', 'amount_pay', 'academic_year', 'date_pay'], 'required'],
            [['student', 'fee_period', 'academic_year', 'reservation_id','payment_method', 'fee_totally_paid'], 'integer'],
            [['amount_to_pay', 'amount_pay', 'balance'], 'number'],
            [['date_pay', 'date_created', 'date_updated'], 'safe'],
            [['comments'], 'string', 'max' => 255],
            [['created_by', 'updated_by', 'num_chekdepo'], 'string', 'max' => 64],
            [['payment_method'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::className(), 'targetAttribute' => ['payment_method' => 'id']],
            [['reservation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reservation::className(), 'targetAttribute' => ['reservation_id' => 'id']],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['fee_period'], 'exist', 'skipOnError' => true, 'targetClass' => Fees::className(), 'targetAttribute' => ['fee_period' => 'id']],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'fee_period' => Yii::t('app', 'Fee Period'),
            'amount_to_pay' => Yii::t('app', 'Amount To Pay'),
            'amount_pay' => Yii::t('app', 'Amount Pay'),
            'balance' => Yii::t('app', 'Balance'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_pay' => Yii::t('app', 'Date Pay'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'reservation_id' => Yii::t('app', 'Reservation ID'),
            'comments' => Yii::t('app', 'Comments'),
            'num_chekdepo'=> Yii::t('app', '# check/deposit'),
            'fee_totally_paid' => Yii::t('app', 'Fee Totally Paid'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(SrcPaymentMethod::className(), ['id' => 'payment_method']);
    }
    
    public function getReservation()
		   {
		       return $this->hasOne(SrcReservation::className(), ['id' => 'reservation_id']);
		   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeePeriod()
    {
        return $this->hasOne(SrcFees::className(), ['id' => 'fee_period']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }
}
