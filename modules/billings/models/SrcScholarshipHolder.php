<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\ScholarshipHolder;
use app\modules\billings\models\SrcPartners;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcDevises; 
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcAcademicperiods;

/**
 * SrcScholarshipHolder represents the model behind the search form about `app\modules\billings\models\ScholarshipHolder`.
 */
class SrcScholarshipHolder extends ScholarshipHolder
{
    public $sponsor;
    public $program;
    public $globalSearch;
    public $student_fullname;
public $partner_name;
public $is_full;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'partner', 'fee', 'is_full', 'is_internal', 'academic_year'], 'integer'],
            [['percentage_pay'], 'number'],
            ['percentage_pay', 'compare', 'compareValue' => 100, 'operator' => '<=', 'type' => 'number', 'message' => 'Percentage to pay can\'t be gerater than 100 !'],
            ['percentage_pay', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number', 'message' => 'Percentage to pay must be gerater than 0 !'],
            
            [['comment', 'globalSearch', 'student_fullname', 'partner_name', 'sponsor', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
           // [['student','fee','academic_year'], 'unique', 'targetAttribute' => ['student','fee','academic_year'],  'message' => Yii::t('app', 'This combinaison "student - fee - academic_year" has already been taken.')],
        ];
    }


 public function attributeLabels()
    {
        return array_merge(
		    	parent::attributeLabels(), [
            'sponsor' => Yii::t('app', 'Donor'),
            'partner_name' => Yii::t('app', 'Partner'),
            'program' => Yii::t('app', 'Program'),
            'is_full'=> Yii::t('app', 'Is Full'),
            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcScholarshipHolder::find();
//$criteria->condition = 'sh.comment IS NULL AND academic_year='.$acad;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'partner' => $this->partner,
            'fee' => $this->fee,
            'percentage_pay' => $this->percentage_pay,
            'is_internal' => $this->is_internal,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
  public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;
 

    $query = SrcScholarshipHolder::find()->groupBy('student');
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

   $query->joinWith(['academicYear', 'student0',]);
    $query->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
 
 public function searchForOneByAcad($student,$acad)
         {
        $query = SrcScholarshipHolder::find()->where('student='.$student.' AND academic_year='.$acad);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

          
        return $dataProvider;
    }
         
         
	public function feeNotNullByStudentId($stud,$acad)
	{
		$query = SrcScholarshipHolder::find()
		                                ->joinWith(['student0','academicYear'])
		                                ->where(['student'=>$stud])
		                                ->andWhere(['academic_year'=>$acad])->all();
		/*                                
		$dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100,
             ],
           ]);
		            
		*/

		return $query;//$dataProvider->getModels();
	}


public function getScholarshipPartnerByStudentIdFee($stud,$fee,$acad)
	{
		if($fee==NULL)
			{ 
			    $query = SrcScholarshipHolder::find()
		                                ->joinWith(['student0','academicYear'])
		                               ->andWhere(['student'=>$stud])
		                                ->andWhere(['academic_year'=>$acad]);
			}
		else
		   { $query = SrcScholarshipHolder::find()
		                                ->joinWith(['student0','academicYear'])
		                                ->where(['fee'=>$fee ])
		                                ->andWhere(['student'=>$stud])
		                                ->andWhere(['academic_year'=>$acad]);
		     }
		
		$dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100,
             ],
           ]);
		            
		

		return $dataProvider->getModels();
	}


	public function search_exemption($acad,$params)
	{
		$modelAcad = SrcAcademicperiods::findOne($acad); 
                     
                $query = SrcScholarshipHolder::find()
                        ->alias('sh')
                         ->joinWith(['academicYear', 'student0',])
                         ->where('sh.comment IS NOT NULL')
                         ->andWhere(['academic_year'=>$acad])
                         ->orWhere('sh.date_created>=\''.$modelAcad->date_start.'\' AND sh.date_created<=\''.$modelAcad->date_end.'\'')
                         ->orderBy(['partner'=>SORT_ASC,'persons.last_name'=>SORT_ASC,'fee'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

  // $query->joinWith(['academicYear', 'student0',]);
    $query->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
		
		
	
	}

        
public function getIsScholarshipHolder($stud,$acad) //
	  {
			  $model_scholarship = SrcScholarshipHolder::find()
			                       ->where(['student' =>$stud])
			                       ->andWhere(['academic_year'=>$acad])
			                       ->all();
														           	  
			  if($model_scholarship != null)
				 return 1;
			  else
				 return 0;
				   
			
                
			}
    
public function getPartner()   
 {
        $partner ='';
        
        if($this->partner == '')
          {
          	  $school_name = infoGeneralConfig('school_name');
			  
			  $partner = $school_name;
           }
        else
          {
        
		        $partner_ = SrcPartners::findOne($this->partner);
				    
			     $partner = $partner_->name;
            }
            
		return $partner;
		
	}   
    
public function getFee()
	{
        $fee_name ='';
        
        if($this->fee == NULL)
          {
          	  
			  $fee_name = Yii::t('app','All fees');
           }
        else
          {
   		        $fee_name_ = SrcFees::findOne($this->fee);
				    
			     $fee_name = $fee_name_->fee0->fee_label;
            }
            
		return $fee_name; 
		
	 }


public function getAmountForPercentage($percentage,$fee,$program,$acad)
      {         
      	    $tot_amount =0;
      	    //return 0 or the default model 
      	    $modelDevise = new SrcDevises();
      	    $default_devise = $modelDevise->checkDehaultSet();
      	    
      	      
      	     if($fee==null)
      	     { 
      	     	$fee_info_ = Yii::$app->db->createCommand('SELECT amount FROM fees WHERE  program='.$program.' AND academic_period='.$acad.' AND devise='.$default_devise->id)->queryAll();
									                                               		
					
			    if($fee_info_!=null)
			      { 
			      	foreach($fee_info_ as $fee_)
			      	{ $tot_amount = $tot_amount + (($fee_['amount'] * $percentage ) / 100 );
			          
			      	}
			          
			      }

      	     }
      	     else
      	      {
      	       $fee_info_ = SrcFees::find()
                       ->where(['id'=>$fee])
                       //->andWhere(['program'=> $program])
                       //->andWhere(['academic_period'=> $acad])
                       ->all();
		       
				    
			    if($fee_info_!=null)
			      { 
			      	 foreach($fee_info_ as $fee_)
			      	   { $tot_amount = ($fee_->amount * $percentage ) / 100;
			      	   }
			      	 
			      }
      	       }
      	      
      	    			    
			     return $tot_amount;
      	
      	}
      	
 
 public function getAmount()
      {   
      	$acad=Yii::$app->session['currentId_academic_year']; 
      	
      	     $tot_amount =0;
      	     
      	     //get program
      	        $program = getProgramByStudentId($this->student);
      	     
      	     if($this->fee==null)
      	     {
      	     	$fee_info_= Yii::$app->db->createCommand('SELECT amount FROM fees WHERE  program='.$program.' AND academic_period='.$acad)->queryAll();
		        
			    if($fee_info_!=null)
			      { 
			      	foreach($fee_info_ as $fee_)
			      	{ $tot_amount = $tot_amount + (($fee_->amount * $this->percentage_pay ) / 100 );
			          
			      	}
			          
			      }

      	     }
      	     else
      	      {
      	       $fee_info_ = SrcFees::find()->where(['id'=>$this->fee])->andWhere(['program'=> $program])->andWhere(['academic_period'=> $acad])->all();
		        
				    
			    if($fee_info_!=null)
			      { 
			      	foreach($fee_info_ as $fee_)
			      	  {  $tot_amount = ($fee_->amount * $this->percentage ) / 100;
			      	  }
			      }
      	       }
      	      
			    
			    
			     return $tot_amount;
      	
      	}     	

      		
public function getIsInternal(){
            switch($this->is_internal)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
                
            }
        }
    
 
public function getProgram()
      {   
      	$acad=Yii::$app->session['currentId_academic_year']; 
      	  //get program
      	   $program = getProgramByStudentId($this->student);
      	     
      	 	$programLabel = SrcProgram::findOne($program);
      	 	     return $programLabel->label;
      	
      	}  

public function getLevel()
      {   
      	$acad=Yii::$app->session['currentId_academic_year']; 
      	  //get program
      	   $level = getLevelByStudentId($this->student);
      	     
      	 	return $level;
      	
      	}          

public function getShift()
  {   
      	 $modelPerson = SrcPersons::findOne($this->student);
         
         $shift = $modelPerson->studentOtherInfo0[0]->apply_shift;
      	     
      	 	$shift_name = SrcShifts::findOne($shift);
      	 	     return $shift_name->shift_name;
      	
   } 
   
 public function getTotalScholarshipHolders($acad)
  {
        $t = 0;
    	  $sql='SELECT DISTINCT student as total_persons FROM scholarship_holder WHERE academic_year='.$acad.' GROUP BY student';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
          if($result!=null)
          { foreach($result as $r)
             $t++;// = $r["total_persons"]; 
           }
           
           return $t;
   
    }   
    
    
    
    
    
}
