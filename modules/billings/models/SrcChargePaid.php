<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\ChargePaid;
use app\modules\billings\models\SrcChargePaid;

/**
 * SrcChargePaid represents the model behind the search form about `app\modules\billings\models\ChargePaid`.
 */
class SrcChargePaid extends ChargePaid
{
    public $depensesItems;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_charge_description', 'devise', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['payment_date', 'comment', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $acad=Yii::$app->session['currentId_academic_year'];
        
        $query = SrcChargePaid::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_charge_description' => $this->id_charge_description,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'payment_date' => $this->payment_date,
            'academic_year' => $this->academic_year,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['academic_year'=>$acad]);

        return $dataProvider;
    }
}
