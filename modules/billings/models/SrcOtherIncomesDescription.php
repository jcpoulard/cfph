<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\OtherIncomesDescription;
use app\modules\billings\models\SrcOtherIncomesDescription;

/**
 * SrcOtherIncomesDescription represents the model behind the search form about `app\modules\billings\models\OtherIncomesDescription`.
 */
class SrcOtherIncomesDescription extends OtherIncomesDescription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category'], 'integer'],
            [['income_description', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcOtherIncomesDescription::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'income_description', $this->income_description])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
    
    
public function getCategory()
			{
			
		        
      // $params= array(':categ'=>$this->category);
           $sql='select category from label_category_for_billing lcb where id ='.$this->category;
	
         $result= Yii::$app->db->createCommand($sql)->queryAll();

    		             if((isset($result))&&($result!=null))
		               { 
		               	   foreach($result as $r)
		               	     return Yii::t('app',$r['category']);
		               
		               }
		             else
		                 return null;
		                             
			}
		    
    
    
    
    
    
    
}
