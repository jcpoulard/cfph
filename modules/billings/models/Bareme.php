<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "bareme".
 *
 * @property integer $id
 * @property double $min_value
 * @property double $max_value
 * @property double $percentage
 * @property integer $compteur
 * @property integer $old_new
 * @property string $date_created
 * @property string $created_by
 */
class Bareme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bareme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['min_value', 'max_value', 'percentage', 'compteur', 'date_created', 'created_by'], 'required'],
            [['min_value', 'max_value', 'percentage'], 'number'],
            [['compteur', 'old_new'], 'integer'],
            [['date_created'], 'safe'],
            [['created_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'min_value' => Yii::t('app', 'Min Value'),
            'max_value' => Yii::t('app', 'Max Value'),
            'percentage' => Yii::t('app', 'Percentage'),
            'compteur' => Yii::t('app', 'Compteur'),
            'old_new' => Yii::t('app', 'Old New'),
            'date_created' => Yii::t('app', 'Date Created'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
