<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\PayrollSettings;
use app\modules\billings\models\PayrollSettingTaxes;
use app\modules\billings\models\SrcTaxes;

use app\modules\fi\models\SrcPersons;

/**
 * SrcPayrollSettings represents the model behind the search form about `app\modules\billings\models\PayrollSettings`.
 */
class SrcPayrollSettings extends PayrollSettings
{
    public $full_name;
    public $employee_lname; 
	public $employee_fname;
	public $teacher_lname; 
	public $teacher_fname; 
	public $academic_year;
	public $group;
	public $group_payroll;
	public $date_payroll;
	public $first_name;
	public $last_name;
	public $payroll_month;
	
	public $taxe;
	public $net_salary;
	public $payment_date;
	
	
	public $taxes;

    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id', 'devise', 'an_hour', 'number_of_hour', 'academic_year', 'as_', 'old_new'], 'integer'],
            [['amount', 'assurance_value', 'frais'], 'number'],
            [['frais', 'date_created', 'date_updated', 'created_by', 'updated_by'], 'safe'],
        ];
    }


public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                    [
                        'full_name'=> Yii::t('app','Full name'),
                        'employee_fname'=> Yii::t('app','Employee First Name'),
                        'employee_lname'=> Yii::t('app','Employee Last Name'),
                        'teacher_fname'=> Yii::t('app','Teacher First Name'),
                        'teacher_lname'=> Yii::t('app','Teacher Last Name'),
                        'group'=> Yii::t('app','By Group'),
                        'AnHour'=>Yii::t('app','An Hour'),
                        'as_'=> Yii::t('app','As'),
                        'old_new'=> Yii::t('app','New setting'),
                                   
                        ]
                    );
           
	}


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($acad,$params)
    {
        $query = SrcPayrollSettings::find()->joinWith(['person'])->where('old_new=1 and academic_year='.$acad)->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC,'payroll_settings.as_'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'person_id' => $this->person_id,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'an_hour' => $this->an_hour,
            'number_of_hour' => $this->number_of_hour,
            'frais' => $this->frais,
            'assurance_value' => $this->assurance_value, 
            'academic_year' => $this->academic_year,
            'as_' => $this->as_,
            'old_new' => $this->old_new,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }

   
    
    public function search_($acad)
    {
        $query = SrcPayrollSettings::find()
                                          ->joinWith(['payrolls','academicYear','person','devise0'])
                                          ->orderBy(['concat(persons.first_name," ",persons.last_name)'=>SORT_ASC,'payroll_settings.as_'=>SORT_ASC])
                                          ->where(['old_new'=>1])
                                          ->andWhere(['academic_year'=>$acad])
                                          ->andWhere(['in', 'persons.active',[1,2] ])
                                          ->andWhere(['academic_year'=>$acad]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
        return $dataProvider;
    }    
    
    
    
    
    
 public function getPayrollGroup(){
            return array(
                1=>Yii::t('app','Employees'), // Les personnes dans person qui ont un poste [done] 
                2=>Yii::t('app','Teachers'), // Les professeurs uniquement sans un poste [done]
                
            );            
        }
 public function getAnHour(){
            switch($this->an_hour)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
               
            }
        }
        
   public function getAnHourValue(){
            return array(
                0=>Yii::t('app','No'),
                1=>Yii::t('app','Yes'),
                             
            );            
        } 		
  
 
   public function getAsValue(){
            switch($this->as_)
               {          
                   case 0:
                    return Yii::t('app','Employee');
                   case 1:
                    return Yii::t('app','Teacher');
                
                }
                             
                      
        } 		
 
 
   public function getOldNewValue(){
            switch($this->old_new)
               {          
                   case 0:
                    return Yii::t('app','Old setting');
                   case 1:
                    return Yii::t('app','New setting');
                
                }
                             
                      
        } 	 

 
public function getNumberHour(){
           if(($this->number_of_hour!=0))
            {
            	return $this->number_of_hour;
             }
            else 
             {  if($this->an_hour==0)
          	    return  Yii::t('app','N/A');
          	 elseif($this->an_hour==1)
          	      return  null;
             }
               
           
        }	


 public function getGrossSalary(){
        
        if(($this->number_of_hour!=0)&&($this->number_of_hour!=''))
           {
           	  return ($this->amount * $this->number_of_hour);
           	  
           }
        else
           {  return  $this->amount;
           
             }
                
        }
        
  public function getGrossSalaryInd(){
        
           
       if(($this->number_of_hour!=0)&&($this->number_of_hour!=''))
           {
           	  if(isset($this->devise0->devise_symbol))
                  return $this->devise0->devise_symbol.' '.numberAccountingFormat( ($this->amount * $this->number_of_hour) );
           	  
           }
        else
           {  if(isset($this->devise0->devise_symbol))
                   return  $this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
           
             }
                
        }


	 public function getAmount(){
          
          if(isset($this->devise0->devise_symbol))
             return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
        }
        
    public function getAssurance(){
          
          if(isset($this->devise0->devise_symbol))
             return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->assurance_value);
        }

public function getFrais(){
          
          if(isset($this->devise0->devise_symbol))
             return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->frais);
        }
        
public function getNumberHourValue(){
            if(($this->number_of_hour!=0)&&($this->number_of_hour!=''))
            {
            	return $this->number_of_hour;
             }
            else 
                return '';
        }
        
          


public function getTaxeToPay(){
           
           $taxes = '';
                            
			$modelTaxe = PayrollSettingTaxes::find()->orderBy(['id_taxe'=>SORT_ASC])->where(['id_payroll_set'=>$this->id])->all();
			$i =0;
			 if($modelTaxe!=null)
              { foreach($modelTaxe as $taxe)
                  {    $taxe_desc = SrcTaxes::findOne($taxe->id_taxe);
                  	    
                  	    	
		                  	if($i==0)
		                  	  {
		                  	  	  $i =1;
		                  	  	  $taxes = $taxe_desc->taxe_description;
		                  	  	}
		                  	else
		                  	   $taxes = $taxes.', '.$taxe_desc->taxe_description;
		                  	   
		               
                   }
               }
               
			   	 
           
           
          	      return  $taxes;
         
           
        }	


 
public function getSimpleNumberHourValue($person){
	
	$acad=Yii::$app->session['currentId_academic_year']; 
	
	$nbr_time = 0;
	
     $sql = 'SELECT number_of_hour, an_hour FROM payroll_settings  WHERE old_new=1 AND person_id='.$person.' AND academic_year='.$acad.' order by id DESC';
		
		$info_setting_p = Yii::$app->db->createCommand($sql)->queryAll(); 
									       	   
		  if($info_setting_p!=null) 
			{    
				$modelPers = new SrcPersons();
			 
			     $employee_teacher=  $modelPers->isEmployeeTeacher($person, $acad);
	  
			 if($employee_teacher) 
			   {
			   	     $compt=0;
			   	     
			   	     foreach($info_setting_p as $info_)
					  {
					      $compt++;
					      
					      if($info_['an_hour']==1)
			                {     
						      $nbr_time = $info_['number_of_hour'];					  	   	    
			                }
					     
					     if($compt==2)
					        break;
					        
					   }
			   	     
			   	     
			     }
			  elseif(!$employee_teacher)
			    {

				 foreach($info_setting_p as $info_)
				  {
				      if($info_['an_hour']==1)
			           {
			              $nbr_time = $info_['number_of_hour'];
					      
			             }
			             					  	   	    
				     break;
				   }
				
			    }
			}
					
			if(($nbr_time!=0)&&($nbr_time !=''))
            {
            	return $nbr_time;
             }
            else 
                return '';
                
                
  }
        
          
  
public function getIdPayrollSettingByPersonId($person_id)  
	{
		$acad=Yii::$app->session['currentId_academic_year'];
		
		$sql = '';
		//check if is employee_teacher
		$modelPers = new SrcPersons();
		 $employee_teacher= $modelPers->isEmployeeTeacher($person_id, $acad);
		 
		  if($employee_teacher) 
			   {
			   		$sql = 'SELECT ps.id FROM payroll_settings ps where ps.old_new = 1 AND ps.as_=0 AND ps.person_id ='.$person_id;		   	     
			     }
			  elseif(!$employee_teacher)
			    {
			         $sql = 'SELECT id FROM payroll_settings where old_new = 1 AND person_id ='.$person_id;
			      }


		$info__p = Yii::$app->db->createCommand($sql)->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  return $info['id'];
          	         break;
          	     }
          
          }
        else
           return null;
           	

	 
	 }

      
public function getPersonIdByIdPayrollSetting($id_payroll_set)  
	{
		$acad=Yii::$app->session['currentId_academic_year'];
		
		 $sql = 'SELECT person_id FROM payroll_settings where id ='.$id_payroll_set;		   	     
			
		$info__p = Yii::$app->db->createCommand($sql)->queryAll(); 
        
        if($info__p!=null)
          { 
          	  foreach($info__p as $info)
          	     {  return $info['person_id'];
          	         break;
          	     }
          
          }
        else
           return null;
           	 
	 }
      

//return id PayrollSettings
public function ifAsAlreadySetAs($person_id, $as, $acad)
 {
 	  	
	
			$sql_result= Yii::$app->db->createCommand('SELECT ps.id  FROM payroll_settings ps left join persons p on(p.id=ps.person_id) WHERE ps.old_new = 1 AND ps.person_id='.$person_id.' AND ps.as_='.$as.' AND ps.academic_year='.$acad)->queryAll();
		//if($sql_result!=null)
		  return $sql_result;
 	}
 	


                
public function searchPersonsMakePayroll($condition)
	{
		$query = SrcPayrollSettings::find()
		                                 ->joinWith(['person'])
		                                 //->select(['persons.id', 'persons.last_name', 'persons.first_name', 'persons.gender', 'amount', 'an_hour', 'number_of_hour', 'as_', 'old_new'])
		                                 ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
		                                 ->where('old_new=1 and '.$condition);
 
        // add conditions that should always apply here

      return  $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000,
        ],
        ]);

		
	
	}




public function searchEmployeeForPayroll($condition)
	{     
			    
                           $query = SrcPersons::find()
		                                 ->orderBy(['last_name'=>SORT_ASC])
		                                 ->where($condition);

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 100000,
        ],
        ]);

	
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
