<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\Taxes;

/**
 * SrcTaxes represents the model behind the search form about `app\modules\billings\models\Taxes`.
 */
class SrcTaxes extends Taxes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employeur_employe', 'particulier', 'valeur_fixe', 'academic_year'], 'integer'],
            [['taxe_description'], 'safe'],
            [['taxe_value'], 'number'],
             [['taxe_description','academic_year','employeur_employe'], 'unique', 'targetAttribute' => ['taxe_description','employeur_employe','academic_year'],  'message' => Yii::t('app', 'This combinaison "taxe description - employeur/employe" has already been taken.')]
        ];
        
        
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($acad,$params)
    {
        $query = SrcTaxes::find()->where(['academic_year'=>$acad]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employeur_employe' => $this->employeur_employe,
            'taxe_value' => $this->taxe_value,
            'valeur_fixe'=> $this->valeur_fixe,
            'particulier' => $this->particulier,
            'academic_year' => $this->academic_year,
        ]);

        $query->andFilterWhere(['like', 'taxe_description', $this->taxe_description]);

        return $dataProvider;
    }
    
    
//return info of all taxes description       
public static function searchTaxesForPS($acad)
  {
		$command= Yii::$app->db->createCommand("SELECT  id, taxe_description,taxe_value FROM taxes WHERE  employeur_employe=0 AND academic_year=".$acad);
		//$command->bindValue(':acad', $acad);
		$sql_result = $command->queryAll();
		//if($sql_result!=null)
		  return $sql_result;
  }
       
	
	
public function getTaxeValue(){
             if(($this->taxe_description=='IRI'))
            {  return Yii::t('app','According to the range' );
            	
             }
            else 
             {    if($this->valeur_fixe==1)
                    {
                       return $this->taxe_value.' ( '.Yii::t('app', 'Valeur fixe').' )';
                    }
                   else
                       return $this->taxe_value;
                
             }
             
             
        }	
	
       public function getEmployeurEmploye(){
          
            switch($this->employeur_employe)
	            {
	                case 0:
	                    return Yii::t('app','Employee');
	                case 1:
	                    return Yii::t('app','Employer');
	                    
	                
	                
	            }
           
        }
        
     
     
         public function getEmployeurEmployeValue(){
            return array(
                
                0=>Yii::t('app','Employee'),
                1=>Yii::t('app','Employer'),
                
                               
            );            
        } 			
	
	
  public function getParticulier(){
          
          switch($this->particulier)
	            {
	                case 0:
	                    return Yii::t('app','General');
	                case 1:
	                    return Yii::t('app','Particular');
	                    
	                
	                
	            }
           
        }
        
	
 public function getParticulierValue(){
            return array(
                
                0=>Yii::t('app','General'),
                1=>Yii::t('app','Particular'),
                
                               
            );            
        } 		
        
       
    
    
    
    
    
    
    
}
