<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\Reservation;

/**
 * SrcReservation represents the model behind the search form about `app\modules\billings\models\Reservation`.
 */
class SrcReservation extends Reservation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'postulant_student', 'is_student', 'payment_method', 'already_checked', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['payment_date', 'comments', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcReservation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'postulant_student' => $this->postulant_student,
            'is_student' => $this->is_student,
            'amount' => $this->amount,
            'payment_method' => $this->payment_method,
            'payment_date' => $this->payment_date,
            'already_checked' => $this->already_checked,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
 //return an array(id, amount) or null value	
public function getNonCheckedReservation($student, $previous_acad)
	{   	    
      $result = SrcReservation::find()
			       	            ->select(['id','amount'])
			       	            ->where(['already_checked'=>0])
			       	            ->andWhere(['is_student'=>1])
			       	            ->andWhere(['postulant_student'=>$student])
			       	            ->andWhere(['academic_year'=>$previous_acad])
			       	            ->all(); 
           
       return $result;
           
 }   
    
    
    
    
    
    
    
    
    
}
