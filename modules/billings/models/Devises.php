<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "devises".
 *
 * @property integer $id
 * @property string $devise_name
 * @property string $devise_symbol
 * @property string $description
 * @property integer $is_default
 * @property string $date_create
 * @property string $date_update
 * @property string $create_by
 * @property string $update_by
 */
class Devises extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'devises';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['devise_name', 'devise_symbol'], 'required'],
            [['description'], 'string'],
            [['is_default'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['devise_name', 'devise_symbol', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['devise_symbol'], 'unique'],
            [['devise_name'], 'unique'],
            [['devise_name','devise_symbol'], 'unique', 'targetAttribute' => ['devise_name','devise_symbol'],  'message' => Yii::t('app', 'This combinaison "devise name - devise symbol" has already been taken.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'devise_name' => Yii::t('app', 'Devise Name'),
            'devise_symbol' => Yii::t('app', 'Devise Symbol'),
            'description' => Yii::t('app', 'Description'),
            'is_default' => Yii::t('app', 'Is Default'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
    
  
   /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getFees() 
		   { 
		       return $this->hasMany(Fees::className(), ['devise' => 'id']); 
		   }   
    
    
    
}

