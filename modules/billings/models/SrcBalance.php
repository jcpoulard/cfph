<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\Balance;

/**
 * SrcBalance represents the model behind the search form about `app\modules\billings\models\Balance`.
 */
class SrcBalance extends Balance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'devise'], 'integer'],
            [['balance'], 'number'],
            [['date_created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcBalance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'balance' => $this->balance,
            'devise' => $this->devise, 
            'date_created' => $this->date_created,
        ]);

        return $dataProvider;
    }
    
    
 /*   
	public function searchByRoom($room)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('student0');


		$criteria->alias='b';
		$criteria->join='inner join room_has_person rh on(rh.students = b.student) ';
        $criteria->condition = 'rh.room='.$room;
	    
	    $criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('student0.first_name',$this->student_fname,true);
		$criteria->compare('student0.last_name',$this->student_lname,true);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('date_created',$this->date_created,true);
		
		return new CActiveDataProvider($this, array(
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
    	    'criteria'=>$criteria,
		));
	}
	
	public function searchByRoomFee($room,$fee)
	{
		// SELECT distinct b.student FROM balance b inner join persons p on(p.id=b.student) inner join room_has_person rh on(rh.students = b.student) where b.student in(select bs.student from billings bs inner join fees f on(f.id=bs.fee_period) where f.fee=1 and fee_totally_paid=0) and rh.room=8 

		$criteria=new CDbCriteria;
		
		$criteria->alias='b';
		$criteria->join='inner join persons p on(p.id=b.student) inner join room_has_person rh on(rh.students = b.student)';
		
		if($fee!=NULL)
           $criteria->condition = ' b.student in(select bs.student from billings bs inner join fees f on(f.id=bs.fee_period) where f.fee='.$fee.' and fee_totally_paid=0) and rh.room='.$room;
        else
           $criteria->condition = ' b.student in(select bs.student from billings bs where fee_totally_paid=0) and rh.room='.$room;
	    
	    
	    
	    //$criteria->select='b.id,b.student,p.first_name,p.last_name,b.balance,b.date_created';
	    $criteria->compare('b.id',$this->id);
		$criteria->compare('b.student',$this->student);
		$criteria->compare('student0.first_name',$this->student_fname,true);
		$criteria->compare('student0.last_name',$this->student_lname,true);
		$criteria->compare('b.balance',$this->balance);
		$criteria->compare('b.date_created',$this->date_created,true);
	    
		
		return new CActiveDataProvider($this, array(
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
    	    'criteria'=>$criteria,
		));
	}
	
	
	
	public function searchByParentUsername($userName)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('student0');

		$criteria->condition = 'u.username=:user AND u.is_parent=1 ';
	    $criteria->params = array( ':user'=>$userName,);


		$criteria->alias='b';
		$criteria->join='left join room_has_person rh on(rh.students = b.student) inner join users u ON(u.person_id=b.student) ';
        
		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('student0.first_name',$this->student_fname,true);
		$criteria->compare('student0.last_name',$this->student_lname,true);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('date_created',$this->date_created,true);
		
		
		
		return new CActiveDataProvider($this, array(
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
    	    'criteria'=>$criteria,
		));
	}
	
	
		

	
	public function searchByStudentUsername($userName)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with= array('student0');

		$criteria->condition = 'u.username=:user AND u.is_parent=0 ';
	    $criteria->params = array( ':user'=>$userName,);


		$criteria->alias='b';
		$criteria->join='left join room_has_person rh on(rh.students = b.student) inner join users u ON(u.person_id=b.student) ';
        
		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('student0.first_name',$this->student_fname,true);
		$criteria->compare('student0.last_name',$this->student_lname,true);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('date_created',$this->date_created,true);
		
		
		
		return new CActiveDataProvider($this, array(
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
    	    'criteria'=>$criteria,
		));
	}
	
*/	
	     public function getBalance(){
                        return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->balance);
        }
    	
	
/*	public function getRooms($stud)
			{ 
				$acad=Yii::app()->session['currentId_academic_year']; 
				
			$modelRH=new RoomHasPerson;
		$idRoom = $modelRH->find(array('select'=>'room',
                                     'condition'=>'students=:studID AND academic_year=:acad',
                                     'params'=>array(':studID'=>$stud,':acad'=>$acad),
                               ));
		$room = new Rooms;
                if($idRoom !=null){
                        $result=$room->find(array('select'=>'id,room_name',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->room),
                               ));
			
                
						   

						   
				if($result!=null)		   
				   return $result->room_name;
				else
				   return null;
				   
                }
                else
                  return null;
                
			}
	
	*/    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
