<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;

/**
 * This is the model class for table "scholarship_holder".
 *
 * @property integer $id
 * @property integer $student
 * @property integer $partner
 * @property integer $fee
 * @property double $percentage_pay
 * @property integer $is_internal
 * @property integer $academic_year
 * @property string $comment
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Academicperiods $academicYear
 * @property Persons $student0
 */
class ScholarshipHolder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scholarship_holder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'student',  'academic_year'], 'required'],
            [['id', 'student', 'partner', 'fee', 'is_internal', 'academic_year'], 'integer'],
            [['percentage_pay'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            
           // [['student','fee','academic_year'], 'unique', 'targetAttribute' => ['student','fee','academic_year'],  'message' => Yii::t('app', 'This combinaison "student - fee - academic_year" has already been taken.')],            
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'partner' => Yii::t('app', 'Partner'),
            'fee' => Yii::t('app', 'Fee'),
            'percentage_pay' => Yii::t('app', 'Percentage Pay'),
            'is_internal' => Yii::t('app', 'Is Internal'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'comment' => Yii::t('app', 'Comment'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }
}
