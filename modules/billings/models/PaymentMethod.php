<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "payment_method".
 *
 * @property integer $id
 * @property string $method_name
 * @property string $description
 * @property integer $is_default 
 * @property string $date_create
 * @property string $date_update
 * @property string $create_by
 * @property string $update_by
 *
 * @property Billings[] $billings
 * @property EnrollmentIncome[] $enrollmentIncomes 
		* @property Reservation[] $reservations 
 */
class PaymentMethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_method';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'method_name'], 'required'],
            [['id'], 'integer'],
            [['description'], 'string'],
            [['is_default'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['method_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['method_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'method_name' => Yii::t('app', 'Method Name'),
            'description' => Yii::t('app', 'Description'),
            'is_default' => Yii::t('app', 'Is Default'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillings()
    {
        return $this->hasMany(Billings::className(), ['payment_method' => 'id']);
    }
    
      /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getEnrollmentIncomes() 
		   { 
		       return $this->hasMany(EnrollmentIncome::className(), ['payment_method' => 'id']); 
		   } 
		 
		   /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getReservations() 
		   { 
		       return $this->hasMany(Reservation::className(), ['payment_method' => 'id']); 
		   } 
}
