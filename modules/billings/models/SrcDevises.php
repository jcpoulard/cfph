<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\Devises;

/**
 * SrcDevises represents the model behind the search form about `app\modules\billings\models\Devises`.
 */
class SrcDevises extends Devises
{
   public $globalSearch;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_default'], 'integer'],
            [['devise_name', 'devise_symbol', 'is_default', 'globalSearch', 'description', 'date_create', 'date_update', 'create_by', 'update_by'], 'safe'],
            [['devise_symbol'], 'unique'],
            [['devise_name'], 'unique'],
            [['devise_name','devise_symbol'], 'unique', 'targetAttribute' => ['devise_name','devise_symbol'],  'message' => Yii::t('app', 'This combinaison "devise name - devise symbol" has already been taken.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcDevises::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_default' => $this->is_default,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'devise_name', $this->devise_name])
            ->andFilterWhere(['like', 'devise_symbol', $this->devise_symbol])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
 public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcDevises::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->orFilterWhere(['like', 'devise_name', $this->globalSearch])
           ->orFilterWhere(['like', 'devise_symbol', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
    

//return 0 or the default model    
 public function checkDehaultSet()   
    {
    	$query = SrcDevises::find()->where(['is_default'=>1])->all();
    	
    	if($query!=null)
    	   { 
    	   	   foreach($query as $r)
    	   	     {
    	   	     	 return $r;
    	   	     	}
    	   	     	
    	   	   
    	   
    	   }
    	else
    	    return 0;
    	
    }
    
    
    public function getIsDefault(){
    switch ($this->is_default){
        case 0:
            return Yii::t('app','No');
            break;
        case 1:
            return Yii::t('app','Yes');
            break;
       
    }
   }
    
    
    
}
