<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\ChargeDescription;

/**
 * SrcChargeDescription represents the model behind the search form about `app\modules\billings\models\ChargeDescription`.
 */
class SrcChargeDescription extends ChargeDescription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category'], 'integer'],
            [['description', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        
        $query = SrcChargeDescription::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
    
    
public function getCategory()
			{
			
		        
      
           $sql='select category from label_category_for_billing lcb where id ='.$this->category;
	
         $result= Yii::$app->db->createCommand($sql)->queryAll();

    		        if((isset($result))&&($result!=null))
		               { 
		               	   foreach($result as $r)
		               	     return Yii::t('app',$r['category']);
		               
		               }
		             else
		                 return null;
		                             
			}
			
    
    
    
    
    
    
    
    
    
    
    
    
    
}
