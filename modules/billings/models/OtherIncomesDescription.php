<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "other_incomes_description".
 *
 * @property integer $id
 * @property string $income_description
 * @property integer $category
 * @property string $comment
 */
class OtherIncomesDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_incomes_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['income_description', 'category', 'comment'], 'required'],
            [['category'], 'integer'],
            [['income_description'], 'string', 'max' => 65],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'income_description' => Yii::t('app', 'Income Description'),
            'category' => Yii::t('app', 'Category'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }
}
