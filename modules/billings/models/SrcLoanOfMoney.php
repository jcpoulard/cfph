<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\LoanOfMoney;

/**
 * SrcLoanOfMoney represents the model behind the search form about `app\modules\billings\models\LoanOfMoney`.
 */
class SrcLoanOfMoney extends LoanOfMoney
{
    
    public $employee_lname; 
	public $employee_fname;
	public $teacher_lname; 
	public $teacher_fname; 
	//public $academic_year;
	public $total_loan;
	public $repayment_start_on;
	
	 public $depensesItems;
	 
	 public $refund;
	 
	 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id', 'devise', 'payroll_month', 'deduction_percentage', 'paid', 'number_of_month_repayment', 'remaining_month_number', 'academic_year'], 'integer'],
            [['loan_date', 'date_created', 'date_updated', 'created_by'], 'safe'],
            [['amount', 'refund', 'solde'], 'number'],
        ];
    }

  public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                    [
                    'employee_fname'=> Yii::t('app','Employee First Name'),
                        'employee_lname'=> Yii::t('app','Employee Last Name'),
                        'teacher_fname'=> Yii::t('app','Teacher First Name'),
                        'teacher_lname'=> Yii::t('app','Teacher Last Name'),
                        'repayment_start_on'=>Yii::t('app','Repayment start on'),
                        'depensesItems'=> Yii::t('app','Depenses Items'),
                                    
                        ]
                    );
           
	}
	

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($acad,$params)
    {
        $query = SrcLoanOfMoney::find()->where(['academic_year'=>$acad]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'loan_date' => $this->loan_date,
            'person_id' => $this->person_id,
            'amount' => $this->amount,
            'devise' => $this->devise, 
            'payroll_month' => $this->payroll_month,
            'deduction_percentage' => $this->deduction_percentage,
            'solde' => $this->solde,
            'paid' => $this->paid,
            'number_of_month_repayment' => $this->number_of_month_repayment,
            'remaining_month_number' => $this->remaining_month_number,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
 
  public function searchForOne($acad,$person_id)   
 {
        $query = SrcLoanOfMoney::find()->where(['person_id'=>$person_id,'academic_year'=>$acad]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);


        return $dataProvider;
    }
    
    
 public function search_($acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
 
		$criteria->with=array('academicYear','person');
		
		$criteria->join='left join persons p on(p.id=l.person_id)';
		
		$criteria->alias='l';
		$criteria->condition='l.academic_year='.$acad;

		$criteria->compare('id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('p.first_name',$this->employee_fname,true);
		$criteria->compare('p.last_name',$this->employee_lname,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('payroll_month',$this->payroll_month);
		$criteria->compare('deduction_percentage',$this->deduction_percentage);
		$criteria->compare('solde',$this->solde);
		$criteria->compare('paid',$this->paid);
		$criteria->compare('number_of_month_repayment',$this->number_of_month_repayment);
		$criteria->compare('remaining_month_number',$this->remaining_month_number);
		$criteria->compare('loan_date',$this->date_created,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>[
        			'pageSize'=>10000000,
    			],
		));
	}
	

				
public function searchByMonth($month_, $acad)  //il fo cree colonne academic_year ds la table BILLINGS
	{
		// Warning: Please modify the following id to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('academicYear','person');
		
		$criteria->join='left join persons p on(p.id=l.person_id)';
		
		$criteria->alias='l';
		$criteria->condition='MONTH(l.loan_date)='.$month_.' AND  l.academic_year='.$acad;
		$criteria->order = 'person.last_name ASC, payroll_month ASC';

		$criteria->compare('id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('p.first_name',$this->employee_fname,true);
		$criteria->compare('p.last_name',$this->employee_lname,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('payroll_month',$this->payroll_month);
		$criteria->compare('deduction_percentage',$this->deduction_percentage);
		$criteria->compare('solde',$this->solde);
		$criteria->compare('paid',$this->paid);
		$criteria->compare('number_of_month_repayment',$this->number_of_month_repayment);
		$criteria->compare('remaining_month_number',$this->remaining_month_number);
		$criteria->compare('loan_date',$this->date_created,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('created_by',$this->created_by,true);

	     
		//$criteria->compare('id',$this->id_bill, true); 

		return new CActiveDataProvider($this, array(
			'pagination'=>[
        			'pageSize'=>10000000,
    			],
			'criteria'=>$criteria,
		));
	}
	

				
	public function searchForView($person,$acad) //il fo cree colonne academic_year ds la table BILLINGS
	{
		 $query = SrcLoanOfMoney::find()
		                 ->joinWith(['academicYear','person'])
		                 ->orderBy(['persons.last_name'=>SORT_ASC, 'payroll_month'=>SORT_ASC])
		                 ->where('loan_of_money.person_id ='.$person.' AND  loan_of_money.academic_year='.$acad);

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

	}
	
	
	
	
	//return the sum of all loan in this month
public function hasLoan($person,$month){
     
    	 $query = SrcLoanOfMoney::find()
		                 ->joinWith(['person', 'person.payrollSettings'])
		                 ->select(['loan_of_money.id', 'loan_of_money.amount', 'loan_of_money.payroll_month', 'loan_of_money.paid', 'loan_of_money.deduction_percentage', 'loan_of_money.solde',  'loan_of_money.loan_date'])
		                 ->where('payroll_settings.old_new=1 and payroll_settings.person_id ='.$person.' and payroll_month='.$month );

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

			
    }
   

//return the sum of all loan in this month
public function isPaid($person){
     
    	 $query = SrcLoanOfMoney::find()
		                 ->orderBy(['date_created'=>SORT_DESC ])
		                 ->select(['id', 'amount', 'payroll_month', 'paid', 'deduction_percentage', 'solde',  'loan_date'])
		                 ->where('person_id ='.$person);

        // add conditions that should always apply here

       return $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

	   
   
    }


  
public function getTotalLoan($person, $acad)
  {
     
    	  $sql='SELECT SUM(amount) as total_loan FROM loan_of_money  WHERE person_id ='.$person.' AND  academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
           foreach($result as $r)
            return $r["total_loan"];   
   
    }

public function getTotalSolde($person, $acad)
  {
     
    	  $sql='SELECT SUM(solde) as total_solde FROM loan_of_money  WHERE person_id ='.$person.' AND  academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
           foreach($result as $r)
            return $r["total_solde"];   
   
    }
    
public function getTotalPersonsHavingLoan($acad)
  {
        $t = 0;
    	  $sql='SELECT COUNT(person_id) as total_persons FROM loan_of_money  WHERE academic_year='.$acad;
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
          if($result!=null)
          { foreach($result as $r)
             $t = $r["total_persons"]; 
           }
           
           return $t;
   
    }
    
    
 	 public function getAmount(){
          
            return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
        }
    	 
    	 
    public function getSolde(){
           
            return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->solde);
        }
    
   
   
 // Return the key=>value  of a month in long format 
public function getLongMonthValue(){
     return array(
        1=>Yii::t('app','January'),
        2=>Yii::t('app','February'),
        3=>Yii::t('app','March'),
        4=>Yii::t('app','April'),
        5=>Yii::t('app','May'),
        6=>Yii::t('app','June'),
        7=>Yii::t('app','July'),
        8=>Yii::t('app','August'),
        9=>Yii::t('app','September'),
        10=>Yii::t('app','October'),
        11=>Yii::t('app','November'),
        12=>Yii::t('app','December'),
        13=>Yii::t('app','13 eme'),
        14=>Yii::t('app','14 eme'),
        );  
    }
   

// Return the name of a month in long format 
public function getSelectedLongMonth($month){
    switch ($month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
        case 13:
            return Yii::t('app','13 eme');
            break;

        case 14:
            return Yii::t('app','14 eme');
            break;

    }
   }
   
  // Return the name of a month in long format 
public function getLongMonth(){
    switch ($this->payroll_month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
         case 13:
            return Yii::t('app','13 eme');
            break;
         case 14:
            return Yii::t('app','14 eme');
            break;


    }
   }

                  
// Return the name of a month in long format 
public function getloanPaid(){
    switch ($this->paid){
        case 0:
            return Yii::t('app','No');
            break;
        case 1:
            return Yii::t('app','Yes');
            break;
       
    }
   }

	
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
