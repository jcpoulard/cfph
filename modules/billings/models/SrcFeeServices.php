<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\FeeServices;

/**
 * SrcFeeServices represents the model behind the search form about `app\modules\billings\models\FeeServices`.
 */
class SrcFeeServices extends FeeServices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'service_description', 'old_new','devise',], 'integer'],
            [['price'], 'number'],
            [['created_date', 'updated_date', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcFeeServices::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'service_description' => $this->service_description,
            'price' => $this->price,
            'old_new' => $this->old_new,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by]);

        return $dataProvider;
    }
    
    
    
    
    
        public function getPrice(){
           
           
            return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->price);
        }
    
    
    
    
}
