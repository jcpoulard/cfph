<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcFeeServices;

/**
 * This is the model class for table "billings_services".
 *
 * @property integer $id
 * @property integer $student_id
 * @property integer $fee
 * @property double $price
 * @property string $request_date
 * @property integer $is_delivered
 * @property string $delivered_date
 * @property string $date_created
 * @property string $date_updated
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property FeeServices $fee0
 * @property User $createdBy
 * @property Persons $student
 * @property BillingsServicesItems $billingsServicesItems
 */
class BillingsServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billings_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fee','student_id', 'is_delivered', 'created_by', 'updated_by'], 'integer'],
            [['fee', 'request_date', 'date_created', 'created_by'], 'required'],
            [['price'], 'number'],
            [['request_date', 'delivered_date', 'date_created', 'date_updated'], 'safe'],
            [['fee'], 'exist', 'skipOnError' => true, 'targetClass' => SrcFeeServices::className(), 'targetAttribute' => ['fee' => 'id']], 
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student_id' => Yii::t('app', 'Student'),
            'fee' => Yii::t('app', 'Fee'),
            'price' =>Yii::t('app','Price'),
            'request_date' => Yii::t('app', 'Request Date'),
            'is_delivered' => Yii::t('app', 'Is Delivered'),
            'delivered_date' => Yii::t('app', 'Delivered Date'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFee0()
		   {
		       return $this->hasOne(SrcFeeServices::className(), ['id' => 'fee']);
		   }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingsServicesItems()
    {
        return $this->hasOne(BillingsServicesItems::className(), ['billings_services_id' => 'id']);
    }
}
