<?php

namespace app\modules\billings\models;

use Yii;

use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\Persons;

/**
 * This is the model class for table "pending_balance".
 *
 * @property integer $id
 * @property integer $student
 * @property double $balance
 * @property integer $is_paid
 * @property integer $academic_year
 * @property string $date_created
 *
 * @property Academicperiods $academicYear
 * @property Persons $student0
 */
class PendingBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pending_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'balance', 'academic_year', 'date_created'], 'required'],
            [['id', 'student', 'is_paid', 'academic_year'], 'integer'],
            [['balance'], 'number'],
            [['date_created'], 'safe'],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'balance' => Yii::t('app', 'Balance'),
            'is_paid' => Yii::t('app', 'Is Paid'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_created' => Yii::t('app', 'Date Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(Persons::className(), ['id' => 'student']);
    }
}
