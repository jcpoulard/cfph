<?php

namespace app\modules\billings\models;

use Yii;

/**
 * This is the model class for table "billings_services_items".
 *
 * @property integer $billings_services_id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $promotion
 * @property string $option_program
 *
 * @property BillingsServices $billingsServices
 */
class BillingsServicesItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billings_services_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['billings_services_id', 'first_name', 'last_name', 'promotion', 'option_program'], 'required'],
            [['billings_services_id'], 'integer'],
            [['first_name', 'last_name', 'option_program'], 'string', 'max' => 255],
            [['phone', 'promotion'], 'string', 'max' => 20],
            [['billings_services_id'], 'exist', 'skipOnError' => true, 'targetClass' => BillingsServices::className(), 'targetAttribute' => ['billings_services_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'billings_services_id' => Yii::t('app', 'Billings Services ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'phone' => Yii::t('app', 'Phone'),
            'promotion' => Yii::t('app', 'Promotion'),
            'option_program' => Yii::t('app', 'Option Program'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingsServices()
    {
        return $this->hasOne(BillingsServices::className(), ['id' => 'billings_services_id']);
    }
}
