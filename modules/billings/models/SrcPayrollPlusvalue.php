<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\PayrollPlusvalue;

/**
 * SrcPayrollPlusvalue represents the model behind the search form about `app\modules\billings\models\PayrollPlusvalue`.
 */
class SrcPayrollPlusvalue extends PayrollPlusvalue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person_id', 'month', 'devise', 'acad'], 'integer'],
            [['amount'], 'number'],
            [['created_date', 'updated_date', 'created_by', 'updated_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayrollPlusvalue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'person_id' => $this->person_id,
            'month' => $this->month,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'acad' => $this->acad,
            
        ]);

        return $dataProvider;
    }
    

public function searchByAcad($acad)
    {
        $query = SrcPayrollPlusvalue::find()->where(['acad'=>$acad])->groupBy('person_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

          
        return $dataProvider;
    }
  
 public function searchForOneByAcad($person_id,$acad)
    {
        $query = SrcPayrollPlusvalue::find()->where('person_id='.$person_id.' AND acad='.$acad);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
         ]);

          
        return $dataProvider;
    }
    

public function getAmount(){
          
            return $this->devise0->devise_symbol.' '.numberAccountingFormat($this->amount);
        }
    	 

public function isAlreadySetForOne($person,$month,$acad)
  {
  	   $query = SrcPayrollPlusvalue::find()->where(['person_id'=>$person,'month'=>$month,'acad'=>$acad])->all();
  	   if($query!=null)
  	   {
  	   	  foreach($query as $q)
  	   	    {
  	   	    	if($q!=null)
  	   	    	  return $q;
  	   	    	else
  	   	    	   return null;
  	   	    	   
  	   	    	break;
  	   	      }
  	   	}
  	   else
  	       return null;
  	
  	}    	 
 
public function getPayrollPlusvalue($person_id,$month,$acad)
  {
  	     $query = SrcPayrollPlusvalue::find()->where(['person_id'=>$person_id,'month'=>$month,'acad'=>$acad])->all();
  	   if($query!=null)
  	   {
  	   	  foreach($query as $q)
  	   	    {
  	   	    	if($q!=null)
  	   	    	  return $q->amount;
  	   	    	else
  	   	    	   return 0;
  	   	    	   
  	   	    	break;
  	   	      }
  	   	}
  	   else
  	       return 0;
  	}
   
        
        


        
 // Return the key=>value  of a month in long format 
public function getLongMonthValue(){
     return array(
        1=>Yii::t('app','January'),
        2=>Yii::t('app','February'),
        3=>Yii::t('app','March'),
        4=>Yii::t('app','April'),
        5=>Yii::t('app','May'),
        6=>Yii::t('app','June'),
        7=>Yii::t('app','July'),
        8=>Yii::t('app','August'),
        9=>Yii::t('app','September'),
        10=>Yii::t('app','October'),
        11=>Yii::t('app','November'),
        12=>Yii::t('app','December'),
        13=>Yii::t('app','13 eme'),
        14=>Yii::t('app','14 eme'),
        );  
    }
   

// Return the name of a month in long format 
public function getSelectedLongMonth($month){
    switch ($month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
        case 13:
            return Yii::t('app','13 eme');
            break;

        case 14:
            return Yii::t('app','14 eme');
            break;

    }
   }
   
  // Return the name of a month in long format 
public function getLongMonth(){
    switch ($this->month){
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
         case 13:
            return Yii::t('app','13 eme');
            break;
         case 14:
            return Yii::t('app','14 eme');
            break;


    }
   }


	
	
	
}
