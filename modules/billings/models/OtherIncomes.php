<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\billings\models\SrcOtherIncomesDescription;
use app\modules\billings\models\SrcDevises;

/**
 * This is the model class for table "other_incomes".
 *
 * @property integer $id
 * @property integer $id_income_description
 * @property double $amount
  * @property integer $devise 
 * @property string $income_date
 * @property integer $academic_year
 * @property string $description
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 * 
 * @property OtherIncomesDescription $idIncomeDescription
 * @property Devises $devise0 
 */
class OtherIncomes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'other_incomes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_income_description', 'amount', 'devise', 'income_date', 'academic_year'], 'required'],
            [['id_income_description', 'devise', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['income_date', 'date_created', 'date_updated'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'string', 'max' => 65],
            [['id_income_description'], 'exist', 'skipOnError' => true, 'targetClass' => SrcOtherIncomesDescription::className(), 'targetAttribute' => ['id_income_description' => 'id']],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_income_description' => Yii::t('app', 'Income Description'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'), 
            'income_date' => Yii::t('app', 'Income Date'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'description' => Yii::t('app', 'Comment'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getIdIncomeDescription()
		   {
		       return $this->hasOne(SrcOtherIncomesDescription::className(), ['id' => 'id_income_description']);
		   }
		   
		   /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getDevise0() 
		   { 
		       return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
		   } 
}
