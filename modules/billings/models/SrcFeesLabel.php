<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\FeesLabel;

/**
 * SrcFeesLabel represents the model behind the search form about `app\modules\billings\models\FeesLabel`.
 */
class SrcFeesLabel extends FeesLabel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['fee_label'], 'safe'],
            [['fee_label'], 'unique'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

  
  
   /**
         * 
         * @return type Status value 
         * 0 ->  
         * 1 -> 
         */
public function getStatus(){
          switch($this->status)
            {
                case 0:
                    return Yii::t('app','Other fees');
                case 1:
                    return Yii::t('app','Tuition fees');
                
            }
        }
        
        /**
         * 
         * @return type
         * Return human readable value for status from the DB 
         * 
         */
public function getStatusValue(){
            return array(
                
                0=>Yii::t('app','Other fees'),
                1=>Yii::t('app','Tuition fees'),
                
                               
            );            
        } 		

  
public function getFeeLabel(){
           
            	return Yii::t('app',$this->fee_label);
                 
        }	
  
  
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcFeesLabel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'fee_label', $this->fee_label]);

        return $dataProvider;
    }
    
    
    
}
