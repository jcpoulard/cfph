<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\SrcPersons;


use app\modules\billings\models\SrcDevises;

/**
 * This is the model class for table "balance".
 *
 * @property integer $id
 * @property integer $student
 * @property double $balance
 * @property integer $devise 
 * @property string $date_created
 *
  * @property Devises $devise0 
  * @property Persons $student0
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'balance','devise',  'date_created'], 'required'],
            [['student','devise' ], 'integer'],
            [['balance'], 'number'],
            [['date_created'], 'safe'],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['student' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'balance' => Yii::t('app', 'Balance'),
            'devise' => Yii::t('app', 'Devise'),
            'date_created' => Yii::t('app', 'Date Created'),
        ];
    }

  /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getDevise0() 
		   { 
		       return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
		   }
		   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }
}
