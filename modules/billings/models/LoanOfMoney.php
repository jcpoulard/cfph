<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\SrcAcademicperiods;

use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcDevises;


/**
 * This is the model class for table "loan_of_money".
 *
 * @property integer $id
 * @property string $loan_date
 * @property integer $person_id
 * @property double $amount
 * @property integer $devise 
 * @property integer $payroll_month
 * @property integer $deduction_percentage
 * @property double $solde
 * @property integer $paid
 * @property integer $number_of_month_repayment
 * @property integer $remaining_month_number
 * @property integer $academic_year
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 *
 * @property Persons $person
 * @property Devises $devise0 
 * @property Academicperiods $academicYear
 */
class LoanOfMoney extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_of_money';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loan_date', 'person_id', 'amount', 'devise', 'payroll_month','academic_year'], 'required'],
            [['id', 'person_id', 'devise', 'payroll_month', 'deduction_percentage', 'paid', 'number_of_month_repayment', 'remaining_month_number', 'academic_year'], 'integer'],
            [['loan_date', 'date_created', 'date_updated'], 'safe'],
            [['amount', 'solde'], 'number'],
            [['created_by'], 'string', 'max' => 65],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['person_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'loan_date' => Yii::t('app', 'Loan Date'),
            'person_id' => Yii::t('app', 'Full Name'),
            'amount' => Yii::t('app', 'Initial Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'payroll_month' => Yii::t('app', 'Repayment start'),
            'deduction_percentage' => Yii::t('app', 'Deduction Percentage'),
            'solde' => Yii::t('app', 'Solde'),
            'paid' => Yii::t('app', 'Paid'),
            'number_of_month_repayment' => Yii::t('app', 'Repayment deadline(numb. of month)'),
            'remaining_month_number' => Yii::t('app', 'Remaining Month Number'),
            'number_of_month_repayment' => Yii::t('app', 'Number Of Month Repayment'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }

    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getAcademicYear() 
		   { 
		       return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']); 
		   } 

 /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getLoanElements()
		   {
		       return $this->hasMany(LoanElements::className(), ['loan_id' => 'id']);
		   }
				 
		 
   /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'person_id']);
    }
    
     /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getDevise0()
		   {
		       return $this->hasOne(SrcDevises::className(), ['id' => 'devise']);
		   }
	
    
}
