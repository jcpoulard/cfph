<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\Program;
use app\modules\fi\models\SrcProgram;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcDevises;


/**
 * This is the model class for table "fees".
 *
 * @property integer $id
 * @property integer $program
 * @property integer $academic_period
 * @property integer $level
 * @property integer $fee
 * @property double $amount
 * @property integer $devise
 * @property string $date_limit_payment
 * @property integer $checked
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property string $create_by
 * @property string $update_by
 *
  * @property Billings[] $billings
  * @property FeesLabel $fee0
 * @property Devises $devise0
 * @property Academicperiods $academicPeriod
 * @property Program $program0
 */
class Fees extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fees';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program', 'academic_period', 'level','fee', 'amount'], 'required'],
            [['program', 'academic_period','level', 'fee', 'devise', 'checked'], 'integer'],
            [['amount'], 'number'],
            [['date_limit_payment', 'date_create', 'date_update'], 'safe'],
            [['description'], 'string'],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            
           
             [['program','fee','level', 'academic_period'], 'unique', 'targetAttribute' => ['program','fee','level', 'academic_period'],  'message' => Yii::t('app', 'This combinaison "program - fee - level - academic_period" has already been taken.')]
           /* [['fee'], 'exist', 'skipOnError' => true, 'targetClass' => FeesLabel::className(), 'targetAttribute' => ['fee' => 'id']],
            [['academic_period'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_period' => 'id']],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => Devises::className(), 'targetAttribute' => ['devise' => 'id']],
            
            [['program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program' => 'id']],
            */
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'program' => Yii::t('app', 'Program'),
            'academic_period' => Yii::t('app', 'Academic Year'),
            'level' => Yii::t('app', 'Level'),
            'fee' => Yii::t('app', 'Fee'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'date_limit_payment' => Yii::t('app', 'Date Limit Payment'),
            'checked' => Yii::t('app', 'Checked'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFee0()
    {
        return $this->hasOne(SrcFeesLabel::className(), ['id' => 'fee']);
    }
    
     public function getBillings()
		   {
		       return $this->hasMany(SrcBillings::className(), ['fee_period' => 'id']);
		   }
    
       public function getDevise0()
		   {
		       return $this->hasOne(SrcDevises::className(), ['id' => 'devise']);
		   }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicPeriod()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_period']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(SrcProgram::className(), ['id' => 'program']);
    }
}
