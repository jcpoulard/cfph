<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\SrcBareme;

/**
 * SrcBareme represents the model behind the search form about `app\modules\billings\models\Bareme`.
 */
class SrcBareme extends Bareme
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'compteur', 'old_new'], 'integer'],
            [['min_value', 'max_value', 'percentage'], 'number'],
            [['date_created', 'created_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcBareme::find()->where(['old_new'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'min_value' => $this->min_value,
            'max_value' => $this->max_value,
            'percentage' => $this->percentage,
            'compteur' => $this->compteur,
            'old_new' => $this->old_new,
            'date_created' => $this->date_created,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
    
    
 //return an array  
public function getBaremeInUse(){
	
	$sql = 'SELECT id, min_value, max_value, percentage FROM bareme WHERE old_new=1';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          {  
			  return $info__p;
			  
          }
        else
           return null;
	
   }
        
//return an integer   
public function getLastCompteur(){
	
	$sql = 'SELECT DISTINCT MAX(compteur) as compteur FROM bareme';
		$command = Yii::$app->db->createCommand($sql);
        $info__p = $command->queryAll(); 
        
        if($info__p!=null)
          {  foreach($info__p as $info_)
				  { return $info_['compteur'];
				       break;
				  }
          }
        else
           return 0;
	
   }
       
      
    
    
    
    
    
    
}
