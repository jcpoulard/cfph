<?php

namespace app\modules\billings\models;

use Yii;

use app\modules\billings\models\SrcDevises;
use app\modules\billings\models\SrcOtherIncomesDescription;

/**
 * This is the model class for table "fee_services".
 *
 * @property integer $id
 * @property integer $service_description
 * @property double $price
 * @property integer $devise 
 * @property integer $old_new
 * @property string $created_date
 * @property string $updated_date
 * @property string $created_by
 * @property string $updated_by
 *
 * @property OtherIncomesDescription $serviceDescription
 */
class FeeServices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fee_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_description', 'price', 'devise',], 'required'],
            [['service_description', 'old_new','devise',], 'integer'],
            [['price'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 255],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']], 
            [['service_description'], 'exist', 'skipOnError' => true, 'targetClass' => SrcOtherIncomesDescription::className(), 'targetAttribute' => ['service_description' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'service_description' => Yii::t('app', 'Service Description'),
            'price' => Yii::t('app', 'Price'),
            'devise' => Yii::t('app', 'Devise'), 
            'old_new' => Yii::t('app', 'Old New'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_date' => Yii::t('app', 'Updated Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

   /**
	* @return \yii\db\ActiveQuery
    */
	public function getDevise0() 
	 { 
        return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
	   } 
		 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceDescription()
    {
        return $this->hasOne(SrcOtherIncomesDescription::className(), ['id' => 'service_description']);
    }
}
