<?php

namespace app\modules\billings\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\billings\models\BillingsServices;
use app\modules\billings\models\SrcBillingsServices;

/**
 * SrcBillingsServices represents the model behind the search form about `app\modules\billings\models\BillingsServices`.
 */
class SrcBillingsServices extends BillingsServices
{
    
        public $first_name;
        public $last_name;
        public $phone;
        public $promotion;
        public $option_program;
        
        public $recettesItems;
        
        
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fee','student_id', 'is_delivered', 'created_by', 'updated_by'], 'integer'],
            [['price'], 'number'],
            [['first_name', 'last_name', 'option_program','recettesItems'], 'string', 'max' => 255],
            [['phone', 'promotion'], 'string', 'max' => 20],
            [['request_date', 'delivered_date', 'date_created', 'date_updated'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
    public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'first_name' =>Yii::t('app','First Name'),
			               'last_name' =>Yii::t('app','Last Name'),
			               'phone' =>Yii::t('app','Phone'),
			               //'price' =>Yii::t('app','Price'),
			               'promotion' =>Yii::t('app','Promotion'),
			               'option_program' =>Yii::t('app','Option/Program'),
                                       'recettesItems'=>Yii::t('app','Recettes Items'),
		));
	}		


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $acad = Yii::$app->session['currentId_academic_year'];
        
        $query = SrcBillingsServices::find()->where('request_date between (select date_start from academicperiods where id='.$acad.') and (select date_end from academicperiods where id='.$acad.')');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student_id' => $this->student_id,
            'fee' => $this->fee,
            'price' => $this->price,
            'request_date' => $this->request_date,
            'is_delivered' => $this->is_delivered,
            'delivered_date' => $this->delivered_date,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
       
      
        return $dataProvider;
    }
    
    
    public function searchByFeeId($fee_id)
    {
        $query = SrcBillingsServices::find()->where('fee='.$fee_id);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
        
public function getPerson()    
   {
        if($this->student_id!=NULL)
          {
          	 $query = SrcBillingsServices::findOne($this->id);
          	 
          	 return $query;
          	
           }
        elseif($this->student_id==NULL)
          {
		        $query = BillingsServicesItems::findOne($this->id);
		
		        // add conditions that should always apply here
		
		        return $query;
		        
          }
    } 
 
public function getIsDelivered()   
    {
        switch($this->is_delivered)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
                
            }

    }
    
 public function getTotalAmountForServices($devise,$acad)
   {
        $t = 0;
    	  $sql='SELECT SUM(bs.price) as total_amount FROM billings_services bs inner join fee_services fs on(bs.fee=fs.id) WHERE  fs.old_new=1 AND fs.devise='.$devise.' AND request_date between (select date_start from academicperiods where id='.$acad.') and (select date_end from academicperiods where id='.$acad.')';
		  $result = Yii::$app->db->createCommand($sql)->queryAll();
           
          if($result!=null)
          { foreach($result as $r)
             $t = $r["total_amount"]; 
           }
           
           return $t;
   
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
