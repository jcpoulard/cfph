<?php

namespace app\modules\billings\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\billings\models\ChargeDescription;
use app\modules\billings\models\SrcChargeDescription;
use app\modules\billings\models\SrcDevises;

/**
 * This is the model class for table "charge_paid".
 *
 * @property integer $id
 * @property integer $id_charge_description
 * @property double $amount
 * @property integer $devise 
 * @property string $payment_date
 * @property string $comment
 * @property integer $academic_year
 * @property string $created_by
 *
 * @property Devises $devise0 
 * @property Academicperiods $academicYear
 * @property ChargeDescription $idChargeDescription
 */
class ChargePaid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'charge_paid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_charge_description', 'amount', 'devise', 'payment_date', 'academic_year'], 'required'],
            [['id_charge_description', 'devise', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['payment_date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 65],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => SrcDevises::className(), 'targetAttribute' => ['devise' => 'id']],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => SrcAcademicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['id_charge_description'], 'exist', 'skipOnError' => true, 'targetClass' => SrcChargeDescription::className(), 'targetAttribute' => ['id_charge_description' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_charge_description' => Yii::t('app', 'Charge Description'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'comment' => Yii::t('app', 'Comment'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }

    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getDevise0() 
		   { 
		       return $this->hasOne(SrcDevises::className(), ['id' => 'devise']); 
		   } 
		 
   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdChargeDescription()
    {
        return $this->hasOne(SrcChargeDescription::className(), ['id' => 'id_charge_description']);
    }
}
