<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PendingBalance */

$this->title = Yii::t('app', 'Create Pending Balance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pending Balances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pending-balance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
