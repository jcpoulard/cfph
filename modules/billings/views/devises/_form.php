<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Devises */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="devises-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">
           <?= $form->field($model, 'devise_name')->textInput(['maxlength' => true]) ?>

        </div>
        
        <div class="col-lg-4">
             <?= $form->field($model, 'devise_symbol')->textInput(['maxlength' => true]) ?>
             
        </div>
        
        <div class="col-lg-4">
           <br/>
              <?php  echo $form->field($model, 'is_default')->checkbox();  ?>
          </div>
   </div>
   <div class="row">
        <div class="col-lg-12">
             <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
        </div>
        
         
   </div>
    
    
     <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
