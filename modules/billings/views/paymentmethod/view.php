<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PaymentMethod */

$this->title = $model->method_name;

?>
<?= $this->render("//layouts/settingsLayout") ?>
<p></p>
    
<div class="row">
     <div class="col-lg-7">
         <h3><?= Html::encode($this->title) ?></h3>
     </div>
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'set_pm'], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'wh'=>'set_pm'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'set_pm'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

  
    
        </div>
    </div>

<div class="payment-method-view">

  
    <?= DetailView::widget([
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'model' => $model,
        'attributes' => [
            //'id',
            'method_name',
            'description:ntext',
            //'date_create',
            //'date_update',
            //'create_by',
            //'update_by',
        ],
    ]) ?>

</div>
