<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PaymentMethod */

$this->title = Yii::t('app', 'Create Payment Method');

?>
<?= $this->render("//layouts/settingsLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="payment-method-create">

       <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
