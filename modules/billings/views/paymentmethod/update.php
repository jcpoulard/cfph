<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PaymentMethod */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Payment Method',
]) . $model->method_name;

?>
<?= $this->render("//layouts/settingsLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="payment-method-update">

    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
