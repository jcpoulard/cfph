<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;

use app\modules\billings\models\SrcBillings;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Balance */

$this->title = $model->id;


$acad = Yii::$app->session['currentId_academic_year'];
?>


<div class="row">
    <div class="col-lg-7">
         <h3><?php   echo Yii::t('app','Balance to be paid'); ?> </h3>
    </div>
    <div class="col-lg-5">
        <p>
       <!-- <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'ri'=>0,'wh'=>'inc_bil'], ['class' => 'btn btn-info btn-sm']) ?>
       -->
        
    </p>
    </div>
</div> 

<div style="clear:both"></div>



<div class="balance-view">

<div class="row">
		<div class="col-lg-7 ">
		<?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
		            ['attribute'=>'student', 'label'=>Yii::t('app','First name'), 'value'=>$model->student0->first_name],
		            
		            ['attribute'=>'student', 'label'=>Yii::t('app','Last name'),'value'=>$model->student0->last_name],
		            
		            ['attribute'=>'balance','value'=>$model->getBalance()],
		            
		            'date_created:date',
		        ],
		    ]) ?>
		</div>
		<div class="col-lg-2 text-center" >
		   <?php
		        $path = $model->student0->image;
             ?>
		  <img src="<?php echo Url::to("@web/$path"); ?>" style="max-height:150px" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
		
		</div>
		<div class="col-lg-2 text-center">
                                                        <?php 
                                                        $program='';
                                                       if(isset($model->student0->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $model->student0->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($model->student0->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($model->student0->birthday).Yii::t('app',' yr old').' )'; 
                                                        }
					         	else
					         	  echo '';
					         	  
					         	                 $path = $model->student0->image;
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            
                            
                                                       
          </div>
    
</div>

<?php
   $modelBil = new SrcBillings;
$modelBillings = $modelBil->searchForBalance($_GET['stud']);

if($modelBillings->getModels()!=null)
{
?><div style="">
		<h2><span class="fa fa-2y" style="font-size: 15px; font-style:italic;"><?php echo Yii::t('app','On pending fees'); ?></span></h2> </div>
<?php 


?>
<?= GridView::widget([
        'dataProvider' => $modelBillings,
        'id'=>'billing-grid',
        //'filterModel' => $searchModel,
        'summary'=>"",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
  
            //'fee_period',
            ['attribute'=>'fee_fname',
			'label' => Yii::t('app','Fee name'),
               'content'=>function($data){
	                return $data->feePeriod->fee0->fee_label;//.' ('.$data->feePeriod->program0->label.')';
               }
             ],
             
              //'amount_to_pay',
            ['attribute'=>'amount_to_pay',
			'label' => Yii::t('app', 'Amount To Pay'),
               'content'=>function($data){
	                return $data->getAmountToPay();
               }
             ],
             
             //'balance',
             ['attribute'=>'balance',
			'label' => Yii::t('app', 'Balance'),
               'content'=>function($data){
	                return $data->getBalance();
               }
             ],

             ['attribute'=>'period_academic_lname',
			'label' => Yii::t('app', 'Academic period'),
               'content'=>function($data){
	                return $data->academicYear->name_period;
               }
             ],

         

                         
             
            
        ],
    ]); ?>
    
    
    
    
<?php    

}

 $modelBillings2 = $modelBil->searchForBalance2($_GET['stud']);

if($modelBillings2->getModels()!=null)
{
 ?><div  style="">
		<h2><span class="fa fa-2y" style="font-size: 15px; font-style:italic;"><?php echo Yii::t('app','On transactions'); ?></span></h2> </div>
<?php


?>
<?= GridView::widget([
        'dataProvider' => $modelBillings2,
        'id'=>'billing-grid',
        //'filterModel' => $searchModel,
        'summary'=>"",
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
  
            //'fee_period',
            ['attribute'=>'fee_fname',
			'label' => Yii::t('app','Fee name'),
               'content'=>function($data){
	                return $data->feePeriod->fee0->fee_label;//.' ('.$data->feePeriod->program0->label.')';
               }
             ],
             
              //'amount_to_pay',
            ['attribute'=>'amount_to_pay',
			'label' => Yii::t('app', 'Amount To Pay'),
               'content'=>function($data){
	                return $data->getAmountToPay();
               }
             ],
             
              
             ['attribute'=>'amount_pay',
			'label' => Yii::t('app', 'Amount Pay'),
               'content'=>function($data){
	                return $data->getAmountPay();
               }
             ],
             
             //'balance',
             ['attribute'=>'balance',
			'label' => Yii::t('app', 'Balance'),
               'content'=>function($data){
	                return $data->getBalance();
               }
             ],

             ['attribute'=>'period_academic_lname',
			'label' => Yii::t('app', 'Academic period'),
               'content'=>function($data){
	                return $data->academicYear->name_period;
               }
             ],

         

                         
             
            
        ],
    ]); ?>
    
    
    
    
<?php 

}


?>
</div>
