<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcOtherIncomesDescription;
use app\modules\billings\models\SrcDevises;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\OtherIncomes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="other-incomes-form">

    <?php $form = ActiveForm::begin(); ?>

 <div class="row">
        <div class="col-lg-6">
    <?php  
              echo $form->field($model, 'id_income_description')->widget(Select2::classname(), [
					                       'data'=>ArrayHelper::map(SrcOtherIncomesDescription::find()->where('category in( select id from label_category_for_billing where category not like(\'Services\') )')->all(),'id','income_description' ),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Select --')],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					    ?>
          </div>
        
        <div class="col-lg-6">
    
              <?php
                       if($model->income_date=='')
	                       $model->income_date = date('Y-m-d');
	                                   
                echo $form->field($model, 'income_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
       </div>
</div>

 <div class="row">
        <div class="col-lg-6">      
    <?= $form->field($model, 'amount')->textInput() ?>
        </div>
        
        <div class="col-lg-6">
           <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                   
		     ?>
    
       </div>
</div>

 <div class="row">
        <div class="col-lg-12"> 
         <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
          </div>
   </div>
          

      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
