<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcDevises;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcOtherIncomes */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Other Income');

?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create?ri='.$_GET['ri'].'&part=rec',], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>

   
<div class="row">
      
 <div class="col-lg-2">
       <?php $form = ActiveForm::begin(); ?>
       <?php   
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
										if($recettesItems!='')
                    $searchModel->recettesItems = [$recettesItems];
                    
						echo $form->field($searchModel, 'recettesItems')->widget(Select2::classname(), [

                       'data'=>loadRecettesItems(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select recettes items  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ], 
                       ]);  
		   
								   ?>
          <?php ActiveForm::end(); ?>
           
    </div>
</div> 

   
<div class="row">
<div class="wrapper wrapper-content other-incomes-index">
      
          
              <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Income Date'); ?></th>
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <th><?= Yii::t('app','Income Description'); ?></th>
				            <th><?= Yii::t('app','Comment'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

   
    $dataOtherIncomes = $dataProvider->getModels();
    
          foreach($dataOtherIncomes as $otherIncomes)
           {
           	    $modelDevise = SrcDevises::findOne($otherIncomes->devise);
                $devise_symbol = $modelDevise->devise_symbol;
         
           	  
           	   echo '  <tr >
                                                                       
                        <td >'.Yii::$app->formatter->asDate($otherIncomes->income_date).'</td>
                        
                          <td >'.$devise_symbol.' '.numberAccountingFormat($otherIncomes->amount).'</td>
                                                    
                        <td >'.$otherIncomes->idIncomeDescription->income_description.' </td>
                        
                        <td >'.$otherIncomes->description.'</td>
                                                    
                                                    <td >'; 
                                  
                                                              
                                                          if(Yii::$app->user->can('billings-otherincome-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/otherincomes/update?id='.$otherIncomes->id.'&wh=', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }

                                                              
                                                         if(Yii::$app->user->can('billings-otherincome-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/otherincomes/delete?id='.$otherIncomes->id.'&wh=', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>

</div></div>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Other Income List'},
                    {extend: 'pdf', title: 'Other Income List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });
        
        
        
      

JS;
$this->registerJs($script);

?>

