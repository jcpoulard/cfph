<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\OtherIncomes */

$this->title = Yii::t('app', 'Create Other Income');

?>
<div class="row">
    
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index?ri='.$_GET['ri'].'&part=rec'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>
    
    
<div class="wrapper wrapper-content other-incomes-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
