<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\OtherIncomes */

$this->title = Yii::t('app', 'Update Other Income ', [
    'modelClass' => 'Other Incomes',
]);

?>
<div class="row">
    
        <div class="col-lg-7">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-5">
<p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create?ri='.$_GET['ri'].'&part=rec',], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index?ri='.$_GET['ri'].'&part=rec', 'id' => $model->id ], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete?ri='.$_GET['ri'].'&part=rec', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    </div>

        
    </div>

<div class="wrapper wrapper-content other-incomes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
