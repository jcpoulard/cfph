<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\BillingsServices */

$this->title = Yii::t('app', 'Create Billings Services');

?>

<div class="row">
    
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>
    
    
<div class="wrapper wrapper-content billings-services-create">

      <?= $this->render('_form', [
        'model' => $model,
        'add_info'=>$add_info,
    ]) ?>

</div>
