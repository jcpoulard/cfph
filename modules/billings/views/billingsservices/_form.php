<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcFeeServices;
use app\modules\billings\models\SrcDevises;

use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\BillingsServices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="billings-services-form">

    <?php $form = ActiveForm::begin(); ?>

   
    <div class="row">

<?php     
     if($add_info==0)
     {
     	
 ?>
        <div class="col-lg-5">

    			<?php
                      
                      
                  /*    if((isset($_GET['id_prl'])&&($_GET['id_prl']!=''))&&($model->product_label=='') )
                       {
                       	    $product_id = preg_replace("/[^0-9]/","",$_GET['id_prl']);
                       	    
                       	    $model->product_label = $product_id;
                       	 }
                 */
             

    			
                    echo $form->field($model, 'student_id')->widget(Select2::classname(), [

                       'data'=>getStudentFullNameForServices(),//ArrayHelper::map(SrcPersons::find()->select(['id','last_name','first_name'])->where(['is_student'=>1])->andWhere(['in','active',[1,2] ])->all(),'id','first_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select student  --'),
                                   // 'onchange'=>'submit()',
                                    
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                             'tags' => true,
                    'tokenSeparators' => [',', ' '],
                             
                             
                         ],
                       ]) 
            ?>

        </div>
 <?php
        $col= 6;
        $fa= "fa fa-plus";
        $title = Yii::t('app', 'Add info student');
     }
  elseif($add_info==1)
    { $fa= "fa fa-minus";
     $title = Yii::t('app', 'Pick student in list');
     $col= 4;
    }
 ?>
        <div class="col-lg-1" style="margin-left:-15px; margin-right:15px; margin-top:12px;">
         
          <?php 
             echo '<input name="add_info_st" type="hidden" value="'.$add_info.'" />';
             ?>
          <?php
                  if(!isset($_GET['id']))
                     echo Html::submitButton('<i class="'.$fa.'"></i>', [
                                    'title' => $title,
                                    'name' => 'add_info_s',
                                    'class' => 'btn btn-primary btn-sm'
                        ]); ?>
           
         
        </div> 
 
 <?php     
     if($add_info==1)
     {   //ajoute info elev la
  ?>   
     </div>
     <div class="row">
        <div class="col-lg-4">      
            <?= $form->field($model, 'first_name')->textInput() ?>
        </div>
         <div class="col-lg-4">      
            <?= $form->field($model, 'last_name')->textInput() ?>
        </div>
         <div class="col-lg-4">      
            <?= $form->field($model, 'phone')->textInput() ?>
        </div>
      </div> 
      <div class="row">
        <div class="col-lg-4">      
            <?= $form->field($model, 'promotion')->textInput() ?>
        </div>
         <div class="col-lg-4">      
            <?= $form->field($model, 'option_program')->textInput() ?>
        </div>
        

<?php      
     }	
 ?>

   
  
   
   <div class="col-lg-<?= $col ?>">
    <?php  
              echo $form->field($model, 'fee')->widget(Select2::classname(), [
					                       'data'=>loadFeeServices(),//ArrayHelper::map(SrcFeeServices::find()->where('old_new =1')->all(),'id','serviceDescription.income_description' ),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Select --')],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					    ?>
          </div> 
  </div> 
<div class="row">    
    
     <div class="col-lg-6">
    
              <?php
                       if($model->request_date=='')
	                       $model->request_date = date('Y-m-d');
	                                   
                echo $form->field($model, 'request_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
       </div>

     <div class="col-lg-6">
            
         <?php        
              echo $form->field($model, 'is_delivered')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'No'), '1'=>Yii::t('app', 'Yes')],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'), ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

 </div>

<div class="row">    
    
     <div class="col-lg-6">
    
              <?php
                       
	                                   
                echo $form->field($model, 'delivered_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
       </div>
 </div>
 
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
