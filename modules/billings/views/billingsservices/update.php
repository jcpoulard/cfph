<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\BillingsServices */

$this->title = Yii::t('app', 'Update Billings Services');

?>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>


<div class="billings-services-update">

    <?= $this->render('_form', [
        'model' => $model,
        'add_info'=>$add_info,
    ]) ?>

</div>
