<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\web\JsExpression;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcBillingsServices */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Billings Services');

?>
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create',], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 
<p></p>



<div class="row">

 <div class="col-lg-2">
       <?php $form = ActiveForm::begin(); ?>
       <?php
               
				if($recettesItems!='')
                    $searchModel->recettesItems = [$recettesItems];

						echo $form->field($searchModel, 'recettesItems')->widget(Select2::classname(), [

                       'data'=>loadRecettesItems(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select recettes items  --'),
                                    'onchange'=>'submit()',

                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);

								   ?>
          <?php ActiveForm::end(); ?>

    </div>

</div>


<div class="wrapper wrapper-content billings-services-index">

     <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Fee Services'); ?></th>
				            <th><?= Yii::t('app','Request Date'); ?></th>
				            <th><?= Yii::t('app','Price'); ?></th>
				            <th><?= Yii::t('app','Delivered Date'); ?></th>
				           
				       <!--     <th><?php //echo Yii::t('app','Room'); ?></th>   -->
				            				            
				            <th></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 
            
    $dataBillingServices = $dataProvider->getModels();
    
          foreach($dataBillingServices as $billing_services)
           {
           	  $first_name = '';
           	       $last_name = '';
           	       
           	  if($billing_services->student_id!=NULL)
           	   {  $first_name = $billing_services->getPerson()->student->first_name;
           	       $last_name = $billing_services->getPerson()->student->last_name;
           	       
           	    }
           	  elseif($billing_services->student_id==NULL)
           	   {  $first_name = $billing_services->getPerson()->first_name;
           	       $last_name = $billing_services->getPerson()->last_name;
           	       
           	    }
           	
           	$devise = getDeviseNameSymbolById($billing_services->fee0->devise)->devise_symbol;  
           	
           	  if($billing_services->student_id!=null)
           	    $href= '../../fi/persons/moredetailsstudent?id='.$billing_services->student_id.'&is_stud=1';  
           	  else
           	     $href= '../../billings/billingsservicesitems/view?id='.$billing_services->id;//'../../fi/persons/moredetailsstudent?id='.$billing_services->student_id.'&is_stud=1';
           	     
           	   echo '  <tr > 
                                                    
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$first_name.' </a></td>
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$last_name.' </a></td>
                                                    <td >'.$billing_services->fee0->serviceDescription->income_description.' </td>
                                                    <td >'.Yii::$app->formatter->asDate($billing_services->request_date).'</td>
                                                    <td >'.$devise.' '.$billing_services->price.' </td>
                                                    <td >'.Yii::$app->formatter->asDate($billing_services->delivered_date).'</td>
                                                    
                                                    <td >';
                                                    switch ($billing_services->is_delivered)
                                                      {
										                case 0: // Si pending 
										                {

                                                    ?>
                <a href="#" class="delivered" data-name="<?= $billing_services->id; ?>" data-type="date" data-pk="<?= $billing_services->id ?>"  data-url="set-delivereddate?is=up" data-title="<?= Yii::t('app','Select delivered date') ?>">
                     <i class="fa fa-thumb-tack" style="color:#F47203;"></i>
                 </a>
                 
                                                  <?php  
                                                           										                }
										                break;
										               case 1: // Si livre 
                                                           {
                                                              echo Html::a('<span class="fa fa-thumbs-up" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/billings/billingsservices/nondelivered/?id='.$billing_services->id.'&wh=', [
                                    'title' => Yii::t('app', 'Make non delivered'),
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to make this item non delivered ?'),
                                    'data-method' => 'post',
                ]) ;
                                                           }
                                                           break;
                                                           
                                                           
                                                       }
                                                    echo '</td>';
                                                  
                                                   echo ' <td >'; 
                                                            
                                                          
                                                          if(Yii::$app->user->can('billings-billingsservices-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', '../billingsservices/update?id='.$billing_services->id, [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                      
                                                              
                                                         if(Yii::$app->user->can('billings-billingsservices-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', '../billingsservices/delete?id='.$billing_services->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>



</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            
             $('.delivered').editable();
             
             
             $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Billins-Services List'},
                    {extend: 'pdf', title: 'Billins-Services List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>


