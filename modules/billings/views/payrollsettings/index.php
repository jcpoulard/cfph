<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcPayrollSettings */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payroll Settings');

$payroll_setting_twice = infoGeneralConfig('payroll_setting_twice');
?>

 <?= $this->render('//layouts/billingSettingLayout'); ?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'set_pa'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content payroll-settings-index">
               
               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				   <?php      if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                          {
	                    ?>
	                         <th><?= Yii::t('app','Gross salary'); ?></th>
	                 <?php   
				                }
				            elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                            {
		           ?>
				            <th><?= Yii::t('app','Amount'); ?></th>
				    <?php
	                            }
				    ?>
				            <th><?= Yii::t('app','An Hour'); ?></th>
				            <th><?= Yii::t('app','Number Of Hour'); ?></th>
				   <?php     if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                            {
		           ?>
				            <th><?= Yii::t('app','Gross salary'); ?></th>
				    <?php
	                            }
				    ?>
				            <th><?= Yii::t('app','Frais'); ?></th>
				            <th><?= Yii::t('app','Assurance'); ?></th>
				            <th><?= Yii::t('app','As'); ?></th>
				            <!--  <th><?= Yii::t('app','Date Created'); ?></th>
				          <th><?= Yii::t('app','Created By'); ?></th>  -->
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 


    $dataPayrollSet = $dataProvider->getModels();
    
          foreach($dataPayrollSet as $payrollSet)
           {
           	   echo '  <tr >
                                                    <td >'.Html::a($payrollSet->person->first_name, Yii::getAlias('@web').'/index.php/billings/payrollsettings/view?id='.$payrollSet->id.'&wh=set_pa', [
                                    'title' => $payrollSet->person->first_name,
                        ]).' </td>
                                                    <td >'.Html::a($payrollSet->person->last_name, Yii::getAlias('@web').'/index.php/billings/payrollsettings/view?id='.$payrollSet->id.'&wh=set_pa', [
                                    'title' => $payrollSet->person->last_name,
                        ]).' </td>
                                                    <td >';
                                                    if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                                                   echo $payrollSet->getGrossSalaryInd();
	                                                 elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                                                     echo $payrollSet->getAmount();
                                                    
                                                    echo ' </td>
                                                    <td >'.$payrollSet->getAnHour().' </td>
                                                    <td >'.$payrollSet->getNumberHour().' </td>';
                                               if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
                                                   echo ' <td >'.$payrollSet->getGrossSalaryInd().' </td>';
                                                   echo ' <td >'.$payrollSet->getFrais().' </td>
                                                    <td >'.$payrollSet->getAssurance().' </td>
                                                    <td >'.$payrollSet->getAsValue().' </td>
                                                    <!--  <td >'.Yii::$app->formatter->asDate($payrollSet->date_created ).' </td>
                                                   <td >'.$payrollSet->created_by.' </td>  -->
                             
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                              
                                                         if(Yii::$app->user->can('billings-payrollsettings-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/payrollsettings/update?id='.$payrollSet->id.'&wh=set_pa', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('billings-payrollsettings-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/payrollsettings/delete?id='.$payrollSet->id.'&wh=set_pa', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Payroll Setting List'},
                    {extend: 'pdf', title: 'Payroll Setting List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>

