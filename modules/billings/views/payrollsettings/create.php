<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollSettings */

$this->title = Yii::t('app', 'Create Payroll Settings');

?>
 <?= $this->render('//layouts/billingSettingLayout'); ?>
<p></p>

<div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index','wh'=>'set_pa'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

 
 <div class="wrapper wrapper-content payroll-settings-create">

    <?= $this->render('_form', [
        'model' => $model,
        'message_noAmountEntered'=>$message_noAmountEntered,
    ]) ?>

</div>
