<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;


use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\PayrollSettingTaxes;


use app\modules\fi\models\SrcPersons;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollSettings */
/* @var $form yii\widgets\ActiveForm */


$acad=Yii::$app->session['currentId_academic_year'];

$payroll_setting_twice = infoGeneralConfig('payroll_setting_twice');

?>

<div class="payroll-settings-form">

    <?php $form = ActiveForm::begin(); ?>

 <div class="row">
        <div class="col-lg-4">
            <?php echo $form->field($model, 'group')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()','value'=>1 ]); ?> 
        </div>
      
        
        <?php				                  
                               if($model->group==0)
                                  {//one by one
                                   echo  '<div class="col-lg-4">'; 
    //_______________________________ YON MOUN KA GEN 2 CHEK (ALAFWA ANPLWAYE - PWOFESE)  ___________________________________________//       
                        	
                                      if(isset($_GET['id'])&&($_GET['id']!=''))
										 {  
										 	//$criteria = new CDbCriteria(array('group'=>'id','alias'=>'p', 'order'=>'last_name', 'condition'=>'p.is_student=0 AND p.active IN(1, 2) AND (p.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.') )) '));
										 	$data_person = ArrayHelper::map(SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.') )) ')->all(),'id','fullName' );
										 	
										  echo $form->field($model, 'person_id')->widget(Select2::classname(), [
					                       'data'=>$data_person,
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', ' --  select --')],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
										 	
										 	$dataProvider_updateOne =SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.') )) ')->all();
										 	
										 	$modPerson = $dataProvider_updateOne;
										    
										 }
										else
										  {
										  	 //$criteria = new CDbCriteria(array('group'=>'id','alias'=>'p', 'order'=>'last_name', 'condition'=>'p.is_student=0 AND p.active IN(1, 2) '));
										  	 
										  	 $data_person = ArrayHelper::map(SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) ')->all(),'id','fullName' );
										  	 $dataProvider_createOne =SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) ')->all();
										  	 
										  	 if($payroll_setting_twice==1) //yon moun ka konfigire 1 fwa selman
			                                   {  
			                                    	$data_person = ArrayHelper::map(SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id NOT IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.') )) ')->all(),'id','fullName' );
			                                    	$dataProvider_createOne = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id NOT IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.') )) ')->all();
			                                   }
			                                   
										  	 echo $form->field($model, 'person_id')->widget(Select2::classname(), [
					                       'data'=>$data_person,
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --')],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					                      
										  	 
										  	 
										  	  if($dataProvider_createOne!=null)
										  	    $there_is_employee_or_teacher=true;
										  	  else
										  	    $there_is_employee_or_teacher=false;
										  	    
										  	    
										  	  $modPerson = $dataProvider_createOne;
										  	  
										    }
			
	//__________________________________________________________________________________________________________________________// 
        							    
       							      echo ' </div>';

                                   }
                               elseif($model->group==1)
				                   { 
			                         if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa
			                           {
			                           	 echo  '<div class="col-lg-4">'; 
			                           	 
					                         if(!isset($_GET['all']))
		                                     {     
								                   echo $form->field($model, 'group_payroll')->widget(Select2::classname(), [
							                       'data'=>$model->getPayrollGroup(),
							                       'size' => Select2::MEDIUM,
							                       'theme' => Select2::THEME_CLASSIC,
							                       'language'=>'fr',
							                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
							                                        
							                       'pluginOptions'=>[
							                             'allowclear'=>true,
							                         ],
							                       ]) ;
								                   
		                                       }
		                                     else
		                                       {
		                                       	      $model_ = new SrcPayrollSettings;
												    
									                 echo $form->field($model_, 'group_payroll')->widget(Select2::classname(), [
								                       'data'=>$model_->getPayrollGroup(),
								                       'size' => Select2::MEDIUM,
								                       'theme' => Select2::THEME_CLASSIC,
								                       'language'=>'fr',
								                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
								                                       
								                       'pluginOptions'=>[
								                             'allowclear'=>true,
								                         ],
								                       ]) ;
									               						                      
		                                        	}
		                                        	
		                                       echo '</div>';
		                                        	
			                              }
			                          //else tou afiche tout moun
                                       	
                                       	
				                    }
                             ?>
   
        
         
 <?php if($model->group==0)
       {   
       	if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa
		   {
     ?>         
       <div class="col-lg-4">
             <?= $form->field($model, 'as_')->radio(['label' => Yii::t('app','Employees'), 'value' => 0, 'uncheck' => null,'style'=>'display:inline' ]) ?>
                      <?= $form->field($model, 'as_')->radio(['label' => Yii::t('app','Teachers'), 'value' => 1, 'uncheck' => null,'style'=>'display:inline' ]) ?>

         </div>
 <?php
		   }
       } 
 ?>      
 
 </div>
 
 
 
  <?php   
  
  $noPerson=false;
  $there_is_employee_or_teacher=true;
                          	
     if($model->group==0)
         { 
           
        
        
            //$modPerson=Persons::model()->findAll($criteria);
                    	
           if($modPerson!=null)
             {
             	
       ?>
       
 <br/>
 
  <div class="row">
        <div class="col-lg-4">
            <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                   
		     ?>
         </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'amount')->textInput() ?>
         </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'number_of_hour')->textInput(['placeholder'=>Yii::t('app', 'Leave blank if it\'s not a timely salary.')]) ?>
            
         </div>
  </div>       
         
<div class="row">         
         <div class="col-lg-4">
            <?= $form->field($model, 'frais')->textInput() ?>
         </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'assurance_value')->textInput(['placeholder'=>Yii::t('app', 'Total amount of assurance')]) ?>
            
         </div>
  </div>      
<?php        
    }
              
  }
 elseif($model->group==1)
   {         
   	
   	
  ?>  
     
     
     
     
  <br/>   
     
     
       
           
     <div class="list_secondaire" style="margin-left:10px; margin-bottom:-50px; width:90%;">
											

			<?php
			
			$header='';
              $condition='';
              $modelPers = new SrcPersons;
              
               
               $id='';
 
     //_______________________________ YON MOUN KA GEN 2 CHEK (ALAFWA ANPLWAYE - PWOFESE)  _______________________________________// 
        
                 if(!isset($_GET['all'])) //to create
                   { 
			    	  if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa
			            {	    
			    	  	    
			    	  	       if($model->group_payroll=='2')//teacher
		                          {
                                 	  $check_teacher_this_year= Yii::$app->db->createCommand('SELECT teacher FROM courses c left join academicperiods a on(a.id=c.academic_year) WHERE  c.old_new=1 AND (a.year='.$acad.' OR (a.id='.$acad.'))' )->queryAll();
		                        
		                        if($check_teacher_this_year==null)
		                           $there_is_employee_or_teacher =false;
		 
                                 	
                                $condition=' is_student=0 AND (p.id IN(SELECT teacher FROM courses c left join academicperiods a on(a.id=c.academic_year) WHERE c.old_new=1 AND (a.year='.$acad.' OR (a.id='.$acad.'))  ) AND p.id NOT IN(SELECT person_id FROM payroll_settings ps WHERE ps.as_=1  AND ps.academic_year='.$acad.') ) AND active IN(1, 2) ';  //depi moun nan ap fe ton kou
                                    
                                    $header=Yii::t('app','Teachers name');
                                 }
                               elseif($model->group_payroll=='1')//employee
                                  { 
                                  	
                                  	$check_employee_this_year= Yii::$app->db->createCommand('SELECT persons_id FROM persons_has_titles pt WHERE pt.academic_year='.$acad )->queryAll();
		                        if($check_employee_this_year==null)
		                           $there_is_employee_or_teacher =false;
		 

                                 $condition=' is_student=0 AND active IN(1, 2) AND ( p.id IN(SELECT persons_id FROM persons_has_titles pt WHERE pt.academic_year='.$acad.' ) AND p.id NOT IN(SELECT person_id FROM payroll_settings ps WHERE ps.as_=0 AND ps.academic_year='.$acad.') ) ';   //depi moun nan gen yon tit(title)
                                  
                                      $header=Yii::t('app','Employees name');
                                   }
                                   
			                 }
			             elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
			                  {
			                  	   // $check_employee_this_year= Yii::$app->db->createCommand('SELECT persons_id FROM persons_has_titles pt WHERE pt.academic_year='.$acad )->queryAll();
		                       // if($check_employee_this_year==null)
		                          // $there_is_employee_or_teacher =false;
		 

                                 $condition=' is_student=0 AND active IN(1, 2) AND ( p.id NOT IN(SELECT person_id FROM payroll_settings ps WHERE ps.as_=0 AND ps.academic_year='.$acad.') ) ';   //tout moun 
                                  
                                      $header=Yii::t('app','Employees name');
			                    
			                    }
                                                    
                         $dataProvider_noPerson =$modelPers->searchEmployeeForPayroll($condition);
                         $dataProvider = $dataProvider_noPerson;
                         
	    	  	       if($dataProvider_noPerson!=null)
	    	  	          $dataProvider_noPerson =$dataProvider_noPerson->getModels();
	    	  	       
	    	  	       
	    	  	       
                   }
                 else  //to update
                  {
                  	   if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa
			            {
                  	           if($model->group_payroll=='2')//teacher
                                 {
                                $condition='is_student=0 AND (p.id IN(SELECT teacher FROM courses c left join academicperiods a on(a.id=c.academic_year) WHERE c.old_new=1 AND (a.year='.$acad.' OR (a.id='.$acad.'))  ) AND (p.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.' AND ps.as_=1) ))  ) AND active IN(1, 2) ';   //depi moun nan ap fe ton kou
                                    
                                    $header=Yii::t('app','Teachers name');
                                 }
                               elseif($model->group_payroll=='1')//employee
                                  { $condition='is_student=0 AND active IN(1, 2) AND (p.id IN(SELECT persons_id FROM persons_has_titles pt WHERE  pt.academic_year='.$acad.'  ) AND (p.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.' AND ps.as_=0) ))  ) ';     //depi moun nan gen yon tit(title)
                                  
                                      $header=Yii::t('app','Employees name');
                                   } 
                                   
                            }         
                        elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
			                {   
			                    $condition='is_student=0 AND active IN(1, 2) AND ( p.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.academic_year='.$acad.' AND ps.as_=0) )  ) ';     //tout moun 
                                  
                                      $header=Yii::t('app','Employees name');	
			                  
			                  } 
                                 
                           $dataProvider_noPerson =$modelPers->searchEmployeeForPayroll($condition);
                           $dataProvider = $dataProvider_noPerson;
                           
	    	  	       if($dataProvider_noPerson!=null)
	    	  	         $dataProvider_noPerson =$dataProvider_noPerson->getModels();
	    	  	       
	    	  	        
                  	}            
               
        //___________________________________________________________________________________________________________________________//
          
                                        
           	  
				  		   
				  		   if($there_is_employee_or_teacher==false)
				  		   {
				  		   	    if($model->group_payroll==1)
				  			       { 
				  			       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','No employee to set payroll for.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				  			       	
				  			       }
				  			    elseif($this->group_payroll==2)
				  			     { 
				  			       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','No teacher to set payroll for.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				  			       	
				  			       }
				  			       
				  			      
				  		   	}
				  		   elseif( ($dataProvider_noPerson ==null)&&($condition!=null) ) 
				  			{   
				  				$noPerson=true;
				  				
				  				if($model->group_payroll==1)
				  			      $all='e';
				  			    elseif($model->group_payroll==2)
				  			      $all='t';
				  			   
				  			   //to get an id  
				  			     $mod_id= $model->search_($acad);
				  			      $mod_id=$mod_id->getModels();
				  			      foreach($mod_id as $i)
				  			        {
				  			        	$id = $i->id;
				  			        	 break;
				  			        	}
				  			      
				  			 	//echo '<span style="color:red;" >'.Yii::t('app','Payroll is already set for everyone.').Html::a(Yii::t('app',' Click here to use UPDATE option.'),['/billings/payrollSettings/update/','id'=>$id,'all'=>$all]).' </span> <br/>';
				  			 	     Yii::$app->getSession()->setFlash('info', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Payroll is already set for everyone.') ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
										
							      echo '<div><span style="color:red;" >'.Html::a(Yii::t('app',' Click here to use UPDATE option.'),['/billings/payrollSettings/update/','id'=>$id,'all'=>$all]).' </span></div> <br/>'; 
							      
				  			 	}
				  			
				  			if($message_noAmountEntered)
				  			 {
				  			 	      Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You must enter at least one AMOUNT.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
										
				  			 	}
				   			 			   
				  		              	    	  	
	    	  	 if(!isset($_GET['all'])) //to create
                   {
	    	  	       
	    	  	       //$dataProvider=$modelPers->searchEmployeeForPayroll($condition);
	   	  	    
	    
	if($dataProvider_noPerson!=null)
	    {	  	        
        echo GridView::widget([
        'id'=>'payroll-settings-grid',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'',
						 'label'=>$header,
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->getFullName();
							 }
						  ],
						  
						 
						  [
			             'attribute'=>'devise',
						 'label'=>Yii::t("app","Devise"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	  $option='';
						 	  $modelDevise=  getDevises();
						 	   foreach($modelDevise as $devise=>$val)
						 	   {
						 	   	$option = $option.'<option name="devise['.$data->id.']" value="'.$devise.'">'.$val.'</option>';
						 	   	}    
						 	        
						 	        $val ='<select name="devise['.$data->id.']">'.$option.'</select>
			          
					           <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  
						  [
			             'attribute'=>'amount',
						 'label'=>Yii::t("app","Amount"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="amount['.$data->id.']" type=text style="width:97%;" />
						 	               
			          
					           <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'number_of_hour',
						 'label'=>Yii::t("app","Number Of Hour"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="numberHour['.$data->id.']" type=text placeholder="'.Yii::t("app", "Leave blank if it\'s not a timely salary.").'" style="width:97%;" />
			          
					           <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'frais',
						 'label'=>Yii::t("app","Frais"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="frais['.$data->id.']" type=text style="width:97%;" />
						 	               
			          
					           <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'assurance_value',
						 'label'=>Yii::t("app","Assurance Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="assurance_v['.$data->id.']" type=text style="width:97%;" />
						 	               
			          
					           <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
               ]); 

	    }
					                          
                     }
                  else  //to update
                    {
                         
                         $dataProvider=$modelPers->searchPersonsForShowingPayrollSetting($condition,$acad);	
                         
                        
							
                          echo GridView::widget([
        'id'=>'payroll-settings-grid',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'',
						 'label'=>$header,
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->getFullName();
							 }
						  ],
						  
						 
						  [
			             'attribute'=>'amount',
						 'label'=>Yii::t("app","Amount"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="amount['.$data->id.']" type=text value="'.$data->amount.'" style="width:97%;" />
						 	               
			                    <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->personId.'" />
							   <input name="id_ps['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'number_of_hour',
						 'label'=>Yii::t("app","Number Of Hour"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="numberHour['.$data->id.']" type=text  placeholder="'.Yii::t("app", "Leave blank if it\'s not a timely salary.").'"  value="'.$data->numberHour_update.'" style="width:97%;" />
					          
							   <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->personId.'" />
							   <input name="id_ps['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'frais',
						 'label'=>Yii::t("app","Frais"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="frais['.$data->id.']" type=text placeholder="'.Yii::t("app", "Frais").'" value="'.$data->frais.'" style="width:97%;" />
						 	               
			                    <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->personId.'" />
							   <input name="id_ps['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  [
			             'attribute'=>'assurance_value',
						 'label'=>Yii::t("app","Assurance Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	        $val ='<input name="assurance_v['.$data->id.']" type=text placeholder="'.Yii::t("app", "Assurance Value").'" value="'.$data->assurance_value.'" style="width:97%;" />
						 	               
			                    <input name="id_pers['.$data->id.']" type="hidden" value="'.$data->personId.'" />
							   <input name="id_ps['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
               ]);  
                     
                     
                      }
	    	  	
	    	  	 
				

			 ?>

 
 
                        </div>       

                  
     
                    
    <?php   }

$display=false;

if($model->group==1)
{
   if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa
	 { 
	 	 if($model->group_payroll!='')
		      $display=true;
		    else
		       $display=false;
	  }
	elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	   { 
	   	   $display=true;
	   }
	   
 }
elseif($model->group==0) 
    $display=true;
    
  if(($noPerson==false)&&($there_is_employee_or_teacher==true)&&($display==true))  
    {
         	


 ?>
       
<?php // Obligation
                                       				   
					$taxes_desc = array();
					$modelTaxes = new SrcTaxes();
					$taxe = $modelTaxes->searchTaxesForPS($acad);
                 		
                 		if($taxe!=null)
				          {  
					         foreach($taxe as $id_taxe)
					           {
			 	                  if($id_taxe["taxe_description"]!="BONIS")
			 	                     $taxes_desc[$id_taxe["id"]] = $id_taxe["taxe_description"];									        						
								 }
											
							}

					 
				if($taxes_desc!=null)
				 {	   
?>
        
     <div class="col-lg-12">
                <div class="row">        
                    <label for=""><?php echo Yii::t('app','Choose taxes to be paid').'<br/>'; ?></label>                      
                </div> 
                <div class="row">               
                               <?php // Obligation
                                       				   
					         
						       $modelTaxe = new SrcTaxes;
				
				              if((isset($_GET['id'])))
                                 {  
                                   	$payrollSettingTaxe=new PayrollSettingTaxes;
                        $payrollSettingTaxe=PayrollSettingTaxes::find()->where(['id_payroll_set'=>$_GET['id'] ])->all();
                        
                           $checked_taxes = PayrollSettingTaxes::find()->where(['id_payroll_set'=>$_GET['id'] ])->all();
                                
                                
                                     $checked_taxes_array = array();
                                      if($checked_taxes!=null)
                                       { foreach($checked_taxes as $checked)
                                           $checked_taxes_array[] = $checked['id_taxe'];
                                        }
                                      
                                    if($taxe!=null)
				                     {   $i=0;
					                   foreach($taxe as $id_taxe)
					                      {   
			 	                               
			 	                                
			                                if (in_array($id_taxe["id"], $checked_taxes_array))  
					                          { 
		                                            if($id_taxe["taxe_description"]!="BONIS")
			 	                                      {
			 	                                         echo '<div style="float:left; margin-left:1%; margin-bottom:30px; width:auto;">   <div style="float:left; width:auto;"> <div style="float:left; margin-right:5px; width:auto; ">'.$id_taxe["taxe_description"].'</div><div style="float:right; margin-top:-2px; "><input type="checkbox" name="'.$id_taxe["id"].'" id="'.$id_taxe["id"].'" checked="checked" value="'.$id_taxe["id"].'"></div>  </div>     </div>';
			 	                                      }
			                              
					                           }
							                 else
							                  {
							                   	  if($id_taxe["taxe_description"]!="BONIS")
			 	                                     {
			 	                                        echo '<div style="float:left; margin-left:1%; margin-bottom:30px; width:auto;">   <div style="float:left; width:auto;"> <div style="float:left; margin-right:5px; width:auto; ">'.$id_taxe["taxe_description"].'</div><div style="float:right; margin-top:-2px; "><input type="checkbox" name="'.$id_taxe["id"].'" id="'.$id_taxe["id"].'" value="'.$id_taxe["id"].'"></div>  </div>     </div>';
			 	                                     }
			 	                                     
							                    }
 
								               
								               $i++;

										        						
											}
											
											
											
								       }
								      
                                      
			                            
                                     }
                                  else
                                    {   $i=0;
                                    	 foreach($taxe as $id_taxe)
					                      {    
			 	                             if($id_taxe["taxe_description"]!="BONIS")
			 	                               {
			 	                                 echo '<div style="float:left; margin-left:1%; margin-bottom:30px; width:auto;">   <div style="float:left; width:auto;"> <div style="float:left; margin-right:5px; width:auto; ">'.$id_taxe["taxe_description"].'</div><div style="float:right; margin-top:-2px; "><input type="checkbox" name="'.$id_taxe["id"].'" id="'.$id_taxe["id"].'" value="'.$id_taxe["id"].'"></div>  </div>     </div>';
			 	                               }
			 	                                
					                        }
                                   
                                                                       
					                  }
	         
	         
				 
				 
				 
	                     ?>
	               
	            </div> 
        </div>
  
   <?php
          
				 }
   
         }
        else 
           echo '<br/><br/>';
   ?> 


  
<?php
  if( ( ($model->group==1)&&( $dataProvider_noPerson!=null) )||($model->group==0) )
     {

?>
  <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>   
   
<?php
     }

?>
    <?php ActiveForm::end(); ?>

</div>
