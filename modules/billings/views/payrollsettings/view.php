<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollSettings */

$this->title = Yii::t('app', 'View {modelClass} for : ', [
    'modelClass' => 'Payroll Settings',
]) . $model->person->getFullName();

?>

<?= $this->render('//layouts/billingSettingLayout'); ?>
<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php  
                    echo $this->title
                        
                  ?>
    
    </h3>
    </div>

  

    <div class="col-lg-5">
        <p>
       <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'set_pa',], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create','wh'=>'set_pa', ], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'set_pa',], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id,'wh'=>'set_pa'], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>


</div> 

<div class="payroll-settings-view">

<?php
 $payroll_setting_twice = infoGeneralConfig('payroll_setting_twice');
  
  if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	{  $attributes = [
            
            
            [ 'attribute'=>Yii::t('app', 'Gross salary'),
               'value'=>$model->getGrossSalaryInd(),
            ],
            
            [ 'attribute'=>'an_hour',
               'value'=>$model->getAnHour(),
            ],
            
            [ 'attribute'=>Yii::t('app', 'Number Of Hour'),
               'value'=>$model->getNumberHour(),
            ],
            
            [ 'attribute'=>Yii::t('app', 'Frais'),
               'value'=>$model->getFrais(),
            ],
            [ 'attribute'=>Yii::t('app', 'Assurance'),
               'value'=>$model->getAssurance(),
            ],
            [ 'attribute'=>Yii::t('app', 'As'),
               'value'=>$model->getAsValue(),
            ],
            'date_created:datetime',
            'date_updated:datetime',
            'created_by',
            'updated_by',
            
            
        ];
        
	}
 elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	{  $attributes = [
            
            [ 'attribute'=>'amount',
               'value'=>$model->getAmount(),
            ],
            
            [ 'attribute'=>'an_hour',
               'value'=>$model->getAnHour(),
            ],
            
            [ 'attribute'=>Yii::t('app', 'Number Of Hour'),
               'value'=>$model->getNumberHour(),
            ],
            [ 'attribute'=>Yii::t('app', 'Gross salary'),
               'value'=>$model->getGrossSalaryInd(),
            ],
            [ 'attribute'=>Yii::t('app', 'Frais'),
               'value'=>$model->getFrais(),
            ],
            [ 'attribute'=>Yii::t('app', 'Assurance'),
               'value'=>$model->getAssurance(),
            ],
            [ 'attribute'=>Yii::t('app', 'As'),
               'value'=>$model->getAsValue(),
            ],
            'date_created:datetime',
            'date_updated:datetime',
            'created_by',
            'updated_by',
            
            
        ];
        
	}
		
		
?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ]) ?>

</div>

