<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcOtherIncomesDescription;
use app\modules\billings\models\SrcDevises;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\FeeServices */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fee-services-form">

    <?php $form = ActiveForm::begin(); ?>

 
 <div class="row">
        <div class="col-lg-4">
    <?php  
              echo $form->field($model, 'service_description')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcOtherIncomesDescription::find()->where('category in( select id from label_category_for_billing where category like(\'Services\'))')->all(),'id','income_description' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee service  --'), 'disabled'=>'disabled',
                           // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
					    ?>
          </div>
   
<div class="col-lg-4">
    <?= $form->field($model, 'price')->textInput() ?>
</div>
 <div class="col-lg-4">
           <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                   
		     ?>
    
       </div>
</div>

    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
