<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\FeeServices */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Fee Services',
]) . $model->serviceDescription->income_description;
?>
<?= $this->render("//layouts/billingSettingLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="fee-services-update">

       <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
