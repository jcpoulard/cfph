<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcOtherIncomesDescription;
use app\modules\billings\models\SrcDevises;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Fees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fees-services-form">

    <?php $form = ActiveForm::begin(
            [
            'id' => $model->formName(),
            'enableClientValidation'=> false
            ]
            ); 
    ?>

<br/>
    <div class="row">      
    <table class="table table-striped table-bordered table-hover tablo">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Fee Service') ?></th>
                <th><?= Yii::t('app','Devise') ?></th>
                <th><?= Yii::t('app','Price') ?></th>
                
            </tr>
                <tbody>
                    <?php
                        for($i=1;$i<=10;$i++){
                          ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td>
                            <?php
		          echo $form->field($model, 'service_description')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcOtherIncomesDescription::find()->where('category in( select id from label_category_for_billing where category like(\'Services\'))')->all(),'id','income_description' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee service  --'),
                           'id'=>'fee_s'.$i,
                           'name'=>'fee_s'.$i,
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false); 
		     ?>
                        </td>
                        <td>
                            <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                           'id'=>'devise'.$i,
                           'name'=>'devise'.$i,
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false); 
                   
		     ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'price')->textInput(['name'=>'price'.$i])->label(false); ?>
                        </td>
                        
                        
                    </tr>
                    <?php 
                        }
                    ?>
                </tbody>
        </thead>
    </table>
    
</div>	
     <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4 bouton">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


<br/><br/><br/><br/><br/>
    <?php ActiveForm::end(); ?>

    
    
    
</div>


