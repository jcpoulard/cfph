<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\FeeServices */

$this->title = Yii::t('app', 'Create Fee Services');

?>
<?= $this->render("//layouts/billingSettingLayout") ?>
<p></p>

 <div class="row">
    
         <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
  <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="fee-services-create">

   
    <?= $this->render('_formlist', [
        'model' => $model,
    ]) ?>

</div>
