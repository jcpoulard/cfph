<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcDevises; 


/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcScholarshipHolder */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Scholarship Holders');
  
$acad=Yii::$app->session['currentId_academic_year']; 


?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
           
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
        
</div> 


<div class="wrapper wrapper-content scholarship-holder-index">

                 <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Program'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Total amount'); ?></th>
				            <th><?= Yii::t('app','Sponsor'); ?></th>
				            <th><?= Yii::t('app','Percentage Pay'); ?></th>
				            				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataBilling = $dataProvider->getModels();
    
          foreach($dataBilling as $billing)
           {
           	   echo '  <tr >
                                                    <td >';
                                                    
                                                    $last_id_trans = lastBillingTransactionID($billing->student, $acad);
               	    
	                                                 if($last_id_trans!=0)
	                                                  echo Html::a($billing->student0->first_name, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$last_id_trans.'&ri=0&part=rec&stud='.$billing->student.'&from=sho', [
                                    'title' => $billing->student0->first_name,
                        ]);
                                                      else
                                                         echo $billing->student0->first_name;
                                                    
                                              echo '</td>
                                                    <td >';
                                                    
                                                    $last_id_trans = lastBillingTransactionID($billing->student, $acad);
               	    
	                                                 if($last_id_trans!=0)
	                                                  echo Html::a($billing->student0->last_name, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$last_id_trans.'&ri=0&part=rec&stud='.$billing->student.'&from=sho', [
                                    'title' => $billing->student0->last_name,
                        ]);
                                                      else
                                                         echo $billing->student0->last_name;
                                                    
                                              echo '</td>
                                                    <td >'.$billing->getProgram().' </td>
                                                    <td >'.$billing->getFee().'</td>
                                                    <td >';
                                                    
                                                    if($billing->fee!='')
									                 {   $program=getProgramByStudentId($billing->student0->id);
									                 	$fee_info_ = SrcFees::find()->where(['id'=>$billing->fee])->andWhere(['program'=> $program])->andWhere(['academic_period'=> $acad])->all(); 
									                 	 $fee_data = null;
									                 	 foreach($fee_info_ as $fee_inf)
									                 	   {
									                 	   	   $fee_data = $fee_inf;
									                 	   	 }
									                 	   	 
									                 	 echo  $fee_data->devise0->devise_symbol.' '.numberAccountingFormat($billing->getAmountForPercentage($billing->percentage_pay,$billing->fee,$program, $acad) );
									                 
									                  }
									               else
									                 {    $program=getProgramByStudentId($billing->student0->id); 
									                 	 //return 0 or the default model 
												      	    $modelDevise = new SrcDevises();
												      	    $default_devise = $modelDevise->checkDehaultSet();
								      	    
									                 	 echo  $default_devise->devise_symbol.' '.numberAccountingFormat($billing->getAmountForPercentage($billing->percentage_pay,NULL,$program, $acad) );
									                 }
                                                    
                                            echo   '</td>
                                                     <td >'.$billing->getPartner().' </td>
                                                     <td >'.$billing->percentage_pay.'</td>
                                                    
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Scholarship Holder List'},
                    {extend: 'pdf', title: 'Scholarship Holder List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>



	