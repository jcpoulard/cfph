<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use yii\grid\GridView;
use yii\helpers\Url;


use app\modules\billings\models\SrcScholarshipHolder;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\ScholarshipHolder */

$this->title = $model->student0->getFullName();

?>
  <?php // echo $this->render('//layouts/billingSettingLayout'); 
$acad = Yii::$app->session['currentId_academic_year'];?>
 
 
      

<div class="row">
    <div class="col-lg-6">
         <h3> 
            <?php  
                    echo Yii::t('app','Scholarship view').' : ';
             if(isset($model->student))
                       echo Html::a($model->student0->getFullName(), [ '/fi/persons/moredetailsstudent', 'id' => $model->student,'is_stud'=>1,'from'=>'schol'], ['class' => '']);                    
                      else
                        echo $full_name;
                        
                  ?>
    
    </h3>
    </div>

<?php
        
 
 ?>   

    <div class="col-lg-5">
        <p>
       
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['masscreate', 'from'=>'oth'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index','from'=>'oth'], ['class' => 'btn btn-info btn-sm']) ?>
        
    </p>
    </div>

<?php
          
?>

</div> 



<div style="clear:both"></div>



<div class="row">
   
     <div class="col-lg-10 scholarship-holder-view">
   
	 
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app', 'Sõpnsor'); ?></th>
				            <th><?= Yii::t('app', 'Fee'); ?></th>
                                            <th><?= Yii::t('app', 'Percentage Pay'); ?></th>
                                            <th><?= Yii::t('app', 'Is Internal'); ?></th>
                                            <th><?= Yii::t('app', 'Date Created'); ?></th>
                                            <th><?= Yii::t('app', 'Create By'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $searchModel = new SrcScholarshipHolder();
    $acad=Yii::$app->session['currentId_academic_year'];
     $dataProvider = $searchModel->searchForOneByAcad($model->student,$acad);
     $dataScholarshipHolder = $dataProvider->getModels();
     
    
          foreach($dataScholarshipHolder as $scholarshipholder)
           {
           	   echo '  <tr >
                                                  <td >'.$scholarshipholder->getPartner().' </td>   
                                                  <td >'.$scholarshipholder->getFee().' </td>   
                                                  <td >'.$scholarshipholder->percentage_pay.' </td>   
                                                  <td >'.$scholarshipholder->getIsInternal().' </td>   
                                                  <td >'.Yii::$app->formatter->asDate($scholarshipholder->date_created).' </td>   
                                                  <td >'.$scholarshipholder->create_by.' </td>   
                                                    
                                                    
                                                    <td >'; 
                                                
                                                         
                                              
                                                              
                                               
                                                         if(Yii::$app->user->can('billings-scholarshipholder-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/scholarshipholder/update?id='.$scholarshipholder->id.'&from=oth', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('billings-scholarshipholder-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/scholarshipholder/delete?id='.$scholarshipholder->id.'&from=oth', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                
	    
		
		</div>
		
		<div class="col-lg-2 text-center">
                                                        <?php 
                                                        
                                                   foreach($dataScholarshipHolder as $scholarshipholder)
                                                     {
                                                        $program='';
                                                       if(isset($scholarshipholder->student0->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $scholarshipholder->student0->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($scholarshipholder->student0->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($scholarshipholder->student0->birthday).Yii::t('app',' yr old').' )';
                                                        }
					         	else
					         	  echo '';
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            <?php if($scholarshipholder->student0->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {          $imaj = $scholarshipholder->student0->image;
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/$imaj")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>


                                                      <span>
                                                            <strong>
                                                        <?php 
					         	  								echo Yii::t('app', 'Level').' : ';
					         	  								if(isset($scholarshipholder->student0->studentLevel->level) )
					         	  								  echo $scholarshipholder->student0->studentLevel->level;
                                                     }	  						
                                                   ?>
					         	  						   </strong>
                                                        </span>
                            
                          
                        </div>
    
</div>


 


                                                       


 
 
 
 
 
 