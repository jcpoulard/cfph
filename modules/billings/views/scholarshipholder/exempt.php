<?php 
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\ScholarshipHolder */

$this->title = Yii::t('app', 'Record exemption');
?>


<div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index_exempt','from'=>'bil'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

 
 <div class="wrapper wrapper-content exempt-create">

    

    <?= $this->render('_exempt', [
        'model' => $model,
        'dataProvider'=>$dataProvider,
    ]) ?>

</div>






