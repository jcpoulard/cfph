<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\CheckBoxColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\ScholarshipHolder */
/* @var $form yii\widgets\ActiveForm */


?>
	
<div class="exempt-form">

    <?php $form = ActiveForm::begin(); ?>
 
   <div class="row">
        <div class="col-lg-4">
           <?php
                
                  
                 echo $form->field($model, 'student')->widget(Select2::classname(), [

                       'data'=>getStudentToExemptFee(),//ArrayHelper::map(SrcPersons::find()->select(['id','last_name','first_name'])->where(['is_student'=>1])->andWhere(['in','active',[1,2] ])->all(),'id','first_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select student  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
           ?>
        </div>
 </div>

<?php
    if($model->student!='')
      {   Yii::$app->session['exempt_student']=$model->student;
?>


 <div class="row">
        <div class="col-lg-9">
        
        <div  style="margin-top:-20px; margin-left:10px;">


   <?= GridView::widget([
        'id'=>'exempt',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         //'showFooter'=>true,
         // 'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '50px'], 
        ],
        
        
         [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Fees"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
			 	$acad=Yii::$app->session['currentId_academic_year']; 
                   	$year=$acad;
                $student = Yii::$app->session['exempt_student'];
				 return Yii::t("app",$data->fee_label)." (".$data->devise_symbol." ".numberAccountingFormat($data->getFeeAmount($student,$year,$data->amount)).')';
				 }
			  ],
			 
			 [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Deadline"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return Yii::$app->formatter->asDate( $data->date_limit_payment );
				 }
			  ],
			 
			 
			 [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Amount Already Paid"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
			 	$student = Yii::$app->session['exempt_student'];
			 	    return $data->getAmountPayOnFee($student,$data->id);
			      }
				 
			  ],
			  
			  
			  
			  ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '15px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['id'] ];
                                                 },
         ],
         
              ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>

    
 </div>       

        
       </div>
 </div>        

    
  
 <div class="row">
        <div class="col-lg-12">
           <?= $form->field($model, 'comment')->textarea(['rows' => 1])  ?>
        </div>
 </div>        


<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


   <?php
     
       
     
     
      }
    else
      unset(Yii::$app->session['exempt_student']);
    
     ?>
     
     

  <?php ActiveForm::end(); ?>

</div>


              

