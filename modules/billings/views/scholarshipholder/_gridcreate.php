<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPartners;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\ScholarshipHolder */
/* @var $form yii\widgets\ActiveForm */
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<?php

   function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }

//if(infoGeneralConfig('nb_grid_line')!=null){
//    $number_line = infoGeneralConfig('nb_grid_line');
//}else{
    $number_line = 6; 
//} 

            
?>



<div class="scholarship-holder-form">

    <?php $form = ActiveForm::begin(); ?>

  <div class="row">
        <div class="col-lg-4"> 
            <?php echo $form->field($model, 'student')->widget(Select2::classname(), [

                       'data'=>getStudentFullName(),//ArrayHelper::map(SrcPersons::find()->select(['id','last_name','first_name'])->where(['is_student'=>1])->andWhere(['in','active',[1,2] ])->all(),'id','first_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select student  --'),
                                    'onchange'=>'submit()',
                                       //chanje fee selon program
                                     /*   $.post("../../billings/fees/feeinprogram?from=schola&stud='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-shift").html(data);	
                                       //chanje course 
                                        $("#srcgrades-room").html("");
                                        	
                                        });',
                                    */
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ], 
                       ]);
                  ?>
          </div>

<?php
        if($model->student!='')
          {
?>      
          <div class="col-lg-4">
                   <br/>  
                  
              <?php 
                echo $form->field($model, 'is_full')->checkbox(['onchange'=> 'submit()','value'=>1, ]);
               /*
		            if($this->is_full==1)
                       { echo $form->checkBox($model,'is_full',array(','checked'=>'checked','name'=>'is_full','value'=>1));
				                             
                        }elseif($this->is_full==0)
                            echo $form->checkBox($model,'is_full',array('onchange'=> 'submit()','name'=>'is_full','value'=>null));
					
					echo Yii::t('app','Full scholarship');
					
				  */		               
                ?>
                 

            </div>
<?php
          }
?>
      </div>
   

<?php
        if($model->student!='')
          {
?>    
    <table id="" class="table table-striped table-bordered detail-view">
      <tbody> 
        <tr>
        <th><?php //echo Yii::t('app','Is Internal'); ?></th>
        <th><?php echo Yii::t('app','Sponsor'); ?></th>
        <?php if($is_full == 0) { ?>
        <th><?php echo Yii::t('app','Fee'); ?></th>
        <?php } ?>
        <th><?php echo Yii::t('app','Percentage Pay'); ?></th>
        </tr>
        
        <?php 
        
        $feesLabel = Yii::$app->session['feesLabel_scholarship'];
        
        
        for($i=0; $i<$number_row; $i++){ ?>
        <tr class="<?php echo evenOdd($i); ?>">
            <td>
                <?php 
                        echo $form->field($model, "is_internal[$i]")->checkbox(['onchange'=> 'submit()','value'=>1, ]);
		          /* if($internal[$i]==1)
                       { echo $form->checkBox($model,'is_internal',array('onchange'=> 'submit()','checked'=>'checked','name'=>'internal'.$i,'value'=>1));
				                             
                        }elseif($internal[$i]==0)
                            echo $form->checkBox($model,'is_internal',array('onchange'=> 'submit()','name'=>'internal'.$i,'value'=>null));
					*/		               
                ?>
                
            </td>
            <td>
                <?php
                     if($internal[$i]==0){ 
                        echo $form->field($model, "partner[$i]")->label(false)->widget(Select2::classname(), [

						                       'data'=>ArrayHelper::map(SrcPartners::find()->all(),'id','name' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select sponsor  --'),
						                                    'onchange'=>'',
						                                    
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
				       /* echo $form->dropDownList($model, 'partner',
                        CHtml::listData(Partners::model()->findAll(),'id','name'),
                        array( 'prompt'=>Yii::t('app','-- Please select --'),'name'=>'partner'.$i)
                        );
                        */
                      }
                   elseif($internal[$i]==1)
                     {   
                            echo $form->field($model, "partner_name[$i]")->label(false)->textInput(['disabled' => true,'value'=>$school_name,]);
                            
                            //echo $form->textField($model,'partner_name',array('size'=>60, 'disabled'=>'disabled','value'=>$school_name,'name'=>'partner_name'.$i));
                            }
                
                ?>
            </td>
            <?php if($is_full == 0) { ?>
            <td>
                <?php 
				         $program_ =0;
							$data_fee = [];
                   
                         
                      	 $level= 0;
                      	 $program= 0;
								if($student_name!='')
								  {   //return the last program id
								     $program= getProgramByStudentId($student_name);
								     
								     $level= getLevelByStudentId($student_name);
								   }
								   
								
                 	   
                             $data_fee = loadFeeNameByProgramLevelForScholarship($program,$level,$student_name,$acad);
                      
                     
                     echo $form->field($model, "fee[$i]")->label(false)->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFeesLabel::find()->all(),'id','fee_label' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
						                       
						                             		
					/*				 if($student_name!='')
									  {  $program = getProgramByStudentId($student_name); 
								          
									   }
						
					 $criteria = new CDbCriteria(array('alias'=>'fl','join'=>'inner join fees f on(f.fee=fl.id)','condition'=>'fl.fee_label NOT LIKE("Pending balance") AND f.level='.$level_,'order'=>'fl.id'));
		
		
                        echo $form->dropDownList($model, 'fee',
                        CHtml::listData(FeesLabel::model()->findAll($criteria),'id','fee_label'),

                        array('prompt'=>Yii::t('app','-- Please select fee --'),'name'=>'fee'.$i)
                        );
						*/		                        
                        
								
                        ?>
            </td>
            <?php  } ?>
            <td>
                 <?php 
                 
                       //if($percentage_pay[$i]!='')
                       
                 if($is_full == 0) 
                   { 
                 		echo $form->field($model, "percentage_pay[$i]")->label(false)->textInput(['placeholder'=>Yii::t('app','Percentage').'(%)','id'=>'percentage_pay'.$i,]);
                 		
                 		//echo $form->textField($model,'percentage_pay',array('size'=>60,'placeholder'=>Yii::t('app','Percentage').'(%)','name'=>'percentage_pay'.$i,'id'=>'percentage_pay'.$i,'onchange'=>'validatePercent("percentage_pay'.$i.'","'.$message_validation.'")')); 
                 		
                    }
                 elseif($is_full == 1) 
                   {      
                   	     //$model->setAttribute("[percentage_pay][$i]", 100);
                   	
                 	      echo $form->field($model, "percentage_pay")->label(false)->textInput(['placeholder'=>Yii::t('app','Percentage').'(%)','readonly'=>'true']);
                 	      
                 	      //echo $form->textField($model,'percentage_pay',array('size'=>60,'placeholder'=>Yii::t('app','Percentage').'(%)','name'=>'percentage_pay'.$i,'id'=>'percentage_pay'.$i, 'readonly'=>'true')); 
                   
                    }
                 		
                 	?>
            </td>
           
            
        </tr>      
        <?php } ?>
        
    </tbody>    
    </table>

 <?php 
     
        
        
   ?>
    
    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
           <input type="hidden" name="nombre_ligne" value="<?php echo $number_row; ?>"/>
        <?= Html::submitButton(Yii::t('app', 'Save') , ['name' =>'create' ,'id' =>'create' , 'class' => 'btn btn-success' ]) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','id'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

  <?php
        
      
      ?>   
 <?php
          }
?>  
<br/><br/><br/>  



<?php ActiveForm::end(); ?>
</div>
