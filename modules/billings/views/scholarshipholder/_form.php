<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPartners;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\ScholarshipHolder */
/* @var $form yii\widgets\ActiveForm */
$acad = Yii::$app->session['currentId_academic_year'];

?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 



<div class="scholarship-holder-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4"> 
            <?php 
            echo $form->field($model, "is_internal")->checkbox(['onchange'=> 'submit()','value'=>1, ]); ?>

         </div>
         <div class="col-lg-4"> 
    

      <!-- <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>    -->
    </div>
    </div>

  <div class="row">
        <div class="col-lg-4"> 
                      
           <?php 
           
                  if($model->is_internal==0){ 
                        echo $form->field($model, "partner")->widget(Select2::classname(), [

						                       'data'=>ArrayHelper::map(SrcPartners::find()->all(),'id','name' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select sponsor  --'),
						                                    'onchange'=>'',
						                                    
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
				       /* echo $form->dropDownList($model, 'partner',
                        CHtml::listData(Partners::model()->findAll(),'id','name'),
                        array( 'prompt'=>Yii::t('app','-- Please select --'),'name'=>'partner'.$i)
                        );
                        */
                      }
                   elseif($model->is_internal==1)
                     {   
                            echo $form->field($model, "partner")->textInput(['disabled' => true,'value'=>$school_name,]);
                            
                            //echo $form->textField($model,'partner_name',array('size'=>60, 'disabled'=>'disabled','value'=>$school_name,'name'=>'partner_name'.$i));
                            }
                            
                            
                                ?>
         </div>
         <div class="col-lg-4"> 
            <?php 
                        
                   $data_fee = [];
                   
                   if($model->fee!='')
                      {       $level= 0;
                                $program= 0;
								if($model->student!='')
								  {   //return the last program id
								     $program= getProgramByStudentId($model->student);
								   
								    $level= getLevelByStudentId($model->student);
								   }
								   
								
                 	   
                             $data_fee = loadFeeNameByProgramLevel($program,$level,$acad);
                       }
                                      
								       
                         
								       echo $form->field($model, 'fee')->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFeesLabel::find()->all(),'id','fee_label' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'',
                                                                                    'disabled' => true,
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
						                       
						 ?>

    </div>
         
         <div class="col-lg-4"> 
    <?php //$form->field($model, 'percentage_pay')->textInput()
            echo $form->field($model, "percentage_pay")->textInput(['placeholder'=>Yii::t('app','Percentage').'(%)','id'=>'percentage_pay',]);
     ?>
          </div>
    </div>

    
   
     <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['name' => 'update','id' => 'update', 'class' =>  'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','id'=>'cancel','class'=>'btn btn-warning']) ?>
        
            
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>    <?php ActiveForm::end(); ?>

</div>

