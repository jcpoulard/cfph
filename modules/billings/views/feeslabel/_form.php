<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcFeesLabel;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\FeesLabel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fees-label-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
        <div class="col-lg-6">
    <?= $form->field($model, 'fee_label')->textInput(['maxlength' => true]) ?>
         </div>
        
        <div class="col-lg-6">
    
            <?php  
                    $modelFL =new SrcFeesLabel();
                    $data_feeStatus = $modelFL->getStatusValue();
            ?>
                      <?= $form->field($model, 'status')->widget(Select2::classname(), [
                       'data'=>$data_feeStatus,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select status  --'),
                                    //'onchange'=>'submit()', 
                                   ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>       
          
           
       </div>
</div>

   
   <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
