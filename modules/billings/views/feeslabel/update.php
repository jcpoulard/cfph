<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\FeesLabel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Fees Description',
]) . $model->fee_label;

?>

<?= $this->render("//layouts/billingSettingLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="fees-label-update">

       <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
