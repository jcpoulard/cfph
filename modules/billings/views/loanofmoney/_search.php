<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\SrcLoanOfMoney */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-of-money-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'loan_date') ?>

    <?= $form->field($model, 'person_id') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'payroll_month') ?>

    <?php // echo $form->field($model, 'deduction_percentage') ?>

    <?php // echo $form->field($model, 'solde') ?>

    <?php // echo $form->field($model, 'paid') ?>

    <?php // echo $form->field($model, 'number_of_month_repayment') ?>

    <?php // echo $form->field($model, 'remaining_month_number') ?>

    <?php // echo $form->field($model, 'academic_year') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_updated') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
