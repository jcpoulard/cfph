<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\LoanElements;
use app\modules\billings\models\SrcPayroll;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\LoanOfMoney */

$this->title = $model->id;

$acad = Yii::$app->session['currentId_academic_year'];

$loan_payment_method = infoGeneralConfig('loan_payment_method');

$modelLoan = new SrcLoanOfMoney;
$modelLoanEl = new LoanElements;
$modelPayrol= new SrcPayroll;

 
?>

<?php 

/*  if(isset($_GET['msguv'])&&($_GET['msguv']=='y'))
           $this->message_UpdateAlreadyPaid=true;
	
			//error message
	    if(($this->message_UpdateAlreadyPaid))		
			      { echo '<div class="" style=" padding-left:0px;margin-right:250px;  margin-bottom:0px; ';//-20px; ';
				      echo '">';
				      
				      echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	
				      				      
			       }			      
				 				   
				  if($this->message_UpdateAlreadyPaid)
				     { echo '<span style="color:red;" >'.Yii::t('app','Update is denied, this loan is already paid.').'</span>';
				     $this->message_UpdateAlreadyPaid=false;
				     }
				     
				 
			 echo'</td>
					    </tr>
						</table>';
*/						
?>

<div class="loan-of-money-view">

  

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php   //if(isset($model->person))
                       echo Yii::t('app','Loan').' : '.Html::a($model->person->getFullName(), ['/fi/persons/moredetailsemployee', 'id' => $model->person_id, 'is_stud'=>0, 'from' =>'emp' ], ['class' => '']);                    // part/rec/ri/'.$ri.'
                    //  else
                      //  echo $full_name;
                        
                  ?>
    
    </h3>
    </div>
    <div class="col-lg-5">
        <p>
 <?php
   
 if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("Administration économat"))  )  
   {  
     if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob otomatik
       {
     ?>   
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id, ], ['class' => 'btn btn-success btn-sm']) ?>
   <?php
         }
     ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create',], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', ], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
  <?php
   
   }  
?>
    </p>
    </div>
</div> 




   

<div style="clear:both"></div>   


<div class="row">
   
           
<div class="col-lg-3">
	 <strong><?php  
	                  echo Yii::t('app','Loan summary');
	                    
	         ?></strong>
	         
      <table class="detail-view table table-striped table-condensed">
        
        <tr class="odd"><th><?php echo Yii::t('app','Total Loan '); ?></th></tr>
        <tr class="odd">
            <td >
                    
                     <?php // 
                                       				   
					$total_loan = 0;
					
				$total_loan = $modelLoan->getTotalLoan($model->person_id, $acad);
                 echo '<div style="margin-left:20px;   " > '.$model->devise0->devise_symbol.' '.numberAccountingFormat($total_loan).' </div>';
								       				 
	                     ?>
        
            </td>
        </tr>
        
        <tr class="odd"><th><?php echo Yii::t('app','Total Balance'); ?></th></tr>
        <tr class="odd">
        <td>

                     <?php // 
                           $total_solde = 0;            				   
					 $total_solde = $modelLoan->getTotalSolde($model->person_id, $acad);
					  
					echo '<div style="margin-left:20px;   " > '.$model->devise0->devise_symbol.' '.numberAccountingFormat($total_solde).' </div>';			       				 
	                     ?>        
        
        </td>
        </tr>
        
        
    </table>

</div>



<div class="col-lg-6">


<?php

if($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
     {
     	 $modelLoanEl = new LoanElements;
     	 
     	if(isset($_GET['id']) )
     	  $dataProvider = $modelLoanEl->searchElementsByLoan_id($_GET['id']);
     	else
     	   $dataProvider = $modelLoanEl->searchElementsByLoan_id($model->id);
     	   
     	    	 
     	   
     	echo  GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>"",
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'loan_id',
            
             ['attribute'=>'month',
		                'label'=>Yii::t('app','Month'),
			        'content'=>function($data,$url){
	                              return $data->getLongMonth();
                               }
					],
					
			['attribute'=>'amount',
		                'label'=>Yii::t('app','Amount Refund'),
			        'content'=>function($data,$url){
	                               return $data->getAmount();
                               }
			 ],
			 
		   ['attribute'=>'loan_rate',
		              //  'label'=>Yii::t('app','Loan Rate'),
			        'content'=>function($data,$url){
			        	$acad = Yii::$app->session['currentId_academic_year'];
	                               return $data->getLoanRate($data->loan->person_id,$data->month,$acad);
                               }
			 ],
            
            ['attribute'=>'is_paid',
		                'label'=>Yii::t('app','Paid'),
			        'content'=>function($data,$url){
	                               return $data->getElementPaid();
                               }
			  ],
           
          // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    

     }
 elseif($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob otomatik
     {
?>


 <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            
            ['attribute'=>'loan_date',
		                'label'=>Yii::t('app','Loan Date'),
			        'value'=>Yii::$app->formatter->asDate($model->loan_date),
					],
					
            ['attribute'=>'amount',
		                'label'=>Yii::t('app','Amount'),
			        'value'=>$model->getAmount(),
					],
		
		    ['attribute'=>'deduction_percentage',
		                'label'=>Yii::t('app','Deduction Percentage'),
			        'value'=>$model->deduction_percentage."% ",
					],
		
		    ['attribute'=>'repayment_start_on',
		                'label'=>Yii::t('app','Repayment start on'),
			        'value'=>$model->getLongMonth(),
					],
	    
	        ['attribute'=>'number_of_month_repayment',
		                'label'=>Yii::t('app','Number Of Month Repayment'),
			        'value'=>$model->number_of_month_repayment,
					],
					
		    ['attribute'=>'remaining_month_number',
		                'label'=>Yii::t('app','Remaining Month Number'),
			        'value'=>$model->remaining_month_number,
					],


		'solde',
		
		['attribute'=>'paid',
		                'label'=>Yii::t('app','Paid'),
			        'value'=>$model->getLoanPaid(),
					],
					
					
        ],
    ]) ?>
    
    
<?php
     }
?>

</div>
    
    
<div class="col-lg-2 text-center">
                                                        <?php 
                                echo '<b>'.Yii::t('app','Gross salary').': '.$modelPayrol->getGrossSalary($model->person->payrollSettings[0]->devise,$model->person_id).'</b>';                         					         	  
					         	                 $path = 'documents/photo_upload/'.$model->person->image;
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                           <?php if($model->person->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/$path")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>
                             
                                                        
          </div>    
      
</div>

<div style="clear:both"></div>
 <!-- Seconde ligne -->
 

<div class="row">
            
<div class="col-lg-12">

<?php

if($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
     {
     	 echo GridView::widget([
	        'dataProvider' => $modelLoan->searchForView($model->person_id,$acad),
	        'id'=>'loan-of-money-grid',
	        //'filterModel' => $searchModel,
	        'summary'=>"",
	        'columns' => [
	           // ['class' => 'yii\grid\SerialColumn'],
	
	            //'id',
	                        
	            ['attribute'=>'id',
				'label' => Yii::t('app', 'ID'),
	               'content'=>function($data,$url){
		                return Html::a($data->id, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->id,
	                        ]);
	               }
	             ],
	          
	          ['attribute'=>'loan_date',
				'label' => Yii::t('app', 'Loan Date'),
	               'content'=>function($data,$url){
		                return Html::a(Yii::$app->formatter->asDate($data->loan_date), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => Yii::$app->formatter->asDate($data->loan_date),
	                        ]);
	               }
	             ],

	             ['attribute'=>'amount',
				'label' => Yii::t('app', 'Initial Amount'),
	               'content'=>function($data,$url){
		                return Html::a($data->getAmount(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->getAmount(),
	                        ]);
	               }
	             ],

	            ['attribute'=>'remaining_month_number',
				'label' => Yii::t('app', 'Remaining month number'),
	               'content'=>function($data,$url){
		                return Html::a($data->remaining_month_number, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->remaining_month_number,
	                        ]);
	               }
	             ],
	
	          
	          ['attribute'=>'solde',
				'label' => Yii::t('app', 'Solde'),
	               'content'=>function($data,$url){
		                return Html::a($data->getSolde(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->getSolde(),
	                        ]);
	               }
	             ],

	             ['attribute'=>'paid',
				'label' => Yii::t('app', 'Paid'),
	               'content'=>function($data,$url){
		                return Html::a($data->getLoanPaid(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->getLoanPaid(),
	                        ]);
	               }
	             ],
	
			                       
	        ],
	    ]);
     	
     }
 elseif($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob otomatik
     {

	       echo GridView::widget([
	        'dataProvider' => $modelLoan->searchForView($model->person_id,$acad),
	        'id'=>'loan-of-money-grid',
	        //'filterModel' => $searchModel,
	        'summary'=>"",
	        'columns' => [
	           // ['class' => 'yii\grid\SerialColumn'],
	
	            //'id',
	                        
	            ['attribute'=>'id',
				'label' => Yii::t('app', 'ID'),
	               'content'=>function($data,$url){
		                return Html::a($data->id, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->id,
	                        ]);
	               }
	             ],
	
	            ['attribute'=>'remaining_month_number',
				'label' => Yii::t('app', 'Remaining month number'),
	               'content'=>function($data,$url){
		                return Html::a($data->remaining_month_number, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->remaining_month_number,
	                        ]);
	               }
	             ],
	
	             ['attribute'=>'paid',
				'label' => Yii::t('app', 'Paid'),
	               'content'=>function($data,$url){
		                return Html::a($data->getLoanPaid(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id, [
	                                    'title' => $data->getLoanPaid(),
	                        ]);
	               }
	             ],
	
			                       
	        ],
	    ]); 

     }
 ?>
</div>


</div>

</div>


