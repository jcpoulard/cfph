<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;

use app\modules\rbac\models\User;




/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcLoanOfMoney */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(Yii::$app->session['profil_as']==0)
   $this->title = Yii::t('app', 'Loan Of Moneys');
elseif(Yii::$app->session['profil_as']==1)
 {  
 	$person_id = 0;
          	         $modelUser = User::findOne(Yii::$app->user->identity->id); 

				     if($modelUser->person_id!=NULL)
				         $person_id = $modelUser->person_id;
				 
				$modelPers = SrcPersons::findOne($person_id); 
				        
      $this->title = Yii::t('app', 'Loan history').' [ '.$modelPers->fullName.' ]';	
  }
$acad=Yii::$app->session['currentId_academic_year']; 

$loan_payment_method = infoGeneralConfig('loan_payment_method');
         	
         	
        
?>



<div class="row">
 <?php
    if(Yii::$app->session['profil_as']==0)
      {
 ?>   
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'mod'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
 <?php
      }
 ?>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>


<div class="wrapper wrapper-content loan-of-money-index">

              <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				 <?php
    if(Yii::$app->session['profil_as']==0)
      {
 ?>   
            
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
<?php
      }
   elseif(Yii::$app->session['profil_as']==1)
     {
?>				  <th></th>
<?php
     }
?>          
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <th><?= Yii::t('app','Total Balance'); ?></th>
				            <th><?= Yii::t('app','Repayment start on'); ?></th>
				            <th><?= Yii::t('app','Loan Date'); ?></th>
				            <th><?= Yii::t('app','Paid'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataLoan = $dataProvider->getModels();
    
    $i=1;
          foreach($dataLoan as $loan)
           {
           	   echo '  <tr >';
           	                
           	 
    if(Yii::$app->session['profil_as']==0)
      {
  
                
                              echo  '          <td >'; echo Html::a($loan->person->first_name, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$loan->id.'&', [
                                    'title' => $loan->person->first_name,
                        ]).' </td>
                                                    <td >'; echo Html::a($loan->person->last_name, Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$loan->id.'&', [
                                    'title' => $loan->person->last_name,
                        ]).' </td>';
                        
      }      
    elseif(Yii::$app->session['profil_as']==1)
      { echo '<td >'.$i++.'</td >';  
          //$i
      }                  
                                              echo'      <td >'.$loan->devise0->devise_symbol.' '.$loan->amount.' </td>
                                                    <td >'.$loan->devise0->devise_symbol.' '.$loan->getTotalSolde($loan->person->id, $acad).' </td>
                                                    <td >'.$loan->getLongMonth().' </td>
                                                    <td >'.Yii::$app->formatter->asDate($loan->loan_date).' </td>
                                                    <td >'.$loan->getLoanPaid().' </td>
                             
                                                    <td >'; 
                                                
                                                         
                                               /*          if(Yii::$app->user->can('billings-loanofmoney-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', '../loanofmoney/view?id='.$loan->id.'&', [
                                    'title' => Yii::t('app', 'view'),
                        ]); 
                                                              }
                                                  */
                                                              
                                            if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob otomatik
                                               {   
                                                         if(Yii::$app->user->can('billings-loanofmoney-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/loanofmoney/update?id='.$loan->id.'&', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                             }
                                                             
                                               }
                                                              
                                                         if(Yii::$app->user->can('billings-loanofmoney-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/loanofmoney/delete?id='.$loan->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>




</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Loan List'},
                    {extend: 'pdf', title: 'Loan List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>

