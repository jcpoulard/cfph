<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\LoanOfMoney */

$this->title = Yii::t('app', 'Update {modelClass} for: ', [
    'modelClass' => 'Loan Of Money',
]) . $model->person->getFullName();

?>

<div class="row">
    <div class="col-lg-8">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-4">
            <p>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
       <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create',], ['class' => 'btn btn-primary btn-sm']) ?>
   
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
            </p>
    </div>
        
    </div>


<div class="wrapper wrapper-content loan-of-money-update">

   <?= $this->render('_form', [
        'model' => $model,
        'messageLoanNotPaid'=>$messageLoanNotPaid,
        'messageLoanNotPossible'=>$messageLoanNotPossible,
    ]) ?>

</div>
