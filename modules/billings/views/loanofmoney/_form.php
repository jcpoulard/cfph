<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\LoanOfMoney */
/* @var $form yii\widgets\ActiveForm */

$acad=Yii::$app->session['currentId_academic_year']; 
?>



<div class="loan-of-money-form">

    <?php $form = ActiveForm::begin(); ?>

<?php
/* //error message 
	        	if(($this->messageLoanNotPaid)||($this->messageLoanNotPossible)||($this->messageAmountNotAllowed)||($this->messageRemainPayroll)||($this->messageRefund))//||($this->success))
			      { echo '<div class="" style=" padding-left:0px;margin-right:250px; margin-bottom:-17px; ';//-20px; ';
				      echo '">';
				      				      
			           echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';
			       
			       }			      
				 					     	
				   
				    if($this->messageLoanNotPaid)
				      {  echo '<span style="color:red;" >'.$model->person->fullName.', '.Yii::t('app','You are not available to get a loan. Your solde must be zero.').'</span>'.'<br/>';
				      
				          $this->messageLoanNotPossible=false;
				      }
				 
				    if($this->messageLoanNotPossible)
				      echo '<span style="color:red;" >'.Yii::t('app','You cannot get a loan at this month. Payroll or next payroll is already done.').'</span>'.'<br/>';
				    
				     
				    if($this->messageAmountNotAllowed)
				      echo '<span style="color:red;" >'.Yii::t('app','Amount not allowed.').'</span>'.'<br/>';
				      
				    
				     if($this->messageRemainPayroll)
				        echo '<span style="color:red;" >'.Yii::t('app','Number of payroll remain < Repayment deadline(numb. payroll).').'</span>'.'<br/>';

                     
                     if($this->messageRefund)
				        echo '<span style="color:red;" >'.Yii::t('app','Refund > Net salary.').'</span>'.'<br/>';


					   
			     if(($this->messageLoanNotPaid)||($this->messageLoanNotPossible)||($this->messageAmountNotAllowed)||($this->messageRemainPayroll)||($this->messageRefund))//||($this->success))
 */

?>    
    <div class="row">
       <div class="col-lg-4">
           <?= $form->field($model, 'person_id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcPersons::find()->where('is_student=0 and active in(1,2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (academic_year='.$acad.') )) ')->all(),'id','fullName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select a person  --'), 'onChange'=>'submit()'],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
	    </div> 

<?php
        if($model->person_id!='')
          {
?>	    
	     <div class="col-lg-4"> 
	   			 <?= $form->field($model, 'amount')->textInput() ?>
	    </div>
		
		<div class="col-lg-4">
    	
		    <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                   
		     ?>
		     
		     
		    
	 	</div>

		 <?php
          }
?>	    
	    
    </div>

<?php
        if($model->person_id!='')
          {
?>	    
   
    <div class="row">
        
		<div class="col-lg-4"> 
	   		 <?php
                echo $form->field($model, 'loan_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
	
	    </div>

        <div class="col-lg-4"> 
	  		  <?= $form->field($model, 'number_of_month_repayment')->textInput() ?>
	    </div>
		
		<div class="col-lg-4"> 
			    <?php
			    echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [

						                       'data'=>getSelectedLongMonthValueForOne($model->person_id),  
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select repayment start  --'),
						                                    
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
	               ?>
	    </div>
	 </div>
	 
<?php 
      if((!$messageLoanNotPaid)&&(!$messageLoanNotPossible))       
        {
 ?>	    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

 <?php
          }
 
  }
?>	    

    <?php ActiveForm::end(); ?>

</div>
