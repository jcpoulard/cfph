<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\helpers\Url;

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcLoanOfMoney;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Payroll */

$this->title = $model->id;

$modelPers = new SrcPersons;
$modelPayroll = new SrcPayroll;
$modelPS= new SrcPayrollSettings;
$modelLoan = new SrcLoanOfMoney;

 
 $acad = Yii::$app->session['currentId_academic_year'];
 
 
      	 $birthday = '0000-00-00';
     $image='';
    
   if(isset($model->idPayrollSet->person_id))
      {
      	 $person_id = $model->idPayrollSet->person->id;
      	 $birthday = $model->idPayrollSet->person->birthday;
      	 $image= $model->idPayrollSet->person->image;
      	 $full_name = $model->idPayrollSet->person->getFullName();
      }
   else
      {
      	  if(isset($_GET['emp']))
      	    {  $person_id = $_GET['emp'];
      	       
      	       $emp=SrcPersons::findOne($person_id);
      	   
      	       $birthday = $emp->birthday;
      	       
      	       $image= $emp->image;
      	       $full_name = $emp->getFullName();
      	   
      	    }
       }

 $type_profil = '';//Yii::app()->session['profil_selector'];


     if(isset($_GET['all_']))
       $group=Yii::$app->session['payroll_group'];
     
     $grouppayroll=Yii::$app->session['payroll_group_payroll'];
     
     
     if(Yii::$app->session['payroll_payroll_month_']!='')
        { $payroll_month=Yii::$app->session['payroll_payroll_month_'];
             
        }
     else
        {
        	$payroll_month=1;
        	
        	}

?>

<?php
if(Yii::$app->user->can('Manager') || Yii::$app->user->can('Teacher') || $type_profil == "emp" || $type_profil=="teach")
 { 
    if(isset($_GET['id'])){
    $id_payroll = $_GET['id'];
    $data = User::find()
                   ->select(['id'])
                   ->where('person_id = (SELECT DISTINCT ps.person_id FROM payroll p inner join payroll_settings ps ON (p.id_payroll_set = ps.id) where p.id ='.$id_payroll.')' )
                   ->all();
    
    $compteur=0;
    foreach($data as $d){
       // echo $d->id;
        if($d->id == Yii::$app->user->identity->id){
            
            $compteur = 1;
        }
    }
   
    
}

if($compteur==1){
    
}else{
    
  Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','{name}, You tried to violate your access level !', ['name'=>Yii::$app->user->identity->username] ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
  
   return $this->redirect(Yii::$app->user->returnUrl);
}
    
}
    
    

?>

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php  
                    echo Yii::t('app','Payroll view : ').' ';
             if(isset($model->idPayrollSet->person))
                       echo Html::a($model->idPayrollSet->person->getFullName(), [ '/fi/persons/moredetailsemployee', 'id' => $model->idPayrollSet->person_id,'di'=>0,'is_stud'=>0,'from'=>'emp'], ['class' => '']);                    
                      else
                        echo $full_name;
                        
                  ?>
    
    </h3>
    </div>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administration économat"))  )
          {
 
 ?>   

    <div class="col-lg-5">
        <p>
       <?php if((isset($_GET['from1']))&&($_GET['from1']=='vfr'))
               echo Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'from'=>'view','month_'=>$_GET['month_'], 'year_'=>$_GET['year_'], 'di'=>$_GET['di'],'emp'=>$_GET['emp'],'from1'=>'vfr', 'part'=>'pay'], ['class' => 'btn btn-success btn-sm']);
              else
               echo Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'from'=>'view','month_'=>$_GET['month_'], 'year_'=>$_GET['year_'], 'di'=>$_GET['di'],], ['class' => 'btn btn-success btn-sm']);
               
               ?>
               
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'ri'=>0,'wh'=>'inc_bil'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'ri'=>0,'month_'=>$_GET['month_'], 'year_'=>$_GET['year_'], 'di'=>$_GET['di'],'wh'=>'inc_bil'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id,'month_'=>$_GET['month_'], 'year_'=>$_GET['year_'], 'di'=>$_GET['di'] ], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

<?php
          }
?>

</div> 




   

<div style="clear:both"></div>

<!-- La ligne superiure  -->
<div class="row">
   
     <div class="col-lg-7 payroll-view">
   
		    <?= DetailView::widget([
		        'model' => $model,
		        'attributes' => [
		      
			         ['attribute'=>Yii::t('app','Salary'),
		                //'header'=>Yii::t('app','Salary'),
			        'value'=>$model->getGrossSalaryInd($model->idPayrollSet->devise,$model->idPayrollSet->person_id,$model->payroll_month,getYear($model->payment_date)),
					],
					
        
                    ['attribute'=>Yii::t('app','Number Of Hour'),
		               // 'header'=>Yii::t('app','Number Of Hour'),
			        'value'=>$model->idPayrollSet->numberHour,
					],
					
		            ['attribute'=>Yii::t('app','Frais'),
			           'value'=>$model->Frais,
			           ],
			           
			        ['attribute'=>Yii::t('app','Plus Value'),
			           'value'=>$model->PlusValue,
			           ],
			           
			        ['attribute'=>Yii::t('app','Taxe'),
		                //'header'=>Yii::t('app','Taxe'),
			        'value'=>$model->getTaxe(),
					],
					
		            ['attribute'=>Yii::t('app','Assurance'),
		               // 'header'=>Yii::t('app','Assurance'),
			        'value'=>$model->idPayrollSet->devise0->devise_symbol.' '.$model->getAssurance(),
					],
					
					['attribute'=>Yii::t('app','Loan(deduction)'),
		                //'header'=>Yii::t('app','Loan(deduction)'),
			        'value'=>$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($model->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalaryIndex_value($model->idPayrollSet->devise,$model->idPayrollSet->person_id,$model->payroll_month,getYear($model->payment_date)),$model->idPayrollSet->number_of_hour,0,$model->net_salary,$model->getFrais_numb(),$model->plus_value,$model->taxe, $model->getAssurance_numb(),$model->payroll_month) ),//$model->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalary($model->idPayrollSet->person_id),$model->idPayrollSet->number_of_hour,$model->missing_hour,$model->net_salary,$model->taxe),
					],
					
		            ['attribute'=>Yii::t('app','Net Salary'),
			           'value'=>$model->NetSalary,
			           ],
		
		            ['attribute'=>'payroll_date',
						//'header'=>Yii::t('app','Payroll Date'), 
						'value'=>Yii::$app->formatter->asDate($model->payroll_date),
			         ],
	        
	   
		            ['attribute'=>'payment_date',
						//'header'=>Yii::t('app','Payment Date'), 
						'value'=>Yii::$app->formatter->asDate($model->payment_date),
					 ],
	        
	              /*   ['attribute'=>'cash_check',
						//'header'=>Yii::t('app','Cash/Check'), 
						'value'=>$model->cash_check,
					  ],
	                 */


		            
		        ],
		    ]) ?>
		
		</div>
		
		<div class="col-lg-2 text-center">
                                                        <?php
                                                        if(ageCalculator($model->idPayrollSet->person->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($model->idPayrollSet->person->birthday).Yii::t('app',' yr old').' )'; 
                                                        }
					         	else
					         	  echo '';
					         	  
					         	                 if($model->idPayrollSet->person->image=='')
					         	                    $path = '@web/img/no_pic.png';
					         	                 else
					         	                    $path = 'documents/photo_upload/'.$model->idPayrollSet->person->image;
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            <img src="<?php echo Url::to("@web/$path"); ?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                             
                                                       
          </div>
    
</div>

<div style="clear:both"></div>
 <!-- Seconde ligne -->

<div class="row">

<ul class="nav nav-tabs">
       <?php if(((isset($_GET['id']))&&(isset($_GET['emp'])))||( (!Yii::$app->user->can('Admin'))&&(!Yii::$app->user->can('Billing') ) )){ ?>
                    <li class="active"><a data-toggle="tab" href="#transaction_list">  <?php echo Yii::t('app','Payroll history'); ?></a></li>
    
    <li ><a data-toggle="tab" href="#payroll_receipt"  aria-expanded="false">  <?php echo Yii::t('app','Payroll receipt'); ?></a></li> 
    
 <!--   <li ><a data-toggle="tab" href="#loan"  aria-expanded="false">  <?php echo Yii::t('app','Loan history'); ?></a></li> -->
    
    
                                            
  <?php  $class1="tab-pane";
        $class2="tab-pane active";
        $class3="tab-pane";
        
       }else{  ?>
        	<li class="active"><a data-toggle="tab" href="#payroll_receipt">  <?php echo Yii::t('app','Payroll receipt'); ?></a></li>
    
            <li ><a data-toggle="tab" href="#transaction_list"  aria-expanded="false">  <?php echo Yii::t('app','Payroll history'); ?></a></li>
            
          <!--   <li ><a data-toggle="tab" href="#loan"  aria-expanded="false">  <?php echo Yii::t('app','Loan history'); ?></a></li>  -->
 <?php  
        $class1="tab-pane active";
        $class2="tab-pane";
        $class3="tab-pane";

       }
        
       ?>

                           
                        </ul>
</div>

<div class="wrapper wrapper-content tab-content">
                           
        
 
   <!--  ************************** Transactions list *************************    -->
         <div id="transaction_list" class="<?php echo $class2; ?>">

    <?php
             $dataprovider_la = $modelPayroll->searchByPersonId($model->idPayrollSet->person_id,$acad);
             
 echo GridView::widget([
        'dataProvider' => $dataprovider_la,
        'id'=>'billing-grid',
        //'filterModel' => $searchModel,
        'summary'=>"",
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'payment_date',
			'label' => Yii::t('app', 'Payment Date'),
               'content'=>function($data,$url){
	                return Html::a(Yii::$app->formatter->asDate($data->payment_date), Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$data->id.'&from=pay&id_='.$data->id.'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => Yii::$app->formatter->asDate($data->payment_date),
                        ]);
               }
             ],
             
             
             ['attribute'=>'payroll_month',
			'label' => Yii::t('app', 'Payroll Month'),
               'content'=>function($data,$url){
	                return Html::a($data->getLongMonth(), Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$data->id.'&from=pay&id_='.$data->id.'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => $data->getLongMonth(),
                        ]);
               }
             ],
             
                      
              [//'attribute'=>'',
			   'label' => Yii::t('app', 'Gross salary'),
               'content'=>function($data,$url){
	                return $data->getGrossSalaryInd($data->idPayrollSet->devise,$data->idPayrollSet->person_id,$data->payroll_month,getYear($data->payment_date));
               }
             ],         
					    
					    
					    //'number_of_hour',
						/*	array('name'=>'number_of_hour',
							                'header'=>Yii::t('app','Number Of Hour'),
								        'value'=>'$data->NumberHour',
										),
							*/
					    /* array('header' =>Yii::t('app','Deduction').' ('.Yii::t('app','Missing hour').')', 
					            'value'=>'$data->getMissingHourDeduction($data->idPayrollSet->person_id,$data->missing_hour)',
					             ),
					      */
					      
						//'taxe', 
			 
			 [//'attribute'=>'',
			   'label' => Yii::t('app', 'Taxe'),
               'content'=>function($data,$url){
	                return $data->getTaxe();
               }
             ], 
				
		     
		     [//'attribute'=>'',
			   'label' => Yii::t('app', 'Assurance'),
               'content'=>function($data,$url){
	                return $data->idPayrollSet->devise0->devise_symbol.' '.$data->getAssurance();
               }
             ],
             
             
             [//'attribute'=>'',
			   'label' => Yii::t('app', 'Frais'),
               'content'=>function($data,$url){
	                return $data->getFrais();
               }
             ], 
             
             [//'attribute'=>'',
			   'label' => Yii::t('app', 'Plus Value'),
               'content'=>function($data,$url){
	                return $data->getPlusValue();
               }
             ],  		
									
			  
				[//'attribute'=>'',
			   'label' => Yii::t('app', 'Loan(deduction)'),
               'content'=>function($data,$url){
	                return $data->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($data->getLoanDeduction($data->idPayrollSet->person_id,$data->getGrossSalaryIndex_value($data->idPayrollSet->devise,$data->idPayrollSet->person_id,$data->payroll_month,getYear($data->payment_date)),$data->number_of_hour,0,$data->net_salary,$data->getFrais_numb(),$data->plus_value,$data->taxe,$data->getAssurance_numb(),$data->payroll_month ) );
               }
             ],
             
             
             
             [//'attribute'=>'',
			   'label' => Yii::t('app', 'Net Salary'),
               'content'=>function($data,$url){
	                return $data->getNetSalary();
               }
             ], 			
						 			
				],	
					    ]);
            
            
               
 ?>

   </div>
   
   
   
     <!--  ************************** payroll receipt *************************    -->
   
    <div id="payroll_receipt" class="<?php echo $class1; ?>">
       

   <?php
	
		                                                                                       //Extract school name 
								               $school_name = infoGeneralConfig('school_name');
                                                                                                //Extract school address
				   								$school_address = infoGeneralConfig('school_address');
                                                                                                //Extract  email address 
                                               $school_email_address = infoGeneralConfig('school_email_address');
                                                                                                //Extract Phone Number
                                                $school_phone_number = infoGeneralConfig('school_phone_number');
                                                
                                                 $school_acronym = infoGeneralConfig('school_acronym');
     
													$school_name_school_acronym = $school_name; 
													
													if($school_acronym!='')
													   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';


	

      $employee = $model->idPayrollSet->person->fullName;
         
         $id_payroll_set ='';
         $id_payroll_set2 ='';
     	$payment_date ='';
     	$payroll_date = '';
     	$net_salary = 0;
     	$taxe = 0;
     	$frais = 0;
     	$plus_value = 0;
     	$total_deduction = 0;
     	$number_of_hour = null;
     	$missing_hour = 0;
     	$gross_for_hour = 0;
     	
     	$assurance = 0;
     	
     	$title = $modelPers->getTitles($model->idPayrollSet->person->id,$acad);
     	$working_dep = $modelPers->getWorkingDepartment($model->idPayrollSet->person->id,$acad);
     	$gross_salary= $modelPayroll->getGrossSalaryIndex_value($model->idPayrollSet->devise,$model->idPayrollSet->person->id,$model->payroll_month,getYear($model->payment_date));
     	$currency = '';
     /*   $currency_result = Fees::model()->getCurrency($acad);
     	foreach($currency_result as $result)
     	 {
     	 	$currency = $result["devise_name"].'('.$result["devise_symbol"].')';
     	 	break;
     	 	 }
       
       $currency = $currency_name.' '.$currency_symbol;
       */  
        
        //cheche payroll la
        
     	 $modelPayroll1 = $modelPayroll->searchByMonthPersonId($model->payroll_month, $model->payroll_date, $model->idPayrollSet->person->id, $acad);
     	 $modelPayroll2 = $modelPayroll1->getModels();
     	 if($modelPayroll2!=null)
     	   {
     	   	    foreach($modelPayroll2 as $payroll_)
     	   	      {
     	   	      	     $payment_date = $payroll_->payment_date;
     	   	      	     $payroll_date = $payroll_->payroll_date;
     	   	      	     $net_salary = $payroll_->net_salary;
     	   	      	     $id_payroll_set = $payroll_->id_payroll_set;
     	   	      	     $id_payroll_set2 = $payroll_->id_payroll_set2;
     	   	      	     $assurance = $payroll_->idPayrollSet->assurance_value;
     	   	      	     $frais = $payroll_->idPayrollSet->frais;
     	   	      	     $plus_value = $payroll_->plus_value;
     	   	      	     $taxe = $payroll_->taxe;
     	   	      	     $missing_hour = $payroll_->missing_hour;
   	   	      	     
     	   	      	}
     	   	      	
     	   	 }
	     
	     if($missing_hour!=0)
	       {
	       	   $number_of_hour = $modelPS->getSimpleNumberHourValue($model->idPayrollSet->person->id);
	       	   $gross_for_hour = $gross_salary;
	       	 }
	       	 
 $currency_symbol = $model->idPayrollSet->devise0->devise_symbol;
		
?>  
        <div id="print" style=" width:100%;">
			<?php
         	
         				             
           echo ' <div id="header" style="display:none; ">
                 
                  <div >
                  <table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> <img style="width: 170px;" class="img-circle"  src="'. Yii::$app->request->baseUrl.'/img/Logo_Cana_Reel_G.gif" >
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
				 <br/></div> 
                  
                  
                  <div  style="text-align:center; "> <b>'.strtoupper(strtr(Yii::t('app','Payroll receipt'), pa_daksan() )).'</b></div>
                  </div>'; 
                  
			echo '<div  style="padding-left:30px;">
                       <div id="emp_name" style="float:left; display:none;">'.Yii::t('app','Name').': '.$employee.' </div>
			      
			         <div id="p_month" style="margin-left:62%; display:none;">'.Yii::t('app','Payroll month').': '.$model->getSelectedLongMonth($model->payroll_month).'</div> 
			               <div style="float:left;">'.Yii::t('app','Working department').': '.$working_dep.'</div> 
			      
			         <div id="p_date" style="margin-left:62%; display:none;">'.Yii::t('app','Payment date').' '.ChangeDateFormat($model->payment_date).' </div>    
			         
			             <br/><div style=""><b>'.Yii::t('app','Monthly gross salary').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).'</div><br/></div>';
			        
			         
			         //gad si yo pran taxe pou payroll sa
			         $employee_teacher = $modelPers->isEmployeeTeacher($model->idPayrollSet->person->id, $acad);	
					
					 $deduct_iri=false; 
										   
					  if(!$employee_teacher)//si moun nan pa alafwa anplwaye-pwofese 
			           { 
			           	   
			           	   echo ' <br/><div style="margin-left:1%;">
			           	   
			           	     <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>( '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).'): 
			         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
									   <tr> 
									   
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
									       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
									       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
									       
									       
									    </tr>';

                                    if($model->payroll_month==0)
                                   {
                                   	     $sql__ = 'SELECT id, taxe_value,valeur_fixe FROM taxes WHERE taxe_description like("BONIS")';
															
										  $command__ = Yii::$app->db->createCommand($sql__);
										  $result__ = $command__->queryAll(); 
																					       	   
											if($result__!=null) 
											 { foreach($result__ as $r)
											     { 
											     	  $deduction = 0;
                                                                                                  $tx_val_fixe= $r['valeur_fixe'];
											     	 $tx_val= $r['taxe_value'];
                                                                                                 $tx_val_1=$r['taxe_value'];
												
                                                                                                 if($tx_val_fixe==0)
											    	     $deduction = ( ($gross_salary * $tx_val)/100);
                                                                                                 elseif($tx_val_fixe==1)
                                                                                                 { $deduction = $tx_val;
                                                                                                 
                                                                                                   $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                 }
                                                                                                 
													 $total_deduction = $total_deduction + $deduction;
											     	     	
						 						     echo '<tr>
											                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																	       <td style="text-align:center; border:solid 1px; ">'.$tx_val_1.' </td>
																	       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																	 </tr>';
												  }
													  
											 }

                                   	
                                   	}
                                 else
                                   {

						           	  if($taxe != 0)  
						           	    { 
						           	    				           	    	
						           	    	
						           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set;
																		
										  $command__ = Yii::$app->db->createCommand($sql__);
										  $result__ = $command__->queryAll(); 
																					       	   
											if($result__!=null) 
											 { foreach($result__ as $r)
											     { 
											     	  $deduction = 0;
											     	 $tx_des='';
											     	   $tx_val='';
                                                                                                   $tx_val_1='';
                                                                                                   $tx_val_fixe= 0;
											     	   
											     	 $sql_tx_des = 'SELECT taxe_description, taxe_value,valeur_fixe FROM taxes WHERE id='.$r['id_taxe'];
																				
													  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
													  $result_tx_des = $command_tx_des->queryAll(); 
																								       	   
														foreach($result_tx_des as $tx_desc)
														     {   $tx_des= $tx_desc['taxe_description'];
														         $tx_val= $tx_desc['taxe_value'];
                                                                                                                         $tx_val_1 =$tx_desc['taxe_value'];
                                                                                                                         $tx_val_fixe= $tx_desc['valeur_fixe'];
														       }
											     	  
                                                                                                 
											     	 
											     	 if( ($tx_des!='IRI') ) //c pa iri, 
											     	      {
											     	      	   if($tx_val_fixe==0)
                                                                                                                 $deduction = ( ($gross_salary * $tx_val)/100);
                                                                                                             elseif($tx_val_fixe==1)
                                                                                                             { $deduction = $tx_val;

                                                                                                               $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                             }
                                                                                                            
														          $total_deduction = $total_deduction + $deduction;
											     	      	      echo '<tr>
												                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																		       <td style="text-align:center; border:solid 1px; ">'.$tx_val_1.' </td>
																		       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																		 </tr>';
			
														         
														         
														         
											     	      	}
											     	    elseif($tx_des=='IRI')
											     	       $deduct_iri=true; 
						 						     	      	
													  
													  }
														  
												 }
							              
						           	      } 
                                   } 
			           	          
				                  
				             if($deduct_iri)
		                           {$iri = 0; 
		                           	$iri = getIriDeduction($id_payroll_set,$id_payroll_set2,$gross_salary);
		                           	  $total_deduction = $total_deduction + $iri;
		                           	 echo '   
									  <tr> 
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','IRI ').' </td>
									       <td style="text-align:center; border:solid 1px; ">&nbsp;&nbsp; </td>
									       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($iri).'</td>
									       
									       
									    </tr>';
		                              }     
				              
				                 
				               echo '  </table> 
						           </div></div>';  
						            
			             }
			          elseif($employee_teacher)//si moun nan alafwa anplwaye-pwofese 
			           {  
			           	     $assurance =0;
			           	     $frais = 0;
			           	     
			           	     if(($id_payroll_set2=='')||($id_payroll_set2==NULL))
							    $condition = 'payroll_settings.id IN('.$id_payroll_set.')';
                             else						
							    $condition ='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.')';
			                      
								  $modelPayrollSettings = SrcPayrollSettings::find()
								                               ->orderBy(['date_created'=>SORT_DESC])
								                               ->where($condition)
								                               ->all();
			                        
			                        $as_emp=0;
			                        $as_teach=0;
			                        $payroll_set1 = '';
			                        $deduct_iri=false;
			                        
			                      	 
			                      foreach($modelPayrollSettings as $amount)
				                   {   
				                   	  $gross_salary1 =0;
				                   	  $deduction1 =0;
				                   	  $as = $amount->as_;
				                   	  $gross_for_hour = 0;
				                   	  
				                     //fosel pran yon ps.as=0 epi yon ps.as=1
				                       if(($as_emp==0)&&($as==0))
				                        { $as_emp=1;
				                          
					                     if(($amount!=null))
							               {  
							               	   $id_payroll_set1 = $amount->id;
							               	   
							               	   $gross_salary1 =$amount->amount;
							               	   
							               	   $number_of_hour = $amount->number_of_hour;
							               	   
							               	   $assurance = $assurance + $amount->assurance_value;
								               	   
								               	   $frais = $frais + $amount->frais;
							               	    
								               	   if($amount->an_hour==1)
								                     {
								                         $gross_salary1 = $gross_salary1 * $number_of_hour;
								                        }
								                        
								                     
							               	       $gross_for_hour = $gross_salary1;
							               	   
							                 }
						           
						                
			           	         echo ' <br/><div style="margin-left:1%;">
			           	               <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary1).' '.strtolower(Yii::t('app','As').' '.Yii::t('app','Employee')).'): 
			         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
									   <tr>
									   
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
									       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
									       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
									       
									       
									    </tr>';

 	                                if($model->payroll_month==0)
                                   {
                                   	     $sql__ = 'SELECT id, taxe_value,valeur_fixe FROM taxes WHERE taxe_description like("BONIS")';
															
										  $command__ = Yii::$app->db->createCommand($sql__);
										  $result__ = $command__->queryAll(); 
																					       	   
											if($result__!=null) 
											 { foreach($result__ as $r)
											     { 
											     	  $deduction = 0;
                                                                                                  $tx_val_fixe= $r['valeur_fixe'];
											     	 $tx_val= $r['taxe_value'];
                                                                                                 $tx_val_1=$r['taxe_value'];
												
                                                                                                 if($tx_val_fixe==0)
											    	     $deduction = ( ($gross_salary * $tx_val)/100);
                                                                                                 elseif($tx_val_fixe==1)
                                                                                                 { $deduction = $tx_val;
                                                                                                 
                                                                                                   $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                 }
                                                                                                 
													 $total_deduction = $total_deduction + $deduction;
											     	     	
						 						     echo '<tr>
											                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																	       <td style="text-align:center; border:solid 1px; ">'.$tx_val_1.' </td>
																	       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																	 </tr>';
												  }
													  
											 }

                                   	
                                   	}
                                 else
                                   {

						           	  if($taxe != 0)  
						           	    { 
						           	    				           	    	
						           	    	
						           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set1;
																		
										  $command__ = Yii::$app->db->createCommand($sql__);
										  $result__ = $command__->queryAll(); 
																				       	   
											if($result__!=null) 
											 { foreach($result__ as $r)
											     { 
											     	  $deduction1 = 0;
											     	 $tx_des='';
											     	   $tx_val='';
                                                                                                   $tx_val_1='';
                                                                                                   $tx_val_fixe= 0;
											     	   
											     	 $sql_tx_des = 'SELECT taxe_description, taxe_value,valeur_fixe FROM taxes WHERE id='.$r['id_taxe'];
																				
													  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
													  $result_tx_des = $command_tx_des->queryAll(); 
																								       	   
														foreach($result_tx_des as $tx_desc)
														     {   $tx_des= $tx_desc['taxe_description'];
														         $tx_val= $tx_desc['taxe_value'];
                                                                                                                         $tx_val_1 =$tx_desc['taxe_value'];
                                                                                                                         $tx_val_fixe= $tx_desc['valeur_fixe'];
														       }
											     	  
                                                                                                 
											     	 
											     	 if( ($tx_des!='IRI') ) //c pa iri, 
											     	      {
											     	      	   if($tx_val_fixe==0)
                                                                                                                 $deduction1 = ( ($gross_salary1 * $tx_val)/100);
                                                                                                             elseif($tx_val_fixe==1)
                                                                                                             { $deduction1 = $tx_val;

                                                                                                               $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                             }
                                                                                                            
														          $total_deduction = $total_deduction + $deduction1;
											     	      	      echo '<tr>
												                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																		       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																		       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($deduction1).'</td>
																		 </tr>';
			
														         
														         
														         
											     	      	}
											     	    elseif($tx_des=='IRI')
											     	       $deduct_iri=true; 
						 						     	      	
													  
													  }
														  
												 }
								              
							           	      } 
                                   }                                     
							              
							             echo '  </table> 
						                    </div>';
				                          
				                          }
				                        elseif(($as_teach==0)&&($as==1))
				                           {   $as_teach=1;
				                          
						                     if(($amount!=null))
								               {  
								               	   $id_payroll_set2 = $amount->id;
								               	   
								               	   $gross_salary1 =$amount->amount;
								               	   
								               	   $number_of_hour = $amount->number_of_hour;
								               	   
								               	   $assurance = $assurance + $amount->assurance_value;
								               	   
								               	   $frais = $frais + $amount->frais;
								               	   
								               	   if($amount->an_hour==1)
								                     {
								                         $gross_salary1 = $gross_salary1 * $number_of_hour;
								                        }
								                        
								                     
							               	            $gross_for_hour = $gross_salary1;
								               	   
								                 }
							           
						                
					           	         echo ' <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary1).' '.strtolower(Yii::t('app','As').' '.Yii::t('app','Teacher')).'): 
					         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
											   <tr>
											   
											   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
											       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
											       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
											       
											       
											    </tr>';
		
		                                    if($model->payroll_month==0)
		                                   {
		                                   	     $sql__ = 'SELECT id, taxe_value,valeur_fixe FROM taxes WHERE taxe_description like("BONIS")';
																	
												  $command__ = Yii::$app->db->createCommand($sql__);
												  $result__ = $command__->queryAll(); 
																							       	   
													if($result__!=null) 
													 { foreach($result__ as $r)
													     { 
													     	  $deduction = 0;
                                                                                                  $tx_val_fixe= $r['valeur_fixe'];
											     	 $tx_val= $r['taxe_value'];
                                                                                                 $tx_val_1=$r['taxe_value'];
												
                                                                                                 if($tx_val_fixe==0)
											    	     $deduction = ( ($gross_salary * $tx_val)/100);
                                                                                                 elseif($tx_val_fixe==1)
                                                                                                 { $deduction = $tx_val;
                                                                                                 
                                                                                                   $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                 }
                                                                                                 
                                                                                                 $total_deduction = $total_deduction + $deduction;
													     	     	
								 						     echo '<tr>
													                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																			       <td style="text-align:center; border:solid 1px; ">'.$tx_val_1.' </td>
																			       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																			 </tr>';
														  }
															  
													 }
		
		                                   	
		                                   	}
		                                 else
		                                   {

								           	  if($taxe != 0)  
								           	    { 
								          
								           	    	
								           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set2;
																				
												  $command__ = Yii::$app->db->createCommand($sql__);
												  $result__ = $command__->queryAll(); 
																							       	   
													if($result__!=null) 
													 { foreach($result__ as $r)
													     { 
													     	  $deduction1 = 0;
                                                                                                                     $tx_des='';
                                                                                                                       $tx_val='';
                                                                                                                       $tx_val_1='';
                                                                                                                       $tx_val_fixe= 0;

                                                                                                                     $sql_tx_des = 'SELECT taxe_description, taxe_value,valeur_fixe FROM taxes WHERE id='.$r['id_taxe'];

                                                                                                                              $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
                                                                                                                              $result_tx_des = $command_tx_des->queryAll(); 

                                                                                                                                    foreach($result_tx_des as $tx_desc)
                                                                                                                                         {   $tx_des= $tx_desc['taxe_description'];
                                                                                                                                             $tx_val= $tx_desc['taxe_value'];
                                                                                                                                             $tx_val_1 =$tx_desc['taxe_value'];
                                                                                                                                             $tx_val_fixe= $tx_desc['valeur_fixe'];
                                                                                                                                           }



                                                                                                                     if( ($tx_des!='IRI') ) //c pa iri, 
                                                                                                                          {
                                                                                                                               if($tx_val_fixe==0)
                                                                                                                                     $deduction1 = ( ($gross_salary1 * $tx_val)/100);
                                                                                                                                 elseif($tx_val_fixe==1)
                                                                                                                                 { $deduction1 = $tx_val;

                                                                                                                                   $tx_val_1 = $tx_val.' ('.Yii::t('app', 'Valeur fixe').')';
                                                                                                                                 }
                                                                                                            
														          $total_deduction = $total_deduction + $deduction1;
													     	      	      echo '<tr>
														                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																				       <td style="text-align:center; border:solid 1px; ">'.$tx_val_1.' </td>
																				       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($deduction1).'</td>
																				 </tr>';
					
																         
																         
																         
													     	      	}
													     	    elseif($tx_des=='IRI')
													     	       $deduct_iri=true; 
								 						     	      	
															  
															  }
																  
														 }
									              
								           	      } 
								           	      
		                                   }
								              
								           
				                     echo '  </table> 
						                    </div></div>';				                          
				                             
				                             }
			                       
			                   
			                      }//end foreach
			                       
			                      	
			                   
			                     if($deduct_iri)
		                           {
		                           	   $iri = 0; 
		                              	$iri = getIriDeduction($id_payroll_set,$id_payroll_set2,$gross_salary);;
		                           	  $total_deduction = $total_deduction + $iri;
		                         	  
		                         echo ' <div style="margin-left:12%;"><b>'.Yii::t('app','IRI').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).' '.Yii::t('app','Gross salary').'): 
					         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
											   <tr>
											   
											   <td style="text-align:; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
											       <td style="text-align:center; border:solid 1px; "> &nbsp;&nbsp;</td>
											       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
											       
											       
											    </tr>  
											  <tr> 
											   <td style="text-align:; border:solid 1px; "> '.Yii::t('app','IRI ').' </td>
											       <td style="text-align:center; border:solid 1px; ">&nbsp;&nbsp; </td>
											       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($iri).'</td>
											       
											       
											    </tr>
											     </table> 
						                 </div>';
		                              }     
				              


			                  }
			                   
		                              
						 echo '    <br/> <div style="margin-left:12%;"><b>'.Yii::t('app','Total charge').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($total_deduction).' </div>  ';
						 
						  if($model->payroll_month!=0)
                                   {
                                   	    

						 echo '    <br/> <div style="margin-left:12%;"><b>'.Yii::t('app','Assurance').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($assurance).' </div>  ';
						          
			                 //tcheke loan
			                 $loan = 0;
			               
			               $loan = $modelPayroll->getLoanDeduction($model->idPayrollSet->person->id,$gross_salary,$number_of_hour,0,$net_salary,$frais,$model->plus_value,$taxe,$assurance,$model->payroll_month );
			                   
			      echo '     <br/><div style="margin-left:12%;"><b>'.Yii::t('app','Loan(deduction)').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($loan).' </div>  ';
			          
			          
			          $total_deduction = $total_deduction + $loan + $assurance;
			          
                                   }
			          
/*if($missing_hour!=0)
	       {
	       	   $number_of_hour = PayrollSettings::model()->getSimpleNumberHourValue($model->idPayrollSet->person->id);
	       	   $gross_salary_deduction = ($gross_for_hour * $missing_hour) / $number_of_hour;
	       	   
	       	   $total_deduction = $total_deduction + $gross_salary_deduction;
	       	   
	       	    echo '     <br/><div style="margin-left:12%;">'.Yii::t('app','Deduction').' ('.Yii::t('app','Missing hour').': '.$missing_hour.') : '.$gross_salary_deduction.' </div>  ';
	       	   
	       	 }
			*/          
			      echo '    <br/><div style=""><b>'.Yii::t('app','Total deduction').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($total_deduction).' </div>  ';
			       
			      echo '    <br/><div style=""><b>'.Yii::t('app','Total frais').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($frais).' </div>  ';
			      
			      echo '    <br/><div style=""><b>'.Yii::t('app','Plus Value').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($plus_value).' </div>  ';
			       
			     
			         
	 if($model->payroll_month!=0) 
		   {	        
			      echo '    <br/><div style=""><b>'.Yii::t('app','Monthly net salary').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($net_salary).' </div>';
	       }
		else 
		  {      $numb = 0;
		            if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	  {
	             	      $numb_payroll =0;
	             	      $modelPayroll = new SrcPayroll; 
	             	                            	
	             	       $empModelPastPayroll = $modelPayroll->searchForBonis($model->idPayrollSet->person_id, $acad);
	             	                            	 
	             	       if($empModelPastPayroll!=null)
	             	          { 
	             	             $empModelPayroll = $empModelPastPayroll->getModels();
	             	               foreach($empModelPayroll as $payroll)
	             	                  {
	             	                     $numb_payroll=$numb_payroll+$payroll->gross_salary;
	             	                   }
	             	                            	   	       
	             	           }
	             	                            	  
	             	          $numb = $numb_payroll.'/12';                  	  	
	             	                            	   	
	             	      }
	             	    elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	       {
	             	         $numb_payroll =1;	             	                            	
	             	                    $numb = $numb_payroll;        	
	             	        } 
	             	                            
	             	                            
		  	      
		  	       
		  	 echo '    <br/><div style=""><b>'.Yii::t('app','BONIS').'</b> ('.$numb.'): '.$currency_symbol.' '.numberAccountingFormat($net_salary).' </div>';
		  	}    

			      
			    echo ' <br/>';
			      //echo ' <div style="text-indent: 450px; font-weight: bold; font-style: italic;"> &nbsp;&nbsp;&nbsp;'.Yii::t('app','Authorized signature').'</div>';
			/*    echo '  <br/><br/>
			       
		<div style="float:right; text-align: right; font-size: 6px; margin-bottom:-8px;margin-right:50px;">'. Yii::t('app','Powered by ').'LOGIPAM </div>';
		*/
																			
	   ?>
	
               </div>
               
                           <div id="bouton" class="row">
						        <div class="col-sm-2"  style="float:right">
						            <div class="form-group">
						        
						                <button onclick="printContent('print')" class = 'btn btn-success'><?= Yii::t('app', 'Print') ?></button>
						      						        
						            </div>
						        </div>
						        
						   </div>
			 
   
</div>


   <!--  ************************** Loan *************************    -->
 <!--     <div id="loan" class="<?php echo $class3; ?>">  -->

    <?php
  /*           
 echo GridView::widget([
        'dataProvider' => $modelLoan->searchForView($model->idPayrollSet->person_id,$acad),
        //'filterModel' => $searchModel,
        'id'=>'loan-of-money-info',
        'summary'=>'',
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            ['attribute'=>'loan_date',
			'label' => Yii::t('app', 'Loan Date'),
               'content'=>function($data,$url){
	                return Html::a(Yii::$app->formatter->asDate($data->loan_date ), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id.'&from=pay&id_='.$_GET['id'].'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => Yii::$app->formatter->asDate($data->loan_date ),
                        ]);
               }
             ],
             
             
             
             
             ['attribute'=>'amount',
			'label' => Yii::t('app', 'Amount'),
               'content'=>function($data,$url){
	                return Html::a($data->getAmount(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id.'&from=pay&id_='.$_GET['id'].'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => $data->getAmount(),
                        ]);
               }
             ],
           
           [//'attribute'=>'amount',
			'label' => Yii::t('app','Solde'),
               'content'=>function($data, $url){
	                return Html::a($data->getSolde(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id.'&from=pay&id_='.$_GET['id'].'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => $data->getSolde(),
                        ]);
               }
             ],
				                                         
                      
         ['attribute'=>'repayment_start_on',
			'label' => Yii::t('app', 'Repayment start on'),
               'content'=>function($data,$url){
	                return Html::a($data->getLongMonth(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id.'&from=pay&id_='.$_GET['id'].'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => $data->getLongMonth(),
                        ]);
               }
             ],			        
		
		
		['attribute'=>'paid',
			'label' => Yii::t('app', 'Paid'),
               'content'=>function($data,$url){
	                return Html::a($data->getLoanPaid(), Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$data->id.'&from=pay&id_='.$_GET['id'].'&month_='.$_GET['month_'].'&year_='.$_GET['year_'].'&di='.$_GET['di'], [
                                    'title' => $data->getLoanPaid(),
                        ]);
               }
             ],	
             
             		
		

        ],
    ]);             
 */           
               
 ?>

<!--   </div>  -->
   
   



</div>


<br/><br/><br/>
<script>
      function printContent(el)
      {
          document.getElementById("header").style.display = "block";
		    document.getElementById("emp_name").style.display = "block";
		    document.getElementById("p_month").style.display = "block";
		    document.getElementById("p_date").style.display = "block";
		     document.getElementById("bouton").style.display = "none";
		     
     
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
         
          
     document.getElementById("header").style.display = "none";
      document.getElementById("emp_name").style.display = "none";
    document.getElementById("p_month").style.display = "none";
    document.getElementById("p_date").style.display = "none";
    document.getElementById("bouton").style.display = "block";
     }
   </script>
