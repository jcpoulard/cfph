<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;


use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\PayrollSettingTaxes;
use app\modules\billings\models\SrcPayrollSettings;


use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Payroll */
/* @var $form yii\widgets\ActiveForm */

$acad=Yii::$app->session['currentId_academic_year'];

$payroll_setting_twice = infoGeneralConfig('payroll_setting_twice'); 

$frais=0;
?>



    <?php $form = ActiveForm::begin(); ?>

<hr/ >

 <div class="row" style="margin-bottom:-18px; margin-top:-18px;">
   <?php
         if(!isset($_GET['id']))
            {
    ?>     
        <div class="col-lg-3">
            <?php // echo $form->field($model, 'group')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()','value'=>1 ]); ?> 
        </div>
    
    <?php
            }
    ?>    
        <div class="col-lg-9"> 
             
             <?php   // echo $form->field($model, 'devise')->radio($allDevises,['separator'=>' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','options'=>['onchange' => 'submit()'  ] ])->label(false);
                     
                      foreach($allDevises as $devise=>$val)
                       { 
                       	   echo '<div class="col-lg-4">'.$form->field($model, 'devise')->radio(['label' =>$val, 'onchange' => 'submit()', 'value' => $devise, 'uncheck' => null,'style'=>'display:inline, width:20px;' ]).'</div>';
                       
                       	}
             ?>
         </div>
         
         </div>
<hr/ >


<?php 
    $Pay_set = new SrcPayrollSettings;
    
   if($group==0)
    { 
  
  ?>

<div class="row">
    <div class="col-lg-4">
       <?php     
                          		if(!isset($_GET['id']))//create
                          	      {  
                          	      	     echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$model->getSelectedLongMonthValueByPerson($person_id),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					                       
                          	      }        
					           elseif((isset($_GET['id']))&&($_GET['id']!=''))   //update
						         {
						         	if( ((isset($_GET['from1']))&&($_GET['from1']=='vfr'))&&(isset($_GET['add'])))
							      	  {
								            echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$model->getSelectedLongMonthValueByPerson($person_id),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					                       
							      	   }
							      	else
							      	  { 
									       echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$model->getPastLongMonthValueByPerson($id_payroll_set),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
								       
							           }

						         }

                          	 	
                          	
						       
                           
                  ?>

     </div>


     <div class="col-lg-4">
            <?php
            
            
            //get a default month
								  $default_month='';
								  $month ='';
								  $month_ =$model->getLongMonthValue();
								   foreach($month_ as $m)
								    { $default_month = $model->getIdLongMonth($m);
								     
								      break;
								     }
								  
								  if($payroll_month !='')
								     $month = $payroll_month;
								  else
								     $month = $default_month;   
								     
								      
                    	if((isset($_GET['id']))&&($_GET['id']!=''))
                          {  
	                           if((isset($_GET['from1']))&&($_GET['from1']=='vfr'))
	                            {	
	                            	$person_id=$_GET['emp'];
	                              }
	                            else
	                              {
	                              	if(!isset($_GET['group'])) 
	                                  $person_id=$model->idPayrollSet->person->id;
	                                  
	                              	}
                                
                          }
                               
                 	
                    	if($person_id!='')
						  {  
							  $model->person_id = [$person_id];
							  
							  //$criteria = new CDbCriteria(array('group'=>'id','alias'=>'p', 'order'=>'last_name', 'condition'=>'p.is_student=0 AND p.active IN(1, 2) AND (p.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.old_new =1 AND ps.academic_year='.$acad.') )) '));
							  
							  //echo $form->dropDownList($model, 'person_id',CHtml::listData(Persons::model()->findAll($criteria),'id','fullName'),array('onchange'=> 'submit()', 'options' => array($person_id=>array('selected'=>true)), 'disabled'=>'disabled'));
										$dataPers = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.old_new =1 AND ps.academic_year='.$acad.') )) ')->groupBy(['id'])->all();
										
										$dataProvider_updateOne = $dataPers;  
										
										echo $form->field($model, 'person_id')->widget(Select2::classname(), [
					                       'data'=>ArrayHelper::map($dataPers,'id','fullName' ),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()'],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;  
							}
						else
					  		{   
								   
												 
								// $criteria = new CDbCriteria(array('group'=>'id','alias'=>'p', 'order'=>'last_name', 'condition'=>'p.is_student=0 AND p.active IN(1, 2) AND ((p.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.old_new = 1 AND ps.academic_year='.$acad.' )) AND (p.id NOT IN(SELECT person_id FROM payroll_settings ps INNER JOIN payroll p ON(p.id_payroll_set=ps.id) WHERE ps.old_new = 1 AND ps.academic_year='.$acad.' AND p.payroll_month='.$month.' ) )) '));
								
								//$criteria = new CDbCriteria(array('group'=>'id','alias'=>'p', 'order'=>'last_name', 'condition'=>'p.is_student=0 AND p.active IN(1, 2) AND ((p.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.old_new = 1 AND ps.academic_year='.$acad.' )) AND (p.id NOT IN(SELECT person_id FROM payroll_settings ps INNER JOIN payroll p ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' AND p.payroll_month='.$month.' ) )) '));
								 
								 //echo $form->dropDownList($model, 'person_id',CHtml::listData(Persons::model()->findAll($criteria),'id','fullName'),array( 'onchange'=> 'submit()', 'prompt'=>Yii::t('app','-- Search --'), 'disabled'=>''));
										  	
										  $dataPers = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('is_student=0 AND active IN(1, 2) AND ((persons.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.old_new = 1 AND ps.academic_year='.$acad.' )) AND (persons.id NOT IN(SELECT person_id FROM payroll_settings ps INNER JOIN payroll p ON(p.id_payroll_set=ps.id) WHERE ps.academic_year='.$acad.' AND p.payroll_month='.$month.' ) )) ')->groupBy(['id'])->all();
										  
										  	$dataProvider_createOne = $dataPers;
										  	echo $form->field($model, 'person_id')->widget(Select2::classname(), [
					                       'data'=>ArrayHelper::map($dataPers,'id','fullName' ),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()'],
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;  

							 }
										    
                          
                     				                    
             
?>

      </div>
    
      <div class="col-lg-3">
            <?php echo $form->field($model, 'taxe')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;',  ]); ?> 
        </div>
            
 </div>        
        
  <?php
        
        
   $dataProvider=null;     
        
        $col= 'col-lg-4';
            $style = '';
            $br ='';
          
          
      echo $br;
     
     $gross_sal = 0;    
     
     if((($person_id!='')))
        {
  
  
  ?>      

<div class="row">
       <div class="col-lg-4">
           <?php
		                
		                  
		                    if(isset($_GET['id'])&&($_GET['id']!=''))
		                       $gross_sal = $model->getGrossSalaryIndex_value($model->devise,$person_id,$model->payroll_month,getYear($model->payment_date)  );
		                    else
		                        $gross_sal = $model->getGrossSalary($model->devise,$person_id);
		                        
		                    
		                    
		                        
		                           $devise_symbol='';
		                           if(getDeviseNameSymbolById($model->devise)!=null)
		                               $devise_symbol= getDeviseNameSymbolById($model->devise)->devise_symbol;
		                             
		                          
		                   $model->to_pay =  $devise_symbol.' '.numberAccountingFormat($gross_sal);
		                   
		            
		                    
		                    echo Yii::t('app','Salary');
		                    
		                    
		                       echo $form->field($model, 'to_pay')->textInput(['readonly'=>true,'disabled'=>'disabled'  ])->label(false); 
		                
		                   
		                ?>

 
        </div>
  <?php
          $modPerson = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC])->where('persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (ps.old_new =1 AND ps.academic_year='.$acad.') )) ')->groupBy(['id'])->all();

                    	
                    if($modPerson!=null)
                     {

         if($person_id!='')
		  {  
		                   	   	
		?>         
         
        
         <?php 
           $modelPers = new SrcPersons;
             $employee_teacher = $modelPers->isEmployeeTeacher($person_id, $acad);	
			
			
			//return nbre d'hre de l'annee en cours
			$number_hour = $Pay_set->getSimpleNumberHourValue($person_id, $acad);
				
		    
         if($number_hour!=0)
		  {  
		  			                      	   
		?>  
        <div class="col-lg-4">
            

                    
                       <?php 
                               $modelPay_set = new SrcPayrollSettings;
                       
                                  // echo $form->labelEx($modelPay_set,'number_of_hour'); 
                                     
                                    echo $form->field($modelPay_set, 'number_of_hour')->textInput(['readonly'=>true,'value'=> $number_hour  ]); 
                                 
                                     
								    
								   
		                      
		                      
						    ?>

		     
        </div>
      <?php
          	   
		                      	   
		        }
		                      
	      }	  
                     
      }


  ?>
     
        <div class="col-lg-4">
          <?php  
                   		$id_pay_set = $Pay_set->getIdPayrollSettingByPersonId($person_id);
                   		$modelPaySet = SrcPayrollSettings::findOne($id_pay_set);                          
		                   
		                  if(isset($_GET['id'])&&($_GET['id']!=''))
		                       $frais = $model->getFraisIndex_value($model->devise,$person_id,$model->payroll_month,getYear($model->payment_date)  );
		                    else
		                        $frais = $model->getFrais_($model->devise,$person_id);
		                        
		                     $model->frais_p = $frais;
		                   
		                    echo Yii::t('app','Frais');
		                    
		                    
		                       echo $form->field($model, 'frais_p')->textInput(['readonly'=>true,'disabled'=>'disabled'  ])->label(false); 
		     ?>
		                       
         </div>
         
      <div class="col-lg-4">
            
                <?= $form->field($model, 'loan_rate')->textInput()->label(Yii::t('app','Loan Rate').' ('.getDeviseNameSymbolById($model->devise)->devise_symbol.')') ?>
        </div> 
    
  </div>

<div class="row">
   
          
    <!--      <div class="col-lg-4">
            
                <?php //echo $form->field($model, 'plus_value')->textInput(); ?>
        </div> 
        
       <div class="col-lg-4">
            
                <?php //echo $form->field($model, 'cash_check')->textInput(); ?>
        </div> 
        
         
        
   </div>  
      
<div class="row">
 -->
          
        
        <div class="<?=$col ?>" >
           <?php 
		           if($model->payroll_date=='')
		             $model->payroll_date = date('Y-m-d');
		             
		           echo $form->field($model, 'payroll_date')->widget(DatePicker::classname(), [
		                    'options'=>['placeholder'=>'' ],
		                    'language'=>'fr',
		                    'pluginOptions'=>[
		                         //'autoclose'=>true, 
		                           'format'=>'yyyy-mm-dd', 
		                        ],
		
		                    ] );
              ?> 
        </div>
        <div class="<?=$col ?>" >
           
              <?php 
                        if($model->payment_date=='')
		                       $model->payment_date = date('Y-m-d');
		             
		             echo $form->field($model, 'payment_date')->widget(DatePicker::classname(), [
		                    'options'=>['placeholder'=>'' ],
		                    'language'=>'fr',
		                    'pluginOptions'=>[
		                        // 'autoclose'=>true, 
		                           'format'=>'yyyy-mm-dd', 
		                        ],
		
		                    ] ); 
              ?>
        </div>
</div>
      
<?php 
        }


  
      }
   elseif($group==1)
    {
     
?>

<div class="row">
    <div class="col-lg-4">
       <?php   
                          
                          	 if(!isset($_GET['group']))
	                           {       
								       echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$model->getSelectedLongMonthValue(),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
								       
	                            }
	                          else
	                            {	                              
	                              echo $form->field($model, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$model->getPastLongMonthValueByPayroll_group($grouppayroll),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
					                       
	                            }
                           	
                           	  
          

                  ?>

     </div>


     
            <?php
            
            
            //get a default month
								  $default_month='';
								  $month ='';
								  $month_ =$model->getLongMonthValue();
								   foreach($month_ as $m)
								    { $default_month = $model->getIdLongMonth($m);
								     
								      break;
								     }
								  
								  if($payroll_month !='')
								     $month = $payroll_month;
								  else
								     $month = $default_month;   
								     
								      
                    	if((isset($_GET['id']))&&($_GET['id']!=''))
                          {  
	                           if((isset($_GET['from1']))&&($_GET['from1']=='vfr'))
	                            {	
	                            	$person_id=$_GET['emp'];
	                              }
	                            else
	                              {
	                              	if(!isset($_GET['group'])) 
	                                  $person_id=$model->idPayrollSet->person->id;
	                                  
	                              	}
                                
                          }
                         
                      
  if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	    {
	    	?>
	<div class="col-lg-4">    	
	  <?php                     
			                         if($grouppayroll!=null)
			                           {
									      $model->group_payroll = [$grouppayroll];
						                }
						               else
						                 {
						                    $model->group_payroll = null;
						                   }
						                
						                echo $form->field($model, 'group_payroll')->widget(Select2::classname(), [
					                       'data'=>$model->getPayrollGroup(),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'), 'onchange'=> 'submit()',],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
 
            
            
             
?>

      </div>
<?php
	    }
?>    
      <div class="col-lg-3">
            <?php echo $form->field($model, 'taxe')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;',  ]); ?> 
        </div>
        

            
 </div>        
        
  <?php
        
        
   $dataProvider=null;     
        
     $col= 'col-lg-4';
             $style = '';//'margin-left:92px;'; 
             $br ='';//'<br/>';
          
      echo $br;
     
     $gross_sal = 0;
     $frais = 0;    
     
    
  ?>
  <div class="row">
               
        
        <div class="<?=$col ?>" >
           <?php 
		           if($model->payroll_date=='')
		             $model->payroll_date = date('Y-m-d');
		             
		           echo $form->field($model, 'payroll_date')->widget(DatePicker::classname(), [
		                    'options'=>['placeholder'=>'' ],
		                    'language'=>'fr',
		                    'pluginOptions'=>[
		                         //'autoclose'=>true, 
		                           'format'=>'yyyy-mm-dd', 
		                        ],
		
		                    ] );
              ?> 
        </div>
        <div class="<?=$col ?>" >
           
              <?php 
                        if($model->payment_date=='')
		                       $model->payment_date = date('Y-m-d');
		             
		             echo $form->field($model, 'payment_date')->widget(DatePicker::classname(), [
		                    'options'=>['placeholder'=>'' ],
		                    'language'=>'fr',
		                    'pluginOptions'=>[
		                        // 'autoclose'=>true, 
		                           'format'=>'yyyy-mm-dd', 
		                        ],
		
		                    ] ); 
              ?>
        </div>
</div>


<div style="margin-bottom:-40px;">      
<?php 
       

    
         $provider_to_show_button=null;
         $dataProvider_noPerson =null;
         
         $modelPS = new SrcPayrollSettings;
			$header='';
              $condition='';
               
               $id='';
               
          if(!isset($_GET['group'])) //to create
                   {    
                   	   if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                    {
                   	     //MOUN KI ALAFWA ANPLWAYE-PWOFESE PAP PARET NAN LIS PWOFESE
	    	  	              if($grouppayroll=='2')//teacher
                                 {	
                                    //$condition='persons.is_student=0 AND persons.active IN(1, 2) AND persons.id NOT IN((SELECT c.teacher FROM courses c INNER JOIN academicperiods a ON(a.id=c.academic_year) WHERE (c.academic_year='.$acad.'  OR a.year='.$acad.' ) AND c.teacher IN(SELECT persons_id FROM persons_has_titles WHERE academic_year='.$acad.' ) )) AND payroll_settings.academic_year='.$acad.' AND payroll_settings.as_=1 AND payroll_settings.id NOT IN( SELECT id_payroll_set FROM payroll pl WHERE pl.payroll_month='.$month.' )';
                                    $condition='persons.is_student=0 AND persons.active IN(1, 2) AND payroll_settings.devise='.$model->devise.' and payroll_settings.academic_year='.$acad.' AND payroll_settings.as_=1 AND payroll_settings.id NOT IN( SELECT id_payroll_set FROM payroll pl WHERE pl.payroll_month='.$month.' )';
                                    
                                    $header=Yii::t('app','Teachers name');
                                 }
                               elseif($grouppayroll=='1')//employee
                                  { $condition='persons.is_student=0 AND persons.active IN(1, 2) AND payroll_settings.devise='.$model->devise.' and payroll_settings.academic_year='.$acad.' AND payroll_settings.as_=0 AND payroll_settings.id NOT IN( SELECT id_payroll_set FROM payroll pl WHERE pl.payroll_month='.$month.' )';
                                  
                                      $header=Yii::t('app','Employees name');
                                   }
                                   
                            }
                          elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                        {
	                        	  $condition='persons.is_student=0 AND persons.active IN(1, 2) AND payroll_settings.devise='.$model->devise.' and payroll_settings.academic_year='.$acad.' AND payroll_settings.as_=0 AND payroll_settings.id NOT IN( SELECT id_payroll_set FROM payroll pl WHERE pl.payroll_month='.$month.' )';
                                  
                                      $header=Yii::t('app','Employees name');
	                          }
                                   
                         $dataProvider_noPerson =$modelPS->searchPersonsMakePayroll($condition);//searchPersonsMakePayroll
	    	  	             if($dataProvider_noPerson!=null)
	    	  	                $dataProvider_noPerson =$dataProvider_noPerson->getModels();
	    	  	       
                   }
                 else  //to update
                  {
                  	    if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                        {
	                        	      if($grouppayroll=='2')//teacher
                                {  //MOUN KI ALAFWA ANPLWAYE-PWOFESE PAP PARET NAN LIS PWOFESE
                                  //$condition='persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.academic_year='.$acad.' AND ps.as_=1  ) AND persons.id NOT IN((SELECT c.teacher FROM courses c INNER JOIN academicperiods a ON(a.id=c.academic_year) WHERE (c.academic_year='.$acad.'  OR a.year='.$acad.' ) AND c.teacher IN(SELECT persons_id FROM persons_has_titles WHERE academic_year='.$acad.' ) )) )';
                                  $condition='persons.is_student=0 AND persons.active IN(1, 2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.devise='.$model->devise.' and ps.academic_year='.$acad.' AND ps.as_=1  ) )';
                                    
                                    $header=Yii::t('app','Teachers name');
                                 }
                               elseif($grouppayroll=='1')//employee
                                  { $condition='is_student=0 AND active IN(1, 2) AND persons.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.devise='.$model->devise.' and ps.academic_year='.$acad.' AND ps.as_=0 ) ';
                                  
                                      $header=Yii::t('app','Employees name');			
                                   }
                                   
	                        }
	                     elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                        {
	                        	$condition='is_student=0 AND active IN(1, 2) AND persons.id IN(SELECT person_id FROM payroll_settings ps WHERE ps.devise='.$model->devise.' and ps.academic_year='.$acad.' AND ps.as_=0 ) ';
                                  
                                      $header=Yii::t('app','Employees name');
	                          }
	                        
                           $dataProvider_noPerson =$modelPers->searchEmployeeForPayroll($condition);
	    	  	            if($dataProvider_noPerson!=null)
	    	  	                $dataProvider_noPerson =$dataProvider_noPerson->getModels();
                  	}            
     
               
               
                if( $dataProvider_noPerson ==null) 
				  			{   
				  			
				  			if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                           $all='e';
	                        elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                        {	
				  				if($grouppayroll==1)
				  			      $all='e';
				  			    elseif($grouppayroll==2)
				  			      $all='t';
	                        }
				  			   
				  			   //to get an id  
				  			     $mod_id= $model->search_($acad);
				  			      $mod_id=$mod_id->getModels();
				  			      foreach($mod_id as $i)
				  			        {
				  			        	$id = $i->id;
				  			        	 break;
				  			        	}
				  			      
				  			 	
				  			 	}
 
               if(!isset($_GET['group'])) //to create  
                   {
	    	  	       
	    	  	       $dataProvider=$modelPS->searchPersonsMakePayroll($condition);
	    	  	     $provider_to_show_button = $dataProvider;
	  
	                 if($dataProvider!=null)	
	                   {  	     
	    	  	           echo GridView::widget([
						        'id'=>'list-payroll',
						        'dataProvider' => $dataProvider,
						       // 'filterModel' => $searchModel,
						        'summary'=>'',
						         'showFooter'=>true,
						          'showHeader' =>true,
						         'tableOptions' => ['class' => 'table table-hover'],
						    'columns' => [
						         ['class' => 'yii\grid\SerialColumn',
						           // 'header' => 'No',
						           'options' => ['width' => '50px'], 
						        ],
						        
						        
						         [
						             //'attribute'=>'',
									 'label'=>Yii::t('app','First Name'), //$header,
									 'format'=>'text', // 'raw', // 'html', //
									 'value'=>function($data){
										 return $data->person->first_name;
										 }
									  ],
						         
						           [
						             //'attribute'=>'',
									 'label'=>Yii::t('app','Last Name'),  //$header,
									 'format'=>'text', // 'raw', // 'html', //
									 'value'=>function($data){
										 return $data->person->last_name;
										 }
									  ],
						         
						           [  //'attribute'=>'',
									  'label' =>Yii::t('app','Amount'), 
									  'format'=>'text', // 'raw', // 'html', //
									  'value'=>function($data){
										 return $data->getAmount();
										 }		            
									],
											           
								/*	[  //'attribute'=>'',
									  'label' =>Yii::t('app','An Hour'), 
									  'format'=>'text', // 'raw', // 'html', //
									  'value'=>function($data){
										 return $data->getanHour();
										 }		            
									],		       
								*/			     
									[
									  //'attribute'=>'',
									  'label'=>Yii::t('app','Number Of Hour'),
									  'format'=>'raw', //'raw', // 'text', //  
									  'value'=>function($data){
										 return $data->numberHour;
										 },
									  
									 ],
									 
									 [
									  //'attribute'=>'',
									  'label'=>Yii::t('app','Frais'),
									  'format'=>'raw', //'raw', // 'text', //  
									  'value'=>function($data){
										 return $data->getFrais();
										 },
									 ],
									 
									/* [
									  //'attribute'=>'',
									  'label'=>Yii::t('app','Plus Value'),
									  'format'=>'raw', //'raw', // 'text', //  
									  'value'=>function($data){
												 	        $val ='<input name="plusValue['.$data->id.']" type=text style="width:97%;" />
									          
											   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
											               return $val;
												        },
									 ],
									 */
												  		                 
									[
									  //'attribute'=>'',
									  'label'=>Yii::t('app','Loan Rate').' ('.getDeviseNameSymbolById($model->devise)->devise_symbol.')',
									  'format'=>'raw', //'raw', // 'text', //  
									  'value'=>function($data){
												 	        $val ='<input name="loanRate['.$data->id.']" type=text style="width:97%;" />
									          
											   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
											               return $val;
												        },
									 ],
												  		                 
											            
						     /*     [
									  //'attribute'=>'',
									  'label'=>Yii::t('app','Cash/Check'),
									  'format'=>'raw', //'raw', // 'text', //  
									  'value'=>function($data){
												 	        $val ='<input name="cashCheck['.$data->id.']" type=text style="width:97%;" />
									          
											   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
											               return $val;
												        },
									 ],
							*/			  		                 
											            
						          ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
						                        'header' => Html::checkBox('selection_all', false, [
										        'class' => 'select-on-check-all',
										        'label' => Yii::t("app","All"),
										         ]), 
										        'options' => ['width' => '15px'], 
										         
						                         'checkboxOptions' => function($model, $key, $index, $widget) {
						                                                          return ['value' => $model['id'] ];
						                                                 },
						         ],  
						            
						           ], 
						           // ['class' => 'yii\grid\ActionColumn'],               
						        
						        ]);
	    	  	         
	                       }
	                                    
                     }
                  else  //to update
                    {
                         
                         $dataProvider= $model->searchPersonsForUpdatePayroll($condition,$this->payroll_month, $this->payroll_date, $acad);
                         $provider_to_show_button =$dataProvider;	
                                         
                         	     echo GridView::widget([
        'id'=>'list-payroll',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '50px'], 
        ],
        
        
         [
             //'attribute'=>'',
			 'label'=>$header,
			 'format'=>'text', // 'raw', // 'html', //
			 'value'=>function($data){
				 return $data->person->getFullName();
				 }
			  ],
         
           [  //'attribute'=>'',
			  'label' =>Yii::t('app','Amount'), 
			  'format'=>'text', // 'raw', // 'html', //
			  'value'=>function($data){
				 return $data->getAmount();
				 }		            
			],
					           
			[  //'attribute'=>'',
			  'label' =>Yii::t('app','An Hour'), 
			  'format'=>'text', // 'raw', // 'html', //
			  'value'=>function($data){
				 return $data->getanHour();
				 }		            
			],		       
					     
			[
			  //'attribute'=>'',
			  'label'=>Yii::t('app','Number Of Hour'),
			  'format'=>'raw', //'raw', // 'text', //  
			  'value'=>function($data){
						 	        $val ='<input name="numberHour['.$data->id.']"  value="'.$data->numberHour.'" type=text style="width:97%;" />
			          
					   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
			 ],
			 
			 [
			  //'attribute'=>'',
			  'label'=>Yii::t('app','Frais'),
			  'format'=>'raw', //'raw', // 'text', //  
			  'value'=>function($data){
										 return $data->getFrais();
										 },
			 ],
			 
			/* [
			  //'attribute'=>'',
			  'label'=>Yii::t('app','Plus Value'),
			  'format'=>'raw', //'raw', // 'text', //  
			  'value'=>function($data){
						 	        $val ='<input name="plusValue['.$data->id.']"  value="'.$data->plus_value.'" type=text style="width:97%;" />
			          
					   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
			 ],
			 */
			
			 [
			  //'attribute'=>'',
			  'label'=>Yii::t('app','Loan Rate'),
			  'format'=>'raw', //'raw', // 'text', //  
			  'value'=>function($data){
						 	        $val ='<input name="loanRate['.$data->id.']"  value="'.$data->loan_rate.'" type=text style="width:97%;" />
			          
					   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
			 ],
			 
		/*	 [
			  //'attribute'=>'',
			  'label'=>Yii::t('app','Cash/Check'),
			  'format'=>'raw', //'raw', // 'text', //  
			  'value'=>function($data){
						 	        $val ='<input name="cashCheck['.$data->id.']" type=text style="width:97%;" />
			          
					   <input name="id_payroll_set['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
			 ],
	*/			  		                 
					            
          ['class' => 'yii\grid\CheckBoxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '15px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['id'] ];
                                                 },
         ],  
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        ]);
        
                     
      }
	    	  	
                
               
       
    
 ?>
 </div>   


<?php
   

    	
    	}




if(  ($gross_sal!=0)||(($gross_sal==0)&&($frais!=0))||( isset($dataProvider)&&($dataProvider->getModels()!=null) )  )
  {
  	
   ?>   

    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
<?php
  }

?>
<p></p>

  
    <?php ActiveForm::end(); ?>


