<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcDevises;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcPayroll */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payrolls');


$acad=Yii::$app->session['currentId_academic_year'];

$grouppayroll=Yii::$app->session['payroll_group_payroll'];

?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create',], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>


    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>



</div>

<p></p>


<div class="row">

 <div class="col-lg-2">
       <?php $form = ActiveForm::begin(); ?>
       <?php
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/

										if($depensesItems!='')
                                            $searchModel->depensesItems = [$depensesItems];

						echo $form->field($searchModel, 'depensesItems')->widget(Select2::classname(), [

                       'data'=>loadDepensesItems(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select depenses item  --'),
                                    'onchange'=>'submit()',

                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);

								   ?>
          <?php ActiveForm::end(); ?>

    </div>
</div>




 <div class="tabs-container">
                        <ul class="nav nav-tabs">
                          <?php

     $last_dat = '';
     $display = true;

     $month_ = 0;
     $year_ =0;
     $current_month =0;
     $current_year =0;
     $i = 0;
     $class = "";
     $all='';

         $di = 1;
     if($status_ == 1)
		  $di = 1;
		elseif($status_ == 2)
		  $di = 2;





     if(!isset($_GET['month_']))
       {
       	   $sql__ = 'SELECT DISTINCT payroll_month,payroll_date,payment_date FROM payroll p INNER JOIN payroll_settings ps ON(ps.id = p.id_payroll_set) INNER JOIN academicperiods a ON(a.id = ps.academic_year) WHERE a.id='.$acad.' ORDER BY payroll_date DESC';

		  $command__ = Yii::$app->db->createCommand($sql__);
		  $result__ = $command__->queryAll();

			if($result__!=null)
			 { foreach($result__ as $r)
			     { if(($r['payroll_month']!='')&&($r['payroll_month']!=NULL))
			        { $current_month = $r['payroll_month'];
			         $current_year = getYear($r['payroll_date']);
			         $last_dat = $r['payment_date'];
			        }
			          break;

			     }
			  }

			if(!isDateInAcademicRange($last_dat,$acad))
              $display = false;


        }
     else
       {  $current_month = $_GET['month_'];
       	  $current_year = $_GET['year_'];
        }

     if(isset($_GET['di']))
	   $di= $_GET['di'];

  if($display)
    {

     $sql = 'SELECT DISTINCT p.id, payroll_month,payroll_date,payment_date FROM payroll p INNER JOIN payroll_settings ps ON(ps.id = p.id_payroll_set) INNER JOIN academicperiods a ON(a.id = ps.academic_year) WHERE a.id='.$acad.' ORDER BY payroll_date ASC, payroll_month ASC, payment_date ASC';


	  $command = Yii::$app->db->createCommand($sql);
	  $result = $command->queryAll();

		if($result!=null)
		 {
		 	$old_month = '';
		 	$new_month = '';

		     foreach($result as $s){


			     $month_=$s['payroll_month'];
			      $year_=getYear($s['payroll_date']);

				         if($month_!=$current_month)
				           {  $class = "";

				           }
				         else
				           { if($year_!=$current_year)
				               $class = "";
				             else
				                 $class = "active";

				            }

				         $new_month = getShortMonth($s['payroll_month']).' '.getYear2($s['payroll_date']);

				         if($old_month!= $new_month)
				           {
				              echo '<li class="'.$class.'" style=""><a style="padding-left:10px;padding-right:5px;" href="'.Yii::getAlias('@web').'/index.php/billings/payroll/index?month_='.$month_.'&year_='.getYear($s['payroll_date']).'&di='.$di.'&from="> <i class="">'.$new_month.'</i></a></li>';

				              $old_month = $new_month;

				           }

			      }


		 }

    }





                          ?>
                        </ul>










                                <?php

                                ?>
                        <div class="col-md-14 table-responsive" style="margin-top:-12px; margin-bottom:55px;" >
                                   <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				      <!--      <th><?= Yii::t('app','Payment Date'); ?></th>  -->
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Gross salary'); ?></th>
				            <th><?= Yii::t('app','Frais'); ?></th>
				            <th><?= Yii::t('app','Plus Value'); ?></th>
				          <!--        <th><?= Yii::t('app','Taxe'); ?></th> -->
				          <!--        <th><?= Yii::t('app','Assurance'); ?></th> -->
				            <!-- <th><?= Yii::t('app','Loan(deduction)'); ?></th>  -->
				            <th><?= Yii::t('app','Net Salary'); ?></th>
				            <th></th>

				            </tr>
				        </thead>
				        <tbody>
<?php


    $dataPayroll = $dataProvider->getModels();

          foreach($dataPayroll as $payroll)
           {
           	    $modelDevise = SrcDevises::findOne($payroll->idPayrollSet->devise);
                $devise_symbol = $modelDevise->devise_symbol;

           	   echo '  <tr >';
                                                   /*  <td >'.Html::a(Yii::$app->formatter->asDate($payroll->payment_date), Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll->id.'&month_='.$current_month.'&year_='.$year_.'&di='.$di.'&from=', [
                                    'title' => Yii::$app->formatter->asDate($payroll->payment_date),
                        ]).'</td>  */
                    echo '                                   <td >'.Html::a($payroll->idPayrollSet->person->first_name, Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll->id.'&month_='.$current_month.'&year_='.$year_.'&di='.$di.'&from=', [
                                    'title' => $payroll->idPayrollSet->person->first_name,
                        ]).' </td>
                                                    <td >'.Html::a($payroll->idPayrollSet->person->last_name, Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll->id.'&month_='.$current_month.'&year_='.$year_.'&di='.$di.'&from=', [
                                    'title' => $payroll->idPayrollSet->person->last_name,
                        ]).' </td>
                                                    <td >'.$payroll->getGrossSalaryInd($payroll->idPayrollSet->devise,$payroll->idPayrollSet->person_id,$payroll->payroll_month, getYear($payroll->payment_date) ).' </td>
                                                    <td >'.$payroll->getFrais().'</td>

                                          <td >'.$payroll->getPlusValue().'</td>';

                                               /*   <td >'.$payroll->getTaxe().'</td>

                                                 <td >'.$payroll->getAssurance().'</td>
                                                 */
                                     echo '
                      <!--    <td >'.Html::a($devise_symbol.' '.numberAccountingFormat($payroll->getLoanDeduction($payroll->idPayrollSet->person_id,$payroll->getGrossSalaryIndex_value($payroll->idPayrollSet->devise,$payroll->idPayrollSet->person_id,$payroll->payroll_month, getYear($payroll->payment_date) ),$payroll->number_of_hour,0,$payroll->net_salary,$payroll->getFrais_numb(),$payroll->plus_value,$payroll->taxe,$payroll->getAssurance_numb(),$payroll->payroll_month  ) ), Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll->id.'&month_='.$month_.'&year_='.$year_.'&di='.$di.'&from=', [
                                    'title' => $devise_symbol.' '.numberAccountingFormat($payroll->getLoanDeduction($payroll->idPayrollSet->person_id,$payroll->getGrossSalaryIndex_value($payroll->idPayrollSet->devise,$payroll->idPayrollSet->person_id,$payroll->payroll_month, getYear($payroll->payment_date) ),$payroll->number_of_hour,0,$payroll->net_salary,$payroll->getFrais_numb(),$payroll->plus_value,$payroll->taxe,$payroll->getAssurance_numb(),$payroll->payroll_month ) ),
                        ]).'</td>

                        -->

                                                     <td >'.$payroll->getNetSalary().'</td>
                                                    <td >';


                                                 /*         if(Yii::$app->user->can('billings-payroll-print'))
                                                              {
												                echo Html::a('<span class="fa fa-print"></span>', Yii::getAlias('@web').'/index.php/billings/payroll/receipt?id='.$payroll->id.'&part=balanc', [
                                    'title' => Yii::t('app', 'Print'),
                        ]);
                                                              }
                                                    */

                                                           if(Yii::$app->user->can('billings-payroll-update'))
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/payroll/update?id='.$payroll->id.'&wh=inc_bil&ri=0', [
                                    'title' => Yii::t('app', 'Update'),
                        ]);
                                                              }


                                                         if(Yii::$app->user->can('billings-payroll-delete'))
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/payroll/delete?id='.$payroll->id.'&wh=inc_bil&ri=0', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }

                                             echo ' </td>

                                                </tr>';
           	 }
?>
                             </tbody>
                    </table>
                        </div>


                               <?php

                              ?>








</div>







<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Payroll List'},
                    {extend: 'pdf', title: 'Payroll List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });





JS;
$this->registerJs($script);

?>
