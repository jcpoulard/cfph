<?php 
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Payroll */

$this->title = Yii::t('app', 'Payroll receipt');


?>

<div clas="row">


</div>
<p></p>
 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>


<div class="wrapper wrapper-content payroll-receipt">

    <?= $this->render('_receipt', [
        'model' => $model,
        'person_id'=>$person_id,
        'payroll_month'=>$payroll_month,
    ]) ?>

</div>
	


