<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;


use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\PayrollSettingTaxes;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcPayroll;


use app\modules\fi\models\SrcPersons;

?>
<?php
/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Payroll */
/* @var $form yii\widgets\ActiveForm */

  $acad=Yii::$app->session['currentId_academic_year']; 
 
           
 
 $grouppayroll=Yii::$app->session['payroll_group_payroll'];


     	$modelPers= new SrcPersons;
     	$modelPayroll= new SrcPayroll;
     	$modelPS = new SrcPayrollSettings;
     	
     	$employee=$model->idPayrollSet->person->fullName;
     	$id_payroll_set =$model->id_payroll_set;
     	$id_payroll_set2 = $model->id_payroll_set2;
     	$payment_date =$model->payment_date;
     	$payroll_date = $model->payroll_date;
     	$net_salary = $model->net_salary;
     	$frais = $model->frais;
     	$plus_value = $model->plus_value;
     	$taxe = $model->taxe;
     	$total_deduction = 0;
     	$number_of_hour = null;
     	$missing_hour = $model->missing_hour;
     	$gross_for_hour = 0;
     	//$payroll_month = '';
     	
     	$title = $modelPers->getTitles($person_id,$acad);
     	$working_dep = $modelPers->getWorkingDepartment($person_id,$acad);
     	$gross_salary= $modelPayroll->getGrossSalaryPerson_idMonthAcad($model->idPayrollSet->devise,$person_id,$payroll_month,$acad);
     	
    /* 	$currency_result = Fees::model()->getCurrency($acad);
     	foreach($currency_result as $result)
     	 {
     	 	$currency = $result["devise_name"].'('.$result["devise_symbol"].')';
     	 	break;
     	 	 }
     	*/
     	//cheche payroll la
     	  
     	  $assurance =0;
     	  $frais = 0;
     	 
     	  $modelPayroll_ = $modelPayroll->searchByMonthPersonId($payroll_month, $payroll_date, $person_id, $acad);
     	 $modelPayroll_0 = $modelPayroll_->getModels();
     	 if($modelPayroll_0!=null)
     	   {
     	   	    foreach($modelPayroll_0 as $payroll_)
     	   	      {
  	   	      	   
     	   	      	     $payment_date = $payroll_->payment_date;
     	   	      	     $payroll_date = $payroll_->payroll_date;
     	   	      	     $net_salary = $payroll_->net_salary;
     	   	      	     $id_payroll_set = $payroll_->id_payroll_set;
     	   	      	     $id_payroll_set2 = $payroll_->id_payroll_set2;
     	   	      	     $assurance = $payroll_->idPayrollSet->assurance_value;
     	   	      	     $frais = $payroll_->idPayrollSet->frais;
					     $plus_value = $payroll_->plus_value;
					     $taxe = $payroll_->taxe;
     	   	      	     $missing_hour = $payroll_->missing_hour;
     	   	      	     $payroll_month = $payroll_->payroll_month;
     	   	      	     $employee = $payroll_->idPayrollSet->person->fullName;
     	   	      	}
     	   	      	
     	   	 }
	        
	     if($missing_hour!=0)
	       {
	       	   $number_of_hour = $modelPS->getSimpleNumberHourValue($person_id);
	       	   $gross_for_hour = $gross_salary;
	       	 }
	       	 
	                                                   //Extract school name 
								               $school_name = infoGeneralConfig('school_name');
                                                                                                //Extract school address
				   								$school_address = infoGeneralConfig('school_address');
                                                                                                //Extract  email address 
                                               $school_email_address = infoGeneralConfig('school_email_address');
                                                                                                //Extract Phone Number
                                                $school_phone_number = infoGeneralConfig('school_phone_number');
                                                
                                                 $school_acronym = infoGeneralConfig('school_acronym');
     
													$school_name_school_acronym = $school_name; 
													
													if($school_acronym!='')
													   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';

     	     	   	
?> 
    <br/> <br/> <br/> <br/> 
 			<div id="payroll_receipt" style=" width:100%;">
			<?php
         	
         				             
           echo ' <div id="header" >
                       
                  <br/>            
                  <div class="info" style="text-align:center; "> <b>'.strtoupper(strtr(Yii::t('app','Payroll receipt'), pa_daksan() )).'</b></div>
                  <br/>
                  </div>'; 
                  
			echo '<div class="info" style="padding-left:30px;">
                      <div class="row" style="">
                          <div id="emp_name" style="float:left; width:68%;">'.Yii::t('app','Name').': '.$employee.'</div> <div style="float:right;">'.Yii::t('app','Payroll month').': '.$model->getSelectedLongMonth($model->payroll_month).'</div> 
			         </div>
			         <div class="row" style="display:inline">
			               <div style="float:left; width:68%;">'.Yii::t('app','Working department').': '.$working_dep.'</div> <div style="float:right;">'.Yii::t('app','Payment date').': '.ChangeDateFormat($model->payment_date).' </div>    
			         </div>
			         
			             <br/><div style=""><b>'.Yii::t('app','Monthly gross salary').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).'</div><br/>';
			        
			         
			         //gad si yo pran taxe pou payroll sa
			         $employee_teacher = $modelPers->isEmployeeTeacher($model->idPayrollSet->person->id, $acad);	
					
					 $deduct_iri=false; 
										   
					  if(!$employee_teacher)//si moun nan pa alafwa anplwaye-pwofese 
			           { 
			           	   
			           	   
			           	   echo ' <br/><div style="margin-left:1%;">
			           	   
			           	     <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>( '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).'): 
			         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
									   <tr>
									   
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
									       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
									       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
									       
									       
									    </tr>';

                                 
                                 if($model->payroll_month==0)
                                   {
                                   	     $sql__ = 'SELECT id, taxe_value FROM taxes WHERE taxe_description like("BONIS")';
															
										  $command__ = Yii::$app->db->createCommand($sql__);
										  $result__ = $command__->queryAll(); 
																					       	   
											if($result__!=null) 
											 { foreach($result__ as $r)
											     { 
											     	  $deduction = 0;
											     	 $tx_val= $r['taxe_value'];
																				
											    	 $deduction = ( ($gross_salary * $tx_val)/100);
													 $total_deduction = $total_deduction + $deduction;
											     	     	
						 						     echo '<tr>
											                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																	       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																	       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																	 </tr>';
												  }
													  
											 }

                                   	
                                   	}
                                 else
                                   {
				           	         if($taxe != 0)  
						           	    { 
					           	    				           	    	
					           	    	
					           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set;
																	
									  $command__ = Yii::$app->db->createCommand($sql__);
									  $result__ = $command__->queryAll(); 
																				       	   
										if($result__!=null) 
										 { foreach($result__ as $r)
										     { 
										     	  $deduction = 0;
										     	 $tx_des='';
										     	   $tx_val='';
										     	   
										     	 $sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];
																			
												  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
												  $result_tx_des = $command_tx_des->queryAll(); 
																							       	   
													foreach($result_tx_des as $tx_desc)
													     {   $tx_des= $tx_desc['taxe_description'];
													         $tx_val= $tx_desc['taxe_value'];
													       }
										     	 
										     	 
										     	 if( ($tx_des!='IRI') ) //c pa iri, 
										     	      {
										     	      	   $deduction = ( ($gross_salary * $tx_val)/100);
													          $total_deduction = $total_deduction + $deduction;
										     	      	      echo '<tr>
											                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																	       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																	       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																	 </tr>';
		
													         
													         
													         
										     	      	}
										     	    elseif($tx_des=='IRI')
										     	       $deduct_iri=true; 
					 						     	      	
												  
												  }
													  
											 }
						              
					           	      }  
					           	      
                                   }
			           	          
				                  
				             if($deduct_iri)
		                           {$iri = 0; 
		                           	$iri = getIriDeduction($id_payroll_set,$id_payroll_set2,$gross_salary);
		                           	  $total_deduction = $total_deduction + $iri;
		                           	 echo '   
									  <tr> 
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','IRI ').' </td>
									       <td style="text-align:center; border:solid 1px; ">&nbsp;&nbsp; </td>
									       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($iri).'</td>
									       
									       
									    </tr>';
		                              }     
				              
				                 
				               echo '  </table> 
						           </div>';  
						            
			             }
			          elseif($employee_teacher)//si moun nan alafwa anplwaye-pwofese 
			           {  
			           	   $assurance =0;
			           	   
			           	     if(($id_payroll_set2=='')||($id_payroll_set2==NULL))
							    $condition = 'payroll_settings.id IN('.$id_payroll_set.')';
                             else						
							    $condition ='payroll_settings.id IN('.$id_payroll_set.','.$id_payroll_set2.')';
			                      
								  $modelPayrollSettings = SrcPayrollSettings::find()
								                               ->orderBy(['date_created'=>SORT_DESC])
								                               ->where($condition)
								                               ->all();
			                        
			                        $as_emp=0;
			                        $as_teach=0;
			                        $payroll_set1 = '';
			                        $deduct_iri=false;
			                        
			                      	 
			                      foreach($modelPayrollSettings as $amount)
				                   {   
				                   	  $gross_salary1 =0;
				                   	  $deduction1 =0;
				                   	  $as = $amount->as_;
				                   	  $gross_for_hour = 0;
				                   	  
				                     //fosel pran yon ps.as=0 epi yon ps.as=1
				                       if(($as_emp==0)&&($as==0))
				                        { $as_emp=1;
				                          
					                     if(($amount!=null))
							               {  
							               	   $id_payroll_set1 = $amount->id;
							               	   
							               	   $gross_salary1 =$amount->amount;
							               	   
							               	   $number_of_hour = $amount->number_of_hour;
							               	   
							               	   $assurance = $assurance + $amount->assurance_value;
								               	   
								               	   $frais = $frais + $amount->frais;
							               	    
								               	   if($amount->an_hour==1)
								                     {
								                         $gross_salary1 = $gross_salary1 * $number_of_hour;
								                        }
								                        
								                     
							               	       $gross_for_hour = $gross_salary1;
							               	   
							                 }
						           
						                
			           	         echo ' <br/><div style="margin-left:1%;">
			           	               <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary1).' '.strtolower(Yii::t('app','As').' '.Yii::t('app','Employee')).'): 
			         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
									   <tr>
									   
									   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
									       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
									       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
									       
									       
									    </tr>';

 	
						           	  if($model->payroll_month==0)
	                                   {
	                                   	     $sql__ = 'SELECT id, taxe_value FROM taxes WHERE taxe_description like("BONIS")';
																
											  $command__ = Yii::$app->db->createCommand($sql__);
											  $result__ = $command__->queryAll(); 
																						       	   
												if($result__!=null) 
												 { foreach($result__ as $r)
												     { 
												     	  $deduction = 0;
												     	 $tx_val= $r['taxe_value'];
																					
												    	 $deduction = ( ($gross_salary * $tx_val)/100);
														 $total_deduction = $total_deduction + $deduction;
												     	     	
							 						     echo '<tr>
												                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																		       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																		       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																		 </tr>';
													  }
														  
												 }
	
	                                   	
	                                   	}
	                                 else
                                       {

						           	      if($taxe != 0)  
							           	    { 
							           	    				           	    	
							           	    	
							           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set1;
																			
											  $command__ = Yii::$app->db->createCommand($sql__);
											  $result__ = $command__->queryAll(); 
																					       	   
												if($result__!=null) 
												 { foreach($result__ as $r)
												     { 
												     	  $deduction1 = 0;
												     	 $tx_des='';
												     	   $tx_val='';
												     	   
												     	 $sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];
																					
														  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
														  $result_tx_des = $command_tx_des->queryAll(); 
																									       	   
															foreach($result_tx_des as $tx_desc)
															     {   $tx_des= $tx_desc['taxe_description'];
															         $tx_val= $tx_desc['taxe_value'];
															       }
												     	 
												     	 
												     	 if( ($tx_des!='IRI') ) //c pa iri,
												     	      {
												     	      	   $deduction1 = ( ($gross_salary1 * $tx_val)/100);
															          $total_deduction = $total_deduction + $deduction1;
												     	      	      echo '<tr>
													                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																			       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																			       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($deduction1).'</td>
																			 </tr>';
				
															         
															         
															         
												     	      	}
												     	    elseif($tx_des=='IRI')
												     	       $deduct_iri=true; 
							 						     	      	
														  
														  }
															  
													 }
									              
								           	      } 
                                            }
                                            
							             echo '  </table> 
						                    </div>';
				                          
				                          }
				                        elseif(($as_teach==0)&&($as==1))
				                           {   $as_teach=1;
				                          
						                     if(($amount!=null))
								               {  
								               	   $id_payroll_set2 = $amount->id;
								               	   
								               	   $gross_salary1 =$amount->amount;
								               	   
								               	   $number_of_hour = $amount->number_of_hour;
								               	   
								               	   $assurance = $assurance + $amount->assurance_value;
								               	   
								               	   $frais = $frais + $amount->frais;
								               	   
								               	   if($amount->an_hour==1)
								                     {
								                         $gross_salary1 = $gross_salary1 * $number_of_hour;
								                        }
								                        
								                     
							               	            $gross_for_hour = $gross_salary1;
								               	   
								                 }
							           
						                
					           	         echo ' <div style="margin-left:12%;"><b>'.Yii::t('app','Taxe').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary1).' '.strtolower(Yii::t('app','As').' '.Yii::t('app','Teacher')).'): 
					         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
											   <tr>
											   
											   <td style="text-align:center; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
											       <td style="text-align:center; border:solid 1px; ">'.Yii::t('app','%').' </td>
											       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
											       
											       
											    </tr>';
		
		 
								           	    if($model->payroll_month==0)
				                                   {
				                                   	     $sql__ = 'SELECT id, taxe_value FROM taxes WHERE taxe_description like("BONIS")';
																			
														  $command__ = Yii::$app->db->createCommand($sql__);
														  $result__ = $command__->queryAll(); 
																									       	   
															if($result__!=null) 
															 { foreach($result__ as $r)
															     { 
															     	  $deduction = 0;
															     	 $tx_val= $r['taxe_value'];
																								
															    	 $deduction = ( ($gross_salary * $tx_val)/100);
																	 $total_deduction = $total_deduction + $deduction;
															     	     	
										 						     echo '<tr>
															                             <td style="text-align:center; border:solid 1px; "> BONIS  </td>
																					       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																					       <td style="text-align:center; border:solid 1px; "> '.$currency_symbol.' '.numberAccountingFormat($deduction).'</td>
																					 </tr>';
																  }
																	  
															 }
				
				                                   	
				                                   	}
				                                 else
				                                   {
   
								           	      if($taxe != 0)  
									           	    { 
									          
									           	    	
									           	    	$sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set2;
																					
													  $command__ = Yii::$app->db->createCommand($sql__);
													  $result__ = $command__->queryAll(); 
																								       	   
														if($result__!=null) 
														 { foreach($result__ as $r)
														     { 
														     	  $deduction1 = 0;
														     	 $tx_des='';
														     	   $tx_val='';
														     	   
														     	 $sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];
																							
																  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
																  $result_tx_des = $command_tx_des->queryAll(); 
																											       	   
																	foreach($result_tx_des as $tx_desc)
																	     {   $tx_des= $tx_desc['taxe_description'];
																	         $tx_val= $tx_desc['taxe_value'];
																	       }
														     	 
														     	 
														     	 if( ($tx_des!='IRI') ) //c pa iri,
														     	      {
														     	      	  $deduction1 = ( ($gross_salary1 * $tx_val)/100);
																	          $total_deduction = $total_deduction + $deduction1;
														     	      	      echo '<tr>
															                             <td style="text-align:center; border:solid 1px; "> '.$tx_des.'  </td>
																					       <td style="text-align:center; border:solid 1px; ">'.$tx_val.' </td>
																					       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($deduction1).'</td>
																					 </tr>';
						
																	         
																	         
																	         
														     	      	}
														     	    elseif($tx_des=='IRI')
														     	       $deduct_iri=true; 
									 						     	      	
																  
																  }
																	  
															 }
										              
									           	      } 
									           	      
				                                   }
								              
								           
				                     echo '  </table> 
						                    </div>';				                          
				                             
				                             }
			                       
			                   
			                      }//end foreach
			                       
			                      	
			                   
			                     if($deduct_iri)
		                           {
		                           	   $iri = 0; 
		                              	$iri = getIriDeduction($id_payroll_set,$id_payroll_set2,$gross_salary);;
		                           	  $total_deduction = $total_deduction + $iri;
		                         	  
		                         echo ' <div style="margin-left:12%;"><b>'.Yii::t('app','IRI').'</b>('.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($gross_salary).' '.Yii::t('app','Gross salary').'): 
					         				<table class="" style="width:80%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
											   <tr>
											   
											   <td style="text-align:; border:solid 1px; "> '.Yii::t('app',' Taxe ').' </td>
											       <td style="text-align:center; border:solid 1px; "> &nbsp;&nbsp;</td>
											       <td style="text-align:center; border:solid 1px; "> '.Yii::t('app','Worth value').'</td>
											       
											       
											    </tr>  
											  <tr> 
											   <td style="text-align:; border:solid 1px; "> '.Yii::t('app','IRI ').' </td>
											       <td style="text-align:center; border:solid 1px; ">&nbsp;&nbsp; </td>
											       <td style="text-align:center; border:solid 1px; "> '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($iri).'</td>
											       
											       
											    </tr>
											     </table> 
						                 </div>';
		                              }     
				              


			                  }
			                   
		                              
						 echo '    <br/> <div style="margin-left:12%;"><b>'.Yii::t('app','Total charge').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($total_deduction).' </div>  ';
						 
						 echo '    <br/> <div style="margin-left:12%;"><b>'.Yii::t('app','Assurance').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($assurance).' </div>  ';
						          
			                 //tcheke loan
			                 $loan = 0;
			               //$loan = Payroll::model()->getLoanDeduction($model->idPayrollSet->person->id,$gross_salary,$number_of_hour,$missing_hour,$net_salary,$taxe);
			               $loan = $modelPayroll->getLoanDeduction($person_id,$gross_salary,$number_of_hour,0,$net_salary,$frais,$model->plus_value,$taxe,$assurance,$model->payroll_month);
			                   
			      echo '     <br/><div style="margin-left:12%;"><b>'.Yii::t('app','Loan(deduction)').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($loan).' </div>  ';
			          
			          
			          $total_deduction = $total_deduction + $loan + $assurance;
			          
/*if($missing_hour!=0)
	       {
	       	   $number_of_hour = PayrollSettings::model()->getSimpleNumberHourValue($model->idPayrollSet->person->id);
	       	   $gross_salary_deduction = ($gross_for_hour * $missing_hour) / $number_of_hour;
	       	   
	       	   $total_deduction = $total_deduction + $gross_salary_deduction;
	       	   
	       	    echo '     <br/><div style="margin-left:12%;">'.Yii::t('app','Deduction').' ('.Yii::t('app','Missing hour').': '.$missing_hour.') : '.$gross_salary_deduction.' </div>  ';
	       	   
	       	 }
			*/          
			      echo '    <br/><div style=""><b>'.Yii::t('app','Total deduction').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($total_deduction).' </div>  ';
			       
			     echo '    <br/><div style=""><b>'.Yii::t('app','Total frais').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($frais).' </div>  ';
			      
			      echo '    <br/><div style=""><b>'.Yii::t('app','Plus Value').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($plus_value).' </div>  ';
			       
			       
			      $net_salary = $net_salary + $frais + $plus_value; 
		if($payroll_month!=0) 
		   {	        
			      echo '    <br/><div style=""><b>'.Yii::t('app','Monthly net salary').'</b>: '.$model->idPayrollSet->devise0->devise_symbol.' '.numberAccountingFormat($net_salary).' </div>';
	       }
		else
		  {      $numb = 0;
		            if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	  {
	             	      $numb_payroll =0;
	             	      $modelPayroll = new SrcPayroll;
	             	                            	
	             	       $empModelPastPayroll = $modelPayroll->searchForBonis($this->person_id, $acad);
	             	                            	 
	             	       if($empModelPastPayroll!=null)
	             	          {
	             	             $empModelPayroll = $empModelPastPayroll->getModels();
	             	               foreach($empModelPayroll as $payroll) 
	             	                  {
	             	                     $numb_payroll=$numb_payroll+$payroll->gross_salary;
	             	                   }
	             	                            	   	       
	             	           }
	             	                            	  
	             	          $numb = $numb_payroll.'/12';                  	  	
	             	                            	   	
	             	      }
	             	    elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	       {
	             	         $numb_payroll =1;	             	                            	
	             	                    $numb = $numb_payroll;        	
	             	        } 
	             	                            
	             	                            
		  	      
		  	       
		  	 echo '    <br/><div style=""><b>'.Yii::t('app','BONIS').'</b> ('.$numb.'): '.$currency_symbol.' '.numberAccountingFormat($net_salary).' </div>';
		  	}    

			      
			    echo ' <br/>';
			      //echo ' <div style="text-indent: 450px; font-weight: bold; font-style: italic;"> &nbsp;&nbsp;&nbsp;'.Yii::t('app','Authorized signature').'</div>';
		/*	    echo '  <br/><br/>
			         
			       
		<div style="float:right; text-align: right; font-size: 6px; margin-bottom:-8px;margin-right:50px;">'. Yii::t('app','Powered by ').'LOGIPAM </div>';
		*/
																			
	   ?>
	
    </div>     
       

