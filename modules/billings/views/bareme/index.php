<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcBareme;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcBareme */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bareme');

?>
 
 
  <?= $this->render('//layouts/billingSettingLayout'); ?>
  
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'set_ba'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <?php  if(!$noCurrentBarem){  ?>     
    <div class="" style="width:auto;float:left; margin-left:10px;">
            <?= Html::a('<i class="fa fa-edit"></i> ', ['create','wh'=>'set_ba'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Update')]) ?> 
        
            
    </div>
    <?php }  ?>     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content bareme-index">

      <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th  style="text-align: center;"><?php echo ''; ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Minimum value'); ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Maximum value'); ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Percentage'); ?></th>
				            
				            
				            </tr>
				        </thead>
				        <tbody>     
           
  
        
        <?php 
              //get the current bareme
              //return an array  
                $modelBareme = new SrcBareme;
                
           $bareme = $modelBareme->getBaremeInUse();
        
           if($bareme!=null)
             { $i =0;
                foreach($bareme as $bar_){ ?>
        <tr class="">
            
            <td style="text-align: center;">
               <?php echo '<b> '.Yii::t('app','Range').' '.($i+1).' </b>';
                      echo '<input name="range['.$bar_['id'].']" type="hidden" value="'.$bar_['id'].'" />';
                   ?>
            </td>
            
            <td style="text-align: center;">
               <?php echo numberAccountingFormat($bar_['min_value']); ?>
            </td>
            
            <td style="text-align: center;">
                <?php echo numberAccountingFormat($bar_['max_value']); ?>
            </td>
            
            <td style="text-align: center;">
                <?php echo $bar_['percentage'].'%'; ?>
            </td>
            
            
            
        </tr>      
        <?php  $i++; }   } ?>
    
    
    
    </tbody>
                    </table>



</div>
