<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Bareme */
/* @var $form yii\widgets\ActiveForm */
?>



<?php //if(infoGeneralConfig('nb_grid_line')!=null){
//    $number_line = infoGeneralConfig('nb_grid_line');
//}else{
    $number_line = 7; 
//}   ?>

<div class="bareme-form">

    <?php $form = ActiveForm::begin(); ?>

    
         <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th  style="text-align: center;"><?php echo ''; ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Minimum value'); ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Maximum value'); ?></th>
				            <th  style="text-align: center;"><?= Yii::t('app','Percentage'); ?></th>
				            
				            
				            </tr>
				        </thead>
				        <tbody>     
        
        <?php for($i=0; $i<$number_line; $i++){ ?>
        <tr class="">
        
            <td style="text-align: center;">
               <?php echo '<b> '.Yii::t('app','Range').' '.($i+1).' </b>'; ?>
            </td>
            
            <td style="text-align: center;">
               <input style="text-align: right;" size="35" type="text" name="min<?php echo $i ?>" placeholder="<?php echo Yii::t('app','Minimum value'); ?>"/>
            </td>
            
            <td style="text-align: center;">
                <input style="text-align: right;" size="35" type="text" name="max<?php echo $i ?>" placeholder="<?php echo Yii::t('app','Maximum value'); ?>"/>
            </td>
            
            <td style="text-align: center;">
                <input style="text-align: center;" size="35" type="text" name="percent<?php echo $i ?>" placeholder="<?php echo Yii::t('app','Percentage'); ?>"/>
            </td>
            
            
            
        </tr>      
        <?php } ?>
   
  				 </tbody>
           </table>

         
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Save'), ['name' => 'create' , 'class' => 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    
    <?php ActiveForm::end(); ?>

</div>
