<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;
 
 
/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Billings */
/* @var $form yii\widgets\ActiveForm */
?>

<div >

    <?php $form = ActiveForm::begin(); ?>
    <?php


 
    $acad = Yii::$app->session['currentId_academic_year'];
    
   	 
   	  $message_fullScholarship=false;
   	  $message_scholarship=false;
   	  //check if student is a full scholarship holder
  if($model->student!='')
   	{
   	   //check if student is scholarship holder
   	   $modelScholarship = new SrcScholarshipHolder;
	   $model_scholarship = $modelScholarship->getIsScholarshipHolder($model->student,$acad); 
   	   
   	   $modelFee = SrcFees::findOne($fee_period);
   	   
   	  
   	   
														           	  	
																		$percentage_pay = 0;
																		 $internal=0;
                                                                       $partner_repondant = NULL;
                                                                         if($modelFee!=NULL)
																			{																		
																			          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																							   $notNullFee = $modelScholarship->feeNotNullByStudentId($model->student,$acad); 
																			           	      $full=1;
																						   if($notNullFee!=NULL)
																							{																								 
																							  foreach($notNullFee as $scholarship)
																			           	    	{
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   {
																			           	    	   	$full=0;
																			           	    	   	
																			           	    	     if($scholarship->fee == $modelFee->id)
																			           	    	      { 
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																		           	                    $full_scholarship = true;
																											   $message_fullScholarship = true;

																									   if(($partner_repondant==NULL))
																									    {	
																											 if(($percentage_pay==100))
																											  { 																										                                              $internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																											 else
																											    $internal=0;
																									     
																										  }
																										 
																			           	    	        }
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $full_scholarship = true;
																										          $internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								
																							}
																						   else
																						     {
																								 //fee ka NULL tou
																								  $check_partner=$modelScholarship->getScholarshipPartnerByStudentIdFee($model->student,NULL,$acad);
																								    
																								  if($check_partner!=NULL)
																								   {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }
																								   
																								       if(($percentage_pay==100))
																										 { 
																										 	 if(($partner_repondant==NULL))
																									          {  
																										         $internal=1;
																									          }
																									          $full_scholarship = true;
																								      	          $message_fullScholarship = true;
																									     }
																								       else
																								       	{		  

																									   if(($partner_repondant==NULL))
																									      {  
																									      	$internal=0;
																										  }
																										  $full_scholarship = true;
																								      	          $message_fullScholarship = true;
																								       	}
																								  
																							 }
																			           	    	 
																			           	    
																			           	 }
																			   
																			     }  	 
   	 
  
	       	
   	  }       					
 
    
    
                 $modelProgram = null;
								
								$level=0;
								$program=0;
								if($model->student=='')
								  {
								  	if(isset($_GET['stud'])&&($_GET['stud']!=''))
								  	  {  
								  	  	//return the last program id
								  	  	$program= getProgramByStudentId($_GET['stud']);
								  	  	
								  	  	$level= getLevelByStudentId($_GET['stud']);
								  	     
								  	     $model->student = $_GET['stud'];
								  	  }
								  	}
								else
								{   //return the last program id
								   $program= getProgramByStudentId($model->student);
								   
								   $level= getLevelByStudentId($model->student);
								   }
								 
								   

/* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
if(!$message_paymentAllow)
				     { 
				     	if($special_payment)
				     	   { 
				     	      Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Press the SAVE button to confirm this payment.') ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				     	   }
				     	 else
				     	   { 
				     	   	     if(($message_fullScholarship)||($message_scholarship))
								     { 
								     	 
								     	 
								     	 Yii::$app->getSession()->setFlash('success', [
														    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','Fee is totaly paid.').'<br/>'.Yii::t('app','{name} is subsidized up to {percentage}%.', array('name'=>$model->student0->getFullName(),'percentage'=>$percentage_pay) ) ),
														    'title' => Html::encode(Yii::t('app','Info') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
								       
								       
								     }
								   else
								    {
								    	
						     	       Yii::$app->getSession()->setFlash('success', [
												    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Fee is totaly paid.') ),
												    'title' => Html::encode(Yii::t('app','Info') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
												
								    }
				     	   }
				   
				       
				     }
				   
				   if($message_positiveBalance)
				     { 
				        Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} has an old fee with a positive balance.', ['name'=>$model->student0->getFullName() ]) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   
				       
				     }
				    
				     if($message_2paymentFeeDatepay)
				     { 
				     	$fee_name='';
				     		                
								$data_fee = SrcFees::find()
								             ->joinWith(['fee0'])
								             ->where(['fees.id'=>$model->fee_period])
								             ->andWhere(['program'=>$program])
								             ->andWhere(['level'=>$level])
								             ->andWhere(['academic_period'=>$acad])
								             ->all(); 
								             
	                            if($data_fee!=null)
	                              {
	                              	foreach($data_fee as $fee)
	                              	  $fee_name = $fee->fee0->fee_label;
	                              	  
	                              	}

				     	
				     	   Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} already has a record for {name_fee} on {name_datePay}. Please, update it.', ['name'=>$model->student0->getFullName(), 'name_fee'=>$fee_name, 'name_datePay'=>$model->date_pay ] ) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   
				       
				     } 				     
				  
				 
				   if(($message_fullScholarship)||($message_scholarship))
				     { 
				     	 
				     	 
				     	 Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is subsidized up to {percentage}%.', array('name'=>$model->student0->getFullName(),'percentage'=>$percentage_pay) ) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				       
				       
				     }
				     
	?>

    <div class="row">
    <div class="col-lg-4">
		
           <?php
           
     
                  
                 echo $form->field($model, 'student')->widget(Select2::classname(), [

                       'data'=>getStudentFullName(),//ArrayHelper::map(SrcPersons::find()->select(['id','last_name','first_name'])->where(['is_student'=>1])->andWhere(['in','active',[1,2] ])->all(),'id','first_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select student  --'),
                                    'onchange'=>'submit()',
                                    
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                             'tags' => true,
                    'tokenSeparators' => [',', ' '],
                             
                             
                         ],
                       ]) 
           ?>
        </div>
    
    	<div class="col-lg-4">
		    <?php
                   
                 
                       
                        	
						if(isset($_GET['id']))
							 {
							 	
							 	 
							 	if(isset($_GET['stud']))
								  { $data_fee = loadFeeName($status_,$program,$level,$acad,$model->student);
								       
                         
								       echo $form->field($model, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
								  
									}
								  else
								     { //echo $form->dropDownList($model,'fee_period',loadFeeName($this->status_,$level,$acad_sess,$this->student_id), array('onchange'=>'submit()'  ,'options' => array($this->fee_period=>array('selected'=>true)), 'disabled'=>'disabled'));
								          
								           $data_fee = loadFeeName($status_,$program,$level,$acad,$model->student);
								           
								           echo $form->field($model, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                     'disabled'=>'disabled',
						                                    //'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
	     	
								       }
								       
									   
								
							 }
							else
							  {     
							  
							     $data_fee = loadFeeName($status_,$program,$level,$acad,$model->student);
							     
							     echo $form->field($model, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee,  //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
															    
								}
								
								//['readonly' => !$model->isNewRecord]
           ?>

         </div> 
   
        <div class="col-lg-4">
           <?= $form->field($model, 'amount_to_pay')->textInput(['disabled' => true]) ?>
        </div>
    
    </div>

 
  <?php
      
      if(((($message_paymentAllow)&&(!$message_positiveBalance))&&($fee_period!=''))||($special_payment))
        {
 
 ?>

    <div class="row">
        
    	<div class="col-lg-4">
		    <?= $form->field($model, 'amount_pay')->textInput() ?>
         </div>
         
         <div class="col-lg-4">
           <?php
                 
                 echo $form->field($model, 'payment_method')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcPaymentMethod::find()->all(),'id','method_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select payment method  --'),
                                    //'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
           ?>
         </div>
    
    	<div class="col-lg-4">
		    <?php
                     if(($model->date_pay ==null)||($model->date_pay =='0000-00-00'))
                        $model->date_pay = date('Y-m-d');
                     
                echo $form->field($model, 'date_pay')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
         </div>
    </div>
    
      <div class="row">
        <div class="col-lg-12">
           <?= $form->field($model, 'comments')->textarea(['rows' => 1])  ?>
        </div>
    </div>
    

<?php 
   
   if((!$message_fullScholarship)||( $internal==0))
     {
?>                            
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

<?php
        }
        
        
        
    }
      
 
 ?>
  
    <?php ActiveForm::end(); ?>

</div>
