<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\web\JsExpression;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcBillings */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Billings');

$acad=Yii::$app->session['currentId_academic_year'];

 if($status_==1)
      $this->title = Yii::t('app', 'Billings');
 elseif($status_==0)
    $this->title = Yii::t('app', 'Other fees');
    
     $current_month ='';
     $current_year ='';
     $year_ ='';
?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?php echo Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'inc_bil','ri'=>$_GET['ri'],'part'=>'rec'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]); ?>


    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>



</div>

<p></p>



<div class="row">

 <div class="col-lg-2">
       <?php $form = ActiveForm::begin(); ?>
       <?php
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
										if($recettesItems!='')
                    $searchModel->recettesItems = [$recettesItems];

						echo $form->field($searchModel, 'recettesItems')->widget(Select2::classname(), [

                       'data'=>loadRecettesItems(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select recettes items  --'),
                                    'onchange'=>'submit()',

                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);

								   ?>
          <?php ActiveForm::end(); ?>

    </div>

</div>


 <div class="tabs-container">
   
                  
  <ul class="nav nav-tabs">
                          <?php

/*
     $condition ='fl.status='.$status_.' AND ';

     $month_ = 0;
      $year_ = 0;
     $current_month ='';
     $current_year ='';
     $i = 0;
     $class = "";

     $last_dat = '';
     $display = true;


         $ri = 0;
     if($status_ == 1)
		  $ri = 0;
		elseif($status_ == 0)
		  $ri = 1;





     if(!isset($_GET['month_']))
       {
       	   $sql__ = 'SELECT DISTINCT date_pay FROM billings b INNER JOIN fees f ON( f.id = b.fee_period ) INNER JOIN fees_label fl ON( fl.id = f.fee ) LEFT JOIN academicperiods a ON(a.id = b.academic_year) WHERE a.id='.$acad.' AND date_pay <> \'0000-00-00\' AND fl.status='.$status_.' ORDER BY date_pay DESC';

		  $command__ = Yii::$app->db->createCommand($sql__);
		  $result__ = $command__->queryAll();

			if($result__!=null)
			 { foreach($result__ as $r)
			     { if(($r['date_pay']!='')&&($r['date_pay']!=NULL))
			        { $current_month = getMonth($r['date_pay']);
			         $current_year = getYear($r['date_pay']);
			         $last_dat = $r['date_pay'];
			        }
			          break;

			     }
			  }

			if(!isDateInAcademicRange($last_dat,$acad))
              $display = false;


        }
     else
       {  $current_month = $_GET['month_'];
       	  $current_year = $_GET['year_'];
        }

     if(isset($_GET['ri']))
	   $ri= $_GET['ri'];

  if($display)
    {

     $sql = 'SELECT DISTINCT date_pay FROM billings b INNER JOIN fees f ON( f.id = b.fee_period ) INNER JOIN fees_label fl ON( fl.id = f.fee ) LEFT JOIN academicperiods a ON(a.id = b.academic_year) WHERE a.id='.$acad.'  AND date_pay <> \'0000-00-00\' AND fl.status='.$status_.'  ORDER BY date_pay ASC';


	  $command = Yii::$app->db->createCommand($sql);
	  $result = $command->queryAll();

		if($result!=null)
		 {
		 	$old_month = '';
		 	$new_month = '';

		     foreach($result as $s){


			     $month_=getMonth($s['date_pay']);
			      $year_=getYear($s['date_pay']);

				         if($month_!=$current_month)
				           {  $class = "";

				           }
				         else
				           { if($year_!=$current_year)
				               $class = "";
				             else
				                 $class = "active";

				            }

				         $new_month = getShortMonth(getMonth($s['date_pay'])).' '.getYear2($s['date_pay']);

				         if($old_month!= $new_month)
				           {
				              echo '<li class="'.$class.'" style=""><a style="padding-left:10px;padding-right:5px;" href="'.Yii::getAlias('@web').'/index.php/billings/billings/index?month_='.$month_.'&year_='.getYear($s['date_pay']).'&ri='.$ri.'&from="> <i class="">'.$new_month.'</i></a></li>';

				              $old_month = $new_month;

				           }

			      }


		 }

    }


*/


                          ?>
                        </ul>



             <div class="col-md-14 table-responsive"  style="margin-top:-12px; margin-bottom:55px;" >
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Amount To Pay'); ?></th>
				            <th><?= Yii::t('app','Amount Pay'); ?></th>
				            <th><?= Yii::t('app','Balance'); ?></th>
				            <th><?= Yii::t('app','Date Pay'); ?></th>
				            <th></th>

				            </tr>
				        </thead>
				        <tbody>
<?php

    $dataBilling = $dataProvider->getModels();


          foreach($dataBilling as $billing)
           {


           	   echo '  <tr >
                                                    <td >'.$billing->student0->id_number.' </td>
                                                    <td >'.Html::a($billing->student0->first_name, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$billing->student.'&month_='.$current_month.'&year_='.$year_.'&wh=inc_bil&ri=0', [
                                    'title' => $billing->student0->first_name,
                        ]).' </td>
                                                    <td >'.Html::a($billing->student0->last_name, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$billing->student.'&month_='.$current_month.'&year_='.$year_.'&wh=inc_bil&ri=0', [
                                    'title' => $billing->student0->last_name,
                        ]).' </td>
                                                    <td >'.Html::a($billing->feePeriod->fee0->fee_label.' ('.$billing->feePeriod->program0->short_name.')', Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$billing->student.'&month_='.$current_month.'&year_='.$year_.'&wh=inc_bil&ri=0', [
                                    'title' => $billing->feePeriod->fee0->fee_label.' ('.$billing->feePeriod->program0->short_name.')',
                        ]).' </td>
                                                    <td >'.$billing->getAmountToPay().'</td>
                                                    <td >'.$billing->getAmountPay().'</td>
                                                    <td >'.$billing->getBalance().'</td>
                                                    <td >'.Yii::$app->formatter->asDate($billing->date_pay).'</td>
                                                    <td >';



                                                      /*    if(Yii::$app->user->can('billings-billings-view'))
                                                              {
												                echo Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$billing->id.'&wh=inc_bil&ri=0', [
                                    'title' => Yii::t('app', 'View'),
                        ]);
                                                              }
                                                        */
                                                           if(Yii::$app->user->can('billings-billings-update'))
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/billings/update?id='.$billing->id.'&month_='.$current_month.'&year_='.$year_.'&wh=inc_bil&ri=0', [
                                    'title' => Yii::t('app', 'Update'),
                        ]);
                                                              }


                                                         if(Yii::$app->user->can('billings-billings-delete'))
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/billings/delete?id='.$billing->id.'&month_='.$current_month.'&year_='.$year_.'&wh=inc_bil&ri=0', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }

                                             echo ' </td>

                                                </tr>';
           	 }
?>
                             </tbody>
                    </table>


                 </div>





</div>


<?php
/*    Modal::begin(
            [
                'options' => [
       'id' => 'kartik-modal',
        'tabindex' => false, // important for Select2 to work properly

    ],


                'header'=>'<h4>'.Yii::t('app','Billings').'</h4>',
                //'id'=>'modal',
                'size'=>'modal-md',
             'toggleButton' => ['label' => 'Show Modal', 'class' => 'btn btn-lg btn-primary'],
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE,],
            ]
            );
   echo Select2::widget([
    'name' => 'state_40',
    'data' => [1 => "First", 2 => "Second", 3 => "Third", 4 => "Fourth", 5 => "Fifth"],
    'options' => ['class'=>'col-md-3','id'=>'id_select',
    'placeholder' => 'Select a state ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);      //echo '<div id="modalContent" >';




  //  echo '<div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';



    Modal::end();
 */
    ?>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Billing List'},
                    {extend: 'pdf', title: 'Billing List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });





JS;
$this->registerJs($script);

?>
