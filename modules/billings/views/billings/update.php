<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Billings */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Billings',
]) . $model->student0->getFullName().' ('.$model->feePeriod->fee0->fee_label.')';

?>
<div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="wrapper wrapper-content billings-update">

    <?= $this->render('_form', [
        'model' => $model,
         'status_'=>$status_,
                'student_id'=>$student_id,
                'id_reservation'=>$id_reservation,
                'internal'=>$internal,
                'is_pending_balance'=>$is_pending_balance,
                'fee_period'=>$fee_period,
                'message_paymentMethod'=>$message_paymentMethod,
				'message_datepay'=>$message_datepay,
                'message_positiveBalance'=>$message_positiveBalance,
                'amount_reservation'=>$amount_reservation,
                'remain_balance'=>$remain_balance,
                'special_payment'=>$special_payment,
                'message_paymentAllow'=>$message_paymentAllow,
                'message_2paymentFeeDatepay'=>$message_2paymentFeeDatepay,
    ]) ?>

</div>
