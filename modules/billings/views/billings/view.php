<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use yii\grid\GridView;
use yii\helpers\Url;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcPaymentMethod;

use app\modules\fi\models\SrcProgram; 
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Billings */


$full_name = '';
  if(isset($model->student0))
   {
      $this->title = $model->student0->getFullname();
      $full_name = $model->student0->getFullname();
      $id_student = $model->student;
    }
  else 
      {
        $modelPerson = SrcPersons::findOne($_GET['id']);
        $this->title = $modelPerson->getFullname();
        $full_name = $modelPerson->getFullname();
        $id_student = $modelPerson->id;
     }

?>
<?php
   

 $acad = Yii::$app->session['currentId_academic_year'];
 
 $modelAcad = new SrcAcademicPeriods();
 $previous_year= $modelAcad->getPreviousAcademicYear($acad); 
 
 $pending_balance=null;
 
 $summary_extended_text = '';

  $student_id = 0;
      	 $birthday = '0000-00-00';
     $image='';
    
   if(isset($model->student))
      {
      	 $student_id = $model->student0->id;
      	 $birthday = $model->student0->birthday;
      	 $image= $model->student0->image;
      	 $full_name = $model->student0->getFullName();
      }
   else
      {
      	  if(isset($_GET['id']))
      	    {  $student_id = $_GET['id'];
      	       
      	       $student=SrcPersons::findOne($student_id);
      	   
      	       $birthday = $student->birthday;
      	       
      	       $image= $student->image;
      	       $full_name = $student->getFullName();
    	 	   
      	    }
       }
     
//si $this->is_pending_balance =true met summary pou ane pase epi retire afe pending balance lan
 if($is_pending_balance==true)
  { $acad = $previous_year;
  
      $summary_extended_text = Yii::t('app','Pending balance');
      
      $command5 = Yii::$app->db->createCommand('SELECT level FROM student_level_history WHERE student_id='.$_GET['id'].' AND academic_year='.$previous_year)->queryAll();
        if(($command5!=null))
          {
            foreach($command5 as $result)
              {
                $level = $result['level'];
               
              }
          }
   }
 else {
     $level= getLevelByStudentId($student_id);
      
     $is_pending_balance =false;
    $acad = Yii::$app->session['currentId_academic_year'];
                      
}
 
$condition = '';
$ri= 1;

 if(isset($_GET['ri']))
   {         
       if($_GET['ri']==0)
         {  $ri= 0;     
        
          }
        elseif($_GET['ri']==1)
          { $ri= 1;     
         
          	 }
               
               
               
               
    }

 function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }



?>
<div class="row">
    <div class="col-lg-7" >
         <h3 > 
            <?php   if(isset($modelAdd->student))
                       echo Html::a('<div style="color:#CD3D4C; float:left;">'.$full_name.'  </div>', ['/fi/persons/moredetailsstudent', 'id' => $id_student,'ri'=>0,'is_stud'=>1], ['class' => '']);                    // part/rec/ri/'.$ri.'
                      else
                        echo $full_name;
                        
                  ?>
             
         <div class="col-xs-6" ><div class="input-group">
            <input type="text" class="form-control" id="no-stud" name="no-stud" placeholder="<?= Yii::t('app','Search a student'); ?>" >
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idstud-selected'>
                <input type='hidden' id='idstud' value="<?= $_GET['id']; ?>">
                
        </div></div>   
    
    </h3>
    </div>
    <div class="col-lg-5">
        <p>
        
    </p>
    </div>
</div> 

<br/>


   

<div style="clear:both"></div>

<!-- La ligne superiure  -->


<div class="row">
   <div class="col-lg-3">
       <div style="background-color:#EDF1F6; color:#F0652E; border:1px solid #DDDDDD; padding:5px; ">
       
           <span  style="font-weight:bold; "><?php echo Yii::t('app','Summary').' '.$summary_extended_text; ?></span> 
        </div>
  
                
	    <table class="table table-striped table-bordered detail-view table-responsive">
	        
			        <tr class="odd"><th><?php echo Yii::t('app','Exonerate Fee(s) '); ?></th></tr>
			        <tr class="odd">
			            <td>
			                    
			         <?php // 
			                     
			                if($recettesItems ==  0)     
								{ $status_ = 1;	
								  $ri=0; 
								}				              
							elseif($recettesItems ==  1)     
								{   $status_ = 0;
								    $ri=1;
								}
			
			 //gad si elev la gen balans ane pase ki poko peye
		/*   
			$modelPendingBal= SrcPendingBalance::find()
			                  ->select(['id', 'balance'])
			                  ->where(['student'=>$student_id])
			                  ->andWhere(['is_paid'=>0])
			                  ->andWhere(['academic_year'=>$previous_year])
				              ->all();
			//si gen pending, ajoutel nan lis apeye a			
			if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
			 {
			   foreach($modelPendingBal as $bal)
				 {	
				 	$pending_balance = $bal->balance;
				 }
			 }				          
			*/         
			              $condition = ['fees_label.status'  => $status_];
			              $condition_receipt = '';
			              
			              $full_paid_fees_array = [];
								$fees_paid_array = [];
								$paid_fees_array = [];
						  $arraya_size=0;
								
								 
								 //tcheke si elev sa se yon bousye
								 $modelBous = new SrcScholarshipHolder();
								 $is_scholarshipHolder = $modelBous->getIsScholarshipHolder($student_id,$acad);    
																	           	  
							     if($is_scholarshipHolder == 1) //se yon bousye
								   {  //tcheke tout fee ki peye net yo    // pou ajoute l/yo sou $fees_paid 
								       $modelBill = new SrcBillings();
								       
                                                                       if($is_pending_balance==false)
                                                                           $full_paid_fee = $modelBill->searchFullPaidFeeByStudentId($student_id, $status_, $acad);
                                                                       elseif($is_pending_balance==true)
								           $full_paid_fee = $modelBill->searchPastFullPaidFeeByStudentId($student_id, $level, $status_, $acad);
                                                                           
								       if($full_paid_fee!=null)
                                                                        {  $i=0; 
                                                                              foreach($full_paid_fee as $full_paid)
                                                                                {
                                                                                
                                                                                    $full_paid_fees_array[0][$i] = Yii::t('app',$full_paid->fee_label); 
                                                                                    $full_paid_fees_array[1][$i] = $full_paid->id_fee; 

                                                                                    //ajoute full paid fee
                                                                                    $paid_fees_array[] = $full_paid->id_fee;
                                                                                    $arraya_size++;
                                                                                    $i++;    	 
                                                                                 }
                                                                             }
                                                                          else  //al cheche l nan tab bousye yo
                                                                            {
                                                                              
                                                                              }
								       
								    }
								    
							        if($full_paid_fees_array!=null)
							                     {  
								                   if(sizeof($full_paid_fees_array)==0)
								                     {
								                     //	echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][0].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][0].'" id="'.$full_paid_fees_array[1][0].'" checked="checked" value="'.$full_paid_fees_array[1][0].'" disabled="disabled" ></div></div></div>';
								                     
								                     echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][0].' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
								                    
								                    }
								                 else
								                    {
								                   
								                   
								                   for($k=0; $k<$arraya_size; $k++)
								                      {   
						 	                               
					 //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][$k].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][$k].'" id="'.$full_paid_fees_array[1][$k].'" checked="checked" value="'.$full_paid_fees_array[1][$k].'" disabled="disabled" ></div></div></div>';
					 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][$k].' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
						                              										        						
														}
														
								                    }
														
											       }		       				 
				                     ?>
			        
			            </td> 
			        </tr>
			        
			                <tr class="odd"><th><?php echo Yii::t('app','Paid Fee(s) '); ?></th></tr>
			        <tr class="odd">
			            <td>
			                    
			         <?php // 
			                     
			                if($recettesItems ==  0)     
								{ $status_ = 1;	
								  $ri=0;
								}				              
							elseif($recettesItems ==  1)     
								{   $status_ = 0;
								    $ri=1;
								}
								          
			         
			              $condition =['fees_label.status' => $status_];
			              $condition_receipt = '';
			               
			              
								                       				   
								$fees_desc = array();
							     			                      
						    //return id_fee, fee_name
						       $modelBill = new SrcBillings();
								 $fees_paid = $modelBill->searchPaidFeesByStudentId($student_id, $status_, $acad);
			                 	
			                 		      if($fees_paid!=null)
							                     {  
								                   foreach($fees_paid as $id_fees_paid)
								                      {   
					 //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$id_fees_paid->fee_label).'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$id_fees_paid->id_fee.'" id="'.$id_fees_paid->id_fee.'" checked="checked" value="'.$id_fees_paid->id_fee.'" disabled="disabled" ></div></div></div>';
					 
					 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$id_fees_paid->fee_label).' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
						                              										        						
														}
														
											       }
											     
										       				 
				                     ?>
			        
			            </td>
			        </tr>
			        
			        <tr class="odd"><th><?php echo Yii::t('app','Pending Fee(s) '); ?></th></tr>
			        <tr class="odd">
			        <td>
			
			           <?php // 
			               
			               if($status_==1)
			                 {                        				   
								 $fees_desc = array();
								 
								 
			                                      
			                       if($fees_paid!=null)
			                         { foreach($fees_paid as $fee)
			                              $paid_fees_array[] = $fee->id_fee;
			                           }
			                    	
			                    					  
								 
								 $program= getProgramByStudentId($student_id);
								 
								 
								 
								   //return id_fee, fee_name
								$modelBill = new SrcBillings();
								$fees_pending = $modelBill->searchPendingFeesByStudentId($program, $level, $acad);
			                 		
			                 		      if($fees_pending!=null)
							                     {  
								                   foreach($fees_pending as $fees_pen)
								                      {  
						 	                              if (!in_array($fees_pen->id_fee, $paid_fees_array))  
								                          {  
								                          	//if($pending_balance==null)
								                          	// {
								                          	  if($fees_pen->fee_label !="Pending balance")
								                          	    { 
								                          	   
					//echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($fees_pen->amount).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen->id_fee.'" id="'.$fees_pen->id_fee.'"  value="'.$fees_pen->id_fee.'" disabled="disabled" ></div></div></div>';
					echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($fees_pen->amount).') <i class="fa fa-square-o" aria-hidden="true"></i></div></div></div>';	
								                          	    }
								                          	    
								                          /*	 }
								                            else
								                              {
								                              	    if($fees_pen->fee_label !="Pending balance")
								                          	            $pending_balance = $fees_pen->amount;
								                          	            
								                          	         //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($pending_balance).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen->id_fee.'" id="'.$fees_pen->id_fee.'"  value="'.$fees_pen->id_fee.'" disabled="disabled" ></div></div></div>';
								                          	         
								                          	         echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($pending_balance).') <i class="fa fa-square-o" aria-hidden="true"></i></div></div></div>';
								                              	} 
								                          	*/ 		                              
								                           }
		 						                             
														}
														
											       }
									
			                    }
			                 	       				 
				                     ?>        
			        
			        </td>
			        </tr> 
			        

        
         
               </table>

       </div>
   
     <div class="col-lg-7 billings-view" style="border:1px solid #DDDDDD; min-height:280px;" >
   
	<?php
 //*********************************************************************************************       
        
            //pou update 
                if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                  $title = Yii::t('app', 'Update Billings'); 
                else
                   $title = Yii::t('app', 'Create Billings'); 
            
            if( ($is_pending_balance==true) &&($modelAdd->exempt_fees==1) )
                 $title = Yii::t('app', 'Record exemption'); 
            else
                $modelAdd->exempt_fees=0;
           ?>
               <div class="row">
    
                    <div class="col-lg-11">
                         <h3><?= $title; ?></h3>
                    </div>


                </div>


                  <div class="wrapper wrapper-content billings-create" style=" margin-bottom: 0px;">
                      <div >

                              <?php $form = ActiveForm::begin(); ?>
                      <?php


 
    
 if(($modelAdd->exempt_fees==0) ) 
  {
   	  $message_fullScholarship=false;
   	  $message_scholarship=false;
   	  //check if student is a full scholarship holder
  if($modelAdd->student!='')
   	{
   	   //check if student is scholarship holder
   	   $modelScholarship = new SrcScholarshipHolder;
	   $model_scholarship = $modelScholarship->getIsScholarshipHolder($modelAdd->student,$acad); 
   	   
   	 
           
           
           
           
           
           $modelFee = SrcFees::findOne($fee_period);
   	   
   	  
   	   
														           	  	
                    $percentage_pay = 0;
                     $internal=0;
$partner_repondant = NULL;
if($modelFee!=NULL)
                            {																		
                                      if($model_scholarship == 1) //se yon bousye
                                       {  //tcheke tout fee ki pa null nan bous la
                                                               $notNullFee = $modelScholarship->feeNotNullByStudentId($modelAdd->student,$acad); 
                                                  $full=1;
                                                       if($notNullFee!=NULL)
                                                            {																								 
                                                              foreach($notNullFee as $scholarship)
                                                    {
                                                      if(isset($scholarship->fee) )
                                                       {
                                                            $full=0;

                                                         if($scholarship->fee == $modelFee->id)
                                                          { 
                                                            $percentage_pay = $scholarship->percentage_pay;
                                                        $full_scholarship = true;
                                                                                               $message_fullScholarship = true;

                                                                               if(($partner_repondant==NULL))
                                                                                {	
                                                                                             if(($percentage_pay==100))
                                                                                              { 																										                                              $internal=1;
                                                                                                     $partner_repondant = $scholarship->partner;
                                                                                              }
                                                                                             else
                                                                                                $internal=0;

                                                                                      }

                                                            }
                                                        }
                                                      else
                                                        {
                                                            $percentage_pay = $scholarship->percentage_pay;
                                                            $partner_repondant = $scholarship->partner;
                                                          }

                                                     }

                                                 if($full==1)
                                                   {
                                                     if(($percentage_pay==100))
                                                                                              { $full_scholarship = true;
                                                                                              $internal=1;
                                                                                                     $partner_repondant = $scholarship->partner;
                                                                                              }

                                                     }


                                                            }
                                                       else
                                                         {
                                                                     //fee ka NULL tou
                                                                      $check_partner=$modelScholarship->getScholarshipPartnerByStudentIdFee($modelAdd->student,NULL,$acad);

                                                                      if($check_partner!=NULL)
                                                                       {  
                                                                         foreach($check_partner as $cp)
                                                                                       {   $partner_repondant = $cp->partner;
                                                                                          $percentage_pay = $cp->percentage_pay;
                                                                                             break;
                                                                                       }
                                                                       }

                                                                           if(($percentage_pay==100))
                                                                                     { 
                                                                                             if(($partner_repondant==NULL))
                                                                                      {  
                                                                                             $internal=1;
                                                                                      }
                                                                                      $full_scholarship = true;
                                                                                      $message_fullScholarship = true;
                                                                                 }
                                                                           else
                                                                            {		  

                                                                               if(($partner_repondant==NULL))
                                                                                  {  
                                                                                    $internal=0;
                                                                                      }
                                                                                      $full_scholarship = true;
                                                                                      $message_fullScholarship = true;
                                                                            }

                                                             }


                                             }

                                 }  	 
   	 
  
	       	
   	  }       					
 
    
    
                 $modelProgram = null;
								
								//$level=0;
								$program=0;
								if($modelAdd->student=='')
								  {
								  	if(isset($_GET['id'])&&($_GET['id']!=''))
								  	  {  
								  	  	//return the last program id
								  	  	$program= getProgramByStudentId($_GET['id']);
								  	  	
								  	  	//$level= getLevelByStudentId($_GET['id']);
								  	     
								  	     $modelAdd->student = $_GET['id'];
								  	  }
								  	}
								else
								{   //return the last program id
								   $program= getProgramByStudentId($modelAdd->student);
								   
								   //$level= getLevelByStudentId($modelAdd->student);
								   }
								 
								   

/* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
if(!$message_paymentAllow)
				     { 
				     	if($special_payment)
				     	   { 
				     	      Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Press the SAVE button to confirm this payment.') ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				     	   }
				     	 else
				     	   { 
				     	   	     if(($message_fullScholarship)||($message_scholarship))
								     { 
								     	 
								     	 
								     	 Yii::$app->getSession()->setFlash('success', [
														    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','Fee is totaly paid.').'<br/>'.Yii::t('app','{name} is subsidized up to {percentage}%.', array('name'=>$modelAdd->student0->getFullName(),'percentage'=>$percentage_pay) ) ),
														    'title' => Html::encode(Yii::t('app','Info') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
								       
								       
								     }
								   else
								    {
								    	
						     	       Yii::$app->getSession()->setFlash('success', [
												    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Fee is totaly paid.') ),
												    'title' => Html::encode(Yii::t('app','Info') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
												
								    }
				     	   }
				   
				       
				     }
				   
				   if($message_positiveBalance)
				     { 
				        Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} has an old fee with a positive balance.', ['name'=>$modelAdd->student0->getFullName() ]) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   
				       
				     }
				    
				     if($message_2paymentFeeDatepay)
				     { 
				     	$fee_name='';
				     		                
								$data_fee = SrcFees::find()
								             ->joinWith(['fee0'])
								             ->where(['fees.id'=>$modelAdd->fee_period])
								             ->andWhere(['program'=>$program])
								             ->andWhere(['level'=>$level])
								             ->andWhere(['academic_period'=>$acad])
								             ->all(); 
								             
	                            if($data_fee!=null)
	                              {
	                              	foreach($data_fee as $fee)
	                              	  $fee_name = $fee->fee0->fee_label;
	                              	  
	                              	}

				     	
				     	   Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} already has a record for {name_fee} on {name_datePay}. Please, update it.', ['name'=>$modelAdd->student0->getFullName(), 'name_fee'=>$fee_name, 'name_datePay'=>$modelAdd->date_pay ] ) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   
				       
				     } 				     
				  
				 
				   if(($message_fullScholarship)||($message_scholarship))
				     { 
				     	 
				     	 
				     	 Yii::$app->getSession()->setFlash('success', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is subsidized up to {percentage}%.', array('name'=>$modelAdd->student0->getFullName(),'percentage'=>$percentage_pay) ) ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				       
				       
				     }
				     
	?>

    <div class="row">
    	<div class="col-lg-4">
		    <?php
                   
                 
                       
                        	        if(isset($_GET['bil']))
					 {
                                            $modelAdd->fee_period = $fee_period;
                                         }
                                        
						if(isset($_GET['id']))
							 {
							 	
							 	 
							 	if(isset($_GET['id']))
								  { $data_fee = loadFeeName($status_,$program,$level,$acad,$modelAdd->student);
								       
                         					       echo $form->field($modelAdd, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
								  
									}
								  else
								     { //echo $form->dropDownList($modelAdd,'fee_period',loadFeeName($this->status_,$level,$acad_sess,$this->student_id), array('onchange'=>'submit()'  ,'options' => array($this->fee_period=>array('selected'=>true)), 'disabled'=>'disabled'));
								          
								           $data_fee = loadFeeName($status_,$program,$level,$acad,$modelAdd->student);
								           
								           echo $form->field($modelAdd, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee, //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                     'disabled'=>'disabled',
						                                    //'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
	     	
								       }
								       
									   
								
							 }
							else
							  {     
							  
							     $data_fee = loadFeeName($status_,$program,$level,$acad,$modelAdd->student);
							     
							     echo $form->field($modelAdd, 'fee_period')->widget(Select2::classname(), [

						                       'data'=>$data_fee,  //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
						                                    'onchange'=>'submit()',
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
															    
								}
                                                                
                                              
								
								//['readonly' => !$modelAdd->isNewRecord]
           ?>

         </div> 
   
        <div class="col-lg-4">
           <?= $form->field($modelAdd, 'amount_to_pay')->textInput(['disabled' => true]) ?>
        </div>
        
           <div class="col-lg-4">
		    <?php 
                    if(((($message_paymentAllow)&&(!$message_positiveBalance))&&($fee_period!=''))||($special_payment))
                         echo $form->field($modelAdd, 'amount_pay')->textInput();
                     else 
                        echo $form->field($modelAdd, 'amount_pay')->textInput(['disabled' => true]);
                     ?>
         </div>
        
    
    </div>

 
  <?php
      
      if(((($message_paymentAllow)&&(!$message_positiveBalance))&&($fee_period!=''))||($special_payment))
        {
 
 ?>

    <div class="row">
        
    	<div class="col-lg-4">
           <?php
                 
                 echo $form->field($modelAdd, 'payment_method')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcPaymentMethod::find()->all(),'id','method_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select payment method  --'),
                                    //'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
           ?>
         </div>
    
    	<div class="col-lg-4">
		    <?php
                     if(($modelAdd->date_pay ==null)||($modelAdd->date_pay =='0000-00-00'))
                        $modelAdd->date_pay = date('Y-m-d');
                     
                echo $form->field($modelAdd, 'date_pay')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
         </div>
        
        <div class="col-lg-4">
           <?= $form->field($modelAdd, 'num_chekdepo')->textInput()  ?>
        </div>
    </div>
    
      
    

<?php 
   
   if((!$message_fullScholarship)||( $internal==0))
     {
?>                            
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['name' => 'create', 'class' => 'btn btn-success' ]) ?>
        
       
               
          <button type="button" class="btn btn-warning" id="cancel" title="<?= Yii::t('app', 'Cancel') ?>" >
                            <?= Yii::t('app', 'Cancel'); ?>
                    </button>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

<?php
        }
        
    }
    
    }  //########################## DEBU EXEMPTION #################################### 
elseif(($modelAdd->exempt_fees==1) )
{
    
                
          $program =getProgramByStudentId($_GET['id']);
          $dataProvider=$modelFee->searchPastFeesToExempt($_GET['id'],$program,$level,$previous_year); 
                
    ?>
    <div class="row">
        <div class="col-lg-12" >
        
        <div  style="margin-top:-30px; margin-left:10px;">


   <?= GridView::widget([
        'id'=>'exempt',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         //'showFooter'=>true,
         // 'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '50px'], 
        ],
        
        
         [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Fees"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
			 	$acad=Yii::$app->session['currentId_academic_year']; 
                                $modelAcad_ = new SrcAcademicPeriods();
                                $previous_year= $modelAcad_->getPreviousAcademicYear($acad);
                   	$year=$previous_year;
                $student = $_GET['id'];//Yii::$app->session['exempt_student'];
				 return Yii::t("app",$data->fee_label)." (".$data->devise_symbol." ".numberAccountingFormat($data->getFeeAmount($student,$year,$data->amount)).')';
				 }
			  ],
			 
			 [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Deadline"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return Yii::$app->formatter->asDate( $data->date_limit_payment );
				 }
			  ],
			 
			 
			 [
             //'attribute'=>'student',
			 'label'=>Yii::t("app","Amount Already Paid"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
			 	$student = $_GET['id'];//Yii::$app->session['exempt_student'];
			 	    return $data->getAmountPayOnFee($student,$data->id);
			      }
				 
			  ],
			  
			  
			  
			  ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '15px'], 
				         
                         'checkboxOptions' => function($modelExempt, $key, $index, $widget) {
                                                          return ['value' => $modelExempt['id'] ];
                                                 },
         ],
         
              ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>

    
 </div>       

        
       </div>
 </div>        

    
  
 <div class="row">
        <div class="col-lg-12">
           <?= $form->field($modelExempt, 'comment')->textarea(['rows' => 1])  ?>
        </div>
 </div>        


<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Create') , ['name' => 'exempt' , 'class' => 'btn btn-success']) ?>
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
  <?php                        
}  //########################## FEN EXEMPTION #################################### 
    
    if( ($is_pending_balance==true) )
        {
  ?>
        
             <div class="col-lg-4">
            <?php echo $form->field($modelAdd, 'exempt_fees')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()','value'=>1 ]); ?> 
             </div>
     <?php
     
        }   
 
 ?>
  
    <?php ActiveForm::end(); ?>

</div>
                 </div>
  <?php   
  //*****************************************************************************************************
  ?>
		
		</div>
		
		<div class="col-lg-2 text-center">
                                                         <?php 
                                                        $program='';
                                                       if(isset($modelAdd->student0->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $modelAdd->student0->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($modelAdd->student0->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($modelAdd->student0->birthday).Yii::t('app',' yr old').' )'; 
                                                        }
					         	else
					         	  echo '';
					         	  
					         	                 $path = 'documents/photo_upload/'.$modelAdd->student0->image;
                                                         ?>
                                                            </strong>
                                                        </span> 
                                                       
                                                            <?php if($path=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/$path")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>
                                                            
                                                          <span>
                                                            <strong>
                                                              <?php 
                                                                            echo Yii::t('app', 'Level').' : ';
                                                                            if(isset($modelAdd->student0->studentLevel->level) )
                                                                              echo $modelAdd->student0->studentLevel->level;
                                                             ?>
					         	   </strong>
                                                        </span>
                            
                                                       
          </div> 
    
</div>

<div style="clear:both"></div>
 <!-- Seconde ligne -->


<?php

  // if(isset($model->student))
   //   {
?>
<div class="row">

<ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#transaction_list" aria-expanded="true"><?= Yii::t('app', 'Transactions list') ?> </a></li>
                            <?php
                                     if($is_pending_balance==true)
                                     {
                              ?>
                              <li class=""><a data-toggle="tab" href="#lastyear_transaction_list" aria-expanded="true"><?= Yii::t('app', 'Last year transactions list') ?> </a></li>
                            
                            <?php           
                                     }
                            ?>
                          <!--  <li class=""><a data-toggle="tab" href="#payment_receipt" aria-expanded="false"><?= Yii::t('app', 'Payment receipt') ?> </a></li>  -->
                           
                        </ul>


<div class="wrapper wrapper-content tab-content">
                           
         <!--  ************************** Transactions list *************************    -->
 <?php
     if(Yii::$app->user->can('billings-billings-print'))
         {
 ?>       <div class="row">
						        <div class="col-sm-1" style="float:right; margin-right: 20px; margin-top: -20px;">
						           <div class="form-group">
						        
						                <button onclick="printContent('print')" class = 'btn btn-success'><?= Yii::t('app', 'Print') ?></button>
						      						        
						            </div>
						        </div>
						        
						    </div>
   <?php
        }
   ?>
         
         <div id="transaction_list" class="tab-pane active">

    <?php
              $current_acad = Yii::$app->session['currentId_academic_year'];
              $searchModel = new SrcBillings();
              $dataProvider = $searchModel->searchForView($_GET['id'],$current_acad);
 echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'billing-grid',
        //'filterModel' => $searchModel,
        'summary'=>"",
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'date_pay:date',
            
            
        /*    ['attribute'=>'id',
			'label' => Yii::t('app', 'Transaction ID'),
               'content'=>function($data,$url){
	                return $data->id;
               }
             ],
         * 
         */

            //'fee_period',
            ['attribute'=>'fee_period',
			'label' => Yii::t('app', 'Fee'),
               'content'=>function($data,$url){
	                return $data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->academicPeriod->name_period.')';
               }
             ],

            //'amount_to_pay',
            ['attribute'=>'amount_to_pay',
			'label' => Yii::t('app', 'Amount To Pay'),
               'content'=>function($data,$url){
	                return $data->getAmountToPay();
               }
             ],
             
            //'amount_pay',
            ['attribute'=>'amount_pay',
			'label' => Yii::t('app', 'Amount Pay'),
               'content'=>function($data,$url){
	                return $data->getAmountPay();
               }
             ],
             
            //'balance',
             ['attribute'=>'balance',
			'label' => Yii::t('app', 'Balance'),
               'content'=>function($data,$url){
	                return $data->getBalance();
               }
             ], 
                     
            
            ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
			   'label' => Yii::t('app', 'Payment M.'),
               'content'=>function($data,$url){
	                if(isset($data->paymentMethod))
	                  return $data->paymentMethod->method_name;
               }
             ],

           
             ['attribute'=>'num_chekdepo',
			'label' => Yii::t('app', '# check/deposit'),
               'content'=>function($data,$url){
	                return $data->num_chekdepo;
               }
             ],
                     
             ['attribute'=>Yii::t('app', 'Created By'),//'payment_method',
			   'label' => Yii::t('app', 'Created By'),
               'content'=>function($data,$url){
	                if(isset($data->updated_by)&&($data->updated_by!=NULL))
	                  return $data->updated_by;
                        else
                            return $data->created_by;
               }
             ],
        
         
           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url,$model) {
                 $url = 'view?id='.$_GET["id"].'&bil='.$model->id.'&month_=&year_=&wh=inc_bil&ri=0';   
                 return Html::a(
                        '<span class="fa fa-edit"></span>', 
                        $url);
                },
                'link' => function ($url,$model,$key) {
                    return Html::a('Action', $url);
                },
	        ],
        ],          
                     
                     
        ],
    ]); 
      
 ?>

   </div>
         
         
   <?php
         if($is_pending_balance==true)
         {
     ?>     
             <div id="lastyear_transaction_list" class="tab-pane">

    <?php    
              $searchModel = new SrcBillings();
              $dataProvider = $searchModel->searchLastYearForView($_GET['id'],$acad);
 echo GridView::widget([
        'dataProvider' => $dataProvider,
        'id'=>'billing-grid',
        //'filterModel' => $searchModel,
        'summary'=>"",
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'date_pay:date',
            
            
        /*    ['attribute'=>'id',
			'label' => Yii::t('app', 'Transaction ID'),
               'content'=>function($data,$url){
	                return $data->id;
               }
             ],
         * 
         */

            //'fee_period',
            ['attribute'=>'fee_period',
			'label' => Yii::t('app', 'Fee'),
               'content'=>function($data,$url){
	                return $data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->academicPeriod->name_period.')';
               }
             ],

            //'amount_to_pay',
            ['attribute'=>'amount_to_pay',
			'label' => Yii::t('app', 'Amount To Pay'),
               'content'=>function($data,$url){
	                return $data->getAmountToPay();
               }
             ],
             
            //'amount_pay',
            ['attribute'=>'amount_pay',
			'label' => Yii::t('app', 'Amount Pay'),
               'content'=>function($data,$url){
	                return $data->getAmountPay();
               }
             ],
             
            //'balance',
             ['attribute'=>'balance',
			'label' => Yii::t('app', 'Balance'),
               'content'=>function($data,$url){
	                return $data->getBalance();
               }
             ], 
                     
            
            ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
			   'label' => Yii::t('app', 'Payment M.'),
               'content'=>function($data,$url){
	                if(isset($data->paymentMethod))
	                  return $data->paymentMethod->method_name;
               }
             ],

           
             ['attribute'=>'num_chekdepo',
			'label' => Yii::t('app', '# check/deposit'),
               'content'=>function($data,$url){
	                return $data->num_chekdepo;
               }
             ],
                     
             ['attribute'=>Yii::t('app', 'Created By'),//'payment_method',
			   'label' => Yii::t('app', 'Created By'),
               'content'=>function($data,$url){
	                if(isset($data->updated_by)&&($data->updated_by!=NULL))
	                  return $data->updated_by;
                        else
                            return $data->created_by;
               }
             ],
        
         
           [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url,$model) {
                 $url = 'view?id='.$_GET["id"].'&bil='.$model->id.'&month_=&year_=&wh=inc_bil&ri=0';   
                 return Html::a(
                        '<span class="fa fa-edit"></span>', 
                        $url);
                },
                'link' => function ($url,$model,$key) {
                    return Html::a('Action', $url);
                },
	        ],
        ],          
                     
                     
        ],
    ]); 
      
 ?>

   </div>
         
   <?php         
         }
   
   ?>      
   
   
   <!--  ************************** payment receipt *************************    -->
   
    <div id="payment_receipt" class="tab-pane">
       
       
   <?php
	
		                                                 //Extract school name 
								               $school_name = infoGeneralConfig('school_name');
                                                                                                //Extract school address
				   								$school_address = infoGeneralConfig('school_address');
                                                                                                //Extract  email address 
                                               $school_email_address = infoGeneralConfig('school_email_address');
                                                                                                //Extract Phone Number
                                                $school_phone_number = infoGeneralConfig('school_phone_number');
                                                
                                                 $school_acronym = infoGeneralConfig('school_acronym');
     
													$school_name_school_acronym = $school_name; 
													
													if($school_acronym!='')
													   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';




	//get program for this student
								$program = getProgramByStudentId($_GET['id']);
								
  
$programName='';
         if($program!='')
           { $modelProgram = SrcProgram::findOne($program);
           	 $programName = $modelProgram->label;
           	 
           }

$acadModel = SrcAcademicperiods::findOne($acad);         
       //  $student=$this->getStudent($student_id);
         
		 

?>  

			<div id="print" >
			 
			<?php	echo ' <div id="header" style="display:none; ">
                 
                  <div >
                  <table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> <img style="width: 170px;" class="img-circle"  src="'. Yii::$app->request->baseUrl.'/img/Logo_Cana_Reel_G.gif" >
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
				 <br/></div>  
                  
                  <br/>  
                  
                  <br/> 
                  
                  <div  style="text-align:center; "> <b>'.strtoupper(strtr(Yii::t('app','Payment receipt'), pa_daksan() )).'</b></div>';
                  
               ?> 
            				
			
				<table   >
				  <tbody>
				    <tr>
				     <td > <strong><?=  Yii::t('app', 'First name & Last name') ?>: </strong> <?= $full_name  ?><br/>
				           <strong><?=  Yii::t('app', 'Program') ?>: </strong> <?= $programName ?><br/>
				           <strong><?=  Yii::t('app', 'Year') ?>: </strong> <?=  $acadModel->name_period ?> 
				     </td>
				     
				  </tr>
					  
				  </tbody>  
			   </table>
			   
			   <br/>
			<?php   
			    echo '</div>'; 
              ?>

				<?php			   
							   $searchModel = new SrcBillings();
				              $dataProvider = $searchModel->searchForView($_GET['id'],$current_acad);
				 echo GridView::widget([
				        'dataProvider' => $dataProvider,
				        'id'=>'billing-grid',
				        //'filterModel' => $searchModel,
				        'summary'=>"",
				        'columns' => [
				         //   ['class' => 'yii\grid\SerialColumn'],
				
				            //'id',
				            [//'attribute'=>'id',
							'label' => Yii::t('app', 'Date Pay'),
				               'content'=>function($data,$url){
					                return Yii::$app->formatter->asDate($data->date_pay );
				               }
				             ],
				            
				            
				            [//'attribute'=>'id',
							'label' => Yii::t('app', 'Transaction ID'),
				               'content'=>function($data,$url){
					                return $data->id;
				               }
				             ],
                                          
				
				            //'fee_period',
				            [//'attribute'=>'fee_period',
							'label' => Yii::t('app', 'Fee'),
				               'content'=>function($data,$url){
					                return $data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->academicPeriod->name_period.')'.' ['.$data->feePeriod->program0->label.']';
				               }
				             ],
				
				            //'amount_to_pay',
				            [//'attribute'=>'amount_to_pay',
							'label' => Yii::t('app', 'Amount To Pay'),
				               'content'=>function($data,$url){
					                return $data->getAmountToPay();
				               }
				             ],
				             
				            //'amount_pay',
				            [//'attribute'=>'amount_pay',
							'label' => Yii::t('app', 'Amount Pay'),
				               'content'=>function($data,$url){
					                return $data->getAmountPay();
				               }
				             ],
				             
				       /*      ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
							   'label' => Yii::t('app', 'Payment M.'),
				               'content'=>function($data,$url){
					                return Html::a($data->paymentMethod->method_name, Yii::getAlias('@web').'/billings/billings/view?id='.$data->id.'&wh=inc_bil', [
				                                    'title' => $data->paymentMethod->method_name,
				                        ]);
				               }
				             ],
				          */
				             //'balance',
				             [//'attribute'=>'balance',
							'label' => Yii::t('app', 'Balance'),
				               'content'=>function($data,$url){
					                return $data->getBalance();
				               }
				             ],
                                             
                                             [//'attribute'=>'num_chekdepo',
							'label' => Yii::t('app', '# check/deposit'),
				               'content'=>function($data,$url){
					                return $data->num_chekdepo;
				               }
				             ],
                                                     
                                              
				           
				                       
				        ],
				    ]); 
				?>		       
		       
			
		
			 
			      

			                
			
			</div>
			
			      
      

   
  
  
                          <!--   <div class="row">
						        <div class="col-sm-2" style="float:right">
						           <div class="form-group">
						        
						                <button onclick="printContent('print')" class = 'btn btn-success'><?= Yii::t('app', 'Print') ?></button>
						      						        
						            </div>
						        </div>
						        
						    </div>
                          -->

              </div>

</div>

<?php

 $script1 = <<< JS
    $(document).ready(function(){
         
         
          $('#no-stud').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_students; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idstud-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/billings/billings/view?id="+id_stud+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }
           
            });
         
         $('#cancel').click(function(){
            var id_stud =  $('#idstud').val();
                window.location.href = "/cfph/web/index.php/billings/billings/view?id="+id_stud+"&month_=&year_=&wh=inc_bil&ri=0";
            
            });
         
         

        });
         
     $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/billings/billings/view?id="+id_stud+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }

        }

    });

JS;

 $this->registerJs($script1);       
?>
    
<script>
      function printContent(el)
      {
          document.getElementById("header").style.display = "block";
		  //  document.getElementById("emp_name").style.display = "block";
		 //   document.getElementById("p_month").style.display = "block";
		  //  document.getElementById("p_date").style.display = "block";
		     
     
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
         
          
     document.getElementById("header").style.display = "none";
     // document.getElementById("emp_name").style.display = "none";
    //document.getElementById("p_month").style.display = "none";
    //document.getElementById("p_date").style.display = "none";
     }
   </script>





