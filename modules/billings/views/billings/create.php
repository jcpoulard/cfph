<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Billings */

$this->title = Yii::t('app', 'Create Billings');

?>
<div class="row">
    
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index','ri'=>$_GET['ri'],'from'=>''], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>
    
    
<div class="wrapper wrapper-content billings-create">

        <?= $this->render('_form', [
        'model' => $model,
               'status_'=>$status_,
                'student_id'=>$student_id,
                'id_reservation'=>$id_reservation,
                'internal'=>$internal,
                'is_pending_balance'=>$is_pending_balance,
                'fee_period'=>$fee_period,
                'message_positiveBalance'=>$message_positiveBalance,
                'amount_reservation'=>$amount_reservation,
                'remain_balance'=>$remain_balance,
                'special_payment'=>$special_payment,
                'message_paymentAllow'=>$message_paymentAllow,
                'message_2paymentFeeDatepay'=>$message_2paymentFeeDatepay,
    ]) ?>

</div>
