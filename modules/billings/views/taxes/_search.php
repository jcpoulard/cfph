<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\SrcTaxes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taxes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'taxe_description') ?>

    <?= $form->field($model, 'employeur_employe') ?>

    <?= $form->field($model, 'taxe_value') ?>

    <?= $form->field($model, 'particulier') ?>

    <?php // echo $form->field($model, 'academic_year') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
