<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Taxes */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Taxes',
]) . $model->taxe_description;

?>
<?= $this->render('//layouts/billingSettingLayout'); ?>
<p></p>

<div class="row">
    <div class="col-lg-8">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-4">
            <p>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index','wh'=>'set_ta'], ['class' => 'btn btn-info btn-sm']) ?>
       <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create','wh'=>'set_ta'], ['class' => 'btn btn-primary btn-sm']) ?>
   
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
            </p>
        </div>
        
    </div>


<div class="wrapper wrapper-content taxes-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
