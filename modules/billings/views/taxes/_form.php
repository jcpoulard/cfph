<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Taxes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taxes-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
         
    ?>

    <div class="row">
        <div class="col-lg-4">
             <?= $form->field($model, 'taxe_description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
		    
		
		<?= $form->field($model, 'employeur_employe')->widget(Select2::classname(), [
                       'data'=>loadEmployerEmployee(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
		  </div>
        <div class="col-lg-2">
		      <?= $form->field($model, 'taxe_value')->textInput() ?>
           </div>
        
        <div class="col-lg-2" style="margin-top: 25px;">
		       <?php echo $form->field($model, 'valeur_fixe')->checkbox();
             ?>
           </div>
    </div> 
    
    <div class="row">
         <div class="col-lg-4">
             <?php echo $form->field($model, 'particulier')->checkbox();
             ?>
         </div> 
     </div>
    
  <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>   
   
    
    

    <?php ActiveForm::end(); ?>

</div>
