<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcBillingsServicesItems */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Billings Services Items');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billings-services-items-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Billings Services Items'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'billings_services_id',
            'first_name',
            'last_name',
            'phone',
            'promotion',
            // 'option_program',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
