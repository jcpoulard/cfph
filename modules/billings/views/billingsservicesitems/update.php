<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\BillingsServicesItems */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Billings Services Items',
]) . $model->billings_services_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Billings Services Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->billings_services_id, 'url' => ['view', 'id' => $model->billings_services_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="billings-services-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
