<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\BillingsServicesItems */

$this->title = Yii::t('app', 'Item detail info');//$model->billings_services_id;
?>

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php   
                       echo $this->title;
                        
                  ?>
    
    </h3>
    </div>
    <div class="col-lg-5">
        <p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['/billings/billingsservices/update', 'id' => $model->billings_services_id,], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['/billings/billingsservices/create',], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['/billings/billingsservices/index?ri=5&part=rec', ], ['class' => 'btn btn-info btn-sm']) ?>
        
    </p>
    </div>
</div> 




   

<div style="clear:both"></div>


<div class="billings-services-items-view">

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'billings_services_id',
            'first_name',
            'last_name',
            'phone',
            'promotion',
            'option_program',
        ],
    ]) ?>

</div>
