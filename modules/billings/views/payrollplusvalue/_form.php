<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;

//use app\modules\billings\models\SrcFees;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollPlusvalue */
/* @var $form yii\widgets\ActiveForm */

$acad=Yii::$app->session['currentId_academic_year']; 

?>

<div class="payroll-plusvalue-form">

    <?php $form = ActiveForm::begin(); ?>


 <div class="row">      
      <div class="col-lg-4">
            <?php
		          echo $form->field($model, 'month')->widget(Select2::classname(), [

						                       'data'=>getSelectedLongMonthValueForOne($model->person_id),  
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select repayment start  --'),
						                                  'id'=>'month',
                                                           'name'=>'month',  
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ]) ;
						                       
		         
		     ?>
          </div>             
          <div class="col-lg-4">            
           
           <?= $form->field($model, 'amount')->textInput(['name'=>'amount']); ?>
          </div>                
</div>

 
 <?php    	
    
?>	    
   
   	 
<?php 
    
 ?>	    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


	    
<br/><br/><br/>

    <?php ActiveForm::end(); ?>

</div>
