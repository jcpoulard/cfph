<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;

//use app\modules\billings\models\SrcFees;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollPlusvalue */
/* @var $form yii\widgets\ActiveForm */

$acad=Yii::$app->session['currentId_academic_year']; 

?>

<div class="payroll-plusvalue-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="row">
       <div class="col-lg-4">
           <?= $form->field($model, 'person_id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcPersons::find()->where('is_student=0 and active in(1,2) AND (persons.id IN(SELECT person_id FROM payroll_settings ps WHERE (academic_year='.$acad.') )) ')->all(),'id','fullName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select a person  --'), 'onChange'=>'submit()'],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
	    </div> 

<?php
        if($model->person_id!='')
          {
?>	    
	    <div class="col-lg-4">
    	
		    <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                   
		     ?>
		     
		     
		    
	 	</div>
   </div>
    		
	
 <div class="row">      
    <table class="table table-striped table-bordered table-hover tablo">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Month') ?></th>
                <th><?= Yii::t('app', 'Amount') ?></th>
                
             </tr>
                <tbody>
                    <?php
                         $num_month = sizeof(getSelectedLongMonthValueForOne($model->person_id));
                         
                        
                        for($i=1;$i<=$num_month;$i++){
                          ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td>
                            <?php
		          echo $form->field($model, 'month')->widget(Select2::classname(), [

						                       'data'=>getSelectedLongMonthValueForOne($model->person_id),  
						                       'size' => Select2::MEDIUM,
						                       'theme' => Select2::THEME_CLASSIC,
						                       'language'=>'fr',
						                       'options'=>['placeholder'=>Yii::t('app', ' --  select repayment start  --'),
						                                  'id'=>'month'.$i,
                                                           'name'=>'month'.$i,  
						                                    
						                             ],
						                       'pluginOptions'=>[
						                             'allowclear'=>true,
						                         ],
						                       ])->label(false) ;
						                       
		         
		     ?>
                        </td>
                       <td>
                            <?= $form->field($model, 'amount')->textInput(['name'=>'amount'.$i])->label(false); ?>
                        </td>
                        
                        
                                           </tr>
                    <?php 
                        }
                    ?>
                </tbody>
        </thead>
    </table>
    
</div>

 
 <?php    	
    
?>	    
   
   	 
<?php 
    
 ?>	    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

 <?php
   
     }
 
  
?>	    
<br/><br/><br/>

    <?php ActiveForm::end(); ?>

</div>
