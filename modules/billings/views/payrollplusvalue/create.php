<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\PayrollPlusvalue */

$this->title = Yii::t('app', 'Create Payroll Plusvalue');
?>
<div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

 
 <div class="wrapper wrapper-content payroll-plusvalue-create">

  
      <?= $this->render('_form_list', [
        'model' => $model,
    ]) ?>

</div>
