<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\helpers\Url;

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcPersonsHasTitles;
use app\modules\fi\models\SrcTitles;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\SrcPayrollPlusvalue;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Payroll */



$modelPers = new SrcPersons;
$modelPayroll = new SrcPayroll;
$modelPS= new SrcPayrollSettings;
$modelLoan = new SrcLoanOfMoney;

 
 $acad = Yii::$app->session['currentId_academic_year'];
 
 
      	 $birthday = '0000-00-00';
     $image='';
    
   
      	 $person_id = $model->person->id;
      	 $birthday = $model->person->birthday;
      	 $image= $model->person->image;
      	 $full_name = $model->person->getFullName();
     



    
    

?>

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php  
                    echo Yii::t('app','Payroll plus value view : ').' ';
             if(isset($model->person))
                       echo Html::a($model->person->getFullName(), [ '/fi/persons/moredetailsemployee', 'id' => $model->person_id,'di'=>0,'is_stud'=>0,'from'=>'emp'], ['class' => '']);                    
                      else
                        echo $full_name;
                        
                  ?>
    
    </h3>
    </div>

<?php
        
 
 ?>   

    <div class="col-lg-5">
        <p>
       
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'wh'=>'mod'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id, 'wh'=>'mod'], ['class' => 'btn btn-info btn-sm']) ?>
        
    </p>
    </div>

<?php
          
?>

</div> 




   

<div style="clear:both"></div>

<!-- La ligne superiure  -->
<div class="row">
   
     <div class="col-lg-7 payroll-view">
   
	 
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Month'); ?></th>
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $searchModel = new SrcPayrollPlusvalue();
    $acad=Yii::$app->session['currentId_academic_year'];
     $dataProvider = $searchModel->searchForOneByAcad($model->person_id,$acad);
     $dataPlusvalue = $dataProvider->getModels();
    
          foreach($dataPlusvalue as $plusvalue)
           {
           	   echo '  <tr >
                                                  <td >'.$plusvalue->getLongMonth().' </td>   
                                                  <td >'.$plusvalue->devise0->devise_symbol.' '.$plusvalue->amount.' </td>
                                                   
                                                    
                                                    <td >'; 
                                                
                                                         
                                              
                                                              
                                               
                                                         if(Yii::$app->user->can('billings-payrollplusvalue-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/payrollplusvalue/update?id='.$plusvalue->id.'&', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                             }
                                                             
                                               
                                                              
                                                         if(Yii::$app->user->can('billings-payrollplusvalue-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/payrollplusvalue/delete?id='.$plusvalue->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                
	    
		
		</div>
		
		<div class="col-lg-2 text-center"> 
                    
                                             <br/><br/>
                                                        <?php  
                                                        $hire_date=' ? ';
                                                       if(isset($model->person->employeeInfos[0]->hire_date)&&($model->person->employeeInfos[0]->hire_date!='') )  
                                                            $hire_date = Yii::$app->formatter->asDate( $model->person->employeeInfos[0]->hire_date );
                                                        ?>
                                                        <strong> <?= Yii::t('app',' Hire date').': <br/>'.$hire_date ?></strong><br/>
                                                        
                                                        <span>
                                                            <strong>
                                                        <?php if(isset($model->person->employeeInfos[0]->job_status)&&($model->person->employeeInfos[0]->job_status!=null) )
                                                              echo $model->person->employeeInfos[0]->jobStatus->status_name; ?>
                                                            </strong>
                                                        </span>
                                                          <?php if($model->person->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/".$model->person->image)?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>
                            
                                                             <span>
                                                            <strong>
                                                        <?php 
					         	  								//echo Yii::t('app', 'Title').' : ';
					         	  								
					         	  								$title_id = 0;
					         	  								$title='';
					         	  								
					         	  								$modelTitle=SrcPersonsHasTitles::find()-> where('persons_id='.$model->person->id.' and academic_year='.$acad)->all();
		                      
											                     if($modelTitle!=null)
											                       { 
											                       	   foreach($modelTitle as $tit)
											                       	     $title_id= $tit->titles_id;
											                       }
		                       	     
					         	  								if( ($title_id != 0 )&&($title_id != null ) )
					         	  								  { $modelTitle = SrcTitles::findOne($title_id);
					         	  								      $title = $modelTitle->title_name;
					         	  								   }
					         	  								
					         	  								
					         	  								  echo $title;
					         	  						 ?>
					         	  						   </strong>
                                                        </span>
                                                       
          </div>
    
</div>


 


                                                       


 
 
 
 
 
 
 