<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Fees */

$this->title = $model->fee0->fee_label;

?>
<?= $this->render("//layouts/billingSettingLayout") ?>
 <div class="row">
     <div class="col-lg-7">
         <h3><?= '<span style="color:#CD3D4C">'.$this->title.' :</span> '; ?></h3>
     </div>
        <div class="col-lg-5">
<p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'set_f'], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'wh'=>'set_f'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'set_f'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

   
    
        </div>
    </div>




<div class="fees-view">

    

    <?= DetailView::widget([
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'model' => $model,
        'attributes' => [
            //'id',
            ['attribute'=>'program','value'=>$model->program0->label],
            //'academic_period',
            ['attribute'=>'level','value'=>$model->getLevel()],
          //  'fee0.fee_label',
            ['attribute'=>'amount','value'=>$model->getAmount()],
            //'devise',
            'date_limit_payment:date',
            //'checked',
            'description:ntext',
            //'date_create',
            //'date_update',
            'create_by',
            'update_by',
        ],
    ]) ?>

</div>
