<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcProgram;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\billings\models\SrcFees */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Fees');

?>


  <?= $this->render('//layouts/billingSettingLayout'); ?>
  
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['createlist','wh'=>'set_f'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content fees-index">
                 <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Program'); ?></th>
				            <th><?= Yii::t('app','Level'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <th><?= Yii::t('app','Date Limit Payment'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 


    $dataFee= $dataProvider->getModels();
    
          foreach($dataFee as $fee)
           {
           	   echo '  <tr >
                                                    <td ><span data-toggle="tooltip" title="'.$fee->program0->label.'">'.$fee->program0->short_name.'</span> </td>
                                                    <td >'.$fee->getLevel().' </td>
                                                    <td >'.$fee->fee0->fee_label.' </td>
                                                    <td >'.$fee->getAmount().' </td>
                                                    <td >'.Yii::$app->formatter->asDate($fee->date_limit_payment ).' </td>
                             
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                         if(Yii::$app->user->can('billings-fees-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/billings/fees/view?id='.$fee->id.'&wh=set_f', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('billings-fees-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/billings/fees/update?id='.$fee->id.'&wh=set_f', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('billings-fees-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/billings/fees/delete?id='.$fee->id.'&wh=set_f', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Fees List'},
                    {extend: 'pdf', title: 'Fees List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>



