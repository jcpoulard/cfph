<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcDevises;
use app\modules\fi\models\SrcProgram;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Fees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fees-form">

    <?php $form = ActiveForm::begin(
            [
            'id' => $model->formName(),
            'enableClientValidation'=> false
            ]
            ); 
    ?>


    
        <div class="row">
            <div class="col-lg-6">
           <?php
                 
                 echo $form->field($model, 'program')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                           'id'=>'program',
                           'name'=>'program',
                                    //'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
           ?>
        </div>
            <div class="col-lg-6 level">
                <?php
                     echo $form->field($model, 'level')->widget(Select2::classname(), [
                       'data'=>loadLevels(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
                           'id'=>'level',
                           'name'=>'level'
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                                               ]);


            ?>
                        </div>
        </div>
        
    <div class="row">      
    <table class="table table-striped table-bordered table-hover tablo">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Fee') ?></th>
                <th><?= Yii::t('app','Devise') ?></th>
                <th><?= Yii::t('app','Amount') ?></th>
                <th><?= Yii::t('app','Date Limit Payment'); ?></th>
                <th><?= Yii::t('app','Description') ?></th>
            </tr>
                <tbody>
                    <?php
                        for($i=1;$i<=12;$i++){
                          ?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td>
                            <?php
		          echo $form->field($model, 'fee')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcFeesLabel::find()->all(),'id','fee_label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
                           'id'=>'fee'.$i,
                           'name'=>'fee'.$i,
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false); 
		     ?>
                        </td>
                        <td>
                            <?php
		         echo $form->field($model, 'devise')->widget(Select2::classname(), [

                       'data'=>getDevises(),  //ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                           'id'=>'devise'.$i,
                           'name'=>'devise'.$i,
                                   // 'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false); 
                   
		     ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'amount')->textInput(['name'=>'amount'.$i])->label(false); ?>
                        </td>
                        <td>
                            <?php
                            echo $form->field($model, 'date_limit_payment')->widget(DatePicker::classname(), [
                                'options'=>['placeholder'=>'',
                                    'id'=>'date_limit_payment'.$i,
                                    'name'=>'date_limit_payment'.$i,
                                    ],
                                'language'=>'fr',
                                
                                'pluginOptions'=>[
                                    
                                     'autoclose'=>true, 
                                       'format'=>'yyyy-mm-dd', 
                                    ],

                                ] )->label(false);
               
                            ?>
                        </td>
                        <td>
                            <?= $form->field($model, 'description')->textarea(['rows' => 1,'name'=>'description'.$i])->label(false) ?>
                        </td>
                    </tr>
                    <?php 
                        }
                    ?>
                </tbody>
        </thead>
    </table>
    
</div>	
     <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4 bouton">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    
    <?php 
        for($i=0;$i<10;$i++){
            
            ?>
    <br/>
    <?php 
        }
    ?>
    
</div>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;

$script = <<< JS
      $('#program').change(function(){
        $('.level').show();
        
        
   });
   
   $('#level').change(function(){
        $('.bouton').show();
        $('.tablo').show();
        }
       );
        
   $(document).ready(function(){
        $('.level').hide();
        $('.bouton').hide(); 
        $('.tablo').hide();
        
   });      
JS;
$this->registerJs($script);
?>

