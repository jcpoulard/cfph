<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Fees */

$this->title = Yii::t('app', 'Create Fees');

?>
<?= $this->render("//layouts/billingSettingLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="fees-create">

    
    <?= $this->render('_formlist', [
        'model' => $model,
    ]) ?>

</div>
