<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\OtherIncomesDescription */

$this->title = Yii::t('app', 'Create Other Income Description');

?>

<?= $this->render("//layouts/billingSettingLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>

<div class="other-incomes-description-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
