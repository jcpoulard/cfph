<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\billings\models\SrcOtherIncomeDescription;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\OtherIncomesDescription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="other-incomes-description-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
           <?= $form->field($model, 'income_description')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-6">
            
            <?php  
                    $data_incomeDescription = loadCategoryIncome();
            ?>
                      <?= $form->field($model, 'category')->widget(Select2::classname(), [
                       'data'=>$data_incomeDescription,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select category  --'),
                                    //'onchange'=>'submit()', 
                                   ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>       
       </div>
</div>

<div class="row">
     <div class="col-lg-12">
           <?= $form->field($model, 'comment')->textarea(['rows' => 1]) ?>
       </div>
</div>

       <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
