<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\Billings;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcReservation;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder; 
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcAcademicperiods;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * BillingsController implements the CRUD actions for Billings model.
 */
class BillingsController extends Controller
{
     public $layout = "/inspinia";
     
    public $student_id; 
	public $is_b_check; 
	public $old_balance;
	public $old_student;
	public $fee_period;
	public $old_fee_period;
	public $old_amount_to_pay;
	public $old_amount_pay;
	public $remain_balance;
	
	
	public $applyProgram;	
	//public $previous_level;
		
	public $id_income_desc;
	public $recettesItems = 0;
	public $part; 
	public $month_ =0;
	public $status_ = 1;
	public $internal=0;
        public $exempt_fees_wanted =0;
	
	public $id_reservation;
	public $amount_reservation;
	public $reservation = false;				     
	
	public $message_paymentMethod=false;
	public $message_datepay=false;
	public $message_paymentAllow=true;
	public $message_positiveBalance=false;
	public $message_2paymentFeeDatepay=false;
	public $message_fullScholarship=false;
	public $message_scholarship=false;
	public $full_scholarship = false;
	public $special_payment = false;
	
	public $is_pending_balance = false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Billings models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set("memory_limit", "-1");
set_time_limit(0);

        if(Yii::$app->user->can('billings-billings-index'))
         {
       
       $acad=Yii::$app->session['currentId_academic_year']; 
       
       $model= new SrcBillings();
		
		if(isset($_POST['SrcBillings']['recettesItems']))
		    $this->recettesItems = $_POST['SrcBillings']['recettesItems'];
		else
	     {
	     	 if(isset($_GET['ri']) )
	     	   $this->recettesItems = $_GET['ri']; 
	       }
		
		
		  
		 
    if($this->recettesItems==0)
      {         
             
               $this->status_ = 1;
               $condition ='fees_label.status=1';
               
                
          
	if(!isset($_GET['month_']))
       {
       	   $result__ = SrcBillings::find()->orderBy(['billings.date_pay'=>SORT_DESC])
			       	            ->joinWith(['feePeriod','feePeriod.fee0'])
			       	            ->select(['date_pay'])
			       	            ->distinct(true)
			       	            ->where(['fees_label.status'=>$this->status_])
			       	            ->all(); 
												 				       	   
						if($result__!=null) 
						 { foreach($result__ as $r)
						     { $current_month = getMonth($r['date_pay']);
						       $current_year = getYear($r['date_pay']);
						         
						          break;
						     }
						  }			
			//if(!isDateInAcademicRange($last_dat,$acad))
           //   $display = false;

       	  
        }
     else 
       {  $current_month = $_GET['month_'];
       	  $current_year = $_GET['year_'];
       	  $ri = $_GET['ri'];
        }


	        $searchModel = new SrcBillings();
	        //$dataProvider = $model->searchByMonth($condition,$current_month,$current_year,$acad);
	        $dataProvider = $model->searchByAcad($acad);
	        
		
				 
         }
       elseif($this->recettesItems==1)
		    {
		        $this->status_ = 0;
		        $condition =['fees_label.status'=>0];
		        
		           if(!isset($_GET['month_']))
			       {
			       	   $result__ = SrcBillings::find()->orderBy(['billings.date_pay'=>SORT_DESC])
			       	            ->joinWith(['feePeriod','feePeriod.fee0'])
			       	            ->select(['date_pay'])
			       	            ->distinct(true)
			       	            ->where(['fees_label.status'=>$this->status_])
			       	            ->all();
			       	   
			       	   if($result__!=null) 
						 { foreach($result__ as $r)
						     { $current_month = getMonth($r['date_pay']);
						        $current_year = getYear($r['date_pay']);
						        
						          break;
						     }
						  }
									       	  
			        }
			     else 
			       {  $current_month = $_GET['month_'];
			        $current_year = $_GET['year_'];
			        $ri = $_GET['ri'];
			       	  
			        }
             
            $searchModel = new SrcBillings();
               //$dataProvider = $model->searchByMonth($condition,$current_month,$current_year,$acad);
               $dataProvider = $model->searchByAcad($acad);
        
		
		
           
 
		
		      }
		   elseif($this->recettesItems==2) 
		    {
		        return $this->redirect(array('/billings/otherincomes/index?ri=2&from=b'));
		
		      }
		    elseif($this->recettesItems==3)
		       {
		          return $this->redirect(array('/billings/enrollmentincome/index?part=rec&from=b'));
		
		      }
             elseif($this->recettesItems==4)
		       {
		          return $this->redirect(array('/billings/reservation/index?part=rec&from=b'));
		
		      }
		    elseif($this->recettesItems==5)
		       {
		          return $this->redirect(array('/billings/billingsservices/index?part=rec&from=b'));
		
		      }
          
             return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'recettesItems'=>$this->recettesItems,
            'status_'=>$this->status_,
            'model'=>$model,
            
        ]);
        
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Displays a single Billings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-billings-view'))
         {
        
                $this->status_ = 1;
                $acad = Yii::$app->session['currentId_academic_year'];

                $modelAcad = new SrcAcademicperiods;
                 $previous_year= $modelAcad->getPreviousAcademicYear($acad);

                $modelAdd = new SrcBillings;
                $reservationInfo = new SrcReservation;
                
                $modelExempt=new SrcScholarshipHolder;
	        $modelFee = new SrcFees();
		
		$this->message_paymentAllow=true;
		$this->message_positiveBalance=false;
		$this->message_2paymentFeeDatepay=false;
		
		$this->full_scholarship = false;
		$this->special_payment = false;
		
		$this->is_pending_balance =false; //true si gen fre ane pase ki poko peye epi lok ekran sou fre sa yo
		
		$level= 0;
		
		$program= 0;
                
                $this->exempt_fees_wanted =0;
			 	                               
				
		$modelCurrency = getDefaultCurrency(); 
		if($modelCurrency!=null)
		   $currency_id = $modelCurrency->id; 
		else
		   $currency_id = null; 
                
                 $command1= Yii::$app->db->createCommand('SELECT id,fee_period,amount_to_pay,amount_pay,balance FROM `billings` WHERE student='.$id.' AND `fee_totally_paid`=0 AND balance<>0 AND academic_year='.$previous_year ); 
                 $data = $command1->queryAll();
                                                                        
                 if($data!=NULL) 
                  {
                       $this->is_pending_balance =true;
                      $acad = $previous_year;
                      
                      $command5 = Yii::$app->db->createCommand('SELECT level FROM student_level_history WHERE student_id='.$_GET['id'].' AND academic_year='.$previous_year)->queryAll();
                        if(($command5!=null))
                          {
                            foreach($command5 as $result)
                              {
                                $level = $result['level'];
                              }
                          }
                      
                     } 
                  else 
                      {
                        $this->is_pending_balance =false;
                        $modelAdd->exempt_fees=0;
                        $acad = Yii::$app->session['currentId_academic_year'];
                        $level=getLevelByStudentId($modelAdd->student);
                      }
                                 
         $all_students = loadAllStudentsJson();
        $json_all_students = json_encode($all_students,false);
        
 
	 if(($id==0)||($id==''))
	   {
			 $model = new SrcBillings;
                         $reservationInfo = new SrcReservation;
                         
                         if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                           {
                              $modelAdd = SrcBillings::findOne($_GET['bil']);
                              $this->fee_period = $modelAdd->fee_period;
                            }
                          else
                           $modelAdd = new SrcBillings;
			 
	     }
	  else
	    {	
		  // $modelBilling = $this->findModel($id);
               $status = 0;
                  //  $currency_id= $modelBilling->feePeriod->devise;
		
	     if(isset($_POST['Billings']['recettesItems']))
              {
                    $this->recettesItems = $_POST['Billings']['recettesItems'];
                
                
                
                    if($this->recettesItems ==  0)     
                                             $status = 1;					              
                                    elseif($this->recettesItems ==  1)     
                                               $status = 0;

                            $condition_fee_status = ['fees_label.status'=>$status];	

                                           $modelBill = new SrcBillings; 
                                           
                       // $last_id = $modelBill->getLastTransactionID($modelBilling->student, $condition_fee_status, $currency_id, $acad);
                       $last_id = $modelBill->getLastTransactionIDForGuest($id, $condition_fee_status, $acad);

					  // if($last_id==null)
					   //  $last_id = $id;
                }
              else
                { 
                    $condition_fee_status = ['fees_label.status'=>$this->status_]; 
                      //return an Integer (id)
					$modelBill = new SrcBillings;   	
                      //$last_id = $modelBill->getLastTransactionID($modelBilling->student, $condition_fee_status, $currency_id, $acad);
                       $last_id = $modelBill->getLastTransactionIDForGuest($id, $condition_fee_status, $acad);
		 	                               
				
		               if(isset($_GET['ri']))
					   {         
					       if($_GET['ri']==0)
					         {  $this->recettesItems =  0;     
					         	$status = 1;
					         	// $last_id = $_GET['id'];
                                                        $last_id = $modelBill->getLastTransactionIDForGuest($_GET['id'], $condition_fee_status, $acad);
					          }
					        elseif($_GET['ri']==1)
					          { $this->recettesItems =  1;     
					          	  $status = 0;
					          	  // $last_id = $_GET['id'];
                                                          $last_id = $modelBill->getLastTransactionIDForGuest($_GET['id'], $condition_fee_status, $acad);
					          	 }
					   }
					   
					   
					  // if($last_id==null)
					   //  $last_id = $id;
                   
                                           
                                           
                  }
            
 //            &&&&&&&&&&&&&&&&&&&&&     
                  
                   if($last_id!=null)
                    {
                       $model = $this->findModel($last_id);
                       
                       if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                            {$modelAdd = SrcBillings::findOne($_GET['bil']);
                            $this->fee_period = $modelAdd->fee_period;
                           }
                         else
                           $modelAdd = new SrcBillings;
                    }
                   else 
                      {
                          $model = new SrcBillings;
                          
                          if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                           {$modelAdd = SrcBillings::findOne($_GET['bil']);
                            $this->fee_period = $modelAdd->fee_period;
                           }
                         else
                           $modelAdd = new SrcBillings;
                          
                          $reservationInfo = new SrcReservation;
                          
                       }
            
   //########################################################################################
    		
                       if( (isset($_GET['id'])&&($_GET['id']!=''))&&(! isset($_GET['bil'])||($_GET['bil']=='')) ) 
                        { 
                          $modelAdd->student = $_GET['id'];
                          $this->student_id = $modelAdd->student;
                        
                      
                     
					 //get level and program for this student
				    if($modelAdd->student!=null)
				      {  
				      	//$level=getLevelByStudentId($modelAdd->student);
				      	$program=getProgramByStudentId($modelAdd->student);
				      	
				       }
		
								 
					 if($modelAdd->student != $this->student_id)
					   {  
					   	$data_fees=null;
					     	 
				   	      //gad si elev la gen balans ane pase ki poko peye
		   	                     /*  $modelPendingBal=SrcPendingBalance::find()
		   	                                     ->select(['id', 'balance'])
		   	                                     ->where(['student'=>$modelAdd->student])
		   	                                     ->andWhere(['is_paid'=>0])
		   	                                     ->andWhere(['academic_year'=>$previous_year])
		   	                                     ->all();
													 
					//si gen pending, ajoutel nan lis apeye a			
							if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
							  {
								  foreach($modelPendingBal as $bal)
								     {			     	 
										$this->is_pending_balance =true;
										
										$data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	               ->andWhere(['devise'=>$bal->devise])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
										
                                                                                $currency_id = $bal->devise;
							     		}
							     
							  }
							else
							   {
							   $this->is_pending_balance =false;
							  */ 	 
							   	 $data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['not like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$currency_id])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
							   	
							   	
							   //	}
							   	
							   	
				 	
						if($data_fees!=null)
						 {  
						 	foreach($data_fees as $fee)
						 	 {     
						 	 	$fee_ = $fee->id;
						 	 	
						 	 	 $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$modelAdd->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
							     if($modelBillings_balance0==null)
							 	  { $this->fee_period = $fee_;
                                                                  
                                                                     $currency_id = $fee->devise;
										 	    
									$modelAdd->fee_period= [$fee_];
							 	 
							 	 	break;
							 	  }
						 	  
						 	  }
			
					 	 	
						   }
						 else
						    $this->fee_period = '';
								 	
                                    
								
		     	
					$this->student_id= $modelAdd->student;
					
					  unset(Yii::$app->session['Student_billings']);
		                      Yii::$app->session['Student_billings'] = $this->student_id;
			    }
			  else
			   {
			   	   //$this->fee_period = $modelAdd->fee_period;
			   	      
			   	      //gad si elev la gen balans ane pase ki poko peye
	   	                      /*   $modelPendingBal=SrcPendingBalance::find()
		   	                                     ->select(['id', 'balance'])
		   	                                     ->where(['student'=>$modelAdd->student])
		   	                                     ->andWhere(['is_paid'=>0])
		   	                                     ->andWhere(['academic_year'=>$previous_year])
		   	                                     ->all();
		   	                                     
		   	                                    
				                       //si gen pending, ajoutel nan lis apeye a			
							if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
							  {
								  foreach($modelPendingBal as $bal)
								     {			     	 
										$this->is_pending_balance =true;
										
										$data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$bal->devise])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
                                                                                
                                                                                $currency_id = $bal->devise;
																			
							     		}
							     
							  }
							else
							   {
							   $this->is_pending_balance =false;
							   */	 
							   	 $data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['not like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$currency_id])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
							   	
							   	
							   //	}
							   	
	
						
						if($data_fees!=null)
						  {
						  	    if(isset($_POST['SrcBillings']['fee_period']))
						  	      { //$this->fee_period = $modelAdd->fee_period;
						  	      
						  	        
						  	         $this->fee_period = $_POST['SrcBillings']['fee_period'];
                                                                 
                                                                 $modelAdd->fee_period= $this->fee_period;
						  	        
						  	        if($this->fee_period =='')
						  	          {
						  	          	
                                                                    foreach($data_fees as $fee)
									 	 { 
									 	 		$fee_ = $fee->id;
									 	 		
									 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$modelAdd->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
											     if($modelBillings_balance0==null)
											 	  { $this->fee_period = $fee_;
													$currency_id = $fee->devise;	 	    
													$modelAdd->fee_period= [$fee_];
											 	 
											 	 	break;
											 	  }
						 	  
						 	                    
									 	  
									 	   }
								 	   
						  	          	}
                                                                        
						  	          	
						  	     
						  	       
						  	       }
						  	    else
						  	      {
						  	      	foreach($data_fees as $fee)
								 	 { 
                                                                            $fee_ = $fee->id;
                                                                    
								 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$modelAdd->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
										     if($modelBillings_balance0==null)
										 	  { $this->fee_period = $fee_;
											  $currency_id = $fee->devise;		 	    
												$modelAdd->fee_period= [$fee_];
										 	 
										 	 	break;
										 	  }
									 	  								 	  
								 	   }
								 	   
			
								 	   
						  	      	}
						  	}
						else
						 { 
						 	
						 	
						     if(isset($_POST['SrcBillings']['fee_period']))
						  	      { //$this->fee_period = $modelAdd->fee_period;
						  	      
					  	          
						  	        
						  	         $this->fee_period = $_POST['SrcBillings']['fee_period'];
                                                                 
                                                                 $modelAdd->fee_period= $this->fee_period;
						  	        
						  	        if($this->fee_period =='')
						  	          {
						  	          	foreach($data_fees as $fee)
									 	 { 
									 	 		$fee_ = $fee->id;
									 	 		
									 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$modelAdd->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
											     if($modelBillings_balance0==null)
											 	  { $this->fee_period = $fee_;
												$currency_id = $fee->devise;		 	    
													$modelAdd->fee_period= [$fee_];
											 	 
											 	 	break;
											 	  }
						 	  
						 	                    
									 	  
									 	   }
								 	   
						  	          	}
						  	       
						  	       }
						  	    else
						  	      {
						  	      	/*
						  	      	foreach($data_fees as $fee_)
								 	 { 
								 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$modelAdd->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
										     if($modelBillings_balance0==null)
										 	  { $this->fee_period = $fee_;
													 	    
												$modelAdd->fee_period= [$fee_];
										 	 
										 	 	break;
										 	  }
									 	  								 	  
								 	   }
								 	   
		                             */
								 	   
						  	      $modelAdd->fee_period = '';
						  	      	}
						  	   
						  	    
						  	    
						  	       
						   }
						     
		          }
            
                          
                    }
                  elseif(( isset($_GET['bil'])||($_GET['bil']!='')) ) 
                    {     
                        $this->remain_balance = $modelAdd->amount_to_pay;
                      }
                                 
				
				$last_payment_transaction_fee_period =null;
						$last_fee_paid = null;				
				
			   
							 if(($this->student_id !='')&&($this->fee_period !=''))
								 {
					                 $this->message_positiveBalance = false;
					               
					               $modelPay_method = new SrcPaymentMethod;
								    //tcheke si default method lan defini deja
						      	   $default_pmethod_defined = $modelPay_method->isAlreadyDefined(); 
						      	   
						      	 if($default_pmethod_defined!=NULL)
                                    { 
		                                foreach($default_pmethod_defined as $default_pmethod_def)
		                                  {    
		                                  	 if($modelAdd->payment_method=='')
		                                  	    $modelAdd->payment_method = $default_pmethod_def['id'];  
		                                  }
							               
                                     }
					                 
					                 
					             //gad si gen rezevasyon   
					                 //return an array(id, amount) or null value
										$modelReservation = new SrcReservation;
										$deja_peye = $modelReservation->getNonCheckedReservation($this->student_id, $previous_year);				  
										if( ($deja_peye==null)||($deja_peye==0) )
										  $deja_peye = 0;
										else
										   {  foreach($deja_peye as $r)
                                                { $this->amount_reservation = $r->amount;
                                                  $currency_id= $r->devise;
                                                   $this->id_reservation = $r->id;
                                                  }
                                                 
										   	    $this->reservation = true;
										      
										     }
					           
				                   //-------------------------
				                   
				                   
				                   
				                   
					                 
								   //	$to_pay = Fees::model()->FindByAttributes(array('id'=>$this->fee_period));
								   		$tou_pay = SrcFees::find()
								   		          ->joinWith(['fee0',])
								   		          ->where(['fees.id'=>$this->fee_period])
								   		          ->andWhere(['fees_label.status'=>$this->status_])
								   		          ->all();
								   	  
							   
								     	
								   	  
						  if($tou_pay!=null) 
							{	  
							   foreach($tou_pay as $to_pay)
							   {    
								   	  
								   	  $student = $this->student_id;
								   	  $date_lim = $to_pay->date_limit_payment;
						              $new_balance = 0;
						              
						              $currency_id = $to_pay->devise;
						              
						              $condition_fee_status = ['fees_label.status'  => $this->status_]; 
									
									//return "id", "amount_pay" and "balance"
									$modelBilling = new SrcBillings();
									$checkForBalance_ = $modelBilling->checkForBalance($this->student_id, $this->fee_period, $this->status_, $acad);
								
							   
							   if($checkForBalance_ != null)
								 {    
								 	foreach($checkForBalance_ as $checkForBalance)
								 	{ 
									    
									    if($checkForBalance->amount_pay!=0)
									       { 
										      
									       	  $this->remain_balance = $checkForBalance->balance;
									       }
									    else
									       {
									    	        $amount = 0;
											                	 
												                	 $modelFee = SrcFees::findOne($this->fee_period);
												                	 
												                	 $currency_id= $modelFee->devise;
												                	 
												                	  //check if student is scholarship holder
												                	  $modelPerson = new SrcPersons();
														           	 //return 0(pa bousye) or 1(bousye)
				  								           	  $model_scholarship = $modelPerson->getIsScholarshipHolder($this->student_id,$acad);    
														           	  
														           	  $konte =0;
																	  $amount = 0;	
																		$percentage_pay = 0;
																		 $porcentage_level = 0; //mwens ke 100%
																		  $this->internal=0;
                                                                       $partner_repondant = NULL;																		
																	  $premye_fee = NULL;
																		          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																			                 $modelScholarshipH = new SrcScholarshipHolder();
																							   $notNullFee = $modelScholarshipH->feeNotNullByStudentId($this->student_id,$acad);
																		           	          $full=1;
																						   if($notNullFee!=NULL)
																							{																								                                                            
																							  foreach($notNullFee as $scholarship)
																			           	    	{ $konte++;
																			           	    	  
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   {
																			           	    	   	 $full=0;
																			           	    	   	 
																			           	    	     if($scholarship->fee == $modelFee->id)
																			           	    	     { 
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																										$partner_repondant = $scholarship->partner;
																		           	                    
																									   if(($partner_repondant==NULL))
																									    {	$amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																		           	                    
																											 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																									     
																										  }
																										 else
																											  $amount = $modelFee->amount;
																			           	    	      }
																			           	    	      
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								if((($konte>0)&&($amount==0))&&(!$this->full_scholarship))
																				           	    	  $amount = $modelFee->amount;
																							}
																						   else
																						     {
																								 // $this->full_scholarship = true;
																								 
																								 //fee ka NULL tou
																								  $check_partner=$modelScholarshipH ->getScholarshipPartnerByStudentIdFee($this->student_id,NULL,$acad);
																								  //$porcentage_level = 1; //100%
																								  //$percentage_pay=100;
																								  
																								   if($check_partner!=NULL)
																								   {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }
																								   
																								       if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												  
																												 
																											  }
																								       else
																								       {			  

																								   if(($partner_repondant==NULL))
																								      {  $amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																								         $this->internal=1;
																									  }
																								   else
																								      $amount = $modelFee->amount;
																								       }
																							 }
																			           	    	 
																			           	    
																			           	 }
																			           elseif($model_scholarship == 0)   //se pa yon bousye
																					     $amount = $modelFee->amount;
						           	                                            

														             //$this->remain_balance = $amount ;
														             
														       //etap 2
														            //gade si denye peman gen balans (-) nan tab billins lan
															       //return an Integer (id)
															        $modelBilling = new SrcBillings();
                                                                                                                                
                                                                                                                       // denye tranzaksyon pou devise sa
															      //return id or null value 
																  $last_payment_transaction = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
																  
																 if($last_payment_transaction!=null)
																  {
																	  
																	  //return "id", "amount_pay" and "balance"
									                                  $getPreviousFeeBalance = $modelBilling->getPreviousFeeBalanceByDevise($this->student_id, $modelFee->program, $modelFee->date_limit_payment,$modelFee->devise, $acad);
								
												                       if($getPreviousFeeBalance != null)
													                       {     
														                      foreach($getPreviousFeeBalance as $previousFeeBalance)
														                        {
														                        	if($previousFeeBalance->balance >0)
														                                     $this->message_positiveBalance = true;
                                                                                                                                                 else 
                                                                                                                                                   {     $feePaid_array_plus = [];                                
                                                                                                                                                         $modelFeePaid  = $modelBilling->searchPaidFeesByStudentIdDevise($this->student_id, $condition_fee_status, $modelFee->devise, $acad);
                                                                                                                                                         
                                                                                                                                                         if($modelFeePaid!=null)
                                                                                                                                                            {
                                                                                                                                                                foreach($modelFeePaid as $feePaid)
                                                                                                                                                                  {
                                                                                                                                                                       $feePaid_array_plus[] = $feePaid->id;
                                                                                                                                                                    }
                                                                                                                                                                    
                                                                                                                                                              }
                                                                                                                                                              
                                                                                                                                                              
                                                                                                                                                                $data_fees_plus = SrcFees::find()
                                                                                                                                                                            ->orderBy(['date_limit_payment'=>SORT_ASC])
                                                                                                                                                                            ->joinWith(['fee0',])
                                                                                                                                                                            ->where(['academic_period'=>$acad])
                                                                                                                                                                            ->andWhere(['not like','fees_label.fee_label','Pending balance'])
                                                                                                                                                                            ->andWhere(['fees_label.status'=>$condition_fee_status])
                                                                                                                                                                            ->andWhere(['devise'=>$modelFee->devise])
                                                                                                                                                                            ->andWhere(['program'=>$program])
                                                                                                                                                                            ->andWhere(['level'=>$level])
                                                                                                                                                                            ->all();
                                                                                                                                                          

                                                                                                                                                                $balans_plus = 0;
                                                                                                                                                                
                                                                                                                                                                if($data_fees_plus!=null)
                                                                                                                                                                 {  
                                                                                                                                                                        foreach($data_fees_plus as $fee)
                                                                                                                                                                         {     
                                                                                                                                                                                    $fee_ = $fee->id;
                                                                                                                                                                                
                                                                                                                                                                                if(!in_array($fee_, $feePaid_array_plus)  )
                                                                                                                                                                                  {
                                                                                                                                                                                                                 
                                                                                                                                                                                     $modelBillings_balance0=SrcBillings::find()
                                                                                                                                                                                                          ->orderBy(['id'=>SORT_DESC])
                                                                                                                                                                                                          ->where('student='.$this->student_id.' and fee_period='.$fee_.' and academic_year='.$acad.' and balance<>0 and fee_totally_paid=0')
                                                                                                                                                                                                          ->all();
                                                                                                                                                                                     if($fee_!=$modelFee->id)
                                                                                                                                                                                               { 
                                                                                                                                                                                        if($modelBillings_balance0!=null)
                                                                                                                                                                                             { 
                                                                                                                                                                                               $balans_plus = 1;
                                                                                                                                                                                               break;
                                                                                                                                                                                             
                                                                                                                                                                                                
                                                                                                                                                                                                
                                                                                                                                                                                               }

                                                                                                                                                                                             }
                                                                                                                                                                                         else 
                                                                                                                                                                                             break;
                                                                                                                                                                                             
                                                                                                                                                                                  }
                                                                                                                                                                                  
                                                                                                                                                                                  
                                                                                                                                                                         }
                                                                                                                                                                         
                                                                                                                                                                   }
                                                                                                                                                                  
                                                                                                                                                                   if($balans_plus==1)
                                                                                                                                                                    {
                                                                                                                                                                       $this->message_positiveBalance = true;
                                                                                                                                                                    }
                                                                                                                                                                 

                                                                                                                                                     }
                                                                                                                                                     
										
														                        }
														       
													                       }	
													                       	
												 				    $modelBillings_last_payment= SrcBillings::findOne($last_payment_transaction);
																       
																       if($modelBillings_last_payment->balance <= 0)
																          {  
																          
																          
																          	$this->remain_balance = $amount + $modelBillings_last_payment->balance;
																          	   if($this->remain_balance <= 0)
										                                           { $this->special_payment = true;
										                                              
										                                               $modelAdd->setAttribute('amount_pay', $amount );
										                                             }
																          	   
																          	}
																       elseif($modelBillings_last_payment->balance == $checkForBalance->balance)
                                                                                                                                            $this->remain_balance = $checkForBalance->balance;
                                                                                                                                          else
																               $this->message_positiveBalance = true;
							                                             			   }
																 else
																   {
																   	   $this->remain_balance = $amount;
																   	 }

														             
									    	
									          }
								    
										    if(($this->remain_balance <= 0) )//
												$this->message_paymentAllow = false;
								    
								         
								          break;
								        
								         }//end foreach checkbalance
								         
								     }
								   else
								     {     
										  $amount = 0;
												                	 
												                	 $modelFee = SrcFees::findOne($this->fee_period);
												                
												                  $currency_id = $modelFee->devise;
												                //si se pending balance
												                if($this->is_pending_balance==false)
												                 {	 
												                	  $modelAdd->exempt_fees=0;
												                	  
												                	  //check if student is scholarship holder
												                	  $modelScholoarshipH = new SrcScholarshipHolder();
												                	  //return 0(pa bousye) or 1(bousye)
														           	  $model_scholarship = $modelScholoarshipH->getIsScholarshipHolder($this->student_id,$acad);    
														           	  
														           	  $konte =0;
																	  $amount = 0;	
																		$percentage_pay = 0;
																		 $porcentage_level = 0; //mwens ke 100%
																		 $this->internal=0;
                                                                   $partner_repondant = NULL;																		
																	  $premye_fee = NULL;
			        													          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																							   $modelScholoarshipH = new SrcScholarshipHolder();
																							   $notNullFee = $modelScholoarshipH->feeNotNullByStudentId($this->student_id,$acad);
																			           	      $full=1;
																						   if($notNullFee!=NULL)
																							{																								
																							  foreach($notNullFee as $scholarship)
																			           	    	{ $konte++;
																			           	    	  
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   { 
																			           	    	   	  $full=0;
																			           	    	    if($scholarship->fee == $modelFee->id)
																			           	    	     { 
																			           	    	     	$currency_id = $modelFee->devise;
																			           	    	     	
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																										$partner_repondant = $scholarship->partner;
																		           	                    
																									   if(($partner_repondant==NULL))
																									    {	$amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																		           	                    
																											 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																									     
																										  }
																										 else
																											  $amount = $modelFee->amount;
																			           	    	      }
																			           	    	      
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								if((($konte>0)&&($amount==0))&&(!$this->full_scholarship))
																				           	    	  $amount = $modelFee->amount;
																							}
																						   else
																						     {
																								// $this->full_scholarship = true;
																								 
																								 //fee ka NULL tou
																								  $check_partner=$modelScholoarshipH->getScholarshipPartnerByStudentIdFee($this->student_id,NULL,$acad);
																								  //$porcentage_level = 1; //100%
																								  //$percentage_pay=100;
																								  
																								   if($check_partner!=NULL)
																								   {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }
																								   
																								       if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												   
																												 
																											  }
																								       else
																								       {			  

																								   if(($partner_repondant==NULL))
																								      {  $amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																								         $this->internal=1;
																									  }
																								   else
																								      $amount = $modelFee->amount;
																								       }
																							 }
																			           	    	 
																			           	    
																			           	 }
																			           elseif($model_scholarship == 0)   //se pa yon bousye
																					     $amount = $modelFee->amount;
							           	  

														             //$this->remain_balance = $amount ;
														             
														       //etap 2
														            //gade si denye peman gen balans (-) nan tab billins lan
															      $modelBilling = new SrcBillings();
															       //return an Integer (id)
																  $last_payment_transaction = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
																  
																 if($last_payment_transaction!=null)
																  {
																  	
												 				    $modelBillings_last_payment= SrcBillings::findOne($last_payment_transaction);
																       
																    //return "id", "amount_pay" and "balance"
									                                $getPreviousFeeBalance = $modelBilling->getPreviousFeeBalanceByDevise($this->student_id, $modelFee->program, $modelFee->date_limit_payment,$modelFee->devise, $acad);
								
												                       if($getPreviousFeeBalance != null)
													                       {     
														                      foreach($getPreviousFeeBalance as $previousFeeBalance)
														                        {
														                           if($previousFeeBalance->balance >0)
														                                $this->message_positiveBalance = true;
                                                                                                                                                                   
														                              
														                                
														                        }
														       
													                       }
                                                                                                                                                 else 
                                                                                                                                                   {     $feePaid_array_plus = [];                                
                                                                                                                                                         $modelFeePaid  = $modelBilling->searchPaidFeesByStudentIdDevise($this->student_id, $condition_fee_status, $modelFee->devise, $acad);
                                                                                                                                                         
                                                                                                                                                         if($modelFeePaid!=null)
                                                                                                                                                            {
                                                                                                                                                                foreach($modelFeePaid as $feePaid)
                                                                                                                                                                  {
                                                                                                                                                                       $feePaid_array_plus[] = $feePaid->id;
                                                                                                                                                                    }
                                                                                                                                                                    
                                                                                                                                                              }
                                                                                                                                                              
                                                                                                                                                              
                                                                                                                                                                $data_fees_plus = SrcFees::find()
                                                                                                                                                                            ->orderBy(['date_limit_payment'=>SORT_ASC])
                                                                                                                                                                            ->joinWith(['fee0',])
                                                                                                                                                                            ->where(['academic_period'=>$acad])
                                                                                                                                                                            ->andWhere(['not like','fees_label.fee_label','Pending balance'])
                                                                                                                                                                            ->andWhere(['fees_label.status'=>$condition_fee_status])
                                                                                                                                                                            ->andWhere(['devise'=>$modelFee->devise])
                                                                                                                                                                            ->andWhere(['program'=>$program])
                                                                                                                                                                            ->andWhere(['level'=>$level])
                                                                                                                                                                            ->all();
                                                                                                                                                          

                                                                                                                                                                $balans_plus = 0;
                                                                                                                                                                
                                                                                                                                                                if($data_fees_plus!=null)
                                                                                                                                                                 {  
                                                                                                                                                                        foreach($data_fees_plus as $fee)
                                                                                                                                                                         {     
                                                                                                                                                                                    $fee_ = $fee->id;
                                                                                                                                                                                
                                                                                                                                                                                if(!in_array($fee_, $feePaid_array_plus)  )
                                                                                                                                                                                  {
                                                                                                                                                                                                                 
                                                                                                                                                                                     $modelBillings_balance0=SrcBillings::find()
                                                                                                                                                                                                          ->orderBy(['id'=>SORT_DESC])
                                                                                                                                                                                                          ->where('student='.$this->student_id.' and fee_period='.$fee_.' and academic_year='.$acad.' and balance<>0 and fee_totally_paid=0')
                                                                                                                                                                                                          ->all();
                                                                                                                                                                                     if($fee_!=$modelFee->id)
                                                                                                                                                                                               { 
                                                                                                                                                                                        if($modelBillings_balance0!=null)
                                                                                                                                                                                             { 
                                                                                                                                                                                               $balans_plus = 1;
                                                                                                                                                                                               break;
                                                                                                                                                                                                                                                                                                                                     
                                                                                                                                                                                                
                                                                                                                                                                                               }

                                                                                                                                                                                             }
                                                                                                                                                                                         else 
                                                                                                                                                                                             break;
                                                                                                                                                                                             
                                                                                                                                                                                  }
                                                                                                                                                                                  
                                                                                                                                                                                  
                                                                                                                                                                         }
                                                                                                                                                                         
                                                                                                                                                                   }
                                                                                                                                                                  
                                                                                                                                                                   if($balans_plus==1)
                                                                                                                                                                    {
                                                                                                                                                                       $this->message_positiveBalance = true;
                                                                                                                                                                    }
                                                                                                                                                                 

                                                                                                                                                     }
                                                                                                                                                     
                                                                                                                                                     
								                                     
																       if($modelBillings_last_payment->balance <= 0)
																          {  
		  														          	$currency_id = $modelBillings_last_payment->feePeriod->devise;
		  														          	
																          	$this->remain_balance = $amount + $modelBillings_last_payment->balance;
																          	  if(($modelBillings_last_payment->balance < 0)&&($this->remain_balance==0))
																          	   {
																          	   	   $this->message_paymentAllow =false;
																          	     	$this->special_payment = true;
																          	     	
										                                              
										                                               $this->remain_balance = $amount;
										                                               $modelAdd->setAttribute('amount_pay', $amount );
																          	   	  }
																          	     
																          	     if($this->remain_balance < 0)
										                                           { $this->special_payment = true;
										                                              
										                                               $modelAdd->setAttribute('amount_pay', $amount );
										                                             }
																          	}
																         else
																            $this->message_positiveBalance = true;
																          
																          
																          
																          
																   }
																 else
																   {
																   	   $this->remain_balance = $amount;
																   	 }
																   	 
								                         }//fen se pa "Pending balance"

											if(($this->remain_balance <= 0) )//
										        $this->message_paymentAllow = false;		
										      
			               
          	             
								       
								       
								       }
								    
								    }
								      
                               
								  
								 }//end foreach topay							 
		
                     if( (isset($_GET['id'])&&($_GET['id']!=''))&&(isset($_GET['bil'])&&($_GET['bil']!='')) )
                       {
                           $this->fee_period = $modelAdd->fee_period;
                           
                          
                       }
                       
                       
								   		          
								  }
								   
					        
				      //tcheke si elev sa te peye rezevasyon 
					    if($this->reservation == true)
						   {
						   	   $this->remain_balance = $this->remain_balance - $this->amount_reservation;
						   	   
						   	   if($this->remain_balance!=0)
								 {
									//$this->message_paymentAllow = false;
									//$this->special_payment = false;
													     
								  }
								else
								  {     $this->message_paymentAllow = false;
										$this->special_payment = true;
										
										//load appropriate info reservation 
										$reservationInfo = SrcReservation::findOne($this->id_reservation);
										
										$modelAdd->setAttribute('amount_pay', $this->amount_reservation );
										$modelAdd->setAttribute('payment_method', $reservationInfo->payment_method );
										$modelAdd->date_pay=$reservationInfo->payment_date;
										$modelAdd->setAttribute('comments', $reservationInfo->comments );
										$modelAdd->setAttribute('reservation_id', $this->id_reservation );
										           	
								   }
								   
						   	 }       
                             
								  $modelAdd->setAttribute('amount_to_pay', $this->remain_balance );
//pou update			                   
   if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
     {
         $this->message_paymentAllow=true;
	 $this->message_positiveBalance=false;
	 $this->message_2paymentFeeDatepay=false;
		
          $this->old_balance =0;
          $this->old_student ="";
		
          $balance_current_record = $modelAdd->balance;
        
          $this->old_balance = $modelAdd->balance;  //
           $this->old_student = $modelAdd->student; //
            $this->fee_period=$modelAdd->fee_period;
            $this->old_fee_period = $modelAdd->fee_period;
            $this->old_amount_to_pay = $modelAdd->amount_to_pay;
            $this->old_amount_pay = $modelAdd->amount_pay;
        
        $modelUp = SrcBillings::findOne($_GET['bil']);
        
        $modelAddFee = SrcFees::findOne($modelAdd->fee_period);
        
        $currency_id = $modelAddFee->devise;
        
        //$modelAddFeeLabel = FeesLabel::model()->findByPk($modelAddFee->fee);
          if($modelAddFee->fee0->fee_label=="Pending balance")
            $this->is_pending_balance=true;
          
        } //fen pou update     
                                                                  
         if($modelAdd->load(Yii::$app->request->post()) ) 
           {      
              //pou update
              if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                {
                   $condition_fee_status = ['fees_label.status'=>$this->status_];
					    
                    $this->student_id = $this->old_student;//Yii::app()->session['Student_billings'];
                    $this->fee_period = $this->old_fee_period;

                    $this->message_positiveBalance = false;
                  
                 }//fen pou update
                 
                 
            //########################## DEBU EXEMPTION #################################### 
              if(isset($modelAdd->exempt_fees)&&($modelAdd->exempt_fees==1))
                { 
                  if($this->exempt_fees_wanted != $modelAdd->exempt_fees)
                    {  
                      
                     } 
                     
                     if(isset($_POST['exempt']))
                      {
                         $modelExempt->load(Yii::$app->request->post());
                         $comment = Yii::t('app','This balance is exempted by {name}.',array('{name}'=> Yii::$app->user->identity->username) ).' ('.ChangeDateFormat(date('Y-m-d')).') ';
                         if($modelExempt->comment!='')
			    $comment = $modelExempt->comment;
                          
                         $selection=(array)Yii::$app->request->post('selection');//typecasting
				       if($selection!=null)
					    {   foreach($selection as $id)
					         {
					            $fee_id = (int)$id;
			
                                                    $amount_exempted = 0;
                      	 $percentage_exempted = 100;
                      	 
                      	  $modelFee1 = SrcFees::findOne($fee_id);
                      	  
                      	  $fee_amount = $modelFee1->amount;
                      	  
                      	  $amount_exempted = $modelFee1->amount;
                      	 
                          $total_pay_on_fee= $modelFee1->getTotalAmountPayOnFee($_GET['id'],$fee_id);
                           
                           $dbTrans = Yii::$app->db->beginTransaction();
                          $is_save=false;
                               	
                           if($total_pay_on_fee!=0)  
                             {
                             	$amount_exempted = $fee_amount-$total_pay_on_fee;
                             	
                             	$percentage_paid = round( (($total_pay_on_fee * 100) / $fee_amount), 2);
                             	
                             	$percentage_exempted = 100 - $percentage_paid;
                             	
                             	 //met balans denye tranzaksyon an a 0, ajoute komante pou sa
                             	 //return a integer (id)
                                 $Last_transaction_id = $modelBil->getLastTransactionIdByFeeId($_GET['id'], $fee_id, $acad);
                                    
                             	     $command_ = Yii::$app->db->createCommand();
								     $command_->update('billings', ['fee_totally_paid' => 1, 'balance'=>0,'comments'=>$comment  ], ['id'=>$Last_transaction_id ])
									         ->execute();
								                 
								                  
								         //met fee_totally_paid=1 pou tranzaksyon anvan yo
								           $command1 = Yii::$app->db->createCommand();
								           $command1->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$fee_id,'student'=>$_GET['id'] ])
									              ->execute();

     
                             	     
                             	      
                             	 
                             	  //retire % exempted a nan total balans li
                             	  $modBalance = new SrcBalance();
                             	   $modelBalance_=SrcBalance::find()->where(['student'=>$_GET['id'] ])->all();
					                 $new_balance =0;
					                  if($modelBalance_!=null)    
					                     {  
					                     	foreach($modelBalance_ as $modelBalance)
					                     	{
						                     	$new_balance=$modelBalance->balance - $amount_exempted;
						                        
						                        $modelBalance->setAttribute('balance',  $new_balance);
																       	    
												if($modelBalance->save())
                                                                                                        $is_save=true;
                                                                                                else 
                                                                                                    $is_save=false;
					                     	}
															       	     
					                     }
                             	      
                            	      
                             	      
                             	       //ajoute l nan bousye ak komante %li pa peye a
                             	       $modelScholarshipHolder=new SrcScholarshipHolder;
		                                    $modelScholarshipHolder->setAttribute('student', $_GET['id']);
		                                    $modelScholarshipHolder->setAttribute('partner', NULL);
		                                    $modelScholarshipHolder->setAttribute('fee', $modelFee1->id);
		                                    $modelScholarshipHolder->setAttribute('is_internal', 1);
		                                    $modelScholarshipHolder->setAttribute('comment', $comment);
		                                    $modelScholarshipHolder->setAttribute('percentage_pay', $percentage_exempted);
											$modelScholarshipHolder->setAttribute('academic_year', $acad);
											$modelScholarshipHolder->setAttribute('create_by', currentUser()  );
										    $modelScholarshipHolder->setAttribute('date_created', date('Y-m-d') );
									  
                             	                                                   if($modelScholarshipHolder->save() )
                                                                                    {
                                                                                        if($is_save)
                                                                                            {
                                                                                                $dbTrans->commit();
                                                                                            }
                                                                                             else
                                                                                                { $dbTrans->rollback();

                                                                                                 }
                                                                                    }
                                                                                  else
                                                                                    { $dbTrans->rollback();

                                                                                     }
                             	 
                             	}
                             else
                               { // lanse reket delete liy sa nan billings
                                     $command0 = Yii::$app->db->createCommand();
	                                 $command0->delete('billings', ['student'=>$_GET['id'],'fee_period'=>$fee_id, 'amount_pay'=>0, 'academic_year'=>$acad])
									              ->execute();
									              
									              //$command0->delete('billings', 'student=:stud AND fee_period=:fee AND amount_pay=0 AND academic_year=:acad', array(':stud'=>$this->student_id,':fee'=>$fee_id,':acad'=>$acad_sess));
                               	    
                               	     
                               	     //retire % exempted a nan total balans li 
                               	     $modBalance = new SrcBalance();
                             	   $modelBalance_=SrcBalance::find()->where(['student'=>$_GET['id'] ])->all();
					                  $new_balance =0;
					                  if($modelBalance_!=null)    
					                     {  
					                     	foreach($modelBalance_ as $modelBalance)
					                     	{
						                     	$new_balance=$modelBalance->balance - $amount_exempted;
						                        
						                        $modelBalance->setAttribute('balance',  $new_balance);
																       	    
												if($modelBalance->save())
                                                                                                        $is_save=true;
                                                                                                else 
                                                                                                    $is_save=false;
					                     	 }
															       	     
					                     }
                               	     
                               	     
                               	     //ajoute l nan bousye ak komante
                               	     $modelScholarshipHolder=new SrcScholarshipHolder;
		                                    $modelScholarshipHolder->setAttribute('student', $_GET['id']);
		                                    $modelScholarshipHolder->setAttribute('partner', NULL);
		                                    $modelScholarshipHolder->setAttribute('fee', $modelFee1->id);
		                                    $modelScholarshipHolder->setAttribute('is_internal', 1);
		                                    $modelScholarshipHolder->setAttribute('comment', $comment);
		                                    $modelScholarshipHolder->setAttribute('percentage_pay', $percentage_exempted);
											$modelScholarshipHolder->setAttribute('academic_year', $acad);
											$modelScholarshipHolder->setAttribute('create_by', currentUser()  );
										    $modelScholarshipHolder->setAttribute('date_created', date('Y-m-d') );
									  
									  if($modelScholarshipHolder->save() )
                                                                                    {
                                                                                        if($is_save)
                                                                                            {
                                                                                                $dbTrans->commit();
                                                                                            }
                                                                                             else
                                                                                                { $dbTrans->rollback();

                                                                                                 }
                                                                                    }
                                                                                  else
                                                                                    { $dbTrans->rollback();

                                                                                     }
                               	}   
                           
                           					            
					            
					            
					            
					            
					            				                      
					         } 
					             	//return $this->redirect(array('index_exempt'));
					      }
                       }
                
                 }
             //########################## FEN EXEMPTION #################################### 
              
             if(isset($_POST['create']))
		 {
                  		                  $save_ok = false;
				                          	
								if(($_POST['SrcBillings']['date_pay']!="")&&($_POST['SrcBillings']['date_pay']!='0000-00-00'))
								 {  
				 				 	$date_pay_ = $_POST['SrcBillings']['date_pay'];
								 	$modelAdd->setAttribute('date_pay', $date_pay_);
								 	if($modelAdd->payment_method!="")
									  {	
									 	
                                                                             //pou update 
                                                                             if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                                                                               {
                                                                                  $modelBilling = new SrcBillings;
                                                                             
                                                                               //return 0: false; 1: true
                                                                               $chekDepoUnique = $modelBilling->isChekDepoUnique($modelAdd->num_chekdepo,$currency_id);
                                                                           
                                                                            if($chekDepoUnique==0)
                                                                              {   
										
                                                                                         $amount_to_pay = 0;
                                                                                    $modelAdd->setAttribute('student',$this->old_student );

                                                                                           $amount_pay = $modelAdd->amount_pay; 
                                                                                           //$acad_year=$modelAdd->academic_year;

                                                                                           // $to_pay = Fees::model()->FindByPk($this->fee_period);

                                                                                    $balance = $modelAdd->amount_to_pay - $modelAdd->amount_pay;

                                                                                      $payment_method = $modelAdd->payment_method;

                                                                                      $comments = $modelAdd->comments;
                                                                                      $num_checkDepo = $modelAdd->num_chekdepo;
                                                                                      
                                                                                                                
                                                                                      if($modelAdd->date_pay =='0000-00-00')
                                                                                         $date_pay = date('Y-m-d');
                                                                                      else
                                                                                         $date_pay = $modelAdd->date_pay;

                                                                                        //$modelUp= new Billings;

                                                                                      // $modelUp=$this->findModel($_GET['bil']);

                                                                                    //si old > new =>  ajout (old - new) sur la balance de la table balance
                                                                                        //si old < new =>  soustr (new - old) sur la balance de la table balance

                                                                                       if($balance==0)
                                                                                         $modelUp->fee_totally_paid= 1;
                                                                                       else
                                                                                         $modelUp->fee_totally_paid= 0;

                                                                                           $modelUp->amount_pay= $amount_pay;
                                                                                           $modelUp->balance= $balance;

                                                                                           $modelUp->payment_method= $payment_method;
                                                                                           $modelUp->comments= $comments;
                                                                                            $modelUp->num_chekdepo= $num_checkDepo;
                                                                                           $modelUp->date_pay= $date_pay;

                                                                                        $modelUp->date_updated=date('Y-m-d');
                                                                                        $modelUp->updated_by=currentUser();


                                                                               if($modelUp->save())
                                                                                 { //$dbTrans->commit();  	
                                                                                   /* if( ($this->is_pending_balance==true)&&($balance<=0) ) //update pending_balance to paid
                                                                                        {
                                                                                           $command11 = Yii::$app->db->createCommand();
                                                                                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'student'=>$modelUp->student ])
                                                                                       ->execute();

                                                                                        }
                                                                                      elseif( ($this->is_pending_balance==true)&&($balance >0) ) //update pending_balance to not paid
                                                                                        {
                                                                                           $command11 = Yii::$app->db->createCommand();
                                                                                          $command11->update('pending_balance', ['is_paid' => 0,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$modelUp->student ])
                                                                                       ->execute();


                                                                                        }
                                                                                        */


                                                                                                        $new_balance = 0;
                                                                                    $date_pay = null;
                                                                                    $student = $modelUp->student;
                                                                                    $fee_period = $modelUp->fee_period;
                                                                                    $old_balance_ = $modelUp->balance;
                                                                                    $old_fee_period = $modelUp->fee_period;

                                                                                    $id_bill = $modelUp->id;


                                                                                            //cheche tout lot peman ki fet apre peman sila pou moun sa (date_pay DESC)
                                                                                            //$sql_1 = 'SELECT b.id, b.balance FROM billings b  INNER JOIN fees f ON(f.id=b.fee_period) INNER JOIN fees_label fl ON(fl.id=f.fee)  WHERE b.student='.$this->student_id.' AND fl.status='.$this->status_.' AND b.date_pay > \''.$modelUp->date_pay.'\'  order by b.id ASC';

                                                                                            $result1 = SrcBillings::find()
                                                                                             ->select(['billings.id','billings.balance'])
                                                                                             ->orderBy(['billings.id'=>SORT_ASC])
                                                                                             ->joinWith(['feePeriod','feePeriod.fee0',])
                                                                                             ->where(['billings.student'=>$this->student_id])
                                                                                             ->andWhere(['fees_label.status'=>$this->status_])
                                                                                             ->andWhere(['fees.devise'=>$currency_id])
                                                                                             ->andWhere(['>','billings.date_pay',$modelUp->date_pay])
                                                                                             ->all();


                                                                                            if($result1!=null) 
                                                                                              { 
                                                                                                $last_id_in_range ='';
                                                                                                $stop=false;

                                                                                                foreach($result1 as $bill)
                                                                                                  {  
                                                                                                     $id_bill = $bill->id;
                                                                                                     $last_id_in_range = $bill->id;

                                                                                                      $new_model=$this->findModel($bill->id);

                                                                                                      if(($balance!=0)&&($stop==true))
                                                                                                       {
                                                                                                               $balance_current_record = $new_model->balance;

                                                                                                               if($balance < 0)
                                                                                                                 {
											        	  	         /*  if( ($this->is_pending_balance==true) ) //update pending_balance to paid
																  {
                                                                                                                                        $command11 = Yii::$app->db->createCommand();
                                                                                                                                        $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devi$modelUpAddse'=>$currency_id,'student'=>$modelUpAdd->student ])
                                                                                                                                              ->execute();
																     }
											        	  	            */
                                                                                                                        $amount_to_pay = $new_model->amount_to_pay + $balance;
                                                                                                                        $new_balance1 = $amount_to_pay - $new_model->amount_pay;
											        	  	 
											        	   	          if($new_balance1<=0)
														  	   	         $new_model->fee_totally_paid= 1;
														  	   	      // else
														  	   	        // $new_model->setAttribute('fee_totally_paid', 0);
														  	   	    
															  	   	   $new_model->amount_to_pay= $amount_to_pay;
															  	   	   $new_model->balance= $new_balance1;
															  	   	    
												                         $new_model->updated_by = currentUser();
														                        $new_model->date_updated= date('Y-m-d');
														                        
                                                                                                                                        if($new_model->save())
                                                                                                                                          {
                                                                                                                                              unset($new_model); 
                                                                                                                                                   $stop=true;         
                                                                                                                                                 $balance = $new_balance1;  
												 	 	   	      	        
												 	 	   	      	               /*  if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
                                                                                                                                                            $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$modelUp->student ])
                                                                                                                                                         ->execute();
																  	   	          }
                                                                                                                                                          */
												 	 	   	      	          
														                            } 
							 	 	   	      	             
									        	  	
											        	  	         }
                                                                                                                    elseif($balance > 0)
                                                                                                                      {
                                                                                                                            if($new_model->fee_period == $old_fee_period)
                                                                                                                               { $amount_to_pay = $balance;
											        	   	          
                                                                                                                                  $new_balance1 = $amount_to_pay - $new_model->amount_pay;
											        	   	          
                                                                                                                                     if($new_balance1<=0)
														  	   	         $new_model->fee_totally_paid= 1;
														  	   	      // else
														  	   	        // $new_model->setAttribute('fee_totally_paid', 0);
														  	   	    
															  	   	   $new_model->amount_to_pay= $amount_to_pay;
															  	   	   $new_model->balance= $new_balance1;
															  	   	    
                                                                                                                                         $new_model->updated_by= currentUser();
														                        $new_model->date_updated= date('Y-m-d');
														                        
                                                                                                                                            if($new_model->save())
                                                                                                                                              {  unset($new_model); 
							 	 	   	      	                        
                                                                                                                                                  $balance = $new_balance1;
											        	   	               
                                                                                                                                                   /*  if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
                                                                                                                                                                $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$modelUp->student ])
                                                                                                                                                             ->execute();
																  	   	          }
                                                                                                                                                          */
																  	   	             
                                                                                                                                                }
											        	   	        
					
                                                                                                                                }
                                                                                                                            else
                                                                                                                               {  //fe yon nouvo anrejistreman pou fin peye fre sa
                                                                                                                                    $amount_to_pay =  $balance;
                                                                                                                                    $amount_pay =  $balance;
                                                                                                                                    $date_pay = $new_model->date_pay;
                                                                                                                                     $fee_totally_paid = 1;
								  	   	    
                                                                                                                                        $new_balance1 = 0;

                                                                                                                                        $payment_method = $new_model->payment_method;

                                                                                                                                           unset($modelUpBillings_); 
                                                                                                                                        $modelUpBillings_= new SrcBillings; 
							 	 	   	      	                            
                                                                                                                                           $modelUpBillings_->fee_totally_paid = $fee_totally_paid;
														  	   	       
															  	   	   $modelUpBillings_->amount_to_pay= $amount_to_pay;
															  	   	   $modelUpBillings_->amount_pay= $amount_pay;
															  	   	   $modelUpBillings_->balance= $new_balance1;
															  	   	   $modelUpBillings_->payment_method= $payment_method;
															  	   	   $modelUpBillings_->date_pay= $date_pay;
															  	   	    
                                                                                                                                          $modelUpBillings_->created_by=currentUser();
                                                                                                                                          $modelUpBillings_->date_created= date('Y-m-d');
														                        
                                                                                                                                         if( $modelUpBillings_->save())
                                                                                                                                            {
                                                                                                                                                      $balance = $new_balance1;
											        	   	               
                                                                                                                                                  /*  if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
                                                                                                                                                                $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$modelUp->student ])
                                                                                                                                                             ->execute();
																  	   	          }
                                                                                                                                                          */
							 	 	   	      	               
                                                                                                                                                     $old_fee_period = $new_model->fee_period;
											        	   	      	        
                                                                                                                                            }
									  	   	                             
                                                                                                                                      }
                                                                                                                        }
											                   }    
                                                                                                        else
                                                                                                           {      
                                                                                                                     //pran balans lan retire l nan tab balance lan aavan delete
                                                                                                                 $balance_current_record = $new_model->balance;
                                                                                                                     $new_model->delete();

                                                                                                                 }
											         
											            }
											 
											 
                                                                                                        $modelUpBill = SrcBillings::findOne($last_id_in_range);

                                                                                                        if($balance>0)
                                                                                                         { 
                                                                                                            if($modelUpBill != null)
                                                                                                             {	
                                                                                                                $modelUpBill->fee_totally_paid= 0;
                                                                                                                $modelUpBill->save(); 
                                                                                                              }
                                                                                                          }
								  	   	       
								  	   	       
								  	   	              
                                                                                                /*        $new_balance = 0;
                                                                                                        $bal = 0;

                                                                                                           $modelUpBalance= SrcBalance::find()
                                                                                                               ->where('student='.$modelUp->student.' AND devise='.$currency_id)
                                                                                                               ->all();             
					                        
                                                                                                    if($modelUpBalance!=null)
                                                                                                       {   
                                                                                                            foreach($modelUpBalance as $modelUpBalance)
                                                                                                            {
                                                                                                                if($balance_current_record > $balance)
                                                                                                                    { $new_balance = $balance_current_record - $balance;
                                                                                                                      $bal = $modelUpBalance->balance - $new_balance;

                                                                                                                        if($bal>=0)
                                                                                                                           {	 // nan balanse total la
                                                                                                                                                             $modelUpBalance->balance= $bal;
                                                                                                                                                             $modelUpBalance->save();  
                                                                                                                           }
                                                                                                                         else
                                                                                                                         {	 // nan balanse total la
                                                                                                                                                             $modelUpBalance->balance= 0;
                                                                                                                                                             $modelUpBalance->save();  
                                                                                                                           }

                                                                                                                    }
                                                                                                                 elseif($balance_current_record < $balance)
                                                                                                                    {   $new_balance = $balance - $balance_current_record;
                                                                                                                       $bal = $modelUpBalance->balance + $new_balance;

                                                                                                                            if($bal>=0)
                                                                                                                               {// nan balanse total la
                                                                                                                                 $modelUpBalance->balance= $bal;
                                                                                                                                 $modelUpBalance->save();   

                                                                                                                                }
                                                                                                                             else
                                                                                                                               {// nan balanse total la
                                                                                                                                 $modelUpBalance->balance= 0;
                                                                                                                                 $modelUpBalance->save();   

                                                                                                                                }

                                                                                                                    }


                                                                                                             }  


                                                                                                         }
                                                                                                        else
                                                                                                           {
                                                                                                               if($balance>0)
                                                                                                                 {
                                                                                                                    $new_modelBalance= new SrcBalance;

                                                                                                                  $new_modelBalance->student=$modelUp->student;
                                                                                                                  $new_modelBalance->balance= $balance;
                                                                                                                  $new_modelBalance->devise = $currency_id;
                                                                                                                  $new_modelBalance->date_created= date('Y-m-d');

                                                                                                                   $new_modelBalance->save();
                                                                                                                 }

                                                                                                            }
                                                                                                            */
						                             
								  	   	        
											 
											 
                                                                                                }     
                                                                                             else
                                                                                              {     
                                                                                                        $modelUpBill= SrcBillings::findOne($modelUp->id);
                                                                                                        if($modelUp->balance>0)
                                                                                                         { 
                                                                                                           if($modelUpBill != null)
                                                                                                            {	
                                                                                                               $modelUpBill->fee_totally_paid= 0;
                                                                                                               $modelUpBill->save(); 
                                                                                                             }

                                                                                                          } 



                                                                                                       /*     $new_balance = 0;
                                                                                                              $bal = 0;

                                                                                                                      $modelUpBalance= SrcBalance::find()
                                                                                                                        ->where('student='.$modelUp->student.' And devise='.$currency_id)
                                                                                                                        ->all();             

                                                                                                            if($modelUpBalance!=null)
                                                                                                              {   
                                                                                                                foreach($modelUpBalance as $modelUpBalance)
                                                                                                                {
                                                                                                                    if($balance_current_record > $modelUp->balance)
                                                                                                                     { $new_balance = $balance_current_record - $modelUp->balance;
                                                                                                                       $bal = $modelUpBalance->balance - $new_balance;

                                                                                                                        if($bal>=0)
                                                                                                                          {                       
                                                                                                                            // nan balanse total la
                                                                                                                            $modelUpBalance->balance= $bal;
                                                                                                                            $modelUpBalance->save();
                                                                                                                          }
                                                                                                                        else
                                                                                                                          {                       
                                                                                                                            // nan balanse total la
                                                                                                                            $modelUpBalance->balance= 0;
                                                                                                                            $modelUpBalance->save();
                                                                                                                          }

                                                                                                                      }
                                                                                                                   elseif($balance_current_record < $modelUp->balance)
                                                                                                                      {   $new_balance = $modelUp->balance - $balance_current_record;
                                                                                                                          $bal = $modelUpBalance->balance + $new_balance;

                                                                                                                         if($bal>=0)
                                                                                                                          {
                                                                                                                          // nan balanse total la
                                                                                                                            $modelUpBalance->balance= $bal;
                                                                                                                            $modelUpBalance->save();
                                                                                                                          }
                                                                                                                         else
                                                                                                                           {
                                                                                                                          // nan balanse total la
                                                                                                                            $modelUpBalance->balance= 0;
                                                                                                                            $modelUpBalance->save();
                                                                                                                          }


                                                                                                                      }

                                                                                                                 }

                                                                                                                }
                                                                                                              else
                                                                                                                 {
                                                                                                                     if($modelUp->balance >0)
                                                                                                                          {  $new_modelBalance= new SrcBalance;

                                                                                                                         $new_modelBalance->student=$modelUp->student;
                                                                                                                         $new_modelBalance->balance= $modelUp->balance;
                                                                                                                         $new_modelBalance->devise = $currency_id;
                                                                                                                         $new_modelBalance->date_created= date('Y-m-d');

                                                                                                                          $new_modelBalance->save();

                                                                                                                          }

                                                                                                                   }
                                                                                                        */



                                                                                                }


                                                                                               $new_balance1 = $old_balance_;     


                                                                                                    return $this->redirect(['view','id'=>$modelUp->student,'ri'=>$_GET['ri'],'part'=>'rec']);  	                                


                                                                                  }
                                                                               else
                                                                                 { //   $dbTrans->rollback();
                                                                                   }							       
                                                                              
                                                                               }
                                                                            elseif($chekDepoUnique==1)
                                                                              {
                                                                                            Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','# check/deposit').' '.Yii::t('app','is not unique')  ),
														    'title' => Html::encode(Yii::t('app','Info') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
                                                                              }
							              
                                                                          }//fen pou update
                                                                       else  //pou create
                                                                         {
                                                                           if($modelAdd->amount_pay!=null)
                                                                           {
                                                                             $modelBilling = new SrcBillings;
                                                                             
                                                                               //return 0: false; 1: true
                                                                               $chekDepoUnique = $modelBilling->isChekDepoUnique($modelAdd->num_chekdepo,$currency_id);
                                                                           
                                                                            if($chekDepoUnique==0)
                                                                              {   
										//tcheke si yo peye fre sa nan date_pay sa deja
										$modelBillings_datePay=Billings::find()
										                        ->orderBy(['id'=>SORT_DESC])
										                        ->where(['student'=>$this->student_id])
										                        ->andWhere(['fee_period'=>$this->fee_period])
										                        ->andWhere(['academic_year'=>$acad])
										                        ->andWhere(['date_pay'=>$date_pay_])
										                        ->all();
										            
									          if($modelBillings_datePay==null)
										    {
										
											 $condition_fee_status = ['fees_label.status'  => $this->status_];
											//return an Integer (id)
											$last_payment_transaction_id = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
				
											
											$fee_period_r = $_POST['SrcBillings']['fee_period'];
											$amount_pay = $modelAdd->amount_pay; 
											//$acad_sess_year=$model->academic_year;
                                                                                        $num_checkDepo = $modelAdd->num_chekdepo;
											
											//$to_pay = Fees::model()->FindByAttributes(array('id'=>$fee_period_r));
											
											if($this->special_payment)
											  $amount_2p = $modelAdd->amount_pay;
											else
											   $amount_2p = $modelAdd->amount_to_pay;
															   
										
                                                                                            //tcheke balans pou peryod sa lap peye pou li a
                                                                                            $there_is_balance=$modelBilling->checkForBalance($this->student_id, $this->fee_period, $this->status_, $acad);
					
										           if(isset($there_is_balance)&&($there_is_balance!=null))
										             { 
                                                                                                    $id_bill_transaction ='';
                                                                                                    foreach($there_is_balance as $balance_there)
                                                                                                          $id_bill_transaction = $balance_there->id;

                                                                                                        $new_model_billing =SrcBillings::findOne($id_bill_transaction);

                                                                                                        $amount_to_pay = 0;

                                                                                                        if($to_pay!=null)
                                                                                                            $amount_to_pay = $amount_2p;
													
												if($new_model_billing->amount_pay==0)// update this record made by SYGES
												 {
												 	
												  	   if($this->special_payment)
												  	     {    if( ($amount_pay-$this->remain_balance) == 0)
														  	    $balance = 0;
														  	 else
														  	   $balance = $this->remain_balance;// + $amount_pay;
														  	
												  	      }
												  	   else
												  	      $balance = $amount_to_pay - $amount_pay;  
												  	   	
												  	   /*	if( ( ($modelAdd->payment_method==3)||($modelAdd->payment_method==4) )&&($modelAdd->num_chekdepo==null))
                                                                                                                  {
                                                                                                                            Yii::$app->getSession()->setFlash('Warning', [
                                                                                                                            'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                                                            'duration' => 12000,
                                                                                                                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                                                            'message' => Html::encode(Yii::t('app','# check/deposit cannot be null; or choose an other payment method.') ),
                                                                                                                            'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                                                            'positonY' => 'top',   //   top,//   bottom,//
                                                                                                                            'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                                                        ]);
                                                                                                                  }
                                                                                                               else
                                                                                                                { 
                                                                                                              
                                                                                                             */
                                                                                                              
                                                                                                                 
												  	   	  $payment_method = $modelAdd->payment_method;
												  	   	  $comments = $modelAdd->comments;
												  	   	  $date_pay = $modelAdd->date_pay;
												  	   	  
												  	   	  if($balance==0)
												  	   	    $new_model_billing->fee_totally_paid= 1;
												  	   	       
												  	   	      
												  	   	   
												  	   	   //$new_model_billing->amount_to_pay', $amount_to_pay);
												  	   	   $new_model_billing->amount_pay=$amount_pay;
												  	   	   $new_model_billing->balance= $balance;
												  	   	   $new_model_billing->date_pay=$date_pay;
												  	   	   
												  	   	   $new_model_billing->payment_method=$payment_method;
                                                                                                                   $new_model_billing->num_chekdepo= $num_checkDepo;
												  	   	   $new_model_billing->comments=$comments;
												  	   	   $new_model_billing->setAttribute('academic_year', $acad);
									                        
                                                                                                                     $new_model_billing->updated_by=currentUser();
                                                                                                                     $new_model_billing->date_updated= date('Y-m-d');


                                                                                                                      $ID = '';

                                                                                                                       if($new_model_billing->save())
                                                                                                                         {  
                                                                                                                                //fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
                                                                                                                                //$model_billing_anvan =$this->findModel($last_payment_transaction_id);
                                                                                                                                     $command = Yii::$app->db->createCommand();
                                                                                                                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
                                                                                                                                       ->execute();


                                                                                                                                // fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
                                                                                                                                if($balance==0)
                                                                                                                                  {  $command = Yii::$app->db->createCommand();
                                                                                                                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$new_model_billing->fee_period,'student'=>$model->student ])
                                                                                                                                       ->execute();


                                                                                                                               /*  if($this->is_pending_balance==true) //update pending_balance to paid
                                                                                                                                         {
                                                                                                                                                $command11 = Yii::$app->db->createCommand();
                                                                                                                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id, 'student'=>$model->student ])
                                                                                                                                       ->execute();

                                                                                                                                          }
                                                                                                                                * 
                                                                                                                                */

                                                                                                                                     }

                                                                                                                                $modelAdd->balance = $new_model_billing->balance;
                                                                                                                                $ID = $new_model_billing->id;
                                                                                                                                $save_ok = true;  
                                                                                                                          }
                                                                                                                          
                                                                                                                   //  }
												          
												  }
												else
												   {	
												  	   	  if($new_model_billing->balance > 0)
												  	   	    {
												  	   	    	if($this->special_payment)
														  	      {  if( ($amount_pay-$this->remain_balance) == 0)
														  	             $balance = 0;
														  	         else
														  	            $balance = $this->remain_balance;// + $amount_pay;
														  	       }
														  	    else
														  	      $balance = $new_model_billing->balance - $amount_pay;
												  	   	    }
												  	   	  else
												  	   	    {
												  	   	      if($this->special_payment)
														  	     {  if( ($amount_pay-$this->remain_balance) == 0)
														  	             $balance = 0;
														  	        else
														  	            $balance = $this->remain_balance;// + $amount_pay;
														  	            
														  	      }
														  	    else
														  	      $balance = $new_model_billing->balance + $amount_pay;
												  	   	    }
												  	   	
                                                                                                              /*  if( ( ($modelAdd->payment_method==3)||($modelAdd->payment_method==4) )&&($modelAdd->num_chekdepo==null))
                                                                                                                  {
                                                                                                                            Yii::$app->getSession()->setFlash('Warning', [
                                                                                                                            'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                                                            'duration' => 12000,
                                                                                                                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                                                            'message' => Html::encode(Yii::t('app','# check/deposit cannot be null; or choose an other payment method.') ),
                                                                                                                            'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                                                            'positonY' => 'top',   //   top,//   bottom,//
                                                                                                                            'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                                                        ]);
                                                                                                                  }
                                                                                                               else 
                                                                                                                {
												  	   	*/  
                                                                                                                   $payment_method = $modelAdd->payment_method;
												  	   	  $comments = $modelAdd->comments;
												  	   	  $date_pay = $modelAdd->date_pay;
												  	   	  
												  	   	   if($balance==0)
												  	   	     $modelAdd->setAttribute('fee_totally_paid', 1);
												  	   	   
												  	   	   $modelAdd->setAttribute('amount_to_pay', $amount_to_pay); 
												  	   	   $modelAdd->setAttribute('amount_pay', $amount_pay);
												  	   	   $modelAdd->setAttribute('balance', $balance);
												  	   	   $modelAdd->setAttribute('date_pay', $date_pay);
                                                                                                                   $modelAdd->setAttribute('num_chekdepo', $num_checkDepo);
												  	   	   
												  	   	   $modelAdd->setAttribute('payment_method', $payment_method);
												  	   	   $modelAdd->setAttribute('comments', $comments);
												  	   	   $modelAdd->setAttribute('academic_year', $acad);
									                        
                                                                                                                     $modelAdd->setAttribute('created_by', currentUser() );
                                                                                                                     $modelAdd->setAttribute('date_created', date('Y-m-d'));



                                                                                                                      $ID = '';

                                                                                                                       if($modelAdd->save())
                                                                                                                        {  
                                                                                                                               //fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
                                                                                                                               //$model_billing_anvan =$this->findModel($last_payment_transaction_id);
                                                                                                                                    $command_ = Yii::$app->db->createCommand();
                                                                                                                                    $command_->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
                                                                                                                                      ->execute();


                                                                                                                              // fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
                                                                                                                               if($balance==0)
                                                                                                                                 {  $command = Yii::$app->db->createCommand();
                                                                                                                                    $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$modelAdd->fee_period,'student'=>$model->student ])
                                                                                                                                      ->execute();

                                                                                                                                   /*   if($this->is_pending_balance==true) //update pending_balance to paid
                                                                                                                                        {
                                                                                                                                               $command11 = Yii::$app->db->createCommand();
                                                                                                                                                $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$modelAdd->student ])
                                                                                                                                               ->execute();
                                                                                                                                         }
                                                                                                                                    * 
                                                                                                                                    */

                                                                                                                                  }


                                                                                                                               $ID = $modelAdd->id;
                                                                                                                               $save_ok = true;  
                                                                                                                         } 
                                                                                                                         
                                                                                                                     // }
												          
												    }
										  	        
										              }
										           else
										             {
                                                                                                    $amount_to_pay =0;

                                                                                                    if($to_pay!=null)
                                                                                                       $amount_to_pay = $amount_2p;
										    
										                       if($this->special_payment)
													  {       if( ($amount_pay-$this->remain_balance) == 0)
                                                                                                                    $balance = 0;
                                                                                                                 else
                                                                                                                   $balance = $this->remain_balance;// + $amount_pay;
													  	   
													   }
													 else
														$balance = $amount_to_pay - $amount_pay;
												  	   	
                                                                                                          /*    if( ( ($modelAdd->payment_method==3)||($modelAdd->payment_method==4) )&&($modelAdd->num_chekdepo==null))
                                                                                                                  {
                                                                                                                            Yii::$app->getSession()->setFlash('Warning', [
                                                                                                                            'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                                                            'duration' => 12000,
                                                                                                                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                                                            'message' => Html::encode(Yii::t('app','# check/deposit cannot be null; or choose an other payment method.') ),
                                                                                                                            'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                                                            'positonY' => 'top',   //   top,//   bottom,//
                                                                                                                            'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                                                        ]);
                                                                                                                  }
                                                                                                               else
                                                                                                                {
												  	   	  
                                                                                                              */     
                                                                                                                   $payment_method = $modelAdd->payment_method;
												  	   	  $comments = $modelAdd->comments;
												  	   	  $date_pay = $modelAdd->date_pay;
												  	   	  
												  	   	   if($balance==0)
												  	   	    $modelAdd->setAttribute('fee_totally_paid', 1);
												  	   	    
												  	   	   $modelAdd->setAttribute('amount_to_pay', $amount_to_pay);
												  	   	   $modelAdd->setAttribute('amount_pay', $amount_pay);
												  	   	   $modelAdd->setAttribute('balance', $balance);
												  	   	   $modelAdd->setAttribute('date_pay', $date_pay);
                                                                                                                   $modelAdd->setAttribute('num_chekdepo', $num_checkDepo);
												  	   	   
												  	   	   $modelAdd->setAttribute('payment_method', $payment_method);
												  	   	   $modelAdd->setAttribute('comments', $comments);
												  	   	   $modelAdd->setAttribute('academic_year', $acad);
									                         
									                                          $modelAdd->setAttribute('created_by', currentUser() );
											                          $modelAdd->setAttribute('date_created', date('Y-m-d'));
											              
                                                                                                                    //si t gen rezevasyon   
                                                                                                                      if($this->reservation==true)
                                                                                                                        { if( ($this->message_paymentAllow == true)&&($this->special_payment == false) )
                                                                                                                                $modelAdd->setAttribute('reservation_id', $this->id_reservation);
                                                                                                                         }
										     
										     
                                                                                                                     $ID = '';

                                                                                                                  if($modelAdd->save())
                                                                                                                    { 
                                                                                                                           //si t gen rezevasyon   #################################
                                                                                                                            if($this->reservation==true)
                                                                                                                              {  
                                                                                                                                    /*if( ($this->message_paymentAllow == true)&&($this->special_payment == false) )
                                                                                                                                      { //anrejistre reservation ki te fet la
                                                                                                                                        //load appropriate info reservation 
                                                                                                                                                   $reservationInfo = Reservation::findOne($this->id_reservation);

                                                                                                                                                   $modelFeeInfo = Fees::findOne($this->fee_period);

                                                                                                                                                   $__balance = $modelFeeInfo->amount-$this->amount_reservation;

                                                                                                                                                   $model_new = new Billings;

                                                                                                                                                   if($model->balance==0)
                                                                                                                                                     $model_new->setAttribute('fee_totally_paid', 1);
                                                                                                                                                   else
                                                                                                                                                     $model_new->setAttribute('fee_totally_paid', 0);

                                                                                                                                                                    $model_new->setAttribute('student', $modelAdd->student );
                                                                                                                                                                    $model_new->setAttribute('fee_period', $modelAdd->fee_period );
                                                                                                                                                                    $model_new->setAttribute('amount_to_pay', $modelFeeInfo->amount );
                                                                                                                                                                    $model_new->setAttribute('amount_pay', $this->amount_reservation );
                                                                                                                                                                    $model_new->setAttribute('balance', $__balance );
                                                                                                                                                                    $model_new->setAttribute('payment_method', $reservationInfo->payment_method );
                                                                                                                                                                    $model_new->date_pay=$reservationInfo->payment_date;
                                                                                                                                                                    $model_new->setAttribute('comments', $reservationInfo->comments );
                                                                                                                                                                    $model_new->setAttribute('academic_year', $acad);

                                                                                                                                                                     $model_new->setAttribute('created_by', currentUser() );
                                                                                                                                         $model_new->setAttribute('date_created', date('Y-m-d'));

                                                                                                                                             $model_new->save();

                                                                                                                                       }
                                                                                                                                      */

                                                                                                                                     $command_11 = Yii::$app->db->createCommand();
                                                                                                                                      $command_11->update('reservation', ['already_checked' => 1,   ], ['id'=>$this->id_reservation ])
                                                                                                                                           ->execute();

                                                                                                                                }

                                                                                                                              //##########################################

                                                                                                                                    //fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
                                                                                                                                    //$model_billing_anvan =$this->findModel($last_payment_transaction_id);
                                                                                                                                         $command_ = Yii::$app->db->createCommand();
                                                                                                                                         $command_->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
                                                                                                                                           ->execute();

                                                                                                                                    // fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
                                                                                                                                    if($balance==0)
                                                                                                                                      {  $command = Yii::$app->db->createCommand();
                                                                                                                                         $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$modelAdd->fee_period,'student'=>$modelAdd->student ])
                                                                                                                                           ->execute();

                                                                                                                                        /*  if($this->is_pending_balance==true) //update pending_balance to paid
                                                                                                                                             {
                                                                                                                                                    $command11 = Yii::$app->db->createCommand();
                                                                                                                              $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id, 'student'=>$modelAdd->student ])
                                                                                                                                           ->execute();
                                                                                                                                              }
                                                                                                                                         * 
                                                                                                                                         */

                                                                                                                                       }


                                                                                                                           $ID = $modelAdd->id;
                                                                                                                           $save_ok = true;
                                                                                                                      }
                                                                                                                      
                                                                                                                // }
										 
										             }     
							                      
							                  
							                       
											 	       if($save_ok)
													 {  
														//update tab balance lan
													  /*	
                                                                                                                $modelBalance_=SrcBalance::find()
                                                                                                                ->where('student='.$modelAdd->student.' AND devise='.$currency_id)
                                                                                                                ->all();
								                                        
                                                                                                             if($modelBalance_!=null)
                                                                                                               {   
										                           	   $id_modelBalance=0;
										                           	   
                                                                                                                 foreach($modelBalance_ as $modelBalance)
                                                                                                                   {  	     //$modelBalance = $modelBal;
										                           	     
										                           	      $id_modelBalance = $modelBalance->id;
										                           	 
										                           	       $new_model_balance = SrcBalance::findOne($id_modelBalance);
											                           	       
                                                                                                                      if($this->status_ == 1) 
                                                                                                                        {
											                           	  if($modelAdd->balance == 0) //delete  $modelBalance
												                            {	
												                            	$new_balance_1= 0;
												                            	//$modelBalance->delete();
												                            	
												                            	if($modelBalance->balance >= $modelAdd->amount_pay)
												                            	  { $new_balance_1= $modelBalance->balance - $modelAdd->amount_pay;
												                            	     
												                            	     $new_model_balance->setAttribute('balance',  $new_balance_1);
																			       	    
																			       	     $new_model_balance->save();
												                            	  }
												                            	else
												                            	  {   $new_balance_1= 0;
												                            	
												                            	     $new_model_balance->setAttribute('balance',  $new_balance_1);
																			       	    
																			       	     $new_model_balance->save();
												                            	  	
												                            	    }
												                          
												                             }
												                            elseif($modelAdd->balance != 0) //$model->balance se nouvo balans lan
												                            {  
																	       	    $new_balance_1= 0;
												                            	//$modelBalance->delete();
												                            	if($modelBalance->balance >= $modelAdd->amount_pay)
												                            	   $new_balance_1= $modelBalance->balance - $modelAdd->amount_pay;
												                            	//else
												                            	//   $new_balance_1= $modelBalance->balance + $modelAdd->amount_pay;
												                            	
																	       	    $new_model_balance->setAttribute('balance',  $new_balance_1);
																	       	    
																	       	     $new_model_balance->save();
												                            	
												                              }	 
												                              
                                                                                                                        }
                                                                                                                      elseif($this->status_ == 0)
                                                                                                                        {
										                           	   	  if($modelAdd->balance != 0) //$model->balance se nouvo balans lan
												                            {  
																	       	   
																	       	   // on ajoute 
												                            	  
																			       	    $new_balance1= $modelBalance->balance + $modelAdd->balance;
																			       	    $new_model_balance->setAttribute('balance',  $new_balance1);
																			       	    
																			       	     $new_model_balance->save();
												                            	   
												                              }	 
												                              
										                           	        }
																		
										                                   }
																		
                                                                                                                }
                                                                                                              else // add new balance record in balance table
                                                                                                               {   

                                                                                                                       if($modelAdd->balance != 0) 
                                                                                                                         {
                                                                                                                                     $modelBalance= new SrcBalance();

                                                                                                                           $modelBalance->setAttribute('student',$modelAdd->student);
                                                                                                                           $modelBalance->setAttribute('balance', $modelAdd->balance);
                                                                                                                           $modelBalance->setAttribute('devise', $currency_id);
                                                                                                                           $modelBalance->setAttribute('date_created', date('Y-m-d'));

                                                                                                                            $modelBalance->save();
                                                                                                                         }


                                                                                                               }
										                         */
										                         
								                      					
														return $this->redirect(['view','id'=>$modelAdd->student,'ri'=>$this->recettesItems,'part'=>'rec']);
														
														
													  }  
									
							                            }
									         else
									           {  $this->message_2paymentFeeDatepay=true;
									           }
                                                                                   
                                                                              }
                                                                            elseif($chekDepoUnique==1)
                                                                              {
                                                                                            Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','# check/deposit').' '.Yii::t('app','is not unique')  ),
														    'title' => Html::encode(Yii::t('app','Info') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
                                                                              }
                                                                              
                                                                           }//fen si amount_pay pa null
								     
                                                                         }//fen pou create
                                                                                 
                                                                                   
                                                                                   
                                                                                   
							               }
								         else
								           { $this->message_paymentMethod=true;
								           }
									 }
									else
							           { $this->message_datepay=true;
							           }
						           
			                    }
                        
                                      }
  //########################################################################################
                       
                       
	       }
	   
       
       
	       return $this->render('view', [
            'model' => $model,
            'modelAdd' => $modelAdd,
             'modelExempt'=>  $modelExempt,
             'modelFee'=>$modelFee,    
            'status_'=>$this->status_, 
            'recettesItems'=>$this->recettesItems,
            'json_all_students'=>$json_all_students,
               'id_reservation'=>$this->id_reservation,
                'internal'=>$this->internal,
                'is_pending_balance'=>$this->is_pending_balance,
                'fee_period'=>$this->fee_period,
                'message_positiveBalance'=>$this->message_positiveBalance,
                'amount_reservation'=>$this->amount_reservation,
                'remain_balance'=>$this->remain_balance,
                'special_payment'=>$this->special_payment,
                'message_paymentAllow'=>$this->message_paymentAllow,
                'message_2paymentFeeDatepay'=>$this->message_2paymentFeeDatepay,
        ]);
        
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Creates a new Billings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
    	if(Yii::$app->user->can('billings-billings-create'))
         {
    	
    	$this->status_ = 1;
    	$acad = Yii::$app->session['currentId_academic_year'];
    	
    	$modelAcad = new SrcAcademicperiods;
    	 $previous_year= $modelAcad->getPreviousAcademicYear($acad);
    	
        $model = new SrcBillings();
         
        $reservationInfo = new SrcReservation;
		
		$this->message_paymentAllow=true;
		$this->message_positiveBalance=false;
		$this->message_2paymentFeeDatepay=false;
		
		$this->full_scholarship = false;
		$this->special_payment = false;
		
		$this->is_pending_balance =false;
		
		$level= 0;
		
		$program= 0;
		
		$modelCurrency = getDefaultCurrency(); 
		if($modelCurrency!=null)
		   $currency_id = $modelCurrency->id; 
		else
		   $currency_id = null;
		
		//$this->status_ = 1;
		 
	  /*	
		if(isset($_POST['Billings']['recettesItems']))
		  { $this->recettesItems = $_POST['Billings']['recettesItems'];
		       $this->fee_period ='';
		       $model->setAttribute('fee_period',0);
		   }
		else
		     {
		     	 if(isset($_GET['ri']) )
		     	   $this->recettesItems = $_GET['ri']; 
		       }
		
		if($this->recettesItems==0)
		    {
		        $this->status_ = 1;
		 
		      }
		   elseif($this->recettesItems==1)
		    {
		        $this->status_ = 0;
		
		      }
		   elseif($this->recettesItems==2)
		    {
		        $this->redirect(array('/billings/otherincomes/create?ri=2&from=b'));
		
		      }
		    elseif($this->recettesItems==3)
		    {
		        $this->redirect(array('/billings/enrollmentincome/create?part=rec&ri=3&from=b'));
		
		      }
		    elseif($this->recettesItems==4)
		    {
		        $this->redirect(array('/billings/reservation/create?part=rec&ri=4&from=b'));
		
		      }   
        */

       
	       if($model->load(Yii::$app->request->post()) ) 
			 {
				    
				    if(isset($_GET['stud'])&&($_GET['stud']!='')) 
				        { Yii::$app->session['Student_billings'] = $_GET['stud'];
				          $model->student = $_GET['stud'];  
				        }	
				         
				         				    
					 $this->student_id = Yii::$app->session['Student_billings'];
					 
					 
					 //get level and program for this student
				    if($model->student!=null)
				      {  
				      	$level=getLevelByStudentId($model->student);
				      	$program=getProgramByStudentId($model->student);
				      	
				       }
		
					 
					 if($model->student != $this->student_id)
					   {  
					   	$data_fees=null;
					     	 
				   	      //gad si elev la gen balans ane pase ki poko peye
		   	                 $modelPendingBal=SrcPendingBalance::find()
		   	                                     ->select(['id', 'balance'])
		   	                                     ->where(['student'=>$model->student])
		   	                                     ->andWhere(['is_paid'=>0])
		   	                                     ->andWhere(['academic_year'=>$previous_year])
		   	                                     ->all();
													 
					//si gen pending, ajoutel nan lis apeye a			
							if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
							  {
								  foreach($modelPendingBal as $bal)
								     {			     	 
										$this->is_pending_balance =true;
										
										$data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	               ->andWhere(['devise'=>$bal->devise])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
																			
							     		}
							     
							  }
							else
							   {
							   $this->is_pending_balance =false;
							   	 
							   	 $data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['not like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$currency_id])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
							   	
							   	
							   	}
							   	
							   	
						
						
						if($data_fees!=null)
						 {  
						 	foreach($data_fees as $fee)
						 	 {     
						 	 	$fee_ = $fee->id;
						 	 	
						 	 	 $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$model->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
							     if($modelBillings_balance0==null)
							 	  { $this->fee_period = $fee_;
										 	    
									$model->fee_period= [$fee_];
							 	 
							 	 	break;
							 	  }
						 	  
						 	  }
						 	 	
						   }
						 else
						    $this->fee_period = '';
								 	
					   
								
		     	
					$this->student_id= $model->student;
					
					  unset(Yii::$app->session['Student_billings']);
		                      Yii::$app->session['Student_billings'] = $this->student_id;
			    }
			  else
			   {
			   	   //$this->fee_period = $model->fee_period;
			   	      
			   	      //gad si elev la gen balans ane pase ki poko peye
	   	                 $modelPendingBal=SrcPendingBalance::find()
		   	                                     ->select(['id', 'balance'])
		   	                                     ->where(['student'=>$model->student])
		   	                                     ->andWhere(['is_paid'=>0])
		   	                                     ->andWhere(['academic_year'=>$previous_year])
		   	                                     ->all();
		   	                                     
		   	                                    
				          //si gen pending, ajoutel nan lis apeye a			
							if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
							  {
								  foreach($modelPendingBal as $bal)
								     {			     	 
										$this->is_pending_balance =true;
										
										$data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$bal->devise])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
																			
							     		}
							     
							  }
							else
							   {
							   $this->is_pending_balance =false;
							   	 
							   	 $data_fees = SrcFees::find()
							   	             ->orderBy(['date_limit_payment'=>SORT_ASC])
							   	             ->joinWith(['fee0',])
							   	             ->where(['academic_period'=>$acad])
							   	             ->andWhere(['not like','fees_label.fee_label','Pending balance'])
							   	              ->andWhere(['fees_label.status'=>$this->status_])
							   	              ->andWhere(['devise'=>$currency_id])
							   	              ->andWhere(['program'=>$program])
							   	              ->andWhere(['level'=>$level])
							   	             ->all();
							   	
							   	
							   	}
							   	

						
						if($data_fees!=null)
						  {
						  	    if(isset($_POST['SrcBillings']['fee_period']))
						  	      { //$this->fee_period = $model->fee_period;
						  	      
						  	          
						  	        
						  	         $this->fee_period = $_POST['SrcBillings']['fee_period'];
						  	        
						  	        if($this->fee_period =='')
						  	          {
						  	          	foreach($data_fees as $fee)
									 	 { 
									 	 		$fee_ = $fee->id;
									 	 		
									 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$model->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
											     if($modelBillings_balance0==null)
											 	  { $this->fee_period = $fee_;
														 	    
													$model->fee_period= [$fee_];
											 	 
											 	 	break;
											 	  }
						 	  
						 	                    
									 	  
									 	   }
								 	   
						  	          	}
						  	          	
						  	       
						  	       
						  	       }
						  	    else
						  	      {
						  	      	foreach($data_fees as $fee)
								 	 { 
								 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$model->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
										     if($modelBillings_balance0==null)
										 	  { $this->fee_period = $fee_;
													 	    
												$model->fee_period= [$fee_];
										 	 
										 	 	break;
										 	  }
									 	  								 	  
								 	   }
								 	   
			
								 	   
						  	      	}
						  	}
						else
						 { 	
						     if(isset($_POST['SrcBillings']['fee_period']))
						  	      { //$this->fee_period = $model->fee_period;
						  	         
						  	        
						  	         $this->fee_period = $_POST['SrcBillings']['fee_period'];
						  	        
						  	        if($this->fee_period =='')
						  	          {
						  	          	foreach($data_fees as $fee)
									 	 { 
									 	 		$fee_ = $fee->id;
									 	 		
									 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$model->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
											     if($modelBillings_balance0==null)
											 	  { $this->fee_period = $fee_;
														 	    
													$model->fee_period= [$fee_];
											 	 
											 	 	break;
											 	  }
						 	  
						 	                    
									 	  
									 	   }
								 	   
						  	          	}
						  	       
						  	       }
						  	    else
						  	      {
						  	      	/*
						  	      	foreach($data_fees as $fee)
								 	 { 
								 	 	  $modelBillings_balance0=SrcBillings::find()
						 	 	                          ->orderBy(['id'=>SORT_DESC])
						 	 	                          ->where(['student'=>$model->student])
						 	 	                          ->andWhere(['fee_period'=>$fee_])
						 	 	                          ->andWhere(['academic_year'=>$acad])
						 	 	                          ->andWhere(['balance'=>0 ])
						 	 	                          ->all();
						            
										     if($modelBillings_balance0==null)
										 	  { $this->fee_period = $fee_;
													 	    
												$model->fee_period= [$fee_];
										 	 
										 	 	break;
										 	  }
									 	  								 	  
								 	   }
								 	   
		                             */
								 	   
						  	      $model->fee_period = '';
						  	      	}
						  	   
						  	    
						  	    
						  	       
						   }
						     
		          }
			 			 
				
				$last_payment_transaction_fee_period =null;
						$last_fee_paid = null;				
				
			   
							 if(($this->student_id !='')&&($this->fee_period !=''))
								 {
					                 $this->message_positiveBalance = false;
					               
					               $modelPay_method = new SrcPaymentMethod;
								    //tcheke si default method lan defini deja
						      	   $default_pmethod_defined = $modelPay_method->isAlreadyDefined(); 
						      	   
						      	 if($default_pmethod_defined!=NULL)
                                    { 
		                                foreach($default_pmethod_defined as $default_pmethod_def)
		                                  {    
		                                  	 if($model->payment_method=='')
		                                  	    $model->payment_method = $default_pmethod_def['id'];  
		                                  }
							               
                                     }
					                 
					                 
					             //gad si gen rezevasyon   
					                 //return an array(id, amount) or null value
										$modelReservation = new SrcReservation;
										$deja_peye = $modelReservation->getNonCheckedReservation($this->student_id, $previous_year);				  
										if( ($deja_peye==null)||($deja_peye==0) )
										  $deja_peye = 0;
										else
										   {  foreach($deja_peye as $r)
                                                { $this->amount_reservation = $r->amount;
                                                  $currency_id= $r->devise;
                                                   $this->id_reservation = $r->id;
                                                  }
                                                 
										   	    $this->reservation = true;
										      
										     }
					           
				                   //-------------------------
				                   
				                   
				                   
				                   
					                 
								   //	$to_pay = Fees::model()->FindByAttributes(array('id'=>$this->fee_period));
								   		$tou_pay = SrcFees::find()
								   		          ->joinWith(['fee0',])
								   		          ->where(['fees.id'=>$this->fee_period])
								   		          ->andWhere(['fees_label.status'=>$this->status_])
								   		          ->all();
								   	  
									   
								     	
								   	  
						  if($tou_pay!=null) 
							{	  
							   foreach($tou_pay as $to_pay)
							   {    
								   	  
								   	  $student = $this->student_id;
								   	  $date_lim = $to_pay->date_limit_payment;
						              $new_balance = 0;
						              
						              $currency_id = $to_pay->devise;
						              
						              $condition_fee_status = ['fees_label.status'  => $this->status_]; 
									
									//return "id", "amount_pay" and "balance"
									$modelBilling = new SrcBillings();
									$checkForBalance_ = $modelBilling->checkForBalance($this->student_id, $this->fee_period, $this->status_, $acad);
								
							   
							   if($checkForBalance_ != null)
								 {    
								 	foreach($checkForBalance_ as $checkForBalance)
								 	{ 
									    
									    if($checkForBalance->amount_pay!=0)
									       { 
										      
									       	  $this->remain_balance = $checkForBalance->balance;
									       }
									    else
									       {
									    	        $amount = 0;
											                	 
												                	 $modelFee = SrcFees::findOne($this->fee_period);
												                	 
												                	 $currency_id= $modelFee->devise;
												                	 
												                	  //check if student is scholarship holder
												                	  $modelPerson = new SrcPersons();
														           	 //return 0(pa bousye) or 1(bousye)
				  								           	  $model_scholarship = $modelPerson->getIsScholarshipHolder($this->student_id,$acad);    
														           	  
														           	  $konte =0;
																	  $amount = 0;	
																		$percentage_pay = 0;
																		 $porcentage_level = 0; //mwens ke 100%
																		  $this->internal=0;
                                                                       $partner_repondant = NULL;																		
																	  $premye_fee = NULL;
																		          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																			                 $modelScholarshipH = new SrcScholarshipHolder();
																							   $notNullFee = $modelScholarshipH->feeNotNullByStudentId($this->student_id,$acad);
																		           	          $full=1;
																						   if($notNullFee!=NULL)
																							{																								                                                            
																							  foreach($notNullFee as $scholarship)
																			           	    	{ $konte++;
																			           	    	  
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   {
																			           	    	   	 $full=0;
																			           	    	   	 
																			           	    	     if($scholarship->fee == $modelFee->id)
																			           	    	     { 
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																										$partner_repondant = $scholarship->partner;
																		           	                    
																									   if(($partner_repondant==NULL))
																									    {	$amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																		           	                    
																											 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																									     
																										  }
																										 else
																											  $amount = $modelFee->amount;
																			           	    	      }
																			           	    	      
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								if((($konte>0)&&($amount==0))&&(!$this->full_scholarship))
																				           	    	  $amount = $modelFee->amount;
																							}
																						   else
																						     {
																								 // $this->full_scholarship = true;
																								 
																								 //fee ka NULL tou
																								  $check_partner=$modelScholarshipH ->getScholarshipPartnerByStudentIdFee($this->student_id,NULL,$acad);
																								  //$porcentage_level = 1; //100%
																								  //$percentage_pay=100;
																								  
																								   if($check_partner!=NULL)
																								   {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }
																								   
																								       if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												  
																												 
																											  }
																								       else
																								       {			  

																								   if(($partner_repondant==NULL))
																								      {  $amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																								         $this->internal=1;
																									  }
																								   else
																								      $amount = $modelFee->amount;
																								       }
																							 }
																			           	    	 
																			           	    
																			           	 }
																			           elseif($model_scholarship == 0)   //se pa yon bousye
																					     $amount = $modelFee->amount;
						           	                                            

														             //$this->remain_balance = $amount ;
														             
														       //etap 2
														            //gade si denye peman gen balans (-) nan tab billins lan
															       //return an Integer (id)
															        $modelBilling = new SrcBillings();
                                                                                                                                
                                                                                                                       // denye tranzaksyon pou devise sa
															      //return id or null value 
																  $last_payment_transaction = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
																  
																 if($last_payment_transaction!=null)
																  {
																	  
																	  //return "id", "amount_pay" and "balance"
									                                  $getPreviousFeeBalance = $modelBilling->getPreviousFeeBalanceByDevise($this->student_id, $modelFee->program, $modelFee->date_limit_payment,$modelFee->devise, $acad);
								
												                       if($getPreviousFeeBalance != null)
													                       {     
														                      foreach($getPreviousFeeBalance as $previousFeeBalance)
														                        {
														                        	  if($previousFeeBalance->balance >0)
														                                  $this->message_positiveBalance = true;
														                                  
														                        }
														       
													                       }	
													                       	
												 				    $modelBillings_last_payment= SrcBillings::findOne($last_payment_transaction);
																       
																       if($modelBillings_last_payment->balance <= 0)
																          {  
																          
																          
																          	$this->remain_balance = $amount + $modelBillings_last_payment->balance;
																          	   if($this->remain_balance <= 0)
										                                           { $this->special_payment = true;
										                                              
										                                               $model->setAttribute('amount_pay', $amount );
										                                             }
																          	   
																          	}
																       elseif($modelBillings_last_payment->balance == $checkForBalance->balance)
							                                                  $this->remain_balance = $checkForBalance->balance;
							                                                else
																               $this->message_positiveBalance = true;
							                                                    									          
																   }
																 else
																   {
																   	   $this->remain_balance = $amount;
																   	 }

														             
									    	
									          }
								    
										    if(($this->remain_balance <= 0) )//
												$this->message_paymentAllow = false;
								    
								         
								          break;
								        
								         }//end foreach checkbalance
								         
								     }
								   else
								     {     
										  $amount = 0;
												                	 
												                	 $modelFee = SrcFees::findOne($this->fee_period);
												                
												                  $currency_id = $modelFee->devise;
												                //si se pending balance
												                if($modelFee->fee0->fee_label=="Pending balance")
												                 {
												                 	 //gad si elev la gen balans ane pase ki poko peye
																		$modelPendingBal=SrcPendingBalance::find()
													   	                                     ->select(['id', 'balance'])
													   	                                     ->where(['student'=>$model->student])
													   	                                     ->andWhere(['is_paid'=>0])
													   	                                     ->andWhere(['devise'=>$currency_id])
													   	                                     ->andWhere(['academic_year'=>$previous_year])
													   	                                     ->all();
													   	              	
																		//si gen pending, ajoutel nan lis apeye a			
																		if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
																		 {
																		   foreach($modelPendingBal as $bal)
																			 {	
																			 	$this->remain_balance =  $bal->balance;
																			 	
																			 	$this->is_pending_balance =true;
																			 }
																		 }	
												                 	
												                 	
												                  }
												                else
												                 {	 
												                	  $this->is_pending_balance =false;
												                	  
												                	  //check if student is scholarship holder
												                	  $modelScholoarshipH = new SrcScholarshipHolder();
												                	  //return 0(pa bousye) or 1(bousye)
														           	  $model_scholarship = $modelScholoarshipH->getIsScholarshipHolder($this->student_id,$acad);    
														           	  
														           	  $konte =0;
																	  $amount = 0;	
																		$percentage_pay = 0;
																		 $porcentage_level = 0; //mwens ke 100%
																		 $this->internal=0;
                                                                   $partner_repondant = NULL;																		
																	  $premye_fee = NULL;
			        													          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																							   $modelScholoarshipH = new SrcScholarshipHolder();
																							   $notNullFee = $modelScholoarshipH->feeNotNullByStudentId($this->student_id,$acad);
																			           	      $full=1;
																						   if($notNullFee!=NULL)
																							{																								
																							  foreach($notNullFee as $scholarship)
																			           	    	{ $konte++;
																			           	    	  
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   { 
																			           	    	   	  $full=0;
																			           	    	    if($scholarship->fee == $modelFee->id)
																			           	    	     { 
																			           	    	     	$currency_id = $modelFee->devise;
																			           	    	     	
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																										$partner_repondant = $scholarship->partner;
																		           	                    
																									   if(($partner_repondant==NULL))
																									    {	$amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																		           	                    
																											 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																									     
																										  }
																										 else
																											  $amount = $modelFee->amount;
																			           	    	      }
																			           	    	      
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								if((($konte>0)&&($amount==0))&&(!$this->full_scholarship))
																				           	    	  $amount = $modelFee->amount;
																							}
																						   else
																						     {
																								// $this->full_scholarship = true;
																								 
																								 //fee ka NULL tou
																								  $check_partner=$modelScholoarshipH->getScholarshipPartnerByStudentIdFee($this->student_id,NULL,$acad);
																								  //$porcentage_level = 1; //100%
																								  //$percentage_pay=100;
																								  
																								   if($check_partner!=NULL)
																								   {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }
																								   
																								       if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												   
																												 
																											  }
																								       else
																								       {			  

																								   if(($partner_repondant==NULL))
																								      {  $amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																								         $this->internal=1;
																									  }
																								   else
																								      $amount = $modelFee->amount;
																								       }
																							 }
																			           	    	 
																			           	    
																			           	 }
																			           elseif($model_scholarship == 0)   //se pa yon bousye
																					     $amount = $modelFee->amount;
							           	  

														             //$this->remain_balance = $amount ;
														             
														       //etap 2
														            //gade si denye peman gen balans (-) nan tab billins lan
															      $modelBilling = new SrcBillings();
															       //return an Integer (id)
																  $last_payment_transaction = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
																  
																 if($last_payment_transaction!=null)
																  {
																  	
												 				    $modelBillings_last_payment= SrcBillings::findOne($last_payment_transaction);
																       
																    //return "id", "amount_pay" and "balance"
									                                $getPreviousFeeBalance = $modelBilling->getPreviousFeeBalanceByDevise($this->student_id, $modelFee->program, $modelFee->date_limit_payment,$modelFee->devise, $acad);
								
												                       if($getPreviousFeeBalance != null)
													                       {     
														                      foreach($getPreviousFeeBalance as $previousFeeBalance)
														                        {
														                           if($previousFeeBalance->balance >0)
														                              {  $this->message_positiveBalance = true;
														                                  
														                              }
														                                
														                        }
														       
													                       }	
								                                     
																       if($modelBillings_last_payment->balance <= 0)
																          {  
		  														          	$currency_id = $modelBillings_last_payment->feePeriod->devise;
		  														          	
																          	$this->remain_balance = $amount + $modelBillings_last_payment->balance;
																          	  if(($modelBillings_last_payment->balance < 0)&&($this->remain_balance==0))
																          	   {
																          	   	   $this->message_paymentAllow =false;
																          	     	$this->special_payment = true;
																          	     	
										                                              
										                                               $this->remain_balance = $amount;
										                                               $model->setAttribute('amount_pay', $amount );
																          	   	  }
																          	     
																          	     if($this->remain_balance < 0)
										                                           { $this->special_payment = true;
										                                              
										                                               $model->setAttribute('amount_pay', $amount );
										                                             }
																          	}
																         else
																            $this->message_positiveBalance = true;
																          
																          
																          
																          
																   }
																 else
																   {
																   	   $this->remain_balance = $amount;
																   	 }
																   	 
								                         }//fen se pa "Pending balance"

											if(($this->remain_balance <= 0) )//
										        $this->message_paymentAllow = false;		
										      
										       	             
								       
								       
								       }
								    
								    }
								      
                               
								  
								 }//end foreach topay							 
			
								   		          
								  }
								   
					        
				      //tcheke si elev sa te peye rezevasyon 
					    if($this->reservation == true)
						   {
						   	   $this->remain_balance = $this->remain_balance - $this->amount_reservation;
						   	   
						   	   if($this->remain_balance!=0)
								 {
									//$this->message_paymentAllow = false;
									//$this->special_payment = false;
													     
								  }
								else
								  {     $this->message_paymentAllow = false;
										$this->special_payment = true;
										
										//load appropriate info reservation 
										$reservationInfo = SrcReservation::findOne($this->id_reservation);
										
										$model->setAttribute('amount_pay', $this->amount_reservation );
										$model->setAttribute('payment_method', $reservationInfo->payment_method );
										$model->date_pay=$reservationInfo->payment_date;
										$model->setAttribute('comments', $reservationInfo->comments );
										$model->setAttribute('reservation_id', $this->id_reservation );
										           	
								   }
								   
						   	 }       
                             
								  $model->setAttribute('amount_to_pay', $this->remain_balance );
						
								  
				
                          // $dbTrans = Yii::app->db->beginTransaction(); 
			            
					       
			            if(isset($_POST['create']))
			            {
				                  $save_ok = false;
				                          	
								if(($_POST['SrcBillings']['date_pay']!="")&&($_POST['SrcBillings']['date_pay']!='0000-00-00'))
								 {  
								 	$date_pay_ = $_POST['SrcBillings']['date_pay'];
								 	$model->setAttribute('date_pay', $date_pay_);
								 	if($model->payment_method!="")
									  {	
										$modelBilling = new SrcBillings;
										//tcheke si yo peye fre sa nan date_pay sa deja
										$modelBillings_datePay=Billings::find()
										                        ->orderBy(['id'=>SORT_DESC])
										                        ->where(['student'=>$this->student_id])
										                        ->andWhere(['fee_period'=>$this->fee_period])
										                        ->andWhere(['academic_year'=>$acad])
										                        ->andWhere(['date_pay'=>$date_pay_])
										                        ->all();
										            
									 if($modelBillings_datePay==null)
										{
										
											 $condition_fee_status = ['fees_label.status'  => $this->status_];
											//return an Integer (id)
											$last_payment_transaction_id = $modelBilling->getLastTransactionID($this->student_id, $condition_fee_status, $currency_id, $acad);
				
											
											$fee_period_r = $_POST['SrcBillings']['fee_period'];
											$amount_pay = $model->amount_pay; 
											//$acad_sess_year=$model->academic_year;
											
											//$to_pay = Fees::model()->FindByAttributes(array('id'=>$fee_period_r));
											
											if($this->special_payment)
											  $amount_2p = $model->amount_pay;
											else
											   $amount_2p = $model->amount_to_pay;
															   
										
										//tcheke balans pou peryod sa lap peye pou li a
										$there_is_balance=$modelBilling->checkForBalance($this->student_id, $this->fee_period, $this->status_, $acad);
					
										if(isset($there_is_balance)&&($there_is_balance!=null))
										  { 
										  	   $id_bill_transaction ='';
										  	   foreach($there_is_balance as $balance_there)
										  	         $id_bill_transaction = $balance_there->id;
										  	         
										  	  $new_model_billing =SrcBillings::findOne($id_bill_transaction);
										  	
										  	  $amount_to_pay = 0;
										  	 
										  	  if($to_pay!=null)
				                                 $amount_to_pay = $amount_2p;
													
												if($new_model_billing->amount_pay==0)// update this record made by SYGES
												 {
												 	
												  	   if($this->special_payment)
												  	     {    if( ($amount_pay-$this->remain_balance) == 0)
														  	    $balance = 0;
														  	 else
														  	   $balance = $this->remain_balance;// + $amount_pay;
														  	
												  	      }
												  	   else
												  	      $balance = $amount_to_pay - $amount_pay;  
												  	   	
												  	   	     
												  	   	  $payment_method = $model->payment_method;
												  	   	  $comments = $model->comments;
												  	   	  $date_pay = $model->date_pay;
												  	   	  
												  	   	  if($balance==0)
												  	   	    $new_model_billing->fee_totally_paid= 1;
												  	   	       
												  	   	      
												  	   	   
												  	   	   //$new_model_billing->amount_to_pay', $amount_to_pay);
												  	   	   $new_model_billing->amount_pay=$amount_pay;
												  	   	   $new_model_billing->balance= $balance;
												  	   	   $new_model_billing->date_pay=$date_pay;
												  	   	   
												  	   	   $new_model_billing->payment_method=$payment_method;
												  	   	   $new_model_billing->comments=$comments;
												  	   	   $new_model_billing->setAttribute('academic_year', $acad);
									                        
									                         $new_model_billing->updated_by=currentUser();
											                 $new_model_billing->date_updated= date('Y-m-d');
											                        
												  	
												  	  $ID = '';
												  	      
												  	   if($new_model_billing->save())
												         {  
												         	//fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
												         	//$model_billing_anvan =$this->findModel($last_payment_transaction_id);
												         	     $command = Yii::$app->db->createCommand();
										                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
									                                               ->execute();
									                                               
												         	
												         	// fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
												         	if($balance==0)
												         	  {  $command = Yii::$app->db->createCommand();
										                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$new_model_billing->fee_period,'student'=>$model->student ])
									                                               ->execute();
									                            
									                           
												                 if($this->is_pending_balance==true) //update pending_balance to paid
												  	   	         {
												  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id, 'student'=>$model->student ])
									                                               ->execute();
									                                               
												  	   	          }
												         	  
												         	     }
												         	
												         	$model->balance = $new_model_billing->balance;
												         	$ID = $new_model_billing->id;
												         	$save_ok = true;  
												          }
												          
												  }
												else
												   {	
												  	   	  if($new_model_billing->balance > 0)
												  	   	    {
												  	   	    	if($this->special_payment)
														  	      {  if( ($amount_pay-$this->remain_balance) == 0)
														  	             $balance = 0;
														  	         else
														  	            $balance = $this->remain_balance;// + $amount_pay;
														  	       }
														  	    else
														  	      $balance = $new_model_billing->balance - $amount_pay;
												  	   	    }
												  	   	  else
												  	   	    {
												  	   	      if($this->special_payment)
														  	     {  if( ($amount_pay-$this->remain_balance) == 0)
														  	             $balance = 0;
														  	        else
														  	            $balance = $this->remain_balance;// + $amount_pay;
														  	            
														  	      }
														  	    else
														  	      $balance = $new_model_billing->balance + $amount_pay;
												  	   	    }
												  	   	  
												  	   	  $payment_method = $model->payment_method;
												  	   	  $comments = $model->comments;
												  	   	  $date_pay = $model->date_pay;
												  	   	  
												  	   	   if($balance==0)
												  	   	     $model->setAttribute('fee_totally_paid', 1);
												  	   	   
												  	   	   $model->setAttribute('amount_to_pay', $amount_to_pay); 
												  	   	   $model->setAttribute('amount_pay', $amount_pay);
												  	   	   $model->setAttribute('balance', $balance);
												  	   	   $model->setAttribute('date_pay', $date_pay);
												  	   	   
												  	   	   $model->setAttribute('payment_method', $payment_method);
												  	   	   $model->setAttribute('comments', $comments);
												  	   	   $model->setAttribute('academic_year', $acad);
									                        
									                         $model->setAttribute('created_by', currentUser() );
											                 $model->setAttribute('date_created', date('Y-m-d'));
											                        
												  	
												  	  
												  	  $ID = '';
												  	      
												  	   if($model->save())
												         {  
												         	//fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
												         	//$model_billing_anvan =$this->findModel($last_payment_transaction_id);
												         	     $command_ = Yii::$app->db->createCommand();
										                          $command_->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
									                                               ->execute();
									                                               
				
				                                           // fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
												         	if($balance==0)
												         	  {  $command = Yii::$app->db->createCommand();
										                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$model->fee_period,'student'=>$model->student ])
									                                               ->execute();
												         	  
												         	    if($this->is_pending_balance==true) //update pending_balance to paid
												  	   	         {
												  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                                               ->execute();
												  	   	          }
												         	    
												         	   }
				 
												         	
												         	$ID = $model->id;
												         	$save_ok = true;  
												          } 
												          
												    }
										  	        
										   }
										else
										  {
										     $amount_to_pay =0;
										     
										     if($to_pay!=null)
				                                 $amount_to_pay = $amount_2p;
										    
										            if($this->special_payment)
														 {  if( ($amount_pay-$this->remain_balance) == 0)
														  	    $balance = 0;
														  	 else
														  	   $balance = $this->remain_balance;// + $amount_pay;
													  	   
														  }
													 else
														$balance = $amount_to_pay - $amount_pay;
												  	   	  
												  	   	  $payment_method = $model->payment_method;
												  	   	  $comments = $model->comments;
												  	   	  $date_pay = $model->date_pay;
												  	   	  
												  	   	   if($balance==0)
												  	   	    $model->setAttribute('fee_totally_paid', 1);
												  	   	    
												  	   	   $model->setAttribute('amount_to_pay', $amount_to_pay);
												  	   	   $model->setAttribute('amount_pay', $amount_pay);
												  	   	   $model->setAttribute('balance', $balance);
												  	   	   $model->setAttribute('date_pay', $date_pay);
												  	   	   
												  	   	   $model->setAttribute('payment_method', $payment_method);
												  	   	   $model->setAttribute('comments', $comments);
												  	   	   $model->setAttribute('academic_year', $acad);
									                         
									                         $model->setAttribute('created_by', currentUser() );
											                 $model->setAttribute('date_created', date('Y-m-d'));
											              
											              //si t gen rezevasyon   
										        	        if($this->reservation==true)
										        	          { if( ($this->message_paymentAllow == true)&&($this->special_payment == false) )
										        	          	  $model->setAttribute('reservation_id', $this->id_reservation);
										        	           }
										     
										     
										$ID = '';
							                      
										      if($model->save())
										        { 
										        	       //si t gen rezevasyon   #################################
										        	        if($this->reservation==true)
										        	          {  
										        	          	/*if( ($this->message_paymentAllow == true)&&($this->special_payment == false) )
										        	          	  { //anrejistre reservation ki te fet la
										        	          	    //load appropriate info reservation 
														               $reservationInfo = Reservation::findOne($this->id_reservation);
														               
														               $modelFeeInfo = Fees::findOne($this->fee_period);
														               
														               $__balance = $modelFeeInfo->amount-$this->amount_reservation;
														               
														               $model_new = new Billings;
														               
														               if($model->balance==0)
												  	   	                 $model_new->setAttribute('fee_totally_paid', 1);
												  	   	               else
												  	   	                 $model_new->setAttribute('fee_totally_paid', 0);
												  	   	    
																		$model_new->setAttribute('student', $model->student );
																		$model_new->setAttribute('fee_period', $model->fee_period );
																		$model_new->setAttribute('amount_to_pay', $modelFeeInfo->amount );
																		$model_new->setAttribute('amount_pay', $this->amount_reservation );
																		$model_new->setAttribute('balance', $__balance );
																		$model_new->setAttribute('payment_method', $reservationInfo->payment_method );
																		$model_new->date_pay=$reservationInfo->payment_date;
																		$model_new->setAttribute('comments', $reservationInfo->comments );
																		$model_new->setAttribute('academic_year', $acad);
																		
																		 $model_new->setAttribute('created_by', currentUser() );
											                             $model_new->setAttribute('date_created', date('Y-m-d'));
																		
										        	          	         $model_new->save();
										        	          	    
										        	          	   }
										        	          	  */
										        	          	 
												                 $command_11 = Yii::$app->db->createCommand();
										                          $command_11->update('reservation', ['already_checked' => 1,   ], ['id'=>$this->id_reservation ])
									                                               ->execute();
												                 
										        	            }
										        	            
										        	          //##########################################
										        	       
										        	        //fee_totally_paid=1 pou tranzaksyon ki vin anvan sa
												         	//$model_billing_anvan =$this->findModel($last_payment_transaction_id);
												         	     $command_ = Yii::$app->db->createCommand();
										                          $command_->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
									                                               ->execute();
				
				                                          // fee_totally_paid=1 pou tout lot tranzaksyon fee sa pou ane a
												         	if($balance==0)
												         	  {  $command = Yii::$app->db->createCommand();
										                          $command->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$model->fee_period,'student'=>$model->student ])
									                                               ->execute();
												         	  
												         	     if($this->is_pending_balance==true) //update pending_balance to paid
												  	   	         {
												  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id, 'student'=>$model->student ])
									                                               ->execute();
												  	   	          }
												  	   	          
												         	   }
				
				
				                                        $ID = $model->id;
										        	   $save_ok = true;
										         }
										 
										  }     
							                      
							                  
							                       
											 	if($save_ok)
													{  
														//update tab balance lan
														
								                         $modelBalance_=SrcBalance::find()
								                                        ->where('student='.$model->student.' AND devise='.$currency_id)
								                                        ->all();
								                                        
									                      if($modelBalance_!=null)
										                    {   
										                           	   $id_modelBalance=0;
										                           	   
										                       foreach($modelBalance_ as $modelBalance)
										                         {  	     //$modelBalance = $modelBal;
										                           	     
										                           	      $id_modelBalance = $modelBalance->id;
										                           	 
										                           	 $new_model_balance = SrcBalance::findOne($id_modelBalance);
											                           	       
										                           	 if($this->status_ == 1) 
										                           	  {
											                           	  if($model->balance == 0) //delete  $modelBalance
												                            {	
												                            	$new_balance_1= 0;
												                            	//$modelBalance->delete();
												                            	
												                            	if($modelBalance->balance >= $model->amount_pay)
												                            	  { $new_balance_1= $modelBalance->balance - $model->amount_pay;
												                            	     
												                            	     $new_model_balance->setAttribute('balance',  $new_balance_1);
																			       	    
																			       	     $new_model_balance->save();
												                            	  }
												                            	else
												                            	  {   $new_balance_1= 0;
												                            	
												                            	     $new_model_balance->setAttribute('balance',  $new_balance_1);
																			       	    
																			       	     $new_model_balance->save();
												                            	  	
												                            	  	}
												                          
												                             }
												                            elseif($model->balance != 0) //$model->balance se nouvo balans lan
												                            {  
																	       	    $new_balance_1= 0;
												                            	//$modelBalance->delete();
												                            	if($modelBalance->balance >= $model->amount_pay)
												                            	   $new_balance_1= $modelBalance->balance - $model->amount_pay;
												                            	//else
												                            	//   $new_balance_1= $modelBalance->balance + $model->amount_pay;
												                            	
																	       	    $new_model_balance->setAttribute('balance',  $new_balance_1);
																	       	    
																	       	     $new_model_balance->save();
												                            	
												                              }	 
												                              
										                           	   }
										                           	 elseif($this->status_ == 0)
										                           	   {
										                           	   	 if($model->balance != 0) //$model->balance se nouvo balans lan
												                            {  
																	       	   
																	       	   // on ajoute 
												                            	  
																			       	    $new_balance1= $modelBalance->balance + $model->balance;
																			       	    $new_model_balance->setAttribute('balance',  $new_balance1);
																			       	    
																			       	     $new_model_balance->save();
												                            	   
												                              }	 
												                              
										                           	   	 }
																		
										                             }
																		
										                           }
										                         else // add new balance record in balance table
										                          {   
										                          		  
											                          if($model->balance != 0) 
										                           	    {
											                           	  	$modelBalance= new SrcBalance();
											                              
											                              $modelBalance->setAttribute('student',$model->student);
											                              $modelBalance->setAttribute('balance', $model->balance);
											                              $modelBalance->setAttribute('devise', $currency_id);
											                              $modelBalance->setAttribute('date_created', date('Y-m-d'));
											                              
											                               $modelBalance->save();
										                           	    }
											                      
										                              			                          	
										                          }
										                         
										                         
								                      					
														return $this->redirect(['index','id'=>$ID,'ri'=>$this->recettesItems,'part'=>'rec']);
														
														
														}  
									
							                    }
									         else
									           {  $this->message_2paymentFeeDatepay=true;
									           }
							              
							               }
								         else
								           { $this->message_paymentMethod=true;
								           }
									 }
									else
							           { $this->message_datepay=true;
							           }
						           
				                 }
			            
			            
			          } 
	

            return $this->render('create', [
                'model' => $model,
                'status_'=>$this->status_,
                'student_id'=>$this->student_id,
                'id_reservation'=>$this->id_reservation,
                'internal'=>$this->internal,
                'is_pending_balance'=>$this->is_pending_balance,
                'fee_period'=>$this->fee_period,
                'message_positiveBalance'=>$this->message_positiveBalance,
                'amount_reservation'=>$this->amount_reservation,
                'remain_balance'=>$this->remain_balance,
                'special_payment'=>$this->special_payment,
                'message_paymentAllow'=>$this->message_paymentAllow,
                'message_2paymentFeeDatepay'=>$this->message_2paymentFeeDatepay,
            ]);
            
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }


 public function actionSelectstudent()
    {
    	$acad = Yii::$app->session['currentId_academic_year'];
    	
    	 $model = new SrcBillings();
         
    
       
	       if($model->load(Yii::$app->request->post()) ) 
			 {
				    
			 }
	

            return $this->renderAjax('selectStudent', [
                'model' => $model,
                
            ]);
        
    }
    
    
    /**
     * Updates an existing Billings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('billings-billings-update'))
         { 
        $acad=Yii::$app->session['currentId_academic_year']; 
		$modelAcad = new SrcAcademicperiods;
		$previous_year= $modelAcad->getPreviousAcademicYear($acad);
 
 
        $this->is_pending_balance=false;
 
		
		
		    if($_GET['ri'] ==  0)     
					 $this->status_ = 1;					              
				elseif($_GET['ri'] ==  1)     
					   $this->status_ = 0;

        
        $model = $this->findModel($id);

        $this->message_paymentAllow=true;
		$this->message_positiveBalance=false;
		$this->message_2paymentFeeDatepay=false;
		
         $this->old_balance =0;
         $this->old_student ="";
		
        $balance_current_record = $model->balance;
        
        $this->old_balance = $model->balance;  //
        $this->old_student = $model->student; //
        $this->fee_period=$model->fee_period;
        $this->old_fee_period = $model->fee_period;
        $this->old_amount_to_pay = $model->amount_to_pay;
        $this->old_amount_pay = $model->amount_pay;
        
        
        $modelFee = SrcFees::findOne($model->fee_period);
        
        $currency_id = $modelFee->devise;
        
        //$modelFeeLabel = FeesLabel::model()->findByPk($modelFee->fee);
          if($modelFee->fee0->fee_label=="Pending balance")
            $this->is_pending_balance=true;
          
        
	             if($model->load(Yii::$app->request->post()) ) 
			        {
			            // $dbTrans = Yii::app->db->beginTransaction(); 
			            $condition_fee_status = ['fees_label.status'=>$this->status_];
					    
						$this->student_id = $this->old_student;//Yii::app()->session['Student_billings'];
						$this->fee_period = $this->old_fee_period;
						 
						$this->message_positiveBalance = false;
			  
					       
			            if(isset($_POST['update']))
			            { 
						    $from="";
					  	  if(isset($_GET['from']))
					  	       $from=$_GET['from'];
					  	 
						 if(($_POST['SrcBillings']['date_pay']!="")&&($_POST['SrcBillings']['date_pay']!='0000-00-00'))
						    {  $model->setAttribute('date_pay',$_POST['SrcBillings']['date_pay'] );
						 	    $date_pay_ = $_POST['SrcBillings']['date_pay'];
						 	  if($model->payment_method!="")
							    {  	
							    	$amount_to_pay = 0;
							  	    $model->setAttribute('student',$this->old_student );
							  	 	
							  	  	   $amount_pay = $model->amount_pay; 
									   //$acad_year=$model->academic_year;
									
									   // $to_pay = Fees::model()->FindByPk($this->fee_period);
								 
			                     	          $balance = $model->amount_to_pay - $model->amount_pay;
			                     	          
									  	   	  $payment_method = $model->payment_method;
									  	   	  	
									  	   	  $comments = $model->comments;
									  	   	  if($model->date_pay =='0000-00-00')
									  	   	     $date_pay = date('Y-m-d');
									  	   	  else
									  	   	     $date_pay = $model->date_pay;
									  	   	  
									  	   	    $model= new Billings;
									  	   	    
									  	   	   $model=$this->findModel($_GET['id']);

    //si old > new =>  ajout (old - new) sur la balance de la table balance
	//si old < new =>  soustr (new - old) sur la balance de la table balance
									  	   	   
									  	   	   if($balance==0)
								  	   	         $model->fee_totally_paid= 1;
								  	   	       else
								  	   	         $model->fee_totally_paid= 0;
								  	   	    
									  	   	   $model->amount_pay= $amount_pay;
									  	   	   $model->balance= $balance;
									  	   	   
									  	   	   $model->payment_method= $payment_method;
									  	   	   $model->comments= $comments;
									  	   	   $model->date_pay= $date_pay;
						                        
						                        $model->date_updated=date('Y-m-d');
						                        $model->updated_by=currentUser();
			                    
				
							  if($model->save())
								 { //$dbTrans->commit();  	
					                if( ($this->is_pending_balance==true)&&($balance<=0) ) //update pending_balance to paid
													{
													   $command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'student'=>$model->student ])
									                               ->execute();
									                                              
													}
											      elseif( ($this->is_pending_balance==true)&&($balance >0) ) //update pending_balance to not paid
													{
													   $command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 0,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                               ->execute();
									                               
									                               
													}
											
										        
										        $new_balance = 0;
					                            $date_pay = null;
					                            $student = $model->student;
					                            $fee_period = $model->fee_period;
					                            $old_balance_ = $model->balance;
					                            $old_fee_period = $model->fee_period;
					                            
					                            $id_bill = $model->id;
					              
					          					               
					               //cheche tout lot peman ki fet apre peman sila pou moun sa (date_pay DESC)
					         //$sql_1 = 'SELECT b.id, b.balance FROM billings b  INNER JOIN fees f ON(f.id=b.fee_period) INNER JOIN fees_label fl ON(fl.id=f.fee)  WHERE b.student='.$this->student_id.' AND fl.status='.$this->status_.' AND b.date_pay > \''.$model->date_pay.'\'  order by b.id ASC';
					          
					          $result1 = SrcBillings::find()
					                         ->select(['billings.id','billings.balance'])
							   	             ->orderBy(['billings.id'=>SORT_ASC])
							   	             ->joinWith(['feePeriod','feePeriod.fee0',])
							   	             ->where(['billings.student'=>$this->student_id])
							   	             ->andWhere(['fees_label.status'=>$this->status_])
							   	             ->andWhere(['fees.devise'=>$currency_id])
							   	             ->andWhere(['>','billings.date_pay',$model->date_pay])
							   	             ->all();
 
											       	   
									if($result1!=null) 
									  { 
										 $last_id_in_range ='';
										 $stop=false;
										 
										 foreach($result1 as $bill)
											{  
											      $id_bill = $bill->id;
											      $last_id_in_range = $bill->id;
											      
											       $new_model=$this->findModel($bill->id);
											      
											       if(($balance!=0)&&($stop==true))
											        {
											        	$balance_current_record = $new_model->balance;
											        	
											        	if($balance < 0)
											        	  {
											        	  	  if( ($this->is_pending_balance==true) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                               ->execute();
																  	   	          }
											        	  	 
											        	  	 $amount_to_pay = $new_model->amount_to_pay + $balance;
											        	  	 $new_balance1 = $amount_to_pay - $new_model->amount_pay;
											        	  	 
											        	   	          if($new_balance1<=0)
														  	   	         $new_model->fee_totally_paid= 1;
														  	   	      // else
														  	   	        // $new_model->setAttribute('fee_totally_paid', 0);
														  	   	    
															  	   	   $new_model->amount_to_pay= $amount_to_pay;
															  	   	   $new_model->balance= $new_balance1;
															  	   	    
												                         $new_model->updated_by = currentUser();
														                        $new_model->date_updated= date('Y-m-d');
														                        
														                 if($new_model->save())
														                   {
														                       unset($new_model); 
												 	 	   	      	            $stop=true;         
												 	 	   	      	          $balance = $new_balance1;  
												 	 	   	      	        
												 	 	   	      	        if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                               ->execute();
																  	   	          }
												 	 	   	      	          
														                   } 
							 	 	   	      	             
									        	  	
											        	  	}
											        	 elseif($balance > 0)
											        	   {
											        	   	    if($new_model->fee_period == $old_fee_period)
											        	   	       { $amount_to_pay = $balance;
											        	   	          
											        	   	          $new_balance1 = $amount_to_pay - $new_model->amount_pay;
											        	   	          
											        	   	          if($new_balance1<=0)
														  	   	         $new_model->fee_totally_paid= 1;
														  	   	      // else
														  	   	        // $new_model->setAttribute('fee_totally_paid', 0);
														  	   	    
															  	   	   $new_model->amount_to_pay= $amount_to_pay;
															  	   	   $new_model->balance= $new_balance1;
															  	   	    
												                         $new_model->updated_by= currentUser();
														                        $new_model->date_updated= date('Y-m-d');
														                        
														                if($new_model->save())
														                  {  unset($new_model); 
							 	 	   	      	                        
											        	   	                  $balance = $new_balance1;
											        	   	               
											        	   	               if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                               ->execute();
																  	   	          }
																  	   	             
														                  }
											        	   	        
					
											        	   	       }
											        	   	   else
											        	   	      {  //fe yon nouvo anrejistreman pou fin peye fre sa
											        	   	           $amount_to_pay =  $balance;
											        	   	           $amount_pay =  $balance;
											        	   	           $date_pay = $new_model->date_pay;
											        	   	            $fee_totally_paid = 1;
								  	   	    
									  	   	                            $new_balance1 = 0;
									  	   	   
									  	   	                            $payment_method = $new_model->payment_method;
									  	   	                           
									  	   	                               unset($modelBillings_); 
							 	 	   	      	                            $modelBillings_= new SrcBillings; 
							 	 	   	      	                            
							 	 	   	      	                      $modelBillings_->fee_totally_paid = $fee_totally_paid;
														  	   	       
															  	   	   $modelBillings_->amount_to_pay= $amount_to_pay;
															  	   	   $modelBillings_->amount_pay= $amount_pay;
															  	   	   $modelBillings_->balance= $new_balance1;
															  	   	   $modelBillings_->payment_method= $payment_method;
															  	   	   $modelBillings_->date_pay= $date_pay;
															  	   	    
												                         $modelBillings_->created_by=currentUser();
														                        $modelBillings_->date_created= date('Y-m-d');
														                        
														               if( $modelBillings_->save())
									  	   	                             {
											        	   	               $balance = $new_balance1;
											        	   	               
											        	   	               if( ($this->is_pending_balance==true)&&($new_balance1<=0) ) //update pending_balance to paid
																  	   	         {
																  	   	         	$command11 = Yii::$app->db->createCommand();
										                          $command11->update('pending_balance', ['is_paid' => 1,   ], ['academic_year'=>$previous_year,'devise'=>$currency_id,'student'=>$model->student ])
									                               ->execute();
																  	   	          }
							 	 	   	      	               
											        	   	      	        $old_fee_period = $new_model->fee_period;
											        	   	      	        
									  	   	                             }
									  	   	                             
											        	   	      	}
											        	   	}
											         }
											       else
											          {      
											          	    //pran balans lan retire l nan tab balance lan aavan delete
											                $balance_current_record = $new_model->balance;
											          	    $new_model->delete();
											          	    
											          	}
											         
											 }
											 
											 
											$modelBill = SrcBillings::findOne($last_id_in_range);
										
										if($balance>0)
								  	   	         { 
								  	   	            if($modelBill != null)
								  	   	             {	
								  	   	             	$modelBill->fee_totally_paid= 0;
								  	   	                $modelBill->save(); 
								  	   	              }
								  	   	          }
								  	   	       
								  	   	       
								  	   	              
					                  	   $new_balance = 0;
					                        $bal = 0;
					                        
					                       $modelBalance= SrcBalance::find()
					                                      ->where('student='.$model->student.' AND devise='.$currency_id)
					                                      ->all();             
					                        
					                        if($modelBalance!=null)
						                      {   
                                                 foreach($modelBalance as $modelBalance)
                                                 {
							                  	    if($balance_current_record > $balance)
							                          { $new_balance = $balance_current_record - $balance;
							                            $bal = $modelBalance->balance - $new_balance;
							
														if($bal>=0)
							                               {	 // nan balanse total la
									 	 	   	  			 $modelBalance->balance= $bal;
															 $modelBalance->save();  
							                               }
							                             else
							                             {	 // nan balanse total la
									 	 	   	  			 $modelBalance->balance= 0;
															 $modelBalance->save();  
							                               }
							                                                       
							                           }
							                        elseif($balance_current_record < $balance)
							                           {   $new_balance = $balance - $balance_current_record;
							                               $bal = $modelBalance->balance + $new_balance;
							  
			                                               if($bal>=0)
							                               {// nan balanse total la
									 	 	   	  			 $modelBalance->balance= $bal;
															 $modelBalance->save();   
															 
							                               }
							                              else
							                              {// nan balanse total la
									 	 	   	  			 $modelBalance->balance= 0;
															 $modelBalance->save();   
															 
							                               }
							                                                        
							                           }
							                           
							                           
						                            }  
					                                
														
						                         }
						                       else
						                          {
						                           	   if($balance>0)
							                             {
							                               	$new_modelBalance= new SrcBalance;
							                              
							                              $new_modelBalance->student=$model->student;
							                              $new_modelBalance->balance= $balance;
							                              $new_modelBalance->devise = $currency_id;
							                              $new_modelBalance->date_created= date('Y-m-d');
							                              
							                               $new_modelBalance->save();
							                             }
							                             
						                           	}
						                             
								  	   	        
											 
											 
									   }     
					                 else
					                  {     
					                  	    $modelBill= SrcBillings::findOne($model->id);
									  	   	    if($model->balance>0)
								  	   	          { 
								  	   	            if($modelBill != null)
								  	   	             {	
								  	   	             	$modelBill->fee_totally_paid= 0;
								  	   	                $modelBill->save(); 
								  	   	              }
								  	   	              
								  	   	           } 
								  	   	         
								  	   	         
								  	   	         
					                  	   $new_balance = 0;
					                        $bal = 0;
					                        
					                       $modelBalance= SrcBalance::find()
					                                       ->where('student='.$model->student.' And devise='.$currency_id)
					                                       ->all();             
					                        
					                        if($modelBalance!=null)
						                      {   
                                                 foreach($modelBalance as $modelBalance)
                                                 {
							                  	    if($balance_current_record > $model->balance)
							                          { $new_balance = $balance_current_record - $model->balance;
							                            $bal = $modelBalance->balance - $new_balance;
							  
			                                            if($bal>=0)
							                               {                       
							                                 // nan balanse total la
									 	 	   	  			 $modelBalance->balance= $bal;
															 $modelBalance->save();
							                               }
							                             else
							                               {                       
							                                 // nan balanse total la
									 	 	   	  			 $modelBalance->balance= 0;
															 $modelBalance->save();
							                               }
							                               
							                           }
							                        elseif($balance_current_record < $model->balance)
							                           {   $new_balance = $model->balance - $balance_current_record;
							                               $bal = $modelBalance->balance + $new_balance;
								                               
							                              if($bal>=0)
							                               {
							                               // nan balanse total la
									 	 	   	  			 $modelBalance->balance= $bal;
															 $modelBalance->save();
							                               }
							                              else
							                                {
							                               // nan balanse total la
									 	 	   	  			 $modelBalance->balance= 0;
															 $modelBalance->save();
							                               }
							                              
							                               
							                           }
                                                   }
						                             
						                         }
						                       else
						                          {
						                           	  if($model->balance >0)
							                               {  $new_modelBalance= new SrcBalance;
							                              
							                              $new_modelBalance->student=$model->student;
							                              $new_modelBalance->balance= $model->balance;
							                              $new_modelBalance->devise = $currency_id;
							                              $new_modelBalance->date_created= date('Y-m-d');
							                              
							                               $new_modelBalance->save();
							                               
							                               }
							                               
						                           	}
						                            
						                            
						                            
					                  	}
					               
	
                                  $new_balance1 = $old_balance_;     
	          
						                       			       	  
								  	     return $this->redirect(['index','id'=>$model->id,'ri'=>$_GET['ri'],'part'=>'rec']);  	                                
								       	 
								       	  
								       }
									else
									 { //   $dbTrans->rollback();
									   }							       
								     
					            }
					          else
					             $this->message_paymentMethod=true;
					        }
					      else
				             $this->message_datepay=true;
				      }
				      
				     
				     
				     
		        
		        
		        
		         }





	
											      
					                
					        
	
            return $this->render('update', [
                'model' => $model,
                'status_'=>$this->status_,
                'student_id'=>$this->student_id,
                'id_reservation'=>$this->id_reservation,
                'internal'=>$this->internal,
                'is_pending_balance'=>$this->is_pending_balance,
                'fee_period'=>$this->fee_period,
                'message_paymentMethod'=>$this->message_paymentMethod,
				'message_datepay'=>$this->message_datepay,
                'message_positiveBalance'=>$this->message_positiveBalance,
                'amount_reservation'=>$this->amount_reservation,
                'remain_balance'=>$this->remain_balance,
                'special_payment'=>$this->special_payment,
                'message_paymentAllow'=>$this->message_paymentAllow,
                'message_2paymentFeeDatepay'=>$this->message_2paymentFeeDatepay,
            ]);
        
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Deletes an existing Billings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
public function actionDelete($id)
  {
  	  if(Yii::$app->user->can('billings-billings-delete'))
         {      
$acad=Yii::$app->session['currentId_academic_year']; 
		
	try{	
   			   $modelB = $this->findModel($id);
                           
                           
   			   
   			   $student = $modelB->student; 
   			   $acad = $modelB->academic_year;
   			   $date_pay = $modelB->date_pay;
   			   $fee_period = $modelB->fee_period;
   			   $id_bill = $modelB->id;
   			   $balance_bill = $modelB->balance;
   			   $amount_pay = $modelB->amount_pay;
   			   
   			   $currency_id = $modelB->feePeriod->devise;
   			
                                                 
   			  if(!isset($_GET['ajax']))
				{ 
					//tcheke si gen lot peman apre peman sila pou fre sa
					 $modelBilling_next = SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND devise= '.$currency_id.' AND fee_period='.$fee_period.' AND academic_year='.$acad.' AND date_pay >\''.$date_pay.'\'')->all();
					 
					 if($modelBilling_next==null)
					  {
					 		//tcheke si gen lot peman avan peman sila pou fre sa
					       $modelBilling_before =SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND devise= '.$currency_id.' AND fee_period='.$fee_period.' AND academic_year='.$acad.' AND date_pay <\''.$date_pay.'\'')->all();
					 
					 			
							//update tab balance lan
							    
							 	$modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
								   if($modelBalance!=null)
									 {  
									  foreach($modelBalance as $modelBalance)
									 	{
									 	if( $modelBilling_before!=null) 
									 	  { $balance = $modelBalance->balance + $amount_pay;
									 	      $modelBalance->setAttribute('balance',$balance);
									 	      $modelBalance->save();
									 	  }
									 	else
									 	  { 
									 	  	//if($this->status_==0)
									 	  	  //{ 
									 	  	  	$balance = $modelBalance->balance + $amount_pay; //$balance = $modelBalance->balance + $balance_bill; 
									 	  	     $modelBalance->setAttribute('balance',$balance);
									 	         $modelBalance->save();
									 	  	//  }
									 	  	  
									 	   }
									 	} 									 	
									   }
									else
									  {
									  	  $modelBalance1= new SrcBalance;
									  	  
									  	//  if($this->status_==0) 
									  	  // {
									  	         $balance1 = $amount_pay  + $balance_bill;
							                              
							                              $modelBalance1->setAttribute('student',$student);
							                              $modelBalance1->setAttribute('devise', $currency_id);
							                              $modelBalance1->setAttribute('balance', $balance1);
							                              $modelBalance1->setAttribute('date_created', date('Y-m-d'));
							                              
							                               $modelBalance1->save();
									  	    //   }
									 	
									  	}
		
		          
		                        $modelB->delete();
		                        
		                        //gade si denye tranzaksyon an gen balans +
		                        $modelBillings_balance=SrcBillings::find()->joinWith(['feePeriod'])->where('devise= '.$currency_id.' AND student='.$student.' AND fee_period='.$fee_period.' AND academic_year='.$acad)->orderBy(['id'=>SORT_DESC])->all();
						            
							            if($modelBillings_balance!=null)
								          {
								          	foreach($modelBillings_balance as $modelBillings_balance)
									 	    {
								          	  if($modelBillings_balance->balance >0)
								          	   {
								          	   	  $modelBill___=$this->findModel($modelBillings_balance->id);
										  	   	    $modelBill___->setAttribute('fee_totally_paid', 0);
									  	   	                $modelBill___->save(); 
									  	   	              
								          	   	
								          	   	}
									 	    }
								          }
		          
		                       //return $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
                                               return  $this->redirect(Yii::$app->request->referrer);
		                       
					       }
					     else
					       { 
					       	    
					       	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Cannot delete a parent transaction.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                    
                                                    return  $this->redirect(Yii::$app->request->referrer);
					       	 }
			  
			    	}
			      else
			        {
			        	
			        	//tcheke si gen lot peman apre peman sila pou fre sa
					 $modelBilling_next =SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND devise= '.$currency_id.' AND fee_period='.$fee_period.' AND academic_year='.$acad.' AND date_pay >\''.$date_pay.'\'')->all();
					 
					 if($modelBilling_next==null)
					  {
						 	//tcheke si gen lot peman avan peman sila pou fre sa
					       $modelBilling_before =SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND devise= '.$currency_id.' AND fee_period='.$fee_period.' AND academic_year='.$acad.' AND date_pay <\''.$date_pay.'\'')->all();
					 
					 			
							//update tab balance lan
							    
							 	$modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
								   if($modelBalance!=null)
									 {  
									 	foreach($modelBalance as $modelBalance)
									 	{
									 	if( $modelBilling_before!=null) 
									 	  { $balance = $modelBalance->balance + $amount_pay;
									 	      $modelBalance->setAttribute('balance',$balance);
									 	       $modelBalance->save();
									 	  }
									 	else
									 	  { 
									 	  	//if($this->status_==0)
									 	  	  //{ 
									 	  	  	$balance = $modelBalance->balance + $amount_pay; //$balance = $modelBalance->balance + $balance_bill;
									 	  	      $modelBalance->setAttribute('balance',$balance);
									            	$modelBalance->save();
									 	  	  //}
									 	   }									 	
									 	
									     }
									   }
									else
									  {
									  	  $modelBalance1= new SrcBalance;
									  	  
									  	    //if($this->status_==0)
									 	  	 // {
									 	  	  	  $balance1 = $amount_pay  + $balance_bill;
							                              
							                              $modelBalance1->setAttribute('student',$student);
							                              $modelBalance1->setAttribute('devise', $currency_id);
							                              $modelBalance1->setAttribute('balance', $balance1);
							                              $modelBalance1->setAttribute('date_created', date('Y-m-d'));
							                              
							                               $modelBalance1->save();
									 	  	  // }
									 	
									  	}
		
							 
							      $this->findModel($id)->delete();
							      
							      //gade si denye tranzaksyon an gen balans +
		                        $modelBillings_balance=SrcBillings::find()->joinWith(['feePeriod'])->where('devise= '.$currency_id.' AND student='.$student.' AND fee_period='.$fee_period.' AND academic_year='.$acad)->orderBy(['id'=>SORT_DESC])->all();
						            
							            if($modelBillings_balance!=null)
								          {
								          	foreach($modelBillings_balance as $modelBillings_balance)
								          	{
								          	  if($modelBillings_balance->balance >0)
								          	   {
								          	   	  $modelBill___=$this->findModel($modelBillings_balance->id);
										  	   	    $modelBill___->setAttribute('fee_totally_paid', 0);
									  	   	                $modelBill___->save(); 
									  	   	              
								          	   	
								          	   	}
								          	}
								          }
		          
							      
							      
				        	 }
					     else
					       { 
                                               
					       	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Cannot delete a parent transaction.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                    
                                                    return  $this->redirect(Yii::$app->request->referrer);
                                                    
					       	 }
			        		
			          }
				
			
		 } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          return  $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                 return  $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
		
		
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return  $this->redirect(Yii::$app->request->referrer);
               }

          }

    }




public function delete($id)
	{
       $modelBilling = new SrcBillings();
       
       //$acad=Yii::$app->session['currentId_academic_year'];
		
   			   $modelB=$this->findModel($id);
   			   
   			   $currency_id = $modelB->feePeriod->devise;
   			   $student = $modelB->student; 
   			   $acad = $modelB->academic_year;
   			   $id_bill = $modelB->id;
   			 
   			      $modelB->delete();
   			 // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			  if(!isset($_GET['ajax']))
				{ 
					
					//update balance cumulee
					//update balance cumulee
					$new_balance = $modelBilling->updateMainBalance($student, $currency_id, $acad);
					
					//update tab balance lan
					if($new_balance == 0)
					 {  
					 	 $modelBillings = SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND fees.devise='.$currency_id)->all(); 
					 	//2 ka posib: 
					 	// a) pa gen tranzaksyon ditou pou elev sa pandan ane a (li gen balans tout echeyans pase yo)
					 	if($modelBillings == null)
					 	 {
					 	 	 $new_balance = 0;
					 	 	 
					 	 	 $modelBalance= SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
							   if($modelBalance!=null)
								 {   
								 	$modelBalance->delete();
								 	
								   }
					 	 	 
					 	 	 $program = getProgramByStudentId($student);
	                           
					 	 	 //cheche tout "fees model" kote "checked=1"
					 	 	 $checked=1;
					 	 	 $modelFee = new SrcFees();
					 	 	 $modelFees = $modelFee->getFeeInfoByLevelAcademicperiodChecked($program, $acad, $checked);
					 	 	 
					 	 	 if($modelFees!=null)
					 	 	   {
					 	 	   	    foreach($modelFees as $fee)
					 	 	   	      {
							 	 	   	      	 unset($modelBillings); 
							 	 	   	      	 $modelBillings= new SrcBillings;
									           	    
													$modelBillings->setAttribute('student',$student);
													$modelBillings->setAttribute('fee_period',$fee['academic_period']);
													$modelBillings->setAttribute('academic_year',$acad);
													$modelBillings->setAttribute('amount_to_pay',$fee['amount']); 
													$modelBillings->setAttribute('amount_pay',0); 
													$modelBillings->setAttribute('balance', $fee['amount']);
													$modelBillings->setAttribute('created_by', Yii::app()->user->name);
													$modelBillings->setAttribute('date_created', date('Y-m-d')); 
													
		
							                if($modelBillings->save())
							                   {          
							                          
									           	   //balance record for each stud
									           	   $modelBalance= new SrcBalance;
									           	   
									           	   $modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$fee['devise'])->all();
											     							           	  
									           	  if(isset($modelBalance)&&($modelBalance!=null))
									           	    {  
									           	    	foreach($modelBalance as $modelBalance)
									           	    	{//update this model
									           	    	$balance1=$modelBalance->balance + $fee['amount'];
									           	    	 $modelBalance->setAttribute('balance',$balance1);
									           	    	 
									           	    	   if($modelBalance->save())
									           	               $pass=true;
									           	    	}
									           	    }
									           	  else
									           	    { //create new model
									           	          unset($modelBalance); 
							                              $modelBalance= new SrcBalance;
							                              
									           	      $modelBalance->setAttribute('student',$student);
									           	      $modelBalance->setAttribute('balance',$fee['amount']);
									           	      $modelBalance->setAttribute('devise',$fee['devise']);
									           	    	 $modelBalance->setAttribute('date_created',date('Y-m-d'));
									           	      
									           	      $modelBalance->save();
									           	           
									           	     
									           	    }
									           	  
							                    } 

					 	 	   	      	}
					 	 	   	 }
					 	 	   	 
					 	 	  
					 	 	
					 	   }
					 	else
					 	  {   
						 	// b) li pa dwe sou okenn tranzaksyon
						 	$modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
						 	 
						 	 if($modelBalance!=null)
								 {   
								 	$modelBalance->delete();
								 	
								   }
								else 
								  {  $modelBalance= new SrcBalance;
							                              
									           	      $modelBalance->setAttribute('student',$student);
									           	      $modelBalance->setAttribute('balance',$new_balance);
									           	      $modelBalance->setAttribute('devise',$currency_id);
									           	      $modelBalance->setAttribute('date_created',date('Y-m-d'));
									           	      
									           	      $modelBalance->save();
								   }
								   
					 	     }
	
					 	
					   }
					 else
					   {  
					   	 //pran info denye peman
					   	  $modelBillings_balance=SrcBillings::find()->joinWith(['feePeriod'])->where('devise= '.$currency_id.' AND student='.$student.' AND fee_period='.$this->fee_period.' AND academic_year='.$acad)->orderBy(['id'=>SORT_DESC])->all();
						            
							            if($modelBillings_balance!=null)
								          { 
								          	foreach($modelBillings_balance as $modelBillings_balance)
								          	{ 
								          	 if($modelBillings_balance->balance != null)
								          	      { $this->remain_balance = $modelBillings_balance->balance;

								          	        $currency_id = $modelBillings_balance->feePeriod->devise;
								          	        
								          	        if($this->remain_balance != 0)
									                  {  
									                  	$modelBalance= SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
														   if($modelBalance!=null)
															 {   
															   $modelBalance->setAttribute('balance',  $new_balance);
																					       	    
																$modelBalance->save();
																
														       }
														    else
														      {
														      	   $modelBalance= new Balance;
							                              
												           	      $modelBalance->setAttribute('student',$student);
												           	      $modelBalance->setAttribute('balance',$new_balance);
												           	      $modelBalance->setAttribute('devise',$currency_id);
												           	      $modelBalance->setAttribute('date_created',date('Y-m-d'));
												           	      
												           	      $modelBalance->save();
									           	      
														      	}

									                  	
									                  	}
									                 
									                 
								          	      }
								          	  
								           }
								           
					                        }
					
					      }

			    	}
			      else
			        {
			        	
					
					//update balance cumulee
					//update balance cumulee
					$modelBilling = new SrcBillings();
					$new_balance = $modelBilling->updateMainBalance($student,$currency_id, $acad);
					
					//update tab balance lan
					if($new_balance == 0)
					 {  
					 	 $modelBillings = SrcBillings::find()->joinWith(['feePeriod'])->where('student='.$student.' AND fees.devise='.$currency_id)->all(); 
					 	//2 ka posib: 
					 	// a) pa gen tranzaksyon ditou pou elev sa pandan ane a (li gen balans tout echeyans pase yo)
					 	if($modelBillings == null)
					 	 {
					 	 	 $new_balance = 0;
					 	 	 
					 	 	 $modelBalance=SrcBalance::find()->where('student='.$student.' AND devise'.$currency_id)->all();
							   if($modelBalance!=null)
								 {   
								 	$modelBalance->delete();
								 	
								   }
					 	 	 
					 	 	 $program = getProgramByStudentId($student);
	
					 	 	 //cheche tout "fees model" kote "checked=1"
					 	 	 $checked=1;
					 	 	 $modelFee = new FeesSrcFees();
					 	 	 $modelFees = $modelFee->getFeeInfoByLevelAcademicperiodChecked($program, $acad, $checked);
					 	 	 
					 	 	 if($modelFees!=null)
					 	 	   {
					 	 	   	    foreach($modelFees as $fee)
					 	 	   	      {
							 	 	   	      	 unset($modelBillings); 
							 	 	   	      	 $modelBillings= new SrcBillings;
									           	    
													$modelBillings->setAttribute('student',$student);
		  							                $modelBillings->setAttribute('fee_period',$fee['academic_period']);
													$modelBillings->setAttribute('academic_year',$acad);
													$modelBillings->setAttribute('amount_to_pay',$fee['amount']); 
													$modelBillings->setAttribute('amount_pay',0); 
													$modelBillings->setAttribute('balance', $fee['amount']);
													$modelBillings->setAttribute('created_by', Yii::app()->user->name);
													$modelBillings->setAttribute('date_created', date('Y-m-d')); 
													
		
							                if($modelBillings->save())
							                   {           
							                          
									           	   //balance record for each stud
									           	   $modelBalance= new SrcBalance;
									           	   
									           	   $modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
											     							           	  
									           	  if(isset($modelBalance)&&($modelBalance!=null))
									           	    {  //update this model
									           	    	foreach($modelBalance as $modelBalance)
									           	    	{
									           	    	$balance1=$modelBalance->balance + $fee['amount'];
									           	    	 $modelBalance->setAttribute('balance',$balance1);
									           	    	 
									           	    	   if($modelBalance->save())
									           	               $pass=true;
									           	    	}
									           	    }
									           	  else
									           	    { //create new model
									           	          unset($modelBalance); 
							                              $modelBalance= new SrBalance;
							                              
									           	      $modelBalance->setAttribute('student',$student);
									           	      $modelBalance->setAttribute('balance',$fee['amount']);
									           	      $modelBalance->setAttribute('balance',$fee['devise']);
									           	      $modelBalance->setAttribute('date_created',date('Y-m-d'));
									           	      
									           	      $modelBalance->save();
									           	           
									           	     
									           	    }
									           	  
							                    } 

					 	 	   	      	}
					 	 	   	 }
					 	 	   	 
					 	 	  
					 	 	
					 	   }
					 	else
					 	  {
						 	// b) li pa dwe sou okenn tranzaksyon
						 	$modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
							   if($modelBalance!=null)
								 {   
								 	$modelBalance->delete();
								 	
								   }
								else 
								  {  $modelBalance= new SrcBalance();
							                              
									           	      $modelBalance->setAttribute('student',$student);
									           	      $modelBalance->setAttribute('balance',$new_balance);
									           	      $modelBalance->setAttribute('devise',$currency_id);
									           	      $modelBalance->setAttribute('date_created',date('Y-m-d'));
									           	      
									           	      $modelBalance->save();
								   }
								   
					 	     }
	
					 	
					   }
					 else
					   {  
					   	 //pran info denye peman
					   	  $modelBillings_balance=SrcBillings::find()->joinWith(['feePeriod'])->where('devise= '.$currency_id.' AND student='.$student.' AND fee_period='.$this->fee_period.' AND academic_year='.$acad)->orderBy(['id'=>SORT_DESC])->all();
						            
							            if($modelBillings_balance!=null)
								          {  
								          	foreach($modelBillings_balance as $modelBillings_balance)
								          	{
								          		if($modelBillings_balance->balance != null)
								          	      { $this->remain_balance = $modelBillings_balance->balance;

								          	        $currency_id = $modelBillings_balance->feePeriod->devise;
								          	        
								          	        if($this->remain_balance != 0)
									                  {  $modelBalance=SrcBalance::find()->where('student='.$student.' AND devise='.$currency_id)->all();
														   if($modelBalance!=null)
															 {   
															   $modelBalance->setAttribute('balance',  $new_balance);
																					       	    
																$modelBalance->save();
																
														       }
														    else
														      {
														      	   $modelBalance= new SrcBalance;
							                              
												           	      $modelBalance->setAttribute('student',$student);
												           	      $modelBalance->setAttribute('devise',$currency_id);
												           	      $modelBalance->setAttribute('balance',$new_balance);
												           	      $modelBalance->setAttribute('date_created',date('Y-m-d'));
												           	      
												           	      $modelBalance->save();
									           	      
														      	}

									                  	
									                  	}
									                 
									                 
								          	      }
								          }
								         
								           }
																      
					      }
					      
					      
					      
			          }
				
			
			



	}
	
	
    /**
     * Finds the Billings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Billings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcBillings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
