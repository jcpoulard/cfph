<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\Taxes;
use app\modules\billings\models\SrcTaxes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * TaxesController implements the CRUD actions for Taxes model.
 */
class TaxesController extends Controller
{
    public $layout = "/inspinia";
    
    public $particulier;
	
	public $old_taxeDescription;
	
	public $message_default_taxe_u = false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Taxes models.
     * @return mixed
     */
    public function actionIndex()
    {
	    if(Yii::$app->user->can('billings-taxes-index'))
         {
	        $acad=Yii::$app->session['currentId_academic_year'];
	        
	        $searchModel = new SrcTaxes();
	        $dataProvider = $searchModel->search($acad,Yii::$app->request->queryParams);
	        
	        if(isset($_GET['SrcTaxes']))
					$dataProvider= $searchModel->search($acad,Yii::$app->request->queryParams);
			
			if (isset($_GET['pageSize'])) 
							         	 {
									        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
									          unset($_GET['pageSize']);
										   }
	
		        return $this->render('index', [
		            'searchModel' => $searchModel,
		            'dataProvider' => $dataProvider,
		        ]);
		        
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single Taxes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {  
    	 if(Yii::$app->user->can('billings-taxes-view'))
         {
	       
	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
	     
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Taxes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('billings-taxes-create'))
         {
	       
        
        $acad=Yii::$app->session['currentId_academic_year'];
        $model = new SrcTaxes();

         if ($model->load(Yii::$app->request->post()) ) 
	      {
                $model->setAttribute('academic_year', $acad);
                
                if(isset($_POST['create']))
                 {
				
					if($model->save())
						{  
							$prosper_marc_hilaire_poulard =0;
								     	//si se premye peryod pou ane a, tou anpeche migration peryod
								     	//return id,date_start,date_end
 											$all_taxes = SrcTaxes::find()->where(['academic_year'=>$acad])->all();
 											foreach($all_taxes as $t)
 											  {
 											  	 $prosper_marc_hilaire_poulard ++;
 											   }
								     	 if($prosper_marc_hilaire_poulard==1) //plis ke 1 se ke li bloke deja
								     	   {
								     	   	  $command_yearMigrationCheck = Yii::$app->db->createCommand();
									           $command_yearMigrationCheck->update('year_migration_check', ['taxes' => 1, 'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')  ], ['academic_year'=>$acad ])
									         ->execute();
								     	   	} 
								     	   	
							return $this->redirect(['index','wh'=>'set_ta' ]);
						}
						
                 }
                 
	        } 
	       
	       
	            return $this->render('create', [
	                'model' => $model,
	            ]);
	            
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

	        
    }

    /**
     * Updates an existing Taxes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('billings-taxes-update'))
         {
	        $this->message_default_taxe_u = false;
		
			$model = $this->findModel($id);
			
			$this->old_taxeDescription= $model->taxe_description;
			
		  if(($this->old_taxeDescription=="IRI"))        
	        { //$this->message_default_taxe_u = true;
	           
	              Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','-IRI- cannot be either updated nor deleted.') ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	  
	        }
	             
			      	       
			      	      	            
		if ($model->load(Yii::$app->request->post()) ) 
	      {
                
                if(isset($_POST['update']))
                 {
                 	if(($this->old_taxeDescription!="IRI"))
				     { 
				         if($this->old_taxeDescription=="TMS")
				          { $model->setAttribute('taxe_description',"TMS");
				             $model->setAttribute('employeur_employe',1);
				          }
				         if($this->old_taxeDescription=="ONA")
				          {  $model->setAttribute('taxe_description',"ONA");
				             $model->setAttribute('employeur_employe',0);
				          	
				          	}
				          	
				         if($this->old_taxeDescription=="BONIS")
				          {  $model->setAttribute('taxe_description',"BONIS");
				             $model->setAttribute('employeur_employe',0);
				          	
				          	}
				          	
						if($model->save())
							return $this->redirect(['index','wh'=>'set_ta' ]);
                    }
                 else
                   {  //$this->message_default_taxe_u = true;
                        Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','-IRI- cannot be either updated nor deleted.') ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
                    }

                 }
                 
	      }
	      	      	            
	/*	
		 if($this->message_default_taxe_u == true)
            {
            	Yii::$app->getSession()->setFlash('info', [
										    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','-IRI- cannot be either updated nor deleted.') ),
										    'title' => Html::encode(Yii::t('app','Info') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				
				$explode_url=Yii::$app->request->referrer;
            	
				$url= explode("/",substr($explode_url,1));
            
                 $view=$url[7];
			
				return $this->redirect([$view,'wh'=>'set_ta' ]);
             
             }	      	      	            
*/
        
            return $this->render('update', [
                'model' => $model,
                
            ]);
            
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }

        
    }



    /**
     * Deletes an existing Taxes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('billings-taxes-delete'))
         {
	        try {
   			   $model=$this->findModel($id);
   			   
   			   $taxeDesc= $model->taxe_description;
	        
	        
            if(($taxeDesc!="IRI")&&($taxeDesc!="TMS")&&($taxeDesc!="ONA")&&($taxeDesc!="BONIS"))        
               {  
                 //$this->loadModel($id)->delete();
                 $model->delete();
                 
               }
             else
              {   // $this->message_default_taxe_u=true;
             	     Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','-IRI- cannot be either updated nor deleted. - TMS - ONA - BONIS - cannot be deleted.') ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
             	}
             	
           
             return $this->redirect(['index','wh'=>'set_ta']);
             
           } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                         return $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}   
             
             
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Taxes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Taxes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcTaxes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
