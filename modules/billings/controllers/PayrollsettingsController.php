<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\PayrollSettings;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\PayrollSettingTaxes;
use app\modules\billings\models\SrcPayroll;



use app\modules\fi\models\SrcPersons;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * PayrollsettingsController implements the CRUD actions for PayrollSettings model.
 */
class PayrollsettingsController extends Controller
{
    public $layout = "/inspinia";
    
    public $person_id;
	public $group;
	public $an_hour;
	public $grouppayroll=1;
	//public $as_;
	public $as_;
	public $old_new;
	
	public $message_noAmountEntered=false;
	public $message_eitherEmployeeNorTeacher=false;
	public $message_notEmployee=false;
	public $message_notTeacher=false;
	public $message_employeeOrTeacher=false;
	public $message_asAlreadySetAs=false;
	

   
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollSettings models.
     * @return mixed
     */
    public function actionIndex()
    {
	     if(Yii::$app->user->can('billings-payrollsettings-index'))
         {
	        $acad=Yii::$app->session['currentId_academic_year'];
	        
	        $searchModel = new SrcPayrollSettings();
	        $dataProvider = $searchModel->search($acad,Yii::$app->request->queryParams);
	        
	        if(isset($_GET['SrcPayrollSettings']))
			   $dataProvider= $searchModel->search($acad,Yii::$app->request->queryParams);
			
			if (isset($_GET['pageSize'])) 
							         	 {
									        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
									          unset($_GET['pageSize']);
										   }
	
	
			        return $this->render('index', [
		            'searchModel' => $searchModel,
		            'dataProvider' => $dataProvider,
		        ]);
		        
		   }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single PayrollSettings model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	      if(Yii::$app->user->can('billings-payrollsettings-view'))
         {   return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new PayrollSettings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	     if(Yii::$app->user->can('billings-payrollsettings-create'))
         {
	        $acad=Yii::$app->session['currentId_academic_year'];
	        
	        $payroll_setting_twice = infoGeneralConfig('payroll_setting_twice');
	        
	        $model = new SrcPayrollSettings();
	        
	        $modelPS = new SrcPayrollSettings();
	        $modelPers = new SrcPersons();
	        $modelTaxe = new SrcTaxes();
	        
	        if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
			  $model->group = 1;
	        
	
        if ($model->load(Yii::$app->request->post()) ) 
	      {
               if(isset($_POST['SrcPayrollSettings']['group']))
			  {
                        $model->group = $_POST['SrcPayrollSettings']['group'];
                    }
                    
		          
		          if($model->group==1)
		            {  
		            	if(isset($_POST['SrcPayrollSettings']['group_payroll']))
						  {
	                        $model->group_payroll = $_POST['SrcPayrollSettings']['group_payroll'];
	                      }
		            }


                
                if(isset($_POST['create']))
                 {
                 	 
                 	 
                 	 if($model->group==0)
					  {
					  	$condition = '($model->as_!=null)&&($model->devise!=null)&&($model->amount!=null)';
					  	
					  	 if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
			              { $model->setAttribute('as_', 0);
			                 $condition = '(($model->devise!=null)&&($model->amount!=null)';
			              }
			               
					  	      $this->as_ = $model->as_;

            	if( $condition )
					  { 
		
					  	//return id PayrollSettings
					  	$ifAsAlreadySetAs = $modelPS->ifAsAlreadySetAs($model->person_id, $this->as_, $acad);
					  	
					  if($ifAsAlreadySetAs==null)	
					  	{
						  	if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
			                   $employee_or_teacher = 0;
			               elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
			                  {  //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
						  	     $employee_or_teacher = $modelPers->isEmployeeOrTeacher($model->person_id, $acad);	
						  	     
			                   }
					  	
						  	if($employee_or_teacher!= -1)
						  	 {
					  	 	
							  	if(($employee_or_teacher==2)||($model->as_==$employee_or_teacher))
							  	 {
								  	if($model->number_of_hour !='') //if(isset($_POST['PayrollSettings']['an_hour']))
							           $this->an_hour = 1; // $_POST['PayrollSettings']['an_hour'];
							        else
							          { $this->an_hour = 0;
							              $model->number_of_hour = 0;
							            }
				                    
							  	
							  	$model->setAttribute('an_hour',$this->an_hour);
							  	$model->setAttribute('academic_year',$acad);
							  	$model->setAttribute('old_new',1);
							  	$model->setAttribute('date_created',date('Y-m-d H:i:s'));
							  	$model->setAttribute('created_by',currentUser());
							  	
							  	 	if($model->save())
									  {
									      
										$taxe = $modelTaxe->searchTaxesForPS($acad);
										     		
							             if($taxe!=null)
										   {  
										   	   $i=0;
										   	   
											  foreach($taxe as $id_taxe)
											    {     
											    	//$taxe_description = $id_taxe["id"];
											    	
											    	$modelTaxes= new SrcTaxes();
											    	
										 	          if(isset($_POST[$id_taxe["id"]]))
										       	 	    {
										       	 	    	$taxeID=$_POST[$id_taxe["id"]];
										       	 	    	
															   $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																      	  
															   $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$model->id);
															   $modelPayrollSettingTaxe->setAttribute('id_taxe',$taxeID);
																	      	  
															    $modelPayrollSettingTaxe->save();
																	      	  
															   unSet($modelPayrollSettingTaxe);
															   //$modelPayrollSettingTaxe = new PayrollSettingTaxes;
															   
															   unSet($modelTaxes);
																	      	
										       	 	      }
										       	 	      
										       	 	      
																	        						
											       }
																		
							                  }					   
			
									     
									  	return $this->redirect(['index','wh'=>'set_pa','from'=>'']);
									  	
									   }
								     else
										  $model->setAttribute('academic_year','');
							  	      
							  	      }
							  	     else
							  	       {
							  	       	  if($model->as_==0)
							  	       	      { //$this->message_notEmployee=true;
							  	       	          Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is not an employee.', ['name'=>$model->person->getFullName() ] ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							  	       	      }
							  	       	  elseif($model->as_==1)
							  	       	      { //$this->message_notTeacher=true;
							  	       	          Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is not a teacher.', array('name'=>$model->person->getFullName() ) ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							  	       	      }
							  	       	 }
									
						        }
						       else
						          {   //$this->message_eitherEmployeeNorTeacher=true;
						          Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is not an employee either a teacher.', array('name'=>$model->person->getFullName() ) ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
										
						           }
					  	
					  	     }
					  	   else
					  	      {  //$this->message_asAlreadySetAs=true;
					  	          Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} is already set as {name1}.', array('name'=>$model->person->getFullName(), 'name1'=>$model->getAsValue() ) ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
										
					  	      }
					        
					       }
					     //else do nothing
					        
					        
							
					    }
					 elseif($model->group==1)
						{
						
						     //$this->message_noAmountEntered=false;
						   $no_pers_has_amount=true;
						   $temwen=false;
						   $person='';
						   $as_ = 0;
						   
						   if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
						     {
							   if( $model->group_payroll==1)
							        $as_ = 0;
							    elseif($model->group_payroll==2)
							           $as_ = 1;
						     }
						           
						   
						        $ok=true;
	                             $model_new = new SrcPayrollSettings();
	                   
					
							   
							  foreach($_POST['id_pers'] as $id)
		                        {   	   
								           if(isset($_POST['amount'][$id])&&($_POST['amount'][$id]!=''))
								                $no_pers_has_amount=false;
									
								}
                      
					
					  
					      if($no_pers_has_amount==false) // omwen 1 etidyan gen not
						    {   foreach($_POST['id_pers'] as $id)
						         {   	   
									if( ( (isset($_POST['amount'][$id]))&&($_POST['amount'][$id]>0) )||( (isset($_POST['frais'][$id]))&&($_POST['frais'][$id]>0) ) )  
									  {
									     $modelPersons = SrcPersons::findOne($id);
									     
									     if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
									        $employee_or_teacher = 0;
									     elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
									        {   //return 0 when employee, 1 when teacher; return 2 when either employee nor teacher
									  	         $employee_or_teacher = $modelPers->isEmployeeOrTeacher($id, $acad);	
									        }
									  	
									  	if($employee_or_teacher!=-1)
									  	 {
									  	 	
										  	if(($employee_or_teacher==2)||($as_==$employee_or_teacher))
										  	 {
	                                         $numberHour = 0;     
									           if(isset($_POST['numberHour'][$id]))
									             {   
									             	if($_POST['numberHour'][$id]!='')
									                  {    $this->an_hour=1;
									                     $numberHour = $_POST['numberHour'][$id];
									                   }
									                 else
									                   {  $this->an_hour=0;
									                   
									                     }
									              }
												else
													$this->an_hour=0; 
													
													
												//if(isset($_POST['amount'][$id]))
									                $amount=$_POST['amount'][$id];
									                
									                $devise = $_POST['devise'][$id];
									                
									                $frais = $_POST['frais'][$id];
									                
									                $assurance_v = $_POST['assurance_v'][$id];
												
								               
										       
												   $model_new->setAttribute('person_id',$id);
												   $model_new->setAttribute('amount',$amount);
												   $model_new->setAttribute('as_',$as_);
												   $model_new->setAttribute('old_new',1);
												   $model_new->setAttribute('devise',$devise);
												   $model_new->setAttribute('an_hour',$this->an_hour);
												   $model_new->setAttribute('number_of_hour',$numberHour);
												   $model_new->setAttribute('frais',$frais);
												   $model_new->setAttribute('assurance_value',$assurance_v);
												   $model_new->setAttribute('academic_year',$acad);
												   $model_new->setAttribute('date_created',date('Y-m-d H:i:s'));
												   $model_new->setAttribute('created_by',currentUser());
												   
												   if($model_new->save())
					                                 {  
					                                 	$payroll_set = $model_new->id;
					                                 	
					                                    $taxe = $modelTaxe->searchTaxesForPS($acad);
								     		
											             if($taxe!=null)
														   {  
														   	   $i=0;
														   	   
															  foreach($taxe as $id_taxe)
															    {     
															    	//$taxe_description = $id_taxe["id"];
															    	
															    	$modelTaxes= new SrcTaxes();
															    	
														 	          if(isset($_POST[$id_taxe["id"]]))
														       	 	    {
														       	 	    	$taxeID=$_POST[$id_taxe["id"]];
													       	 	    	
																			   $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																				      	  
																			   $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$payroll_set);
																			   $modelPayrollSettingTaxe->setAttribute('id_taxe',$taxeID);
																					      	  
																			    $modelPayrollSettingTaxe->save();
																					      	  
																			   unSet($modelPayrollSettingTaxe);
																			   //$modelPayrollSettingTaxe = new PayrollSettingTaxes;
																			   
																			   unSet($modelTaxes);
																					      	
														       	 	      }
														       	 	      
														       	 	      
																					        						
															       }
																					
										                      }	
							     				                                 	
					                                 	unSet($model_new);
													    $model_new = new SrcPayrollSettings;
													   
													   $temwen=true;
													   
													   
											         }
											         
											 
											  }
									  	     else
									  	       {
									  	       	  if($as_==0)
									  	       	      {  //$this->message_employeeOrTeacher=true;
									  	       	                   Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','Persons with a wrong status(employee or teacher) are not saved.' ) ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);
									  	       	        }
									  	       	  elseif($as_==1)
									  	       	      {  //$this->message_employeeOrTeacher=true;
									  	       	                Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','Persons with a wrong status(employee or teacher) are not saved.' ) ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);
									  	       	       }
									  	       	 }
											
								        }
								       else
								         {   //$this->message_eitherEmployeeNorTeacher=true;
						                           Yii::$app->getSession()->setFlash('warning', [
											    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
											    'duration' => 36000,
											    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
											    'message' => Html::encode( Yii::t('app','{name} is not an employee either a teacher.', array('name'=>$$modelPersons->getFullName()) ) ),
											    'title' => Html::encode(Yii::t('app','Warning') ),
											    'positonY' => 'top',   //   top,//   bottom,//
											    'positonX' => 'center'    //   right,//   center,//  left,//
											]);
										
								         }
	
											 
						              }
						        //  else pase l
				                     
								 }
	
							   
					        }
					      else //message vous n'avez entre aucune note
							{
								//$this->message_noAmountEntered=true;
								Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You must enter at least one AMOUNT.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
								
								}
								
						
						  if($temwen)
						    {
						    	
						       return $this->redirect(['index','wh'=>'set_pa','from'=>'']);
						     }
									
						 }   
					    
					    
					    
                 }
                 
	      }
        
        
            return $this->render('create', [
                'model' => $model,
                'message_noAmountEntered'=>$this->message_noAmountEntered,
            ]);
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing PayrollSettings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
 public function actionUpdate($id)
    {
       if(Yii::$app->user->can('billings-payrollsettings-update'))
         { 
         	 $acad=Yii::$app->session['currentId_academic_year'];
         	 
         	 $payroll_setting_twice = infoGeneralConfig('payroll_setting_twice'); 
               
                 $is_payroll_done = 0;
        
        $modelPers = new SrcPersons;
        
        $modelTaxe = new SrcTaxes;
        
		$new_payroll_setting = new SrcPayrollSettings;
		
		
		
		
		if(!isset($_GET['all']))
	      {
			$model = $this->findModel($id);
		   //gade si yo fe payroll ak config sa deja pou ane a
                        $modelParoll = new SrcPayroll;
                        //return 1 si l gen payroll pou li deja; 0 si poko genyen
                        $is_payroll_done = $modelParoll->isPayrollAlreadyDone($id,$acad);
                        
			   
	        }
	      else
	        {
	        	$model=new SrcPayrollSettings;
                $condition='';
                
                if($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	              {
                        if($_GET['all']=='t')//teacher
                                 {
                                    $condition='is_student=0 AND id IN(SELECT DISTINCT teacher FROM courses c  WHERE (c.academic_period='.$acad.') ) AND active IN(1, 2) ';
                                 }
                               elseif($_GET['all']=='e')//employee
                                  { $condition='is_student=0 AND active IN(1, 2) AND p.id NOT IN(SELECT teacher FROM courses c WHERE (academic_period='.$acad.') ) ';
                               
                                  }
                                  
	              }
	            elseif($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	              {
	              	$condition='is_student=0 AND active IN(1, 2) '; //tout moun
	              }

	          
	          
	          }



         // if ($model->load(Yii::$app->request->post()) ) 
	    //  {
	    	        if(isset($_POST['update']))
					   {	
					   	   
					   	    $new_payroll_setting = new SrcPayrollSettings();
					   	    
						if(!isset($_GET['all']))
							  {  
							       $payroll_setting_as = null;	
                                                    //iuoiuoio
							if($is_payroll_done==1)  	
                                                          {	     $new_payroll_setting->load(Yii::$app->request->post());
                                                                 $payroll_setting_as = $new_payroll_setting->as_;
                                                          }
                                                       elseif($is_payroll_done==0)  	
                                                          {	     $model->load(Yii::$app->request->post());
                                                                 $payroll_setting_as = $model->as_;
                                                          }
                                                          
							  	
							  	
							  	$new_payroll_setting->setAttribute('person_id',$model->person_id);
							  	
							  	if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                               $employee_or_teacher = 0;
	                            elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                              {//return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
							  	     $employee_or_teacher = $modelPers->isEmployeeOrTeacher($new_payroll_setting->person_id, $acad);
	                              }	
						  	
							  	if($employee_or_teacher!= -1)				  	 
							  	  {
							  	 	
								  	if(($employee_or_teacher==2)||($payroll_setting_as==$employee_or_teacher))
								  	 {
									  	if($is_payroll_done==1)  
                                                                                 {
                                                                                      $new_payroll_setting->setAttribute('academic_year',$acad);
										  	if(($new_payroll_setting->number_of_hour!='')&&($new_payroll_setting->number_of_hour!=0))
										  	  $new_payroll_setting->setAttribute('an_hour',1);
										  
										  	$new_payroll_setting->setAttribute('person_id',$model->person_id);  
										  	$new_payroll_setting->setAttribute('old_new',1);
										  	$new_payroll_setting->setAttribute('date_created',date('Y-m-d H:i:s'));
										  	$new_payroll_setting->setAttribute('created_by',currentUser());
										  	
										  	
			
									  		if($new_payroll_setting->save())
											  {
											  	   $model->setAttribute('old_new',0);
									  	           $model->setAttribute('date_updated',date('Y-m-d H:i:s'));
									  	           $model->setAttribute('updated_by',currentUser());
									  	           $model->save();
									  	           
									  	           unSet($model);
							                              $model=new SrcPayrollSettings;
							                                 	    
											  	   
											  	       $id_payroll_set=$new_payroll_setting->id;
									                   $payrollSettingTaxe=new PayrollSettingTaxes;
												 		  //supprimer tous les   de la personne ds la table 'payroll_setting_taxes'
												 //$payrollSettingTaxe::model()->deleteAll('id_payroll_set=:IdPayrollSet',array(':IdPayrollSet'=>$new_payroll_setting->id ));
					
					        	
									                  $taxe = $modelTaxe->searchTaxesForPS($acad);
												     		
										             if($taxe!=null)
													   {  
													   	   $i=0;
													   	   
														  foreach($taxe as $id_taxe)
														    {     
													    	//$taxe_description = $id_taxe["id"];
													    	
													     
												 	          if(isset($_POST[$id_taxe["id"]]))
														       	 	    {
														       	 	    	$taxeID=$_POST[$id_taxe["id"]];
												       	 	    	
																	   $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																		      	  
																	   $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$new_payroll_setting->id);
																	   $modelPayrollSettingTaxe->setAttribute('id_taxe',$taxeID);
																			      	  
																	    $modelPayrollSettingTaxe->save();
				 															      	  
																	   unSet($modelPayrollSettingTaxe);
																			      	
												       	 	       }
												       	 	      
												       	 	      
																			        						
													            }
																				
									                         }	
					
												  	return $this->redirect(['index','wh'=>'set_pa','from'=>'']);
												  	
												  }
												else
												  $new_payroll_setting->setAttribute('academic_year',$acad);
											 
                                                                                            }
								  	           elseif($is_payroll_done==0) 
                                                                                   {
                                                                                       
                                                                                      if(($model->number_of_hour!='')&&($model->number_of_hour!=0))
										  	  $model->setAttribute('an_hour',1);
										  
										  	$model->setAttribute('date_updated',date('Y-m-d H:i:s'));
										  	$model->setAttribute('updated_by',currentUser());
										  	
										  	
			
									  		if($model->save())
											  {	   
											  	       $id_payroll_set=$model->id;
									                   $payrollSettingTaxe=new PayrollSettingTaxes;
												 		  //supprimer tous les   de la personne ds la table 'payroll_setting_taxes'
												 $command0 = Yii::$app->db->createCommand();
                                                                                                    $command0->delete('payroll_setting_taxes', ['id_payroll_set'=>$id_payroll_set ])
                                                                                                                ->execute();
					
					        	
									                  $taxe = $modelTaxe->searchTaxesForPS($acad);
												     		
										             if($taxe!=null)
													   {  
													   	   $i=0;
													   	   
														  foreach($taxe as $id_taxe)
														    {     
													    	//$taxe_description = $id_taxe["id"];
													    	
													     
												 	          if(isset($_POST[$id_taxe["id"]]))
														       	 	    {
														       	 	    	$taxeID=$_POST[$id_taxe["id"]];
												       	 	    	
																	   $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																		      	  
																	   $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$id_payroll_set);
																	   $modelPayrollSettingTaxe->setAttribute('id_taxe',$taxeID);
																			      	  
																	    $modelPayrollSettingTaxe->save();
				 															      	  
																	   unSet($modelPayrollSettingTaxe);
																			      	
												       	 	       }
												       	 	      
												       	 	      
																			        						
													            }
																				
									                         }	
					
												  	return $this->redirect(['index','wh'=>'set_pa','from'=>'']);
												  	
												  }
												else
												  $model->setAttribute('academic_year',$acad);
                                                                                       
                                                                                       
                                                                                       
                                                                                       
                                                                                   }
								  	          
											 
											 
											 
											 
											  
										
										}
								  	     else
								  	       {
								  	       	  if($new_payroll_setting->as_==0)
								  	       	      {   //$this->message_notEmployee=true;
								  	       	           Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','{name} is not an employee.', array('name'=>$model->person->getFullName() ) ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
								  	       	      }
								  	       	  elseif($new_payroll_setting->as_==1)
								  	       	      {  //$this->message_notTeacher=true;
								  	       	          Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','{name} is not a teacher.', array('name'=>$model->person->getFullName() ) ) ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
										  	      }
								  	       	 }
										
							        }
							       else
							         { //$this->message_eitherEmployeeNorTeacher=true;
							             Yii::$app->getSession()->setFlash('warning', [
											    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
											    'duration' => 36000,
											    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
											    'message' => Html::encode( Yii::t('app','{name} is not an employee either a teacher.', array('name'=>$model->person->getFullName()) ) ),
											    'title' => Html::encode(Yii::t('app','Warning') ),
											    'positonY' => 'top',   //   top,//   bottom,//
											    'positonX' => 'center'    //   right,//   center,//  left,//
											]);
											
							           }
							         
							
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
                                                                   
							   }
							 else
								{
								
								     $this->message_noAmountEntered=false;
								        $temwen=false;
								        $person='';
								  
								 
								  
								  foreach($_POST['id_ps'] as $id)
					                 {   	   
										 	
											             $numberHour = 0;
											             $frais=null;
											             $assurance_v=null;     
											           if(isset($_POST['numberHour'][$id]))
											             {   
											             	if($_POST['numberHour'][$id]!='')
											                  {    $this->an_hour=1;
											                     $numberHour = $_POST['numberHour'][$id];
											                   }
											                 else
											                     $this->an_hour=0;
											              }
														else
															$this->an_hour=0; 
											                     	
															if(isset($_POST['amount'][$id]))
											                   $amount=$_POST['amount'][$id];
											                   
											                if(isset($_POST['frais'][$id]))
											                   $frais=$_POST['frais'][$id];
											                   
											                if(isset($_POST['assurance_v'][$id]))
											                   $assurance_v=$_POST['assurance_v'][$id];
														
																					  		
											  	    
								                          $new_payroll_setting->setAttribute('academic_year',$acad);
														   $new_payroll_setting->setAttribute('old_new',1);
														     $new_payroll_setting->setAttribute('amount',$amount);
								                         	    $new_payroll_setting->setAttribute('an_hour',$this->an_hour);
								                         	     $new_payroll_setting->setAttribute('number_of_hour',$numberHour);
								                         	     $new_payroll_setting->setAttribute('frais',$frais);
								                         	     $new_payroll_setting->setAttribute('assurance_value',$assurance_v);
								                         	     $new_payroll_setting->setAttribute('date_created',date('Y-m-d H:i:s'));
									  	                        $new_payroll_setting->setAttribute('created_by',currentUser());
			
			
													 if($new_payroll_setting->save())
							                                 {  	 
							                                 	
							                                 	   $model=SrcPayrollSettings::findOne($id);
															  	   $model->setAttribute('old_new',0);
													  	           $model->setAttribute('date_updated',date('Y-m-d H:i:s'));
													  	           $model->setAttribute('updated_by',currentUser());
													  	           $model->save(); 
													  	           
													  	            unSet($model);
							                                 	    $model=new SrcPayrollSettings;
									  	           
									  	                
									  	                 $person=$new_payroll_setting->id;
							                                 	
							                                 	
							                                 	
											                   $payrollSettingTaxe=new PayrollSettingTaxes;
																  //supprimer tous les   de la personne ds la table 'payroll_setting_taxes'
														 //$payrollSettingTaxe::model()->deleteAll('id_payroll_set=:IdPayrollSet',array(':IdPayrollSet'=>$new_payroll_setting->id ));
							
							        	
											                  $taxe = $modelTaxe->searchTaxesForPS($acad);
										     		
										             if($taxe!=null)
													   {  
													   	   $i=0;
													   	   
														  foreach($taxe as $id_taxe)
														    {     
													    	//$taxe_description = $id_taxe["id"];
													    	
													    	
												 	          if(isset($_POST[$id_taxe["id"]]))
												       	 	    {
												       	 	    	$taxeID=$_POST[$id_taxe["id"]];
												       	 	    	
																	   $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																		      	  
																	   $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$new_payroll_setting->id);
																	   $modelPayrollSettingTaxe->setAttribute('id_taxe',$taxeID);
																			      	  
																	    $modelPayrollSettingTaxe->save();
																			      	  
																	   unSet($modelPayrollSettingTaxe);
																	   
																			      	
												       	 	       }
												       	 	      
												       	 	      
																			        						
													            }
																				
									                         }	
			
							                                 	
							                                 	unSet($new_payroll_setting);
															   $new_payroll_setting= new SrcPayrollSettings;
															   
															   $temwen=true;
															   
															   
													         }
													 	
						                     
											   }
										   
							       
								  if($temwen)
								    {
								    	if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
	                                        return $this->redirect(['view','id'=>$person,'all'=>'e','wh'=>'set_pa','from'=>'']);
	                                     elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
	                                      {
									    	if($_GET['all']=='e')
									    	   return $this->redirect(['view','id'=>$person,'all'=>'e','wh'=>'set_pa','from'=>'']);
									    	elseif($_GET['all']=='t')
									    	   return $this->redirect(['view','id'=>$person,'all'=>'t','wh'=>'set_pa','from'=>'']);
									    	   
	                                      }
								
								     }
											
								 }
					       
					        }
	      	
	        
	        
	        
	       // }
               
        
        
        
        
            return $this->render('update', [
                'model' => $model,
                'message_noAmountEntered'=>$this->message_noAmountEntered,
            ]);
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing PayrollSettings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	    if(Yii::$app->user->can('billings-payrollsettings-delete'))
         {   
         	try{
         	 $this->findModel($id)->delete();
	
	          return $this->redirect(['index','wh'=>'set_pa']);
	          
	          } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
	      }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Finds the PayrollSettings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PayrollSettings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcPayrollSettings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
