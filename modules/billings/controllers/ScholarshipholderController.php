<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\ScholarshipHolder;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\billings\models\SrcPartners;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBalance;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * ScholarshipholderController implements the CRUD actions for ScholarshipHolder model.
 */
class ScholarshipholderController extends Controller
{
     public $layout = "/inspinia";
     
     public $part = 'set_sh';
	public $is_internal;
	public $student_id;
        public $school_name = null;
        
        
        public $internal = [];
        public $percentage_pay = [];
        public $feesLabel= []; //
        public $is_full = null;
        public $number_row;
        
        public $student_name = null;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScholarshipHolder models.
     * @return mixed
     */
    public function actionIndex()
    {
	     if(Yii::$app->user->can('billings-scholarshipholder-index'))
         {
	        $searchModel = new SrcScholarshipHolder();
	        $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
	        
	        if(isset($_GET['SrcScholarshipHolder']))
					$dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
			
			if (isset($_GET['pageSize'])) 
							         	 {
									        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
									          unset($_GET['pageSize']);
										   }
	
			   $form = 'index';
			
			      // if(isset($_GET['from']))
				//	    $form = 'index_out';
					    
		        return $this->render($form, [
		            'searchModel' => $searchModel,
		            'dataProvider' => $dataProvider,
		        ]);
        
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single ScholarshipHolder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-scholarshipholder-view'))
         {   return $this->render('view', [
            'model' => $this->findModel($id),
              ]);
           
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   
           
    }

    /**
     * Creates a new ScholarshipHolder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('billings-scholarshipholder-create'))
         {
         	 $model = new SrcScholarshipHolder();
         	 
         	 $model->setAttribute('date_created', date('Y-m-d H:m:s'));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   
         
         
         
    }


 public function actionMasscreate()
    {
       if(Yii::$app->user->can('billings-scholarshipholder-masscreate'))
         { $acad=Yii::$app->session['currentId_academic_year']; 

            $number_line = 6;
            
            $model = new SrcScholarshipHolder;
            
            $sponsor = [];
            $fee = [];
            //$internal_=[];
            //$percentage_pay = [];
            $duplicate_full_scholar = 0;//false
            $error_report = false;
			
			$nbr_line =0;
             
            $this->school_name = infoGeneralConfig('school_name');
          
        if ($model->load(Yii::$app->request->post()) ) 
	     {
                  
            if(isset($_POST['SrcScholarshipHolder']['student']))
            {
                $this->student_name = $_POST['SrcScholarshipHolder']['student'];
                $model->student = $this->student_name;
                $model->is_full = 0; 
            }else{
                $this->student_name = null;
                $model->student = $this->student_name;
                
            }
            
            if(isset($_POST['SrcScholarshipHolder']['is_full']) && $_POST['SrcScholarshipHolder']['is_full']==1)
			{
                 $model->is_full = 1; 
                 
              if($this->student_name!='')		  
			    {  //tcheke si elev sa poko fe tranzakasyon menm ni lot bous pou aksepte full bous   
                      $sql = "SELECT * FROM billings WHERE student=".$this->student_name." AND academic_year=".$acad;
                      $result = Yii::$app->db->createCommand($sql)->queryAll();
                     
                      $sql_1 = "SELECT * FROM scholarship_holder WHERE student=".$this->student_name." AND academic_year=".$acad;
                      $result_1 = Yii::$app->db->createCommand($sql_1)->queryAll();
                      
					  if(($result==null)&&($result_1==null))
                       {
						  $this->is_full = 1;
						  $model->is_full = 1;
						  $this->number_row = 1;
						  $model->setAttribute('percentage_pay', 100);
					   }
					  else
						  {  $this->number_row = $number_line;
						   $model->is_full = 0;
							  
							  Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','He/She cannot have a full scholarship!') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						  }
						  
				}
			   else
				   {
						$this->number_row = $number_line;
				   }
				
            }else{
                $this->is_full = 0;
                $model->is_full = 0; 
                $this->number_row = $number_line;
            }
            
            //$this->internal = Yii::$app->session["internal_scholar"];
           for($i=0;$i<$this->number_row;$i++)
           	   { $this->internal[$i]=null;
           	       $this->percentage_pay[$i] =null;
           	     }
           	   
           for($i=0;$i<$this->number_row;$i++){
               
                
	              if(isset($_POST['SrcScholarshipHolder']['is_internal'][$i] ))
	              {
		                if($_POST['SrcScholarshipHolder']['is_internal'][$i]!=$this->internal[$i]){
		                   $this->internal[$i] = $_POST['SrcScholarshipHolder']['is_internal'][$i];
		                    
		                }
		               
	               }
	              else
	                $this->internal[$i]=0;
	              
	              if(isset($_POST['SrcScholarshipHolder']['percentage_pay'][$i] ))
	              {
		                if($_POST['SrcScholarshipHolder']['percentage_pay'][$i]!=$this->percentage_pay[$i]){
		                   $this->percentage_pay[$i] = $_POST['SrcScholarshipHolder']['percentage_pay'][$i];
		                    
		                }
		               
	               }
	              else
	                $this->percentage_pay[$i]=0;
	                
	              
	             
              }
 
           //Yii::$app->session["internal_scholar"] = $this->internal;
            
            

         
                  
                  
               if(isset($_POST['create']))
	            {   
	            	$model2record = new SrcScholarshipHolder;
	            	
	            	//gad konbyen liy % la defini pou yo
				  for($i=0;$i<$this->number_row;$i++)
				    {  
				    	if($this->is_full == 0)
				    	  {  if(isset($_POST['SrcScholarshipHolder']['percentage_pay'][$i])&&($_POST['SrcScholarshipHolder']['percentage_pay'][$i]!=''))
						       $nbr_line++;
				    	  }
				    	 else
				    	    $nbr_line=1;
					 }
                
               for($j=0;$j<$this->number_row;$j++)
				{
                   //  echo '<br/><br/><br/>TEST _--------------> uuu';
                    if(isset($_POST['SrcScholarshipHolder']['percentage_pay'][$j]) && $_POST['SrcScholarshipHolder']['percentage_pay'][$j]!='')
					{
                       $error=0;
                       $fee[$j] = NULL;
                       
                        $model2record->setAttribute('student', $model->student);
                        
                        if(isset($_POST['SrcScholarshipHolder']['partner'][$j]))
                         {
                           $sponsor[$j] = $_POST['SrcScholarshipHolder']['partner'][$j];
                          }
                        
                        if(isset($_POST['SrcScholarshipHolder']['fee'][$j])){
                            $fee[$j] = $_POST['SrcScholarshipHolder']['fee'][$j];
                        }
                        
                        
                        $this->percentage_pay[$j] = $_POST['SrcScholarshipHolder']['percentage_pay'][$j];
                        
                        if($this->internal[$j]==0){
                            $model2record->setAttribute('partner',$sponsor[$j]);
                            $model2record->setAttribute('is_internal', 0);
                        }else{
                            $model2record->setAttribute('partner',null);
                            $model2record->setAttribute('is_internal', 1); 
                        }
                        
                       /* if($this->is_full==1){
                            $model2record->setAttribute('fee',null);
                        }else{
                            $model2record->setAttribute('fee',$fee[$j]);
                        }
                       */ 
                        if(($fee[$j] == NULL)||($model->is_full==1))
							   {  //fee pa ka null si gen lot bous sou lot fre deja ou si li gentan gen tranzaksyon deja 
								  $sql = "SELECT * FROM billings WHERE student=".$model->student." AND academic_year=".$acad;
								  $result = Yii::$app->db->createCommand($sql)->queryAll();
								  
								  
								  $sql_1 = "SELECT * FROM scholarship_holder WHERE student=".$model->student." AND academic_year=".$acad;
								  $result_1 = Yii::$app->db->createCommand($sql_1)->queryAll();
								  
						          if(($result==null)&&($result_1==null))
							        $model2record->setAttribute('fee', NULL);
								  else
									{  $error=1;
									
									}
							   }
							 else
							   $model2record->setAttribute('fee', $fee[$j]);
							   

                        
                        //return 0:false or 1:true   
                        if($this->validateScholarship($model->student, $acad)==0) //si l pa gen full bous deja
                          {								  
							  if($error==0)  
							  {  
							  	$id_bil='';
                                                                $bil_mod = new SrcBillings();
							  	$bil_ = NULL;
							  	 //tcheke si fre a/yo gen tranzaksyon sou li/yo deja
								  if($fee[$j]!=NULL)
                                                                    {  //$bil_ = $bil_mod->isFeeAlreadyUse($model->student,$fee[$j], $acad);
                                                                      $bil_ = $bil_mod->isFeeAlreadyUse_new($model->student,$fee[$j], $acad);
                                                                    }
                                                                  
                                                    	$pass=1;							  
							  if($bil_ !=NULL)
							    {   
                                                               if($bil_['amount_pay']>0)
                                                               { $error=2;
                                                                  $pass=0;
                                                               }
                                                               else
								   $id_bil= $bil_['id'];       
							      }
                                                              
							  if($pass==1)
							    {  
								  	if($model->is_full == 1)
								  	   $model2record->setAttribute('percentage_pay', $model->percentage_pay);
								  	else
								  	  $model2record->setAttribute('percentage_pay', $this->percentage_pay[$j]);
								  	  
			                        $model2record->setAttribute('academic_year', $acad);
			                        $model2record->setAttribute('date_created', date('Y-m-d H:m:s'));
			                        $model2record->setAttribute('create_by', currentUser());
									   
	                                  $result_2=null;
	                                  
	                                  if($fee[$j]!=NULL)
	                                   {  $sql_2 = "SELECT * FROM scholarship_holder WHERE student=".$model->student." AND fee=".$fee[$j]." AND academic_year=".$acad;
											  $result_2 = Yii::$app->db->createCommand($sql_2)->queryAll();
										}
							   
							          if($result_2==null)
									  {
								  
										   if( ($model->is_full==1) || (($model->is_full==0)&&($fee[$j]!=NULL)&&($nbr_line >1)) || (($model->is_full==0)&&($nbr_line ==1)) ) 
											{  
												if($model2record->save()){
                                                                                                    if($id_bil !=NULL) // update amount_to_pay ak balance
							                                              { 
                                                                                                        $model2record->percentage_pay;
                                                                                                        $modelFee = SrcFees::findOne($fee[$j]);
                                                                                                        $amount = $modelFee->amount - (($modelFee->amount * $model2record->percentage_pay)/100) ;
                                                                                                        
                                                                                                        $new_balance = $modelFee->amount;
                                                                                                        
                                                                                                           $modelBilling = SrcBillings::findOne($id_bil);
                                                                                                           $modelBilling->amount_to_pay= $amount;
                                                                                                           $modelBilling->balance= $amount;
                                                                                                           
                                                                                                           if( $modelBilling->save() ) //update balance tab balans lan
                                                                                                           {
                                                                                                               $modBal= SrcBalance::find()->where('student='.$modelBilling->student.' and devise='.$modelFee->devise)->all();
                                                                                                                $id_bal = '';
                                                                                                               if($modBal!=null)
                                                                                                                { foreach($modBal as $r)
                                                                                                                    {  $new_balance = $r->balance - $amount;
                                                                                                                        $id_bal = $r->id;

                                                                                                                    }
                                                                                                                }
                                                                                                                   
                                                                                                               $newBalance = SrcBalance::findOne($id_bal);
                                                                                                               $newBalance->balance = $new_balance;
                                                                                                               
                                                                                                               $newBalance->save();
                                                                                                           }
                                                                                                           
                                                                                                           
                                                                                                        }
				                                     $this->redirect(array('index?from=oth'));
											   }else
												 {
													$error_report = true;
												  }
											}
									  }
							    }
									  
							  }
							 elseif($error==1)
								 {
									   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','This decision cannot be applied to all fees!') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

								 }
								 
								 
						     
								 
								  
                          }else
                             {
                               // $duplicate_full_scholar = 1;
                             }
                      }
                   unset($model2record);
                    $model2record = new SrcScholarshipHolder;
                }
                
                if($duplicate_full_scholar==1)
                  {
                    
                      
										Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','An Error Was Occuring, Probably A Try To Duplicate A Full Scholarship To A Student !') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                   }
                
                if($error_report)
                  {
                    
										Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','An error was occuring, probably a try to duplicate scholarship to a student !') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  }
                  
                    if($nbr_line>1)  
                        return $this->redirect(array('index?from=oth'));
                    else
                      { 
                      	     if($error==2)
                      	        {
                      	        	
										
										Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','We already have transaction(s) on this fee.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                      	        } 
                      	           
                      	     if($error==0)
                      	       return $this->redirect(array('index?from=oth'));
                      	}
        
                 }
	            
	            
	        } 
         
            return $this->render('gridcreate', [
                'model' => $model,
                'student_name'=>$this->student_name,
                'is_full'=>$this->is_full,
                'number_row'=>$this->number_row,
                'internal'=>$this->internal,
                'percentage_pay'=>$this->percentage_pay,
                'school_name'=>$this->school_name,
            ]);
        
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
 
        
        
    }

    /**
     * Updates an existing ScholarshipHolder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('billings-scholarshipholder-update'))
         {   
         	$acad=Yii::$app->session['currentId_academic_year']; 
              $school_name = infoGeneralConfig('school_name');
        
			$fee =NULL;
			
	    $model = $this->findModel($id);

       //tcheke si fre a/yo gen tranzaksyon sou li/yo deja
          $id_bil='';
          $bil_mod = new SrcBillings();
          $bil_ = NULL;
          
		  $fee_check = $model->fee;
                  $pousanjtaj_peye = $model->percentage_pay;
		   //$bil_ = $mod_bil->isFeeAlreadyUse($model->student,$fee_check, $acad);
                    $bil_ = $bil_mod->isFeeAlreadyUse_new($model->student,$fee_check, $acad);
                                                                    
                                                                  
                $pass=1;							  
                  if($bil_ !=NULL)
                   {   
                       if($bil_['amount_pay']>0)
                       { $error=2;
                          $pass=0;
                       }
                       else
                           $id_bil= $bil_['id'];       
                     }

                  if($pass==0)
                    {   
		        Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','This record is in used, it cannot be either updated nor deleted.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
		        
		        if(isset($_GET['from']))
					return $this->redirect('view?id='.$_GET['id'].'&from=oth');
				else
					return $this->redirect('index?from=oth');
		      }
		  elseif($pass==1)
		    {
			
			     $this->is_internal = $model->is_internal;
			
					 if($this->is_internal == 1)
							 { 
								$school_name = infoGeneralConfig('school_name');
										  $model->setAttribute('partner',$school_name);
								
								}

			

		   if ($model->load(Yii::$app->request->post()) ) 
	             {
					if(isset($_POST['SrcScholarshipHolder']['is_internal']))
					  {
								$this->is_internal = $_POST['SrcScholarshipHolder']['is_internal'];
								$model->is_internal =$this->is_internal;
								
						 }

					
					  
					
						if(isset($_POST['update']))
						 { 
						   
							if($this->is_internal == 1)
							  $model->setAttribute('partner', NULL);
							 					  
							  $model->setAttribute('fee', $fee_check);
							 
                                                          $percent =  $_POST['SrcScholarshipHolder']['percentage_pay'];
								   
									$model->setAttribute('percentage_pay', $percent);
									$model->setAttribute('update_by', currentUser() );
									$model->setAttribute('date_updated', date('Y-m-d H:i:s') );
								   
                                                                if($model->save())
                                                                 {
								     if($id_bil !=NULL) // update amount_to_pay ak balance
                                                                      { 
                                                                         
                                                                        $modelFee = SrcFees::findOne($fee_check);
                                                                        $amount = $modelFee->amount - (($modelFee->amount * $percent)/100) ;

                                                                        $new_balance = $modelFee->amount;

                                                                           $modelBilling = SrcBillings::findOne($id_bil);
                                                                           $modelBilling->amount_to_pay= $amount;
                                                                           $modelBilling->balance= $amount;

                                                                           if( $modelBilling->save() ) //update balance tab balans lan
                                                                           {
                                                                               $modBal= SrcBalance::find()->where('student='.$modelBilling->student.' and devise='.$modelFee->devise)->all();
                                                                                $id_bal = '';
                                                                                $val_bal = '';
                                                                               if($modBal!=null)
                                                                                { foreach($modBal as $r)
                                                                                    {  
                                                                                        $id_bal = $r->id;
                                                                                        $val_bal = $r->balance - $amount;

                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                if($percent > $pousanjtaj_peye)  //diminye balance lan
                                                                                 {
                                                                                    $amount_ = $modelFee->amount - (($modelFee->amount * ($percent-$pousanjtaj_peye) )/100) ;
                                                                                    $new_balance = $r->balance - $amount_;
                                                                                                                        
                                                                                  }
                                                                                elseif($percent < $pousanjtaj_peye) //ogmante balance lan
                                                                                  {
                                                                                      $amount_ = $modelFee->amount - (($modelFee->amount * ($pousanjtaj_peye-$percent) )/100) ;
                                                                                       $new_balance = $r->balance + $amount_;
                                                                                    }

                                                                               $newBalance = SrcBalance::findOne($id_bal);
                                                                               $newBalance->balance = $new_balance;

                                                                               $newBalance->save();
                                                                           }


                                                                        }	
                                                                    
                                                                    return $this->redirect(array('index?from=oth'));
								  }
								  
							  
						
						  }
					 
						
								  

				    }
							

            return $this->render('update', [
                'model' => $model,
                'internal'=>$this->is_internal,
                'percentage_pay'=>$this->percentage_pay,
                'school_name'=>$school_name,
            ]);
        }
       
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
 
      
    }

//return 0:false or 1:true    
    public function validateScholarship($student,$acad){
            $modelScho = new SrcScholarshipHolder;
            $flag = 0;
            $scholar_ = SrcScholarshipHolder::find()->select(['student','fee', 'academic_year'])->where(['student'=>$student])->andWhere(['academic_year'=>$acad])->all();
                
         if($scholar_!=null)
          {   $i=0;
            foreach($scholar_ as $s)
              {   $i++;
                if($s->fee==null)
                 {  
                  $flag = 1;  
                   }
                
              }
               
             if( ($i>1)&&($flag == 1) )
                $flag = 0;
           }
         else
          $flag = 0;
           
          return $flag;
            
        }

    /**
     * Deletes an existing ScholarshipHolder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     if(Yii::$app->user->can('billings-scholarshipHolder-delete'))
      {
          try{
          
            $acad=Yii::$app->session['currentId_academic_year']; 

       $model = $this->findModel($id);
       
        //tcheke si fre a/yo gen tranzaksyon sou li/yo deja
          $id_bil='';
          $bil_mod = new SrcBillings();
          $bil_ = NULL;
          
		  $fee_check = $model->fee;
                  $pousanjtaj_peye = $model->percentage_pay;
		   //$bil_ = $mod_bil->isFeeAlreadyUse($model->student,$fee_check, $acad);
                    $bil_ = $bil_mod->isFeeAlreadyUse_new($model->student,$fee_check, $acad);
                                                                    
                                                                  
                $pass=1;							  
                  if($bil_ !=NULL)
                   {   
                       if($bil_['amount_pay']>0)
                       { $error=2;
                          $pass=0;
                       }
                       else
                           $id_bil= $bil_['id'];       
                     }

                  if($pass==0)
                    {   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','This record is in used, it cannot be either updated nor deleted.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
		 								    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
		       
		        
		        if(isset($_GET['from']))
					return $this->redirect('view?id='.$_GET['id'].'&from=oth');
				else
					return $this->redirect('index?from=oth');
			}
		  elseif($pass==1) 
		    {  $model->delete();

				return $this->redirect(['index?from=oth']);
			}
			
			
		 } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
      
       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }


public function actionIndex_exempt()
	{
		   
	if(Yii::$app->user->can('billings-scholarshipHolder-index_exempt'))
      {
	
         $acad=Yii::$app->session['currentId_academic_year']; 


		$searchModel = new SrcScholarshipHolder();
        $dataProvider = $searchModel->search_exemption($acad,Yii::$app->request->queryParams);
        
        if(isset($_GET['SrcScholarshipHolder']))
				$dataProvider= $searchModel->search_exemption($acad,Yii::$app->request->queryParams);
		
		if (isset($_GET['pageSize'])) 
						         	 {
								        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
								          unset($_GET['pageSize']);
									   }


       
		    
	        return $this->render('index_exempt', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);
	        
	   }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

}
	
	
public function actionExempt()
	{
		   
	
	if(Yii::$app->user->can('billings-scholarshipHolder-exempt'))
      {
	     $acad=Yii::$app->session['currentId_academic_year']; 
	      
	      $comment = Yii::t('app','This balance is exempted by {name}.',array('{name}'=> currentUser()) ).' ('.date('Y-m-d').') ';
	         
	        $model=new SrcScholarshipHolder;
	        $modelFee = new SrcFees();
	        $modelBil = new SrcBillings();
	           
	        $program=0;
	    								   
			$dataProvider= $modelFee->searchFeesToExempt(0,0,1); 						   
		
	        
			
			//if(isset($_POST['ScholarshipHolder']['fee']))
			//				$fee = $_POST['ScholarshipHolder']['fee'];
			
		  if($model->load(Yii::$app->request->post()) ) 
			{
		        
		        
		       // if(isset($_POST['SrcScholarshipHolder']['student']))
		        // {
		         	
		         	//}
		        
		        if($model->student!='')
				  {
					$program =getProgramByStudentId($model->student);
				  }
									   
			    $dataProvider=$modelFee->searchFeesToExempt($model->student,$program,$acad); 
			    
			    

			$this->student_id= $model->student;
			
			if($model->comment!='')
			   $comment = $model->comment;
		
		
			if(isset($_POST['create']))
              { 
             	     
             	     $selection=(array)Yii::$app->request->post('selection');//typecasting
				       if($selection!=null)
					    {   foreach($selection as $id)
					         {
					            $fee_id = (int)$id;
					            
					            $amount_exempted = 0;
                      	 $percentage_exempted = 100;
                      	 
                      	  $modelFee = SrcFees::findOne($fee_id);
                      	  
                      	  $fee_amount = $modelFee->amount;
                      	  
                      	  $amount_exempted = $modelFee->amount;
                      	 
                          $total_pay_on_fee= $modelFee->getTotalAmountPayOnFee($this->student_id,$fee_id);
                          
                          $dbTrans = Yii::$app->db->beginTransaction();
                          $is_save=false;
                                	
                           if($total_pay_on_fee!=0)  
                             {
                             	$amount_exempted = $fee_amount-$total_pay_on_fee;
                             	
                             	$percentage_paid = round( (($total_pay_on_fee * 100) / $fee_amount), 2);
                             	
                             	$percentage_exempted = 100 - $percentage_paid;
                             	
                             	 //met balans denye tranzaksyon an a 0, ajoute komante pou sa
                             	 //return a integer (id)
                                    $Last_transaction_id = $modelBil->getLastTransactionIdByFeeId($this->student_id, $fee_id, $acad);
                                    
                             	     $command_ = Yii::$app->db->createCommand();
								     $command_->update('billings', ['fee_totally_paid' => 1, 'balance'=>0,'comments'=>$comment  ], ['id'=>$Last_transaction_id ])
									         ->execute();
									                                               
									                                              
								                 
								                 
								                  
								         //met fee_totally_paid=1 pou tranzaksyon anvan yo
								           $command1 = Yii::$app->db->createCommand();
								           $command1->update('billings', ['fee_totally_paid' => 1,   ], ['academic_year'=>$acad,'fee_period'=>$fee_id,'student'=>$this->student_id])
									              ->execute();

     
                             	     
                             	      
                             	 
                             	  //retire % exempted a nan total balans li
                             	  $modBalance = new SrcBalance();
                             	   $modelBalance_=SrcBalance::find()->where(['student'=>$this->student_id])->all();
					                 $new_balance =0;
					                  if($modelBalance_!=null)    
					                     {  
					                     	foreach($modelBalance_ as $modelBalance)
					                     	{
						                     	$new_balance=$modelBalance->balance - $amount_exempted;
						                        
						                        $modelBalance->setAttribute('balance',  $new_balance);
																       	    
												if($modelBalance->save())
                                                                                                        $is_save=true;
                                                                                                else 
                                                                                                    $is_save=false;
					                     	}
															       	     
					                     }
                             	      
                            	      
                             	      
                             	       //ajoute l nan bousye ak komante %li pa peye a
                             	       $modelScholarshipHolder=new SrcScholarshipHolder;
		                                    $modelScholarshipHolder->setAttribute('student', $this->student_id);
		                                    $modelScholarshipHolder->setAttribute('partner', NULL);
		                                    $modelScholarshipHolder->setAttribute('fee', $modelFee->id);
		                                    $modelScholarshipHolder->setAttribute('is_internal', 1);
		                                    $modelScholarshipHolder->setAttribute('comment', $comment);
		                                    $modelScholarshipHolder->setAttribute('percentage_pay', $percentage_exempted);
											$modelScholarshipHolder->setAttribute('academic_year', $acad);
											$modelScholarshipHolder->setAttribute('create_by', currentUser()  );
										    $modelScholarshipHolder->setAttribute('date_created', date('Y-m-d') );
									  
                                                                                    if($modelScholarshipHolder->save() )
                                                                                    {
                                                                                        if($is_save)
                                                                                            {
                                                                                                $dbTrans->commit();
                                                                                            }
                                                                                             else
                                                                                                { $dbTrans->rollback();

                                                                                                 }
                                                                                    }
                                                                                  else
                                                                                    { $dbTrans->rollback();

                                                                                     }
                             	       
                             	 
                             	}
                             else
                               { // lanse reket delete liy sa nan billings
                                     $command0 = Yii::$app->db->createCommand();
	                                 $command0->delete('billings', ['student'=>$this->student_id,'fee_period'=>$fee_id, 'amount_pay'=>0, 'academic_year'=>$acad])
									              ->execute();
									              
									              //$command0->delete('billings', 'student=:stud AND fee_period=:fee AND amount_pay=0 AND academic_year=:acad', array(':stud'=>$this->student_id,':fee'=>$fee_id,':acad'=>$acad_sess));
                               	    
                               	     
                               	     //retire % exempted a nan total balans li 
                               	     $modBalance = new SrcBalance();
                             	   $modelBalance_=SrcBalance::find()->where(['student'=>$this->student_id])->all();
					                  $new_balance =0;
					                  if($modelBalance_!=null)    
					                     {  
					                     	foreach($modelBalance_ as $modelBalance)
					                     	{
						                     	$new_balance=$modelBalance->balance - $amount_exempted;
						                        
						                        $modelBalance->setAttribute('balance',  $new_balance);
																       	    
												if($modelBalance->save())
                                                                                                        $is_save=true;
                                                                                                else 
                                                                                                    $is_save=false;
					                     	 }
															       	     
					                     }
                               	     
                               	     
                               	     //ajoute l nan bousye ak komante
                               	     $modelScholarshipHolder=new SrcScholarshipHolder;
		                                    $modelScholarshipHolder->setAttribute('student', $this->student_id);
		                                    $modelScholarshipHolder->setAttribute('partner', NULL);
		                                    $modelScholarshipHolder->setAttribute('fee', $modelFee->id);
		                                    $modelScholarshipHolder->setAttribute('is_internal', 1);
		                                    $modelScholarshipHolder->setAttribute('comment', $comment);
		                                    $modelScholarshipHolder->setAttribute('percentage_pay', $percentage_exempted);
											$modelScholarshipHolder->setAttribute('academic_year', $acad);
											$modelScholarshipHolder->setAttribute('create_by', currentUser()  );
										    $modelScholarshipHolder->setAttribute('date_created', date('Y-m-d') );
									  
                                                                                  if($modelScholarshipHolder->save() )
                                                                                    {
                                                                                        if($is_save)
                                                                                            {
                                                                                                $dbTrans->commit();
                                                                                            }
                                                                                             else
                                                                                                { $dbTrans->rollback();

                                                                                                 }
                                                                                    }
                                                                                  else
                                                                                    { $dbTrans->rollback();

                                                                                     }
									  
                               	}   
                           
                           					            
					            
					            
					            
					            
					            				                      
					         } 
					             	return $this->redirect(array('index_exempt'));
					      }

             	  	
             	  
             	             	
              }
             
                   

		}
                      

		 
			return $this->render('exempt',array(
				'model'=>$model,
				'dataProvider'=>$dataProvider,
			));
	      }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

	}




	public function loadStudentToExempt($criteria)
	{    $code= array();
		   
		    $persons=Persons::model()->findAll($criteria);
            
			
		    if(isset($persons))
			 {  
			    foreach($persons as $stud){
			        $code[$stud->id]= $stud->fullName." (".$stud->id_number.")";
		           
		           }
			 }
		   
		return $code;
         
	}

    /**
     * Finds the ScholarshipHolder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScholarshipHolder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcScholarshipHolder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
