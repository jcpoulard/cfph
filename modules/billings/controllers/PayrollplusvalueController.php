<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\PayrollPlusvalue;
use app\modules\billings\models\SrcPayrollPlusvalue;
use app\modules\billings\models\SrcPayroll;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PayrollplusvalueController implements the CRUD actions for PayrollPlusvalue model.
 */
class PayrollplusvalueController extends Controller
{
   
    public $layout = "/inspinia";
    
    
     /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayrollPlusvalue models.
     * @return mixed
     */
    public function actionIndex()
    {
	   if(Yii::$app->user->can('billings-payrollplusvalue-index'))
         {
	        $searchModel = new SrcPayrollPlusvalue();
	        $acad=Yii::$app->session['currentId_academic_year'];
	        $dataProvider = $searchModel->searchByAcad($acad);
	        
	        if(isset($_GET['SrcPayrollPlusvalue']))
					$dataProvider= $searchModel->searchByAcad($acad);
			
			
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
    }

    /**
     * Displays a single PayrollPlusvalue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-payrollplusvalue-view'))
        {
	       if($id==0)
	         {
	         	Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app',' No payroll to view.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);	
															
						return $this->redirect(Yii::$app->request->referrer);
	           }
	       else
	         {
		        return $this->render('view', [
		            'model' => $this->findModel($id),
		        ]);
	         }
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Creates a new PayrollPlusvalue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('billings-payrollplusvalue-create'))
         {
         	 $acad = Yii::$app->session['currentId_academic_year'];
         	 
         	         
         $model = new SrcPayrollPlusvalue();
         
         

        if($model->load(Yii::$app->request->post()) ) 
          {
                 $person_id = $model->person_id;
			      $devise = $model->devise;
					
			     
	     

	if(isset($_POST['create']))
	  {  
			$person_id = $model->person_id;
			$devise = $model->devise;
						
	        $pass =0;
             	 $is_save = TRUE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    
                      
				      $num_month = sizeof(getSelectedLongMonthValueForOne($model->person_id));
	                           
	               if($devise!='')
	                 {                  
				                                   
			                         for($i=1;$i<=$num_month;$i++)
			                           {
			                           	   
			                           	
			                            if(($_POST["month$i"]!='')&& ($_POST["amount$i"]!='') )
			                             {
			                             	
			                             	  //tcheke sil pa gen pou mwa sa deja pou pa save li
			                             	  $tchek = $model->isAlreadySetForOne($person_id,$_POST["month$i"],$acad);
			                               if($tchek==null)
			                                {	
			   	                             	$model->setAttribute('person_id',$person_id);
			   	                             	$model->setAttribute('devise',$devise);
			   	                             	$model->setAttribute('acad',$acad);
			   	                             	$model->setAttribute('created_date',date('Y-m-d H:i:s'));
									  	        $model->setAttribute('created_by',currentUser());
									  	        
				                                $model->month = $_POST["month$i"];
				                                $model->amount = $_POST["amount$i"];
				                                
				                                if($model->save())
				                                  {  
				                                  	 $pass =1;
				                                  	 unSet($model);
				                                  	 $model = new SrcPayrollPlusvalue();
				                                  }
				                                else
				                                  $is_save = FALSE; 
			                                }
			                                
			                                
			                             }
			                          }
			                            
				           	    
				           	     
	                            if((!$is_save)||($pass ==0) ){
	                                $dbTrans->rollback();
	                                
	                            }
	                            else{
	                                $dbTrans->commit();
	                                return $this->redirect(['index']);
	                            }
	                       
	                     }
                        
		                   
                       
                                 
          

			    
			    
					   
	 }
			   

  }
        
	            return $this->render('create', [
	                'model' => $model,
	                	            ]);
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
             return $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing PayrollPlusvalue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
         if(Yii::$app->user->can('billings-payrollplusvalue-update'))
         {
         		
         	
         	 $model = $this->findModel($id);
         	 
         	 $old_month = $model->month;
        
        $acad=Yii::$app->session['currentId_academic_year']; 
        
           //gad si payroll fet deja
           //check if payroll for this month already done
		$payrollDone = $this->isPayrollDoneForOnes($model->month,$model->person_id,$acad);
       if(!$payrollDone)   //payroll poko fet
         {

	       if ($model->load(Yii::$app->request->post()) ) 
	         {
              
				  
					  if(isset($_POST['update']))
					   { 
					      	if( $old_month != $model->month)
					      	  {      //tcheke sil pa gen pou mwa sa deja pou pa save li
			                         $tchek = $model->isAlreadySetForOne($model->person_id,$model->month,$acad);
			                         if($tchek==null)
			                           {	
			   	                             	$model->setAttribute('updated_date',date('Y-m-d H:i:s'));
									  	        $model->setAttribute('updated_by',currentUser());
									  	        
				                                if($model->save())
				                                    return $this->redirect(['index']);
			                             }
			                          else
			                            {
			                            	Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Employee already recorded on this month.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
			                             
                                                        return $this->redirect(Yii::$app->request->referrer);
                                                    }
			                                
					      	    }
					      	  else
					      	    {
					      	    	
					      	    	       $model->setAttribute('updated_date',date('Y-m-d H:i:s'));
									  	        $model->setAttribute('updated_by',currentUser());
									  	        
									  	        if($model->save())
									  	           return $this->redirect(['index']);
					      	      }
				                                 
				                      
											     
							
			
								
					
					 }
					    
			     
           
                  } 
           
           
	            return $this->render('update', [
	                'model' => $model,
	                 
	            ]);
         
               }
			 else
				{
					  
					   Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Update is denied, this payroll is already done.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
					     return $this->redirect(Yii::$app->request->referrer);
					       	 
				}
         
          
         
         
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
             return $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Deletes an existing PayrollPlusvalue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         if(Yii::$app->user->can('billings-payrollplusvalue-delete'))
         {
         		
         	
         	 $model = $this->findModel($id);
         	 
         	 
        $acad=Yii::$app->session['currentId_academic_year']; 
        
           //gad si payroll fet deja
           //check if payroll for this month already done
			$payrollDone = $this->isPayrollDoneForOnes($model->month,$model->person_id,$acad);
	       if(!$payrollDone)   //payroll poko fet
	         {
  
			        $model->delete();
			        return $this->redirect(['index']);
			         
               }
			 else
				{
					  
					   Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Payroll is already done, record cannot be deleted.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
					return $this->redirect(Yii::$app->request->referrer);     
					       	 
				}
         
          
         
         
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }

          }

    }


//************************  isPayrollDoneForOnes($month,$pers,$acad)  ******************************/	
public function isPayrollDoneForOnes($month,$pers,$acad)
	{    
                $bool=false;
                
                $modelPayroll = new SrcPayroll;
               
		 if($month!='')
		   {
			  $result=$modelPayroll->isDoneForOnes($month,$pers,$acad);
				
				 if(isset($result)&&($result!=null))
				  {  $payroll=$result->getModels();//return a list of  objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						       $bool=true;
						}
						  
				   }
				   
					 
		     }
		     
		     
		return $bool;
         
	}




    /**
     * Finds the PayrollPlusvalue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PayrollPlusvalue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PayrollPlusvalue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
