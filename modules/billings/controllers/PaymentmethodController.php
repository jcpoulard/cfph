<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\PaymentMethod;
use app\modules\billings\models\SrcPaymentMethod;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;
/**
 * PaymentmethodController implements the CRUD actions for PaymentMethod model.
 */
class PaymentmethodController extends Controller
{
     public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentMethod models.
     * @return mixed
     */
    public function actionIndex()
    {
	   if(Yii::$app->user->can('billings-paymentmethod-index'))
         {

            $searchModel = new SrcPaymentMethod();
	        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	        
	        if(isset($_GET['SrcPaymentMethod']))
						          $dataProvider= $searchModel->search(Yii::$app->request->queryParams);
						          
						          if (isset($_GET['pageSize'])) 
							         	 {
									        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
									          unset($_GET['pageSize']);
										   }
	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     
    }

    /**
     * Displays a single PaymentMethod model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	   if(Yii::$app->user->can('billings-paymentmethod-view'))
         {

            return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
	      }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new PaymentMethod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	   if(Yii::$app->user->can('billings-paymentmethod-create'))
         {

            $model = new SrcPaymentMethod();
	
	             if($model->load(Yii::$app->request->post()) ) 
			        {
			            // $dbTrans = Yii::app->db->beginTransaction(); 
			            
					       
			            if(isset($_POST['create']))
			            { 
						    
					    
						    $model->date_create=date('Y-m-d');
						    $model->create_by=currentUser();
			
						      if($model->save() )
						        { //$dbTrans->commit();  	
					                return $this->redirect(['index', 'wh'=>'set_pm']);
								 }
							  else
								 { //   $dbTrans->rollback();
								   }
			               
			              }
			            
			            
			          } 
	
	            return $this->render('create', [
	                'model' => $model,
	            ]);
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing PaymentMethod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	   if(Yii::$app->user->can('billings-paymentmethod-update'))
         {

            $model = $this->findModel($id);
            
             
	
	            if($model->load(Yii::$app->request->post()) ) 
			        {
			            // $dbTrans = Yii::app->db->beginTransaction(); 
			            
					       
			            if(isset($_POST['update']))
			            { 
						   $modelPay_method = new SrcPaymentMethod;
						    //tcheke si default method lan defini deja
				      	   $default_pmethod_defined = $modelPay_method->isAlreadyDefined(); 
                                         
                                        if(($model->id==3)||($model->id==4))  //depot ou cheque  
                                          {
                                                $modelM = $this->findModel($id);
                                              
                                              if($default_pmethod_defined==NULL)
					       { 
                                                 $modelM->setAttribute('description',$model->description);
                                                 $modelM->setAttribute('is_default',$model->is_default);
                                                 $modelM->date_create=date('Y-m-d');
						    $modelM->create_by=currentUser();
                                                    
                                                    if($modelM->save())
                                                    {
                                                        Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','This payment method name cannot be updated.') ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);
                                                        
                                                        $this->redirect (array('index'));
                                                    }
                                               }
                                             else 
                                               {
                                                   foreach($default_pmethod_defined as $default_pmethod_def)
						      	  {
							      	   	if(($default_pmethod_def['id']!=$modelM->id)&&($modelM->is_default==1))
							      	      	  { // Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Last payment method is already defined.' ));
							      	      	      $modelM->is_default= 0;
							      	      	      
							      	      	        Yii::$app->getSession()->setFlash('Warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app','Last payment method is already defined.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							      	      	  }
							      	      	else
							      	      	  {
							      	      	  	         $modelM->setAttribute('description',$model->description);
                                                                                         $modelM->setAttribute('is_default',$model->is_default);
                                                                                         $modelM->date_update=date('Y-m-d');
												    $modelM->update_by=currentUser();
									
												      if($modelM->save() )
												        { //$dbTrans->commit(); 
Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','This payment method name cannot be updated.') ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);

											                return $this->redirect(['index', 'wh'=>'set_pm']);
														 }
													  else
														 { //   $dbTrans->rollback();
														   }
												
							      	      	  	}
							      	      	
							      	      
						      	  
						      	   }
                                               }
                                               
                                               
                                               
                                          }
				      	else
                                         {
                                              if($default_pmethod_defined==NULL)
					      	    { 
					      	    	 if($model->save())
							            $this->redirect (array('index'));
							            
							      }
                                              else
						     { 	 foreach($default_pmethod_defined as $default_pmethod_def)
						      	  {
							      	   	if(($default_pmethod_def['id']!=$model->id)&&($model->is_default==1))
							      	      	  { // Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Last payment method is already defined.' ));
							      	      	      $model->is_default= 0;
							      	      	      
							      	      	        Yii::$app->getSession()->setFlash('Warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app','Last payment method is already defined.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							      	      	  }
							      	      	else
							      	      	  {
							      	      	  	  $model->date_update=date('Y-m-d');
												    $model->update_by=currentUser();
									
												      if($model->save() )
												        { //$dbTrans->commit();  	
											                return $this->redirect(['index', 'wh'=>'set_pm']);
														 }
													  else
														 { //   $dbTrans->rollback();
														   }
												
							      	      	  	}
							      	      	
							      	      
						      	  
						      	   }
				              }
                                              
                                              
                                            }
						            

						    
						
                                              
                                              
                                              
                                              
						    
			               
			              }
			            
			            
			          } 
			          
			          
	            return $this->render('update', [
	                'model' => $model,
	            ]);
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Deletes an existing PaymentMethod model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
	   if(Yii::$app->user->can('billings-paymentmethod-delete'))
         {
           try{
               
                  $model=$this->findModel($id);
               
	            if(($model->id!=3)&&($model->id!=4))  //depot ou cheque         
                      {
                        $model->delete();
                      }
                     else 
                       {
                           Yii::$app->getSession()->setFlash('warning', [
                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                    'duration' => 36000,
                                                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                    'message' => Html::encode( Yii::t('app','This payment method cannot be deleted.') ),
                                                    'title' => Html::encode(Yii::t('app','Warning') ),
                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                ]);

                         }
		
		        return $this->redirect(['index']);
		       } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          return $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                               return $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                   return $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the PaymentMethod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentMethod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
		      if (($model = SrcPaymentMethod::findOne($id)) !== null) {
		            return $model;
		        } else {
		            throw new NotFoundHttpException('The requested page does not exist.');
		        }
		        
		        
    }
    
    
    
    
    
    
    
    
    
}
