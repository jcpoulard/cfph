<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\ErrorException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use app\modules\billings\models\Payroll;
use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\LoanElements;
use app\modules\billings\models\SrcPayrollPlusvalue;

use app\modules\fi\models\SrcPersons;

use yii\web\ForbiddenHttpException;

use yii\helpers\Url;

use kartik\mpdf\Pdf;
 
use kartik\widgets\Alert;

/**
 * PayrollController implements the CRUD actions for Payroll model.
 */
class PayrollController extends Controller
{
    public $layout = "/inspinia";
     
    public $payroll_month;
	public $person_id;
	public $group;
	public $grouppayroll=1;
	public $taxe = 1;
	public $payroll_date;
	
	public $part;
	public $depensesItems1 = 1;
	public $month_ =0;
	
	public $status_ = 0;
	
	public $payrollReceipt_available =false;
	
	
	public $message_PayrollDatePaymentDate_notInAcadRange=false;
	public $message_anyPayrollAfter=false;
	public $message_group_anyPayrollAfter=false;
	public $message_PayrollDate=false;
	public $message_PaymentDate=false;
	public $messageNoPayrollDone = false;
	public $messagePayrollReceipt_available=true;
	public $messageMissingHourNotOk=false;
	public $message_noOneSelected=false;
	public $message_wrongMissingTime=false;
	
	public $message_UpdatePastDate=false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payroll models.
     * @return mixed
     */
    public function actionIndex()
    {
	     if(Yii::$app->user->can('billings-payroll-index'))
         {  
	        $searchModel = new SrcPayroll();
	        
	        $acad=Yii::$app->session['currentId_academic_year'];
	        
	            $last_dat = ''; 
       
     $month_ = 0;
     $year_ =0;
     $current_month =0;
     $current_year =0;
     $i = 0;
      
         $di = 1;
     if($this->status_ == 1)
		  $di = 1;
		elseif($this->status_ == 2)
		  $di = 2;
	
	
	
	   
	  
     if(!isset($_GET['month_']))
       {
       	   $sql__ = 'SELECT DISTINCT payroll_month,payroll_date,payment_date FROM payroll p INNER JOIN payroll_settings ps ON(ps.id = p.id_payroll_set) INNER JOIN academicperiods a ON(a.id = ps.academic_year) WHERE a.id='.$acad.' ORDER BY payroll_date DESC';
												
		  $command__ = Yii::$app->db->createCommand($sql__);
		  $result__ = $command__->queryAll(); 
													       	   
			if($result__!=null) 
			 { foreach($result__ as $r)
			     { if(($r['payroll_month']!='')&&($r['payroll_month']!=NULL))  
			        { $current_month = $r['payroll_month'];
			         $current_year = getYear($r['payroll_date']);
			         $last_dat = $r['payment_date']; 
			        } 
			          break;
			          
			     }
			  }
			
			//if(!isDateInAcademicRange($last_dat,$acad))
           //   $display = false;

       	  
        }
     else 
       {  $current_month = $_GET['month_'];
       	  $current_year = $_GET['year_'];
       	  $di = $_GET['di'];
        }

	        $dataProvider = $searchModel->searchByMonthYear($current_month,$current_year);
	        
	        if(isset($_POST['SrcPayroll']['depensesItems']))
		    $this->depensesItems1 = $_POST['SrcPayroll']['depensesItems'];
		    
		    
		    if($this->depensesItems1==1)
              {         
                 $this->status_ = 1;
                 
                   if(isset($_GET['SrcPayroll']))
					$model->attributes=$_GET['SrcPayroll'];
					
              }
            elseif($this->depensesItems1==2)
	         {
	         	 $this->status_ = 2;
	         	
	         	 return $this->redirect(['/billings/chargepaid/index?di=2&from=b']);
	
	         	}
		

             
             
             
	
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'status_'=>$this->status_,
            'depensesItems'=>$this->depensesItems1,
        ]);
        
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    


    /**
     * Displays a single Payroll model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	   if(Yii::$app->user->can('billings-payroll-view'))
         { 
	       if($id==0)
	         {
	         	Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app',' No payroll to view.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);	
															
						 $this->redirect(Yii::$app->request->referrer);
	           }
	       else
	         {
		        return $this->render('view', [
		            'model' => $this->findModel($id),
		        ]);
	         }
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Payroll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
  public function actionCreate()
    {
        if(Yii::$app->user->can('billings-payroll-create'))
         {
         	$payroll_setting_twice = infoGeneralConfig('payroll_setting_twice');
         	
         	$loan_payment_method = infoGeneralConfig('loan_payment_method'); 
         	
         	 $model = new SrcPayroll();
        $modelPS = new SrcPayrollSettings();
        
		
		$this->message_PayrollDate=false;
		$this->message_PaymentDate=false;
		 $this->message_PayrollDatePaymentDate_notInAcadRange=false;
		 $this->message_anyPayrollAfter=false;
		 $this->message_group_anyPayrollAfter=false;
		
		$acad=Yii::$app->session['currentId_academic_year'];
		
		/*if(isset($_POST['Payroll']['depensesItems']))
		    $this->depensesItems1 = $_POST['Payroll']['depensesItems'];
		

		if($this->depensesItems1==0)
            {
         	    $this->redirect(array('/billings/loanOfMoney/create?part=pay&from=b'));

              }
            elseif($this->depensesItems1==2)
	           {
	         	  $this->redirect(array('/billings/chargePaid/create?di=2&part=pay&from=b'));
	           }
	
*/
              $allDevises = getDevises();
              $default_devise = getDefaultCurrency();
              
              $model->devise=$default_devise->id;
              $model->plus_value =0;
              $model->group_payroll = [1];
              $model->taxe = 1;
              
              $this->group =1;
              $model->group = $this->group;
              $this->person_id='';
			  $this->payroll_month = '';
			  Yii::$app->session['payroll_person_id']='';
                           	 
              
        if($model->load(Yii::$app->request->post()) ) 
		  {
				if(isset($_POST['SrcPayroll']['group']))
			 		 {
                        $this->group = $_POST['SrcPayroll']['group'];
                        
                        $model->group = $this->group;
                        
                         if($this->group==1)
                           {
                           	 $this->person_id='';
								$this->payroll_month = '';
								Yii::$app->session['payroll_person_id']='';
                           	 }
                      }
                    
		            if(isset($_POST['SrcPayroll']['devise']))
					  {
                           $model->devise = $_POST['SrcPayroll']['devise'];
                       }
                       
                     if(isset($_POST['SrcPayroll']['group_payroll']))
					  {
                        $this->grouppayroll = $_POST['SrcPayroll']['group_payroll'];
                             $model->group_payroll = [$this->grouppayroll];
                       }

                 
		           if(isset($_POST['SrcPayroll']['payroll_month']))
		              {  
		              	$this->payroll_month = $_POST['SrcPayroll']['payroll_month'];
					     Yii::$app->session['payroll_payroll_month'] = $this->payroll_month;
		              }
		            else
		              {
		            	 $this->payroll_month=Yii::$app->session['payroll_payroll_month'];
		            	
		            	}
			
                  if(isset($_POST['SrcPayroll']['person_id']))
		              {  
		              	$this->person_id = $_POST['SrcPayroll']['person_id'];
					     Yii::$app->session['payroll_person_id']=$this->person_id;
		              }
		            else
		              {
		            	 $this->person_id=Yii::$app->session['payroll_person_id'];
		            	
		            	}
		            	
			
			
				if(isset($_POST['create']))
				  {    
			  	      $id_payroll_set ='';
				  	  $deduction ='';
				  	  $timely_salary = 0;
				  	  $assurance = 0;
				  	  $frais = 0;
				  	  $number_of_hour =null;
				  	  $missing_hour =null;
				  	  $gross_salary ='';
				  	  $net_salary ='';
				  	  $missing_hour_ok = true;
				  	  
				  	  $date_payment='';
				  	  $date_payroll='';
				  	  
				  	  $this->message_PayrollDate=false;
			          $this->message_PaymentDate=false;
			          $this->message_PayrollDatePaymentDate_notInAcadRange=false;
			          $this->message_anyPayrollAfter=false;
			          $this->message_group_anyPayrollAfter=false;
			          
			            $payment_date =  $model->payment_date;
			            $payroll_date= $model->payroll_date;
			  	  
			  	  
			  	    //if(isset($_POST['SrcPayroll']['taxe']))
			         //  {
                          $this->taxe = $model->taxe; //$_POST['SrcPayroll']['taxe'];
                     //   }
                        
                     
   if(  ($model->payroll_month == getMonth($model->payroll_date) ) || ($model->payroll_month == 0) || ($model->payroll_month > 12)  )
     { 
     	if($model->payment_date >= $model->payroll_date)
     	  {               
              //return 0:false, 1:true
          if((isDateInAcademicRange($model->payroll_date,$acad)==1)&&(isDateInAcademicRange($model->payment_date,$acad)==1)) 
                 {     
                 	                 	        
     //youn pa youn               
         if($this->group==0)
		    {    
                 $missing_hour =0;
                 $total_missing_hour =0;
                 $numberHour_=0;
                 $all='';
                 $plus_value =0;
                
                  
                 
                 $is_save = TRUE;
                 
                 $temp_month = '';
                  
                 //if($model->plus_value =='')
                  // $model->plus_value =0;
                 
                 $modelPayrollPlusvalue= new SrcPayrollPlusvalue();
                 $plus_value = $modelPayrollPlusvalue->getPayrollPlusvalue($this->person_id,$this->payroll_month,$acad); 
                  
                  $assurance = 0;
                  //check if there is any after this payroll_month
                  //return 0:false, 1:true
                  $any_after = $this->anyPayrollDoneAfterForOnes($model->devise, $model->payroll_month,$model->payroll_date,$this->person_id,$acad);
                  
               if($any_after==0)  //anyPayrollAfter
                 {    
                   $dbTrans = Yii::$app->db->beginTransaction();
                   
                   //check if it is a timely salary 
                   $model_ = new SrcPayrollSettings;
                     $pay_set = SrcPayrollSettings::find()
                                             ->orderBy(['date_created'=>SORT_DESC])
                                             ->where('payroll_settings.old_new=1 AND payroll_settings.person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.devise='.$model->devise.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2)) ')
                                             ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set2 = null;
                    $id_payroll_set_emp=null;
                    $id_payroll_set_teach=null;
                    $total_gross_salary=0;
                    $total_gross_salary_initial = 0;
                    $total_net_salary=0;
                    $total_deduction=0;
                    $total_taxe=0;
                    $iri_deduction =0;
                    $frais = 0;
                    
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary_initial =0;
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
                           $assurance = 0;
                                
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $gross_salary_initial =$amount->amount;
			               	   
			               	   $missing_hour = $amount->number_of_hour;
			               	   
			               	   $numberHour_ = $amount->number_of_hour;
			               	   
			               	   $assurance =  $amount->assurance_value;
			               	   
			               	   $frais =  $frais+ $amount->frais;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	 if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['SrcPayrollSettings']))
			             	  {  
				             	 //return working times in minutes
				             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
				                 
				                 $missing_hour = $missing_hour - $number_of_hour;
				                 
				                  $total_missing_hour =  $total_missing_hour + $missing_hour;
			             	  }
			             	 else
			             	    $number_of_hour = $missing_hour;
			                 //calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						          
						        }
			                  
			                  if(($numberHour_!=null)&&($numberHour_!=0))
						       {
						          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
						
						        } 
			                   
			                  
			                   
			                   
			                   
			             
				            }
			           
			            //$total_gross_salary = $total_gross_salary + $gross_salary;
			            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
			          
			         if($model->payroll_month == 0)
			           {
			           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
                        	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
			           }
			         else
			            {  
			          	  if($this->taxe==1)
	                         {
	                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
                        	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
	                       	 } 
	                      elseif($this->taxe==0)
	                        {
	                        	//$net_salary = $gross_salary;
	                        	$net_salary = $gross_salary_initial - $assurance;
	                        	
	                          }
	                          
			             }  
                          
                          $total_net_salary = $total_net_salary + $net_salary;
                          
                          
                          
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
                                $assurance = 0;
                                
	                     
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $gross_salary_initial =$amount->amount;
					               	   
					               	   $missing_hour = $amount->number_of_hour;
					               	   
					               	   $numberHour_ = $amount->number_of_hour;
					               	   
					               	   $assurance =  $amount->assurance_value;
					               	   
					               	   $frais = $frais + $amount->frais;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	  if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
						             	  {  
							             	 //return working times in minutes
							             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
							                 
							                 $missing_hour = $missing_hour - $number_of_hour;
							                 
							                 $total_missing_hour =  $total_missing_hour + $missing_hour;
						             	  }
						             	 else
						             	    $number_of_hour = $missing_hour;
					                 
					                 //calculate $gross_salary by hour if it's a timely salary person 
								    /* if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
								      */
								        
								     if(($numberHour_!=null)&&($numberHour_!=0))
								       {
								          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
								
								        } 
					                   
					             
						            }
					           
					            //$total_gross_salary = $total_gross_salary + $gross_salary;
					            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
					          
					          if($model->payroll_month == 0)
					           {
					           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
		                        	    
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	   // $net_salary = $gross_salary - $deduction;
			                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
					           }
					          else
					          	{ 
					          	  if($this->taxe==1)
			                         {
			                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	    //$net_salary = $gross_salary - $deduction;
 			                       	    
			                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
			                       	 } 
			                      elseif($this->taxe==0)
			                        {
			                        	//$net_salary = $gross_salary;
			                        	$net_salary = $gross_salary_initial - $assurance;
			                          }  
			                          
					          	}
		                          
		                          $total_net_salary = $total_net_salary + $net_salary;
		                          
                          
                          
                               }
                       
                      }
                       
                       
                      if(($as_emp==1) && ($as_teach==1))
		                   {  //alafwa employee e teacher
		                   	   //save payroll la sou employee
		                   	   
		                   	   $all='e';
		                   	   
		                   	   $id_payroll_set=$id_payroll_set_emp;
		                   	   $id_payroll_set2 = $id_payroll_set_teach;
		                   	   $number_of_hour=null;
		                   	   $missing_hour=null;
		                   	   
		                   	 }

                     if($model->payroll_month != 0)
			           {
			           	
                      //DEDUCTION TAXE IRI
                       if($this->taxe==1)
	                     {
	                     		//IRI
	                       	$iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary_initial);
	                       	$total_deduction = $total_deduction + $iri_deduction;
	                       	$total_net_salary = $total_net_salary - $iri_deduction;
	      
	       
	                       }
	                       
			           }
                       
                      
                    
                    
                 $total_net_salary = $total_net_salary + $frais + $plus_value;    
                     
                       //loan nan ap prelve sou montan total payroll moun nan
                       //check if there is a loan
                       $modelLoan = new SrcLoanOfMoney;
                       $dataModelLoan = $modelLoan->find()
                                               ->where('person_id='.$this->person_id.' and academic_year='.$acad.' and paid=0')
                                               ->orderBy(['loan_date'=>SORT_ASC])
                                               ->limit('1')
                                               ->all();
                        
                                      
                        $total_taxe = $total_taxe + $total_deduction;
            
                   
                   if($model->payroll_month != 0)
			         {
			           	     
                        if($dataModelLoan!=null)
                          { 
                          	 $id_loan= 0;
                          	 $loan_devise ='';
                                 $loan_monyth ='';
                          	 
                          	 $loan_=$dataModelLoan;//->getData();
                          	foreach($loan_ as $loan)
                          	   { 
		                          	 $id_loan= $loan->id;
		                          	 $loan_devise =$loan->devise;
		   
                  
                             
		                if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
                                  {  	 
		                          	 $l_payroll_month= $loan->payroll_month;
		                          	 $l_amount= $loan->amount;
		                          	 $l_percent_of_deduct= $loan->deduction_percentage;
		                          	 $l_solde= $loan->solde;
		
		                          	$temp_month = $this->payroll_month;
		                          	
		                          	if($l_payroll_month <= $temp_month )
		                          	  { 
		                          	     $l_remaining_month_number = $loan->remaining_month_number -1;
		                          	 
		                          	 //$deduction_ = ( ($total_gross_salary * $l_percent_of_deduct)/100);
		                          	 $deduction_ = ( ($total_gross_salary_initial * $l_percent_of_deduct)/100);
		                          	 
		                          	 
		                          	 $total_deduction = $total_deduction + $deduction_;
		  	         	
		  	         	             $l_solde=$l_solde-$deduction_;
		  	         	             
		  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
		  	         	             
		  	         	             //if $l_solde >0, ==0, <0
		  	         	             if($l_solde>0)
		  	         	               {
		  	         	               	  $dataModelLoan1->setAttribute('solde',$l_solde);
		  	         	               	  $dataModelLoan1->setAttribute('paid',0);
		  	         	               	  $dataModelLoan1->setAttribute('remaining_month_number',$l_remaining_month_number);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               	  
		  	         	               	  if($dataModelLoan1->save())  
							  	         	{
		  	         	               	       $total_net_salary = $total_net_salary - $deduction_;
							  	         	}
							  	         	
		  	         	                 }
		  	         	              elseif($l_solde==0)
		  	         	                {
		  	         	                	$dataModelLoan1->setAttribute('solde',0);
		  	         	                  $dataModelLoan1->setAttribute('paid',1);
		  	         	                   $dataModelLoan1->setAttribute('remaining_month_number',0);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               	  
		  	         	               	  if($dataModelLoan1->save())  
							  	      	   {
		  	         	               	        $total_net_salary = $total_net_salary - $deduction_;
							  	      	   }
							  	      	   
		  	         	                  }
		  	         	               elseif($l_solde<0)
		  	         	                 {
		  	         	                  	$dataModelLoan1->setAttribute('solde',0);
		  	         	               	  $dataModelLoan1->setAttribute('paid',1);
		  	         	               	   $dataModelLoan1->setAttribute('remaining_month_number',0);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               	  
		  	         	               	  if($dataModelLoan1->save())  
							  	         	{ 
							  	         		$total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
							  	         	}
		  
		  	         	                   }
		  	         	                   
		  	         	                   
		                          	    } 
		                          	    
                                     }
                                   elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
                                      {
                                      	
                                       
                                          	
                                      	  //cheche liy ki koresponn ak mwa pewol la ki poko peye
                                      	    $id_loan= $loan->id;
                                            $loan_devise =$loan->devise;
                                      	    
                                      	     
                                      	    $modelLoanElements = new LoanElements;
                                      	    $loanElements = $modelLoanElements->find()
                                      	                                     ->where('loan_id='.$id_loan.' and month='.$this->payroll_month.' and is_paid=0')
                                      	                                     ->all();
                                      	    
                                      	    if($loanElements!=null)
                                      	      {  
                                      	      	 foreach($loanElements as $loanElem)
                                      	      	   { 
                                      	      	       $l_payroll_month= $loanElem->month;
                                                       
                                                       
                                                      if($model->payroll_month == $l_payroll_month)
                                                       {                                                           
                                                                //tcheke deviz la
                                                           if(($model->devise == $loan_devise)||( ($model->devise != $loan_devise)&&($model->loan_rate!=0)) )
                                                            { 
                                                       
							                          	 $l_amount= $loan->amount;
							                          	 $l_percent_of_deduct= $loanElem->amount;
							                          	 
										               if($model->devise != $loan_devise)
										                {
										                 if( ($model->loan_rate!=0)&&($model->loan_rate!='') )
										                   {
										                     if($model->devise==1)//HTG
										                        $l_percent_of_deduct= round( ($loanElem->amount*$model->loan_rate),2);
										                     else//$USD et autres
										                         $l_percent_of_deduct= round( ($loanElem->amount/$model->loan_rate),2);
												    }
										                  else
										                    { $l_percent_of_deduct= $loanElem->amount;
										                       $is_save=FALSE;
										                     }
										                 }
										               else
										                  { $l_percent_of_deduct= $loanElem->amount;
										                    $model->loan_rate =null;
										                  }
										                   
										                          	
										                   $l_solde= $loan->solde;
			                                      	      	       
			                                      	       $l_remaining_month_number = $loan->remaining_month_number -1;
					                          	 
										                    //$deduction_ = ( ($total_gross_salary * $l_percent_of_deduct)/100);
										                    $deduction_ = $l_percent_of_deduct;
							                          	 
							                          	 
							                          	 $total_deduction = $total_deduction + $deduction_;
							  	         	
							  	         	       if($model->devise != $loan_devise)
										                {
							  	         	             if(($model->loan_rate!=0)&&($model->loan_rate!='') )
										                   {
										                      $l_solde = $l_solde - $loanElem->amount;
										                    										                          
										                    }
										                 else
							  	         	                $l_solde = $l_solde - $deduction_; 
							  	         	                
										                }
										              else
										                {
										                	 $model->loan_rate =null;
										                	 $l_solde = $l_solde - $deduction_; 
										                  }
							  	         	                
							  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
							  	         	             
							  	         	             $modelLoanElements_new = $modelLoanElements->findOne($loanElem->id);
							  	         	             
							  	         	             //if $l_solde >0, ==0, <0
							  	         	             if($l_solde>0)
							  	         	               {
							  	         	               	  $dataModelLoan1->setAttribute('solde',$l_solde);
							  	         	               	  $dataModelLoan1->setAttribute('paid',0);
							  	         	               	  $dataModelLoan1->setAttribute('remaining_month_number',$l_remaining_month_number);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
							  	         	               	  if($dataModelLoan1->save())  
							  	         	               	   {
								  	         	               	  $modelLoanElements_new->setAttribute('is_paid',1);
								  	         	               	 
								  	         	               	 if( $modelLoanElements_new->save())
							  	         	               	      {
                                      							  	         	               	         $total_net_salary = $total_net_salary - $deduction_;
							  	         	               	      }
							  	         	               	   }
							  	         	               	   
							  	         	                 }
							  	         	              elseif($l_solde==0)
							  	         	                {
							  	         	                	$dataModelLoan1->setAttribute('solde',0);
							  	         	                  $dataModelLoan1->setAttribute('paid',1);
							  	         	                   $dataModelLoan1->setAttribute('remaining_month_number',0);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
							  	         	               	  if($dataModelLoan1->save())  
							  	         	               	   {
								  	         	               	  $modelLoanElements_new->setAttribute('is_paid',1);
							  	         	               	      
							  	         	               	      if($modelLoanElements_new->save())
							  	         	               	       {
							  	         	               	          $total_net_salary = $total_net_salary - $deduction_;
							  	         	               	       }
							  	         	               	        else
							  	         	               	           $is_save=FALSE;
							  	         	               	   }
							  	         	               	   
							  	         	                  }
							  	         	               elseif($l_solde<0)
							  	         	                 {
							  	         	                  	$dataModelLoan1->setAttribute('solde',0);
							  	         	               	  $dataModelLoan1->setAttribute('paid',1);
							  	         	               	   $dataModelLoan1->setAttribute('remaining_month_number',0);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
								  	         	               	 if($dataModelLoan1->save())  
								  	         	               	  {
								  	         	               	    $modelLoanElements_new->setAttribute('is_paid',1);
							  	         	               	        
							  	         	               	        if($modelLoanElements_new->save())
							  	         	               	         {
				                                                                                                            $total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
							  	         	               	         }
							  	         	               	        else
							  	         	               	           $is_save=FALSE;
								  	         	               	  }
							  
							  	         	                   }
							  	         	                   
                                      	      	       
                                      	      	       
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   
                                                           }//deviz
                                                         elseif(( ($devise != $loan->devise)&&($loan_rate==0)) || ( ($devise != $loan->devise)&&($loan_rate==null))  )
                                                             {
                                                               /* if($str_name =='')
                                                                    $str_name =  ;
                                                                  else
                                                                     $str_name = $str_name.','. ;
                                                                */
                                                                     $is_save=FALSE;

                                                              }  
                                                              
                                                       }
                                                    else 
                                                        {
                                                                break;
                                                           
                                                       }
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   
                                      	      	       
                                      	      	   }
                                      	      	} 
                                      	      	
                                           
                                      
                                        }
                                        
  	         	                 } 
  	         	            
  	         	             }
  	         	             
  	         	             
                       }

		           
		                  		                   	 
		                  	 
		              // if($model->cash_check=='')
		              //    $model->setAttribute('cash_check', Yii::t('app','Cash') );
		           
			           $model->setAttribute('id_payroll_set',$id_payroll_set);
			           $model->setAttribute('id_payroll_set2',$id_payroll_set2);
			           //$model->setAttribute('plus_value',$plus_value);
			           $model->setAttribute('loan_rate',$loan_rate);
					                  
			           $model->setAttribute('payroll_month',$this->payroll_month);
			          $model->setAttribute('missing_hour',$total_missing_hour);
			          $model->setAttribute('taxe',$total_taxe);
			           $model->setAttribute('gross_salary',$total_gross_salary_initial);
			           $model->setAttribute('net_salary',$total_net_salary);
			           
			           $model->setAttribute('plus_value',$plus_value);
			          
							    
                                   
						$model->setAttribute('date_created',date('Y-m-d H:i:s'));
					  	$model->setAttribute('created_by',currentUser());

			            
					if($model->save())
					 {
					   if($is_save)
					    {
					    	$dbTrans->commit();
				    	
						   $month_=$model->payroll_month;
				           $year_= getYear($model->payment_date);
				           
				           //$this->redirect(array('index','month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>''));  
				                 return $this->redirect(['view','id'=>$model->id,'month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>'']);
					      }
					    else
					      { $dbTrans->rollback();
					          
					       }
					      
					   }
					else
					   $dbTrans->rollback();
		       
					
					
				}
     	     else  //fen "any payroll after this month"
     	       {  
     	       	    //$this->message_anyPayrollAfter=true;
     	       	    Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','Payroll is not allowed because we already have payroll done for a next month.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
     	       	    
     	         }
					
					
					
					
					
					
		  }
	//fen youn pa youn_________________
	elseif($this->group==1)
	   {   
	   	       
	//an gwoup ____________________________				
					   $this->message_noOneSelected=false;
					   $this->message_wrongMissingTime=false;
					   $this->message_group_anyPayrollAfter=false;
					   Yii::$app->session['message_group_anyPayrollAfter']=0;
					   
					   $temwen=false;
					   $person='';
					   $missingHour='';
					   $month='';
					   
					   $temp_month='';
					   $assurance = 0;
					   $loan_rate = 0;
					   
					   $month_='';
					   $year_='';
					   $all='';
					   
					   $payroll_date = $model->payroll_date;
					   $payroll_month = $model->payroll_month;
					   $payment_date = $model->payment_date;   	
					   $devise = $model->devise;
					   
					    $month_=$model->payroll_month;
						$year_= getYear($model->payment_date);
					
					  
				 $selection=(array)Yii::$app->request->post('selection');//typecasting
			 if($selection!=null)
				{ foreach($selection as $id)
		           {  
		           	 $plus_value = 0;
		           	 $assurance = 0;
		           	 $frais = 0;
		           	 
		           	 $is_save=TRUE;
		           	 
		           	 $dbTrans = Yii::$app->db->beginTransaction(); 
		           	 
		             $id_payroll_set = (int)$id;
		             $modelPS_ = SrcPayrollSettings::findOne($id_payroll_set);
		             
		             $this->person_id =$modelPS_->person_id;
		             
		            /*  if( (isset($_POST['plusValue'][$id_payroll_set]))&&($_POST['plusValue'][$id_payroll_set]!='') )
							 {    
								  $plus_value = $_POST['plusValue'][$id_payroll_set];
							   }
					  */
					
					$modelPayrollPlusvalue= new SrcPayrollPlusvalue();
                    $plus_value = $modelPayrollPlusvalue->getPayrollPlusvalue($this->person_id,$payroll_month,$acad); 
                 
                 
					  if( (isset($_POST['loanRate'][$id_payroll_set]))&&($_POST['loanRate'][$id_payroll_set]!='') )
							 {    
								  $loan_rate = $_POST['loanRate'][$id_payroll_set];
								  
								  if($loan_rate =='')
								   $loan_rate = 0;
							   }			                 
                    
		                     
                 $total_missing_hour = 0;
                 $missing_hour =0;
                 $numberHour_=0;
                 $all='';
                 
                 //check if there is any after this payroll_month
                 //return 0:false, 1:true
                  $any_after = $this->anyPayrollDoneAfterForOnes($devise, $payroll_month,$payroll_date,$this->person_id,$acad);
                  
               if($any_after==1)  //anyPayrollAfter
                  {  //Yii::app()->session['message_group_anyPayrollAfter']=1;
	                      Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','People who have payroll already done for any month that follow this one are rejected.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
	                  }
	               else
	               {
	                 //check if it is a timely salary 
                   $model_ = new SrcPayrollSettings;
                     $pay_set = SrcPayrollSettings::find()
                                             ->orderBy(['date_created'=>SORT_DESC])
                                             ->where('payroll_settings.old_new=1 AND payroll_settings.person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.devise='.$devise.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2)) ')
                                             ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set2 = null;
                    $id_payroll_set_emp=null;
                    $id_payroll_set_teach=null;
                    $total_gross_salary=0;
                    $total_gross_salary_initial = 0;
                    $total_net_salary=0;
                    $total_deduction=0;
                    $total_taxe=0;
                    $iri_deduction =0;
                    $frais = 0;
                    
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary_initial =0;
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
                           $assurance = 0;
                                
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $gross_salary_initial =$amount->amount;
			               	   
			               	   $missing_hour = $amount->number_of_hour;
			               	   
			               	   $numberHour_ = $amount->number_of_hour;
			               	   
			               	   $assurance =  $amount->assurance_value;
			               	   
			               	   $frais =  $frais+ $amount->frais;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	 if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['SrcPayrollSettings']))
			             	  {  
				             	 //return working times in minutes
				             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
				                 
				                 $missing_hour = $missing_hour - $number_of_hour;
				                 
				                  $total_missing_hour =  $total_missing_hour + $missing_hour;
			             	  }
			             	 else
			             	    $number_of_hour = $missing_hour;
			                 //calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						          
						        }
			                  
			                  if(($numberHour_!=null)&&($numberHour_!=0))
						       {
						          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
						
						        } 
			                   
			                  
			                   
			                   
			                   
			             
				            }
			           
			            //$total_gross_salary = $total_gross_salary + $gross_salary;
			            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
			          
			         if($payroll_month == 0)
			           {
			           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
                        	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
			           }
			         else
			            {  
			          	  if($this->taxe==1)
	                         {
	                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
                     	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
	                       	 } 
	                      elseif($this->taxe==0)
	                        {
	                        	//$net_salary = $gross_salary;
	                        	$net_salary = $gross_salary_initial - $assurance;
	                        	
	                          }
	                          
			             }  
                          
                          $total_net_salary = $total_net_salary + $net_salary;
                          
                          
                          
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
                                $assurance = 0;
                                
	                     
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $gross_salary_initial =$amount->amount;
					               	   
					               	   $missing_hour = $amount->number_of_hour;
					               	   
					               	   $numberHour_ = $amount->number_of_hour;
					               	   
					               	   $assurance =  $amount->assurance_value;
					               	   
					               	   $frais = $frais + $amount->frais;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	  if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
						             	  {  
							             	 //return working times in minutes
							             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
							                 
							                 $missing_hour = $missing_hour - $number_of_hour;
							                 
							                 $total_missing_hour =  $total_missing_hour + $missing_hour;
						             	  }
						             	 else
						             	    $number_of_hour = $missing_hour;
					                 
					                 //calculate $gross_salary by hour if it's a timely salary person 
								    /* if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
								      */
								        
								     if(($numberHour_!=null)&&($numberHour_!=0))
								       {
								          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
								
								        } 
					                   
					             
						            }
					           
					            //$total_gross_salary = $total_gross_salary + $gross_salary;
					            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
					          
					          if($payroll_month == 0)
					           {
					           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
		                        	    
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	   // $net_salary = $gross_salary - $deduction;
			                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
					           }
					          else
					          	{ 
					          	  if($this->taxe==1)
			                         {
			                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	    //$net_salary = $gross_salary - $deduction;
 			                       	    
			                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
			                       	 } 
			                      elseif($this->taxe==0)
			                        {
			                        	//$net_salary = $gross_salary;
			                        	$net_salary = $gross_salary_initial - $assurance;
			                          }  
			                          
					          	}
		                          
		                          $total_net_salary = $total_net_salary + $net_salary;
		                          
                          
                          
                               }
                       
                      }
                       
                       
                      if(($as_emp==1) && ($as_teach==1))
		                   {  //alafwa employee e teacher
		                   	   //save payroll la sou employee
		                   	   
		                   	   $all='e';
		                   	   
		                   	   $id_payroll_set=$id_payroll_set_emp;
		                   	   $id_payroll_set2 = $id_payroll_set_teach;
		                   	   $number_of_hour=null;
		                   	   $missing_hour=null;
		                   	   
		                   	 }

                     if($payroll_month != 0)
			           {
			           	
                      //DEDUCTION TAXE IRI
                       if($this->taxe==1)
	                     {
	                     		//IRI
	                       	$iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary_initial);
	                       	$total_deduction = $total_deduction + $iri_deduction;
	                       	$total_net_salary = $total_net_salary - $iri_deduction;
	      
	       
	                       }
	                       
			           }
                       
                      
                    
                    
                 $total_net_salary = $total_net_salary + $frais + $plus_value;    
                     
                       //loan nan ap prelve sou montan total payroll moun nan
                       //check if there is a loan
                       $modelLoan = new SrcLoanOfMoney;
                       $dataModelLoan = $modelLoan->find()
                                               ->where('person_id='.$this->person_id.' and academic_year='.$acad.' and paid=0')
                                               ->orderBy(['loan_date'=>SORT_ASC])
                                               ->limit('1')
                                               ->all();
                        
                                      
                        $total_taxe = $total_taxe + $total_deduction;
            
                   
                   if($payroll_month != 0)
			         {
			           	     
                        if($dataModelLoan!=null)
                          { 
                          	 $id_loan= 0;
                          	 $loan_devise='';
                          	 
                          	 $loan_=$dataModelLoan;//->getData();
                          	foreach($loan_ as $loan)
                          	   { 
		                          	 $id_loan= $loan->id;
		                          	 $loan_devise = $loan->devise;
		                           	 
		               if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
                                  {	 
		                          	 $l_payroll_month= $loan->payroll_month;
		                          	 $l_amount= $loan->amount;
		                          	 $l_percent_of_deduct= $loan->deduction_percentage;
		                          	 $l_solde= $loan->solde;
		
		                          	$temp_month = $payroll_month;
	                         	
		                          	if($l_payroll_month <= $temp_month )
		                          	  { 
		                          	     $l_remaining_month_number = $loan->remaining_month_number -1;
		                          	 
		                          	 //$deduction_ = ( ($total_gross_salary * $l_percent_of_deduct)/100);
		                          	 $deduction_ = ( ($total_gross_salary_initial * $l_percent_of_deduct)/100);
		                          	 
		                          	 
		                          	 $total_deduction = $total_deduction + $deduction_;
		  	         	
		  	         	             $l_solde=$l_solde-$deduction_;
		  	         	             
		  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
		  	         	             
		  	         	             //if $l_solde >0, ==0, <0
		  	         	             if($l_solde>0)
		  	         	               {
		  	         	               	  $dataModelLoan1->setAttribute('solde',$l_solde);
		  	         	               	  $dataModelLoan1->setAttribute('paid',0);
		  	         	               	  $dataModelLoan1->setAttribute('remaining_month_number',$l_remaining_month_number);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               	  $dataModelLoan1->save();  
		  	         	               	  
		  	         	               	   $total_net_salary = $total_net_salary - $deduction_;
		  	         	                 }
		  	         	              elseif($l_solde==0)
		  	         	                {
		  	         	                	$dataModelLoan1->setAttribute('solde',0);
		  	         	                  $dataModelLoan1->setAttribute('paid',1);
		  	         	                   $dataModelLoan1->setAttribute('remaining_month_number',0);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               	  $dataModelLoan1->save();
		  	         	               	  
		  	         	               	    $total_net_salary = $total_net_salary - $deduction_;
		  	         	                  }
		  	         	               elseif($l_solde<0)
		  	         	                 {
		  	         	                  	$dataModelLoan1->setAttribute('solde',0);
		  	         	               	  $dataModelLoan1->setAttribute('paid',1);
		  	         	               	   $dataModelLoan1->setAttribute('remaining_month_number',0);
		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
		  	         	               
                                                          $dataModelLoan1->save();
		  	         	               	  
		  	         	               	     $total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
		  
		  	         	                   }
		  	         	                   
		  	         	                   
		                          	    } 
  	         	                 
  	         	                 }
                                   elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
                                      {
                                      	  //cheche liy ki koresponn ak mwa pewol la ki poko peye
                                      	    $id_loan= $loan->id;
                                      	    $loan_devise = $loan->devise;
 	   
                                      	    
		                                      	    
	                                      	    $modelLoanElements = new LoanElements;
	                                      	    $loanElements = LoanElements::find()->where('loan_id='.$id_loan.' and month='.$payroll_month.' and is_paid=0')->all();
                                       	    
                                      	      if($loanElements!=null)
                                      	        {  
                                      	      	   foreach($loanElements as $loanElem)
                                      	      	     { 
                                      	      	       $l_payroll_month= $loanElem->month;
                                                       
                                                                                                                  
                           
                                                               //tcheke deviz la
                                                           if(($devise == $loan_devise)||( ($devise != $loan_devise)&&($loan_rate!=0)) )
                                                            { 
							                          	 $l_amount= $loan->amount;
							                          	 $l_percent_of_deduct= $loanElem->amount;
							                          	 
										              if($devise != $loan_devise)
										                {
							                	
										                	
										                 if( ($loan_rate!=0)&&($loan_rate!='') )
										                   {
										                     if($devise==1)//HTG
										                        $l_percent_of_deduct= round( ($loanElem->amount*$loan_rate),2);
										                     else//$USD et autres
										                         $l_percent_of_deduct= round( ($loanElem->amount/$loan_rate),2);
										                          	   	 
										                    }
										                  else
										                    { $l_percent_of_deduct= $loanElem->amount;
										                       $is_save=FALSE;
										                    }
										                 }
										                  else
										                    { 
                                                                                                      $l_percent_of_deduct= $loanElem->amount;
										                       $loan_rate=null;
										                    }
										                
										                          	
										                   $l_solde= $loan->solde;
			                                      	      	       
			                                      	       $l_remaining_month_number = $loan->remaining_month_number -1;
					                          	 
										                    //$deduction_ = ( ($total_gross_salary * $l_percent_of_deduct)/100);
										                    $deduction_ = $l_percent_of_deduct;
							                          	 
							                          	 
							                          	 $total_deduction = $total_deduction + $deduction_;
							  	         	
							  	         	            
                                                         if($devise != $loan_devise)
										                  {
								  	         	             if(($loan_rate!=0)&&($loan_rate!='') )
											                   {
											                      $l_solde = $l_solde - $loanElem->amount;
											                    										                          
											                    }
											                 else
								  	         	                $l_solde = $l_solde - $deduction_; 
								  	         	                
										                  }
										                else
										                  {   
                                                                                                    $l_solde = $l_solde - $deduction_; 
										                  	  $loan_rate=null;
										                  	}
								  	         	                
										                 
							  	         	                
							  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
							  	         	             
							  	         	             $modelLoanElements_new = $modelLoanElements->findOne($loanElem->id);
							  	         	             
							  	         	             //if $l_solde >0, ==0, <0
							  	         	             if($l_solde>0)
							  	         	               {
							  	         	               	  $dataModelLoan1->setAttribute('solde',$l_solde);
							  	         	               	  $dataModelLoan1->setAttribute('paid',0);
							  	         	               	  $dataModelLoan1->setAttribute('remaining_month_number',$l_remaining_month_number);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
							  	         	               	  if($dataModelLoan1->save())  
							  	         	               	   {
								  	         	               	  $modelLoanElements_new->setAttribute('is_paid',1);
								  	         	               	  if($modelLoanElements_new->save())
							  	         	               	     {
							  	         	               	        $total_net_salary = $total_net_salary - $deduction_;
							  	         	               	     }
							  	         	               	        else
							  	         	               	           $is_save=FALSE;
							  	         	               	        
							  	         	               	   }
							  	         	               	   
							  	         	                 }
							  	         	              elseif($l_solde==0)
							  	         	                {
							  	         	                	$dataModelLoan1->setAttribute('solde',0);
							  	         	                  $dataModelLoan1->setAttribute('paid',1);
							  	         	                   $dataModelLoan1->setAttribute('remaining_month_number',0);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
							  	         	               	  if($dataModelLoan1->save())  
							  	         	               	   {
								  	         	               	  $modelLoanElements_new->setAttribute('is_paid',1);
							  	         	               	      
							  	         	               	      if($modelLoanElements_new->save())
							  	         	               	      {
							  	         	               	        $total_net_salary = $total_net_salary - $deduction_;
							  	         	               	      }
							  	         	               	        else
							  	         	               	           $is_save=FALSE;
							  	         	               	   }
							  	         	               	   
							  	         	                  }
							  	         	               elseif($l_solde<0)
							  	         	                 {
							  	         	                  	$dataModelLoan1->setAttribute('solde',0);
							  	         	               	  $dataModelLoan1->setAttribute('paid',1);
							  	         	               	   $dataModelLoan1->setAttribute('remaining_month_number',0);
							  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
							  	         	               	  
								  	         	               	 if($dataModelLoan1->save())  
								  	         	               	  {
								  	         	               	    $modelLoanElements_new->setAttribute('is_paid',1);
							  	         	               	        
							  	         	               	        if($modelLoanElements_new->save())
							  	         	               	         {
							  	         	               	            $total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
							  	         	               	         }
							  	         	               	        else
							  	         	               	           $is_save=FALSE;
								  	         	               	  }
							  
							  	         	                   }
							  	         	                   
                                      	      	       
                                      	      	         }//deviz
                                                      elseif(( ($devise != $loan->devise)&&($loan_rate==0)) || ( ($devise != $loan->devise)&&($loan_rate==null))  )
                                                         {

                                                            /* if($str_name =='')
                                                                 $str_name =  ;
                                                               else
                                                                  $str_name = $str_name.','. ;
                                                            */
                                                                $is_save=FALSE;
                                                            }
                                                            
                                                       
                                      	      	       
                                      	      	   }
                                      	      	} 
                                      	      	
                                      	      	
                                            
                                      
                                        }									
  	         	                 
  	         	                 
  	         	                 
  	         	                 
  	         	                 break;
  	         	                 
  	         	                 
  	         	                 } 
  	         	            
  	         	             }
  	         	             
  	         	             
                       }

			                   	 
			       /*          if($model->cash_check=='')
		                  $model->setAttribute('cash_check', Yii::t('app','Cash') );
		           */
			           $model->setAttribute('id_payroll_set',$id_payroll_set);
			           $model->setAttribute('id_payroll_set2',$id_payroll_set2);
			           $model->setAttribute('payroll_month',$payroll_month);
			          $model->setAttribute('missing_hour',$total_missing_hour);
			           $model->setAttribute('taxe',$total_taxe);
			           $model->setAttribute('plus_value',$plus_value);
			           $model->setAttribute('loan_rate',$loan_rate);
			           $model->setAttribute('gross_salary',$total_gross_salary_initial);
			           $model->setAttribute('net_salary',$total_net_salary);
			           $model->setAttribute('payment_date',$payment_date);
			           $model->setAttribute('payroll_date',$payroll_date);
							 
						$model->setAttribute('date_created',date('Y-m-d H:i:s'));
					  	$model->setAttribute('created_by',currentUser());

			            
									if($model->save())
									 {
									   //$month_=$model->payroll_month;
							           //$year_= getYear($model->payment_date);
							           	if($is_save)
							           	  {
									           $dbTrans->commit();
									           
									           	unSet($model);
												$model= new SrcPayroll;
															   
												 $total_taxe =0;
												 $total_missing_hour = 0;
												 
												 $assurance = 0;
												 $frais = 0;
															   
												$temwen=true;
												
							           	  }
							           	 else
							           	     $dbTrans->rollback();
	
									   }
						             else
						                $dbTrans->rollback();
			       
						
						
				                     
							     }
									   
		                         
		                }//fe message_group_anyPayrollAfter		   
		                     
							   
				        }
				      else //message vous n'avez croche personne
						{
							//$this->message_noOneSelected=true;
							
							Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','You must select at least a person.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
							
							}
							
					
					  if($temwen)
					    {
					    	 	
						    return $this->redirect(['index','month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>'']);  //$this->redirect(array('view','id'=>$model->id,'part'=>'pay','from'=>''));
					   
						     	
						     					            
					            
					     }
								
					 }		
		//fen an gwoup___________________________
		
		
		
		       }
     	     else  //$model->payment_date or $model->payroll_date not in academic range
     	      {// $this->message_PayrollDatePaymentDate_notInAcadRange=true;
     	      
     	            Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','-payroll date- or -Payment date- is not in this academic year.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
     	      
     	        }
		
		
				
     	  }
     	else  //$model->payment_date < $model->payroll_date
     	  {  //$this->message_PaymentDate=true;
     	       
     	         Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','-Payment date- cannot be earlier than -payroll date-.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
     	  }
     	  
     	  
     }
  else  //$model->payroll_month != getMonth($model->payroll_date)
    {   //$this->message_PayrollDate=true;
           Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','Please make sure -payroll date- is in the appropriate -payroll month-.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
														
        }
			  	
			  	
			  	
          
		  }//fen if($_POST['create'])	
		  
		  if(isset($_POST['cancel']))
			  {    
			  	$model=new Payroll;
				$this->person_id='';
				$this->payroll_month = '';
				Yii::$app->session['payroll_person_id']='';
				$this->grouppayroll = '';
				$this->group = 0;
				$this->taxe=0;
					
			  }
			  
		 }  
	  else
       {
       	       if((isset($_GET['from1']))&&($_GET['from1']=='vfr'))
       	         {
       	         	   $this->group =0;
                       
                         $this->person_id = $_GET['emp'];
					     Yii::$app->session['payroll_person_id']=$this->person_id;
		             
		              $this->payroll_month = $_GET['month_'];
					     Yii::$app->session['payroll_payroll_month'] = $this->payroll_month;
       	          
       	          }
		            
			
       	  }

            return $this->render('create', [
                'model' => $model,
                'person_id'=>$this->person_id,
                'group' => $this->group,
                'payroll_month'=>$this->payroll_month,
                'grouppayroll'=>$this->grouppayroll,
                'allDevises'=>$allDevises,
            ]);
            
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }

    /**
     * Updates an existing Payroll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('billings-payroll-update'))
         {
         	
         	$acad=Yii::$app->session['currentId_academic_year'];
         	
         	$payroll_setting_twice = infoGeneralConfig('payroll_setting_twice'); 
         	
         	$loan_payment_method = infoGeneralConfig('loan_payment_method');
         	
		$all='';
		$this->message_UpdatePastDate=false;
		$this->message_PayrollDate=false;
		$this->message_PaymentDate=false;  
		
		$id_payroll_set='';              
                
		
		$loan_=0;
		$loan_rate=0;
		$total_taxe = 0;
		
		 $model_new=new SrcPayroll;
		 $modelPS = new SrcPayrollSettings();
		 $modelPers=new SrcPersons;
		 
		 $allDevises = getDevises();
              $default_devise = getDefaultCurrency();
              
              $model_new->devise= $default_devise->id;
             // $model->group_payroll = [1];
		 
	if(!isset($_GET['group']))
      {	
		$id_payroll_set ='';
		$gross_salary ='';
		$timely_salary = 0;
		
		$model = $this->findModel($id);
		
		 $model->devise= $model->idPayrollSet->devise;
		 
		 if($model->taxe!=0)
		   { $total_taxe = $model->taxe;
		      $model->taxe = 1;
		   }
		
		 if($model->plus_value =='')
            $model->plus_value =0;
         
         if($model->loan_rate =='')
            $model->loan_rate =0;
            
          $loan_rate = $model->loan_rate;
          $old_rate =$model->loan_rate;
		   
		 $payment_date = $model->payment_date;
		 $payroll_date = $model->payroll_date;
		 $payroll_month = $model->payroll_month;

		 $this->person_id = $model->idPayrollSet->person_id;
		 $this->payroll_month = $model->payroll_month;
		 $id_payroll_set = $model->id_payroll_set;
		 
		 $loan_ = 0;
					
					$assurance_ = 0;
					$frais =$model->getFrais();
					
					$assurance_ = $model->getAssurance();
		
		//$loan = Payroll::model()->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalary($model->idPayrollSet->person_id),$model->idPayrollSet->number_of_hour,$model->missing_hour,$model->net_salary,$model->taxe); 
		$loan_ = $model_new->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalaryIndex_value($model->devise,$model->idPayrollSet->person_id,$payroll_month,getYear($payment_date)  ),$model->idPayrollSet->number_of_hour,0,$model->net_salary,$model->getFrais_numb(),$model->plus_value,$total_taxe,$model->getAssurance_numb(),$payroll_month);
	/*------ nbre jours apres la date de paiement, on ne pourra plus editer les enregistrements    -------*/	
	//Extract limit payroll update 
		$limit_payroll_update = infoGeneralConfig('limit_payroll_update');
		
		if($limit_payroll_update!='')
		 {
		
			$date = $model->payment_date;
	        $newdate = strtotime ( '+'.$limit_payroll_update.' day' , strtotime ( $date ) ) ;
	        $newdate = date ( 'Y-m-j' , $newdate );//date('Y-m-d')
	         if(date('Y-m-d')>$newdate)
			   {
			     //$this->message_UpdateValidate=1;
			     //header('Location: ' . $_SERVER['HTTP_REFERER']);
			      $url=Yii::$app->request->urlReferrer;
				  return $this->redirect($url.'/msguv/y');
				  //$this->redirect(Yii::app()->request->urlReferrer);
			    }
			
          }	       	
				       	
		
		if($model->taxe!=0)
		   $this->taxe=1;
		   
		 $this->person_id =$model->idPayrollSet->person_id; 
		 
		 $id_payroll_set = $model->id_payroll_set;
		 $id_payroll_set2 = $model->id_payroll_set2;
		 
		 
		 $missing_hour1 ='';
		 $number_of_hour = $model->number_of_hour;
		//check if it is a timely salary 
		 //$check = missingTimes($this->person_id,$number_of_hour,$acad);
		 
		$missing_hour1 = $model->missing_hour;
		
         /*          $model_ = new SrcPayrollSettings;
                   $amount = SrcPayrollSettings::find()->where('person_id='.$this->person_id.' AND academic_year=:acad order by date_created DESC',array(':Id'=>$this->person_id,':acad'=>$acad));
                     if(isset($amount)&&($amount!=null))
		               {  
		               	   $id_payroll_set = $amount->id;
		               	   
		               	   $gross_salary =$amount->amount;
		               	   
		               	   if($amount->an_hour==1)
		                     {  
		                     	 $timely_salary = 1;
		                     	 //return working times in minutes
		             	         $base_number_of_hour = $this->getHours($this->person_id,$acad);
		             	         
		                         $missing_hour1 = $base_number_of_hour - $number_of_hour;
		                     }
		                 }

              if($missing_hour1>0)
                {
                	$model->setAttribute('missing_hour',$this->setMissingTimeForUpdate($missing_hour1));
                	
                	}
                	
               */
               
           //if($check!=null)    
              //$model->setAttribute('missing_hour',$missing_hour1);  	

       }
     else 
        {
                $model=new SrcPayroll;
                $condition='';
                $this->group = 1;
                $timely_salary = 0; 
               
               	
               	
               	 if(isset($_POST['SrcPayroll']['devise']))
					  {
                           $model->devise = $_POST['SrcPayroll']['devise'];
                       }
                       
                       
                 if(isset($_POST['SrcPayroll']['payroll_month']))
		            {  
		              	$this->payroll_month = $_POST['SrcPayroll']['payroll_month'];
					     
		              }
 
                
               if($payroll_setting_twice==1) //yon moun ka konfigire 1 sel fwa selman
                  $this->grouppayroll = 1;
               elseif($payroll_setting_twice==2) //yon moun ka konfigire 2 fwa 
                {
                  if(isset($_POST['SrcPayroll']['group_payroll']))
					  {
                        $this->grouppayroll = $_POST['SrcPayroll']['group_payroll'];
                        
                          if($this->grouppayroll!=Yii::$app->session['payroll_group_payroll'])
                            {  
                            	// Yii::$app->session['payroll_group_payroll'] = $this->grouppayroll;
                            	$this->payroll_month=null;
                            	 $this->payroll_date =null;
                                     $this->taxe =0;
                                    
                              }
                        
                       }
                
                   }
                        	
		                 
           
                            if($this->grouppayroll==2)//teacher
                              {   
                                 
                                $condition='p.is_student=0 AND p.active IN(1, 2) ';
                                    
                                    $header=Yii::t('app','Teachers name');
                                   // $all='t';
                                   // $this->grouppayroll=2;
                                                                    }
                               elseif($this->grouppayroll==1)//employee
                                  { $condition='is_student=0 AND active IN(1, 2)  ';
                                  
                                      $header=Yii::t('app','Employees name');
                                     // $all='e';
                                     // $this->grouppayroll=1;
                                     
                                   }
                
               if($this->payroll_month=='')
                 {    $as_ =0;
                 	//get the last payroll month
                       if($this->grouppayroll==2)
                          $as_ =1;
                       
                           
                 	$dataProvider_last_pay= $model_new->getInfoLastPayrollByPayroll_group($devise,$as_);
                 	$last_pay=$dataProvider_last_pay->getModels();
                 	 
                         if($last_pay!=null)
                           {   foreach($last_pay as $r)
                                 {
                                     $this->payroll_month=$r->payroll_month;
                                     break;
                                 	}
                            
                         	  }
                       else
                         { 
                         	$this->payroll_month=-1;
                          }
                   
                   }
                 
                
                $this->payroll_date = $this->getLastPastPayrollDateByGroup($devise, $this->payroll_month);
                
                        $dataProvider=$model_new->searchPersonsForUpdatePayroll($condition, $devise, $this->payroll_month, $this->payroll_date, $acad);	
                         $info=$dataProvider->getModels();
                         
                         if($info!=null)
                           {   $taxe=0;
                               $payment_date='';
                              
                               foreach($info as $r)
                                 {
                                 	  if($r->taxe!=0)
                                 	    $this->taxe=1;
                                 	    
                                     $payment_date=$r->payment_date;
                                     $payroll_date=$r->payroll_date;
                                    
                                     break;
                                 	}
                                 	
                                	
							    $model->setAttribute('taxe',$this->taxe);
                         	    $model->setAttribute('payment_date',$payment_date);
                         	     $model->setAttribute('payroll_date',$this->payroll_date);
                         	    
                          	    
                         	  }
                         	  
                         	   
		              /*------ nbre jours apres la date de paiement, on ne pourra plus editer les enregistrements    -------*/	
					//Extract limit payroll update 
						$limit_payroll_update = infoGeneralConfig('limit_payroll_update');
						
						if($limit_payroll_update!='')
						 {
						   
							$date = $model->payment_date;
					        $newdate = strtotime ( '+'.$limit_payroll_update.' day' , strtotime ( $date ) ) ;
					        $newdate = date ( 'Y-m-j' , $newdate );//date('Y-m-d')
					         if(date('Y-m-d')>$newdate)
							   {
							     $this->group=1;
							     //$this->message_UpdateValidate=1;
							     //header('Location: ' . $_SERVER['HTTP_REFERER']);
							      $url=Yii::$app->request->urlReferrer;
								 return $this->redirect($url.'/msguv/y');
								  //$this->redirect(Yii::app()->request->urlReferrer);
							    }
							
				          }
				
                                 	
                 Yii::$app->session['payroll_group_payroll'] = $this->grouppayroll;
            
            }
            
            
         
         if ($model->load(Yii::$app->request->post())  ) 
          {
            			
				   if(isset($_POST['update']))
				    {    
					   $this->message_PayrollDate=false;
					   $this->message_PaymentDate=false;                
			            $plus_value =0; 
			            $loan_rate=0;  
			            $is_save=TRUE; 
			            
			            
					   
					    $model_new->load(Yii::$app->request->post() );
						  	  
						  
                   	          
						  	  $payment_date =  $model_new->payment_date;
						  	  $payroll_date =  $model_new->payroll_date;
						  	  $payroll_month = $model_new->payroll_month;
						  	  
						  	  //$plus_value = $model_new->plus_value;
						  	  
						  	  $modelPayrollPlusvalue= new SrcPayrollPlusvalue();
                 $plus_value = $modelPayrollPlusvalue->getPayrollPlusvalue($this->person_id,$payroll_month,$acad); 
                 
                 
						  	  $loan_rate = $model_new->loan_rate;
						  	  
						  	    if(isset($_POST['SrcPayroll']['taxe']))
						           {
			                        $this->taxe = $_POST['SrcPayroll']['taxe'];
			                        
			                        }
					   
					  
			 
			  	 
			   if(  ($payroll_month == getMonth($payroll_date) ) || ($payroll_month == 0) || ($payroll_month > 12)  )
			     {
			     	if($payment_date >= $payroll_date)
			     	  {
			   	   
					       
					    
					   if(!isset($_GET['group']))
					      { 
					      	$dbTrans = Yii::$app->db->beginTransaction(); 
				 //youn pa youn ___________________________	  
						  	  $missing_hour =0;
			                  $numberHour_=0;
			                  $all='';
			                           $pay_set = SrcPayrollSettings::find()
                                             ->orderBy(['date_created'=>SORT_DESC])
                                             ->where('payroll_settings.old_new=1 AND payroll_settings.person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.devise='.$model->devise.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2)) ')
                                             ->all();
					
					$as_emp=0;
                    $as_teach=0;
                    $id_payroll_set2 = null;
                    $id_payroll_set_emp=null;
                    $id_payroll_set_teach=null;
                    $total_gross_salary=0;
                    $total_gross_salary_initial = 0;
                    $total_net_salary=0;
                    $total_deduction=0;
                    $total_taxe=0;
                    $iri_deduction =0;
                    $frais = 0;
                    
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary_initial =0;
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
                           $assurance = 0;
                                
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $gross_salary_initial =$amount->amount;
			               	   
			               	   $missing_hour = $amount->number_of_hour;
			               	   
			               	   $numberHour_ = $amount->number_of_hour;
			               	   
			               	   $assurance =  $amount->assurance_value;
			               	   
			               	   $frais =  $frais+ $amount->frais;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	 if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['SrcPayrollSettings']))
			             	  {  
				             	 //return working times in minutes
				             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
				                 
				                 $missing_hour = $missing_hour - $number_of_hour;
				                 
				                  $total_missing_hour =  $total_missing_hour + $missing_hour;
			             	  }
			             	 else
			             	    $number_of_hour = $missing_hour;
			                 //calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						          
						        }
			                  
			                  if(($numberHour_!=null)&&($numberHour_!=0))
						       {
						          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
						
						        } 
			                   
			                  
			                   
			                   
			                   
			             
				            }
			           
			            //$total_gross_salary = $total_gross_salary + $gross_salary;
			            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
			          
			         if($model->payroll_month == 0)
			           {
			           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
                        	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
			           }
			         else
			            {  
			          	  if($this->taxe==1)
	                         {
	                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
                        	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	   // $net_salary = $gross_salary - $deduction;
	                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
	                       	 } 
	                      elseif($this->taxe==0)
	                        {
	                        	//$net_salary = $gross_salary;
	                        	$net_salary = $gross_salary_initial - $assurance;
	                        	
	                          }
	                          
			             }  
                          
                          $total_net_salary = $total_net_salary + $net_salary;
                          
                          
                          
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
                                $assurance = 0;
                                
	                     
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $gross_salary_initial =$amount->amount;
					               	   
					               	   $missing_hour = $amount->number_of_hour;
					               	   
					               	   $numberHour_ = $amount->number_of_hour;
					               	   
					               	   $assurance =  $amount->assurance_value;
					               	   
					               	   $frais = $frais + $amount->frais;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	  if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
						             	  {  
							             	 //return working times in minutes
							             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
							                 
							                 $missing_hour = $missing_hour - $number_of_hour;
							                 
							                 $total_missing_hour =  $total_missing_hour + $missing_hour;
						             	  }
						             	 else
						             	    $number_of_hour = $missing_hour;
					                 
					                 //calculate $gross_salary by hour if it's a timely salary person 
								    /* if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								        }
								      */
								        
								     if(($numberHour_!=null)&&($numberHour_!=0))
								       {
								          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
								
								        } 
					                   
					             
						            }
					           
					            //$total_gross_salary = $total_gross_salary + $gross_salary;
					            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
					          
					          if($model->payroll_month == 0)
					           {
					           	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = (  ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
		                        	    
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	   // $net_salary = $gross_salary - $deduction;
			                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
					           }
					          else
					          	{ 
					          	  if($this->taxe==1)
			                         {
			                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	    //$net_salary = $gross_salary - $deduction;
 			                       	    
			                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
			                       	 } 
			                      elseif($this->taxe==0)
			                        {
			                        	//$net_salary = $gross_salary;
			                        	$net_salary = $gross_salary_initial - $assurance;
			                          }  
			                          
					          	}
		                          
		                          $total_net_salary = $total_net_salary + $net_salary;
		                          
                          
                          
                               }
                       
                      }
                       
                       
                      if(($as_emp==1) && ($as_teach==1))
		                   {  //alafwa employee e teacher
		                   	   //save payroll la sou employee
		                   	   
		                   	   $all='e';
		                   	   
		                   	   $id_payroll_set=$id_payroll_set_emp;
		                   	   $id_payroll_set2 = $id_payroll_set_teach;
		                   	   $number_of_hour=null;
		                   	   $missing_hour=null;
		                   	   
		                   	 }

                     if($model->payroll_month != 0)
			           {
			           	
                      //DEDUCTION TAXE IRI
                       if($this->taxe==1)
	                     {
	                     		//IRI
	                       	$iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary_initial);
	                       	$total_deduction = $total_deduction + $iri_deduction;
	                       	$total_net_salary = $total_net_salary - $iri_deduction;
	      
	       
	                       }
	                       
			           }
                       
                      
                    
                    
                 $total_net_salary = $total_net_salary + $frais + $plus_value;    
                     
                                      
                        $total_taxe = $total_taxe + $total_deduction;
                        
                        if( ($loan_!=0) )
                          {
                          	  if($model->payroll_month != 0)
							    {
							       
							       if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
                                     {
                                     	      $total_net_salary = $total_net_salary - $loan_;
							          }
							        elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
			                          {
			                          	  //loan nan ap prelve sou montan total payroll moun nan
					                       //check if there is a loan
					                       $modelLoan = new SrcLoanOfMoney;
					                       $dataModelLoan = $modelLoan->find()
					                                               ->joinWith(['loanElements'])
					                                               ->where('person_id='.$this->person_id.' and academic_year='.$acad.' and month='.$model->payroll_month)
					                                               ->all();
					                                  
					                                  
					                                   if($model->payroll_month != 0)
												         {
												           	     
									                        if($dataModelLoan!=null)
									                          { 
									                          	 $id_loan= 0;
									                          	 $loan_devise ='';
									                          	 
									                          	 $loan_=$dataModelLoan;//->getData();
									                          	foreach($loan_ as $loan)
									                          	   { 
											                          	 $id_loan= $loan->id;
											                          	 
											                          	 
											                        if($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
								                                      {
								                                      	  //cheche liy ki koresponn ak mwa pewol la ki poko peye
								                                      	    $id_loan= $loan->id;
								                                      	    $loan_devise = $loan->devise;
								                                      	
								                                      	    $modelLoanElements = new LoanElements;
								                                      	    $loanElements = $modelLoanElements->find()
								                                      	                                     ->where('loan_id='.$id_loan.' and month='.$this->payroll_month)
								                                      	                                     ->all();
								                                      	    
								                                      	    if($loanElements!=null)
								                                      	      {  
								                                      	      	 foreach($loanElements as $loanElem)
								                                      	      	   { 
								                                      	      	       $l_payroll_month= $loanElem->month;
                                                                                                                       
                                                                                                                        if($model->payroll_month == $l_payroll_month)
                                                                                                                           {                                                           
                                                                                                                                    //tcheke deviz la
                                                                                                                               if(($model->devise == $loan_devise)||( ($model->devise != $loan_devise)&&($model->loan_rate!=0)) )
                                                                                                                                { 
															                          	 $l_amount= $loan->amount;
															                          	
															                          	if( ($model->devise != $loan_devise) )
															                          	  {
																	                          	if( ($loan_rate!=0)&&($loan_rate!='') )
																	                          	  {
																	                          	   	  if($model->devise==1)//HTG
																	                          	   	    {
																	                          	   	    	 $l_percent_of_deduct_old= round( ($loanElem->amount*$old_rate),2);
																	                          	   	    	 $l_percent_of_deduct= round( ($loanElem->amount*$loan_rate),2);
					
																	                          	   	    	 
																	                          	   	    }
																	                          	   	  else//$USD et autres
																	                          	   	     { $l_percent_of_deduct= round( ($loanElem->amount/$loan_rate),2);
																                          	   	     
																	                          	   	     $l_percent_of_deduct_old= round( ($loanElem->amount/$old_rate),2);
					              /* 10 - 4 =6 + (4-5) =5
					               10 - 5 =5
					               
					               10 - 6 = 4 + (6-3) =7
					               10 - 3 = 7
									*/								                          	   	     
																	                          	   	     }
																	                          	     
																	                          	   	 
																	                          	   }
																	                          	 else
																	                          	  { $l_percent_of_deduct= $loanElem->amount;
																	                          	        $is_save=FALSE;
																	                          	    }
																	                          	   
															                          	     }
															                          	   elseif(($model->devise == $loan->devise) )
															                          	      {  $l_percent_of_deduct= $loanElem->amount;
															                          	         $loan_rate = null;
															                          	      }
															                          	        
																	                          	
																	                          	 $l_solde= $loan->solde;
										                                      	      	       
										                                      	      	         $deduction_ = $l_percent_of_deduct;
															                          	 
										                                      	      	         $deduction_old = $l_percent_of_deduct_old;
																	                          	 
																	                          	 																	                          	 
																	                          	 $total_deduction = $total_deduction + $deduction_;
																	  	         	
																	  	         	             //$l_solde=$l_solde-$deduction_;
																	  	         	              $l_solde=$l_solde + ($deduction_old - $deduction_);
																	  	         	             
																	  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
																	  	         	             
																	  	         	             $modelLoanElements_new = $modelLoanElements->findOne($loanElem->id);
																	  	         	             
																	  	         	             //if $l_solde >0, ==0, <0
																	  	         	             if($l_solde>0)
																	  	         	               {
																	  	         	               	     
																		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																		  	         	               	  if($dataModelLoan1->save())  
																		  	         	               	   {
																		  	         	               	     $total_net_salary = $total_net_salary - $deduction_;
																		  	         	               	   }
																	  	         	               	   
																	  	         	               	   
																	  	         	                 }
																	  	         	              elseif($l_solde==0)
																	  	         	                {
																	  	         	                         																		  	         	               	                                      $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																		  	         	               	  if($dataModelLoan1->save())  
																		  	         	               	   {
																			  	         	               	  
																		  	         	               	        $total_net_salary = $total_net_salary - $deduction_;
																		  	         	               	   }
																		  	         	               
																	  	         	                  }
																	  	         	               elseif($l_solde<0)
																	  	         	                 {
																	  	         	                  	 																	  	         	               	                                  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																			  	         	               	 if($dataModelLoan1->save())  
																			  	         	               	  {
																			  	         	               	    
																		  	         	               	         $total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
																			  	         	               	  }																	  
																	  	         	                  }
															  	         	                   
								                                      	      	       
								                                      	      	       }	//deviz
									  	         	                   elseif(( ($devise != $loan->devise)&&($loan_rate==0)) || ( ($devise != $loan->devise)&&($loan_rate==null))  )
                                                                                                                       {
									  	         	                          	 /* if($str_name =='')
									  	         	                          	     $str_name =  ;
									  	         	                          	  else
									  	         	                          	    $str_name = $str_name.','. ;
									  	         	                          	    */
									  	         	                          	 $is_save=FALSE;
									  	         	                          	}
                                                                                                                           
                                                                                                                                
                                                                                                                                
                                                                                                                       }
                                                                                                                     else 
                                                                                                                       {
                                                                                                                          break;  
                                                                                                                     }     
                                                                                                                           
								                                      	       }
								                                      	    } 
								                                      
								                                                      /*
									  	         	                          else
									  	         	                          	{ 
									  	         	                          		$loan_rate = null;
									  	         	                          		 //deduction = loanElem->amount
									  	         	                          	       $modelLoanElements = new LoanElements;
											                                      	    $loanElements = $modelLoanElements->find()
											                                      	                                     ->where('loan_id='.$id_loan.' and month='.$this->payroll_month)
											                                      	                                     ->all();
											                                      	    
											                                      	    if($loanElements!=null)
											                                      	      {  
											                                      	      	 foreach($loanElements as $loanElem)
											                                      	      	   { 
																				                          	$l_percent_of_deduct= $loanElem->amount;  
																				                          	
																				                          	 $deduction_ = $l_percent_of_deduct;
																		                          	 
													                                      	      	         $total_deduction = $total_deduction + $deduction_;
																				  	         	
																				  	         	            $total_net_salary = $total_net_salary - $deduction_;
																		  	         	                   
											                                      	      	       
											                                      	      	       
											                                      	      	       
											                                      	      	          
											                                      	      	   }
											                                      	      	
											                                      	      	
									  	         	                          	          }
									  	         	                          	
								                                                } //fen deduction = loanElem->amount
                                                                                                                       * 
                                                                                                                       */
									  	         	                 
									  	         	                 
									  	         	                 } 
									  	         	            
									  	         	             }
				  	         	             
				  	         	             
								                          }
							                          
							                          }//fen metod 2
							                          
			                            }
							    }

                          	}
                                                   
                        					                   	 
					                   	 
					             /*  if($model_new->cash_check=='')
					                  $model->setAttribute('cash_check', Yii::t('app','Cash') );
					               else
					                  $model->setAttribute('cash_check', $model_new->cash_check );
					              */    
					                  $model->setAttribute('plus_value',$plus_value);
					                  $model->setAttribute('loan_rate',$loan_rate);
					                  $model->setAttribute('payroll_date',$model_new->payroll_date);
					                  $model->setAttribute('payment_date',$model_new->payment_date);
						              $model->setAttribute('payroll_month',$model_new->payroll_month);
						           
					           
						           //$model->setAttribute('id_payroll_set',$id_payroll_set);
						           //$model->setAttribute('id_payroll_set2',$id_payroll_set2);
						           ///$model->setAttribute('person_id',$this->person_id);
						           ///$model->setAttribute('number_of_hour',$number_of_hour);
						           $model->setAttribute('missing_hour',$missing_hour);
						           $model->setAttribute('taxe',$total_taxe);
						           $model->setAttribute('gross_salary',$total_gross_salary_initial);
			                       $model->setAttribute('net_salary',$total_net_salary);
						           							 
									$model->setAttribute('date_updated',date('Y-m-d H:i:s'));
								  	$model->setAttribute('updated_by',currentUser());
			
						          
								if($model->save())
								 {
								   $month_=$model->payroll_month;
						           $year_= getYear($model->payment_date);
						          
						          if($is_save)
						            { 
						            	$dbTrans->commit();
						           //$this->redirect(array('index','month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>''));  
						            return $this->redirect(['view','id'=>$model->id,'month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>'']);
						            
						            }
						          else
						             $dbTrans->rollback();
						                                   
								   }
								 else
						             $dbTrans->rollback();
								
			                     
					         
						   }
			// fen youn pa youn ___________________________
			             else
			               {
			         //an gwoup ___________________________      	
			                $this->message_noOneSelected=false;
								   $this->message_wrongMissingTime=false;
								   
								   $temwen=false;
								   $person='';
								   $missingHour='';
								   $month='';
								   
								   $month_='';
								   $year_='';
								   $all='';
								   $devise=0;
								$modelPayroll=new SrcPayroll;   
								     	
								
								  
							$selection=(array)Yii::$app->request->post('selection');//typecasting
						 if($selection!=null)
							{ foreach($selection as $id)
					           {  
					             $dbTrans = Yii::$app->db->beginTransaction(); 
					             
					             $id_payroll = (int)$id;
					             $model = SrcPayroll::findOne($id_payroll);
		             
					              	  $id_payroll_set ='';
					              	  $id_payroll_set2 = null;
					$gross_salary ='';
					$timely_salary = 0;
					$loan = 0;
					
					
					
                   	          
					$assurance_ = 0;
					
					$old_rate = $model->loan_rate;
					
					if($model->idPayrollSet->assurance_value!='')
					  $assurance_ = $model->idPayrollSet->assurance_value;
					
					//$loan = $modelPayroll->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalary($model->idPayrollSet->person_id),$model->idPayrollSet->number_of_hour,$model->missing_hour,$model->net_salary,$model->taxe);
					$loan_ = $modelPayroll->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalaryIndex_value($model->idPayrollSet->devise,$model->idPayrollSet->person_id, $model->payroll_month,getYear($model->payment_date)  ),$model->idPayrollSet->number_of_hour,0,$model->net_salary,$model->getFrais_numb(),$model->plus_value,$model->taxe,$assurance_,$model->payroll_month);
					
					
					$this->person_id =$model->idPayrollSet->person_id; 
					 
					$id_payroll_set = $model->id_payroll_set;
					 
					 $missing_hour1 ='';
					 $number_of_hour = $model->number_of_hour;
					
					$missing_hour1 = $model->missing_hour;
					
			        
			        $devise=$model->idPayrollSet->devise;
			        
			        
			        
			         $missing_hour =0;
			                  $numberHour_=0;
			                  $all='';
			
						    	$as_emp=0;
			                    $as_teach=0;
			                    $id_payroll_set_emp=null;
			                    $id_payroll_set_teach=null;
			                    $total_gross_salary=0;
			                    $total_gross_salary_initial = 0;
			                    $total_net_salary=0;
			                    $total_deduction=0;
			                    $total_taxe =0;
			                    $iri_deduction =0;
			                   
			                   $plus_value =0;
			                   
			                   $loan_rate =0;
			                   
			                   $assurance = 0;
			                   
						  	  
						  	  $model_new->load(Yii::$app->request->post());
						  	  
						  	  
						  	    if(isset($_POST['SrcPayroll']['taxe']))
						           {
			                        $this->taxe = $_POST['SrcPayroll']['taxe'];
			                        
			                        }
			                        
			                  	 if($model_new->loan_rate =='')
		                               $loan_rate =0;
		                          else
		                   	          $loan_rate = $model_new->loan_rate; 
		                   	          
		                   	     // if($model_new->plus_value =='')
		                         //      $plus_value =0;
		                        //  else
		                   	     //     $plus_value = $model_new->plus_value;
		                   	     
		                   	     $modelPayrollPlusvalue= new SrcPayrollPlusvalue();
                 $plus_value = $modelPayrollPlusvalue->getPayrollPlusvalue($this->person_id,$this->payroll_month,$acad);  
		                   	           	
						        // $this->payroll_month=Yii::$app->session['payroll_payroll_month'];
						        
								 $employee_teacher = $modelPers->isEmployeeTeacher($this->person_id, $acad);	
										  
							    if(!$employee_teacher)//si moun nan pa alafwa anplwaye-pwofese 
			                     { 
			                 
			                 $modelPayrollSettings = SrcPayrollSettings::find()
			                                                   ->orderBy(['date_created'=>SORT_DESC])
			                                                   ->where('payroll_settings.old_new=1 AND payroll_settings.id='.$id_payroll_set.' AND payroll_settings.person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2)) ')
			                                                   ->all();
			
			                      
				                  foreach($modelPayrollSettings as $amount)
				                   { 
			                        $gross_salary_initial =0;
			                   	    $gross_salary =0;
			                   	    $deduction =0;
			                   	    $net_salary=0;
			                   	  
			                         
				                     if(($amount!=null))
						               {  
						               	   $gross_salary =$amount->amount;
						               	   
						               	   $gross_salary_initial =$amount->amount;
						               	   
						               	   $missing_hour = $amount->number_of_hour;
						               	   
						               	   $numberHour_ = $amount->number_of_hour;
						               	   
						               	   $assurance = $assurance + $amount->assurance_value;
						               	   
						               	   if($amount->as_ ==0)
						               	     $all= 'e';
						               	   elseif($amount->as_ ==1)
						               	      $all= 't';
						               	   
						               	   if($amount->an_hour==1)
						                     $timely_salary = 1;
						                 }
						           //get number of hour if it's a timely salary person
						            if($timely_salary == 1)
						              {  
						              	$model_ = new SrcPayrollSettings;
						              	
						             	 if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
						             	  {  //return working times in minutes
							             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
							                 
							                 $missing_hour = $missing_hour - $number_of_hour;
						             	  }
						             	 else
						             	    $number_of_hour = $missing_hour;
						                 //calculate $gross_salary by hour if it's a timely salary person 
									     /*if(($number_of_hour!=null)&&($number_of_hour!=0))
									       {
									          $gross_salary = ($gross_salary * $number_of_hour);
									          
									        }
						                  */
						                  
						                  if(($numberHour_!=null)&&($numberHour_!=0))
									       {
									          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
									
									        } 
						                   
						                  
						               }
						           
						           // $total_gross_salary = $total_gross_salary + $gross_salary;
						            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
						          
						          if($model_new->payroll_month==0)
						           { 
						          	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
			                      	    
				                       	    $total_deduction = $total_deduction + $deduction;
				                       	    //$net_salary = $gross_salary - $deduction;
				                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
						          	}	 
						          else
						           {
						          	  if($this->taxe==1)
				                         {
				                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
			                      	    
				                       	    $total_deduction = $total_deduction + $deduction;
				                       	    //$net_salary = $gross_salary - $deduction;
				                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
				                       	 } 
				                      elseif($this->taxe==0)
				                        {
				                        	//$net_salary = $gross_salary;
				                        	$net_salary = $gross_salary_initial - $assurance;
				                        	
				                          } 
						           } 
			                          
			                          $total_net_salary = $total_net_salary + $net_salary;
			                          
			                          }
				                      
				                  
				                  if($model_new->payroll_month!=0)
						           { 
						          	//DEDUCTION TAXE IRI
			                       if($this->taxe==1)
				                     {
				                       	//IRI
				                       	$iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary_initial);
				                       	$total_deduction = $total_deduction + $iri_deduction;
				                       	$total_net_salary = $total_net_salary - $iri_deduction;
				                       } 
						           
						           }
						              
			                        $total_taxe = $total_taxe + $total_deduction;
			                        
			                      if($model_new->payroll_month!=0)
						           { 
			                        //DEDUCTION LOAN
			                         $total_deduction = $total_deduction + $loan;
				                       	$total_net_salary = $total_net_salary - $loan;
						             }
			                         
			                       }
			                    else //moun nan alafwa anplwaye-pwofese
			                      {  
			                        $modelPayrollSettings = SrcPayrollSettings::find()
			                                                   ->orderBy(['date_created'=>SORT_DESC])
			                                                   ->where('payroll_settings.old_new=1 AND payroll_settings.person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2)) ')
			                                                   ->all();
			                      	 
			                      foreach($modelPayrollSettings as $amount)
				                   {   
				                   	  $gross_salary_initial =0;
				                   	  $gross_salary =0;
				                   	  $deduction =0;
				                   	  $net_salary=0;
				                   	  $as = $amount->as_;
				                   	  
				                     //fosel pran yon ps.as=0 epi yon ps.as=1
				                       if(($as_emp==0)&&($as==0))
				                        { $as_emp=1;
				                           $all= 'e';
					                     
					                     if(($amount!=null))
							               {  
							               	   $id_payroll_set_emp = $amount->id;
							               	   $id_payroll_set = $amount->id;
							               	   
							               	   $gross_salary =$amount->amount;
							               	   
							               	   $gross_salary_initial =$amount->amount;
							               	   
							               	   $missing_hour = $amount->number_of_hour;
							               	   
							               	   $numberHour_ = $amount->number_of_hour;
							               	   
							               	   $assurance = $assurance + $amount->assurance_value;
							               	   
							               	   if($amount->an_hour==1)
							                     $timely_salary = 1;
							                 }
						           //get number of hour if it's a timely salary person
						            if($timely_salary == 1)
						              {
						              	  $model_ =  new SrcPayrollSettings;
						              	  
						             	 if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
						             	  {  //return working times in minutes
							             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
							                 
							                 $missing_hour = $missing_hour - $number_of_hour;
						             	  }
						             	 else
						             	    $number_of_hour = $missing_hour;
						                 //calculate $gross_salary by hour if it's a timely salary person 
									     /*if(($number_of_hour!=null)&&($number_of_hour!=0))
									       {
									          $gross_salary = ($gross_salary * $number_of_hour);
									          
									        }
						                  */
						                  
						                  if(($numberHour_!=null)&&($numberHour_!=0))
									       {
									          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
									
									        } 
						                   
						             
							            }
						           
						            //$total_gross_salary = $total_gross_salary + $gross_salary;
						            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
						          
						         if($model_new->payroll_month==0)
						           { 
						          	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = ( ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
			                      	    
				                       	    $total_deduction = $total_deduction + $deduction;
				                       	    //$net_salary = $gross_salary - $deduction;
				                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
						          	}
						         else
						           {
						          	  if($this->taxe==1)
				                         {
				                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
			                      	    
				                       	    $total_deduction = $total_deduction + $deduction;
				                       	    //$net_salary = $gross_salary - $deduction;
				                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
				                       	 } 
				                      elseif($this->taxe==0)
				                        {
				                        	//$net_salary = $gross_salary;
				                        	$net_salary = $gross_salary_initial - $assurance;
				                        	
				                          }  
						           }
			                          
			                          $total_net_salary = $total_net_salary + $net_salary;
			                          
			                          
			                          
			                          }
			                        elseif(($as_teach==0)&&($as==1))
			                            {   $as_teach=1;
			                                $all='t';
				                     
						                     if(($amount!=null))
								               {  
								               	   $id_payroll_set_teach = $amount->id;
								               	   $id_payroll_set = $amount->id;
								               	   $id_payroll_set2 = $amount->id;
								               	   
								               	   $gross_salary =$amount->amount;
								               	   
								               	   $gross_salary_initial =$amount->amount;
								               	   
								               	   $missing_hour = $amount->number_of_hour;
								               	   
								               	   $numberHour_ = $amount->number_of_hour;
								               	   
								               	   $assurance = $assurance + $amount->assurance_value;
								               	   
								               	   if($amount->an_hour==1)
								                     $timely_salary = 1;
								                 }
								           //get number of hour if it's a timely salary person
								            if($timely_salary == 1)
								              {
						                     	  $model_ =  new SrcPayrollSettings;
						              	  
								             	  if($modelPS->load(Yii::$app->request->post()) ) //if(isset( $_POST['PayrollSettings']))
									             	  {  //return working times in minutes
										             	 $number_of_hour = $modelPS->number_of_hour;   //$this->getHours($this->person_id,$acad);
										                 
										                 $missing_hour = $missing_hour - $number_of_hour;
									             	  }
									             	 else
									             	    $number_of_hour = $missing_hour;
								                 //calculate $gross_salary by hour if it's a timely salary person 
											    /* if(($number_of_hour!=null)&&($number_of_hour!=0))
											       {
											          $gross_salary = ($gross_salary * $number_of_hour);
											          
											        }
											       */ 
											     if(($numberHour_!=null)&&($numberHour_!=0))
											       {
											          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
											
											        } 
								                   
								             
									            }
								           
								           // $total_gross_salary = $total_gross_salary + $gross_salary;
								            $total_gross_salary_initial = $total_gross_salary_initial + $gross_salary_initial;
								          
								          if($model_new->payroll_month==0)
								           { 
								          	  if(infoGeneralConfig('bonis_set')==1) // kalkil sou payroll ki fet deja
	             	                            {
	             	                            	$numb_payroll =0;
	             	                            	$person_id = 0;
	             	                            	$modelPayrollSet = new SrcPayrollSettings;
	             	                            	$modelPayroll = new SrcPayroll;
	             	                            	
	             	                            	$person_id = $modelPayrollSet->getPersonIdByIdPayrollSetting($id_payroll_set);
	             	                            	 $empModelPastPayroll = $modelPayroll->searchForBonis($person_id, $acad);
	             	                            	 
	             	                            	 if($empModelPastPayroll!=null)
	             	                            	   {
	             	                            	   	     $empModelPayroll = $empModelPastPayroll->getModels();
	             	                            	   	     foreach($empModelPayroll as $payroll)
	             	                            	   	       {
	             	                            	   	       	  $numb_payroll= $numb_payroll+ $payroll->gross_salary;
	             	                            	   	       	}
	             	                            	   	       
	             	                            	   	}
	             	                            	  
	             	                            	  $gross_salary_initial = (  ($numb_payroll/12) ); 	
	             	                            	   	
	             	                             }
	             	                          elseif(infoGeneralConfig('bonis_set')==0) // kalkil sou montan bru a
	             	                            {
	             	                            	
	             	                            	$gross_salary_initial = ( $gross_salary_initial * (12/12) ); 
	             	                            	
	             	                            } 
								          	   
								          	   								          	   
								          	   
								          	   $deduction= $this->getBonisTaxe($id_payroll_set,$gross_salary_initial); 
					                      	    
						                       	    $total_deduction = $total_deduction + $deduction;
						                       	    //$net_salary = $gross_salary - $deduction;
						                       	    $net_salary = $gross_salary_initial - $deduction;// - $assurance;
								          	}
						          	    else
						          	    {
						          		  if($this->taxe==1)
						                         {
						                       	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary_initial); 
						                       	    $total_deduction = $total_deduction + $deduction;
						                       	    //$net_salary = $gross_salary - $deduction;
						                       	    $net_salary = $gross_salary_initial - $deduction - $assurance;
						                       	 } 
						                      elseif($this->taxe==0)
						                        {
						                        	//$net_salary = $gross_salary;
						                        	$net_salary = $gross_salary_initial - $assurance;
						                          }  
						          	    }
					                          $total_net_salary = $total_net_salary + $net_salary;
					                          
			                          
			                          
			                               }
			                       
			                           }
			                       
			                       
			                     if($model_new->payroll_month!=0)
						           { 
						          	  
			                      //DEDUCTION TAXE IRI
			                       if($this->taxe==1)
				                     {
				                       //IRI
				                       	$iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary_initial);
				                       	$total_deduction = $total_deduction + $iri_deduction;
				                       	$total_net_salary = $total_net_salary - $iri_deduction;
				                       }
				                       
						           }
			                       
			                       $total_taxe = $total_taxe + $total_deduction;
			                       
			                      if($model_new->payroll_month!=0)
						           { 
						          	   
			                         //DEDUCTION LOAN
			                         $total_deduction = $total_deduction + $loan;
				                       	$total_net_salary = $total_net_salary - $loan;
						           }
			                      	
			                      	} //fen alafwa anplwaye-pwofese
			                     
			                      if(($as_emp==1) && ($as_teach==1))
					                   {  //alafwa employee e teacher
					                   	   //save payroll la sou employee
					                   	   
					                   	   $all='e';
					                   	   
					                   	   //$id_payroll_set=$id_payroll_set_emp;
					                   	   //$id_payroll_set2 = $id_payroll_set_teach; 
					                   	   $number_of_hour=null;
					                   	   $missing_hour=null;
					                   	   
					                   	 }
					           
					           
					    if( ($loan_!=0) )
                          {
                          	  if($model_new->payroll_month != 0)
							    {
							       
							       if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
                                     {
                                     	      $total_net_salary = $total_net_salary - $loan_;
							          }
							        elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
			                          {
			                          	  //loan nan ap prelve sou montan total payroll moun nan
					                       //check if there is a loan
					                       $modelLoan = new SrcLoanOfMoney;
					                       $dataModelLoan = $modelLoan->find()
					                                               ->joinWith(['loanElements'])
					                                               ->where('person_id='.$this->person_id.' and academic_year='.$acad.' and month='.$this->payroll_month)
					                                               ->all();
					                                  
					                                  
					                                   if($model_new->payroll_month != 0)
												         {
												           	     
									                        if($dataModelLoan!=null)
									                          { 
									                          	 $id_loan= 0;
									                          	 $loan_devise='';
									                          	 $loan_=$dataModelLoan;//->getData();
									                          	foreach($loan_ as $loan)
									                          	   { 
											                          	 $id_loan= $loan->id;
											                          	 
											                        if($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
								                                      {
								                                      	  //cheche liy ki koresponn ak mwa pewol la ki poko peye
								                                      	    $id_loan= $loan->id;
								                                      	    $loan_devise= $loan->devise;
								                                      	 //tcheke deviz la
								                                      	 if(( ($devise != $loan_devise)&&($loan_rate!=0)) )
								                                      	  {
								                                      	    $modelLoanElements = new LoanElements;
								                                      	    $loanElements = $modelLoanElements->find()
								                                      	                                     ->where('loan_id='.$id_loan.' and month='.$this->payroll_month)
								                                      	                                     ->all();
								                                      	    
								                                      	    if($loanElements!=null)
								                                      	      {  
								                                      	      	 foreach($loanElements as $loanElem)
								                                      	      	   { 
								                                      	      	       $l_payroll_month= $loanElem->month;
															                          	 $l_amount= $loan->amount;
															                          	
															                          	if( ($devise != $loan_devise) )
															                          	  {
																	                          	if(($loan_rate!=0)&&($loan_rate!='') )
																	                          	  {
																	                          	   	  if($devise==1)//HTG
																	                          	   	    {
																	                          	   	    	 $l_percent_of_deduct_old= round( ($loanElem->amount*$old_rate),2);
																	                          	   	    	 $l_percent_of_deduct= round( ($loanElem->amount*$loan_rate),2);
					
																	                          	   	    	 
																	                          	   	    }
																	                          	   	  else//$USD et autres
																	                          	   	     { $l_percent_of_deduct= round( ($loanElem->amount/$loan_rate),2);
																                          	   	     
																	                          	   	     $l_percent_of_deduct_old= round( ($loanElem->amount/$old_rate),2);
					              /* 10 - 4 =6 + (4-5) =5
					               10 - 5 =5
					               
					               10 - 6 = 4 + (6-3) =7
					               10 - 3 = 7
									*/								                          	   	     
																	                          	   	     }
																	                          	     
																	                          	   	 
																	                          	   }
																	                          	 else
																	                          	   {  $l_percent_of_deduct= $loanElem->amount;
																	                          	        $is_save=FALSE;
																	                          	   }
																	                          	   
															                          	     }
															                          	   elseif(($devise == $loan->devise) )
															                          	      {  $l_percent_of_deduct= $loanElem->amount;
															                          	          $loan_rate = null;
															                          	      }
															                          	        
																	                          	
																	                          	 $l_solde= $loan->solde;
										                                      	      	       
										                                      	      	         $deduction_ = $l_percent_of_deduct;
															                          	 
										                                      	      	         $deduction_old = $l_percent_of_deduct_old;
																	                          	 
																	                          	 																	                          	 
																	                          	 $total_deduction = $total_deduction + $deduction_;
																	  	         	
																	  	         	             //$l_solde=$l_solde-$deduction_;
																	  	         	              $l_solde=$l_solde + ($deduction_old - $deduction_);
																	  	         	             
																	  	         	             $dataModelLoan1 = $modelLoan->findOne($id_loan);
																	  	         	             
																	  	         	             $modelLoanElements_new = $modelLoanElements->findOne($loanElem->id);
																	  	         	             
																	  	         	             //if $l_solde >0, ==0, <0
																	  	         	             if($l_solde>0)
																	  	         	               {
																	  	         	               	     
																		  	         	               	  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																		  	         	               	  if($dataModelLoan1->save())  
																		  	         	               	   {
																		  	         	               	     $total_net_salary = $total_net_salary - $deduction_;
																		  	         	               	   }
																	  	         	               	   
																	  	         	               	   
																	  	         	                 }
																	  	         	              elseif($l_solde==0)
																	  	         	                {
																	  	         	                         																		  	         	               	                                      $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																		  	         	               	  if($dataModelLoan1->save())  
																		  	         	               	   {
																			  	         	               	  
																		  	         	               	        $total_net_salary = $total_net_salary - $deduction_;
																		  	         	               	   }
																		  	         	               
																	  	         	                  }
																	  	         	               elseif($l_solde<0)
																	  	         	                 {
																	  	         	                  	 																	  	         	               	                                  $dataModelLoan1->setAttribute('date_updated',date('Y-m-d'));
																		  	         	               	  
																			  	         	               	 if($dataModelLoan1->save())  
																			  	         	               	  {
																			  	         	               	    
																		  	         	               	         $total_net_salary = ($total_net_salary - $deduction_) - $l_solde;
																			  	         	               	  }																	  
																	  	         	                  }
															  	         	                   
								                                      	      	       
								                                      	      	       
								                                      	      	       
								                                      	      	   }
								                                      	      	} 
								                                      
								                                             }	//deviz
									  	         	                       elseif(( ($devise != $loan->devise)&&($loan_rate==0)) )
									  	         	                          {
									  	         	                          	 /* if($str_name =='')
									  	         	                          	     $str_name =  ;
									  	         	                          	  else
									  	         	                          	    $str_name = $str_name.','. ;
									  	         	                          	    */
									  	         	                          	 $is_save=FALSE;
									  	         	                          	}
									  	         	                          else
									  	         	                          	{    $loan_rate=null;
									  	         	                          		//deduction = loanElem->amount
									  	         	                          	       $modelLoanElements = new LoanElements;
											                                      	    $loanElements = $modelLoanElements->find()
											                                      	                                     ->where('loan_id='.$id_loan.' and month='.$this->payroll_month)
											                                      	                                     ->all();
											                                      	    
											                                      	    if($loanElements!=null)
											                                      	      {  
											                                      	      	 foreach($loanElements as $loanElem)
											                                      	      	   { 
																				                          	$l_percent_of_deduct= $loanElem->amount;  
																				                          	
																				                          	 $deduction_ = $l_percent_of_deduct;
																		                          	 
													                                      	      	         $total_deduction = $total_deduction + $deduction_;
																				  	         	
																				  	         	            $total_net_salary = $total_net_salary - $deduction_;
																		  	         	                   
											                                      	      	       
											                                      	      	       
											                                      	      	       
											                                      	      	          
											                                      	      	   }
											                                      	      	
											                                      	      	
									  	         	                          	          }
									  	         	                          	
								                                                } //fen deduction = loanElem->amount
									  	         	                 
									  	         	                 
									  	         	                 } 
									  	         	            
									  	         	             }
				  	         	             
				  	         	             
								                          }
							                          
							                          }//fen metod 2
							                          
			                            }
							    }

                          	}    	 
					                   	 
					            /*   if($model_new->cash_check=='')
					                  $model->setAttribute('cash_check', Yii::t('app','Cash') );
					               else
					                  $model->setAttribute('cash_check', $model_new->cash_check );
					              */    
					                  $model->setAttribute('plus_value',$plus_value);
					                  $model->setAttribute('loan_rate',$loan_rate);
					                  $model->setAttribute('payroll_date',$model_new->payroll_date);
					                  $model->setAttribute('payment_date',$model_new->payment_date);
						              $model->setAttribute('payroll_month',$model_new->payroll_month);
						           
					           
						           //$model->setAttribute('id_payroll_set',$id_payroll_set);
						           //$model->setAttribute('id_payroll_set2',$id_payroll_set2);
						          ///$model->setAttribute('person_id',$this->person_id);
						          ///$model->setAttribute('number_of_hour',$number_of_hour);
						           $model->setAttribute('missing_hour',$missing_hour);
						           $model->setAttribute('taxe',$total_taxe);
						           $model->setAttribute('gross_salary',$total_gross_salary_initial);
			                       $model->setAttribute('net_salary',$total_net_salary);
						           							 
									$model->setAttribute('date_updated',date('Y-m-d H:i:s'));
								  	$model->setAttribute('updated_by',currentUser());
			
						            
									if($model->save())
									 {
									   $month_=$model->payroll_month;
							           $year_= getYear($model->payment_date);
							           
							             unSet($model);
													$model= new SrcPayroll;
																   
													 $total_taxe =0;
																   
													$temwen=true;
				
							           
									  }
									
					              	
					               
					               }
											   
					                         
											   
					                     
										   
							        }
							      else //message vous n'avez croche personne
									{
										//$this->message_noOneSelected=true;
										
										Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','You must select at least a person.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	
										
										}
										
								
								  if($temwen)
								    {
								    	 	
									      return $this->redirect(['index','month_'=>$month_,'year_'=>$year_,'all'=>$all,'di'=>1,'part'=>'pay','from'=>'']);  //$this->redirect(array('view','id'=>$model->id,'part'=>'pay','from'=>''));
								   
									     	
								     }
								 else //message vous n'avez croche personne
							       {
								        //$this->message_noOneSelected=true;
								        
								        Yii::$app->getSession()->setFlash('warning', [
														    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
														    'duration' => 36000,
														    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
														    'message' => Html::encode( Yii::t('app','You must select at least a person.' ) ),
														    'title' => Html::encode(Yii::t('app','Warning') ),
														    'positonY' => 'top',   //   top,//   bottom,//
														    'positonX' => 'center'    //   right,//   center,//  left,//
														]);	

											
								    }
					
					               	 
			               	 }			   
				//fen an gwoup ___________________________ 
						
							
			     	  }
			     	else  //$model_new->payment_date < $model_new->payroll_date
			     	  {  //$this->message_PaymentDate=true;
     	       
			     	         Yii::$app->getSession()->setFlash('warning', [
																	    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
																	    'duration' => 36000,
																	    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
																	    'message' => Html::encode( Yii::t('app','-Payment date- cannot be earlier than -payroll date-.' ) ),
																	    'title' => Html::encode(Yii::t('app','Warning') ),
																	    'positonY' => 'top',   //   top,//   bottom,//
																	    'positonX' => 'center'    //   right,//   center,//  left,//
																	]);	
			     	  }
			     	  
			     }
			  else  //$model_new->payroll_month != getMonth($model_new->payroll_date)
			    {   //$this->message_PayrollDate=true;
		           Yii::$app->getSession()->setFlash('warning', [
																    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
																    'duration' => 36000,
																    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
																    'message' => Html::encode( Yii::t('app','Please make sure -payroll date- is in the appropriate -payroll month-.' ) ),
																    'title' => Html::encode(Yii::t('app','Warning') ),
																    'positonY' => 'top',   //   top,//   bottom,//
																    'positonX' => 'center'    //   right,//   center,//  left,//
																]);	
																
		        }
				 
			
							
								
					  }//fen _POST['update']
             } 
             
             
             
            return $this->render('update', [
                'model' => $model,
                'person_id'=>$this->person_id,
                'group' => $this->group,
                'payroll_month'=>$this->payroll_month,
                'grouppayroll'=>$this->grouppayroll,
                'allDevises'=>$allDevises,
                'id_payroll_set'=>$id_payroll_set,
            ]);
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing Payroll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('billings-payroll-delete'))
         {    
         	$acad=Yii::$app->session['currentId_academic_year'];
         	
         	 $loan_payment_method = infoGeneralConfig('loan_payment_method');
         	 
		    //$model2Delete = new SrcPayroll;
		
		try {   
   			  
   			  
   			  $loan=0;
			  $gross_salary =0;
			  $devise =0;

			  $model2Delete = $this->findModel($id);
   			   
   			   $person = $model2Delete->idPayrollSet->person_id;
   			   $assurance = $model2Delete->getAssurance_numb();
   			   $frais = $model2Delete->getFrais_numb();
   			   $plus_value = $model2Delete->plus_value;
   			   $month = $model2Delete->payroll_month;
			   $number_hour =$model2Delete->idPayrollSet->number_of_hour;
			   $net_salary=$model2Delete->net_salary;
			   $devise =$model2Delete->idPayrollSet->devise;
			   $taxe=$model2Delete->taxe;
			   $year = getYear($model2Delete->payment_date);
			   $gross_salary =$model2Delete->getGrossSalaryIndex_value($devise,$person,$month,$year);
 			   
   			   //$loan = Payroll::model()->getLoanDeduction($model->idPayrollSet->person_id,$model->getGrossSalary($model2Delete->idPayrollSet->person_id),$model2Delete->idPayrollSet->number_of_hour,$model2Delete->missing_hour,$model2Delete->net_salary,$model2Delete->taxe);
		         $loan = $model2Delete->getLoanDeduction($person,$gross_salary,$number_hour,0,$net_salary,$frais,$plus_value,$taxe,$assurance,$month);
   			   if($loan==0)
   			    {
   			    	
   			    	
   			       $model2Delete->delete();
   			       
   			       
   			    }
   			  else
   			    {
   			    	
   			    	
   			    	$modelLoan = new SrcLoanOfMoney;
   			    	$solde_ = 0;
   			    	$remaining_month = 0;
   			    	$paid = 0;
   			    	
   			    	//load appropriate loan
   			    	if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
                      {
                      	  $dbTrans = Yii::$app->db->beginTransaction();
                      	
   			    	        $modelLoan = SrcLoanOfMoney::find()
   			    	                       ->where('person_id='.$person.' and payroll_month='.$month.' and academic_year='.$acad)
   			    	                       ->all();
   			    	         
   			    	          if($modelLoan!=null)
								{
								 foreach($modelLoan as $modelLo)
								  {
				   			    	$solde_ = $modelLo->solde;
				   			    	$remaining_month = $modelLo->remaining_month_number;
				   			    	$paid = $modelLo->paid;
				   			    	
				   			    	if($paid==1)
				   			    	  {
				   			    	  	$paid=0;
				   			    	  	}
				   			    	
				   			    	 $remaining_month = $remaining_month + 1;
				   			    	 
				   			    	 $solde_ = $solde_ + $loan;
				   			    	 
				   			    	 $modelLoan_new = SrcLoanOfMoney::findOne($modelLo->id);
				   			    	 $modelLoan_new->setAttribute('solde',$solde_);
									 $modelLoan_new->setAttribute('remaining_month_number', $remaining_month);
									 $modelLoan_new->setAttribute('paid', $paid);
				   			    	
				   			    	 if($modelLoan_new->save())
				   			    	   { 
				   			    	   	$model2Delete->delete();
				   			    	      $dbTrans->commit(); 
				   			    	   
				   			    	   }
				   			    	  else
				   			    	     $dbTrans->rollback();
								  
								    }
								
								}
							   else
							     {
							     	$model2Delete->delete();
							     	$dbTrans->commit();
							     	
							      }
							      
                      }
                    elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob
                      {
                      	 $dbTrans = Yii::$app->db->beginTransaction(); 
                      	 
                      	    $modelLoan = SrcLoanOfMoney::find()
   			    	                       ->joinWith(['loanElements'])
   			    	                       ->where('person_id='.$person.' and month='.$month.' and academic_year='.$acad)
   			    	                       ->all();
   			    	                
   			    	           if($modelLoan!=null)
								{
					   			  foreach($modelLoan as $modelLo)
								  {
				   			    	  	$solde_ = $modelLo->solde;
					   			    	$remaining_month = $modelLo->remaining_month_number;
					   			    	$paid = $modelLo->paid;
					   			    	
					   			    	if($paid==1)
					   			    	  {
					   			    	  	$paid=0;
					   			    	  	}
					   			    	
					   			    	 $remaining_month = $remaining_month + 1;
					   			    	 
					   			    	 //si devise payroll = devise loan
					   			    	 if($devise==$modelLo->devise)
					   			    	    {  
					   			    	    	$solde_ = $solde_ + $loan;
					   			    	    
					   			    	    }
					   			    	  elseif($devise!=$modelLo->devise)
					   			    	    {
					   			    	    	$modelLoanElements = new LoanElements;
					   			    	   	    $loanElements = $modelLoanElements->find()
		                                      	                                     ->where('loan_id='.$modelLo->id.' and month='.$month)
		                                      	                                     ->all();
		                                      	    
		                                      	    if($loanElements!=null)
		                                      	      {  
		                                      	      	 foreach($loanElements as $loanElem)
		                                      	      	     $solde_ = $solde_ + $loanElem->amount;
		                                      	      }
					   			    	      
					   			    	      }
					   			    	     
					   			    	 
					   			    	 $modelLoan_new = SrcLoanOfMoney::findOne($modelLo->id);
					   			    	 
					   			    	 $modelLoan_new->setAttribute('solde',$solde_);
										 $modelLoan_new->setAttribute('remaining_month_number', $remaining_month);
										 $modelLoan_new->setAttribute('paid', $paid);
					   			    	
					   			    	 if($modelLoan_new->save())
					   			    	   {
					   			    	   	  $modelLoanElements1 = new LoanElements;
					   			    	   	  $loanElements1 = $modelLoanElements1->find()
		                                      	                                     ->where('loan_id='.$modelLoan_new->id.' and month='.$month)
		                                      	                                     ->all();
		                                      	    
		                                      	    if($loanElements1!=null)
		                                      	      {  
		                                      	      	 foreach($loanElements1 as $loanElem1)
		                                      	      	   {
		                                      	      	        $loanElements_1 = $modelLoanElements1->findOne($loanElem1->id);
		                                      	      	           
		                                      	                   $loanElements_1->setAttribute('is_paid', 0);
					   			    	   	  
										   			    	   	  if($loanElements_1->save())
										   			    	        { 
										   			    	        	
										   			    	        	$model2Delete->delete();
										   			    	        	$dbTrans->commit();
										   			    	        	
										   			    	        }
										   			    	      else
										   			    	         $dbTrans->rollback();
								   			    	        
		                                      	      	   }
					   			    	        
		                                      	      }
					   			    	   }
					   			    	 else
					   			    	    $dbTrans->rollback();
								   
								   }
								
								}
							   else
							     {
							     	$model2Delete->delete();
							     	$dbTrans->commit();
							     	
							      }
							      
                      }
   			    	
					
   			    	
   			      }
   			   	
					
					
   			 return $this->redirect(['index']);
			
			 } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}

            
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }


public function actionReceipt()
	{
		
		if(Yii::$app->user->can('billings-payroll-receipt'))
         {
		 if((isset($_GET['id']))&&($_GET['id']!=''))
		   {  
		   	   $model=$this->findModel($_GET['id']);
   			   
   			   $this->person_id = $model->idPayrollSet->person_id;
   			   $this->payroll_month = $model->payroll_month;
			  
			  
			  
			  if(isset($_POST['viewPDF'])) //to create PDF file
			    {
			    	
			    	
			    	//$selection=(array)Yii::$app->request->post('selection');//typecasting
				      // if($selection!=null)
					   // { 
					    	  
					    	  $content = $this->renderPartial('_receipt_ind', ['model' => $model, 'person_id'=>$this->person_id, 'payroll_month'=>$this->payroll_month ]);
					    	
					    	   $heading = pdfHeading();  //$this->renderPartial('_pdf_heading');
						    	
						    	$pdf = new Pdf();
						    	  
						    	
						    	$pdf->filename = 'payroll_receipt'.'_'.$model->idPayrollSet->person->first_name.'_'.$model->idPayrollSet->person->last_name.'.pdf';
						    	
						    	 
						    	$pdf->content = Pdf::MODE_CORE;
						    	$pdf->mode = Pdf::MODE_BLANK;
						    	$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
						    	$pdf->defaultFontSize = 10;
						    	$pdf->defaultFont = 'helvetica';
						    	$pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER; //or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
						    	$pdf->orientation = Pdf::ORIENT_PORTRAIT; //or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
						    	$pdf->destination = Pdf::DEST_DOWNLOAD; //or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F
						         
						        $pdf->content = $content;
						    	$pdf->options = ['title' => 'Payroll Receipt'];
						    	$pdf->methods = [
							    	'SetHeader'=>$heading,//[''Payroll Receipt''],
							    	'SetFooter'=>[''], //['{PAGENO}'],
						    	];
						    	
						    	$pdf->options = [
							    	'title' => 'Payroll Receipt',
							    	'autoScriptToLang' => true,
							    	'ignore_invalid_utf8' => true,
							    	'tabSpaces' => 4
						    	];
						  
						    	
						    	// return the pdf output as per the destination setting
						         return $pdf->render();
					    	   
					    //  }
			    	
			      }
		   }
		   
		   
		   
		   
		   
		   
			  
			return $this->render('receipt',[
				'model'=>$model,
				'person_id'=>$this->person_id,
				'payroll_month'=>$this->payroll_month, 
			]);
	    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
	}
	
	
	
	
	

//************************  anyPayrollDoneAfterForOnes($devise, $month,$pers,$acad)  ******************************/
//return 0:false, 1:true	
	public function anyPayrollDoneAfterForOnes($devise, $month,$date,$pers,$acad)
	{    
                $bool=0;
                $modelPayroll = new SrcPayroll;
               
		 if(($month!='')&&($month!=0) )
		   {
		   	  	 $any_after = $modelPayroll->anyAfterDoneForOnes($devise, $month,$date,$pers,$acad);
  
				  	 if(isset($any_after)&&($any_after!=null))
						  {  //$any_after_ = $any_after->getData();//return a list of  objects
						           
						      foreach($any_after as $p)
						       {			   
								  
								  if($p['id']!=null)
								     {				     	
								     	  $bool=1;
								     }
								}  
						   }
				  	
							 
		     }
		     
		     
		return $bool;
         
	}
	

//return charge deduction over gross_salary
public function getTaxes($id_pay_set, $gross_salary)
  {
       if($gross_salary=='')
         $gross_salary = 0;
          
          
       $deduction=0;
  	 $total_deduction=0;
  	 
  	  $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_pay_set;
															
					  $command__ = Yii::$app->db->createCommand($sql__);
					  $result__ = $command__->queryAll(); 
																       	   
						if($result__!=null) 
						 { foreach($result__ as $r)
						     { 
						     	  $deduction = 0;
						     	 $tx_des='';
						     	   $tx_val='';
                                                           $tx_val_fix=0;
					     	   
						     	 $sql_tx_des = 'SELECT taxe_description, taxe_value,valeur_fixe FROM taxes WHERE id='.$r['id_taxe'];
															
								  $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
								  $result_tx_des = $command_tx_des->queryAll(); 
																			       	   
									foreach($result_tx_des as $tx_desc)
									     {   $tx_des= $tx_desc['taxe_description'];
									         $tx_val= $tx_desc['taxe_value'];
                                                                                 $tx_val_fix = $tx_desc['valeur_fixe'];
									       }
						     	 
						     	 
						     	 if( ($tx_des!='IRI')&&($tx_des!='BONIS') ) //c pa iri, 
						     	      {
						     	      	  if($tx_val_fix==0)
                                                                      $deduction = ( ($gross_salary * $tx_val)/100);
                                                                  elseif($tx_val_fix==1)
                                                                      $deduction = $tx_val;
						     	      	}
	 						     	      	
								  $total_deduction = $total_deduction + $deduction;
  //print_r('<br/><br/><br/>*************************************#*'.$deduction.'******@*'.$total_deduction.'********');                                                                
							  }
								  
						 }
	  	     
  	  return $total_deduction;
  	
  }	
  
  
 
//return charge deduction over gross_salary
public function getBonisTaxe($id_pay_set, $gross_salary)
  {
       $deduction=0;
       
       $acad=Yii::$app->session['currentId_academic_year'];
  	
  	  $sql__ = 'SELECT id, taxe_value FROM taxes WHERE taxe_description like("BONIS") and academic_year='.$acad;
															
					  $command__ = Yii::$app->db->createCommand($sql__);
					  $result__ = $command__->queryAll(); 
																       	   
						if($result__!=null) 
						 { foreach($result__ as $r)
						     { 
						     	  $deduction = 0;
						     	 $tx_val= $r['taxe_value'];
															
						    	  $deduction = ( ($gross_salary * $tx_val)/100);
						     	   break;   	
	 						     
							  }
								  
						 }
	  	     
  	  return $deduction;
  	
  }
  
  	
    /**
     * Finds the Payroll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payroll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcPayroll::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
