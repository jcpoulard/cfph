<?php

namespace app\modules\billings\controllers;

use Yii;
use app\modules\billings\models\BillingsServices;
use app\modules\billings\models\SrcBillingsServices;
use app\modules\billings\models\BillingsServicesItems;
use app\modules\billings\models\SrcFeeServices;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * BillingsservicesController implements the CRUD actions for BillingsServices model.
 */
class BillingsservicesController extends Controller
{
    public $layout = "/inspinia";
    
    public $add_info;
    
    public $recettesItems = 5;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BillingsServices models.
     * @return mixed
     */
    public function actionIndex()
    {
       if(Yii::$app->user->can('billings-billingsservices-index'))
      {
          
          if(isset($_POST['SrcBillingsServices']['recettesItems']))
		    $this->recettesItems = $_POST['SrcBillingsServices']['recettesItems'];
		else
	     {
	     	 if(isset($_GET['ri']) )
	     	   $this->recettesItems = $_GET['ri']; 
	       }
        
        	 
    if($this->recettesItems==0)
      {         
           return $this->redirect(array('/billings/billings/index?ri=0&part=rec&from=b')); 		
				 
         }
       elseif($this->recettesItems==1)
		    {
		       return $this->redirect(array('/billings/billings/index?ri=1&part=rec&from=b'));	
				      
		     }
		   elseif($this->recettesItems==2) 
		    {
		        return $this->redirect(array('/billings/otherincomes/index?ri=2&from=b'));
		
		      }
		    elseif($this->recettesItems==3)
		       {
		          return $this->redirect(array('/billings/enrollmentincome/index?ri=3&part=rec&from=b'));
		
		      }
             elseif($this->recettesItems==4)
		       {
		          return $this->redirect(array('/billings/reservation/index?ri=4&part=rec&from=b'));
		
		      }
		    elseif($this->recettesItems==5)
		       {
		         
		          
		           if(!isset($_GET['ri']))
			       {
						$ri = 5;			       	  
			        }
			     else 
			       $ri = $_GET['ri'];
			       	  
			        
             
                    $searchModel = new SrcBillingsServices();
               
                 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);  
		
		      }
		      
        
          
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'recettesItems'=>$this->recettesItems,
        ]);
        
             
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
        
    }

    /**
     * Displays a single BillingsServices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
     if(Yii::$app->user->can('billings-billingsservices-view'))
      {
      	 return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
             
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
    }

    /**
     * Creates a new BillingsServices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      if(Yii::$app->user->can('billings-billingsservices-create'))
      {
      
        if($this->add_info=='')
		  $this->add_info=0;
			     
         $model = new SrcBillingsServices();
         
         $model->is_delivered = 0;
        
       if($model->load(Yii::$app->request->post()) ) 
		 {  
			  if(isset($_POST['add_info_s']))
			     { 
			     	if(isset($_POST['add_info_st']))
			     	 {  $this->add_info = $_POST['add_info_st'];
			     	    			     	     
				     	if($this->add_info==0)
				     	  { $this->add_info=1;
				     	  }
				     	elseif($this->add_info==1)
				     	  {  $this->add_info=0;
				     	  }
			                
			     	  }
			     		 
			      }
			      
			
			    
	     
          if(isset($_POST['create']))
			{ 
				if(isset($_POST['add_info_st']))
			      $this->add_info = $_POST['add_info_st'];
			     
                                
                              if($model->delivered_date >= $model->request_date)
     	                       {
				if($this->add_info==0)
				  {
				     if( ( ($model->is_delivered==1)&&($model->delivered_date!=null) )||( ($model->is_delivered==0)&&($model->delivered_date==null) ) )
				      { 
						     $modelFeeServices = SrcFeeServices::findOne($model->fee);
						    
                                                if($modelFeeServices!=NULL)
                                                {   
						    $model->setAttribute('price', $modelFeeServices->price );
						     $model->setAttribute('date_created',date('Y-m-d H:i:s') );
							   $model->setAttribute('created_by',Yii::$app->user->id );
							   
						    if ($model->save()) 
		                                        return $this->redirect(['index']);
                                                    
                                                }
				      }
				    else
				       { if( ($model->is_delivered==1)&&($model->delivered_date==null) )
                                           {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please fill delivered date.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
                                          elseif( ($model->is_delivered==0)&&($model->delivered_date!=null) )
                                              {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please confirm the delivery.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
                                          
				      
                                       }

                     
				  }
				elseif($this->add_info==1)
				 {
				 	if( ( ($model->is_delivered==1)&&($model->delivered_date!=null) )||( ($model->is_delivered==0)&&($model->delivered_date==null) ) )
				      { 
						 $dbTrans = Yii::$app->db->beginTransaction();
				 	
				 	    	$first_name=$model->first_name;
						         $last_name=$model->last_name;
							     $phone=$model->phone;
							     $promotion=$model->promotion;
							     $option_program=$model->option_program;
						 	
						 	$modelFeeServices = SrcFeeServices::findOne($model->fee);
					   
                                           if($modelFeeServices!=NULL)
                                                {   
						     $model->setAttribute('price', $modelFeeServices->price );
						     $model->setAttribute('date_created',date('Y-m-d H:i:s') );
							   $model->setAttribute('created_by',Yii::$app->user->id );
						 	
						 	if($model->save())
						 	 {
						 	    $modelItem = new BillingsServicesItems();
						 	  
						 	     $modelItem->setAttribute('billings_services_id', $model->id );
						 	     $modelItem->setAttribute('first_name', $first_name );
						             $modelItem->setAttribute('last_name',$last_name );
							     $modelItem->setAttribute('phone',$phone );
							     $modelItem->setAttribute('promotion',$promotion );
							     $modelItem->setAttribute('option_program',$option_program );
						 	  
							 	  if($modelItem->save() )
									{ 
										$dbTrans->commit();  	
										return $this->redirect(['index',]);
									 }
								   else
									 {    $dbTrans->rollback();
										}
						 	 }
						   else
						 	  $dbTrans->rollback();
                                                   
                                                       }
						 	 else
						 	  $dbTrans->rollback();  
				      }
				    else
				       { 
					       if( ($model->is_delivered==1)&&($model->delivered_date==null) )
                                           {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please fill delivered date.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
                                          elseif( ($model->is_delivered==0)&&($model->delivered_date!=null) )
                                              {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please confirm the delivery.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }			 }
				 	  
				  }
                                  
                                  
                                  
                                 }
                                else  //$model->payment_date < $model->payroll_date
                                  {  //$this->message_PaymentDate=true;

                                         Yii::$app->getSession()->setFlash('warning', [
                                                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                    'duration' => 36000,
                                                                                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                    'message' => Html::encode( Yii::t('app','-Delivered date- cannot be earlier than -request date-.' ) ),
                                                                                    'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                ]);	
                                  }
                   
			}
			
			
        } 
          
          return $this->render('create', [
                'model' => $model,
                'add_info'=>$this->add_info,
            ]);
        
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
             
        
    }

    /**
     * Updates an existing BillingsServices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
     if(Yii::$app->user->can('billings-billingsservices-update'))
      {
        $model = $this->findModel($id);
        
        if($model->student_id != null)
		  $this->add_info=0;
		elseif($model->student_id == null)
		  { $this->add_info=1;
		     
		     $modelItem = BillingsServicesItems::findOne($model->id);
		     
		      $model->first_name = $modelItem->first_name;
			  $model->last_name = $modelItem->last_name;
			  $model->phone = $modelItem->phone;
			  $model->promotion = $modelItem->promotion;
			  $model->option_program = $modelItem->option_program;
		   }
		  
	if($model->load(Yii::$app->request->post()) ) 
		 {  
			      
			
			    
	     
          if(isset($_POST['update']))
			{ 
				if(isset($_POST['add_info_st']))
			      $this->add_info = $_POST['add_info_st'];
			    
                             if($model->delivered_date >= $model->request_date)
     	                       { 	 
				if($this->add_info==0)
				  {
				     if( ( ($model->is_delivered==1)&&($model->delivered_date!=null) )||( ($model->is_delivered==0)&&($model->delivered_date==null) ) )
				      {  
				      	  $modelFeeServices = SrcFeeServices::findOne($model->fee);
				     
                                          if($modelFeeServices!=NULL)
                                                {   
                                                    $model->setAttribute('price', $modelFeeServices->price );
                                          
						     $model->setAttribute('date_updated',date('Y-m-d H:i:s') );
						     $model->setAttribute('updated_by',Yii::$app->user->id );
							   
						    if ($model->save()) 
		                                        return $this->redirect(['index']);
                                                }
				      }
				    else
				       { 
					      if( ($model->is_delivered==1)&&($model->delivered_date==null) )
                                           {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please fill delivered date.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
                                          elseif( ($model->is_delivered==0)&&($model->delivered_date!=null) )
                                              {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please confirm the delivery.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
					}
					       	 
					       	 
				  }
				elseif($this->add_info==1)
				 {
				 	if( ( ($model->is_delivered==1)&&($model->delivered_date!=null) )||( ($model->is_delivered==0)&&($model->delivered_date==null) ) )
				      {
						 	$dbTrans = Yii::$app->db->beginTransaction();
						 	
						 	$first_name=$model->first_name;
						         $last_name=$model->last_name;
							     $phone=$model->phone;
							     $promotion=$model->promotion;
							     $option_program=$model->option_program;
						 	
						 	$modelFeeServices = SrcFeeServices::findOne($model->fee);
						     
						if($modelFeeServices!=NULL)
                                                  {   
						     $model->setAttribute('price', $modelFeeServices->price );
						     $model->setAttribute('date_updated',date('Y-m-d H:i:s') );
							   $model->setAttribute('updated_by',Yii::$app->user->id );
						 	
						 	if($model->save())
						 	 {
						 	    $modelItem = BillingsServicesItems::findOne($model->id);
						 	  
						 	     $modelItem->setAttribute('first_name', $first_name );
						         $modelItem->setAttribute('last_name',$last_name );
							     $modelItem->setAttribute('phone',$phone );
							     $modelItem->setAttribute('promotion',$promotion );
							     $modelItem->setAttribute('option_program',$option_program );
						 	  
							 	  if($modelItem->save() )
									{ 
										$dbTrans->commit();  	
										return $this->redirect(['index',]);
									 }
								   else
									 {    $dbTrans->rollback();
										}
						 	 }
						   else
						 	  $dbTrans->rollback();
						 	  
						  }
						 else
						   $dbTrans->rollback();	 	  
				      }
				    else
				       { 
					       if( ($model->is_delivered==1)&&($model->delivered_date==null) )
                                           {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please fill delivered date.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
                                          elseif( ($model->is_delivered==0)&&($model->delivered_date!=null) )
                                              {
					       	   Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please confirm the delivery.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                             }
					}	  
						 	  
				 	  
				  }
                               else  //$model->payment_date < $model->payroll_date
                                  {  //$this->message_PaymentDate=true;

                                         Yii::$app->getSession()->setFlash('warning', [
                                                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                    'duration' => 36000,
                                                                                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                    'message' => Html::encode( Yii::t('app','-Delivered date- cannot be earlier than -request date-.' ) ),
                                                                                    'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                ]);	
                                  }
                   
			}
               }	
			
        }

        
            return $this->render('update', [
                'model' => $model,
                'add_info'=>$this->add_info,
            ]);
            
            
          
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
         
    }

    /**
     * Deletes an existing BillingsServices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     if(Yii::$app->user->can('billings-billingsservices-delete'))
      {
	     try{   
	        
	           $this->findModel($id)->delete();
	
	           return $this->redirect(['index']);
	        
	          } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
			
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    

    }


   public function actionSetDelivereddate(){
       $model = new SrcBillingsServices(); 
       if(isset($_POST))
        {
             if(isset($_GET['is'])) //update
             {
                $model = SrcBillingsServices::findOne($_POST['pk']); 
                $model->delivered_date = $_POST['value'];
                $model->is_delivered = 1;
                $model->updated_by = Yii::$app->user->id;
                $model->date_updated = date('Y-m-d H:i:s'); 
                
                if($model->save())
                   return $this->redirect(['index?ri=5&part=rec',]); 
           }
      /*   else{//create
           $model->postulant = $_POST['pk']; 
           $model->academic_year = $acad; 
           $model->program = $_POST['name']; 
           $model->grade_value = $_POST['value']; 
           $model->create_by = Yii::$app->user->identity->username;
           $model->date_create = date('Y-m-d H:i:s'); 
           $model->save(); 
           }
           */
       }
       
      
   }

 public function actionNondelivered($id)
   {
        $model = $this->findModel($id); 
        $model->delivered_date = NULL;
         $model->is_delivered = 0;
         $model->updated_by = Yii::$app->user->id;
         $model->date_updated = date('Y-m-d H:i:s');
                
        if($model->save()){ 
           return $this->redirect(['index?ri=5&part=rec']);
                      
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
   
   
    /**
     * Finds the BillingsServices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillingsServices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcBillingsServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
