<?php

namespace app\modules\billings\controllers;

use Yii;
use app\modules\billings\models\ChargePaid;
use app\modules\billings\models\SrcChargePaid;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;


/**
 * ChargepaidController implements the CRUD actions for ChargePaid model.
 */
class ChargepaidController extends Controller
{
  
   public $layout = "/inspinia";
   
   
   public $depensesItems2=2;
   
   public $status_ = 0;
	
	
	public $message_paymentDate=false;
  
  
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ChargePaid models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('billings-chargepaid-index'))
         {
              $acad=Yii::$app->session['currentId_academic_year']; 
		
			if(isset($_POST['SrcChargePaid']['depensesItems']))
			  $this->depensesItems2 = $_POST['SrcChargePaid']['depensesItems'];

		  
			  if($this->depensesItems2 == 2)
		        {         
		              $this->status_ = 2;
		             
		           
		                  $searchModel = new SrcChargePaid();
			              $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		           	
		         }
		       elseif($this->depensesItems2==1)
		         { 
		         	 $this->status_ = 1;
		         	 return $this->redirect(['/billings/payroll/index?di=1&from=b']);
		
		           }
             
	        
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	            'depensesItems'=>$this->depensesItems2,
	        ]);
	       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     

    }

    /**
     * Displays a single ChargePaid model.
     * @param integer $id
     * @return mixed
     */
     
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-chargepaid-view'))
         {
		      return $this->render('view', [
		            'model' => $this->findModel($id),
	        ]);
	        
	      }
	    else
	       {
	           if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

	         }
     

    }

    /**
     * Creates a new ChargePaid model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	   if(Yii::$app->user->can('billings-chargepaid-create'))
        {
           $acad=Yii::$app->session['currentId_academic_year'];
	       $model = new ChargePaid();
	       
	       
	        if(isset($_POST['SrcChargePaid']['depensesItems']))
		  $this->depensesItems2 = $_POST['SrcChargePaid']['depensesItems'];
		   
		
		if($this->depensesItems2==2)
		    {
		        $this->status_ = 2;
		
		      }
		 elseif($this->depensesItems2==1)
          { 
         	 $this->status_ = 1;
         	 return $this->redirect(['/billings/payroll/create?di=1&part=pay&from=stud']);

           }
	
	       if($model->load(Yii::$app->request->post()) ) 
			 {
				// $dbTrans = Yii::app->db->beginTransaction(); 
								            
								            
					 if(isset($_POST['create']))
					  {         	
	        	           $model->academic_year= $acad;
	
	                      $model->created_by= currentUser();
	                      
	                      
	                            if($model->save() )
								  { //$dbTrans->commit();  	
									 return $this->redirect(['index']);
									 
								   }
								else
									 { //   $dbTrans->rollback();
									   }
								               
						}
								            
								            
					 } 
		 
	       
	            return $this->render('create', [
	                'model' => $model,
	            ]);
	        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     

	        
    }

    /**
     * Updates an existing ChargePaid model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('billings-chargepaid-update'))
         {
		        $acad=Yii::$app->session['currentId_academic_year'];
		        
		        $model = $this->findModel($id);
		
		        if($model->load(Yii::$app->request->post()) ) 
					  {
									            // $dbTrans = Yii::app->db->beginTransaction(); 
									            
									            
						 if(isset($_POST['update']))
						  { 
		         							
												      if($model->save() )
												        { //$dbTrans->commit();  	
											                return $this->redirect(['index']);
											           }
													  else
														 { //   $dbTrans->rollback();
														   }
									               
							}
									            
									            
						 } 
						
		            return $this->render('update', [
		                'model' => $model,
		            ]);
		       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     

        
    }

    /**
     * Deletes an existing ChargePaid model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('billings-chargepaid-delete'))
         {       
		        $model2Delete = new SrcChargePaid;
				
				  try {	 
			               $model2Delete = $this->findModel($id);
			               
			               $model2Delete->delete();
			               
			                          
		            } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
		
		
		        return $this->redirect(['index']);
		     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     

        
        
    }

    /**
     * Finds the ChargePaid model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ChargePaid the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcChargePaid::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
