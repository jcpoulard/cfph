<?php

namespace app\modules\billings\controllers;
use yii;
use yii\web\Controller;

/**
 * Default controller for the `billings` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
