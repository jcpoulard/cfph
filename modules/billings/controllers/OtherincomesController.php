<?php

namespace app\modules\billings\controllers;

use Yii;
use app\modules\billings\models\OtherIncomes;
use app\modules\billings\models\SrcOtherIncomes;

use app\modules\fi\models\SrcPersons;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;
/**
 * OtherincomesController implements the CRUD actions for OtherIncomes model.
 */
class OtherincomesController extends Controller
{
    public $layout = "/inspinia";
    
    public $recettesItems=2;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OtherIncomes models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set("memory_limit", "-1");
set_time_limit(0);
       if(Yii::$app->user->can('billings-otherincome-index'))
         {
	        
	  if(isset($_POST['SrcOtherIncomes']['recettesItems']))
		    $this->recettesItems = $_POST['SrcOtherIncomes']['recettesItems'];
	  /* else
	     {
	     	 if(isset($_GET['ri']) )
	     	   $this->recettesItems = $_GET['ri']; 
	       }
	       */
		
	 if($this->recettesItems==0)
      {         
        	
		return $this->redirect(['/billings/billings/index?ri=0&from=oi']);
			                
      }
   /*  elseif($this->recettesItems==1)
      {         
        	
		return $this->redirect(['/billings/billings/index?ri=1&from=oi']);
			                
      }
    */ elseif($this->recettesItems==2)
         {  
		          $searchModel = new SrcOtherIncomes();
	               $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


         	}
          /*  elseif($this->recettesItems==3)
		    {
		       return $this->redirect(['/billings/enrollmentIncome/index?part=rec&from=b']);
		
		      }
            elseif($this->recettesItems==4)
		    {
		        return $this->redirect(['/billings/reservation/index?part=rec&from=b']);
		
		      }
           */
         elseif($this->recettesItems==5)
         {  
		         return $this->redirect(['/billings/billingsservices/index?part=rec&from=b']);


         	}
	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	            'recettesItems'=>$this->recettesItems,
	        ]);
	    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
     
    
    }

    /**
     * Displays a single OtherIncomes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-otherincome-view'))
         {
	        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new OtherIncomes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('billings-otherincome-create'))
         {  
         	$acad=Yii::$app->session['currentId_academic_year'];
         	
			          if(isset($_POST['SrcOtherIncomes']['recettesItems']))
			    $this->recettesItems = $_POST['SrcOtherIncomes']['recettesItems'];
		 /* else
		     {
		     	 if(isset($_GET['ri']) )
		     	   $this->recettesItems = $_GET['ri']; 
		       }
	       */
	     
	
     
      if($this->recettesItems==0)
		    {
		        return $this->redirect(['/billings/billings/create?stat=1&ri=0&from=b']);
		
		      }
		/*  elseif($this->recettesItems==1)
		      {         
		        	
				return $this->redirect(array('/billings/billings/create?stat=0&ri=1&from=b'));
					                
		      }
		    elseif($this->recettesItems==3) 
		      {         
		        	
				return $this->redirect(array('/billings/enrollmentIncome/create?stat=0&ri=1&from=b'));
					                
		      }
		    elseif($this->recettesItems==4)
		      {         
		        	 
				return $this->redirect(array('/billings/reservation/create?part=rec&stat=0&ri=1&from=b'));
					                
		      }
		    */  
		      $model = new SrcOtherIncomes();
		
		       if($model->load(Yii::$app->request->post()) ) 
			  {
							            // $dbTrans = Yii::app->db->beginTransaction(); 
							            
							            
				 if(isset($_POST['create']))
				  { 
         			/* if((isset($_GET['p'])&&($_GET['p']!='')&&($_GET['p_id']!=0))&&(isset($_GET['p_id'])&&($_GET['p_id']!='')&&($_GET['p_id']!=0)))
		               {
		   	                  $student= SrcPersons::findOne($_GET['p_id']);
		   	                  $description = Yii::t('app','Admission fee for ').$student->getFullName();
		   	                  
		   	                  $model->setAttribute('id_income_description',1);
		   	                  $model->setAttribute('description',$description);
		   	                  $model->setAttribute('amount',$_GET['p']);
		               }
	   	              */
	   	    
				//$model->setAttribute('income_date', date('Y-m-d') );
			    $model->setAttribute('academic_year', $acad);
			    $model->setAttribute('created_by', currentUser()  );
			    $model->setAttribute('date_created', date('Y-m-d') );
			    
				if($model->save())
					{//$dbTrans->commit();
					  /*	if((isset($_GET['p'])&&($_GET['p']!='')&&($_GET['p_id']!=0))&&(isset($_GET['p_id'])&&($_GET['p_id']!='')&&($_GET['p_id']!=0)))
	                      {
						       $modelPerson = SrcPersons::findOne($_GET['p_id']);
						       if(($modelPerson!=null)&&($modelPerson->paid==0))
						         {  $modelPerson->setAttribute('paid',1);
						           $modelPerson->save();
	                               }
						       $this->redirect(['/academic/persons/viewDetailAdmission/id/'.$_GET['p_id'].'/al/'.$_GET['al'].'/p/'.$_GET['p'].'/from/ad'));
	                      }
	                   else
	                      {  */
	                        	return $this->redirect(['/billings/otherincomes/index?ri='.$this->recettesItems.'&part=rec&from=stud']);
	                      	//}
					}			
				 else
					{ //   $dbTrans->rollback();
					  }
					     
							               
					}
							            
							            
				 } 
				
		  return $this->render('create', [
			      'model' => $model,
				      ]);		   }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing OtherIncomes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('billings-otherincome-update'))
         {
		         $model = $this->findModel($id);
	           
				 if(isset($_POST['SrcOtherIncomes']['recettesItems']))
				    $this->recettesItems = $_POST['SrcOtherIncomes']['recettesItems'];
			/*  else
			     {
			     	 if(isset($_GET['ri']) )
			     	   $this->recettesItems = $_GET['ri']; 
			       }
			  */
	        if($model->load(Yii::$app->request->post()) ) 
			  {
							            // $dbTrans = Yii::app->db->beginTransaction(); 
							            
							            
				 if(isset($_POST['update']))
				  { 
         						   
					$model->setAttribute('updated_by', currentUser()  );
					 $model->setAttribute('date_updated', date('Y-m-d') );
								
										      if($model->save() )
										        { //$dbTrans->commit();  	
									                return $this->redirect(['index',]);
												 }
											  else
												 { //   $dbTrans->rollback();
												   }
							               
					}
							            
							            
				 } 
				
            return $this->render('update', [
                'model' => $model,
            ]);

	       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Deletes an existing OtherIncomes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       if(Yii::$app->user->can('billings-otherincome-delete'))
         {
	          try {	 
	          	
	          	 $model2Delete = new SrcOtherIncomes;
	          	 
	               $model2Delete = $this->findModel($id);
	               
	               $model2Delete->delete();
	               
	                          
                  return $this->redirect(['index', 'wh'=>'set_id']);
                  
               } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}

             
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the OtherIncomes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OtherIncomes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcOtherIncomes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
