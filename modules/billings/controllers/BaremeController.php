<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\Bareme;
use app\modules\billings\models\SrcBareme;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\ForbiddenHttpException;

/**
 * BaremeController implements the CRUD actions for Bareme model.
 */
class BaremeController extends Controller
{
     public $layout = "/inspinia";
     
     public $noCurrentBarem = false;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bareme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SrcBareme();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'noCurrentBarem' =>$this->noCurrentBarem,
        ]);
    }

    /**
     * Displays a single Bareme model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bareme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $acad = Yii::$app->session['currentId_academic_year'];
        
        $model = new SrcBareme();
        
        //$number_line = infoGeneralConfig('nb_grid_line');
        $number_line = 7; 
           $min_value = array(); 
           $max_value = array(); 
           $percent = array();
           
           $error_report = False;
           
           $last_compteur =0;
           
           $j = 0;
      

       if ($model->load(Yii::$app->request->post()) ) 
		 {
		 	 if(isset($_POST['btnSave']))
               {
                   //get the last compteur
                   //return an integer   
                      $last_compteur = $model->getLastCompteur();
                      
                      $prosper_marc_poulard_eder = $last_compteur+1;
                                 
              for($i=0;$i<$number_line;$i++)
               {
                  if((isset($_POST['min'.$i]) && $_POST['min'.$i]!="") && (isset($_POST['max'.$i]) && $_POST['max'.$i]!="")  && (isset($_POST['percent'.$i]) && $_POST['percent'.$i]!="") )
                    {
                      
                      
                      $min_value[$i] = $_POST['min'.$i]; 
                      $max_value[$i] = $_POST['max'.$i];
                      $percent[$i] = $_POST['percent'.$i]; 
                      
                      
                      $model->setAttribute('min_value', $min_value[$i]);
                      $model->setAttribute('max_value', $max_value[$i]);
                      $model->setAttribute('percentage', $percent[$i]);
                      
                      $model->setAttribute('compteur', $prosper_marc_poulard_eder);
                      
                      $model->setAttribute('date_created', date('Y-m-d H:m:s'));
                      $model->setAttribute('created_by', Yii::app()->user->name);
                      
                      if($model->save()){
                              
                         //update all records of this compteur to OLD => old_new =0
                         //$command = Yii::app()->db->createCommand();
                         //$command->update('bareme', array( 'old_new'=>0, ), 'compteur=:comp', array(':comp'=>$last_compteur));
                                   
                             unset($model); 
                             $model = new SrcBareme; 
                                                            
                          }
                      }
                  
	                  unset($model); 
                             $model = new SrcBareme;
	                 }
	              
	              
	              if($last_compteur >= 1)
	                {                         //update all records of this compteur to OLD => old_new =0
	                         $command = Yii::$app->db->createCommand();
	                         $command->update('bareme', ['old_new' => 0,   ], ['compteur'=>$last_compteur ])->execute();
	                	
	                	}
	                	
	                $this->redirect(array('index?wh=set_ba'));

                	 }
             
            
          } 
        
                    return $this->render('create', [
                'model' => $model,
            ]);
        
    }

    /**
     * Updates an existing Bareme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model=new SrcBareme;
        //$model = $this->findModel($id);

        $min_value = array(); 
           $max_value = array(); 
           $percent = array();
           
           $error_report = False;
       
       
       
            return $this->render('update', [
                'model' => $model,
            ]);
        
    }

    /**
     * Deletes an existing Bareme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bareme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bareme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcBareme::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
