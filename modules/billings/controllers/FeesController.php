<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\Fees;
use app\modules\billings\models\SrcFees;
use app\modules\fi\models\SrcStudentLevel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;

use yii\web\ForbiddenHttpException;

/**
 * FeesController implements the CRUD actions for Fees model.
 */
class FeesController extends Controller
{
     public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fees models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('billings-fees-index'))
         {
        
		        $searchModel = new SrcFees();
		        $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
		        
		        if(isset($_GET['SrcFees']))
					          $dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
					          
					          if (isset($_GET['pageSize'])) 
						         	 {
								        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
								          unset($_GET['pageSize']);
									   }
		 
		        return $this->render('index', [
		            'searchModel' => $searchModel,
		            'dataProvider' => $dataProvider,
		        ]);
		  }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       
		        
    }

    /**
     * Displays a single Fees model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	   if(Yii::$app->user->can('billings-fees-view'))
         {
	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
	        
    }

    /**
     * Creates a new Fees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('billings-fees-create'))
         {
       
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = new Fees();
	        
	         
	          if ($model->load(Yii::$app->request->post()) ) 
		        {
		            // $dbTrans = Yii::app->db->beginTransaction(); 
		            
				       if(isset($_POST['SrcFees']['program']))
		              { $program= $_POST['SrcFees']['program'];
		                $model->program=[$program];
		               }
		               
		             if(isset($_POST['SrcFees']['fee']))
		              { $fee= $_POST['SrcFees']['fee'];
		                $model->fee=[$fee];
		               }
		               
		             if(isset($_POST['SrcFees']['devise']))
		              { $devise= $_POST['SrcFees']['devise'];
		                $model->devise=[$devise];
		               }
		               
		               
		            if(isset($_POST['create']))
		            { 
					    
					    //$model->devise=Yii::$app->session['currencyId']; //retire l lew aktive lis la nan fom lan
					    $model->academic_period=$acad;
					    $model->date_create=date('Y-m-d');
					    $model->create_by=currentUser();
					    
		
					      if($model->save() )
					        { //$dbTrans->commit();  	
				                return $this->redirect(['index', 'wh'=>'set_f']);
							 }
						  else
							 { //   $dbTrans->rollback();
							   }
		               
		            }
		            
		            
		        } 
		        
		        
		          return $this->render('create', [
		                'model' => $model,
		                
		                
		            ]);    
	         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       
	
	}
        
        /**
     * Creates a new Fees model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatelist()
    {
       if(Yii::$app->user->can('billings-fees-create'))
         {
       
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = new Fees();
	        
                if(isset($_POST['create'])){
                    $is_save = FALSE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    if(isset($_POST['program']) && isset($_POST['level'])){
                        $level = $_POST['level'];
                        $program = $_POST['program'];
                        for($i=1; $i<=12; $i++){
                            if( (isset($_POST["fee$i"])&&($_POST["fee$i"]!='')) && (isset($_POST["devise$i"])&&($_POST["devise$i"]!='')) && (isset($_POST["amount$i"])&&($_POST["amount$i"]!='')) && isset($_POST["date_limit_payment$i"])){
                              if( (($_POST["fee$i"]!='')) && (($_POST["devise$i"]!='')) && (($_POST["amount$i"]!='')) && isset($_POST["date_limit_payment$i"]))
                               { 
                                $model = new Fees();
                                $model->program = $program;
                                $model->level = $level;
                                $model->devise = $_POST["devise$i"];
                                $model->academic_period = $acad;
                                $model->fee = $_POST["fee$i"];
                                $model->amount = $_POST["amount$i"];
                                $model->date_limit_payment = $_POST["date_limit_payment$i"];
                                if(isset($_POST["description$i"])){
                                    $desciption = $_POST["description$i"];
                                }else{
                                    $desciption = null;
                                }
                                $model->description = $desciption; 
                                $model->date_create = date('Y-m-d');
                                $model->create_by=currentUser();
                                
                                if($model->save())
                                      $is_save = TRUE;
                                
                              }
                              else {
                                  
                              }
                            }
                            }
                            if(!$is_save){
                                $dbTrans->rollback();
                                
                            }
                            else{
                                $dbTrans->commit();
                                return $this->redirect(['index','wh'=>'set_f']);
                            }
                        }
                    
                    
                }
                 return $this->render('createlist', [
		                'model' => $model,
		                
		                
		            ]);    
	         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' =>120000,
                        'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                        'title' => Html::encode(Yii::t('app','Unthorized access') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       
	
	}


    /**
     * Updates an existing Fees model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
	   if(Yii::$app->user->can('billings-fees-update'))
         {
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = $this->findModel($id);
	        
	        $model->program=[$model->program];
	        $model->fee=[$model->fee];
	        
	        if($model->level!=null)
	           $model->level=[$model->level];
	
	          if ($model->load(Yii::$app->request->post()) ) 
		        {
		            // $dbTrans = Yii::app->db->beginTransaction(); 
		            
				       if(isset($_POST['SrcFees']['program']))
		              { $program= $_POST['SrcFees']['program'];
		                $model->program=[$program];
		               }
		               
		               if(isset($_POST['SrcFees']['fee']))
		              { $fee= $_POST['SrcFees']['fee'];
		                $model->fee=[$fee];
		               }
		               
		             if(isset($_POST['SrcFees']['devise']))
		              { $devise= $_POST['SrcFees']['devise'];
		                $model->devise=[$devise];
		               }
		               
		             if(isset($_POST['SrcFees']['level']))
		              { $level= $_POST['SrcFees']['level'];
		                $model->level=[$level];
		               }

 
		            if(isset($_POST['update']))
		            { 
					    $model_new = $this->findModel($id);
					    
					    $model_new->program=  $program;
					    $model_new->fee=  $fee;
					    $model_new->devise=  $devise;
					    $model_new->level= $level;
					    $model_new->amount=$model->amount;
					    $model_new->date_limit_payment= $model->date_limit_payment;
					    $model_new->description=$model->description;
					    $model_new->setAttribute('date_update',date('Y-m-d') );
					    $model_new->setAttribute('update_by',currentUser() );
		
					      if($model_new->save() )
					        { //$dbTrans->commit();  	
				                return $this->redirect(['index', 'wh'=>'set_f']);
							 }
						  else
							 { //   $dbTrans->rollback();
							   }
		               
		            }
		            
		            
		        } 

	            return $this->render('update', [
	                'model' => $model,
	            ]);
	        
	        
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
	        
    }

    /**
     * Deletes an existing Fees model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('billings-fees-delete'))
         {
            try{ $this->findModel($id)->delete();

                 return $this->redirect(['index']);
               } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   
            
    }
    
    
 public function actionFeeinprogram($from,$stud,$acad)
    {
        if($from=='schola')
        { Yii::$app->session['schola_student']= $stud;
           Yii::$app->session['schola_fee']= '';
         
        }
           
           $program= getProgramByStudentId($stud);
           
           $countFees = SrcFees::find()
                       ->joinWith(['fee0','academicPeriod','program0'])
                       ->Where(['not like', 'fee_label', 'Pending balance'])
                       ->andWhere(['program.id'=>$program])
                       ->andWhere(['academicperiods.id'=>$acad])
                       ->count();
           
          
                          
        $allFees = SrcFees::find()
                       ->joinWith(['fee0','academicPeriod','program0'])
                       ->Where(['not like', 'fee_label', 'Pending balance'])
                       ->andWhere(['program.id'=>$program])
                       ->andWhere(['academicperiods.id'=>$acad])
                       ->all();
                       
                              
         if($countFees > 0)
          {
         	   foreach($allFees as $fee)
         	    {
         	   	      echo "<option ></option>"; 
         	   	      
         	   	      echo "<option value='".$fee->id."'>".$fee->getFeeName()."</option>";
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
   
            
    }

    /**
     * Finds the Fees model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fees the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcFees::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
