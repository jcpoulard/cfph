<?php

namespace app\modules\billings\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\billings\models\LoanOfMoney;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\LoanElements;
use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;

use app\modules\rbac\models\User;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * LoanofmoneyController implements the CRUD actions for LoanOfMoney model.
 */
class LoanofmoneyController extends Controller
{
    public $layout = "/inspinia";
    
    public $person_id;
	public $payroll_month;
	
	
	public $depensesItems=0;
	public $month_ =0;
	
	public $message_loandate=false;
	public $messageLoanNotPossible =false;
	public $messageLoanExceed =false;
	public $messageLoanNotPaid =false;
	public $message_UpdateAlreadyPaid=false;
	public $messageAmountNotAllowed=false;
	public $messageRemainPayroll = false;
	public $messageRefund = false;
   
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LoanOfMoney models.
     * @return mixed
     */
    public function actionIndex()
    {
	   if(Yii::$app->user->can('billings-loanofmoney-index'))
         {
	        $searchModel = new SrcLoanOfMoney();
	        $acad=Yii::$app->session['currentId_academic_year'];
	       
	          if( (Yii::$app->session['profil_as']==0)&&(!isset($_GET['from']))  )
	             $dataProvider = $searchModel->search($acad,Yii::$app->request->queryParams);
	          elseif( (Yii::$app->session['profil_as']==0)&&(isset($_GET['from'])&&($_GET['from']=='teach') )  )
                    {
                                $person_id = 0;
          	               $modelUser = User::findOne(Yii::$app->user->identity->id); 

				     if($modelUser->person_id!=NULL)
				         $person_id = $modelUser->person_id;	
				         
				       $dataProvider = $searchModel->searchForOne($acad,$person_id);  
                      }
                   elseif(Yii::$app->session['profil_as']==1)
	            {
	                $person_id = 0;
          	         $modelUser = User::findOne(Yii::$app->user->identity->id); 

				     if($modelUser->person_id!=NULL)
				         $person_id = $modelUser->person_id;	
				         
				       $dataProvider = $searchModel->searchForOne($acad,$person_id);  
				         
	             }
	        			
	

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Displays a single LoanOfMoney model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {  
    	if(Yii::$app->user->can('billings-loanofmoney-view'))
         {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Creates a new LoanOfMoney model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('billings-loanofmoney-create'))
         {
         	 $acad = Yii::$app->session['currentId_academic_year'];
         	 
         	 $loan_payment_method = infoGeneralConfig('loan_payment_method');
         
         $model = new SrcLoanOfMoney();
         
         $this->messageLoanNotPaid =false;

        if($model->load(Yii::$app->request->post()) ) 
          {
                 $id_payroll_set ='';
					$id_payroll_set2=null;
					$amount_to_be_paid=0;
					$an_hour = 0;
					$timely_salary = 0;
					$sum_loan=0;
					
			   if($model->person_id!='')
			       $this->person_id = $model->person_id;
			       
	      if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
             {
		   
			     if($model->payroll_month!='')
			       $this->payroll_month = $model->payroll_month;

				
			       if($this->person_id!='')
				      {   
				    	
				    	//tcheke si moun sa gen avans ki poko fin paye
						 $loanPaid = $this->allLoanPaid($this->person_id);
						 if(!$loanPaid)
						   {  
						   	   
						   	   $this->messageLoanNotPaid =true;
						   	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name}, You are not available to get a loan. Your solde must be zero.', ['name'=>$model->person->getFullName() ] ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
								$this->messageLoanNotPossible=false;
					        }
	
				   				    	
				    	//check if payroll for this month already done
				    	$payrollDone = $this->isPayrollDoneForOnes($this->payroll_month,$this->person_id,$acad);  
									
						  if($payrollDone)
						   {
						   	  $this->messageLoanNotPossible =true;
						   	  Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You cannot get a loan at this month. Payroll or next payroll is already done.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

						   	}
						   	
				       }

                  }

	if(isset($_POST['create']))
	  {  
			$number_of_hour = null;
			$gross_salary = 0;
			$total_payroll =0;
			$remain_payroll =0;
			$number_past_payroll =0;
						
			   
				
		
		
		 if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob la otomatik
             {
			    $model->setAttribute('person_id',$this->person_id);
			    
				//check the total payroll of this year
				$total_payroll = infoGeneralConfig('total_payroll');
				
			
			if(($model->number_of_month_repayment!='')&&($model->amount!=''))
			  {
				//number of past payroll
				$sql2 = 'SELECT DISTINCT payment_date, count(payment_date) as number_past_payroll FROM payroll p INNER JOIN payroll_settings ps ON(ps.id= p.id_payroll_set) WHERE ps.old_new=1 AND ps.person_id='.$this->person_id.' AND ps.academic_year='.$acad;
				
															
					  $command2 = Yii::$app->db->createCommand($sql2);
					  $result2 = $command2->queryAll(); 
																       	   
						if($result2!=null) 
						 { foreach($result2 as $r)
						     { $number_past_payroll =$r['number_past_payroll'];
						          break;
						     }
						  }
				
				$remain_payroll = $total_payroll - $number_past_payroll;
				
								     
				//check if amount value > max_amount_loan
				$max_amount_loan = infoGeneralConfig('max_amount_loan');
	         if($model->amount <= $max_amount_loan)
			  {
				
                     $pay_set = SrcPayrollSettings::find()
                                       ->orderBy(['date_created'=>SORT_DESC])
                                       ->where('old_new=1 AND person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2))' )
                                       ->all();
                    
                    $as_emp=0;
                    $as_teach=0;
                    $id_payroll_set_emp=null;
                    $id_payroll_set_teach=null;
                    $total_gross_salary=0;
                    $total_net_salary=0;
                    $total_deduction=0;
                    $iri_deduction =0;
                    
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	  $net_salary=0;
                   	  $as = $amount->as_;
                   	  
                     //fosel pran yon ps.as=0 epi yon ps.as=1
                       if(($as_emp==0)&&($as==0))
                        { $as_emp=1;
                           $all= 'e';
	                     
	                     if(($amount!=null))
			               {  
			               	   $id_payroll_set_emp = $amount->id;
			               	   $id_payroll_set = $amount->id;
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $missing_hour = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	 $number_of_hour = $amount->number_of_hour;   //$this->getHours($this->person_id,$acad);
			                 
			                //calculate $gross_salary by hour if it's a timely salary person 
						     if(($number_of_hour!=null)&&($number_of_hour!=0))
						       {
						          $gross_salary = ($gross_salary * $number_of_hour);
						          
						          
						
						        }
			                   
			             
				            }
			           
			            $total_gross_salary = $total_gross_salary + $gross_salary;
			          
			          	  	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary); 
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	    $net_salary = $gross_salary - $deduction;
	                       	  
                          
                          $total_net_salary = $total_net_salary + $net_salary;
                          
                          
                          
                          }
                        elseif(($as_teach==0)&&($as==1))
                            {   $as_teach=1;
                                $all='t';
	                     
			                     if(($amount!=null))
					               {  
					               	   $id_payroll_set_teach = $amount->id;
					               	   $id_payroll_set = $amount->id;
					               	   
					               	   $gross_salary =$amount->amount;
					               	   
					               	   $missing_hour = $amount->number_of_hour;
					               	   
					               	   if($amount->an_hour==1)
					                     $timely_salary = 1;
					                 }
					           //get number of hour if it's a timely salary person
					            if($timely_salary == 1)
					              {
					             	  $number_of_hour = $amount->number_of_hour;   //$this->getHours($this->person_id,$acad);
					                 
					                  //calculate $gross_salary by hour if it's a timely salary person 
								     if(($number_of_hour!=null)&&($number_of_hour!=0))
								       {
								          $gross_salary = ($gross_salary * $number_of_hour);
								          
								          
								
								        }
					                   
					             
						            }
					           
					            $total_gross_salary = $total_gross_salary + $gross_salary;
					          
					          	       $deduction= $this->getTaxes($id_payroll_set,$gross_salary); 
			                       	    $total_deduction = $total_deduction + $deduction;
			                       	    $net_salary = $gross_salary - $deduction;
			                       
		                          $total_net_salary = $total_net_salary + $net_salary;
		                          
                          
                          
                               }
                       
                      }
                       
                        if(($id_payroll_set_teach!=null)&&($id_payroll_set_emp!=null))
			                       {
			                       	   $id_payroll_set = $id_payroll_set_emp;
			                       	   $id_payroll_set2 = $id_payroll_set_teach;
			                       	}

                      
                      //DEDUCTION TAXE IRI
                          $iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary);
	                       	$total_deduction = $total_deduction + $iri_deduction;
	                       	$total_net_salary = $total_net_salary - $iri_deduction;
	                       
                       $amount_to_be_paid = $total_net_salary;

				     
				     //$amount_to_be_paid = $this->salaire_net($model->person_id);
				    if(($model->loan_date=='0000-00-00') ||($model->loan_date==''))
				      $model->setAttribute('loan_date',date('Y-m-d'));
				      
				   $amount=$model->amount;
				   //check if already has a loan for this month
				     //return the total amount for this month
				      $sum_loan = $this->personHasLoan($model->person_id,$model->payroll_month);
				   
								   	 
				   	  if($amount_to_be_paid < ($amount + $sum_loan))
				  	     $this->messageLoanExceed =true;
				   	 
				      
				      
				   	  if($remain_payroll >= ($model->number_of_month_repayment))
				   	   {//la remise/payroll
				   	       
				   	       $refund = $amount/$model->number_of_month_repayment;
				   	       
				   	       if($refund <= $total_net_salary)
				   	         {
						   	  	 
						   	  	  $percent = ($refund/$total_net_salary)*100;
						   	  	  
						   	  	  if(is_decimal($percent) )
						   	  	     {
						   	  	     	  $percent = round(($refund/$total_net_salary)*100 , 0) + 1; 
						   	  	       }
						   	  	  
						   	  	   
						   	  	   $model->setAttribute('deduction_percentage',$percent);
						   	  	   $model->setAttribute('remaining_month_number',$model->number_of_month_repayment);
								   $model->setAttribute('academic_year',$acad);
								   $model->setAttribute('solde',$amount);
								   $model->setAttribute('date_created',date('Y-m-d'));
							  	   $model->setAttribute('created_by',currentUser());
			           	   
			           	   if($model->save() )
			                  {  
			                  	   unset(Yii::$app->session['PersonLoan']);
								  	 
								  	return $this->redirect(['view','id'=>$model->id,'part'=>'']);
			                  
			                  }
			                
			              }
				   	    else
				   	       {   //$this->messageRefund = true;  
				   	               Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Refund > Net salary.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   	         }  
					   
				   	   }
				   	 else
				   	   { //$this->messageRemainPayroll = true;
				   	        
				   	         Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Number of payroll remain < Repayment deadline(numb. payroll).') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   	    }
					   
			      }
			    else
			      {
			      	//$this->messageAmountNotAllowed = true;	
			      	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Amount not allowed.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
			      	
			      	}
					   
					   
			    }
			    
         }
      elseif($loan_payment_method==2)//chwazi mwa ou vle peye epi ki kantite kob pou chak mwa yo
        {
             	 $is_save = TRUE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    
                     if(($model->loan_date=='0000-00-00') ||($model->loan_date==''))
				      $model->setAttribute('loan_date',date('Y-m-d'));
				      
                    
                    if(($model->devise!='') &&($model->amount!=0) &&($model->devise!='') )
                      {
	                      	  $remaining_month_number=0;
	                      	  $payroll_month ='';
	                      	  $total_refund = 0;
	                      	  $num_month = sizeof(getSelectedLongMonthValueForOne($model->person_id));
	                          
	                         for($i=1;$i<=$num_month;$i++)
	                           {
	                            if(($_POST["month$i"]!='')&& ($_POST["refund$i"]!='') )
	                             {
	                             	  if($payroll_month=='')
	                             	    $payroll_month = $_POST["month$i"];
	                             	  
	                             	  $total_refund = round( (   round( ($total_refund),2) + round( ($_POST["refund$i"]),2)  ),2);
	                             	  
	                             	  $remaining_month_number++;
	                             }
	                           }
	                   
                                   if($total_refund == $model->amount)
	                        { 
	                                   $model->setAttribute('payroll_month',$payroll_month);
	                                   $model->setAttribute('deduction_percentage',0);
							   	  	   $model->setAttribute('number_of_month_repayment',$remaining_month_number);
							   	  	   $model->setAttribute('remaining_month_number',$remaining_month_number);
									   $model->setAttribute('academic_year',$acad);
									   $model->setAttribute('solde',$model->amount);
									   $model->setAttribute('date_created',date('Y-m-d'));
								  	   $model->setAttribute('created_by',currentUser());
				           	   
				           	   if($model->save() )
				           	     {                         
			                         for($i=1;$i<=$num_month;$i++)
			                           {
			                           	
			                            if(($_POST["month$i"]!='')&& ($_POST["refund$i"]!='') )
			                             {
		   	                             	
			                                $model_el = new LoanElements();
			                                
			                                $model_el->loan_id = $model->id;
			                                $model_el->month = $_POST["month$i"];
			                                $model_el->amount = $_POST["refund$i"];
			                                //$model_el->is_paid = 0;
			                                
			                                if($model_el->save())
			                                     unSet($model_el);
			                                else
			                                  $is_save = FALSE; 
			                             }
			                          }
			                            
				           	    }
				           	     
	                            if(!$is_save){
	                                $dbTrans->rollback();
	                                
	                            }
	                            else{
	                                $dbTrans->commit();
	                                return $this->redirect(['view','id'=>$model->id,'part'=>'']);
	                            }
	                       
                           }
                         else
		                   {  //$this->message_default_taxe_u = true;
		                        Yii::$app->getSession()->setFlash('warning', [
																    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
																    'duration' => 36000,
																    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
																    'message' => Html::encode( Yii::t('app','-Sum of refund amount does not equal to Initial amount.') ),
																    'title' => Html::encode(Yii::t('app','Warning') ),
																    'positonY' => 'top',   //   top,//   bottom,//
																    'positonX' => 'center'    //   right,//   center,//  left,//
																]);	
		                    }
                      
                        }
                      else
	                   {  //$this->message_default_taxe_u = true;
	                        Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','-Initial Amount- and -Devise- cannot be null.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);	
	                    }

         
          }

			    
			    
					   
	 }
			   

  }
        
	            return $this->render('create', [
	                'model' => $model,
	                'messageLoanNotPaid'=>$this->messageLoanNotPaid,
	                'messageLoanNotPossible'=>$this->messageLoanNotPossible,
	            ]);
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
           
    }

    /**
     * Updates an existing LoanOfMoney model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
  public function actionUpdate($id)
    {
       if(Yii::$app->user->can('billings-loanofmoney-update'))
         {
         	$loan_payment_method = infoGeneralConfig('loan_payment_method');
         	
         	
        if($loan_payment_method==1)//chwazi mwa ou vle peye, kantite kob otomatik
         {	
         	
         	 $model = $this->findModel($id);
        
        $acad=Yii::$app->session['currentId_academic_year']; 
        $this->messageLoanNotPossible =false;
        
        $this->person_id = $model->person_id;
        

       if ($model->load(Yii::$app->request->post()) ) 
         {
            
            if(($model->paid!=1))   //rasire ke se pou pwofese li tcheke
              {
				  
				    $id_payroll_set ='';
				    $id_payroll_set2 =null;
					$amount_to_be_paid=0;
					$an_hour = 0;
					$sum_loan=0;
			
			 			
			  
			  
				  if($this->person_id!='')
				    {
				    	
				    	//check if payroll for this month already done
				    	$payrollDone = $this->isPayrollDoneForOnes($model->payroll_month,$this->person_id,$acad);
									
						  if($payrollDone)
						   {
						   	    $this->messageLoanNotPossible =true;
						   	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You cannot get a loan at this month. Payroll or next payroll is already done.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						   	}
					   	
			         }

				  
					  if(isset($_POST['update']))
					   { 
					      $number_of_hour = null;
							$gross_salary = 0;
							$total_payroll =0;
							$remain_payroll =0;
							$number_past_payroll =0;
							$timely_salary =0;
							
							//check the total payroll of this year
							$total_payroll = infoGeneralConfig('total_payroll');
							
							//number of past payroll
							$sql2 = 'SELECT DISTINCT payment_date, count(payment_date) as number_past_payroll FROM payroll p INNER JOIN payroll_settings ps ON(ps.id= p.id_payroll_set) WHERE ps.old_new=1 AND ps.person_id='.$this->person_id.' AND ps.academic_year='.$acad;
							
																		
								  $command2 = Yii::$app->db->createCommand($sql2);
								  $result2 = $command2->queryAll(); 
																			       	   
									if($result2!=null) 
									 { foreach($result2 as $r)
									     { $number_past_payroll =$r['number_past_payroll'];
									          break;
									     }
									  }
							
							$remain_payroll = $total_payroll - $number_past_payroll;
							
											     
							//check if amount value > max_amount_loan
							$max_amount_loan = infoGeneralConfig('max_amount_loan');
				         if($model->amount <= $max_amount_loan)
						  {   
			                      $pay_set = SrcPayrollSettings::find()
                                       ->orderBy(['date_created'=>SORT_DESC])
                                       ->where('old_new=1 AND person_id='.$this->person_id.' AND payroll_settings.academic_year='.$acad.' AND payroll_settings.person_id IN( SELECT id FROM persons p WHERE p.id='.$this->person_id.' AND p.active IN(1, 2))' )
                                       ->all();
			                    
			                    $as_emp=0;
			                    $as_teach=0;
			                    $id_payroll_set_emp=null;
			                    $id_payroll_set_teach=null;
			                    $total_gross_salary=0;
			                    $total_net_salary=0;
			                    $total_deduction=0;
			                    $iri_deduction =0;
			                    
			                 foreach($pay_set as $amount)
			                   {   
			                   	  $gross_salary =0;
			                   	  $deduction =0;
			                   	  $net_salary=0;
			                   	  $as = $amount->as_;
			                   	  
			                     //fosel pran yon ps.as=0 epi yon ps.as=1
			                       if(($as_emp==0)&&($as==0))
			                        { $as_emp=1;
			                           $all= 'e';
				                     
				                     if(($amount!=null))
						               {  
						               	   $id_payroll_set_emp = $amount->id;
						               	   $id_payroll_set = $amount->id;
						               	   
						               	   $gross_salary =$amount->amount;
						               	   
						               	   $missing_hour = $amount->number_of_hour;
						               	   
						               	   if($amount->an_hour==1)
						                     $timely_salary = 1;
						                 }
						           //get number of hour if it's a timely salary person
						            if($timely_salary == 1)
						              {
						             	 $number_of_hour = $amount->number_of_hour;   //$this->getHours($this->person_id,$acad);
						                 
						                //calculate $gross_salary by hour if it's a timely salary person 
									     if(($number_of_hour!=null)&&($number_of_hour!=0))
									       {
									          $gross_salary = ($gross_salary * $number_of_hour);
									          
									          
									
									        }
						                   
						             
							            }
						           
						            $total_gross_salary = $total_gross_salary + $gross_salary;
						          
						          	  	    $deduction= $this->getTaxes($id_payroll_set,$gross_salary); 
				                       	    $total_deduction = $total_deduction + $deduction;
				                       	    $net_salary = $gross_salary - $deduction;
				                       	  
			                          
			                          $total_net_salary = $total_net_salary + $net_salary;
			                          
			                          
			                          
			                          }
			                        elseif(($as_teach==0)&&($as==1))
			                            {   $as_teach=1;
			                                $all='t';
				                     
						                     if(($amount!=null))
								               {  
								               	   $id_payroll_set_teach = $amount->id;
								               	   $id_payroll_set = $amount->id;
								               	   
								               	   
								               	   $gross_salary =$amount->amount;
								               	   
								               	   $missing_hour = $amount->number_of_hour;
								               	   
								               	   if($amount->an_hour==1)
								                     $timely_salary = 1;
								                 }
								           //get number of hour if it's a timely salary person
								            if($timely_salary == 1)
								              {
								             	  $number_of_hour = $amount->number_of_hour;   //$this->getHours($this->person_id,$acad);
								                 
								                  //calculate $gross_salary by hour if it's a timely salary person 
											     if(($number_of_hour!=null)&&($number_of_hour!=0))
											       {
											          $gross_salary = ($gross_salary * $number_of_hour);
											          
											          
											
											        }
								                   
								             
									            }
								           
								            $total_gross_salary = $total_gross_salary + $gross_salary;
								          
								          	       $deduction= $this->getTaxes($id_payroll_set,$gross_salary); 
						                       	    $total_deduction = $total_deduction + $deduction;
						                       	    $net_salary = $gross_salary - $deduction;
						                       
					                          $total_net_salary = $total_net_salary + $net_salary;
					                          
			                          
			                          
			                               }
			                       
			                      }
			                       
			                       if(($id_payroll_set_teach!=null)&&($id_payroll_set_emp!=null))
			                       {
			                       	   $id_payroll_set = $id_payroll_set_emp;
			                       	   $id_payroll_set2 = $id_payroll_set_teach;
			                       	}
			                      
			                      //DEDUCTION TAXE IRI
			                          $iri_deduction = getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary);
				                       	$total_deduction = $total_deduction + $iri_deduction;
				                       	$total_net_salary = $total_net_salary - $iri_deduction;
				                       
			                       $amount_to_be_paid = $total_net_salary;
			
							     
							     //$amount_to_be_paid = $this->salaire_net($model->person_id);
							    if(($model->loan_date=='0000-00-00') ||($model->loan_date==''))
							      $model->setAttribute('loan_date',date('Y-m-d'));
							      
							   $amount=$model->amount;
							   //check if already has a loan for this month
							     //return the total amount for this month
							      $sum_loan = $this->personHasLoan($model->person_id,$model->payroll_month);
							   
							   
							   	 
							   	  if($amount_to_be_paid < ($amount + $sum_loan))
							  	     $this->messageLoanExceed =true;
							   	 
							      
							   	  if($remain_payroll >= ($model->number_of_month_repayment))
							   	   {//la remise/payroll
							   	       
							   	       $refund = $amount/$model->number_of_month_repayment;
							   	       
							   	       if($refund <= $total_net_salary)
							   	         {
									   	  	   $percent = ($refund/$total_net_salary)*100;
						   	  	  
									   	  	  if(is_decimal($percent) )
									   	  	     {
									   	  	     	  $percent = round(($refund/$total_net_salary)*100 , 0) + 1; 
									   	  	       }
						   	  	  									   	  	  
									   	  	   $model->setAttribute('deduction_percentage',$percent);
									   	  	   $model->setAttribute('remaining_month_number',$model->number_of_month_repayment);
											   $model->setAttribute('solde',$amount);
											   $model->setAttribute('date_updated',date('Y-m-d'));
										  	   
			
										  	
										    if($model->save())
											   $this->redirect(['view','id'=>$model->id,'part'=>'pay','from'=>'u']);
											   
							   	         }
							   	       else
							   	          {
							   	          	  //$this->messageRefund = true;  
							   	               Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Refund > Net salary.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
							   	          	
							   	          	}
								   
							   	   }
							   	 else
							   	   {
							   	   	    //$this->messageRemainPayroll = true;
				   	        
						   	         Yii::$app->getSession()->setFlash('warning', [
												    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Number of payroll remain < Repayment deadline(numb. payroll).') ),
												    'title' => Html::encode(Yii::t('app','Warning') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
							   	   	}
								   
						      }
						    else
						      {
						      	  //$this->messageAmountNotAllowed = true;	
						      	    Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Amount not allowed.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
						      	}
			
								
					
					 }
					    
			      }
			     else
				   {
					  // $this->message_UpdateAlreadyPaid=true;
					   //$this->message_UpdateValidate=1;
					   Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Update is denied, this loan is already paid.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
					     
					       	 
					}
           
           } 
           
           
            return $this->render('update', [
                'model' => $model,
                 'messageLoanNotPaid'=>$this->messageLoanNotPaid,
	                'messageLoanNotPossible'=>$this->messageLoanNotPossible,
            ]);
         
         
         
          }
     else
        {
        	/*
		              Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','-'.$titleDescription.'- cannot be either updated nor deleted.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);	
									*/						
						 $this->redirect(Yii::$app->request->referrer);
          } 
         
         
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing LoanOfMoney model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {      
        if(Yii::$app->user->can('billings-loanofmoney-delete'))
         {
        try {
   			       $model = $this->findModel($id);
   			     
   			     if( ($model->paid!=1)&&($model->amount==$model->solde) )
   			        $model->delete();
   			     else
   			        {
							   	          	  //$this->messageRefund = true;  
							   	               Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Loan already paid and cannot be deleted.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
							   	          	
							   	          	}
   			        
   			 // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			  if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			
						
			 return $this->redirect(['index']);
			 
		   } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			     
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
 
			 
    }



//************************  isPayrollDoneForOnes($month,$pers,$acad)  ******************************/	
	public function isPayrollDoneForOnes($month,$pers,$acad)
	{    
                $bool=false;
                
                $modelPayroll = new SrcPayroll;
               
		 if($month!='')
		   {
			  $result=$modelPayroll->isDoneForOnes($month,$pers,$acad);
				
				 if(isset($result)&&($result!=null))
				  {  $payroll=$result->getModels();//return a list of  objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						       $bool=true;
						}
						  
				   }
				   
				//check if there is any after
		/*		if(!$bool)
				  {
				  	 $any_after = Payroll::model()->anyAfterDoneForOnes($month,$pers,$acad);
  
				  	 if(isset($any_after)&&($any_after!=null))
						  {  //$any_after_ = $any_after->getData();//return a list of  objects
						           
						      foreach($any_after as $p)
						       {			   
								  
								  if($p['id']!=null)
								     {
						     	
								     	  $bool=true;
								     }
								}  
						   }
				  	
				  	}
			*/				 
		     }
		     
		     
		return $bool;
         
	}


//************************  isPayrollDone($month)  ******************************/	
	public function isPayrollDone($month)
	{    
                $bool=false;
                
                $modelPayroll = new SrcPayroll;
                
		 if($month!='')
		   {
			  $result=$modelPayroll->isDone($month);
				
				 if(isset($result))
				  {  $payroll=$result->getModels();//return a list of  objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						       $bool=true;
						}  
				   }	 					 
		     }
		     
		     
		return $bool;
         
	}



//return an objects of type Payroll
//************************  infoLastPayrollDone()  ******************************/	
	public function infoLastPayrollDone()
	{    
         $modelPayroll = new SrcPayroll; 
                
		  $result=$modelPayroll->getInfoLastPayroll();
		  
		  	 if(isset($result))
			  {  $payroll=$result->getModels();//return a list of ... objects
			           
			      foreach($payroll as $p)
			       {			   
					  if($p->id!=null)
					     {  
					     	return $p;
					     	 break;
					     	 
					     }
					     
					}  
			   }	 					 
			
		return null;
         
	}


//return an objects of type Payroll
//************************  infoLastPayrollDoneForOne($pers)  ******************************/	
	public function infoLastPayrollDoneForOne($pers)
	 {    
          $modelPayroll = new SrcPayroll;      
		  $result=$modelPayroll->getInfoLastPayrollForOne($pers);
		  
			 if(isset($result))
			  {  $payroll=$result->getModels();//return a list of ... objects
			           
			      foreach($payroll as $p)
			       {			   
					  if($p->id!=null)
					     {  
					     	return $p;
					     	 break;
					     	 
					     }
					     
					}  
			   }	 					 
			
		return null;
         
	}


//************************  getSelectedLongMonthValueForOne($pers)  ******************************/	
 public function getSelectedLongMonthValueForOne($pers)
	{    
         $code = array(); 
          $modelLoan = new SrcLoanOfMoney;      
          
          $nbre_payroll = infoGeneralConfig('total_payroll');  
               
		  $last_payroll_info=$this->infoLastPayrollDoneForOne($pers);
			
			if(isset($last_payroll_info)&&($last_payroll_info!=null))
			 {
				  if($last_payroll_info->payroll_month == $nbre_payroll)
				    {
				    	for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name=$modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
				     }
				  else
					{  for($i=($last_payroll_info->payroll_month + 1); $i<=$nbre_payroll; $i++)
					   {			   
						   $month_name=$modelLoan->getSelectedLongMonth($i);
						   
						   $code[$i]= $month_name; 	  
							     
					     }
					}  
			     
			  }
			else
			  {
			  	 
			  	 
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name=$modelLoan->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  	}
			  	 	
		return $code;
         
	}


//************************  getSelectedLongMonthValue()  ******************************/	
 public function getSelectedLongMonthValue()
	{    
         $code = array(); 
         $modelLoan = new SrcLoanOfMoney;      
		  
		  $nbre_payroll = infoGeneralConfig('total_payroll');  
               
		  $last_payroll_info=$this->infoLastPayrollDone();
			
			if(isset($last_payroll_info)&&($last_payroll_info!=null))
			 {
				  if($last_payroll_info->payroll_month == $nbre_payroll)
				    {
				    	for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name=$modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
				     }
				  else
					{  for($i=($last_payroll_info->payroll_month + 1); $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name=$modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
					}
			     
			  }
			else
			  {
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name=$modelLoan->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  	}
			  	 	
		return $code;
         
	}


			   
//************************  allLoanPaid($person)  ******************************/	
	public function allLoanPaid($person)
	{    
                $bool=true;
                $modelLoan = new SrcLoanOfMoney;
                
		  $result=$modelLoan->isPaid($person);
			
			 if(isset($result))
			  {  $loan_paid=$result->getModels();//return a list of  objects
			           
			      foreach($loan_paid as $l)
			       {			   
					  if($l->paid == 0)
					       $bool=false;
					}  
			   }	 					 
			
		return $bool;
         
	}



//************************  personHasLoan($person,$month)  ******************************/	
//return the total amount for this month
public function personHasLoan($person,$month)
	{    
                $sum=0;
                $modelLoan = new SrcLoanOfMoney;
                
		  $result=$modelLoan->hasLoan($person,$month);
			
			 if(isset($result))
			  {  $loan=$result->getModels();//return a list of  objects
			           
			      foreach($loan as $l)
			       {			   
					   $sum= $sum + $l->amount;
					}  
			   }	 					 
			
		return $sum;
         
	}




//return total deduction over gross_salary
public function getTaxes($id_pay_set, $gross_salary)
  {
  	     
$acad=Yii::$app->session['currentId_academic_year']; 

     $deduction=0;
  	 $total_deduction=0;
  	 
  	  $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_pay_set;
															
					  $result__ = Yii::$app->db->createCommand($sql__)->queryAll(); 
																       	   
						if($result__!=null) 
						 { foreach($result__ as $r)
						     { 
						     	  $deduction = 0;
						     	  
						     	  $sql1 = 'SELECT taxe_value FROM taxes WHERE id='.$r['id_taxe'];
															
								  $result1 = Yii::$app->db->createCommand($sql1)->queryAll(); 
																       	   
								     foreach($result1 as $r1)
								         $deduction = ( ($gross_salary * $r1['taxe_value'])/100);
								     
								  $total_deduction = $total_deduction + $deduction;
							  }
								  
						 }
	  	     
  	  return $total_deduction;
  	
  }




    /**
     * Finds the LoanOfMoney model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LoanOfMoney the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcLoanOfMoney::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
