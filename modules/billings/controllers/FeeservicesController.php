<?php

namespace app\modules\billings\controllers;

use Yii;
use app\modules\billings\models\FeeServices;
use app\modules\billings\models\SrcFeeServices;
use app\modules\billings\models\SrcBillingsServices;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * FeeservicesController implements the CRUD actions for FeeServices model.
 */
class FeeservicesController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FeeServices models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('billings-feeservices-index'))
         {
         	$searchModel = new SrcFeeServices();
	        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);
	    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       

    }

    /**
     * Displays a single FeeServices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('billings-fees-view'))
         {
            return $this->render('view', [
             'model' => $this->findModel($id),
            ]);
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new FeeServices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('billings-fees-create'))
         {
       
	        $model = new SrcFeeServices();
	        
                if(isset($_POST['create'])){
                    $is_save = TRUE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    
                       
                       for($i=1; $i<=10; $i++){
                            if( ($_POST["fee_s$i"]!='') && ($_POST["devise$i"]!='') && ($_POST["price$i"]!='') ){
                                $model = new SrcFeeServices();
                                
                               $model->devise = $_POST["devise$i"];
                                $model->service_description = $_POST["fee_s$i"];
                                $model->price = $_POST["price$i"];
                                
                                $model->created_date = date('Y-m-d H:i:s');
                                $model->created_by=currentUser();
                                if(!$model->save())
                                      $is_save = FALSE; 
                            }
                           else
                              break;
                              
                            }
                            if(!$is_save){
                                $dbTrans->rollback();
                                
                            }
                            else{
                                $dbTrans->commit();
                                return $this->redirect(['index','wh'=>'set_fs']);
                            }
                        
                    
                    
                }
                 return $this->render('create', [
		                'model' => $model,
		                
		                
		            ]);    
	         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       

    }

    /**
     * Updates an existing FeeServices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('billings-feeservices-update'))
         {
	        
	        
	        $model = $this->findModel($id);
	        
	        $fee_s = $model->service_description;
	        $devise = $model->devise;
            $price = $model->price;
	        
	          if ($model->load(Yii::$app->request->post()) ) 
		        {
		           $model_n->load(Yii::$app->request->post());
		            // $dbTrans = Yii::app->db->beginTransaction(); 
		         
		          if(isset($_POST['update']))
		            { 
					   //eske lap itilize deja?
					       $bill_serviceModel = new SrcBillingsServices;
					    $is_there = $bill_serviceModel->searchByFeeId($fee_s)->getModels();
					   
					 if($is_there!=null)
					  {   
					  	 $dbTrans = Yii::$app->db->beginTransaction(); 
					  	 
					   $model->setAttribute('old_new',0 );
					   $model->setAttribute('devise',$devise );
					   $model->setAttribute('price',$price );
					   $model_new->setAttribute('updated_date',date('Y-m-d H:i:s') );
					   $model_new->setAttribute('updated_by',currentUser() );
					   
					   if($model->save() )
					    {  //
					       
					       $model_new =  new SrcFeeServices();
					        
					        $model_new->devise = $model_n->devise;
                            $model_new->service_description = $fee_s;
                            $model_new->price = $model_n->price;
                            
						    $model_new->setAttribute('created_date',date('Y-m-d H:i:s') );
						    $model_new->setAttribute('created_by',currentUser() );
			
						      if($model_new->save() )
						        {  
						        	$dbTrans->commit();  	
					                return $this->redirect(['index', 'wh'=>'set_fs']);
								 }
							  else
								 {    $dbTrans->rollback();
								   }
								   
					       }
					      else
					        $dbTrans->rollback();
					      
					    }
					  else  // li poko itilize
					    {
					    	   $model_new->setAttribute('updated_date',date('Y-m-d H:i:s') );
							   $model_new->setAttribute('updated_by',currentUser() );
							   
							   if($model->save() )
							      return $this->redirect(['index', 'wh'=>'set_fs']);
					      }
					      
		               
		            }
		            
		            
		        } 

	            return $this->render('update', [
	                'model' => $model,
	            ]);
	        
	        
	     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    

    }

    /**
     * Deletes an existing FeeServices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('billings-fees-delete'))
         {
            try{ 
            	
            	 //eske lap itilize deja?
				//	   $bill_serviceModel = new SrcBillingsServices;
				//	    $is_there = $bill_serviceModel->searchByFeeId($id)->getModels();
					   
				//	 if($is_there==null)
				//	  {
					  	 $this->findModel($id)->delete();

                           return $this->redirect(['index','wh'=>'set_fs']);
                           
				//	  }
				//	else
				//	 {
					 	
				//	 	}
                 
                 
                 
               } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   

    }

    /**
     * Finds the FeeServices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FeeServices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FeeServices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
