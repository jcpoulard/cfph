<?php

namespace app\modules\inscription\controllers;

use Yii;
use app\modules\inscription\models\Postulant;
use app\modules\inscription\models\SrcPostulant;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\inscription\models\PostulantParent; 
use app\modules\inscription\models\PostulantLiablePerson;
use app\modules\inscription\models\PostulantHealth; 
use app\modules\inscription\models\PostulantSocial;
use app\modules\inscription\models\PostulantIncome;
use app\modules\inscription\models\PostulantGrades;
use app\modules\fi\models\YearMigrationCheck;
use app\modules\fi\models\SrcAcademicperiods;
use yii\helpers\Html;
use kartik\mpdf\Pdf; // For PDF 


/**
 * PostulantController implements the CRUD actions for Postulant model.
 */
class PostulantController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Postulant models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('inscription-postulant-index'))
         { 
            $searchModel = new SrcPostulant();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $model = new Postulant(); 
            $payment = new PostulantIncome(); 
            $acad = Yii::$app->session['currentId_academic_year'];

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model'=>$model,
                'payment'=>$payment,
                'acad'=>$acad,
            ]);
         }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    public function actionReport(){
        $model = new Postulant();
        return $this->render('report',[
            'model'=>$model
        ]); 
    }
    
     public function actionExam()
    {
        if(Yii::$app->user->can('inscription-postulant-exam'))
         {   
            $date_debut_inscription = infoCmsConfig("date_debut_inscription");
        $searchModel = new SrcPostulant();
        $dataProvider = $searchModel->searchByDateStart($date_debut_inscription);  //search(Yii::$app->request->queryParams);
        $model = new Postulant(); 
        $payment = new PostulantIncome(); 
        $grades = new PostulantGrades(); 
        
        
        return $this->render('exam', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'payment'=>$payment,
            'grades'=>$grades,
        ]);
         }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
         

    }
    
    public function actionGrades()
    {
        if(Yii::$app->user->can('inscription-postulant-decision'))
         { 
           $date_debut_inscription = infoCmsConfig("date_debut_inscription");
        
           $program= '';
	        $shift= '';
                $is_save =0;
	        
                
           $searchModel = new SrcPostulant();
        $dataProvider = $searchModel->searchByProgrameShift($program,$shift,$date_debut_inscription);
           $model = new Postulant(); 
        
        if($model->load(Yii::$app->request->post()) ) 
	   {
                  if(isset($_POST['Postulant']['apply_for_program']))
	                { $program= $_POST['Postulant']['apply_for_program'];
	                  $model->apply_for_program=[$program];
	                }
	                
	                     if(isset($_POST['Postulant']['shift']))
		              { $shift= $_POST['Postulant']['shift'];
		                 $model->shift=[$shift]; 
		         
		              }
                              
        $dataProvider = $searchModel->searchByProgrameShift($program,$shift,$date_debut_inscription);
        
               if(isset($_POST['create'])){
                 
                    $ok=true;
	                    $model_new = new PostulantGrades();
	                   
					
					 $no_stud_has_grade=true;
			 	
                              
                                  	
                                     
                                 		   
				     foreach($_POST['id_grade'] as $id)
		                        {   	   
                                           if(isset($_POST['grades'][$id])&&($_POST['grades'][$id]!=''))
                                                $no_stud_has_grade=false;

                                        }
                                  
			             
					      if($no_stud_has_grade==false) // omwen 1 etidyan gen not
						    {   foreach($_POST['id_grade'] as $id)
						         {
						            
	                                  if(isset($_POST['grades'][$id]))
								                $grade=$_POST['grades'][$id];
											else
											 $grade=0;
											

		                                $model_new->setAttribute('postulant',$id);
		                                $model_new->setAttribute('program',$program);
                                                
                                                $postul = SrcPostulant::findOne($id);
                                                
                                                $model_new->setAttribute('academic_year',$postul->academic_year);
							            
							            
						                $model_new->setAttribute('grade_value',$grade);
									    
						                 $model_new->date_create=date('Y-m-d H:i:s');
						                $model_new->create_by=currentUser();
			
						                if($model_new->save() )
						                  { 
						                  	unset($model_new);
						                     $model_new = new PostulantGrades();
						                    
						                     $ok=true;
						                  	
						                  	}
					                 
					                      
						            
						            										   
								} 
						             
						      }
						    else 
						     { $ok=false;
						       Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You did not insert any grades.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						         //$model->apply_for_program='';
						     }
                                                     
                                                     
									
			               if ($ok==true) 
					          {  
                                                      $is_save =1;
                                                           
                                                           Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                        
                                                           
                                                            return $this->redirect(['grades?wh=exam']); 
                                                            
                                                          
					          }
					        else
					           { //   $dbTrans->rollback();
                                                    $is_save =0;
					           }
                                                   
                    
                                                   
                                                   
                   
                  
	            
                    
                    
                 
                   }
        
                   
           }
           
        return $this->render('grades', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'is_save'=>$is_save,
        ]);
        }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
    }  
    
    
    
   public function actionDecision()
    {
        if(Yii::$app->user->can('inscription-postulant-decision'))
         { 
            
        $date_debut_inscription = infoCmsConfig("date_debut_inscription");
        $searchModel = new SrcPostulant();
        $dataProvider = $searchModel->searchByDateStart($date_debut_inscription);  //search(Yii::$app->request->queryParams);
       $model = new Postulant(); 
        $payment = new PostulantIncome();
        $command = Yii::$app->db->createCommand();
        
        if(isset($_POST['execute'])){
            if(isset($_POST['row_total'])){
              
                 $total_ins = $_POST['row_total'];
                 for($i=1; $i<$total_ins; $i++){
                      $id = $_POST['postulant_id'.$i]; 
                     if(isset($_POST['status'.$i]) && $_POST['status'.$i]!=""){
                         $status = $_POST['status'.$i];
                         $command->update('postulant', ['date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser(),'status'=>$status],['id'=>$id])->execute(); 
                         
                     }else{
                         $command->update('postulant', ['date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser(),'status'=>0],['id'=>$id])->execute(); 
                        
                     }
                 }
                 return $this->redirect(['decision','wh'=>'dec']); 
            }
            
           
        }
        else{

        return $this->render('decision', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'payment'=>$payment,
        ]);
        }
        return $this->render('decision', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'payment'=>$payment,
        ]);
        }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
    }


    /**
     * Displays a single Postulant model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $parent = PostulantParent::find()->where(['postulant'=>$id])->one(); 
        $liable = PostulantLiablePerson::find()->where(['postulant'=>$id])->one();
        $health = PostulantHealth::find()->where(['postulant'=>$id])->one();
        $social = PostulantSocial::find()->where(['postulant'=>$id])->one();
        if(PostulantIncome::find()->where(['postulant'=>$id])->one()!=null){
             $payment = PostulantIncome::find()->where(['postulant'=>$id])->one();
        }else{
            $payment = new PostulantIncome(); 
        }
       
        return $this->render('view', [
            'model' => $this->findModel($id),
            'parent'=>$parent,
            'liable'=>$liable,
            'health'=>$health,
            'social'=>$social,
            'payment'=>$payment,
        ]);
    }
    
    public function actionAdmis(){
        if(Yii::$app->user->can('inscription-postulant-admis'))
         { 
        $date_debut_inscription = infoCmsConfig("date_debut_inscription");
        $searchModel = new SrcPostulant();
        $dataProvider = $searchModel->searchByDateStart($date_debut_inscription);  //search(Yii::$app->request->queryParams);
       $model = new Postulant(); 
        $payment = new PostulantIncome();
        
        
        return $this->render('admis', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'payment'=>$payment,
        ]);
        }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
    }
    
    public function actionNonadmis(){
        if(Yii::$app->user->can('inscription-postulant-non-admis'))
         { 
        $date_debut_inscription = infoCmsConfig("date_debut_inscription");
        $searchModel = new SrcPostulant();
        $dataProvider = $searchModel->searchByDateStart($date_debut_inscription);  //search(Yii::$app->request->queryParams);
       $model = new Postulant(); 
        $payment = new PostulantIncome();
        
        
        return $this->render('nonadmis', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model'=>$model,
            'payment'=>$payment,
        ]);
         }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
    }
    
    public function actionDecisionData($shift,$program){
        $model = new Postulant(); 
        $payment = new PostulantIncome();
        
        return $this->renderAjax('decision_data',
                [
                    'shift'=>$shift,
                    'program'=>$program,
                    'model'=>$model,
                    'payment'=>$payment,
                ]);
    }
    
    public function actionReportData($shift,$program,$exam_date){
        $model = new Postulant(); 
       // $payment = new PostulantIncome();
        
        return $this->renderAjax('report_data',
                [
                    'shift'=>$shift,
                    'program'=>$program,
                    'model'=>$model,
                    'exam_date'=>$exam_date
                    
                ]);
    }

    /**
     * Creates a new Postulant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('inscription-postulant-create'))
         { 
        $model = new Postulant();
        $liable = new PostulantLiablePerson(); 
        $payment = new PostulantIncome();
        $acad = Yii::$app->session['currentId_academic_year'];
        $model->academic_year = $acad; 
        $model->school_date_entry = date('Y-m-d'); 
        $model->status = 0; 
        $model->date_created = date('Y-m-d H:i:s'); 
        $model->create_by = currentUser(); //Yii::$app->user->identity->username;
        $model->cohorte = date('Y'); 
        $code_verif = mt_rand(10,99).''. mt_rand(10,99).''.mt_rand(0,9);
        $model->validation_code = $code_verif; 
        $model->postulant_password = md5($code_verif);
        $model->is_validation_expire = 3;
        
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            
            $liable->postulant = $model->id; 
            $liable->create_date = date('Y-m-d H:i:s'); 
            $liable->create_by = currentuser();
            $liable->load(Yii::$app->request->post()); 
            $liable->save();
            
            $payment->postulant = $model->id; 
            $payment->date_created = date('Y-m-d H:i:s'); 
            $payment->create_by = currentUser();
            $payment->academic_year = $acad;
            $payment->amount = infoGeneralConfig('frais_inscription'); 
             $payment->devise = 1;
             $payment->payment_method = 1;
             $payment->comments = "Inscription en presentiel";
             $payment->payment_date = date("Y-m-d H:i:s");
            
            $payment->load(Yii::$app->request->post()); 
            $payment->save();
            return $this->redirect(['index', 'wh' =>'ins']);
        } else {
            return $this->render('create', [
                'model' =>$model,
                'liable'=>$liable,
                'payment'=>$payment,
            ]);
        }
         }
        else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' =>120000,
                        'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                        'title' => Html::encode(Yii::t('app','Unthorized access') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing Postulant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('inscription-postulant-update')){
        $model = $this->findModel($id);
        $parent = PostulantParent::find()->where(['postulant'=>$id])->one(); 
        $liable = PostulantLiablePerson::find()->where(['postulant'=>$id])->one();
        $health = PostulantHealth::find()->where(['postulant'=>$id])->one();
        $social = PostulantSocial::find()->where(['postulant'=>$id])->one();
        if(PostulantIncome::find()->where(['postulant'=>$id])->one()!=null){
             $payment = PostulantIncome::find()->where(['postulant'=>$id])->one();
        }else{
            $payment = new PostulantIncome(); 
        }
       
        
        $acad = Yii::$app->session['currentId_academic_year'];
       // $model->academic_year = $acad; 
        //$model->school_date_entry = date('Y-m-d'); 
        $model->status = 0; 
        $model->date_updated = date('Y-m-d H:i:s'); 
        $model->update_by = Yii::$app->user->identity->username;
        $model->cohorte = date('Y'); 
        /*
        if(isset($_POST['Postulant']['last_class'])){
            $last_class = $_POST['Postulant']['last_class'];
        }else{
            $last_class = NULL; 
        }
         * 
         */
       // $model->last_class = $last_class; 

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $parent->postulant = $model->id; 
            $parent->update_date = date('Y-m-d H:i:s'); 
            $parent->update_by = Yii::$app->user->identity->username;
            $parent->load(Yii::$app->request->post()); 
            $parent->save();
            
            $liable->postulant = $model->id; 
            $liable->update_date = date('Y-m-d H:i:s'); 
            $liable->update_by = Yii::$app->user->identity->username;
            $liable->load(Yii::$app->request->post()); 
            $liable->save();
            
            $health->postulant = $model->id; 
            $health->update_date = date('Y-m-d H:i:s'); 
            $health->update_by = Yii::$app->user->identity->username;
            $health->load(Yii::$app->request->post()); 
            $health->save();
            
            $social->postulant = $model->id; 
            $social->update_date = date('Y-m-d H:i:s'); 
            $social->update_by = Yii::$app->user->identity->username;
            $social->load(Yii::$app->request->post()); 
            $social->save();
            
            if($payment->id!=NULL){
                $payment->postulant = $model->id; 
                $payment->date_updated = date('Y-m-d H:i:s'); 
                $payment->update_by = Yii::$app->user->identity->username;
                $payment->load(Yii::$app->request->post()); 
                $payment->save();
            }else{
            $payment->postulant = $model->id; 
            $payment->academic_year = $acad; 
            $payment->create_by = Yii::$app->user->identity->username;
            $payment->date_created = date('Y-m-d H:i:s');
            $payment->date_updated = date('Y-m-d H:i:s'); 
            $payment->update_by = Yii::$app->user->identity->username;
            
            $payment->load(Yii::$app->request->post()); 
           // print_r(Yii::$app->request->post());
            $payment->save();
            }
            return $this->redirect(['index', 'wh' =>'ins']);
            
        } else {
            return $this->render('update', [
                'model' => $model,
                'parent'=>$parent,
                'liable'=>$liable,
                'health'=>$health,
                'social'=>$social,
                'payment'=>$payment,
            ]);
        }
        } else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
    }

    /**
     * Deletes an existing Postulant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('inscription-postulant-delete'))
         { 
        $parent = PostulantParent::find()->where(['postulant'=>$id])->one(); 
        if($parent!=NULL){
            $parent->delete(); 
        }
        $liable = PostulantLiablePerson::find()->where(['postulant'=>$id])->one();
        if($liable!=NULL){
            $liable->delete(); 
        }
        $health = PostulantHealth::find()->where(['postulant'=>$id])->one();
        if($health!=NULL){
            $health->delete(); 
        }
        $social = PostulantSocial::find()->where(['postulant'=>$id])->one();
        if($social!=NULL){
            $social->delete(); 
        }
        
        $payment = PostulantIncome::find()->where(['postulant'=>$id])->one();
        if($payment!=NULL){
            $payment->delete(); 
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index','wh'=>'ins']);
         } else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
         
    }
    
    public function actionMakepending($id){
        $model = $this->findModel($id); 
        $model->status = 0;
        $model->date_updated = date('Y-m-d H:i:s');
        $model->update_by = Yii::$app->user->identity->username;
        if($model->save()){ 
            if(isset($_GET['wh']) && $_GET['wh']=='ins')
                return $this->redirect(['index','wh'=>'ins']);
            elseif(isset ($_GET['wh']) && $_GET['wh']=='adm') 
                return $this->redirect(['admis','wh'=>'adm']);
            elseif(isset ($_GET['wh']) && $_GET['wh']=='nadm')
                return $this->redirect(['nonadmis','wh'=>'nadm']);
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionMakenonadmis($id){
        $model = $this->findModel($id); 
        $model->status = 2;
        $model->date_updated = date('Y-m-d H:i:s');
        $model->update_by = Yii::$app->user->identity->username;
        if($model->save()){ 
            if(isset($_GET['wh']) && $_GET['wh']=='ins')
                return $this->redirect(['index','wh'=>'ins']);
            elseif(isset ($_GET['wh']) && $_GET['wh']=='adm') 
                return $this->redirect(['admis','wh'=>'adm']);
          
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionMakeadmis($id){
        $model = $this->findModel($id); 
        $model->status = 1;
        $model->date_updated = date('Y-m-d H:i:s');
        $model->update_by = Yii::$app->user->identity->username;
        if($model->save()){ 
            if(isset($_GET['wh']) && $_GET['wh']=='ins')
                return $this->redirect(['index','wh'=>'ins']);
            elseif(isset ($_GET['wh']) && $_GET['wh']=='nadm') 
                return $this->redirect(['nonadmis','wh'=>'nadm']);
           
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Postulant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Postulant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Postulant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
   public function actionStep(){
       return $this->render('step'); 
   }
   
   public function actionSetGrade(){
       $acad = Yii::$app->session['currentId_academic_year'];
       $model = new PostulantGrades(); 
       if(isset($_POST)){
           if(isset($_GET['is'])){
                $model = PostulantGrades::findOne($_POST['pk']); 
                $model->grade_value = $_POST['value'];
                $model->update_by = Yii::$app->user->identity->username;
                $model->date_update = date('Y-m-d H:i:s'); 
                $model->save(); 
           }else{
           $model->postulant = $_POST['pk']; 
           $model->academic_year = $acad; 
           $model->program = $_POST['name']; 
           $model->grade_value = $_POST['value']; 
           $model->create_by = Yii::$app->user->identity->username;
           $model->date_create = date('Y-m-d H:i:s'); 
           $model->save(); 
           }
       }
       
       echo 'TEST';
   }
   
   public function actionSetStatus(){
      
       $acad=Yii::$app->session['currentId_academic_year'];
                $YearMigrationCheck = new YearMigrationCheck;
                $SrcAcademicPeriods = new SrcAcademicperiods;
             $previous_year= $SrcAcademicPeriods->getPreviousAcademicYear($acad);
             
       if(isset($_POST)){
           $model = Postulant::findOne($_POST['pk']); 
           $model->status = $_POST['value']; 
           $model->update_by = Yii::$app->user->identity->username;
           $model->date_updated = date('Y-m-d H:i:s'); 
           $model->save(); 
           
             
                $start_date =''; // dat ane akademik lan t kreye
              $nonb_de_jou =0;
               $data_migrationCheck = $YearMigrationCheck->getValueYearMigrationCheck($previous_year);

               if($data_migrationCheck!=null)
                   {  foreach($data_migrationCheck as $d)
                       {
                         if($d['date_created']!='')
                            $start_date = $d['date_created'];
                         
                         }
                   
                    }
                    
                    if($start_date!='')
                       $nonb_de_jou = date_diff ( date_create($start_date)  , date_create(date('Y-m-d') ))->format('%R%a');

                      //if($nonb_de_jou > 90) //60 jou
                       //$message_year_migration= Yii::t('app','Migration is not allowed after 90 days start from the begining of the academic year.');
                       if($nonb_de_jou <= 90)
                       {
                           $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET postulant=0, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          
                       }
           
           
       }
   }
   
   public function actionValidate(){
       if(Yii::$app->user->can('inscription-postulant-index'))
         { 
           return $this->render('validate');
         }
          else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   }
   
   public function actionViewInscription(){
       if(Yii::$app->user->can('inscription-postulant-index'))
         { 
           if(isset($_REQUEST['id_stud'])){
              
               $id_postulant = $_REQUEST['id_stud'];
               if(isset(PostulantLiablePerson::findOne(['postulant'=>$id_postulant])->postulant)){
                   $person_responsable = PostulantLiablePerson::findOne(['postulant'=>$id_postulant])->id; 
                   return $this->renderAjax('view-inscription',['id'=>$id_postulant,'responsable_id'=>$person_responsable]);
               }else{
                   return "<div class='alert alert-danger'><button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>".Yii::t('app','The number <strong>{number}</strong> is not found on our database !',['number'=>$id_postulant])."</div>";
               }
               
              
           }else{
               return 0;
           }
           
         }
         else 
             {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
   }
   
   public function actionUpdateInscription(){
    if(isset($_POST['pk'])){
        $pk = $_POST['pk']; 
        $postulant = Postulant::findOne($pk);
        if(isset($_POST['name'])){
            $name = $_POST['name']; 
            switch($name){
                case 'prenom':{
                    $postulant->first_name = $_POST['value'];
                    $postulant->save();
                    
                }
                break;
                case 'nom':{
                    $postulant->last_name = $_POST['value'];
                    $postulant->save();
                }
                break;
                case 'sexe': {
                    $postulant->gender = $_POST['value'];
                    $postulant->save();
                }
                break;
                case 'email': {
                    $postulant->email = $_POST['value'];
                    $postulant->save();
                }
                break;
                case 'vacation':{
                    $postulant->shift = $_POST['value']; 
                    $postulant->save();
                }
                break;
                case 'choix1':{
                    $postulant->apply_for_program = $_POST['value']; 
                    $postulant->save(); 
                }
                break;
                case 'choix2':{
                    $postulant->program_second = $_POST['value']; 
                    $postulant->save(); 
                }
                break;
                case 'adresse':{
                    $postulant->adresse = $_POST['value'];
                    $postulant->save(); 
                }
                break;
                case 'phone':{
                    $postulant->phone = $_POST['value'];
                    $postulant->save(); 
                }
                break;
                case 'school':{
                    $postulant->previous_school = $_POST['value'];
                    $postulant->save();
                }
                break;
                case 'working':{
                    $postulant->is_working = $_POST['value']; 
                    $postulant->save(); 
                }
                break;
                
                case 'responsable_prenom':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->first_name = $_POST['value'];
                    $responsable->save();
                }
                break;
                case 'responsable_nom':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->last_name = $_POST['value'];
                    $responsable->save();
                }
                break;
                case 'responsable_adresse':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->address = $_POST['value'];
                    $responsable->save();
                }
                break;
                case 'responsable_phone':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->phone = $_POST['value'];
                    $responsable->save();
                }
                break;
                case 'responsable_email':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->email_responsable = $_POST['value'];
                    $responsable->save();
                }
                break;
                case 'responsable_relation':{
                    $id_res = $_POST['pk'];
                    $responsable = PostulantLiablePerson::findOne($id_res); 
                    $responsable->relation_responsable = $_POST['value'];
                    $responsable->save();
                }
                break;
            
                case 'exam_date':{
                    
                    $postulant->exam_date = $_POST['value'];
                    $postulant->save();
                }
                break;
            
                case 'birthday':{
                    
                    $postulant->birthday = $_POST['value'];
                    $postulant->save();
                }
                break;
                 
                
            }
        }
    }else{
        return "failled";
    }
}

public function actionListSexe(){
    $all_sexe = [['value'=>0,'text'=>'Masculin'],['value'=>1,'text'=>'Feminin']];
    $json_sexe = json_encode($all_sexe,false);
    return $json_sexe; 
}

public function actionListBool(){
    $all_sexe = [['value'=>0,'text'=>'Non'],['value'=>1,'text'=>'Oui']];
    $json_sexe = json_encode($all_sexe,false);
    return $json_sexe; 
}

public function  actionListShift(){
         $all_values = \app\modules\fi\models\Shifts::findBySql("SELECT id AS 'value', shift_name AS 'text'  FROM shifts ORDER BY shift_name")->asArray()->all();
         $json_values =json_encode($all_values,false); 
         return $json_values; 
    }

public function  actionListProgram(){
         $all_values = \app\modules\fi\models\Program::findBySql("SELECT id AS 'value', label AS 'text'  FROM program WHERE is_special = 0 ORDER BY label")->asArray()->all();
         $json_values =json_encode($all_values,false); 
         return $json_values; 
    } 
    
public function  actionListRelation(){
         $all_values = \app\modules\fi\models\Relations::findBySql("SELECT id AS 'value', relation_name AS 'text'  FROM relations ORDER BY relation_name")->asArray()->all();
         $json_values =json_encode($all_values,false); 
         return $json_values; 
    }
    
    
 public function actionValidateInscription(){
    
     if(isset($_REQUEST['id_postulant'])){
         $postulant_income = new PostulantIncome();
         $acad = Yii::$app->session['currentId_academic_year'];
         $id_postulant = $_REQUEST['id_postulant'];
         $date_examen = $_REQUEST['date_examen']; 
         
         $postulant = Postulant::findOne($id_postulant);
         $postulant->is_validation_expire = 3;
         $postulant->exam_date = date("Y-m-d",strtotime($date_examen));
         $postulant->update_by = currentUser(); 
         $postulant->date_updated = date("Y-m-d H:i:s");
         $postulant->save();
         $postulant_income->postulant = $postulant->id; 
         $postulant_income->amount = infoGeneralConfig("frais_inscription");
         $postulant_income->devise = 1;
         $postulant_income->payment_method = 1;
         $postulant_income->comments = "Inscription en ligne";
         $postulant_income->payment_date = date("Y-m-d H:i:s");
         $postulant_income->academic_year = $acad;
         $postulant_income->date_created = date("Y-m-d H:i:s");
         $postulant_income->create_by = currentUser(); 
         $postulant_income->save(); 
         
         $this->setMailSender(); 
         $school_email = infoGeneralConfig('email_portal_relay');
         Yii::$app->formatter->locale = 'fr-FR'; 
         $date_exam_format = Yii::$app->formatter->format(strtotime($postulant->exam_date), 'date');
         $email_body =Yii::t('app',
                 "Salut,<br/> {postulant_name} votre inscription au CFPH a &eacute;t&eacute; valid&eacute;e le {date_encours}.<br/> Vous avez pay&eacute; un frais de <strong>{montant_frais} HTG</strong>.<br/>"
                 . "<strong>Votre  date d'examen au concours d'admission est fix&eacute;e pour le {date_examen} &agrave; 9h00 AM.</strong><br/>Pour toute information visitez notre site web {site_web}<br/>"
                 . "Cordialement,<br/>La Direction ",
                 [
                     'postulant_name'=>$postulant->first_name.' '.$postulant->last_name,
                     'date_encours'=>Yii::$app->formatter->format(strtotime($postulant_income->payment_date),'date'),
                     'montant_frais'=>$postulant_income->amount,
                     'date_examen'=>$date_exam_format,
                     'site_web'=>"<a href='https://canadotechnique.tech'>https://canadotechnique.tech</a>"
                 ]);
         $email_subject = Yii::t('app','Validation inscription CFPH pour {postulant_name}',['postulant_name'=>$postulant->first_name.' '.$postulant->last_name]);
         voyeEmail([$school_email=>'Canado Technique CFPH'], $postulant->email, $email_subject, $email_body);
         return $this->renderAjax('validate-inscription',['id_postulant'=>$id_postulant]);
     }
     
 }
 
 public function setMailSender(){
        $key = infoGeneralConfig('email_secret_key'); 
        $krip_pass = infoGeneralConfig('email_password_relay');
        $iv = infoGeneralConfig('email_iv');
        try{
        Yii::$app->set('mailer', [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                // Values from db
                'host' => infoGeneralConfig('email_host_relay'), 
                'username' => infoGeneralConfig('email_portal_relay'),
                'password' =>$krip_pass,
                'port' => infoGeneralConfig('email_port_relay'),
                'encryption' =>'ssl',
            ],
        ]);
        } catch (Exception $e){
            echo $e->getMessage(); 
        }
     }
     
 public function actionPrintForm($id)
    {
     
        $content = $this->renderPartial('print-form',['id'=>$id]);
        //return $content;
        
        $postulant = Postulant::findOne($id);
        Yii::$app->formatter->locale = 'fr-FR';
        $date_ins =  Yii::$app->formatter->asDate($postulant->date_created); 
        $date_inscription = "Date d'inscription : $date_ins";
        $pdf = new Pdf(
                [
    
                    'cssFile' => 'css/pdf.css',
               ]
                );
        $pdf->filename = "formulaire_inscription $postulant->first_name $postulant->last_name ".date('Y-m-d h:i:s').".pdf";
        $pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	//$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
        
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'Formualire Inscription CPFH'];
    	$pdf->methods = [
				//'SetHeader'=>['Donnee Enquetes Avril 2015'],
                            'SetFooter'=>["$date_inscription"],
			];
    	
    	$pdf->options = [
	    	'title' => 'Formulaire inscription CFPH',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
        
    	return $pdf->render();
        
    }    
     
   public function actionPrintFormHtml($id){
        return $this->render('print-form',['id'=>$id]);
    }
    
    public function actionMakeChoix($id){
         return $this->renderAjax('choix2',['id'=>$id]); 
     }
     
     public function actionDashboardInscription(){
          $acad = Yii::$app->session['currentId_academic_year'];
          return $this->renderAjax('dashboard-inscription',['acad'=>$acad]); 
     }
     
     public function actionSuiteDashboardInscription(){
          $acad = Yii::$app->session['currentId_academic_year'];
          return $this->renderAjax('dashboard-inscription2',['acad'=>$acad]); 
     }
   
   
    
}
