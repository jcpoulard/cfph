<?php

namespace app\modules\inscription\controllers;

use yii\web\Controller;

/**
 * Default controller for the `inscription` module
 */
class DefaultController extends Controller
{
    
    public $layout = "/inspinia";
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
