<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\inscription\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\inscription\models\SrcPostulant;
use app\modules\inscription\models\PostulantIncome;
use app\modules\inscription\models\SrcPostulantIncome;

use app\modules\billings\models\SrcFeesLabel;

class PostulantincomeController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex(){
       if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
        $date_start = infoCmsConfig("date_debut_inscription");
            
        $all_postulants = loadAllPostulantsJson($date_start);
        $json_all_postulants = json_encode($all_postulants,false);
        
        $searchModel = new SrcPostulantincome();
        $dataProvider = $searchModel->searchPostulantPaye($date_start);
        return $this->render('index',
                [
                    //'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'json_all_postulants'=>$json_all_postulants,
                ]
                );
    }
    
    
        /**
     * Displays a single  model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('inscription-postulantincome-view'))
         {
        
                  $acad = Yii::$app->session['currentId_academic_year'];
          $date_start = infoCmsConfig("date_debut_inscription");
          
           $fee_period=''; 
           $fee_paid = '';
           $fee_already_paid = false;

           $model = new SrcPostulantIncome;
                         
                         
                         if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                           {
                              $modelAdd = SrcPostulantIncome::findOne($_GET['bil']);
                             // $this->fee = $modelAdd->fee;
                            }
                          else
                           $modelAdd = new SrcPostulantIncome; 
                          
         $all_postulants = loadAllStudentsJson();
        $json_all_postulants = json_encode($all_postulants,false);
        
        $searchModel = new SrcPostulantincome();
        $dataProvider = $searchModel->searchPostulantPaye($date_start);
        
        //pou update			                   
   if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
     {
        // $this->old_student ="";
		
          $amount_current_record = $modelAdd->amount;
        
           //$this->old_student = $modelAdd->student; //
           // $this->fee_=$modelAdd->fee;
           // $this->old_fee = $modelAdd->fee;
           // $this->old_amount = $modelAdd->amount;
            
        
        //$modelAdd = SrcPostulantincome::findOne($_GET['bil']);
        $modelUp = SrcPostulantincome::findOne($_GET['bil']);
        
        $fee_period = $modelUp->fee;;
        
        //$modelAddFee = SrcFeesLabel::findOne($modelAdd->fee);
        
        $currency_id = $modelAdd->devise;  //$modelAddFee->devise;
         
        } //fen pou update   
           
        
        
         if($modelAdd->load(Yii::$app->request->post()) ) 
           {      
              $fee_paid= $_POST['SrcPostulantIncome']['fee'];
              
                //tcheke si yo peye fre sa deja //pou pa afiche lot chan yo ak bouton yo
                 $already_paid = $model->isFeeAlreadyPaid($_GET['id'],$fee_paid);

              //pou update
              if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                {
                   
                  if($already_paid==0)
                      $modelAdd->fee = $fee_paid;
                   else
                      $modelAdd->fee = $modelUp->fee;
                 }//fen pou update
                 

             
                 
                 if($already_paid==1) //pa kite anyen afiche
                  {
                     
                     $fee_already_paid =true;
                     $modelAddFee = SrcFeesLabel::findOne($fee_paid);
                     
                     Yii::$app->getSession()->setFlash('warning', [
                                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                    'duration' => 36000,
                                                                    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                    'message' => Html::encode( Yii::t('app','- '.$modelAddFee->fee_label.' - ,').Yii::t('app',' already paid.')  ),
                                                                    'title' => Html::encode(Yii::t('app','Info') ),
                                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                                ]);
                  }
             
              
             if(isset($_POST['create']))
	      {
                 $save_ok = false;
				
                 if(($_POST['SrcPostulantIncome']['payment_date']!="")&&($_POST['SrcPostulantIncome']['payment_date']!='0000-00-00'))
                   {  
                        $date_pay_ = $_POST['SrcPostulantIncome']['payment_date'];
                        $modelAdd->setAttribute('payment_date', $date_pay_);
                        if($modelAdd->payment_method!="")
                          {
                             //pou update 
                             if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                               {
                                  //$modelPostulantincome = new SrcPostulantIncome;
                               
                                
                                      $payment_method = $modelAdd->payment_method;

                                      $comments = $modelAdd->comments;
                                      

                                      if($modelAdd->payment_date =='0000-00-00')
                                         $date_pay = date('Y-m-d');
                                      else
                                         $date_pay = $modelAdd->payment_date;
                                  
                                       $modelUp->payment_method= $payment_method;
                                       $modelUp->comments= $comments;
                                       $modelUp->payment_date= $date_pay;

                                       $modelUp->date_updated=date('Y-m-d');
                                       $modelUp->update_by=currentUser();
                                       
                                      if($modelUp->save())
                                        {       
                                            return $this->redirect(['view','id'=>$modelUp->postulant]);  	          
                                         }
                                        
                                       
                                       
                                       
                                 
                                  
                               }//fen pou update
                           else  //pou create
                             {
                                   if($modelAdd->amount!=null)
                                    {
                                      $modelPostulantincome = new SrcPostulantIncome;
                                          
                                      $payment_method = $modelAdd->payment_method;
                                          $comments = $modelAdd->comments;
                                          $date_pay = $modelAdd->payment_date;

                                           $modelAdd->setAttribute('postulant', $_GET['id']);
                                           //$modelAdd->setAttribute('amount', $amount); 
                                           $modelAdd->setAttribute('payment_date', $date_pay);
                                           
                                           $modelAdd->setAttribute('payment_method', $payment_method);
                                           $modelAdd->setAttribute('comments', $comments);
                                           $modelAdd->setAttribute('academic_year', $acad);

                                             $modelAdd->setAttribute('create_by', currentUser() );
                                             $modelAdd->setAttribute('date_created', date('Y-m-d'));



                                              $ID = '';

                                               if($modelAdd->save())
                                                {
                                                  /* $command_ = Yii::$app->db->createCommand();
                                                    $command_->update('billings', ['fee_totally_paid' => 1,   ], ['id'=>$last_payment_transaction_id ])
                                                      ->execute();
                                                   * */
                                                   

                                                   $ID = $modelAdd->id;
                                                   $save_ok = true; 
                                                }
                                     
                                    }
                              }
                            
                            
                            
                           }
                          else
                          {  //payment method can't be null
                              
                          }
                        
                     }
                  else
                      {  //payment_date can't be null

                      }
                 
                 if($save_ok)
                    {  
                     return $this->redirect(['view','id'=>$modelAdd->postulant]);
														
                    }
                 
                 
                 
                 
                 
	      }
             
              
              
              
              
           }
  //########################################################################################
               
	   
       
       
	       return $this->render('view', [
            'model' => $model,
            'modelAdd' => $modelAdd,
             // 'modelFee'=>$modelFee,    
                   'dataProvider' => $dataProvider,
            'json_all_postulants'=>$json_all_postulants,
              'fee_period'=>$fee_period,
              'fee_already_paid'=>$fee_already_paid,
               //  'remain_balance'=>$this->remain_balance,
               
        ]);
        
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
    }

    
  public function actionDelete($id)
	{
        if(Yii::$app->user->can('inscription-postulantincome-delete'))
         {      
              //$acad=Yii::$app->session['currentId_academic_year']; 
		
	try{	$modelBilling = new SrcPostulantIncome();
       
       //$acad=Yii::$app->session['currentId_academic_year'];
		
   			   $modelB=$this->findModel($id);
   			   
   			   $id_bill = $modelB->id;
   			 
   			      $modelB->delete();
                              return  $this->redirect(Yii::$app->request->referrer);
   			
            } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          return  $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                 return  $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
		
		
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return  $this->redirect(Yii::$app->request->referrer);
               }

          }

       
                        
	}    
    
    
    
    
    
  /**
     * Finds the PostulantIncome model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PostulantIncome the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcPostulantIncome::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }   
    
    
    
    
    
}