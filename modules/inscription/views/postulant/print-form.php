<?php
/* @var $this yii\web\View */
use app\modules\inscription\models\Postulant; 
use app\modules\inscription\models\PostulantLiablePerson;

$postulant = Postulant::findOne($id);
$responsable = PostulantLiablePerson::findOne(['postulant'=>$id]);

//echo $postulant->first_name.' '.$postulant->last_name;
//echo Yii::$app->request->BaseUrl.'/web/img/logo_canado.png';

?>

<table>
    <tr>
        <th width="20%">
            <img src="<?= Yii::$app->request->BaseUrl?>/img/canado_logo.png" width="200px" height="50px">
            
        </th>
        <th width="60%">
            <span style="font-size: 24px;"><?= infoGeneralConfig('school_name');?></span>
            <br/>
            <span style="font-size: 16px;"><?= infoGeneralConfig('school_address'); ?></span>
            <br/>
            <span style="text-align: center"><a href="https://canadotechnique.tech">https://canadotechnique.tech</a></span>
        </th>
        <th width="20%">
            <img src="<?= Yii::$app->request->BaseUrl?>/img/no_pic.png" width="200px" height="200px">
        </th>
    </tr>
    
    
</table>
<h3 style="text-align: center">Formulaire d'Inscription</h3>
<table width="100%" class="bodi">
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>INFORMATION POSTULANT</strong></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Pr&eacute;nom </strong></td>
        <td width="25%" class="bodi"><?= $postulant->first_name ?></td>
        <td width="25%" class="bodi"><strong>Nom </strong></td>
        <td width="25%" class="bodi"><?= $postulant->last_name ?></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Sexe</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->sexe; ?></td>
        <td width="25%" class="bodi"><strong>Date de naissance</strong></td>
        <td width="25%" class="bodi">
            <?php    
            Yii::$app->formatter->locale = 'fr-FR';
            ?>
            <?= Yii::$app->formatter->asDate($postulant->birthday); ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Vacation</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->shift0->shift_name; ?></td>
        <td width="25%" class="bodi"><strong>Programme 1</strong></td>
        <td width="25%" class="bodi">
            <?= $postulant->applyForProgram->label; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Programme 2</strong></td>
        <td width="25%" class="bodi"> 
        <?php
                if(isset($postulant->programSecond->label)){
                    echo $postulant->programSecond->label;
                }else{
                    echo "";
                } 
        ?>
        </td>
        <td width="25%" class="bodi"><strong>Adresse postulant</strong></td>
        <td width="25%" class="bodi">
            <?= $postulant->adresse; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>T&eacute;l&eacute;phone</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->phone; ?></td>
        <td width="25%" class="bodi"><strong>Derni&egrave;re &eacute;cole fr&eacute;quent&eacute;e</strong></td>
        <td width="25%" class="bodi">
            <?= $postulant->previous_school; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Travaillez vous ?</strong></td>
        <td width="25%" class="bodi"><?php if($postulant->is_working==1){echo "Oui"; }else{ echo "Non"; } ?></td>
        <td width="25%" class="bodi"><strong>Email</strong></td>
        <td width="25%" class="bodi">
            <?= $postulant->email; ?>
        </td>
    </tr>
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>PERSONNE RESPONSABLE</strong></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Pr&eacute;nom responsable</strong></td>
        <td width="25%" class="bodi">
            <?= $responsable->first_name; ?>
        </td>
        <td width="25%" class="bodi"><strong>Nom  responsable</strong></td>
        <td width="25%" class="bodi">
            <?= $responsable->last_name; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Adresse responsable</strong></td>
        <td width="25%" class="bodi">
            <?= $responsable->address; ?>
        </td>
        <td width="25%" class="bodi"><strong>T&eacute;l&eacute;phone responsable</strong></td>
        <td width="25%" class="bodi">
            <?= $responsable->phone; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Email responsable</strong></td>
        <td width="25%" class="bodi">
            <?php
            if(isset($responsable->email_responsable)){
                echo $responsable->email_responsable;
            }else{
                echo "";
            } 
            ?>
        </td>
        <td width="25%" class="bodi"><strong>Relation responsable</strong></td>
        <td width="25%" class="bodi">
            <?php
                if(isset($responsable->relation0->relation_name)){
                    echo $responsable->relation0->relation_name;
                } else{
                    echo "";
                }
            ?>
        </td>
    </tr>
    <tr class='bodi'>
        <td width="25%" class="bodi"><strong>Date d'examen</strong></td>
        <td width="25%" class="bodi">
            <?php 
                if(isset($postulant->exam_date)){
                    Yii::$app->formatter->locale = 'fr-FR';
                    echo Yii::$app->formatter->asDate($postulant->exam_date); 
                }else{
                    echo "";
                }
            ?>
        </td>
        <td width="25%" class="bodi"><strong>No cahier de controle</strong></td>
        <td width="25%" class="bodi"></td>
    </tr>
    
  </table>
<p style='text-align: center'>
    Code Inscription : <?= $postulant->id; ?>
    <br/>
<?php
    $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
    echo '<img  src="data:image/png;base64,' . base64_encode($generator->getBarcode($postulant->id, $generator::TYPE_CODE_39)) . '" style="width: 150px; height: 30px; margin-bottom: 5px; margin-top: 10px; display: block;">';
?>
</p>

