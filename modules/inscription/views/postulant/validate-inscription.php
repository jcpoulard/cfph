<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\inscription\models\Postulant; 

$postulant = Postulant::findOne($id_postulant);
?>
<div class='alert alert-warning alert-dismissable'>
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <i class='fa fa-check'></i> <?= Yii::t('app','Subscription for {name} validate with success !',['name'=>$postulant->first_name.' '.$postulant->last_name])?>
    
</div>