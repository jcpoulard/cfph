<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\fi\models\Shifts; 
use app\modules\fi\models\Program; 
use app\modules\inscription\models\Postulant; 
use yii\widgets\ActiveForm;
use app\modules\inscription\models\PostulantIncome; 
use yii\helpers\Html;


$shift = Shifts::find()->all(); 

?>

  <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><?= Yii::t('app','Total subscription by program');?></h5>
                                
                            </div>
                            <div class="ibox-content">
                               <div class="m-t-sm">

                                    <div class="row">
                                        <div class="col-lg-8">
                                          <div class="flot-chart">
                                            <div class="flot-chart-content" id="morris-bar-chart"></div>
                                          </div>
                                        </div>
                                        
                                        <div class="col-lg-4">
                                        <ul class="stat-list">
                                        <?php
                                            $total_pos = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire IN (3)")->count();
                                            foreach($shift as $s){
                                                
                                                $total_pos_by_shift = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire IN (3) AND shift = $s->id ")->count();
                                                
                                                ?>
                                            <li>
                                                <h2 class="no-margins"><?= $total_pos_by_shift; ?></h2>
                                                <small><?= Yii::t('app','Inscription ').' '.$s->shift_name; ?></small>
                                                <div class="stat-percent">
                                                    <?php
                                                        if($total_pos > 0){
                                                            echo round(($total_pos_by_shift/$total_pos)*100,2).'%';
                                                        }else{
                                                            echo '0%';
                                                        }
                                                    ?>
                                                </div>
                                                <div class="progress progress-mini">
                                                <div style="width: <?php if($total_pos > 0) {echo round(($total_pos_by_shift/$total_pos)*100,2); }else{echo '0%';}?>%;" class="progress-bar"></div>
                                            </div>
                                            </li>
                                                
                                            <?php 
                                            }
                                        ?>
                            
                                        </ul>
                                    </div>
                                       
                                    </div>

                                </div>

                                

                            </div>
                        </div>
                    </div>
                  
                    

</div>

<div class="row">
    <div class="col-lg-12"> 
        <?php 
    $form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-ins-decision'
                    ]
                    ]
                ); 
?>
        <div class='row'>
            
            
            <div class='col-lg-12'>
                <div class="ibox-title">
                <h4>
                    <?= Yii::t('app','Liste des postulant en attente de validation'); ?>
                </h4>
             </div>
            </div>
            
        </div>
<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('app','Last name')?></th>
            <th><?= Yii::t('app','First name')?></th>
            <th><?= Yii::t('app','Gender')?></th>
            <th><?= Yii::t('app','No Inscription')?></th>
            <th><?= Yii::t('app','Program')?></th>
            <th><?= Yii::t('app','Shift')?></th>
            <th><?= Yii::t('app','Birthday'); ?></th>
            <th><?= Yii::t('app','Phone') ?></th>
            <th><?= Yii::t('app','Exam date'); ?></th>
            <th></th>
           
        </tr>
    </thead>
    <tbody>
        <?php 
            
            $i = 1;
            $postulant_data = Postulant::find()->where(['academic_year'=>$acad,'is_validation_expire'=>2])->orderBy(['last_name'=>SORT_ASC])->all(); 
            foreach($postulant_data as $p){
        
        ?>
        <tr>
            <td><?= $i; ?></td>
            <td>
                <a  data-idpos='<?= $p->id;?>' class='view-pos'><?= $p->last_name?>
                </a>
            </td>
            <td>
                <a  data-idpos='<?= $p->id;?>' class='view-pos'><?= $p->first_name?></a>
            </td>
            <td>
                <?php 
                if($p->gender == 1){
                    echo Yii::t('app','Female');
                }else{
                    echo Yii::t('app','Male');
                }
            ?>
            </td>
            <td>
                <?= $p->id; ?>
            </td>
            <td>
            <?php 
                if($p->apply_for_program!=NULL){
                    echo Program::findOne($p->apply_for_program)->short_name; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            <td>
                <?php 
                if($p->shift!=NULL){
                    echo Shifts::findOne($p->shift)->shift_name; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            
            <td>
                <?= 
                    Yii::$app->formatter->asDate($p->birthday); 
                ?>
            </td>
            
            <td><?= $p->phone; ?></td>
            <td>
                <?= 
                    Yii::$app->formatter->asDate($p->exam_date); 
                ?>
            </td>
             
            
            <td>
               <?php 
                echo Html::a('<span class="fa fa-trash"></span>', '../postulant/delete?id='.$p->id.'&wh=ins', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this postulant ?'),
                                    'data-method' => 'post',
                ]) ;
               ?>
            </td>
            
            
        </tr>
        
<?php
    $i++;
    }
    
?>
        
    </tbody>
    
</table>
<?php ActiveForm::end(); ?>
    </div>
</div>

<?php

$baseUrl = Yii::$app->request->BaseUrl;
$label_shift = [];
$key_shift = [];
$sum_by_shift = [];
$program_by_shift = [];
$data_pos = "";

$k = 0;
$pass = 0; 

$somme_program = Postulant::findBySql("SELECT p.short_name, count(*) as 'somme' FROM `postulant`  INNER JOIN program p ON (apply_for_program = p.id) where is_validation_expire IN (3) AND academic_year = $acad GROUP BY apply_for_program")->asArray()->all();
   

foreach($somme_program as $ps){
    if($pass==0)
               { 
                $data_pos =  '{ y: "'.$ps["short_name"].'", a:'.$ps["somme"].' }';
                $pass=1;
                 }
              else
                { 
                  $data_pos = $data_pos.', { y: "'.$ps["short_name"].'", a:'.$ps["somme"].' }';
                }
}

$src_txt = Yii::t('app','Search');

$script1 = <<< JS
    $(document).ready(function(){
        
        Morris.Bar({
        element: 'morris-bar-chart',
        data: [
                $data_pos
             ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Total postulant'],
        hideHover: 'auto',
        resize: true,
        barSize: 50,
        barColors: ["#EC4758", "#21B9BB"],
    });
        
    $('.view-pos').click(function(){
            var id_postulant = $(this).attr('data-idpos');
            $.post('$baseUrl/index.php/inscription/postulant/view-inscription',{id_stud:id_postulant},function(data){
                        $('#espas-postulant').html(data);
                });
            $('#espas-suite-dashboard').hide();
           
        });    

       
   
   $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'liste_des_inscrits'},
                    {extend: 'pdf', title: 'liste_des_inscrits'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });     
         
    });
    
       
JS;

 $this->registerJs($script1); 
 
?>
