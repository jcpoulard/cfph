<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\inscription\models\PostulantGrades; 
use app\modules\fi\models\Cities; 
use app\modules\fi\models\Program; 
use app\modules\fi\models\Shifts; 

/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\Postulant */

$this->title = $model->first_name.' '.$model->last_name.' '.Yii::t('app','subcribe to ').' '.Program::findOne($model->apply_for_program)->label.' '.Yii::t('app','in vacation').' '.Shifts::findOne($model->shift)->shift_name;

?>

<?= $this->render("//layouts/inscriptionLayout") ?>

<div class="postulant-view">

    <h2><b><?= Html::encode($this->title) ?></b></h2>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
    
    <div>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <td><b><?= Yii::t('app','No inscription'); ?></b> </td>
                <td><?= $model->no_inscription; ?></td>
                <td><b><?= Yii::t('app','Date inscription')?></b> </td>
                <td><?= Yii::$app->formatter->asDate($payment->payment_date); ?></td>
                <td><b><?= Yii::t('app','Exam date')?></b></td>
                <td><?= Yii::$app->formatter->asDate($model->exam_date); ?></td>
                <td><b><?= Yii::t('app','Exam entrance grade'); ?></b></td>
                <td><?php
                 if(PostulantGrades::find()->where(['postulant'=>$model->id])->one() != NULL){
                        echo PostulantGrades::find()->where(['postulant'=>$model->id])->one()->grade_value ;
                    }
                  ?>
                </td>
            </tr>
            <tr style="background: #1AB394; color: #FFFFFF">
                <td colspan="8">
                    <b><?= Yii::t('app','Basic info'); ?></b>
                </td>
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Last name')?></b></td>
                <td><?= $model->last_name; ?></td>
                <td><b><?= Yii::t('app','First name')?></b></td>
                <td><?= $model->first_name; ?></td>
                 <td><b><?= Yii::t('app','Gender')?></b></td>
                <td>
                        <?php 
                            if($model->gender == 1){
                                echo Yii::t('app','Female');
                            }else{
                                echo Yii::t('app','Male');
                            }
                        ?>
                </td>
                 <td><b><?= Yii::t('app','Birthday')?></b></td>
                <td><?= Yii::$app->formatter->asDate($model->birthday); ?></td>
            </tr>
            <tr>
                 <td><b><?= Yii::t('app','Place of birth')?></b></td>
                 <td><?php 
                        if(Cities::findOne($model->cities)!=NULL){
                            echo Cities::findOne($model->cities)->city_name;
                        }else {
                            echo ''; 
                        }
                        ?>
                 </td>
                 <td><b><?= Yii::t('app','Age')?></b></td>
                 <td><?= ageCalculator($model->birthday); ?></td>
                 <td><b><?= Yii::t('app','Address')?></b></td>
                 <td><?= $model->adresse; ?></td>
                 <td><b><?= Yii::t('app','Phone')?></b></td>
                <td><?= $model->phone; ?></td>
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Previous School'); ?></b></td>
                <td><?= $model->previous_school; ?></td>
                <td><b><?= Yii::t('app','Last class'); ?></b></td>
                <td><?= $model->last_class; ?></td>
                <td><b><?= Yii::t('app','Is Working'); ?></b></td>
                <td>
                    <?php 
                    if($model->is_working==1){
                        echo Yii::t('app','Yes');
                    }else{
                        echo Yii::t('app','No');
                    }
                    
                    ?>
                </td>
                <td><b><?= Yii::t('app','Work address'); ?></b></td>
                <td><?= $model->adress_working; ?></td>
            </tr>
            <tr style="background: #1AB394; color: #FFFFFF">
                <td colspan="8">
                    
                    <b><?= Yii::t('app','Parents Info');?></b>
                </td>
            </tr>
     
 <?php   if(isset($parent)&&($parent!=null))
                {
           ?>
            <tr>
                <td><b><?= Yii::t('app','Mother Name'); ?></b></td>
                <td><?= $parent->mother_name; ?></td>
                <td><b><?= Yii::t('app','Father Name'); ?></b></td>
                <td><?= $parent->father_name; ?></td>
                <td><b><?= Yii::t('app','Address In Haiti'); ?></b></td>
                <td><?= $parent->address_in_haiti; ?></td>
                <td><b><?= Yii::t('app','Phone'); ?></b></td>
                <td>
                    <?php
                    if($parent->phone_father!=NULL && $parent->phone_mother!=NULL){
                        echo $parent->phone_mother.' / '.$parent->phone_father; 
                    }elseif($parent->phone_father==NULL || $parent->phone_father=="" ){
                        echo $parent->phone_mother; 
                    }elseif($parent->phone_mother==NULL){
                        echo $parent->phone_father; 
                    }
                    ?>
                </td>
            </tr>
             <tr>
                <td><b><?= Yii::t('app','Address Foreign'); ?></b></td>
                <td><?= $parent->address_foreign; ?></td>
                <td><b><?= Yii::t('app','Occupation Mother'); ?></b></td>
                <td><?= $parent->occupation_mother; ?></td>
                <td><b><?= Yii::t('app','Occupation Mother Address'); ?></b></td>
                <td><?= $parent->occupation_mother_address; ?></td>
                <td><b><?= Yii::t('app','Occupation Mother Phone'); ?></b></td>
                <td><?= $parent->occupation_mother_phone; ?></td>
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Occupation Father'); ?></b></td>
                <td><?= $parent->occupation_father; ?></td>
                <td><b><?= Yii::t('app','Occupation Father Address'); ?></b></td>
                <td><?= $parent->occupation_father_address; ?></td>
                <td><b><?= Yii::t('app','Occupation Father Phone'); ?></b></td>
                <td><?= $parent->occupation_father_phone; ?></td>
            </tr>
       <?php     }
       
           ?>
            
            <tr style="background: #1AB394; color: #FFFFFF">
                <td colspan="8"><b><?= Yii::t('app','Liable Person');?></b></td>
            </tr>
    
 <?php      if(isset($liable)&&($liable!=null))
                { 
           ?>
            <tr>
                <td><b><?= Yii::t('app','Last Name'); ?></b></td>
                <td><?= $liable->last_name; ?></td>
                <td><b><?= Yii::t('app','First Name'); ?></b></td>
                <td><?= $liable->first_name; ?></td>
                 <td><b><?= Yii::t('app','Address'); ?></b></td>
                <td><?= $liable->address; ?></td>
                 <td><b><?= Yii::t('app','Phone'); ?></b></td>
                <td><?= $liable->phone; ?></td>
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Occupation'); ?></b></td>
                <td><?= $liable->occupation; ?></td>
                 <td><b><?= Yii::t('app','Occupation address'); ?></b></td>
                <td><?= $liable->occupation_address; ?></td>
                 <td><b><?= Yii::t('app','Occupation Phone'); ?></b></td>
                <td><?= $liable->occupation_phone; ?></td>
            </tr>
      <?php      }
            ?>
            <tr style="background: #1AB394; color: #FFFFFF">
                <td colspan="8">
                    <b><?= Yii::t('app','Heath state');?></b>
                </td>
            </tr>
     
 <?php  if(isset($health)&&($health!=null))
                { 
           ?>
            <tr>
                <td><b><?= Yii::t('app','Blood Group'); ?></b></td>
                <td><?= $health->blood_group; ?></td>
                <td><b><?= Yii::t('app','Is Permanent Decease'); ?></b></td>
                <td>
                    <?php 
                        $decease = $health->is_permanent_decease == 0 ? Yii::t('app','No'):Yii::t('app','Yes'); 
                        echo $decease; 
                    ?>
                </td>
                <td><b><?= Yii::t('app','Permanent Decease'); ?></b></td>
                <td colspan="3"><?= $health->permanent_decease; ?></td>
                
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Is Regulary Drug'); ?></b></td>
                <td>
                <?php
                $drug = $health->is_regulary_drug == 0 ? Yii::t('app','No'):Yii::t('app','Yes'); 
                        echo $drug;
                
                ?>
                </td>
                <td><b><?= Yii::t('app','Regulary Drug'); ?></b></td>
                <td colspan="5"><?= $health->regulary_drug; ?></td>
            </tr>
       <?php     }
            ?>
            <tr style="background: #1AB394; color: #FFFFFF">
                <td colspan="8"><b><?= Yii::t('app','Social Situation');?></b></td>
            </tr>
            
       <?php  if(isset($social)&&($social!=null))
                 {
            ?>
            <tr>
                <td><b><?= Yii::t('app','Civil State'); ?></b></td>
                <?php 
                    $civil = ['celibataire'=>Yii::t('app','Single'),'marie'=>Yii::t('app','Maried'),'divorce'=>Yii::t('app','Divorced'),'veuf'=>Yii::t('app','Widower')];
                    $yes_no = [1=>Yii::t('app','Yes'),0=>Yii::t('app','No')];
                    ?>
                <td><?= $civil[$social->civil_state] ?></td>
                <td><b><?= Yii::t('app','Is Leave Outside'); ?></b></td>
                <td><?= $yes_no[$social->is_leave_outside]; ?></td>
                <td><b><?= Yii::t('app','Foreign Country'); ?></b></td>
                <td colspan="3"><?= $social->foreign_country; ?></td>
            </tr>
            <tr>
                <td><b><?= Yii::t('app','Is Prison'); ?></b></td>
                <td><?= $yes_no[$social->is_prison]; ?></td>
                <td><b><?= Yii::t('app','Prison Place'); ?></b></td>
                <td><?= $social->prison_place; ?></td>
                <td><b><?= Yii::t('app','Is Religion'); ?></b></td>
                <td><?= $yes_no[$social->is_religion]; ?></td>
                <td><b><?= Yii::t('app','Religion'); ?></b></td>
                <td><?= $social->religion; ?></td>
            </tr>
      <?php      }
            ?>
        </table>
    </div>

</div>
<br/>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>