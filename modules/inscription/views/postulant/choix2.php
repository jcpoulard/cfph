<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\fi\models\Program;

$program = Program::findBySql("SELECT id, label FROM program  WHERE is_special = 0 AND id <> $id")->all(); 
?>
<div class='form-group'>
        <label><strong>Deuxi&egrave;me choix de programme </strong></label>
        <br/>
        <select name='Postulant[second_program]' id='choix2' class='form-control'>
            <option>Choisir un programme</option>
        <?php 
            foreach($program as $p){
                ?>

            <option value='<?= $p->id; ?>'> <?= $p->label; ?></option>

            <?php 
            }
            ?>
        </select>
</div> 
