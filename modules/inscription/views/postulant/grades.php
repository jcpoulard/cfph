<?php

use yii\helpers\Html;

use app\modules\inscription\models\SrcPostulant;



/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */


   $this->title = Yii::t('app', 'Create Grades');


?>

     
   
<div class="wrapper wrapper-content grades-create">

    <?php
          
                echo $this->render('_grade', [
                 'model' => $model,
                 'dataProvider'=>$dataProvider, 
                    'is_save'=>$is_save,
                 
             ]); 
          
                  
         
          
        ?>

</div>
