<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\SrcPostulant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="postulant-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'first_name') ?>

    <?= $form->field($model, 'last_name') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'birthday') ?>

    <?php // echo $form->field($model, 'cities') ?>

    <?php // echo $form->field($model, 'adresse') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'is_working') ?>

    <?php // echo $form->field($model, 'name_working') ?>

    <?php // echo $form->field($model, 'adress_working') ?>

    <?php // echo $form->field($model, 'apply_for_program') ?>

    <?php // echo $form->field($model, 'last_class') ?>

    <?php // echo $form->field($model, 'previous_school') ?>

    <?php // echo $form->field($model, 'school_date_entry') ?>

    <?php // echo $form->field($model, 'last_average') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'academic_year') ?>

    <?php // echo $form->field($model, 'cohorte') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_updated') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
