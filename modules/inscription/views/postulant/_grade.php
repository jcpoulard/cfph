<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\inscription\models\PostulantGrades;
//use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;

use app\modules\fi\models\SrcShifts;

use app\modules\rbac\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
$acad = Yii::$app->session['currentId_academic_year'];

Yii::$app->session['tab_index']=0;
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<?php 
					

 $modelShift_ = new SrcShifts();
 
 

	 $item_array_1= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->first_name;
							 }
						  ],

                                       [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->last_name;
							 }
						  ],
						  
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	     
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	      $grade_value ='';
						 	      
						 	           $postulGrade_model = PostulantGrades::find()->where(['postulant'=>$data->id,'program'=>$data->apply_for_program,'academic_year'=>$data->academic_year])->one();       
                                                                  
                                                                   if($postulGrade_model!=null)
						 	          { 
                                                                     
                                                                          $grade_value = $postulGrade_model->grade_value;
						 	          }
						 	       
						 	       //$grade_value = $data->grade_value;
						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'"  type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
	  
	  
	  
   
  
?>

<?= $this->render("//layouts/inscriptionLayoutGrades") ?>
<?php 
    $form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-ins-grades'
                    ]
                    ]
                ); 
?>
<div class="row">
    <div class="col-lg-1">
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['exam?wh=exam'], ['class' => 'btn btn-info btn-sm']) ?>
    </div>
    <div class="col-lg-8">
         <h3><?= $this->title; ?></h3>
     </div>
     
</div>

<div class="row">
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($model, 'apply_for_program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcProgram::findAll(['is_special'=>0]),'id','short_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select apply program  --'),
                                'onchange'=>'submit()',
                       ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Apply for program')); 
            ?>
        </div>
    </div>
    
 <?php
    if($model->apply_for_program!='')
    {
 ?>   
    <div class="col-lg-2">
        <div class="form-group" >
             <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                    'data'=>ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ),
                   'size' => Select2::MEDIUM,
                   'theme' => Select2::THEME_CLASSIC,
                   'language'=>'fr',
                   'options'=>['placeholder'=>Yii::t('app', ' --  select apply shift  --'),
                                'onchange'=>'submit()',
                       ],
                   'pluginOptions'=>[
                         'allowclear'=>true,
                     ],
                   ])->label(Yii::t('app','Shift')); 
            ?>
        </div>
    </div>
<?php
    }
?>    
</div>



    
<ul class="nav nav-tabs"></ul>  
<br/>  
<?php
							
										
										
	        	
?>
<br/> 


<?php


if($dataProvider->getModels()!=null)
{
  


 
 echo GridView::widget([
        'id'=>'list-postulant',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array_1, 
                          
       ]);

    
   }

?>

<?php

if( ($dataProvider->getModels()!=null)&&($is_save==0) )
 {
?>   
<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?php
           
        
           echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
            
                                              
        ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
       
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

 <?php
 
   }

  
 ?>
    <?php ActiveForm::end(); ?>


<br/><br/><br/><br/><br/>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    
        // e.keyCode = 37(left), 38(up), 39(right), 40(down)
        
  $('input').keypress(function(e){
   var key=e.keyCode || e.which;
       
   //alert("@-"+key);
        if( (key==13)||(key==40) ){ 
        var x = 1;
         var ind = $(this).attr("tabindex");  
          var next_ind =0;
             next_ind = Number(ind) + Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
        else if(key==38){  //up
        
            var x = 1;
            var ind = $(this).attr("tabindex");  
            var next_ind =0;
             next_ind = Number(ind) - Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
    });  		
				
	

JS;
$this->registerJs($script);

?> 



		   

