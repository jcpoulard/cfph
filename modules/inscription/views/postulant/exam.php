<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\fi\models\Program;
use app\modules\fi\models\Shifts; 
use app\modules\inscription\models\PostulantIncome;
use app\modules\inscription\models\PostulantGrades; 
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\inscription\models\SrcPostulant */
/* @var $dataProvider yii\data\ActiveDataProvider */


 $this->title = Yii::t('app', 'List of postulant');
?>

<?= $this->render("//layouts/inscriptionLayoutGrades") ?>

<div class="row">
     
    <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['grades?wh=exam', ], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Create')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div>
<div class="row">
    <div class="col-lg-12"> 
        
<?php 
    $form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-ins-decision'
                    ]
                    ]
                ); 
?>
<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('app','Last name')?></th>
            <th><?= Yii::t('app','First name')?></th>
            <th><?= Yii::t('app','Gender')?></th>
            <th><?= Yii::t('app','Program')?></th>
            <th><?= Yii::t('app','Shift')?></th>
            <th><?= Yii::t('app','Grades'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php 
            
            $i = 1;
            $postulant_data = $dataProvider->getModels();   //$model::find()->orderBy(['last_name'=>SORT_ASC])->all(); 
            foreach($postulant_data as $p){
        
        ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= $p->last_name?></td>
            <td><?= $p->first_name?></td>
            <td>
                <?php 
                if($p->gender == 1){
                    echo Yii::t('app','Female');
                }else{
                    echo Yii::t('app','Male');
                }
            ?>
            </td>
            <td>
            <?php 
                if($p->apply_for_program!=NULL){
                    echo Program::findOne($p->apply_for_program)->short_name; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            <td>
                <?php 
                if($p->shift!=NULL){
                    echo Shifts::findOne($p->shift)->shift_name; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            
            
            <td>
                <?php 
                    if(PostulantGrades::find()->where(['postulant'=>$p->id])->one() == NULL){
                ?>
                <a href="#" class="grade_value" data-name="<?= $p->apply_for_program; ?>"  data-type="text" data-pk="<?= $p->id ?>"  data-url="set-grade" data-title="<?= Yii::t('app','Add grade') ?>">
                    <i class="fa fa-plus"></i>
                </a>
               
                <?php 
                    }else{
                ?>
                 <a href="#" class="grade_value" data-name="<?= $p->apply_for_program; ?>" data-type="text" data-pk="<?= PostulantGrades::find()->where(['postulant'=>$p->id])->one()->id ?>"  data-url="set-grade?is=up" data-title="<?= Yii::t('app','Update grades') ?>">
                     <?= PostulantGrades::find()->where(['postulant'=>$p->id])->one()->grade_value ?>
                 </a>
                <?php } ?>
                
                
            </td>
            
            
        </tr>
        
<?php
    $i++;
    }
    
?>
        
    </tbody>
    
</table>
<?php ActiveForm::end(); ?>
    </div>
</div>

<?php

    $src_txt = Yii::t('app','Search');
    $script = <<< JS
    $(document).ready(function(){
             $('.grade_value').editable();
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'liste_des_inscrits'},
                    {extend: 'pdf', title: 'liste_des_inscrits'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);


?>
