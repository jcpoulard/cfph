<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\fi\models\Program;
use app\modules\fi\models\Shifts; 
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;

 $this->title = Yii::t('app', 'Liste des inscriptions par programme et par date d\'examen');
?>

<?= $this->render("//layouts/inscriptionLayoutRapport") ?>

<?php 
    $form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-report'
                    ]
                    ]
                ); 
?>

<div class="row">
    
    <div class="col-lg-8">
         <h3><?= $this->title; ?></h3>
     </div>
     
</div>
<div class="row">
    <div class="col-lg-2">
        <div class="form-group">
            <?= $form->field($model, 'apply_for_program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Program::findAll(['is_special'=>0]),'id','short_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select apply program  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Apply for program')); 
            ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group" id="shift_content">
             <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                    'data'=>ArrayHelper::map(Shifts::find()->all(),'id','shift_name' ),
                   'size' => Select2::MEDIUM,
                   'theme' => Select2::THEME_CLASSIC,
                   'language'=>'fr',
                   'options'=>['placeholder'=>Yii::t('app', ' --  select apply shift  --')],
                   'pluginOptions'=>[
                         'allowclear'=>true,
                     ],
                   ])->label(Yii::t('app','Shift')); 
            ?>
        </div>
    </div>
    <div class="col-lg-2">
        <div class="form-group" id="date_exam">
            <?= $form->field($model, 'exam_date')->widget(DatePicker::classname(), [
                        'options'=>['placeholder'=>'' ],
                        'language'=>'fr',
                        'pluginOptions'=>[
                             'autoclose'=>true,
                            'format'=>'yyyy-mm-dd',
                            ],

                        ] )->label(Yii::t('app','Exam Date'));


                ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>


<div class="row">
    <div class="col-lg-12"> 
        <div id="data_report">
    
        </div>

    </div>
</div>



<?php
    $src_txt = Yii::t('app','Search');
    $script = <<< JS
    $(document).ready(function(){
            
            $('#shift_content').hide(); 
            $('#date_exam').hide(); 
            $('#postulant-apply_for_program').change(function(){
                   $('#shift_content').show();
                   
                });
            
            $('#postulant-shift').change(function(){
                $('#date_exam').show(); 
                
            });
            
            $('#postulant-exam_date').change(function(){
                var id_shift = $('#postulant-shift').val(); 
                var id_program = $('#postulant-apply_for_program').val();
                var exam_date = $('#postulant-exam_date').val();
            //alert(exam_date);
                 $.get('report-data',{shift : id_shift, program : id_program,exam_date:exam_date},function(data){
                        $('#data_report').html(data);
                    });
            });
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   // search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'liste_des_inscrits'},
                    {extend: 'pdf', title: 'list_des_inscrits'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
