<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use app\modules\fi\models\Cities;
use app\modules\fi\models\Program;
use app\modules\fi\models\Shifts;
use app\modules\billings\models\SrcPaymentMethod; 
use app\modules\fi\models\Relations; 
/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\Postulant */
/* @var $form yii\widgets\ActiveForm */
?>




<div class="inscription-form">
<div class="col-lg-12">
         <?php 
         $program = Program::findBySql("SELECT id, label FROM program  WHERE is_special = 0")->all(); 
            $form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-inscription'
                    ]
                    ]
                ); 
            echo $form->errorSummary($model); 
            ?>
      <div class="tabs-container">
                  
                        
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="row">
                                    <div class="col-lg-6">
                                       <div class="form-group">
                                         <?= $form->field($model,'first_name')->textInput(['maxlength' => true]); ?>
                                       </div>
                                       <div class="form-group">
                                        <?= $form->field($model,'last_name')->textInput(['maxlength' => true]); ?>
                                        </div>
                                        <div class="form-group">
                                       <?= $form->field($model, 'gender')->widget(Select2::classname(), [
                                               'data'=>gender_array(),
                                               'size' => Select2::MEDIUM,
                                               'theme' => Select2::THEME_CLASSIC,
                                               'language'=>'fr',
                                               'options'=>['placeholder'=>Yii::t('app', ' --  select gender  --')],
                                               'pluginOptions'=>[
                                                     'allowclear'=>true,
                                                 ],
                                               ])->label(Yii::t('app','Gender')); 
                                        ?>
                                        </div>
                                        
                                        <div class="form-group">
                                            <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                                                'data'=>ArrayHelper::map(Shifts::find()->all(),'id','shift_name' ),
                                               'size' => Select2::MEDIUM,
                                               'theme' => Select2::THEME_CLASSIC,
                                               'language'=>'fr',
                                               'options'=>['placeholder'=>Yii::t('app', ' --  select apply shift  --')],
                                               'pluginOptions'=>[
                                                     'allowclear'=>true,
                                                 ],
                                               ])->label(Yii::t('app','Shift')); 
                                            ?>
                                        </div>
                                        <div class="form-group">
                                          <?php
                                            echo $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                                            'options'=>['placeholder'=>'' ],
                                            'language'=>'fr',
                                            'pluginOptions'=>[
                                                 'autoclose'=>true,
                                                'format'=>'yyyy-mm-dd',
                                                ],

                                                ] );
                                              ?>
                                         </div>
                                        
                                        <div class="form-group">
                                            <?= $form->field($model,'adresse')->textInput(); ?>
                                        </div> 
                                        <div class="form-group">
                                          <?php
                                            echo $form->field($model, 'exam_date')->widget(DatePicker::classname(), [
                                            'options'=>['placeholder'=>'' ],
                                            'language'=>'fr',
                                            'pluginOptions'=>[
                                                 'autoclose'=>true,
                                                'format'=>'yyyy-mm-dd',
                                                ],

                                                ] );
                                              ?>
                                         </div>
                                        
                                        <div class="form-group">
                                            <?= $form->field($model,'phone')->textInput()->label(Yii::t('app','Phone')); ?>
                                        </div> 
                                      <div class='form-group'>
                                            <label><strong>Premier choix de programme </strong></label>
                                            <br/>
                                            <select name='Postulant[apply_for_program]' id='choix1' class='form-control'>
                                                <option>Choisir un programme</option>
                                            <?php 
                                                foreach($program as $p){
                                                    ?>

                                                <option value='<?= $p->id; ?>'> <?= $p->label; ?></option>

                                                <?php 
                                                }
                                                ?>
                                            </select>
                                      </div>   
                                     
                                        <div id="espas-choix2">

                                        </div>    
                 </div>
                                <div class="col-lg-6">
                                    
                                        
                                    <div class="form-group">
                                            <?= $form->field($model,'previous_school')->textInput()->label(Yii::t('app','Previous school')); ?>
                                    </div>  
                                    <div class="form-group">
                                            <?= $form->field($model,'email')->textInput()->label(Yii::t('app','Email')); ?>
                                    </div>
                                    <div class="form-group">
                                       <label></label>
                                        <?= $form->field($model,'is_working')->checkbox(['label'=>Yii::t('app','Is Working')]); ?>
                                    </div>
                                    <div class="form-group">
                                    <?= $form->field($liable,'last_name')->textInput()->label(Yii::t('app','Liable person last name')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($liable,'first_name')->textInput()->label(Yii::t('app','Liable person first name')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($liable,'phone')->textInput()->label(Yii::t('app','Liable person phone')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($liable,'address')->textInput()->label(Yii::t('app','Liable person address')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($liable,'email_responsable')->textInput()->label(Yii::t('app','Liable person email')); ?>
                                    </div>
                                    <div class="form-group">
                                            <?= $form->field($liable, 'relation_responsable')->widget(Select2::classname(), [
                                                'data'=>ArrayHelper::map(Relations::find()->all(),'id','relation_name' ),
                                               'size' => Select2::MEDIUM,
                                               'theme' => Select2::THEME_CLASSIC,
                                               'language'=>'fr',
                                               'options'=>['placeholder'=>Yii::t('app', ' --  select relation  --')],
                                               'pluginOptions'=>[
                                                     'allowclear'=>true,
                                                 ],
                                               ])->label(Yii::t('app','Liable person relation')); 
                                            ?>
                                     </div>
                                    
                                </div>        
                    </div>
                                    
                    <div class="row">
                            <div class="col-lg-5">
                             </div>
                            <div class="col-lg-2">
                                <button class="btn btn-success" id="finish" name="finish"><?= Yii::t('app','Finish and Save') ?></button>
                            </div>
                            <div class="col-lg-5">
                            </div>
                    </div>                
                                    
                                    
                                </div>
                            </div>
                   </div>
     </div>
    <?php 
    for($i=0;$i<10;$i++){
        echo '<br/>';
    }
?>
    <?php ActiveForm::end(); ?>
</div>
    
</div>



<?php 
$baseUrl = Yii::$app->request->BaseUrl;

$script1 = <<< JS
     $(document).ready(function(){
        
        // Choix des programmes 
        $('#choix1').click(function(){
        var choix1 = $('#choix1').val();
                $.get('$baseUrl/index.php/inscription/postulant/make-choix',{id:choix1},function(data){
                            $('#espas-choix2').html(data);
                    });
                   
         }); 
        
       // $('#class_autres').hide();
        $('#postulant-name_working').attr('disabled','disabled');
        $('#postulant-adress_working').attr('disabled','disabled');
        $('#postulant-work_phone').attr('disabled','disabled');
        $('#postulanthealth-permanent_decease').attr('disabled','disabled');
        $('#postulanthealth-regulary_drug').attr('disabled','disabled');
        $('#postulantsocial-foreign_country').attr('disabled','disabled');
        $('#postulantsocial-prison_place').attr('disabled','disabled');
        $('#postulantsocial-religion').attr('disabled','disabled');
        
        $('#postulanthealth-is_permanent_decease').change(function(){
                var is_permanent_decease = $('#postulanthealth-is_permanent_decease').val(); 
                if(is_permanent_decease==1){
                    $('#postulanthealth-permanent_decease').attr('disabled',false);
                    }else{
                         $('#postulanthealth-permanent_decease').attr('disabled',true);
                        }     
            });
        
        $('#postulanthealth-is_regulary_drug').change(function(){
                var is_regulary_drug = $('#postulanthealth-is_regulary_drug').val(); 
                if(is_regulary_drug==1){
                    $('#postulanthealth-regulary_drug').attr('disabled',false);
                    }else{
                         $('#postulanthealth-regulary_drug').attr('disabled',true);
                        }     
            });
        
        $('#last_class').change(function(){
            var last_class = $('#last_class').val(); 
            if(last_class=='autres'){
                  $('#class_list').hide();  
                  $('#class_autres').show();
                  $('#last_class').focus();
                }
            });
        
        $('#postulant-is_working').click(function(){
                $('#postulant-name_working').attr('disabled',!this.checked);
                $('#postulant-adress_working').attr('disabled',!this.checked);
                $('#postulant-work_phone').attr('disabled',!this.checked);
                
            });
        
        $('#postulantsocial-is_leave_outside').click(function(){
                $('#postulantsocial-foreign_country').attr('disabled',!this.checked);
            });
        
        $('#postulantsocial-is_prison').click(function(){
                $('#postulantsocial-prison_place').attr('disabled',!this.checked);
            });
        
        $('#postulantsocial-is_religion').click(function(){
                $('#postulantsocial-religion').attr('disabled',!this.checked);
            });
        $('#next1').click(function(){
                $('#l2').addClass('active');
                $('#l1').removeClass('active');
           });
        
        $('#next2').click(function(){
                $('#l3').addClass('active');
                $('#l2').removeClass('active');
           });
        
        $('#next3').click(function(){
                $('#l4').addClass('active');
                $('#l3').removeClass('active');
           });
        
        $('#next4').click(function(){
                $('#l5').addClass('active');
                $('#l4').removeClass('active');
           });
        $('#next5').click(function(){
                $('#l6').addClass('active');
                $('#l5').removeClass('active');
           });
        
        $('#previous1').click(function(){
                $('#l1').addClass('active');
                $('#l2').removeClass('active');
           });
        
        $('#previous2').click(function(){
                $('#l2').addClass('active');
                $('#l3').removeClass('active');
           });
        
        $('#previous3').click(function(){
                $('#l3').addClass('active');
                $('#l4').removeClass('active');
           });
        $('#previous4').click(function(){
                $('#l4').addClass('active');
                $('#l5').removeClass('active');
           });
        $('#previous5').click(function(){
                $('#l5').addClass('active');
                $('#l6').removeClass('active');
           });
        
       });
JS;
$this->registerJs($script1);
 
?>


