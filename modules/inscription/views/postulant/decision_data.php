<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\fi\models\Program;
use app\modules\fi\models\Shifts; 
use app\modules\inscription\models\PostulantIncome;
use app\modules\inscription\models\Postulant;
use app\modules\inscription\models\SrcPostulant;
use app\modules\inscription\models\PostulantGrades;
use yii\helpers\ArrayHelper;

$form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-list-decision'
                    ]
                    ]
                ); 

 $status_list = [0=>Yii::t('app','Pending'),1=>Yii::t('app','Admitted'),2=>Yii::t('app','Non admitted')]; 


?>

<div class="table-responsive">
    <br/>
<table class="table table-striped table-bordered table-hover dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('app','Last name')?></th>
            <th><?= Yii::t('app','First name')?></th>
            <th><?= Yii::t('app','Gender')?></th>
            <th><?= Yii::t('app','Program')?></th>
            <th><?= Yii::t('app','Shift')?></th>
            <th><?= Yii::t('app','Grades')?></th>
            <th><?= Yii::t('app','Mention')?></th>
            
        </tr>
    </thead>
    <tbody>
        <?php 
            
            $i = 1;
            $date_start = infoCmsConfig("date_debut_inscription");
            $searchModel = new SrcPostulant();
            $dataProvider = $searchModel->searchByDateStartForDecision($date_start,$program,$shift);
            $postulant_data = $dataProvider->getModels();
            foreach($postulant_data as $p){
        
        ?>
        <tr>
            <td><?= $i; ?></td>
            <td><a href="view?id=<?= $p->id ?>&wh=ins"><?= $p->last_name?></a></td>
            <td><a href="view?id=<?= $p->id ?>&wh=ins"><?= $p->first_name?></a></td>
            <td>
                <?php 
                if($p->gender == 1){
                    echo Yii::t('app','Female');
                }else{
                    echo Yii::t('app','Male');
                }
            ?>
            </td>
            <td>
            <?php 
                if($p->apply_for_program!=NULL){
                    echo Program::findOne($p->apply_for_program)->label; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            <td>
                <?php 
                if($p->shift!=NULL){
                    echo Shifts::findOne($p->shift)->shift_name; 
                }else
                {
                    echo ""; 
                }
            ?>
            </td>
            <td>
                <?= PostulantGrades::find()->where(['postulant'=>$p->id])->one()->grade_value ?>
            </td>
            <td>
                <a href="#" class="estati" data-name="<?= $p->apply_for_program; ?>" data-type="select" data-pk="<?= $p->id; ?>"  data-url="set-status" data-original-title="<?= Yii::t('app','Update decision');?>" title="">
                     <?= $status_list[$p->status]; ?>
                 </a>
            </td>
            
            
        </tr>
        
<?php
    $i++;
    }
    
?>
    
    </tbody>
    
</table>
    <input name="row_total" type="hidden" value="<?= $i; ?>"/>   
</div>

<?php ActiveForm::end(); ?>

<?php
    $src_txt = Yii::t('app','Search');
    $pending = Yii::t('app','Pending');
    $admitted = Yii::t('app','Admitted');
    $non_admitted = Yii::t('app','Non admitted');
    $script = <<< JS
    $(document).ready(function(){
            var source = [{'value': 0, 'text': "$pending"}, {'value': 1, 'text': "$admitted"},{'value': 2,'text': "$non_admitted"}];
            
            $('.estati').editable(
                {
                //'mode'  : 'inline',
                showbuttons: false,
                'source': function() {
                    return source;
                },
            }
            );
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   // search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'liste_des_inscrits'},
                    {extend: 'pdf', title: 'list_des_inscrits'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
