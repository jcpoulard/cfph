<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\inscription\models\Postulant; 
$count_postulant_attente = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire = 2")->count(); 
$count_postulant_all = Postulant::find()->count();
$count_postulant_valide = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire = 3")->count(); 
$count_postulant_admis = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire = 3 AND status = 1")->count(); 
$count_postulant_pending = Postulant::findBySql("SELECT id FROM postulant WHERE is_validation_expire = 3 AND status = 0")->count(); 
?>
<div class="row" tyle="padding: 8px;">
    <div class="col-lg-3 col-xs-6 ti-bwat">
         <!-- small box -->
          <div class="small-box bg-blue" data-html="true" rel="tooltip" href="#" title="">
            <div class="inner">
                <h3>
                     <a href="#">
                    <span class="teks-blan"><?= $count_postulant_attente; ?></span> <span class="piti-ekriti teks-blan"><?= Yii::t('app','Postulant waiting for validation')?></span>
                     </a>
                </h3>
                        <h4>
                           
                            <span class="percent teks-blan">
                                    <?php 
                                   if($count_postulant_all > 0){
                                       $percent = ($count_postulant_attente/$count_postulant_all)*100;
                                       echo round($percent,2).'%';
                                   }else{
                                       echo "0%";
                                   }
                                    ?>
                            </span>
                           
                        </h4>
              
                
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
         
          </div>
    </div>
    <div class="col-lg-3 col-xs-6 ti-bwat">
         <!-- small box -->
          <div class="small-box bg-yellow" data-html="true" rel="tooltip" href="#" title="">
            <div class="inner">
                <h3><span class="teks-blan"><?= $count_postulant_valide; ?></span> <span class="piti-ekriti teks-blan"><?= Yii::t('app','Inscriptions valid&eacute;es')?></span></h3>
                        <h4>
                            <span class="percent teks-blan">
                                    <?php 
                                   if($count_postulant_all > 0){
                                       $percent = ($count_postulant_valide/$count_postulant_all)*100;
                                       echo round($percent,2).'%';
                                   }else{
                                       echo "0%";
                                   }
                                    ?>
                            </span>
                        </h4>
              
                
            </div>
            <div class="icon">
              <i class="fa fa-thumbs-up"></i>
            </div>
         
          </div>
    </div>
    <div class="col-lg-3 col-xs-6 ti-bwat">
         <!-- small box -->
          <div class="small-box bg-green" data-html="true" rel="tooltip" href="#" title="">
            <div class="inner">
                <h3><span class="teks-blan"><?= $count_postulant_admis; ?></span> <span class="piti-ekriti teks-blan"><?= Yii::t('app','Postulants Admis')?></span></h3>
                        <h4>
                            <span class="percent teks-blan">
                                    <?php 
                                   if($count_postulant_all > 0){
                                       $percent = ($count_postulant_admis/$count_postulant_all)*100;
                                       echo round($percent,2).'%';
                                   }else{
                                       echo "0%";
                                   }
                                    ?>
                            </span>
                        </h4>
              
                
            </div>
            <div class="icon">
              <i class="fa fa-graduation-cap"></i>
            </div>
         
          </div>
    </div>
    <div class="col-lg-3 col-xs-6 ti-bwat">
         <!-- small box -->
          <div class="small-box bg-red" data-html="true" rel="tooltip" href="#" title="">
            <div class="inner">
                <h3><span class="teks-blan"><?= $count_postulant_pending; ?></span> <span class="piti-ekriti teks-blan"><?= Yii::t('app','Postulants Pending')?></span></h3>
                        <h4>
                            <span class="percent teks-blan">
                                    <?php 
                                   if($count_postulant_all > 0){
                                       $percent = ($count_postulant_pending/$count_postulant_all)*100;
                                       echo round($percent,2).'%';
                                   }else{
                                       echo "0%";
                                   }
                                    ?>
                            </span>
                        </h4>
              
                
            </div>
            <div class="icon">
              <i class="fas fa-chalkboard-teacher"></i>
            </div>
         
          </div>
    </div>
</div>
