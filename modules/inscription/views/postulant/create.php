<?php 
    use yii\helpers\Html;
    $this->title = Yii::t('app', 'Create postulant');
?>

<?php // $this->render("//layouts/inscriptionLayout") ?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['../inscription/postulant/create','wh'=>'ap'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<?php
        echo $this->render('_form', [
        'model' => $model,
        'liable'=>$liable,
        'payment'=>$payment,
    ]) ?>

