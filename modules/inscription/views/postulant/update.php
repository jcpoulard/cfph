<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\Postulant */

$this->title = Yii::t('app', 'Update {postulant}',['postulant'=>$model->first_name.' '.$model->last_name]);


?>
<?= $this->render("//layouts/inscriptionLayout") ?>


    
    <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['../inscription/postulant/create','wh'=>'ap'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 
<div class="postulant-update">
<?= $this->render('_form', [
        'model' => $model,
        'parent'=>$parent, 
        'liable'=>$liable,
        'health'=>$health,
        'social'=>$social,
        'payment'=>$payment,
    ]) ?>

</div>
