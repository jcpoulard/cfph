<?php
use app\modules\inscription\models\Postulant; 
use app\modules\inscription\models\PostulantLiablePerson; 
use app\modules\fi\models\Shifts; 
use app\modules\fi\models\Program;
use app\modules\fi\models\ProgramsLeaders;
use app\modules\fi\models\Persons;
use yii\widgets\ActiveForm;



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// echo $id;
setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
$postulant = Postulant::findOne($id); 
$responsable = PostulantLiablePerson::findOne($responsable_id);
$all_program = Program::findAll(['is_special'=>0]);
$this->title = "Formulaire d'inscription de $postulant->first_name ".strtoupper($postulant->last_name);
$baseUrl = Yii::$app->request->BaseUrl;
$count_stud = Postulant::findBySql("SELECT * FROM postulant WHERE is_validation_expire = 3")->count();

$form = ActiveForm::begin(
                [
                    'options' => [
                        'id' => 'form-val-insc'
                    ]
                    ]
                ); 
?>


<div class='col-lg-1'>
</div>
<div class='col-lg-10'>
    
    
    
    
    <div id='espas-msg-valide'>
        
    </div>
<table id="w0" class="table table-striped table-bordered detail-view table-responsive">
    <tbody>
        <tr>
            <th><?= Yii::t('app','First name')?></th>
            <td>
             <a class='zonteks' data-type='text' data-name="prenom" data-pk="<?= $postulant->id;?>" data-url="update-inscription">    
            <?= $postulant->first_name; ?>
             </a>
             </td>
            <th><?= Yii::t('app','Last name')?></th>
            <td>
                <a class='zonteks' data-type='text' data-name="nom" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?= $postulant->last_name; ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Gender');?></th>
            <td>
                <a class='zonteks' data-type='select' data-name="sexe" data-pk="<?= $postulant->id;?>" data-url="update-inscription" data-source="list-sexe">
                    <?= $postulant->sexe; ?>
                 </a>
            </td>
            <th><?=  Yii::t('app','Birthday');?></th>
            <td>
                <a class='zonteks' id="date-naissance" data-type="date" data-name="birthday" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?php    Yii::$app->formatter->locale = 'fr-FR';?>
                    <?= Yii::$app->formatter->asDate($postulant->birthday); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Shift');?></th>
            <td>
                <a class="zonteks" data-type="select" data-name="vacation" data-pk="<?= $postulant->id;?>" data-url="update-inscription"  data-source="list-shift">
                    <?= $postulant->shift0->shift_name; ?>
                </a>
            </td>
            <th><?= Yii::t('app','Program first choice');?></th>
            <td>
                <a class="zonteks" data-type="select" data-name="choix1" data-pk="<?= $postulant->id;?>" data-url="update-inscription" data-source="list-program">
                    <?= $postulant->applyForProgram->label; ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Program Second choice');?></th>
            <td>
                <a class="zonteks" data-type="select" data-name="choix2" data-pk="<?= $postulant->id;?>" data-url="update-inscription" data-source="list-program">
                    <?php
                    if(isset($postulant->programSecond->label)){
                        echo $postulant->programSecond->label; 
                    } else{
                        echo "";
                    }
                    ?>
                </a>
            </td>
            <th><?= Yii::t('app','Address in Port-au-Prince') ?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="adresse" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?= $postulant->adresse; ?>
                 </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Phone');?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="phone" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?= $postulant->phone; ?>
                </a>
            </td>
            <th><?= Yii::t('app','Last school');?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="school" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?= $postulant->previous_school; ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Are you working ?')?></th>
            <td>
                <a  class="zonteks" data-type="select" data-name="working" data-pk="<?= $postulant->id;?>" data-url="update-inscription" data-source="list-bool">
                    <?php if($postulant->is_working==1){echo "Oui"; }else{ echo "Non"; } ?>
                </a>
            </td>
            <th><?= Yii::t('app','Email');?></th>
            <td>
                <a  class="zonteks" data-type="text" data-name="email" data-pk="<?= $postulant->id;?>" data-url="update-inscription" >
                    <?= $postulant->email ?>
                </a>
            </td>
        </tr>
        
        <tr>
            <th><?= Yii::t('app','Liable person first name') ?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="responsable_prenom" data-pk="<?= $responsable->id;?>" data-url="update-inscription">
                    <?= $responsable->first_name; ?>
                </a>
            </td>
            <th><?= Yii::t('app','Liable person last name') ?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="responsable_nom" data-pk="<?= $responsable->id;?>" data-url="update-inscription">
                    <?= strtoupper($responsable->last_name); ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Liable person address')?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="responsable_adresse" data-pk="<?= $responsable->id;?>" data-url="update-inscription">
                    <?= $responsable->address; ?>
                </a>
            </td>
            <th><?= Yii::t('app','Liable person phone');?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="responsable_phone" data-pk="<?= $responsable->id;?>" data-url="update-inscription">
                    <?= $responsable->phone; ?>
                </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Liable person email');?></th>
            <td>
                <a class="zonteks" data-type="text" data-name="responsable_email" data-pk="<?= $responsable->id;?>" data-url="update-inscription">
                    <?= $responsable->email_responsable; ?>
                </a>
            </td>
            <th><?= Yii::t('app','Relation liable person')?></th>
            <td>
                <a class="zonteks" data-type="select" data-name="responsable_relation" data-pk="<?= $responsable->id;?>" data-url="update-inscription" data-source="list-relation">
                    <?php
                    if(isset($responsable->relation0->relation_name)){
                        echo $responsable->relation0->relation_name;
                    } else{
                        echo "";
                    }
                    
                    ?>
                 </a>
            </td>
        </tr>
        <tr>
            <th><?= Yii::t('app','Exam Date');?></th>
            <td>
                <a class='zonteks' id="date-naissance" data-type="date" data-name="exam_date" data-pk="<?= $postulant->id;?>" data-url="update-inscription">
                    <?php    Yii::$app->formatter->locale = 'fr-FR';?>
                    <?= Yii::$app->formatter->asDate($postulant->exam_date); ?>
                </a>
            </td>
        </tr>
    </tbody>
</table>
<?php if($postulant->is_validation_expire==2){ ?>
        <div class='col-xs-3'>
            <div class='input-group'>
                <input type='text' class='form-control' id='date-examen' placeholder="<?= Yii::t('app','Exam date'); ?>">
            </div> 
        </div>
        <a class='btn btn-google' id='valider-inscription' data-idpostulant='<?= $postulant->id;?>'> <?= Yii::t('app','Validate subscription for {name} subscription number {number}',['name'=>$postulant->first_name.' '.$postulant->last_name,'number'=>$postulant->id])?>
        </a>
        <a class='btn btn-primary' id='print-inscription' data-idpostulant='<?= $postulant->id;?>' href="<?= $baseUrl;?>/index.php/inscription/postulant/print-form?id=<?=$postulant->id; ?>" target="_new"> 
            <?= Yii::t('app','Print subscription for {name} subscription number {number}',['name'=>$postulant->first_name.' '.$postulant->last_name,'number'=>$postulant->id])?>
        </a>
        
    <?php }elseif($postulant->is_validation_expire == 3){ ?>
    <a class='btn btn-primary' id='print-inscription-2' data-idpostulant='<?= $postulant->id;?>' href="<?= $baseUrl;?>/index.php/inscription/postulant/print-form?id=<?=$postulant->id; ?>" target="_new"> 
        <?= Yii::t('app','Print subscription for {name} subscription number {number}',['name'=>$postulant->first_name.' '.$postulant->last_name,'number'=>$postulant->id])?>
     </a>
    <div class='alert alert-danger alert-dismissable'>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        
        <?= Yii::t('app','The subscription of {name} is already validate',['name'=>$postulant->first_name.' '.$postulant->last_name])?>
    </div>
    <?php } ?>    
    
</div>
<div class='col-lg-1'>
</div>


<?php ActiveForm::end(); ?>

 <p><br/><br/><br/><br/><br/><br/><br/></p>          
<?php
    
    $script = <<< JS
    $(document).ready(function(){
            $('#valider-inscription').show();
            $('#print-inscription').hide();
           $('.zonteks').editable(
                {
                'emptytext' : 'Ajouter',
                'mode'  : 'popup',
                showbuttons: true,
                
            }
            ); 
            
            $('#date-examen').datepicker({
                autoclose: true,
                dateFormat: 'dd-mm-yy'
            });
            
            $('#valider-inscription').click(function(){
                var id_postulant = $(this).attr('data-idpostulant');
                var date_examen = $('#date-examen').val();
                if(date_examen==''){
                        alert("La date d'examen est obligatoire !")
                    }else{
                $.post('$baseUrl/index.php/inscription/postulant/validate-inscription',{id_postulant:id_postulant,date_examen:date_examen},function(data){
                        $('#espas-msg-valide').html(data);
                        $('#valider-inscription').hide();
                        $('#print-inscription').show();
                    });
                }
                        //alert(id_postulant);
                });
            
        
      });

JS;
$this->registerJs($script);
?>            