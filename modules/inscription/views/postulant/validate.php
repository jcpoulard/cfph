<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\inscription\models\Postulant; 

$count_stud = Postulant::findBySql("SELECT * FROM postulant WHERE is_validation_expire = 3")->count();

?>
<?php //$this->render("//layouts/inscriptionLayout") ?>

<div class="row" style='padding: 8px;'>
    <div style="">
        <div class='col-xs-2'>
            
        </div>
        <!--
        <div class='col-xs-3'>
            <div class='input-group'>
                <input type='text' class='form-control' id='date-examen' placeholder="<?= Yii::t('app','Exam date'); ?>">
            </div> 
        </div>
        -->
        <div class="col-xs-6">
        <div class="input-group">
            
            <input type="text" class="form-control" id="no-stud" name="no-stud" placeholder="<?= Yii::t('app','Type or scan code of subscription')?>" onkeypress="handle(event)">
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app','View')?>">
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type="hidden" id="idstud-selected">
        </div>
        
        </div>
    
    <div class='col-xs-2'>
        <div class="input-group">
           <span class="input-group-btn"> 
                        <a type="button" class="btn btn-primary" id="view-more-detail" href='create' title="<?= Yii::t('app','Add')?>">
                            <i class="fa fa-plus"></i>
                    </a> 
            </span>
                <input type="hidden" id="idstud-selected">
        </div>
            
    </div>
    </div>
  </div>

<div id="espas-dashboard">
    
</div>
<div id="espas-suite-dashboard">
    
</div>

<div class='row'>
    <div id='espas-postulant'>
        
    </div>
</div>

 <p><br/><br/><br/><br/><br/><br/><br/></p>    

<?php
$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    $(document).ready(function(){
        $('#date-examen').datepicker({
                autoclose: true,
                dateFormat: 'dd-mm-yy'
            });
        $.get('$baseUrl/index.php/inscription/postulant/dashboard-inscription',{},function(data){
                $('#espas-dashboard').html(data);
            });
        
        $.get('$baseUrl/index.php/inscription/postulant/suite-dashboard-inscription',{},function(data){
                $('#espas-suite-dashboard').html(data);
            });
        }); 
            
        
    $('#no-stud').keypress(function(e) {
        if(e.which === 13) {
            var id_stud = $('#no-stud').val();
            $.post('$baseUrl/index.php/inscription/postulant/view-inscription',{id_stud:id_stud},function(data){
                        $('#espas-postulant').html(data);
                });
            
            $.get('$baseUrl/index.php/inscription/postulant/dashboard-inscription',{},function(data){
                $('#espas-dashboard').html(data);
            });
            
            $('#espas-suite-dashboard').hide();
        }
    });
        
    $('#view-more-detail').click(function(){
        var id_stud = $('#no-stud').val();
            $.post('$baseUrl/index.php/inscription/postulant/view-inscription',{id_stud:id_stud},function(data){
                        $('#espas-postulant').html(data);
                });
            
            $.get('$baseUrl/index.php/inscription/postulant/dashboard-inscription',{},function(data){
                $('#espas-dashboard').html(data);
            });
            
            $('#espas-suite-dashboard').hide();
        });    

JS;
$this->registerJs($script);

?>
   