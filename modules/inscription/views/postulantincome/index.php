<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 
use yii\widgets\Pjax;



use app\modules\fi\models\SrcProgram;

use app\modules\billings\models\SrcDevises;
use app\modules\billings\models\SrcOtherIncomes;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\inscription\models\SrcPostulantIncome */
/* @var $dataProvider yii\data\ActiveDataProvider */


$acad = Yii::$app->session['currentId_academic_year'];


$this->title = Yii::t('app', 'Postulant Incomes');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
   if(Yii::$app->user->can('inscription-postulantincome-view'))
   {
?>
 <div class='row'>
    <div style="margin-left:auto;margin-right:auto; width:50%;">
        <div class="col-xs-9">
        <div class="input-group">
            <input type="text" class="form-control" id="no-postu" name="no-postu" placeholder="<?= Yii::t('app','Search a postulant'); ?>" >
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idpostu-selected'>
        </div>
        
        </div>
    
    <div  style="width:auto;float:left; margin-top:-8px;">
            

    </div>
    </div>
    
</div>
<?php

}

?>
                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                          <div class="ibox-title">
                             <h5>
                           <?php
                                  echo Yii::t('app','Paid applicants list');
                              ?>
                              
                              </h5>
                               
                            </div>
                <br/>          
                            <div class="ibox-content">
                                 <div class="col-md-14 table-responsive"  style="margin-top:-12px; margin-bottom:55px;" >
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Program').' / '.Yii::t('app','Shift'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <!-- <th><?= Yii::t('app','Payment Method'); ?></th> -->
                                            <th><?= Yii::t('app','Payment Date'); ?></th>
                                            <th><?= Yii::t('app','Received by'); ?></th>
				            <?php
                                              if(Yii::$app->user->can('inscription-postulantincome-view'))
                                              {
                                                  ?>
                                              
                                                       <th></th>
                                              <?php
                                                   }
                                              ?>
				            </tr>
				        </thead>
				        <tbody>
                            <?php

                                    $dataBilling = $dataProvider->getModels();
                                      foreach($dataBilling as $billing)
                                       {


                                       echo '  <tr >
                                                     <td >'.$billing->postulant0->first_name.' </td>
                                                    <td >'.$billing->postulant0->last_name.' </td>
                                                    <td >'.$billing->postulant0->applyForProgram->short_name.' / '.$billing->postulant0->shift0->shift_name.'</td>
                                                    <td >'; if(isset($billing->fee0->fee_label)) echo $billing->fee0->fee_label; 
                                                             else echo ' ';  echo ' </td>
                                                    <td >'.$billing->devise0->devise_symbol.' '.numberAccountingFormat($billing->amount).'</td>';
                                                   // <td >'.$billing->paymentMethod->method_name.'</td>
                                       echo '       <td >'.Yii::$app->formatter->asDate($billing->payment_date).'</td>';
                                                   
                                                 $resupar ='';
                                                       if($billing->update_by!=null)
                                                           $resupar = $billing->update_by;
                                                       else
                                                       {
                                                           if($billing->create_by=='SIGES')
                                                               $resupar = Yii::t('app', 'System');
                                                           else
                                                               $resupar = $billing->create_by;
                                                       }
                                      
                                      echo '         <td >'.$resupar.'</td>';
                                                    
                                      if(Yii::$app->user->can('inscription-postulantincome-view'))
                                                {
                                                      echo '<td >';
                                                           if(Yii::$app->user->can('inscription-postulantincome-view'))
                                                             {
                                                            echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/inscription/postulantincome/view?id='.$billing->postulant.'&bil='.$billing->id.'&wh=incom', [
                                                        'title' => Yii::t('app', 'Update'),
                                            ]);
                                                                }


                                                          if(Yii::$app->user->can('inscription-postulantincome-delete'))
                                                                {
                                                               echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/inscription/postulantincome/delete?id='.$billing->id.'&wh=incom', [
                                                        'title' => Yii::t('app', 'Delete'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                                        'data-method' => 'post',
                                            ]) ;
                                                              }

                                             echo '</td>';
                                                  
                                                }
 
                                                                         

                                                                 echo '

                                                                    </tr>';
                                            }
                           ?>
                                                 </tbody>
                                        </table>


                                     </div>

                            </div>
                        </div>
                    </div>

             

            </div>


<?php

$script1 = <<< JS
    $(document).ready(function(){
         
      $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Recette scolarite du jour'},
                    {extend: 'pdf', title: 'Recette scolarite du jour'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
                
   
        $('#no-postu').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_postulants; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idpostu-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_postu = $('#idpostu-selected').val();
            if(id_postu!==''){
                window.location.href = "/cfph/web/index.php/inscription/postulantincome/view?id="+id_postu+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }
           
            });
         
   });      
     
   $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_postu = $('#idpostu-selected').val();
            if(id_postu!==''){
                window.location.href = "/cfph/web/index.php/inscription/postulantincome/view?id="+id_postu+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }

        }

    });
         
         
JS;

 $this->registerJs($script1); 
 
   
?>