<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

use yii\grid\GridView;
use yii\helpers\Url;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcPaymentMethod;
use app\modules\billings\models\SrcDevises;

use app\modules\fi\models\SrcProgram; 
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\inscription\models\SrcPostulant;


/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\Billings */


$full_name = '';
  if(isset($model->postulant0))
   {
      $this->title = $model->postulant0->getFullname();
      $full_name = $model->postulant0->getFullname();
      $id_student = $model->postulant;
    }
  else 
      {
        $modelPerson = SrcPostulant::findOne($_GET['id']);
        $this->title = $modelPerson->getFullname();
        $full_name = $modelPerson->getFullname();
        $id_student = $modelPerson->id;
     }

?>
<?php
   

 $acad = Yii::$app->session['currentId_academic_year'];
 
 $modelAcad = new SrcAcademicPeriods();
 $previous_year= $modelAcad->getPreviousAcademicYear($acad); 
 
 $pending_balance=null;
 
 $summary_extended_text = '';

  $postulant_id = 0;
      	 $birthday = '0000-00-00';
     $image='';
    
   if(isset($model->postulant0))
      {
      	 $postulant_id = $model->postulant0->id;
      	 $birthday = $model->postulant0->birthday;
      	 $image= $model->postulant0->image;
      	 $full_name = $model->postulant0->getFullName();
      }
   else
      {
      	  if(isset($_GET['id']))
      	    {  $postulant_id = $_GET['id'];
      	       
      	       $postulant=SrcPostulant::findOne($postulant_id);
      	   
      	       $birthday = $postulant->birthday;
      	       
      	      // $image= $postulant->image;
      	       $full_name = $postulant->getFullName();
    	 	   
      	    }
       }
     

     $level=1 ;
      
     $acad = Yii::$app->session['currentId_academic_year'];
                      

 
$condition = '';
$ri= 1;

 if(isset($_GET['ri']))
   {         
       if($_GET['ri']==0)
         {  $ri= 0;     
        
          }
        elseif($_GET['ri']==1)
          { $ri= 1;     
         
          	 }
               
               
               
               
    }

 function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }



?>
<div class="row">
    <div class="col-lg-7" >
        <h3 >  
            <div style="color:#CD3D4C; float:left; margin-top:15px;" >
        
            <?php   if(isset($modelAdd->postulant))
                       echo '<div style="color:#CD3D4C; float:left;">'.$full_name.'  </div>';
                      else
                        echo $full_name;
                        
                  ?>
          </div>   
         </h3>
        <div class="col-xs-6" style="float: right; margin-right: -40px;" ><div class="input-group">
            <input type="text" class="form-control" id="no-postu" name="no-postu" placeholder="<?= Yii::t('app','Search a postulant'); ?>" >
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idpostu-selected'>
                <input type='hidden' id='idpostu' value="<?= $_GET['id']; ?>">
                
        </div></div>   
    
    
    </div>
    <div class="col-lg-5">
        <p>
        
    </p>
    </div>
</div> 

<br/>


   

<div style="clear:both"></div>

<!-- La ligne superiure  -->


<div class="row">
   
     <div class="col-lg-7 billings-view" style="border:1px solid #DDDDDD;  margin-left:15px; margin-top: -15px;" >
   
	<?php
 //*********************************************************************************************       
        
            //pou update 
                if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
                  $title = Yii::t('app', 'Update billing adminssion'); 
                else
                   $title = Yii::t('app', 'Create billing adminssion'); 
            
           
           ?>
           <?php $form = ActiveForm::begin(); ?>   
         <div class="row">
    
                    <div class="col-lg-1">
                         <h3><?= $title; ?></h3>
                          
                    </div>
             
             <div class="col-lg-3" style="margin-top: 10px; margin-left: 20px;">
            <?php


             $data_fee = loadFeeForAdmis();

                   echo $form->field($modelAdd, 'fee')->label(FALSE)->widget(Select2::classname(), [

                   'data'=>$data_fee, //ArrayHelper::map(SrcFees::find()->all(),'id','fee' ),
                   'size' => Select2::MEDIUM,
                   'theme' => Select2::THEME_CLASSIC,
                   'language'=>'fr',
                   'options'=>['placeholder'=>Yii::t('app', ' --  select fee  --'),
                                'onchange'=>'submit()',

                         ],
                   'pluginOptions'=>[
                         'allowclear'=>true,
                     ],
                   ]) ;					
							
           ?>

         </div>


                </div>

<?php
        
      if( ( ($modelAdd->fee!='')&&($fee_already_paid==false) )|| ( (isset($_GET['bil']))&&($_GET['bil']!='') ) )
      {
       
 
 ?>
                  <div class="wrapper wrapper-content billings-create" style=" margin-bottom: 0px;">
                     

                              
                     

    <div class="row">
    	
   
        <div class="col-lg-4">
           <?= $form->field($modelAdd, 'amount')->textInput(['disabled' => false]) ?>
        </div>
        
           <div class="col-lg-4">
		   
           <?php
                 
                 echo $form->field($modelAdd, 'devise')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcDevises::find()->all(),'id','devise_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select devise  --'),
                                    //'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
          
                     ?>
         </div>
        
         
          <div class="col-lg-4">
           <?php
                 
                 echo $form->field($modelAdd, 'payment_method')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcPaymentMethod::find()->all(),'id','method_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select payment method  --'),
                                    //'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
           ?>
         </div>
        
        
        
    </div>

 
  <?php
      
     
 ?>

    <div class="row">
       
    	<div class="col-lg-4">
		    <?php
                     if(($modelAdd->payment_date ==null)||($modelAdd->payment_date =='0000-00-00'))
                        $modelAdd->payment_date = date('Y-m-d');
                     
                echo $form->field($modelAdd, 'payment_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
         </div>
        
        
        <div class="col-lg-8">
           <?= $form->field($modelAdd, 'comments')->textarea(['rows' => 1])  ?>
        </div>
    
    </div>
    
      
    

<?php 
   
   
?>                            
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Create'), ['name' => 'create', 'class' => 'btn btn-success' ]) ?>
        
       
               
          <button type="button" class="btn btn-warning" id="cancel" title="<?= Yii::t('app', 'Cancel') ?>" >
                            <?= Yii::t('app', 'Cancel'); ?>
                    </button>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

<?php
        
  
 ?>
                      </div>
  <?php
         
  
         }      
    
 ?>  
      <?php ActiveForm::end(); ?>


                 
  <?php   
  //*****************************************************************************************************
  ?>
		
		</div>
		
		
    
</div>

<div style="clear:both"></div>
 <!-- Seconde ligne -->
 <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                          <div class="ibox-title">
                             <h5>
                           <?php
                                  echo Yii::t('app','Paid applicants list');
                              ?>
                              
                              </h5>
                               
                            </div>
                <br/>          
                            <div class="ibox-content">
                                 <div class="col-md-14 table-responsive"  style="margin-top:-12px; margin-bottom:55px;" >
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Program').' / '.Yii::t('app','Shift'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Amount'); ?></th>
				            <!-- <th><?= Yii::t('app','Payment Method'); ?></th> -->
                                            <th><?= Yii::t('app','Payment Date'); ?></th>
                                            <th><?= Yii::t('app','Received by'); ?></th>
				            <th></th>

				            </tr>
				        </thead>
				        <tbody>
                            <?php

                                    $dataBilling = $dataProvider->getModels();
                                      foreach($dataBilling as $billing)
                                       {


                                       echo '  <tr >
                                                     <td >'.$billing->postulant0->first_name.' </td>
                                                    <td >'.$billing->postulant0->last_name.' </td>
                                                    <td >'.$billing->postulant0->applyForProgram->short_name.' / '.$billing->postulant0->shift0->shift_name.'</td>
                                                    <td >'; if(isset($billing->fee0->fee_label)) echo $billing->fee0->fee_label; 
                                                             else echo ' ';  echo ' </td>
                                                    <td >'.$billing->devise0->devise_symbol.' '.numberAccountingFormat($billing->amount).'</td>';
                                                   // <td >'.$billing->paymentMethod->method_name.'</td>
                                       echo '       <td >'.Yii::$app->formatter->asDate($billing->payment_date).'</td>';
                                                   
                                                 $resupar ='';
                                                       if($billing->update_by!=null)
                                                           $resupar = $billing->update_by;
                                                       else
                                                       {
                                                           if($billing->create_by=='SIGES')
                                                               $resupar = Yii::t('app', 'System');
                                                           else
                                                               $resupar = $billing->create_by;
                                                       }
                                      
                                      echo '         <td >'.$resupar.'</td>
                                                     <td >';
                                                           if(Yii::$app->user->can('inscription-postulantincome-view'))
                                                             {
                                                            echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/inscription/postulantincome/view?id='.$billing->postulant.'&bil='.$billing->id.'&wh=incom', [
                                                        'title' => Yii::t('app', 'Update'),
                                            ]);
                                                                }


                                                           if(Yii::$app->user->can('inscription-postulantincome-delete'))
                                                                {
                                                               echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/inscription/postulantincome/delete?id='.$billing->id.'&wh=incom', [
                                                        'title' => Yii::t('app', 'Delete'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                                        'data-method' => 'post',
                                            ]) ;
                                                              }

                                             echo '</td>
                                                                        
                                                                        
                                                                        
                                                                        ';

 
                                                                         

                                                                 echo '

                                                                    </tr>';
                                            }
                           ?>
                                                 </tbody>
                                        </table>


                                     </div>

                            </div>
                        </div>
                    </div>

             

            </div>

<?php

  
?>


<?php

 $script1 = <<< JS
    $(document).ready(function(){
         
      $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Recette scolarite du jour'},
                    {extend: 'pdf', title: 'Recette scolarite du jour'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
               
          $('#no-postu').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_postulants; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idpostu-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_postu = $('#idpostu-selected').val();
            if(id_postu!==''){
                window.location.href = "/cfph/web/index.php/inscription/postulantincome/view?id="+id_postu+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }
           
            });
         
         $('#cancel').click(function(){
            var id_postu =  $('#idpostu').val();
                window.location.href = "/cfph/web/index.php/inscription/postulantincome/view?id="+id_postu+"&month_=&year_=&wh=inc_bil&ri=0";
            
            });
         
         

        });
         
     $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_postu = $('#idpostu-selected').val();
            if(id_postu!==''){
                window.location.href = "/cfph/web/index.php/inscription/postulantincome/view?id="+id_postu+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }

        }

    });

JS;

 $this->registerJs($script1);       
?>
    
<script>
      function printContent(el)
      {
          document.getElementById("header").style.display = "block";
		  //  document.getElementById("emp_name").style.display = "block";
		 //   document.getElementById("p_month").style.display = "block";
		  //  document.getElementById("p_date").style.display = "block";
		     
     
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
         
          
     document.getElementById("header").style.display = "none";
     // document.getElementById("emp_name").style.display = "none";
    //document.getElementById("p_month").style.display = "none";
    //document.getElementById("p_date").style.display = "none";
     }
   </script>





