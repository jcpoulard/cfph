<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\PostulantIncome */

$this->title = Yii::t('app', 'Create Postulant Income');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Postulant Incomes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="postulant-income-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
