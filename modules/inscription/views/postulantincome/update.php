<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inscription\models\PostulantIncome */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Postulant Income',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Postulant Incomes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="postulant-income-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
