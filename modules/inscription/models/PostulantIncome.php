<?php

namespace app\modules\inscription\models;

use Yii;
use app\modules\fi\models\Academicperiods;
use app\modules\billings\models\PaymentMethod;
use app\modules\billings\models\Devises;
use app\modules\billings\models\FeesLabel;
use app\modules\inscription\models\Postulant;

/**
 * This is the model class for table "postulant_income".
 *
 * @property integer $id
 * @property integer $postulant
 * @property double $amount
 * @property integer $devise
 * @property integer $fee
 * @property integer $payment_method
 * @property string $comments
 * @property string $payment_date
 * @property integer $academic_year
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Academicperiods $academicYear
 * @property PaymentMethod $paymentMethod
 * @property Devises $devise0
 * @property FeesLabel $fee0
 * * @property Postulant $postulant0 
 */
class PostulantIncome extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_income';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant', 'amount', 'payment_method', 'payment_date', 'academic_year', 'date_created', 'create_by'], 'required'],
            [['postulant', 'devise', 'fee', 'payment_method', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['payment_date', 'date_created', 'date_updated'], 'safe'],
            [['comments'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['payment_method'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentMethod::className(), 'targetAttribute' => ['payment_method' => 'id']],
            [['devise'], 'exist', 'skipOnError' => true, 'targetClass' => Devises::className(), 'targetAttribute' => ['devise' => 'id']],
            [['fee'], 'exist', 'skipOnError' => true, 'targetClass' => FeesLabel::className(), 'targetAttribute' => ['fee' => 'id']],
            [['postulant'], 'exist', 'skipOnError' => true, 'targetClass' => Postulant::className(), 'targetAttribute' => ['postulant' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'amount' => Yii::t('app', 'Amount'),
            'devise' => Yii::t('app', 'Devise'),
            'fee' => Yii::t('app', 'Fee'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'comments' => Yii::t('app', 'Comments'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['id' => 'payment_method']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevise0()
    {
        return $this->hasOne(Devises::className(), ['id' => 'devise']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFee0()
    {
        return $this->hasOne(FeesLabel::className(), ['id' => 'fee']);
    }
 

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getPostulant0() 
   { 
       return $this->hasOne(Postulant::className(), ['id' => 'postulant']); 
   } 
   
   
}
