<?php

namespace app\modules\inscription\models;

use Yii;

/**
 * This is the model class for table "postulant_health".
 *
 * @property integer $id
 * @property integer $postulant
 * @property string $blood_group
 * @property integer $is_permanent_decease
 * @property string $permanent_decease
 * @property integer $is_regulary_drug
 * @property string $regulary_drug
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class PostulantHealth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_health';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant', 'is_permanent_decease', 'is_regulary_drug'], 'integer'],
            [['permanent_decease', 'regulary_drug'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['blood_group'], 'string', 'max' => 8],
            [['create_by', 'update_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'blood_group' => Yii::t('app', 'Blood Group'),
            'is_permanent_decease' => Yii::t('app', 'Is Permanent Decease'),
            'permanent_decease' => Yii::t('app', 'Permanent Decease'),
            'is_regulary_drug' => Yii::t('app', 'Is Regulary Drug'),
            'regulary_drug' => Yii::t('app', 'Regulary Drug'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
