<?php

namespace app\modules\inscription\models;

use Yii;
use app\modules\fi\models\Relations;

/**
 * This is the model class for table "postulant_liable_person".
 *
 * @property integer $id
 * @property integer $postulant
 * @property string $last_name
 * @property string $first_name
 * @property string $phone
 * @property string $address
 * @property string $occupation
 * @property string $occupation_address
 * @property string $occupation_phone
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property Postulant $postulant0
 */
class PostulantLiablePerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_liable_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant'], 'integer'],
            [['address', 'occupation_address'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['last_name', 'first_name', 'occupation', 'create_by', 'update_by'], 'string', 'max' => 64],
            [['phone', 'occupation_phone'], 'string', 'max' => 16],
            [['postulant'], 'exist', 'skipOnError' => true, 'targetClass' => Postulant::className(), 'targetAttribute' => ['postulant' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'last_name' => Yii::t('app', 'Last Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'occupation' => Yii::t('app', 'Occupation'),
            'occupation_address' => Yii::t('app', 'Occupation Address'),
            'occupation_phone' => Yii::t('app', 'Occupation Phone'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulant0()
    {
        return $this->hasOne(Postulant::className(), ['id' => 'postulant']);
    }
    
    public function getRelation0()
    {
        return $this->hasOne(Relations::className(), ['id' => 'relation_responsable']);
    }
}
