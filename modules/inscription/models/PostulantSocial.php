<?php

namespace app\modules\inscription\models;

use Yii;

/**
 * This is the model class for table "postulant_social".
 *
 * @property integer $id
 * @property integer $postulant
 * @property string $civil_state
 * @property integer $is_leave_outside
 * @property string $foreign_country
 * @property integer $is_prison
 * @property string $prison_place
 * @property integer $is_religion
 * @property string $religion
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class PostulantSocial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant', 'is_leave_outside', 'is_prison', 'is_religion'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['civil_state', 'foreign_country', 'prison_place', 'religion'], 'string', 'max' => 32],
            [['create_by', 'update_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'civil_state' => Yii::t('app', 'Civil State'),
            'is_leave_outside' => Yii::t('app', 'Is Leave Outside'),
            'foreign_country' => Yii::t('app', 'Foreign Country'),
            'is_prison' => Yii::t('app', 'Is Prison'),
            'prison_place' => Yii::t('app', 'Prison Place'),
            'is_religion' => Yii::t('app', 'Is Religion'),
            'religion' => Yii::t('app', 'Religion'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
