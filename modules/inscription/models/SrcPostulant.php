<?php

namespace app\modules\inscription\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inscription\models\Postulant;

/**
 * SrcPostulant represents the model behind the search form about `app\modules\inscription\models\Postulant`.
 */
class SrcPostulant extends Postulant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cities', 'is_working', 'apply_for_program', 'status', 'academic_year', 'cohorte'], 'integer'],
            [['first_name', 'last_name', 'gender', 'birthday', 'adresse', 'phone', 'name_working', 'adress_working', 'last_class', 'previous_school', 'school_date_entry', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['last_average'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Postulant::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
public function searchByDateStart($date_start)
{
        $query = Postulant::find()->where("date_created >='".$date_start."'")->orderBy(['last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }

public function searchByProgrameShift($program,$shift,$date_start)
{           
       $query = Postulant::find()->select('postulant.*')->where("status=0 AND date_created >='".$date_start."'")->orderBy(['postulant.first_name'=>SORT_ASC,'postulant.last_name'=>SORT_ASC]);
              
        if( ($program!='')&&($shift!='') )
          {
            $query = Postulant::find()->select('postulant.*')->where("status=0 AND apply_for_program=".$program." AND shift=".$shift." AND date_created >='".$date_start."'")->orderBy(['postulant.first_name'=>SORT_ASC,'postulant.last_name'=>SORT_ASC]);
          }
        elseif( ($program!='')&&($shift=='') )
          {
            $query = Postulant::find()->select('postulant.*')->where("status=0 AND apply_for_program=".$program." AND date_created >='".$date_start."'")->orderBy(['postulant.first_name'=>SORT_ASC,'postulant.last_name'=>SORT_ASC]);
          }
         elseif( ($program='')&&($shift=='') )
          {
            $query = Postulant::find()->select('postulant.*')->where("status=0 AND date_created >='".$date_start."'")->orderBy(['postulant.first_name'=>SORT_ASC,'postulant.last_name'=>SORT_ASC]);
          }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
public function searchByDateStartForDecision($date_start,$program,$shift)
{  //$postulant_data = Postulant::find()->select('postulant.*')->innerJoin('postulant_grades', '`postulant_grades`.`postulant` = `postulant`.`id`')->where(['status'=>0,'apply_for_program'=>$program,'shift'=>$shift])->orderBy(['postulant_grades.grade_value'=>SORT_DESC])->all(); 
            
        $query = Postulant::find()->select('postulant.*')->join('INNER JOIN','postulant_grades', 'postulant_grades.postulant = postulant.id')->where("status=0 AND apply_for_program=".$program." AND shift=".$shift." AND date_created >='".$date_start."'")->orderBy(['postulant_grades.grade_value'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }

    public function searchByDateStartForAdmis($date_start)
{  //$postulant_data = Postulant::find()->where(['status'=>1])->orderBy(['last_name'=>SORT_ASC])->all();; 
            
        $query = Postulant::find()->where("status=1 AND date_created >='".$date_start."'")->orderBy(['last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
 
    public function searchByDateStartForNonAdmis($date_start)
{  //$postulant_data = Postulant::find()->where(['status'=>2])->orderBy(['last_name'=>SORT_ASC])->all();; 
            
        $query = Postulant::find()->where("status=2 AND date_created >='".$date_start."'")->orderBy(['last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    
  
   public function searchPostulantAdmisPaye($date_start)
{           
        $query = Postulant::find()->where("status=1 AND date_created >='".$date_start."'")->orderBy(['last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'cities' => $this->cities,
            'is_working' => $this->is_working,
            'apply_for_program' => $this->apply_for_program,
            'school_date_entry' => $this->school_date_entry,
            'last_average' => $this->last_average,
            'status' => $this->status,
            'academic_year' => $this->academic_year,
            'cohorte' => $this->cohorte,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name_working', $this->name_working])
            ->andFilterWhere(['like', 'adress_working', $this->adress_working])
            ->andFilterWhere(['like', 'last_class', $this->last_class])
            ->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
   
    
public function getPostulantForNextYear($acad) 
  {
       $query = SrcPostulant::find()->select(['gender'])->where('academic_year='.$acad)->all();
       
       return $query;
   }
    
 /* Getter for person full name */
public function getFullName() {
    return $this->first_name . ' ' .strtr( strtoupper($this->last_name), capital_aksan() );
}
   
    
    
    
    
    
    
    
    
}
