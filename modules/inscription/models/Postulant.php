<?php

namespace app\modules\inscription\models;

use Yii;
use app\modules\inscription\models\PostulantLiablePerson; 
use app\modules\fi\models\Academicperiods; 
use app\modules\fi\models\Program; 
use app\modules\fi\models\Shifts; 

/**
 * This is the model class for table "postulant".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $birthday
 * @property integer $cities
 * @property string $adresse
 * @property string $phone
 * @property integer $is_working
 * @property string $name_working
 * @property string $adress_working
 * @property string $work_phone
 * @property integer $apply_for_program
 * @property integer $program_second
 * @property integer $shift
 * @property string $last_class
 * @property string $previous_school
 * @property string $school_date_entry
 * @property double $last_average
 * @property integer $status
 * @property integer $academic_year
 * @property integer $cohorte
 * @property integer $recommandation_letter
 * @property string $exam_date
 * @property string $no_inscription
 * @property string $email
 * @property string $postulant_password
 * @property string $validation_code
 * @property integer $is_validation_expire
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Academicperiods $academicYear
 * @property Program $applyForProgram
 * @property PostulantGrades[] $postulantGrades
 * @property PostulantLiablePerson[] $postulantLiablePeople
 * @property PostulantParent[] $postulantParents
 * @property PostulantSocial[] $postulantSocials
 */
class Postulant extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'postulant';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'gender', 'email', 'postulant_password', 'validation_code', 'is_validation_expire', 'date_created'], 'required'],
            [['birthday', 'school_date_entry', 'exam_date', 'date_created', 'date_updated'], 'safe'],
            [['cities', 'is_working', 'apply_for_program', 'program_second', 'shift', 'status', 'academic_year', 'cohorte', 'recommandation_letter', 'is_validation_expire'], 'integer'],
            [['name_working', 'adress_working', 'postulant_password'], 'string'],
            [['last_average'], 'number'],
            [['first_name'], 'string', 'max' => 120],
            [['last_name', 'gender', 'phone', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['adresse', 'previous_school', 'email'], 'string', 'max' => 255],
            [['work_phone'], 'string', 'max' => 16],
            [['last_class'], 'string', 'max' => 64],
            [['no_inscription', 'validation_code'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['validation_code'], 'unique'],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['apply_for_program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['apply_for_program' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' =>Yii::t('app','First Name'),
            'last_name' =>Yii::t('app','Last Name'),
            'gender' => 'Gender',
            'birthday' => 'Birthday',
            'cities' => 'Cities',
            'adresse' => 'Adresse',
            'phone' => 'Phone',
            'is_working' => 'Is Working',
            'name_working' => 'Name Working',
            'adress_working' => 'Adress Working',
            'work_phone' => 'Work Phone',
            'apply_for_program' => 'Apply For Program',
            'program_second' => 'Program Second',
            'shift' => 'Shift',
            'last_class' => 'Last Class',
            'previous_school' => 'Previous School',
            'school_date_entry' => 'School Date Entry',
            'last_average' => 'Last Average',
            'status' => 'Status',
            'academic_year' => 'Academic Year',
            'cohorte' => 'Cohorte',
            'recommandation_letter' => 'Recommandation Letter',
            'exam_date' => 'Exam Date',
            'no_inscription' => 'No Inscription',
            'email' => 'Email',
            'postulant_password' => 'Postulant Password',
            'validation_code' => 'Validation Code',
            'is_validation_expire' => 'Is Validation Expire',
            'date_created' => 'Date Created',
            'date_updated' => 'Date Updated',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramSecond()
    {
        return $this->hasOne(Program::className(), ['id' => 'program_second']);
    }
    
    public function getApplyForProgram()
    {
        return $this->hasOne(Program::className(), ['id' => 'apply_for_program']);
    }
    
    public function getShift0()
    {
        return $this->hasOne(Shifts::className(), ['id' => 'shift']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulantGrades()
    {
        return $this->hasMany(PostulantGrades::className(), ['postulant' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulantLiablePeople()
    {
        return $this->hasMany(PostulantLiablePerson::className(), ['postulant' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulantParents()
    {
        return $this->hasMany(PostulantParent::className(), ['postulant' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulantSocials()
    {
        return $this->hasMany(PostulantSocial::className(), ['postulant' => 'id']);
    }
    
    public function getSexe(){
        if($this->gender == 0){
            return "Masculin";
        }elseif($this->gender==1){
            return "F&eacute;minin";
        }else{
            return "N/A";
        }
    }
    

    
    
}