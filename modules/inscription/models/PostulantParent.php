<?php

namespace app\modules\inscription\models;

use Yii;

/**
 * This is the model class for table "postulant_parent".
 *
 * @property integer $id
 * @property integer $postulant
 * @property string $mother_name
 * @property string $father_name
 * @property string $address_in_haiti
 * @property string $address_foreign
 * @property string $phone_mother
 * @property string $phone_father
 * @property string $occupation_mother
 * @property string $occupation_father
 * @property string $occupation_mother_address
 * @property string $occupation_father_address
 * @property string $occupation_mother_phone
 * @property string $occupation_father_phone
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property Postulant $postulant0
 */
class PostulantParent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_parent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant'], 'required'],
            [['postulant'], 'integer'],
            [['address_in_haiti', 'address_foreign', 'occupation_mother_address', 'occupation_father_address'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['mother_name', 'father_name'], 'string', 'max' => 128],
            [['phone_mother', 'phone_father', 'occupation_mother_phone', 'occupation_father_phone'], 'string', 'max' => 16],
            [['occupation_mother', 'occupation_father'], 'string', 'max' => 64],
            [['create_by', 'update_by'], 'string', 'max' => 32],
            [['postulant'], 'exist', 'skipOnError' => true, 'targetClass' => Postulant::className(), 'targetAttribute' => ['postulant' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'mother_name' => Yii::t('app', 'Mother Name'),
            'father_name' => Yii::t('app', 'Father Name'),
            'address_in_haiti' => Yii::t('app', 'Address In Haiti'),
            'address_foreign' => Yii::t('app', 'Address Foreign'),
            'phone_mother' => Yii::t('app', 'Phone Mother'),
            'phone_father' => Yii::t('app', 'Phone Father'),
            'occupation_mother' => Yii::t('app', 'Occupation Mother'),
            'occupation_father' => Yii::t('app', 'Occupation Father'),
            'occupation_mother_address' => Yii::t('app', 'Occupation Mother Address'),
            'occupation_father_address' => Yii::t('app', 'Occupation Father Address'),
            'occupation_mother_phone' => Yii::t('app', 'Occupation Mother Phone'),
            'occupation_father_phone' => Yii::t('app', 'Occupation Father Phone'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulant0()
    {
        return $this->hasOne(Postulant::className(), ['id' => 'postulant']);
    }
}
