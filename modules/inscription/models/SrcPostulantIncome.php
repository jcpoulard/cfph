<?php

namespace app\modules\inscription\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inscription\models\PostulantIncome;

/**
 * SrcPostulantIncome represents the model behind the search form about `app\modules\inscription\models\PostulantIncome`.
 */
class SrcPostulantIncome extends PostulantIncome
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'postulant', 'devise', 'fee', 'payment_method', 'academic_year'], 'integer'],
            [['amount'], 'number'],
            [['comments', 'payment_date', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PostulantIncome::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'postulant' => $this->postulant,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'fee' => $this->fee,
            'payment_method' => $this->payment_method,
            'payment_date' => $this->payment_date,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    
  public function searchPostulantPaye($date_start)
{           
        $query = PostulantIncome::find()->joinWith(['postulant0'])->where("postulant.status=1 AND postulant.date_created >='".$date_start."' AND fee IS NOT NULL")->orderBy(['last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'postulant' => $this->postulant,
            'amount' => $this->amount,
            'devise' => $this->devise,
            'fee' => $this->fee,
            'payment_method' => $this->payment_method,
            'payment_date' => $this->payment_date,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        

        return $dataProvider;
    }  
    
   
    //return 0(non trouve),1(trouve)
 public function isFeeAlreadyPaid($postulant,$fee)
         {   
 
    
   $sql = "SELECT id  FROM postulant_income  WHERE postulant=".$postulant." AND fee=".$fee;
			    $result = Yii::$app->db->createCommand($sql)->queryAll();
        
    if($result!=NULL)
        return 1; 
    else
        return 0;
         
    
         }
    
    
    
    
    
}
