<?php

namespace app\modules\inscription\models;


use Yii;
use app\modules\fi\models\Academicperiods; 
use app\modules\fi\models\Program; 
use app\modules\inscription\models\Postulant; 

/**
 * This is the model class for table "postulant_grades".
 *
 * @property integer $id
 * @property integer $postulant
 * @property integer $program
 * @property integer $academic_year
 * @property double $grade_value
 * @property string $date_create
 * @property string $create_by
 * @property string $date_update
 * @property string $update_by
 *
 * @property Academicperiods $academicYear
 * @property Postulant $postulant0
 * @property Program $program0
 */
class PostulantGrades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postulant_grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postulant', 'program', 'academic_year', 'grade_value'], 'required'],
            [['postulant', 'program', 'academic_year'], 'integer'],
            [['grade_value'], 'number'],
            [['date_create', 'date_update'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 64],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['postulant'], 'exist', 'skipOnError' => true, 'targetClass' => Postulant::className(), 'targetAttribute' => ['postulant' => 'id']],
            [['program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'postulant' => Yii::t('app', 'Postulant'),
            'program' => Yii::t('app', 'Program'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'grade_value' => Yii::t('app', 'Grade Value'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulant0()
    {
        return $this->hasOne(Postulant::className(), ['id' => 'postulant']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(Program::className(), ['id' => 'program']);
    }
}
