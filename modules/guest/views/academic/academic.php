<?php
use yii\helpers\Url;
use yii\widgets\DetailView;

use yii\grid\GridView;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\fi\models\SrcCustomField;
use app\modules\fi\models\CustomFieldData;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

use kartik\editable\Editable;
use app\modules\fi\models\Relations;
use yii\helpers\ArrayHelper;
use app\modules\fi\models\ContactInfo;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcContactInfo;

use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder;


use app\modules\rbac\models\User;





$acad = Yii::$app->session['currentId_academic_year'];

 $modelAcad = new SrcAcademicPeriods();
 $previous_year= $modelAcad->getPreviousAcademicYear($acad);
 
 // $modelAcad = new SrcAcademicperiods();
 // $previous_year= $modelAcad->getPreviousAcademicYear($acad);
 
 $pending_balance=null;
 
 $pass= true;//isBillingStudent_Ajou($last_bil_id,$modelStudent->id);
 
 
 
$tit = Yii::t('app', 'Student');
?>

<!-- Menu general de creation - modification et suppression d'un étudiant --> 
    <div class="row">
        <div class="col-lg-7">
            <h3><?= '<span style="color:#CD3D4C">'.$tit.' : </span>'.$modelStudent->getFullName(); ?></h3>
        </div>
        <div class="col-lg-5">
 
    
        </div>
    </div>
<div class="tabs-container">
    
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class='fa fa-user'></i><?= Yii::t('app', 'Personal info') ?> </a></li>
                            <li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false"><i class='fa fa-phone'></i><?= Yii::t('app', 'Contact persons') ?> </a></li>
                            <?php 
                                
                                if( $pass==true )
                                  {
                                ?>
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i class='fa fa-file'></i><?= Yii::t('app', 'Grades') ?> </a></li>
                            <?php 

                                }
                            ?>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i class='fa fa-dollar'></i><?= Yii::t('app', 'Billing') ?> </a></li>
                          <!--  <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i class='fa fa-briefcase'></i><?= Yii::t('app', 'Experiences') ?> </a></li>    -->
                        </ul>
                        <div class="tab-content">
                           
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    
                                 <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    <?= Yii::t('app','Basic info') ?>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-10">    
                                                <table id="w0" class="table table-striped table-bordered detail-view table-responsive">
                                                <tbody>
                                                <tr>
                                                    <th><?= Yii::t('app', 'First Name') ?></th>
                                                    <td><?= $modelStudent->first_name ?></td>
                                                    <th><?= Yii::t('app', 'Last Name') ?></th>
                                                    <td><?= $modelStudent->last_name ?></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Gender') ?></th>
                                                    <td>
                                                        
                                                                  
                                                                <?php 
                                                                    if($modelStudent->getGenders()== NULL){
                                                                ?>
                                                                <a href="#" class="sexe" data-name=""  data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="genderlist" data-url="set-infostudent?set=sexe" data-title="<?= Yii::t('app','Gender') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="sexe" data-name="" data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="genderlist" data-url="set-infostudent?is=up&set=sexe" data-value="<?= $modelStudent->gender ?>" data-title="<?= Yii::t('app','Gender') ?>">
                                                                     <?= $modelStudent->getGenders() ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Blood Group') ?></th>
                                                    <td>
                                                          <?php 
                                                                    if($modelStudent->blood_group== NULL){
                                                                ?>
                                                                <a href="#" class="gs" data-name="gs"  data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="gslist" data-url="set-infostudent?set=gs" data-title="<?= Yii::t('app','Blood Group') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="gs" data-name="gs" data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="gslist" data-url="set-infostudent?is=up&set=gs" data-value="<?= $modelStudent->blood_group ?>" data-title="<?= Yii::t('app','Blood Group') ?>">
                                                                     <?= $modelStudent->blood_group ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Birthday') ?></th>
                                                    <td><?= Yii::$app->formatter->asDate($modelStudent->birthday) ?></td>
                                                    <th><?= Yii::t('app', 'Citizenship') ?></th>
                                                    <td><?= $modelStudent->citizenship ?></td>
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Nif Cin') ?></th>
                                                    <td>
                                                         <?php 
                                                                    if($modelStudent->blood_group== NULL){
                                                                ?>
                                                                <a href="#" class="nif_cin" data-name="nif_cin"  data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?set=nifcin" data-title="<?= Yii::t('app','Nif Cin') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="nif_cin" data-name="nif_cin" data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?is=up&set=nicin" data-title="<?= Yii::t('app','Nif Cin') ?>">
                                                                     <?= $modelStudent->nif_cin ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Id Number') ?></th>
                                                    <td><?= $modelStudent->id_number ?></td>
                                                                                                    </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Shift') ?></th>
                                                    <td>
                                                   <?php 
                                                      if(isset($modelStudent->studentOtherInfo0[0]->apply_shift))
                                                         echo $modelStudent->studentOtherInfo0[0]->applyShift->shift_name; 
                                                    ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Room') ?></th>
                                                    <td>
                                                    <?php
                                                      //if(isset($modelStudent->studentHasCourses[0]->course))
                                                         //echo $modelStudent->studentHasCourses[0]->course0->room0->room_name; 
                                                         
                                                          if(isset($modelStudent->studentLevel->room)&&($modelStudent->studentLevel->room!=0))
                                                         echo $modelStudent->studentLevel->room0->room_name; 
                                                    ?>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Schoolarship holder') ?></th>
                                                    <td><?php $schoolarship_ = $modelStudent->getIsScholarshipHolder($modelStudent->id,$acad);
												 
												    if($schoolarship_==1)
												       $schoolarship1 = Yii::t('app','Yes');
												    elseif($schoolarship_==0) 
												     $schoolarship1 = Yii::t('app','No');
												     ?>
												     <?= $schoolarship1 ?></td>
                                                    <th><?= Yii::t('app', 'Stage') ?></th>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Status') ?></th>
                                                    <td><?= $modelStudent->getActive() ?></td>
                                                     <th><?= Yii::t('app', 'Username') ?></th>
                                                    <td><?php 
                                                              $username = '';
                                                              $stud_username = User::find()->where('person_id='.$modelStudent->id )->all();
                                                        
                                                           foreach($stud_username as $stud_user)
                                                             {
                                                             	$username = $stud_user->username;
                                                             	}
                                                        
                                                        echo $username; 
                                                        
                                                        ?> </td>
                                                        
                                                </tr>
                                                </tbody>
                                        </table>
                                                    </div>
                                                    <div class="col-lg-2 text-center">
                                                        <?php 
                                                        $program='';
                                                       if(isset($modelStudent->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $modelStudent->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($modelStudent->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($modelStudent->birthday).Yii::t('app',' yr old').' )';
                                                        }
					         	else
					         	  echo '';
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            <?php if($modelStudent->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/$modelStudent->image")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>


                                                      <span>
                                                            <strong>
                                                        <?php 
					         	  								echo Yii::t('app', 'Level').' : ';
					         	  								if(isset($modelStudent->studentLevel->level) )
					         	  								  echo $modelStudent->studentLevel->level;
					         	  						 ?>
					         	  						   </strong>
                                                        </span>
                            
                                                       
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                    <?= Yii::t('app','Student contact info'); ?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table id="w1" class="table table-striped table-bordered detail-view">
                                                    <tbody>
                                                        <tr>
                                                            <th><?= Yii::t('app', 'Student email') ?></th>
                                                            <td> <?php 
                                                                    if($modelStudent->email== NULL){
                                                                ?>
                                                                <a href="#" class="email" data-name="email"  data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?set=email" data-title="<?= Yii::t('app','Student email') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="email" data-name="email" data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?is=up&set=email" data-title="<?= Yii::t('app','Student email') ?>">
                                                                     <?= $modelStudent->email ?>
                                                                 </a>
                                                                <?php } ?>
                
                
                                                            </td>
                                                            <th><?= Yii::t('app', 'Student phone') ?></th>
                                                            <td>
                                                                <?php 
                                                                    if($modelStudent->phone== NULL){
                                                                ?>
                                                                <a href="#" class="phone" data-name="phone"  data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?set=phone" data-title="<?= Yii::t('app','Student phone') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="phone" data-name="phone" data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?is=up&set=phone" data-title="<?= Yii::t('app','Student phone') ?>">
                                                                     <?= $modelStudent->phone ?>
                                                                 </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr> 
                                                        
                                                        <tr>
                                                            <th><?= Yii::t('app', 'City') ?></th>
                                                            <td>
                                                            <?php 
                                                                //if(isset($modelStudent->cities0->id))
                                                                  //  echo $modelStudent->cities0->city_name; 
                                                            ?>
                                                                
                                                                <?php 
                                                                    if(! isset($modelStudent->cities0->id)){
                                                                ?>
                                                                <a href="#" class="city" data-name="city"  data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="citylist"  data-url="set-infostudent?set=city" data-title="<?= Yii::t('app','City') ?>" >
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="city" data-name="city" data-type="select" data-pk="<?= $modelStudent->id ?>" data-source="citylist"  data-url="set-infostudent?is=up&set=city" data-value="<?= $modelStudent->cities ?>" data-title="<?= Yii::t('app','City') ?>"  >
                                                                     <?= $modelStudent->cities0->city_name ?>
                                                                 </a>
                                                                <?php } ?>
                                                            </td>
                                                            <th><?= Yii::t('app', 'Adresse') ?></th>
                                                            <td>
                                                                  
                                                                <?php 
                                                                    if($modelStudent->adresse== NULL){
                                                                ?>
                                                                <a href="#" class="adresse" data-name="adresse"  data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?set=adresse" data-title="<?= Yii::t('app','Adresse') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="adresse" data-name="adresse" data-type="text" data-pk="<?= $modelStudent->id ?>"  data-url="set-infostudent?is=up&set=adresse" data-title="<?= Yii::t('app','Adresse') ?>">
                                                                     <?= $modelStudent->adresse ?>
                                                                 </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                      
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                    <?= Yii::t('app','Other info')?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table id="w1" class="table table-striped table-bordered detail-view">
                                                    <tbody>
                                                <?php
                                                $modelCustomFieldData = new CustomFieldData();
                                                $criteria = ['field_related_to'=>'stud'];
                                                $data_custom_label = SrcCustomField::find()->where($criteria)->all();
                                               $id_student = $stud_id;
                                                $konte_liy=0;
                                                    foreach($data_custom_label as $dcl){
                                                        $konte_liy++;
                                                        if($konte_liy==0)
                                                            echo '<tr>';
                                                ?>
                                                    <th><?=  $dcl->field_label; ?></th>
                                                    <td><?php echo $modelCustomFieldData->getCustomFieldValue($id_student,$dcl->id);  ?></td>
                                                    <?php 
                                                    if($konte_liy==2){
                                                        echo '</tr>';
                                                        $konte_liy = 0;
                                                    }
                                                    } ?>
                                                    <tr>
                                                        <th><?= Yii::t('app', 'Comment') ?></th>
                                                        <td colspan="3"><?= $modelStudent->comment ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                </div>    
    <!-- Fin de l'accordeon  -->
                           
                         </div>       
                            
            <?php 
                    if( $pass==true )
                        {
                                ?>
  
         <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                  
                        <div class="row">
                           <div style="width:auto; float:left;">
                              <h4><?= Yii::t('app', 'Modules list') ?> </h4>
                           </div>
                           
                         </div>
                                
     	<?php  $include_course_withoutGrade = infoGeneralConfig('include_course_withoutGrade');
     	       
     	       $modelC = new SrcCourses;
			      
			   $allCourses = $modelC->getCourseByStudentID($stud_id);
		?>	      
			
			                 
              <hr/>   
                 
                                
                                    <table class="table table-responsive">
                                        
                                        <thead>
                                        <th style="width:12%" > <?=  Yii::t('app', 'Code') ?> </th>
                                         <th>
                                            <?= Yii::t('app', 'Modules') ?>
                                        </th>
                                        <th > <?=  Yii::t('app', 'Duration') ?> </th>
									     <th > <?=  Yii::t('app', 'Passing Grade') ?> </th>
									     <th > <?=  Yii::t('app', 'Grade') ?> </th>
									     <th > <?=  Yii::t('app', 'Mention') ?> </th> 
									     <th > <?=  Yii::t('app', 'Status') ?> </th>                                        
									     </thead>
                                        
                                      <?php
                                      
                                      		     foreach($allCourses as $course)
											      { 
											           $modelGrade = new SrcGrades;
											           $modelStudCourse = new SrcStudentHasCourses;
											           
											            $modelGradeInfo = $modelGrade->getGradeInfoByStudentCourse($stud_id,$course->id);
											            
											            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($stud_id,$course->id);
											            
											            if($modelIspass==0)
											              {  $color="color:#ff0000;";
											              	}
											            else
											               $color="";
											        
											       if($include_course_withoutGrade==0)
											        {
												        if(isset($modelGradeInfo->grade_value) ) //si li pa konpoze pou kou a pa metel nan kane a
												         {
												         	
												         	
											?>	  
												   
												  <tr  style="<?= $color ?>" >
												     <td ><?=  $course->module0->code ?></td>
												     <td > <?= $course->module0->subject0->subject_name  ?> </td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $modelGradeInfo->grade_value ?> </td>
												     <td > <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
												     <td > <?=   Yii::t('app', 'Evaluated')  ?> </td>
												  </tr>
											<?php
											              }
											              
											         }
											       elseif($include_course_withoutGrade==1)
											          {  
											          	  $grade= '-';
											          	 if(isset($modelGradeInfo->grade_value) )
											          	   { $grade= $modelGradeInfo->grade_value;
											          	     
											          	    }
											          	   
								  	                         
											     ?>
											       <tr  style="<?= $color ?>" >
												     <td > <a href="#" class='popopKou' data-idcourse="<?= $course->id; ?>"><?=  $course->module0->code ?> </a> </td>
												     <td > <?= $course->module0->subject0->subject_name  ?> </td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $grade ?> </td>
												     <td > <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
												     <td > <?=   Yii::t('app', 'Evaluated')  ?> </td>
												  </tr>
											   <?php       	
											          	}
											         
											      }
											      
											?>
                                                                               
                                                                            
                                      <?php
                                      
                                      		     foreach($allCourses as $course)
											      { 
											           $modelGrade = new SrcGrades;
											           $modelStudCourse = new SrcStudentHasCourses;
											           
											            $modelGradeInfo = $modelGrade->getGradeInfoByStudentCourse($stud_id,$course->id);
											            
											            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($stud_id,$course->id);
											            
											            if($modelIspass==0)
											              {  $color="color:#ff0000;";
											              	}
											            else
											               $color="";
											       
											         
											          	  $grade= '-';
											          	 if(isset($modelGradeInfo->grade_value) )
											          	   { $grade= $modelGradeInfo->grade_value;
											          	     
											          	    }
											          	   
								  	                   if($grade== '-')  
								  	                     {       
											     ?>
											       <tr>
												     <td > <?=  $course->module0->code ?> </td>
												     <td > <?= $course->module0->subject0->subject_name  ?> </td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $grade ?> </td>
												     <td > <?=  Yii::t('app', 'N/A') ?> </td>
												     <td > <?=   Yii::t('app', 'Not yet evaluated')  ?> </td>
												  </tr>
											   <?php       	
								  	                     }
											         
											      }
											      
											?>
                                                                               
                                    </table>
                   
                                </div>
                            </div>
                            <?php 
                                }
                            ?>
                     <div id="tab-3" class="tab-pane ">
                                   
                                <div class="panel-body">
                                
                                
                                
                                
                                
                                
                                
                                <div class="row">
                                                                           
                                    
             <?php
                                      $student=SrcPersons::findOne($stud_id);
						      	   
						      	       $birthday = $student->birthday;
						      	       
						      	       $image= $student->image;
						      	       $full_name = $student->getFullName();
						  ?>    	       
						      	  <div class="col-lg-3">
       <div style="background-color:#EDF1F6; color:#F0652E; border:1px solid #DDDDDD; padding:5px; ">
       
           <span  style="font-weight:bold; "><?php echo Yii::t('app','Summary'); ?></span> 
        </div>
  
                
	    <table class="table table-striped table-bordered detail-view table-responsive">
	        
	        <?php
	        
	        ?>
			        <tr class="odd"><th><?php echo Yii::t('app','Exonerate Fee(s) '); ?></th></tr>
			        <tr class="odd">
			            <td>
			                    
			         <?php // 
			                     
			                //if($recettesItems ==  0)     
							//	{ 
									$status_ = 1;	
								  $ri=0; 
							/*	}				              
							elseif($recettesItems ==  1)     
								{   $status_ = 0;
								    $ri=1;
								}
								*/
			
			 //gad si elev la gen balans ane pase ki poko peye
			   
			$modelPendingBal= SrcPendingBalance::find()
			                  ->select(['id', 'balance'])
			                  ->where(['student'=>$stud_id])
			                  ->andWhere(['is_paid'=>0])
			                  ->andWhere(['academic_year'=>$previous_year])
				              ->all();
			//si gen pending, ajoutel nan lis apeye a			
			if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
			 {
			   foreach($modelPendingBal as $bal)
				 {	
				 	$pending_balance = $bal->balance;
				 }
			 }				          
			         
			              $condition = ['fees_label.status'  => $status_];
			              $condition_receipt = '';
			              
			              $full_paid_fees_array = [];
								$fees_paid_array = [];
								$paid_fees_array = [];
						  $arraya_size=0;
								
								 
								 //tcheke si elev sa se yon bousye
								 $modelBous = new SrcScholarshipHolder();
								 $is_scholarshipHolder = $modelBous->getIsScholarshipHolder($stud_id,$acad);    
																	           	  
							     if($is_scholarshipHolder == 1) //se yon bousye
								   {  //tcheke tout fee ki peye net yo    // pou ajoute l/yo sou $fees_paid 
								       $modelBill = new SrcBillings();
								       $full_paid_fee = $modelBill->searchFullPaidFeeByStudentId($stud_id, $status_, $acad);
								       
								       if($full_paid_fee!=null)
							             {  $i=0; 
								           foreach($full_paid_fee as $full_paid)
								             {
								                 $full_paid_fees_array[0][$i] = Yii::t('app',$full_paid->fee_label); 
								                 $full_paid_fees_array[1][$i] = $full_paid->id_fee; 
								                 
								                 //ajoute full paid fee
								                 $paid_fees_array[] = $full_paid->id_fee;
								                 $arraya_size++;
								                 $i++;    	 
								              }
								          }
								       
								    }
								    
							        if($full_paid_fees_array!=null)
							                     {  
								                   if(sizeof($full_paid_fees_array)==0)
								                     {
								                     //	echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][0].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][0].'" id="'.$full_paid_fees_array[1][0].'" checked="checked" value="'.$full_paid_fees_array[1][0].'" disabled="disabled" ></div></div></div>';
								                     
								                     echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][0].' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
								                    
								                    }
								                 else
								                    {
								                   
								                   
								                   for($k=0; $k<$arraya_size; $k++)
								                      {   
						 	                               
					 //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][$k].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][$k].'" id="'.$full_paid_fees_array[1][$k].'" checked="checked" value="'.$full_paid_fees_array[1][$k].'" disabled="disabled" ></div></div></div>';
					 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][$k].' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
						                              										        						
														}
														
								                    }
														
											       }		       				 
				                     ?>
			        
			            </td> 
			        </tr>
			        
			                <tr class="odd"><th><?php echo Yii::t('app','Paid Fee(s) '); ?></th></tr>
			        <tr class="odd">
			            <td>
			                    
			         <?php // 
			                     
			               // if($recettesItems ==  0)     
							//	{ 
									$status_ = 1;	
								  $ri=0;
							/*	}				              
							elseif($recettesItems ==  1)     
								{   $status_ = 0;
								    $ri=1;
								}
							*/
								          
			         
			              $condition =['fees_label.status' => $status_];
			              $condition_receipt = '';
			               
			              
								                       				   
								$fees_desc = array();
							     			                      
						    //return id_fee, fee_name
						       $modelBill = new SrcBillings();
								 $fees_paid = $modelBill->searchPaidFeesByStudentId($stud_id, $status_, $acad);
			                 		
			                 		      if($fees_paid!=null)
							                     {  
								                   foreach($fees_paid as $id_fees_paid)
								                      {   
						 	                               
					 //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$id_fees_paid->fee_label).'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$id_fees_paid->id_fee.'" id="'.$id_fees_paid->id_fee.'" checked="checked" value="'.$id_fees_paid->id_fee.'" disabled="disabled" ></div></div></div>';
					 
					 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$id_fees_paid->fee_label).' <i class="fa fa-check-square-o" aria-hidden="true"></i></div></div></div>';
						                              										        						
														}
														
											       }
											     
										       				 
				                     ?>
			        
			            </td>
			        </tr>
		<?php
		        {              $total_pending_amount = 0;          				   
								 $fees_desc = array();
								 
								 
			                                      
			                       if($fees_paid!=null)
			                         { foreach($fees_paid as $fee)
			                              $paid_fees_array[] = $fee->id_fee;
			                           }
			                    	
			                    					  
								 
								 $program= getProgramByStudentId($stud_id);
								 
								 $level= getLevelByStudentId($stud_id);
								 
								   //return id_fee, fee_name
								$modelBill = new SrcBillings();
								$fees_pending = $modelBill->searchPendingFeesByStudentId($program, $level, $acad);
			                 		
			                 		      if($fees_pending!=null)
							                     {  
								                   foreach($fees_pending as $fees_pen)
								                      {  
						 	                              if (!in_array($fees_pen->id_fee, $paid_fees_array))  
								                          {  
								                          	if($pending_balance==null)
								                          	 {
								                          	  if($fees_pen->fee_label !="Pending balance")
								                          	    { 
								                          	         $total_pending_amount = $total_pending_amount + $fees_pen->amount;	
								                          	    }
								                          	    
								                          	 }
								                            else
								                              {
								                              	    if($fees_pen->fee_label !="Pending balance")
								                          	            $total_pending_amount = $total_pending_amount + $fees_pen->amount;
								                          	            
								                          	   } 
								                          	 		                              
								                           }
		 						                             
														}
														
											       }
									
			                    } //echo Yii::t('app','Pending Fee(s) ').': ('.numberAccountingFormat($total_pending_amount).')'; 
		?>	        
			        <tr class="odd"><th><?php echo Yii::t('app','Pending Fee(s) '); ?></th></tr>
			        <tr class="odd">
			        <td>
			
			           <?php // 
			               
			               if($status_==1)
			                 {  
			                 		      if($fees_pending!=null)
							                     {  
								                   foreach($fees_pending as $fees_pen)
								                      {  
						 	                           if (!in_array($fees_pen->id_fee, $paid_fees_array))  
								                          {  
								                          	if($pending_balance==null)
								                          	 {
								                          	  if($fees_pen->fee_label !="Pending balance")
								                          	    { 
								                          	   
					//echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($fees_pen->amount).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen->id_fee.'" id="'.$fees_pen->id_fee.'"  value="'.$fees_pen->id_fee.'" disabled="disabled" ></div></div></div>';
					                                                 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($fees_pen->amount).') <i class="fa fa-square-o" aria-hidden="true"></i></div></div></div>';	
								                          	    }
								                          	    
								                          	 }
								                            else
								                              {
								                              	    if($fees_pen->fee_label !="Pending balance")
								                          	            $pending_balance = $fees_pen->amount;
								                          	            
								                          	         //echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($pending_balance).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen->id_fee.'" id="'.$fees_pen->id_fee.'"  value="'.$fees_pen->id_fee.'" disabled="disabled" ></div></div></div>';
								                          	         
								                          	         echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen->fee_label).'('.numberAccountingFormat($pending_balance).') <i class="fa fa-square-o" aria-hidden="true"></i></div></div></div>';
								                              	} 
								                          	 		                              
								                           }
		 						                             
														}
														
											       }
									
			                    }
			                 	       				 
				                     ?>        
			        
			        </td>
			        </tr> 
			        

        
         
               </table>

       </div>      
	    
	    
	     <div class="col-lg-7 billings-view">
   
   <?php
            if(!isset($_GET['id_bil'])||(isset($_GET['id_bil'])&&($_GET['id_bil']==''  )  ))
              $last_transaction_id = $modelBill->getLastTransactionIDForGuest($stud_id, $condition, $acad);
            else
               $last_transaction_id = $_GET['id_bil']; 
          
          $modelBillView = SrcBillings::findOne($last_transaction_id);
          
          if($modelBillView!=null)
            {
   ?>
		    <?= DetailView::widget([
		        'model' => $modelBillView,
		        'attributes' => [
		            'id',
		            //'student',
		            'feePeriod.fee0.fee_label',
		            //'amount_to_pay',
		            ['attribute'=>'amount_to_pay','value'=>$modelBillView->getAmountToPay()],
		            //'amount_pay',
		            ['attribute'=>'amount_pay','value'=>$modelBillView->getAmountPay()],
		            //'balance',
		            ['attribute'=>'balance','value'=>$modelBillView->getBalance()],
		            'date_pay:date',
		            'paymentMethod.method_name',
		            'comments:ntext',
		            //'academicYear.name_period',
		            
		            
		        ],
		    ]) ?>
		
		<?php }    ?>
		</div>
		
		<div class="col-lg-2 text-center">
                                                        <?php 
                                                        $program_='';
                                                       if(isset($student->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program_ = $student->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program_ ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($birthday).Yii::t('app',' yr old').' )'; 
                                                        }
					         	else
					         	  echo '';
					         	  
					         	                 
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            
                                                            <?php if($image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/$image")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
?>
                            
                                                       
          </div> 
          
<div style="clear:both"></div>

<div class="row">

<ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#transaction_list" aria-expanded="true"><?= Yii::t('app', 'Transactions list') ?> </a></li>
                            <li class=""><a data-toggle="tab" href="#payment_receipt" aria-expanded="false"><?= Yii::t('app', 'Payment receipt') ?> </a></li>
                           
</ul>



<div class="wrapper wrapper-content tab-content">
                           
         <!--  ************************** Transactions list *************************    -->
         <div id="transaction_list" class="tab-pane active">

	       <?php
	
						 $searchModel = new SrcBillings();
                         $dataProvider = $searchModel->searchForView($stud_id,$acad);
							 echo GridView::widget([
							        'dataProvider' => $dataProvider,
							        'id'=>'billing-grid',
							       
							        'summary'=>"",
							        'columns' => [
							            ['class' => 'yii\grid\SerialColumn'],
                                      
                                                                   'date_pay:date',
							            
                                                              /*     ['attribute'=>'id',
										'label' => Yii::t('app', 'Transaction ID'),
							               'content'=>function($data,$url){
								                return Html::a($data->id, Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->id,
							                        ]);
							               }
							             ],
                                                               * 
                                                               */
							
							           ['attribute'=>'fee_period',
										'label' => Yii::t('app', 'Fee'),
                                         //'value'=>'feePeriod.fee0.fee_label',
                                          'content'=>function($data,$url){
								                return Html::a($data->feePeriod->fee0->fee_label, Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->feePeriod->fee0->fee_label,
							                        ]);
							               }                                    
                                                                        ],
							
							          
							            ['attribute'=>'amount_to_pay',
										'label' => Yii::t('app', 'Amount To Pay'),
                                         //'value'=>'amountToPay',
                                         'content'=>function($data,$url){
								                return Html::a($data->getAmountToPay(), Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->getAmountToPay(),
							                        ]);
							               }	
                                                                        
							             ],
							             
							           
							            ['attribute'=>'amount_pay',
										'label' => Yii::t('app', 'Amount Pay'),
                                          //'value'=>'amountPay',
                                          'content'=>function($data,$url){
								                return Html::a($data->getAmountPay(), Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->getAmountPay(),
							                        ]);
							               }
                                                                       
							             ],
							             
							           ['attribute'=>'balance',
										'label' => Yii::t('app', 'Balance'),
                                         //'value'=>'balance',
                                         'content'=>function($data,$url){
								                return Html::a($data->balance, Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->balance,
							                        ]);
							               }
                                                                        
							             ],  
                                                                             
                                                                    ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
										   'label' => Yii::t('app', 'Payment M.'),
                                         //'value'=>'paymentMethod.method_name',
                                         'content'=>function($data,$url){
								                return Html::a($data->paymentMethod->method_name, Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->paymentMethod->method_name,
							                        ]);
							               }
                                                                         
							             ],
                                                                    
							            ['attribute'=>'num_chekdepo',
										'label' => Yii::t('app', '# check/deposit'),
							               'content'=>function($data,$url){
								                return Html::a($data->num_chekdepo, Yii::getAlias('@web').'/guest/academic/academic?id_bil='.$data->id.'#tab-3', [
							                                    'title' => $data->num_chekdepo,
							                        ]);
							               }
							             ], 
							           
							                       
							        ],
							    ]); 
							      
							 ?>
		</div>
		
 <!--  ************************** payment receipt *************************    -->
    
    <div id="payment_receipt" class="tab-pane">
       
       
   <?php
	
		                                                 //Extract school name 
								               $school_name = infoGeneralConfig('school_name');
                                                                                                //Extract school address
				   								$school_address = infoGeneralConfig('school_address');
                                                                                                //Extract  email address 
                                               $school_email_address = infoGeneralConfig('school_email_address');
                                                                                                //Extract Phone Number
                                                $school_phone_number = infoGeneralConfig('school_phone_number');
                                                
                                                 $school_acronym = infoGeneralConfig('school_acronym');
     
													$school_name_school_acronym = $school_name; 
													
													if($school_acronym!='')
													   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';




	//get program for this student
								$program = getProgramByStudentId($stud_id);
								
  
$programName='';
         if($program!='')
           { $modelProgram = SrcProgram::findOne($program);
           	 $programName = $modelProgram->label;
           	 
           }

$acadModel = SrcAcademicperiods::findOne($acad);         
       //  $student=$this->getStudent($model->student);
         
		 

?>  

			<div id="print" >
			 
			<?php	echo ' <div id="header" style="display:none; ">
                 
                  <div >
                  <table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> <img style="width: 170px;" class="img-circle"  src="'. Yii::$app->request->baseUrl.'/img/Logo_Cana_Reel_G.gif" >
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
				 <br/></div>  
                  
                  <br/>  
                  
                  <br/> 
                  
                  <div  style="text-align:center; "> <b>'.strtoupper(strtr(Yii::t('app','Payment receipt'), pa_daksan() )).'</b></div>';
                  
               ?> 
            				
			
				<table   >
				  <tbody>
				    <tr>
				     <td > <strong><?=  Yii::t('app', 'First name & Last name') ?>: </strong> <?= $student->getFullName()  ?><br/>
				           <strong><?=  Yii::t('app', 'Program') ?>: </strong> <?= $programName ?><br/>
				           <strong><?=  Yii::t('app', 'Year') ?>: </strong> <?=  $acadModel->name_period ?> 
				     </td>
				     
				  </tr>
					  
				  </tbody>  
			   </table>
			   
			   <br/>
			<?php   
			    echo '</div>'; 
              ?>

				<?php			   
							   $searchModel = new SrcBillings();
				              $dataProvider = $searchModel->searchForView($stud_id,$acad);
				 echo GridView::widget([
				        'dataProvider' => $dataProvider,
				        'id'=>'billing-grid',
				        //'filterModel' => $searchModel,
				        'summary'=>"",
				        'columns' => [
				         //   ['class' => 'yii\grid\SerialColumn'],
				
				            //'id',
				            [//'attribute'=>'id',
							'label' => Yii::t('app', 'Date Pay'),
				               'content'=>function($data,$url){
					                return Yii::$app->formatter->asDate($data->date_pay );
				               }
				             ],
				            
				            
				          /*  [//'attribute'=>'id',
							'label' => Yii::t('app', 'Transaction ID'),
				               'content'=>function($data,$url){
					                return $data->id;
				               }
				             ],
                                           * 
                                           */
				
				            //'fee_period',
				            [//'attribute'=>'fee_period',
							'label' => Yii::t('app', 'Fee'),
				               'content'=>function($data,$url){
					                return $data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->program0->label.')';
				               }
				             ],
				
				            //'amount_to_pay',
				            [//'attribute'=>'amount_to_pay',
							'label' => Yii::t('app', 'Amount To Pay'),
				               'content'=>function($data,$url){
					                return $data->getAmountToPay();
				               }
				             ],
				             
				            //'amount_pay',
				            [//'attribute'=>'amount_pay',
							'label' => Yii::t('app', 'Amount Pay'),
				               'content'=>function($data,$url){
					                return $data->getAmountPay();
				               }
				             ],
				             
				            //'balance',
				             [//'attribute'=>'balance',
							'label' => Yii::t('app', 'Balance'),
				               'content'=>function($data,$url){
					                return $data->getBalance();
				               }
				             ],
                                                     
                                                     
                                            ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
							   'label' => Yii::t('app', 'Payment M.'),
				               'content'=>function($data,$url){
					                return $data->paymentMethod->method_name;
				               }
				             ],
				          
				             
                                            
                                            ['attribute'=>'num_chekdepo',
                                                'label' => Yii::t('app', '# check/deposit'),
                                       'content'=>function($data,$url){
                                                return $data->num_chekdepo;
                                               }
                                             ], 
				           
				                       
				        ],
				    ]); 
				?>		       
		       
			
		
			 
			      

			                
			
			</div>
			
			      
      

            <?php if($dataProvider->getModels()!=null)
                      {
             ?>
  
  
                            <div class="row">
						        <div class="col-sm-2" style="float:right">
						           <div class="form-group">
						        
						                <button onclick="printContent('print')" class = 'btn btn-success'><?= Yii::t('app', 'Print') ?></button>
						      						        
						            </div>
						        </div>
						        
						    </div>
	        <?php   }  ?>					    

           </div>					 
  </div>
  
</div>							 
                              </div>


     	                        </div>
                            </div>
                            
                            
                            
                             <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <h3><?= Yii::t('app', 'Contact Persons') ?> </h3>
                                        </div>
                                        <div class="col-lg-10">
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
           
                                         
                                          <?php
                                           
                                          $i = 1;
                                          
                                          if($modelContactStudent==null)
                                          {   
                                              $mcs = new SrcContactInfo;
                                              
                                              $stud_id = 0;
                                              
                                              //gade si se paran oswa etidyan
                                               $userid= Yii::$app->user->identity->id; 
            
                                                $user_ = User::findOne($userid);

                                                if($user_->is_parent==0) 
                                                   $stud_id = $user_->person_id; 
                                                
                                             
                                               ?>
                                                    
                                    <table id="w0" class="table table-striped table-bordered detail-view table-responsive">
                                        <thead>
                                            <tr>
                                                <th colspan="4"><?= Yii::t('app','Contact # {number}',['number'=>$i])?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr>
                                                <th><?= Yii::t('app','Contact name'); ?></th>
                                                <td>
                                                    
                                                    <?php 
                                                          if(! isset($mcs->contact_name)){
                                                                ?>
                                                                <a href="#" class="contactName" data-name="contactName"  data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"  data-url="set-contactstudent?set=contactName&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Contact name') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="contactName" data-name="contactName" data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?is=up&set=contactName&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Contact name') ?>">
                                                                     <?= $mcs->contact_name ?>
                                                                 </a>
                                                                <?php } ?>
                                            
                                                </td>
                                                <th>
                                                    <?= Yii::t('app','Relation contact'); ?>
                                                </th>
                                                <td>
                                                      <?php 
                                                            if(! isset($mcs->relationName)){
                                                                ?>
                                                                <a href="#" class="relation" data-name="relation"  data-type="select" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"  data-source="relationlist" data-url="set-contactstudent?set=relation&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Relation contact') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="relation" data-name="relation" data-type="select" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"  data-source="relationlist" data-url="set-contactstudent?is=up&set=relation&stud=<?= $stud_id ?>" data-value="<?= $mcs->contact_relationship ?>" data-title="<?= Yii::t('app','Relation contact') ?>">
                                                                     <?= $mcs->relationName ?>
                                                                 </a>
                                                                <?php } ?>
                                                </td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Profession'); ?></th>
                                             <td>
                                                   <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="profession" data-name="profession"  data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?set=profession&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Profession') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="profession" data-name="profession" data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?is=up&set=profession&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Profession') ?>">
                                                                     <?= $mcs->profession ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                             <th><?= Yii::t('app','Phone'); ?></th>
                                             <td>
                                                    <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="phone1" data-name="phone"  data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?set=phone&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Phone') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="phone1" data-name="phone" data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?is=up&set=phone&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Phone') ?>">
                                                                     <?= $mcs->phone ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Email'); ?></th>
                                             <td>
                                                    <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="email1" data-name="email"  data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?set=email&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Email') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="email1" data-name="email" data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?is=up&set=email&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Email') ?>">
                                                                     <?= $mcs->email ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                             <th><?= Yii::t('app','Address'); ?></th>
                                             <td>
                                                   <?php 
                                                          if(! isset($mcs->address)){
                                                                ?>
                                                                <a href="#" class="address1" data-name="address"  data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?set=address&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Address') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="address1" data-name="address" data-type="text" data-pk="<?= $mcs->id ?>" data-stud="<?= $stud_id ?>"   data-url="set-contactstudent?is=up&set=address&stud=<?= $stud_id ?>" data-title="<?= Yii::t('app','Address') ?>">
                                                                     <?= $mcs->address ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                         </tr>
                                          </tbody>
                                    
                                    </table>
                                          <?php
                                          
                                          }
                                          else 
                                            {
                                          
                                           foreach ($modelContactStudent as $mcs){
                                                $modelContactInfo = ContactInfo::findOne($mcs->id);
                                               ?>
                                                    
                                    <table id="w0" class="table table-striped table-bordered detail-view table-responsive">
                                        <thead>
                                            <tr>
                                                <th colspan="4"><?= Yii::t('app','Contact # {number}',['number'=>$i])?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr>
                                                <th><?= Yii::t('app','Contact name'); ?></th>
                                                <td>
                                                    
                                                    <?php 
                                                          if(! isset($mcs->contact_name)){
                                                                ?>
                                                                <a href="#" class="contactName" data-name="contactName"  data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?set=contactName" data-title="<?= Yii::t('app','Contact name') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="contactName" data-name="contactName" data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?is=up&set=contactName" data-title="<?= Yii::t('app','Contact name') ?>">
                                                                     <?= $mcs->contact_name ?>
                                                                 </a>
                                                                <?php } ?>
                                            
                                                </td>
                                                <th>
                                                    <?= Yii::t('app','Relation contact'); ?>
                                                </th>
                                                <td>
                                                      <?php 
                                                            if(! isset($mcs->relationName)){
                                                                ?>
                                                                <a href="#" class="relation" data-name="relation"  data-type="select" data-pk="<?= $mcs->id ?>" data-source="relationlist" data-url="set-contactstudent?set=relation" data-title="<?= Yii::t('app','Relation contact') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="relation" data-name="relation" data-type="select" data-pk="<?= $mcs->id ?>" data-source="relationlist" data-url="set-contactstudent?is=up&set=relation" data-value="<?= $mcs->contact_relationship ?>" data-title="<?= Yii::t('app','Relation contact') ?>">
                                                                     <?= $mcs->relationName ?>
                                                                 </a>
                                                                <?php } ?>
                                                </td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Profession'); ?></th>
                                             <td>
                                                   <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="profession" data-name="profession"  data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?set=profession" data-title="<?= Yii::t('app','Profession') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="profession" data-name="profession" data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?is=up&set=profession" data-title="<?= Yii::t('app','Profession') ?>">
                                                                     <?= $mcs->profession ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                             <th><?= Yii::t('app','Phone'); ?></th>
                                             <td>
                                                    <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="phone1" data-name="phone"  data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?set=phone" data-title="<?= Yii::t('app','Phone') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="phone1" data-name="phone" data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?is=up&set=phone" data-title="<?= Yii::t('app','Phone') ?>">
                                                                     <?= $mcs->phone ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Email'); ?></th>
                                             <td>
                                                    <?php 
                                                          if(! isset($mcs->profession)){
                                                                ?>
                                                                <a href="#" class="email1" data-name="email"  data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?set=email" data-title="<?= Yii::t('app','Email') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="email1" data-name="email" data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?is=up&set=email" data-title="<?= Yii::t('app','Email') ?>">
                                                                     <?= $mcs->email ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                             <th><?= Yii::t('app','Address'); ?></th>
                                             <td>
                                                   <?php 
                                                          if(! isset($mcs->address)){
                                                                ?>
                                                                <a href="#" class="address1" data-name="address"  data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?set=address" data-title="<?= Yii::t('app','Address') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="address1" data-name="address" data-type="text" data-pk="<?= $mcs->id ?>"  data-url="set-contactstudent?is=up&set=address" data-title="<?= Yii::t('app','Address') ?>">
                                                                     <?= $mcs->address ?>
                                                                 </a>
                                                                <?php } ?>
                                             </td>
                                         </tr>
                                          </tbody>
                                    
                                    </table>
                                          <?php
                                          $i++;
                                           }
                                           
                                        }
                                          ?>
                                            
                                       
                                          </div>
                                    </div>
                                    </div>
                                   
                                    
                                   
                                    
                                    
                                </div>
                            </div>
                           
</div>   

<script>
      function printContent(el)
      {
          document.getElementById("header").style.display = "block";
		  //  document.getElementById("emp_name").style.display = "block";
		 //   document.getElementById("p_month").style.display = "block";
		  //  document.getElementById("p_date").style.display = "block";
		     
     
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
         
          
     document.getElementById("header").style.display = "none";
     // document.getElementById("emp_name").style.display = "none";
    //document.getElementById("p_month").style.display = "none";
    //document.getElementById("p_date").style.display = "none";
     }
   </script>
   
   
   
<?php

    
    $script = <<< JS
    $(document).ready(function(){
             $('.email').editable();
            $('.phone').editable();
            
            $('.city').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            
            $('.adresse').editable();
            $('.sexe').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            $('.gs').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            $('.nif_cin').editable();
            
            
            
            $('.contactName').editable();
            $('.relation').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            $('.profession').editable();
            $('.email1').editable();
            $('.phone1').editable();
            $('.address1').editable();
            
            
            

        });

JS;
$this->registerJs($script);






?>
              