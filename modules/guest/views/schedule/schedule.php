<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\SrcEvents;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



?>
<div class="row">
     <div class="col-xs-12 col-md-2 col-lg-2">
        <!-- New format -->
        <h4>
           <?= Yii::t('app','Closest Courses'); ?>
        </h4>

        <div class="ibox float-e-margins">
                
                <div class="ibox-content">
                    <div id="external-events">

        <?php
            $i=1;
            foreach($courseProche as $cp){
        ?>
        <div  title="<?= SrcCourses::findOne(['id'=>$cp->course])->courseName.' ['.Yii::$app->formatter->asTime($cp->time_start).Yii::t('app',' to ').Yii::$app->formatter->asTime($cp->time_end).']'; ?>" class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: <?= $cp->color ?>">

                <span class="pull-right">
                 <?= Yii::$app->formatter->asDate($cp->date_start) ?>

                </span>
                <?= SrcCourses::findOne(['id'=>$cp->course])->module0->code; ?>

        </div>
            <?php

            $i++;
            } ?>
          </div>

                    </div>
                </div>

        <div class="row">

              <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h4>
                        <?= Yii::t('app','Legend'); ?>
                    </h4>

                </div>
                <div class="ibox-content">
                    <div id="external-events">


        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #FF9900">

                <span class="pull-right">
                </span>
                <?= Yii::t('app','Lesson complete') ?>

        </div>
        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #EE82EE">

                <span class="pull-right">
                </span>
                <?= Yii::t('app','Lesson postpone') ?>

        </div>
        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #980000 ">

                <span class="pull-right">
                </span>
                <?= Yii::t('app',"It's a holiday") ?>

        </div>

          </div>

                    </div>
                </div>
        </div>

            </div>
    <div class="col-xs-12 col-md-10 col-lg-10">
        <div id='calendar'></div>

    </div>
</div>

<?php
        $str_json = "";
        foreach($allEvents as $e){
         $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
        }
$language = Yii::$app->params["language"];
$script2 = <<< JS
$('#calendar').fullCalendar({
			locale: '$language',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listMonth'
			},
			defaultDate: Date.now(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true,
			events: [
                                $str_json
                            ],
                        eventClick:  function(event, jsEvent, view) {
                           $('#modalTitle').html(event.title);
                           $("#dateStart").html(moment(event.start).format('Do MMMM YYYY h:mm A'));
                           $("#dateEnd").html(moment(event.end).format('Do MMMM YYYY h:mm A'));
                           $("#lessonStatus").html(event.lessonStatus);
                           $("#program").html(event.program);
                           $("#shift").html(event.shift);
                           $("#dateReport").html(moment(event.report_to).format('Do MMMM YYYY h:mm A'));
                           $('.rapote').hide();
                           var lesson_complete = event.lesson_complete;
                           if(lesson_complete == -1){
                               $('.kacheFet').hide();
                               $('.maske').hide();

                            }else{
                                $('.kacheFet').show();
                                $('.maske').show();
                                }
                           if(lesson_complete==1 || lesson_complete==2){
                                $('.kacheFet').show();
                                $('.maske').hide();
                                }else if(lesson_complete!=-1){

                                    $('.maske').show();

                                }
                            if(lesson_complete==2){
                                    $('.rapote').show();
                                }
                           var jodia = new Date();
                           if(event.start >= jodia){
                                if(lesson_complete == -1){
                                    $('.kacheFet').hide();
                                    $('.maske').hide();
                                }
                               $('.fiti').hide();
                               $('fenmen').show();
                            }else{
                                if(lesson_complete == -1){
                                    $('.kacheFet').hide();
                                    $('.maske').hide();
                                }
                                $('.fiti').show();
                                $('fenmen').show();
                                }
                            var initial_date_course_end = event.initial_date_course_end;
                            if(initial_date_course_end=='anyen'){
                                $('#eventDelete').hide();
                                }else{
                                    $('#eventDelete').show();
                                }

                           $('#eventComplete').attr('href',event.url_complete);
                           $('#eventMove').attr('href',event.url_move);
                           $('#eventMoveChoose').attr('href',event.url_move_choose);
                           $('#eventTeacherAttendance').attr('href',event.url_attendance_teacher);
                           $('#eventDelete').attr('href',event.url_delete_event);
                           $('#eventStudentAttendance').attr('href',event.url_attendance_student);
                           $('#fullCalModal').modal();
                    }
		});
JS;
$this->registerJs($script2);

?>