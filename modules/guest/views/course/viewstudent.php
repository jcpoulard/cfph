<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcCourses;

if(isset($_GET['id'])&&($_GET['id']!='')){
    $course  = $_GET['id'];
//$nom_cours = $searchModel->getSubjectName($course);

$course_info = SrcCourses::findOne($course);
        
$title_part1 =$course_info->module0->subject0->subject_name;// Yii::t('app', 'List student for {coursename}',['coursename'=>$nom_cours]);
//$title_part2 =$course_info->module0->program0->label.' / '.$course_info->shift0->shift_name.' / '.$course_info->room0->room_name;
$title_part2 =$course_info->module0->program0->label.' / '.$course_info->shift0->shift_name;
}else{
    $title_part1 = Yii::t('app','Course not found');
}
?>

   <span style="font-size:14px"><b><?= Html::encode($title_part1) ?> </b> ( </span><span style="font-size:12px"><?= Html::encode($title_part2) ?></span> <span style="font-size:14px"> )</span>
   
<br/><br/>

<div class="row">
    
    <div class="col-lg-12 table-responsive">
        <table class='table table-striped table-bordered table-hover dataTables-example' id="tablo-etidyan">
         <thead>
            <tr>
            <th><?= Yii::t('app','Student'); ?></th>
            <th><?= Yii::t('app','Gender'); ?></th>
            <th><?= Yii::t('app','Email'); ?></th>
            <th><?= Yii::t('app','Level'); ?></th>
            
            </tr>
        </thead>
           <tbody>

 <?php 
 
         $dataProv = $dataProvider->getModels();
     foreach($dataProv as $data)
        {
            ?>
            <tr>
	            <td><?= $data->student0->getFullName(); ?></td>
	            <td><?= $data->student0->geTGenders(); ?></td>
	            <td><?= $data->student0->email ; ?>  </td>
	            <td><?= Yii::t('app','Level').'-'.$data->student0->studentLevel->level; ?></td>
	            
	
             </tr>
  <?php } 
  
  
  ?>
        
           </tbody>
        </table>


    </div>
    
</div>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('#tablo-etidyan').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Student In Course List'},
                    {extend: 'pdf', title: 'Student In Course List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);
 
 

?>


