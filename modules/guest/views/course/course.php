<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 
use yii\widgets\Pjax;

use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcCourses;

use yii\bootstrap\Modal; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Courses/Modules enrolled');

$acad = Yii::$app->session['currentId_academic_year'];
?>


 <div class="row">
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 



<div class="wrapper wrapper-content  courses-index">   
           

               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th></th>
				            <th><?= Yii::t('app','Module'); ?></th>
				            <th><?= Yii::t('app','Duration'); ?></th>
				            <th><?= Yii::t('app','Passing Grade'); ?></th>
				            <th><?= Yii::t('app','Teacher'); ?></th>
				            <th><?= Yii::t('app','Room'); ?></th>
				            <th><?= Yii::t('app','Shift'); ?></th>
				            
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    
    
    $modelCouse = new SrcCourses;
    
          foreach($dataProvider as $course)
           {
           	   echo '  <tr ><td >'; 
           	                     if($modelCouse->studentsByCourse($course->course0->id)->getModels() !=null )
           	                        {                         ?>
                    <a href="#" title="<?= Yii::t('app', 'View student');?>" class='popopKou' data-idcourse="<?= $course->course0->id; ?>"><span class="fa fa-group"></span></a>
                    <?php             }
                                  else
                                    {
                                    	?>
                         <a href="#" title="<?= Yii::t('app', 'View student');?>" class='popopKou' data-idcourse="<?= $course->course0->id; ?>"><span style="color:#d6c2c2" class="fa fa-user-times"></span></a>           	
                    
                    <?php             } 
                    
                    
                    echo '</td >
                                                    <td >'.$course->course0->module0->subject0->subject_name.'</td>
                                                    <td >'.$course->course0->module0->duration.'</td>
                                                    <td >'.$course->course0->passing_grade.'</td>
                                                    <td >'.$course->course0->teacher0->getFullName().' </td>
                                                    <td >'.$course->student0->studentLevel->room0->room_name.'</td>
                                                    <td >'.$course->course0->shift0->shift_name.'</td>
                                                    
                                                    
                                                    
                                                    '; 
                                                              
                                             echo ' </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>
				    
                
 </div>



<?php
   // 3/05/2018, komante pou pa anpeche signout. a resourdre 
     Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Students enrolled ').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md',
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            ]
            ); 
    echo '<div id="modalContent"><div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';
    
    Modal::end(); 
    
    //
    ?>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'My Courses'},
                    {extend: 'pdf', title: 'My Courses'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(function(){
    $(document).on('click','.popopKou',function(){
        var idcourse = $(this).attr('data-idcourse');
       // alert("$baseUrl");
        $.get('$baseUrl/index.php/guest/course/viewstudent',{'id':idcourse},function(data){
            
            $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });
      
    });
    
});
JS;
$this->registerJs($script2);

?>
