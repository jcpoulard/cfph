<?php

namespace app\modules\guest\controllers;

use yii\web\Controller;

/**
 * Default controller for the `guest` module
 */
class BillingsController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionBillings()
    {
        return $this->render('billings');
    }
}
