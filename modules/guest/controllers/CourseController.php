<?php

namespace app\modules\guest\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use app\modules\rbac\models\User; 
use app\modules\fi\models\SrcPersons; 
use app\modules\fi\models\SrcContactInfo; 
use app\modules\fi\models\SrcCourses; 
use app\modules\fi\models\SrcStudentHasCourses; 


/**
 * Default controller for the `guest` module
 */
class CourseController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * Renders the index view for the module
     * @return string
     */
   
    
public function actionCourse()
    {
        if(Yii::$app->user->can('guest'))
          {
             
             $modelContactInfo = new SrcContactInfo(); // a enlever apres 
        if(isset(Yii::$app->session['currentId_academic_year']))
               $acad = Yii::$app->session['currentId_academic_year'];
        else $acad = 1;
       
       if(isset(Yii::$app->user->identity->username))
         {
            $userid= Yii::$app->user->identity->id; 
            
            $user_ = User::findOne($userid);
            
            if($user_->is_parent==0) 
               $stud_id = $user_->person_id; 
            else
              {
              	 $modelContactStudent = SrcContactInfo::findOne($user_->person_id);
              	 
              	 $stud_id = $modelContactStudent->person; 
              	}
          }
        else
          {
            $stud_id = -1;
            return $this->redirect("../../rbac/user/login"); 
           }
           
        //$modelStudent = SrcPersons::findOne($stud_id );
        
	        $searchModel = new SrcStudentHasCourses();
	        $dataProvider = $searchModel->getCourseByIdStudent($stud_id);
	        
	              	 
	          
	        if (isset($_GET['pageSize'])) 
         	 {
		        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
		          unset($_GET['pageSize']);
			   }
	
	        return $this->render('course', [
	            'dataProvider' => $dataProvider,
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
        

    }    
    
    
 
 public function actionViewstudent()
    {
      if(Yii::$app->user->can("guest"))
       {
            $searchModel = new SrcCourses();
            
            $course = $_GET['id']; 
            $dataProvider = $searchModel->studentsByCourse($course); 
             
            if (isset($_GET['pageSize'])) 
         	 {
		        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
		          unset($_GET['pageSize']);
			   }
	
	        return $this->renderPartial('viewstudent', [
	            'dataProvider' => $dataProvider,
	        ]); 
        }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }   
    
    
    
}
