<?php

namespace app\modules\guest\controllers;

use yii\web\Controller;
use Yii;
use app\modules\planning\models\Events;
use app\modules\planning\models\SrcEvents;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\planning\models\Holydays; 
use app\modules\fi\models\SrcCourses; 
use app\modules\fi\models\Courses; 
use app\modules\fi\models\Module; 
use app\modules\fi\models\Program; 
use yii\helpers\Html; 
use yii\web\ForbiddenHttpException;
 
use yii\db\IntegrityException;

use app\modules\rbac\models\User; 
use app\modules\fi\models\SrcPersons; 

/**
 * Default controller for the `guest` module
 */
class ScheduleController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSchedule()
    {
        
        if(Yii::$app->user->can('guest')){ 
        
        if(isset(Yii::$app->user->identity->username))
         {
            $userid= Yii::$app->user->identity->id; 
            
            $user_ = User::findOne($userid);
            
            if($user_->is_parent==0) 
               $stud_id = $user_->person_id; 
            else
              {
              	 $modelContactStudent = SrcContactInfo::findOne($user_->person_id);
              	 
              	 $stud_id = $modelContactStudent->person; 
              	}
          }
        else{
            $stud_id = -1;
            return $this->redirect("../../rbac/user/login");
           }
           
        $course = new SrcCourses(); 
        $course_this_student =$course->getCourseByStudentID($stud_id);
        $string_condition = "(";
        foreach($course_this_student as $cts){
            $string_condition.=$cts->id.","; 
        }
        $string_condition = $string_condition."-1)"; 
        
        $searchModel = new SrcEvents();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        
        $holydays = Holydays::find()->all(); 
        $eventsHolydays = [];
        //$schedules = SrcEvents::find()->all(); 
        $schedules = SrcEvents::findBySql("SELECT * FROM events WHERE course IN $string_condition")->all();
        $schedulesEvent = [];
       
        $j = 0; 
        $k = 0; 
        foreach($holydays as $holyday){
            $eventsHolydays[$j]['id']  = $holyday->id; 
            $eventsHolydays[$j]['title'] = $holyday->name; 
            $eventsHolydays[$j]['start'] = $holyday->date_start; 
            $eventsHolydays[$j]['end'] = date("Y-m-d",strtotime($holyday->date_end."+1 day"));
            $eventsHolydays[$j]['color'] = $holyday->color;
            $eventsHolydays[$j]['allDay'] = 1; 
            $eventsHolydays[$j]['type'] = "holiday";
            $eventsHolydays[$j]['lesson_complete'] = -1;
            $eventsHolydays[$j]['lessonStatus'] = Yii::t('app','Holiday'); 
            $eventsHolydays[$j]['editable'] = false;
            $j++; 
            
        }
        foreach($schedules as $schedule){
            $schedulesEvent[$k]['id'] = $schedule->id; 
            $schedulesEvent[$k]['title'] = $course::findOne(['id'=>$schedule->course])->CourseName;
            $schedulesEvent[$k]['desciption'] = $schedule->description; 
            $schedulesEvent[$k]['start'] = $schedule->date_start; 
            $schedulesEvent[$k]['end'] = $schedule->date_end; 
            $schedulesEvent[$k]['color'] = $schedule->color; 
            $schedulesEvent[$k]['allDay'] = 0; 
            $schedulesEvent[$k]['type'] = "schedule";
            $schedulesEvent[$k]['lessonStatus'] = $schedule->lessoncomplete; 
            $schedulesEvent[$k]['lesson_complete'] = $schedule->lesson_complete;
            $schedulesEvent[$k]['report_to'] = $schedule->report_to; 
            $schedulesEvent[$k]['url_complete'] = "markascomplete?id=$schedule->id";
            $schedulesEvent[$k]['url_move'] = "movelesson?id=$schedule->id&course=$schedule->course";
            $schedulesEvent[$k]['program'] = $schedule->course0->module0->program0->label;
            $schedulesEvent[$k]['shift'] = $schedule->course0->shift0->shift_name; 
            $schedulesEvent[$k]['url_move_choose'] = "movelessonchoose?id=$schedule->id&course=$schedule->course";
            $schedulesEvent[$k]['url_attendance_teacher'] = "../teacherattendance/create?id=$schedule->id&course=$schedule->course&teacher=".$schedule->course0->teacher."&wh=te_att&shift=".$schedule->course0->shift0->id;
            $schedulesEvent[$k]['url_attendance_student'] = "../studentattendance/create?id=$schedule->id&course=$schedule->course&shift=".$schedule->course0->shift0->id."&wh=st_att"; 
            $schedulesEvent[$k]['url_delete_event'] = "deleteevent?id=$schedule->id";
            if($schedule->initial_date_course_end!=null){
            $schedulesEvent[$k]['initial_date_course_end'] = $schedule->initial_date_course_end;
            }else{
                $schedulesEvent[$k]['initial_date_course_end'] = "anyen";
            }
            $k++; 
            
        }
        
        $allEvents = array_merge($eventsHolydays, $schedulesEvent); 
        $sql = "SELECT * FROM events where YEAR(date_start) = YEAR(NOW()) AND course IN $string_condition ORDER BY ABS(DATEDIFF( date_start, NOW())), date_start DESC LIMIT 10";
        $courseProche = Events::findBySql($sql)->all(); 
        return $this->render('schedule', [
            'allEvents'=>$allEvents,
            'eventsHolydays'=>$eventsHolydays,
            'schedulesEvent'=>$schedulesEvent,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'courseProche'=>$courseProche,
            'string'=>$string_condition,
        ]);
       }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
}
