<?php

namespace app\modules\guest\controllers;

use yii\web\Controller;
use app\modules\rbac\models\User; 
use app\modules\fi\models\SrcPersons; 
use app\modules\fi\models\Cities; 
use app\modules\fi\models\SrcContactInfo; 
use app\modules\fi\models\SrcRelations;
use app\modules\billings\models\SrcBillings; 
use app\modules\billings\models\SrcBalance; 
use Yii;

/**
 * Default controller for the `guest` module
 */
class AcademicController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionAcademic()
    {
     if(Yii::$app->user->can('guest')){
        $modelContactInfo = new SrcContactInfo(); // a enlever apres 
        
        if(isset(Yii::$app->session['currentId_academic_year']))
               $acad = Yii::$app->session['currentId_academic_year'];
        else 
         {  $acad = 1;
            return $this->redirect("../../rbac/user/login"); 
         }
         
         if(isset(Yii::$app->user->identity->username))
         {
            $userid= Yii::$app->user->identity->id; 
            
            $user_ = User::findOne($userid);
            
            if($user_->is_parent==0) 
               $stud_id = $user_->person_id; 
            else
              {
              	 $modelContactStudent = SrcContactInfo::findOne($user_->person_id);
              	 
              	 $stud_id = $modelContactStudent->person; 
              	}
          }
        else{
            $stud_id = -1;
            return $this->redirect("../../rbac/user/login"); 
        }
        $modelStudent = SrcPersons::findOne($stud_id );
        $modelContactStudent = SrcContactInfo::findAll(['person'=>$stud_id]);
        
        $condition_fee_status = ['in','fees_label.status', [0,1] ];
        $modelBilling = new SrcBillings();
        $last_bil_id = $modelBilling->getLastTransactionIDForGuest($stud_id, $condition_fee_status, $acad);
        
        return $this->render('academic',[
            'modelStudent'=>$modelStudent,
            'modelContactInfo'=>$modelContactInfo,
            'modelContactStudent'=>$modelContactStudent,
            'stud_id'=>$stud_id,
            'last_bil_id'=>$last_bil_id,
        ]);
    }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }
    
    
   
  public function actionCitylist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_cities = Cities::findBySql("SELECT id AS 'value', city_name AS 'text'  FROM cities ORDER BY city_name")->asArray()->all();
         $json_cities =json_encode($all_cities,false); 
         return $json_cities; 
       
       
    }
    
  public function actionGenderlist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_gender = [
            ['value'=>0,'text'=>Yii::t('app', 'Male')], 
            ['value'=>1,'text'=>Yii::t('app', 'Female')],
            ];
       
         $json_gender =json_encode($all_gender,false); 
         return $json_gender; 
       
       
    }
    
  public function actionGslist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_gs = [
             [ 'value'=>'O+','text'=>Yii::t('app', 'O+')],
             ['value'=>'O-','text'=>Yii::t('app', 'O-')],
             ['value'=>'A+','text'=>Yii::t('app', 'A+')],
             ['value'=>'A-','text'=>Yii::t('app', 'A-')],
             ['value'=>'B+','text'=>Yii::t('app', 'B+')],
             ['value'=>'B-','text'=>Yii::t('app', 'B-')],
             ['value'=>'AB+','text'=>Yii::t('app', 'AB+')],
             ['value'=>'AB-','text'=>Yii::t('app', 'AB-')], 
                 
                 ];

       
               $json_gs =json_encode($all_gs,false); 
         return $json_gs; 
       
       
    }
  
    public function actionRelationlist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_relations = SrcRelations::findBySql("SELECT id AS 'value', relation_name AS 'text'  FROM relations ORDER BY relation_name")->asArray()->all();
       
         $json_relation =json_encode($all_relations,false); 
         return $json_relation; 
       
       
    }
    
   public function actionSetInfostudent(){
       $acad = Yii::$app->session['currentId_academic_year'];
       
       $modelStudent = SrcPersons::findOne($_POST['pk']);
        $modelContactStudent = SrcContactInfo::findAll(['person'=>$_POST['pk'] ]);
        $modelContact = new SrcContactInfo; 
       
 if(isset($_POST)){
           //if(isset($_GET['is'])){
                
               //$model = SrcContactInfo::findOne($_POST['pk']); 
               $model = SrcPersons::findOne($_POST['pk']);
                
               if($_GET['set']=='email')
                   $model->email = $_POST['value'];
               elseif($_GET['set']=='phone')
                       $model->phone = $_POST['value'];
               elseif($_GET['set']=='city')
                       $model->cities = $_POST['value'];
               elseif($_GET['set']=='adresse')
                       $model->adresse = $_POST['value'];
               elseif($_GET['set']=='sexe')
                       $model->gender = $_POST['value'];
               elseif($_GET['set']=='gs')
                       $model->blood_group = $_POST['value'];
               elseif($_GET['set']=='nifcin')
                       $model->nif_cin = $_POST['value'];
               
               
                
                
                $model->update_by = Yii::$app->user->identity->username;
                $model->date_updated = date('Y-m-d H:i:s'); 
                $model->save(); 
           //}
       }
       
       
    } 
 
    
       public function actionSetContactstudent(){
       $acad = Yii::$app->session['currentId_academic_year'];
       
       $modelContactStudent = SrcContactInfo::findOne($_POST['pk']);
         
       
 if(isset($_POST)){
           //if(isset($_GET['is'])){
                
               //$model = SrcContactInfo::findOne($_POST['pk']); 
             if( isset($_POST['pk']) && ($_POST['pk']!=null ) )
             {
              $model = SrcContactInfo::findOne($_POST['pk']);
                
               if($_GET['set']=='email')
                   $model->email = $_POST['value'];
               elseif($_GET['set']=='phone')
                       $model->phone = $_POST['value'];
               elseif($_GET['set']=='adress')
                       $model->adress = $_POST['value'];
               elseif($_GET['set']=='profession')
                       $model->profession = $_POST['value'];
               elseif($_GET['set']=='relation')
                       $model->contact_relationship = $_POST['value'];
               elseif($_GET['set']=='contactName')
                       $model->contact_name = $_POST['value'];
               
               
                
                
                $model->update_by = Yii::$app->user->identity->username;
                $model->date_updated = date('Y-m-d H:i:s'); 
                $model->save(); 
             }
             else 
              {
                  if($_GET['stud']!=0 )
                    {  
                      $modelContact = SrcContactInfo::find()->where('person='.$_GET['stud'])->all();
                      
                      if($modelContact==null)
                       {  $model = new SrcContactInfo;
                          $model->person = $_GET['stud'];
                          
                          $model->create_by = Yii::$app->user->identity->username;
                            $model->date_created = date('Y-m-d H:i:s'); 
                            $model->save();
                       }
                      else
                        {   foreach($modelContact as $contact)
                              { $model = SrcContactInfo::findOne($contact->id);
                                 break;
                               }
                          
                    
                 
                                if($_GET['set']=='email')
                                  $model->email = $_POST['value'];
                              elseif($_GET['set']=='phone')
                                      $model->phone = $_POST['value'];
                              elseif($_GET['set']=='adress')
                                      $model->adress = $_POST['value'];
                              elseif($_GET['set']=='profession')
                                      $model->profession = $_POST['value'];
                              elseif($_GET['set']=='relation')
                                      $model->contact_relationship = $_POST['value'];
                              elseif($_GET['set']=='contactName')
                                      $model->contact_name = $_POST['value'];




                               $model->update_by = Yii::$app->user->identity->username;
                               $model->date_updated = date('Y-m-d H:i:s'); 
                               $model->save(); 

                           }

                   }
                 
               }
           //}
       }
       
       
    } 
    
    
}
