<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "rpt_custom_cat".
 *
 * @property integer $id
 * @property string $categorie_name
 * @property string $cat
 * @property string $date_create
 * @property string $date_update
 * @property string $create_by
 * @property string $update_by
 */
class RptCustomCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rpt_custom_cat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categorie_name', 'date_create', 'date_update', 'create_by', 'update_by'], 'required'],
            [['date_create', 'date_update'], 'safe'],
            [['categorie_name', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['cat'], 'string', 'max' => 32],
            [['categorie_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'categorie_name' => Yii::t('app', 'Categorie Name'),
            'cat' => Yii::t('app', 'Cat'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
