<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "rpt_custom".
 *
 * @property integer $id
 * @property string $title
 * @property string $data
 * @property string $parameters
 * @property integer $academic_year
 * @property integer $categorie
 * @property string $variables
 * @property string $setup_variable
 * @property string $create_by
 * @property string $create_date
 */
class RptCustom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rpt_custom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'data', 'categorie', 'create_by'], 'required'],
            [['data', 'parameters', 'variables', 'setup_variable'], 'string'],
            [['academic_year', 'categorie'], 'integer'],
            [['create_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['create_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'data' => Yii::t('app', 'Data'),
            'parameters' => Yii::t('app', 'Parameters'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'categorie' => Yii::t('app', 'Categorie'),
            'variables' => Yii::t('app', 'Variables'),
            'setup_variable' => Yii::t('app', 'Setup Variable'),
            'create_by' => Yii::t('app', 'Create By'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }
}
