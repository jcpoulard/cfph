<?php

namespace app\modules\reports\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\billings\models\SrcDevises;
use app\modules\reports\models\RptCustom;
use app\modules\reports\models\RptCustomCat;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;



use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcGrades;


use kartik\mpdf\Pdf;

/**
 * Default controller for the `guest` module
 */
class ReportController extends Controller
{
    
    public $layout = "/inspinia";
    
    public $string_sql = null;
    public $attributes_name = array();
    public $query_yii = null;
    public $part = null;
    public $academic_year=null;
    public $relation_name;
    
    
    /**
     * Renders the index view for the module
     * @return string
     */
   
    
public function actionDashboardpedago()
    {
      ini_set("memory_limit", "-1");
      set_time_limit(0);
    
      // if(Yii::$app->user->can('guest'))
       //   {
      if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
          
           $all_students = loadAllStudentsJson();
        $json_all_students = json_encode($all_students,false);
       
        
	        return $this->render('dashboard_pedago', [
	            'json_all_students' => $json_all_students,
	        ]);
    /*
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
        */

    }    
    
    
    public function actionDashboardbilling()
    {
       // if(Yii::$app->user->can('guest'))
       //   {
        if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
             if(isset($_POST['switch_default_currency']))
                 {
                     $defaultCurr ='';
                   $modelDevise = SrcDevises::find()->where('id not in ('.$_POST['switch_default_currency'].')')->all();
                    if($modelDevise != null)
                        {
                            foreach($modelDevise as $devise)
                                {
                                   $defaultCurr = $devise->id;
                                   break;
                                }
                                
                            Yii::$app->session['$default_currency_dashboard'] = $defaultCurr;    
                        }
                 }
        $all_students = loadAllStudentsJson();
        $json_all_students = json_encode($all_students,false);
        
	        return $this->render('dashboard_billing', [
	           // 'dataProvider' => $dataProvider,
                    'json_all_students'=>$json_all_students,
	        ]);
    /*
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['../../rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
        */

    }    
    
    
    public function actionCreate(){
     
        if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
        $model = new RptCustom(); 
            if(isset($_POST['btnSave'])){
                $model->load(Yii::$app->request->post());
                 
                 
                $model->parameters = $_POST['parameter'];
                $model->variables = $_POST['variable'];
                $model->create_by = Yii::$app->user->identity->username; 
                $model->create_date = date("Y:m:d h:i:s"); 
                if($model->save()){
                    return $this->render('report_details',array('model'=>$model,'id'=>$model->id)); 
                }
            }
            return $this->render('create',['model'=>$model]); 
            
    }       
            
    public function actionList(){
        
        if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
        $this->part = "stud";
        
        $modelRptCustom = new RptCustom;
      
        return $this->render('list', [
	            'model' => $modelRptCustom,
                    
	        ]);
    }
    
 
    public function actionReportdetails($id){
          $model = new RptCustom(); 
          
          return $this->render('report_details',array('model'=>$model,'id'=>$id)); 
      }
    
    
    public function actionConfigparam($id){
            if(isset($_GET['id'])){
                $id = $_GET['id']; 
                $sc = array();
                $dc = array(); 
                $v = array(); 
              $model = new RptCustom();
                
                if(isset($_POST['btnSave'])){
                    $rpt = $model->findOne($id); 
                    $variable = $rpt->variables; 
                    $array_variables = explode(",",$variable); 
                    for($i=0;  $i<count($array_variables); $i++){
                        
                        if(isset($_POST[$array_variables[$i]])){
                            $v['data_type'][$i]= $_POST[$array_variables[$i]]; 
                            if(isset($_POST['static-combo'.$i])){
                                $sc['param_value']['static-combo'.$i] = $_POST['static-combo'.$i];
                            }else{
                                $sc['param_value']['static-combo'.$i] = ""; 
                            }
                            if(isset($_POST['dynamic-combo'.$i])){
                                $dc['param_value']['dynamic-combo'.$i] = $_POST['dynamic-combo'.$i]; 
                            }else{
                                $dc['param_value']['dynamic-combo'.$i] = ""; 
                            }
                          
                        }
                        
                    }
                   $full_array = array_merge_recursive($v,$sc,$dc);
                    $v_str = json_encode($v,FALSE); 
                    $sc_str = json_encode($sc,FALSE); 
                    $dc_str = json_encode($dc,FALSE); 
                   $full_str = json_encode($full_array,FALSE);
                    $rpt->setup_variable = ""; 
                    $rpt->setup_variable = $full_str; //$v_str.$sc_str.$dc_str;
                    if($rpt->save()){
                        $this->redirect(array('reportdetails?id='.$id.'&from1=rpt&cat='.$_GET['cat']));
                    }
                    
                }
                return $this->render('config_param',array('id'=>$id,'model'=>$model));
            }
      }
 
      public function actionUpdatereport($id){
            $model = RptCustom::findOne($id); 
            if(isset($_POST['btnUpdate'])){
                $model->load(Yii::$app->request->post());
                 
                 
                $model->parameters = $_POST['parameter'];
                $model->variables = $_POST['variable'];
                $model->create_by = Yii::$app->user->identity->username; 
                $model->create_date = date("Y:m:d h:i:s"); 
                if($model->save()){
                    return $this->render('report_details',array('model'=>$model,'id'=>$model->id)); 
                }
                
               
            }
            return $this->render('updatereport',array('id'=>$id,'model'=>$model));
        }
      
    public function actionExecutereport($id){
         
        if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
        $model = new RptCustom(); 
          $rpt = $model->findOne($id); 
          return $this->render('executereport',array('id'=>$id,'rpt'=>$rpt)); 
      }  
      
      
         public function actionRunreport($id){
           if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             
             $array_param = array(); 
          $post = $_POST;
          unset($_POST['YII_CSRF_TOKEN']);
          for($i=0;$i<count($_POST); $i++){
              if(isset($_POST['field'.$i])){
                  $array_param[$i] = $_POST['field'.$i]; 
              }
                      
          }
          
          $id = $_GET['id']; 
          $model = new RptCustom(); 
          $rpt = $model->findOne($id); 
          return $this->renderPartial('runreport',array('array_param'=>$array_param,'id'=>$id,'rpt'=>$rpt));
      }  
      
      
      
      
 public function actionOnataxes()
    {
	if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
              $month_ = '';
              $year_ = '';
              
	        $searchModel = new SrcPayroll();
                $modelPayroll = new SrcPayroll;
	    
                if(isset($_POST['execute'])) //
		  {
                      $month_ = $_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;
                   }
                
                
                if(isset($_POST['viewPDF'])) //to create PDF file
		  {
			$month_ = $_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;    	
			    	
                      $content = $this->renderPartial('ona_taxes', ['searchModel' => $searchModel,'modelPayroll'=>$modelPayroll,'month_'=>$month_,'year_'=>$year_]);

                      $heading = pdfHeading1();  //$this->renderPartial('_pdf_heading');

                        $pdf = new Pdf();


                        $pdf->filename = 'ona_taxes.pdf';
                        


                        $pdf->content = Pdf::MODE_CORE;
                        $pdf->mode = Pdf::MODE_BLANK;
                        $pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                        $pdf->defaultFontSize = 10;
                        $pdf->defaultFont = 'helvetica';
                        $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER; //or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                        $pdf->orientation = Pdf::ORIENT_PORTRAIT; //or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                        $pdf->destination = Pdf::DEST_DOWNLOAD; //or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F

                        $pdf->content = $content;
                        $pdf->options = ['title' => 'ONA TAXES'];
                        $pdf->methods = [
                                'SetHeader'=>$heading,// ['Krajee Report Header'],  //
                                'SetFooter'=>['|Page {PAGENO}|'],
                        ];

                        $pdf->options = [
                                'title' => 'ONA TAXES',
                                'autoScriptToLang' => true,
                                'ignore_invalid_utf8' => true,
                                'tabSpaces' => 4,
                                
                        ];


                        // return the pdf output as per the destination setting
                         return $pdf->render();
					    	   
					    
			    	
		  }   
	
	
	
        return $this->render('onaTaxes', [
            'searchModel' => $searchModel,
            'modelPayroll'=>$modelPayroll,
            'month_'=>$month_,'year_'=>$year_
            
        ]);
        
     
            
             
    }
     
    
    
    public function actionDgitaxes()
    {
	if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
              $month_ = '';
              $year_ = '';
              
	        $searchModel = new SrcPayroll();
                $modelPayroll = new SrcPayroll;
	    
                if(isset($_POST['execute'])) //
		  {
                      $month_ = $_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;
                   }
                
                
                if(isset($_POST['viewPDF'])) //to create PDF file
		  {
			$month_ = $_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;    	
			    	
                      $content = $this->renderPartial('dgi_taxes', ['searchModel' => $searchModel,'modelPayroll'=>$modelPayroll,'month_'=>$month_,'year_'=>$year_]);

                      $heading = pdfHeading1();  //$this->renderPartial('_pdf_heading');

                        $pdf = new Pdf();


                        $pdf->filename = 'dgi_taxes.pdf';
                        


                        $pdf->content = Pdf::MODE_CORE;
                        $pdf->mode = Pdf::MODE_BLANK;
                        $pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                        $pdf->defaultFontSize = 10;
                        $pdf->defaultFont = 'helvetica';
                        $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER; //or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                        $pdf->orientation = Pdf::ORIENT_LANDSCAPE; //or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                        $pdf->destination = Pdf::DEST_DOWNLOAD; //or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F

                        $pdf->content = $content;
                        $pdf->options = ['title' => 'DGI TAXES'];
                        $pdf->methods = [
                                'SetHeader'=>$heading,// ['Krajee Report Header'],  //
                                'SetFooter'=>['|Page {PAGENO}|'],
                        ];

                        $pdf->options = [
                                'title' => 'DGI TAXES',
                                'autoScriptToLang' => true,
                                'ignore_invalid_utf8' => true,
                                'tabSpaces' => 4,
                                
                        ];


                        // return the pdf output as per the destination setting
                         return $pdf->render();
					    	   
					    
			    	
		  }   
	
	
	
        return $this->render('dgiTaxes', [
            'searchModel' => $searchModel,
            'modelPayroll'=>$modelPayroll,
            'month_'=>$month_,'year_'=>$year_
            
        ]);
        
     
            
             
    }
      
    
    
public function actionDgibonistaxes()
    {
	if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                
              $month_ = -1;
              $year_ = '';
              
	        $searchModel = new SrcPayroll();
                $modelPayroll = new SrcPayroll;
	    
                if(isset($_POST['execute'])) //
		  {
                      $month_ = 0;//$_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;
                   }
                
                
                if(isset($_POST['viewPDF'])) //to create PDF file
		  {
			$month_ = 0; // $_POST['SrcPayroll']['payroll_month'];
                      $modelPayroll->payroll_month = $month_;
                      
                      $year_ = $_POST['SrcPayroll']['year'];
                      $modelPayroll->year = $year_;    	
			    	
                      $content = $this->renderPartial('dgi_bonis_taxes', ['searchModel' => $searchModel,'modelPayroll'=>$modelPayroll,'month_'=>$month_,'year_'=>$year_]);

                      $heading = pdfHeading1();  //$this->renderPartial('_pdf_heading');

                        $pdf = new Pdf();


                        $pdf->filename = 'dgi_bonis_taxes.pdf';
                        


                        $pdf->content = Pdf::MODE_CORE;
                        $pdf->mode = Pdf::MODE_BLANK;
                        $pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                        $pdf->defaultFontSize = 10;
                        $pdf->defaultFont = 'helvetica';
                        $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER; //or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                        $pdf->orientation = Pdf::ORIENT_LANDSCAPE; //or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                        $pdf->destination = Pdf::DEST_DOWNLOAD; //or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F

                        $pdf->content = $content;
                        $pdf->options = ['title' => 'DGI BONIS TAXES'];
                        $pdf->methods = [
                                'SetHeader'=>$heading,// ['Krajee Report Header'],  //
                                'SetFooter'=>['|Page {PAGENO}|'],
                        ];

                        $pdf->options = [
                                'title' => 'DGI BONIS TAXES',
                                'autoScriptToLang' => true,
                                'ignore_invalid_utf8' => true,
                                'tabSpaces' => 4,
                                
                        ];


                        // return the pdf output as per the destination setting
                         return $pdf->render();
					    	   
					    
			    	
		  }   
	
	
	
        return $this->render('dgiBonisTaxes', [
            'searchModel' => $searchModel,
            'modelPayroll'=>$modelPayroll,
            'month_'=>$month_,'year_'=>$year_
            
        ]);
        
     
            
             
    }
      
      
      
     public function actionExportcsv()
    {
        
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = new SrcStudentHasCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	        $program= 0;
	        $shift= 0;
	        $room = 0;
	        $level=0;
	        
                
                
                $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0); 
		     
	      
	      if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	            
	    
	               if(isset($_POST['SrcStudentHasCourses']['program']))
	                { $program= $_POST['SrcStudentHasCourses']['program'];
	                  $model->program=[$program];
	                }
	                
	              
	             
		         if(isset($_POST['SrcStudentHasCourses']['level']))
		           { $level= $_POST['SrcStudentHasCourses']['level'];
		                 $model->level=[$level]; 
		                 
		            }
		              
		             if(isset($_POST['SrcStudentHasCourses']['shift']))
		              { $shift= $_POST['SrcStudentHasCourses']['shift'];
		                 $model->shift=[$shift]; 
		         
		              }
		            
	          
		             if(isset($_POST['SrcStudentHasCourses']['room']))
		              {  $room = $_POST['SrcStudentHasCourses']['room'];
		                   $model->room=[$room];
		               
		              }
		   
	         
        
	        } 
	        
	
	            return $this->render('exportcsv', [
	                'model' => $model,
                        'program'=>$program,
                        'shift'=>$shift,
                        'room'=>$room,
                        'level'=>$level,
	                
	                
	            ]);
          

    }     
      
      
      
      
      
      
      
      
      
      
      
      
      
}
