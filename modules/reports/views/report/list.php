<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\reports\models\RptCustomCat;
use app\modules\reports\models\RptCustom;



$modelCat = new RptCustomCat(); 

?>


   
<div>
<div class="row">
   <?php if(Yii::$app->user->can('superadmin') ){ ?>
   <?php
           
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
       // if(Yii::$app->session['profil_as'] ==0)
        //  {
   ?>
   
    <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','cat'=>$cat, 'from1'=>'rpt'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Create report')]) ?> 
        
            
    </div>

   
   <?php
         // }
   ?>
<?php } ?>
     <div class="col-lg-6">
         <h3><?= Yii::t('app','List custom report'); ?></h3>
     </div>
        
     
        
</div> 





<div class="row">
<div class="col-lg-6"> 
    <?= $this->render('//layouts/navCustomReport'); ?>
</div></div> 


<?php 
    if(isset($_GET['cat'])){
        $cat = $_GET['cat'];
        
        
        $modelCustomCat = RptCustomCat::find()->select(['id'])->where('cat=\''.$cat.'\'')->all(); 
        
         foreach($modelCustomCat as $customcat)
         {
             $id_cat = $customcat->id;
         }
         
            
        
        $data_rpt = RptCustom::find()->WHERE('categorie ='.$id_cat.' ORDER BY title ASC')->all(); 
    }
?>
<div class="wrapper wrapper-content user-index">
       
     <div class="col-md-9 table-responsive">
            <table class='table table-striped table-bordered table-hover dataTables-example'>
             <thead>
                <tr>
                    <th><?php echo Yii::t('app','Report title'); ?></th>
                <?php if(Yii::$app->user->can('superadmin') ) {?>
                    <th><?php echo Yii::t('app','Create date'); ?></th>
                <?php } ?>
                 <?php if(Yii::$app->user->can('superadmin') ) {?>
                    <th></th>
                 <?php } ?>   
                    <th></th>
                 <?php if(Yii::$app->user->can('superadmin') ) {?>
                    <th></th>
                    <th></th>
                 <?php } ?>
                </tr>
            </thead>
            <tbody>    
        <?php 
            foreach($data_rpt as $d){
                $arr_ = json_decode($d->data,true);
                $description = $arr_['report_description'];
                
                
                
        ?>
            <tr>    
                <td><a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/executereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>"> <?php echo $d->title; ?></a></td>
                <?php if(Yii::$app->user->can('superadmin') ) {?>
                <td><?php echo $d->create_date; ?></td>
                <?php } ?>
              
             <?php if(Yii::$app->user->can('superadmin') ) {?>
                <td>
                    <?php if($d->variables != NULL || $d->variables !="") {?>
                    
                    <a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/configparam?id='.$d->id.'&cat='.$cat.'&part=stud&from1=rpt'; ?>" title="<?php echo Yii::t('app','Parameter configuration'); ?>"><i class="fa fa-gear"></i></a>
                    <?php } ?>
                    
                </td>
             <?php } ?>
                <td><?php if(($d->setup_variable != NULL || $d->setup_variable != "") || ($d->variables==NULL || $d->variables=="")) {?>
                    <a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/executereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Run report'); ?>"><i class="fa fa-play"></i></a>
                
                    <?php } ?>
               </td>
             <?php if(Yii::$app->user->can('superadmin') ) {?>
                <td>
                    
                    <a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/updatereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Update report'); ?>"><i class="fa fa-edit"></i></a>
                      
                </td>
             <?php } ?>
                
                 <?php
                if(Yii::$app->user->can('superadmin') ){
                 ?> 
                <td><a Onclick="return ConfirmDelete()" href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/delete?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Delete report'); ?>"><i class="fa fa-trash-o"></i></a></td>
             <?php    
                }
                ?>
           
            </tr>
            <?php    
                 
             }
        ?>
       </tbody>
                    </table>
        

                 </div>
				    
    <div class="col-md-3 " style="background-color: #E1E1E2; border-radius: 12px; height:400px; font-size: 14px;">
       <?php
       if($cat == 'eco')
       {
          if(Yii::$app->user->can('superadmin')|| Yii::$app->user->can('Administration économat') )
           {
       ?> 
        <center><medium><?= Yii::t('app','Special') ?></medium></center>
         
         <li><a title="<?= Yii::t('app', 'Paiement de taxes à l\'ONA ') ?>"  href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/onataxes?cat=<?= $_GET['cat'] ?>&from=bil"><i class="fa fa-"></i><span class=""> <?= Yii::t('app', 'Paiement de taxes à l\'ONA') ?> </span></a> </li>
    
         <li><a title="<?= Yii::t('app', 'Paiement de taxes à la DGI') ?>"  href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dgitaxes?cat=<?= $_GET['cat'] ?>&from=bil"><i class="fa fa-"></i><span class=""> <?= Yii::t('app', 'Paiement de taxes à la DGI') ?> </span></a> </li>
         
      <!--   <li><a title="<?= Yii::t('app', 'Paiement de taxes sur bonis à la DGI') ?>"  href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dgibonistaxes?cat=<?= $_GET['cat'] ?>&from=bil"><i class="fa fa-"></i><span class=""> <?= Yii::t('app', 'Paiement de taxes sur bonis à la DGI') ?> </span></a> </li>
-->
     <?php
            }
       
       }
     if($cat == 'peda')
       {
          if(Yii::$app->user->can('superadmin')|| Yii::$app->user->can('fi-grades-validate') )
           {  
     ?>
          <center><medium><?= Yii::t('app','Special') ?></medium></center>
         
         <li><a title="<?= Yii::t('app', 'Liste des notes par groupe') ?>"  href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/exportcsv?cat=<?= $_GET['cat'] ?>&from=peda"><i class="fa fa-"></i><span class=""> <?= Yii::t('app', 'Liste des notes par groupe') ?> </span></a> </li>

     <?php
            }
            
       }
       ?>
    </div>     

</div></div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'List Report'},
                    //{extend: 'pdf', title: 'List Report'},

                    
                ]

            });

        });

JS;
$this->registerJs($script);

?>

<script>
    function ConfirmDelete()
        {
          var x = confirm("<?php echo Yii::t('app','Are you sure you want to delete?'); ?>");
          if (x)
              return true;
          else
            return false;
        }
</script>