<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 
use yii\widgets\Pjax;

use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcCourses;

use app\modules\inscription\models\SrcPostulant;


$this->title = Yii::t('app', 'Courses/Modules enrolled');

$acad = Yii::$app->session['currentId_academic_year'];


    $modelShifts = SrcShifts::find()->all();

     $modelProgram = SrcProgram::find()->where('is_special=0 ')->all();
     
     $modelAllSutdentEnrolled= new SrcStudentHasCourses;
     $totalStud = $modelAllSutdentEnrolled->searchStudentsEnrolledAllProgram($acad);
     $total_student_all_program = sizeof($totalStud->getModels() );
?>


   
        
    <div class="wrapper wrapper-content">
<?php
    if(Yii::$app->session['profil_as']==0)//se yon teacher 
      {
?>
  <div class='row'>
    <div style="margin-left:auto;margin-right:auto; width:50%;">
        <div class="col-xs-9">
        <div class="input-group">
            <input type="text" class="form-control" id="no-stud" name="no-stud" placeholder="<?= Yii::t('app','Search a student'); ?>" onkeypress="handle(event)">
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idstud-selected'>
        </div>
        
        </div>
    
    <div  style="width:auto;float:left; margin-top:-8px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['/fi/persons/createstudent?from=stud&is_stud=1' ], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add a student')]) ?>


    </div>
    </div>
  </div>
<?php
      }
?>
        
 <!--  Essaie #1 new dashboard -->  

 
 <!-- Fin essaie dasboard --> 
        <div class="row" style="padding: 8px;">
            
                    
 <?php       
        foreach($modelProgram as $program)
            {
     
               $modelSutdentEnrolled= new SrcStudentHasCourses;
               $modelStudentInProgram = $modelSutdentEnrolled->searchStudentsEnrolledByProgram($program->id, $acad);
               $studentCount = sizeof($modelStudentInProgram->getModels() );
               $student_total = $modelStudentInProgram->getModels();
               $pourcentage_student = 0; 
               if($total_student_all_program!=0){ 
                   $pourcentage_student = round(($studentCount/$total_student_all_program*100), 2); 
               }
               else { 
                   $pourcentage_student = 0;
               }
               
               // Compte le nombre de femme et d'homme 
               $fe =0;
                $ma =0;
       
       
               foreach($student_total as $studentInProgram)
                {
                    if($studentInProgram->student0->gender ==0)
                      $ma=$ma+1;
                    elseif($studentInProgram->student0->gender ==1)
                        $fe = $fe+1;
                 }
                 /*title="<?= Yii::t('app','Total student'); ?>:<br/> <?php // $program->short_name ?>" */
               
     ?>
       <div class="col-lg-3 col-xs-6 ti-bwat">
           
          <!-- small box -->
          <div class="small-box <?= getDesignByProgram($program->short_name)['koule']; ?>"  data-html="true" rel="tooltip" href="#"  title='<?= Yii::t('app','Male') ?> : <?= $ma ?> <?= Yii::t('app','Female') ?> : <?= $fe; ?>'>
            <div class="inner">
                <h3><span class="teks-blan"><?= $studentCount ?></span> <span class='piti-ekriti teks-blan'> <?= Yii::t('app','Enrolled student') ?></span></h3>
                <h4><span class='percent teks-blan'><?= $pourcentage_student?>%</span></h4>
              <p class="nom-piti"><?= $program->short_name; ?></p>
                
            </div>
            <div class="icon">
              <i class="<?= getDesignByProgram($program->short_name)['fa']; ?>"></i>
            </div>
         
          </div>
        </div>  
            
           
            
  <?php
                }
                 
  ?>
       <!-- specialisation -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           <?php
               $modelSutdentEnrolled1= new SrcStudentHasCourses;
               $modelStudentInProgram1 = $modelSutdentEnrolled1->searchStudentsEnrolledSpecialisation($acad);
               $studentCount1 = sizeof($modelStudentInProgram1->getModels() );
               $student_total1 = $modelStudentInProgram1->getModels();
               $pourcentage_student1 = 0; 
               if($total_student_all_program!=0){ 
                   $pourcentage_student1 = round(($studentCount1/$total_student_all_program*100), 2); 
               }
               else { 
                   $pourcentage_student1 = 0;
               }
               
               // Compte le nombre de femme et d'homme 
               $fe =0;
                $ma =0;
       
       
               foreach($student_total1 as $studentInProgram)
                {
                    if($studentInProgram->student0->gender ==0)
                      $ma=$ma+1;
                    elseif($studentInProgram->student0->gender ==1)
                        $fe = $fe+1;
                 }
           ?>
          <!-- small box -->
          <div class="small-box bg-blue"  data-html="true" rel="tooltip" href="#" title='<?= Yii::t('app','Male') ?> : <?= $ma ?> <?= Yii::t('app','Female') ?> : <?= $fe; ?>'>
            <div class="inner">
                <h3><span class="teks-blan"><?= $studentCount1 ?></span> <span class='piti-ekriti teks-blan'> <?= Yii::t('app','Student').'(s)' ?></span></h3>
                <h4><span class='percent teks-blan'><?= $pourcentage_student1?>%</span></h4>
              <p><?= Yii::t('app','Specialisation'); ?></p>
            </div>
            <div class="icon">
              <i class="fa fa-graduation-cap"></i>
            </div>
         
          </div>
        </div>  
            
     <!-- total course -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           <?php
                $courseCount1 = 0; 
                
                $modelCourse= new SrcCourses;
               //tout moun ki anrole
               $totalCourses = $modelCourse->searchDashboard();
               
               if($totalCourses!=null)
                {
                   $courseCount1 = sizeof($totalCourses->getModels());
                }
                        
               
       
               
           ?>
          <!-- small box -->
          <div class="small-box bg-info"  data-html="true" rel="tooltip" href="#" >
            <div class="inner">
                <h3><span class="teks-blan"><?= $courseCount1 ?></span> <span class='piti-ekriti teks-blan'> <?= Yii::t('app','Courses') ?></span></h3>
                <h4><span class='percent teks-blan'><br/></span></h4>
              <p><?= Yii::t('app','Courses'); ?></p>
            </div>
            <div class="icon">
              <i class="fas fa-chalkboard"></i>
            </div>
         
          </div>
        </div>  
                
            
        <!-- teacher -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           <?php
                $teacherCount1 = 0; 
                $fe = 0; 
                $ma = 0;

                $modelCourses= new SrcCourses;
               //tout moun ki anrole
               $modelTeacher = $modelCourses->getAllTeachersByAcad($acad);
               
               if($modelTeacher!=null)
                {
                   foreach($modelTeacher as $teacher)
                     {
                                  $teacherCount1 ++;
                                  
                                  if($teacher->teacher0->gender ==0)
                                    $ma=$ma+1;
                                  elseif($teacher->teacher0->gender ==1)
                                      $fe = $fe+1;
                                  
                            
                     }
                }
                        
               
               
               
               
       
       
               
           ?>
          <!-- small box -->
          <div class="small-box bg-fuchsia"  data-html="true" rel="tooltip" href="#" title='<?= Yii::t('app','Male') ?> : <?= $ma ?> <?= Yii::t('app','Female') ?> : <?= $fe; ?>'>
            <div class="inner">
                <h3><span class="teks-blan"><?= $teacherCount1 ?></span> <span class='piti-ekriti teks-blan'> <?= Yii::t('app','Teacher').'(s)' ?></span></h3>
                <h4><span class='percent teks-blan'><br/></span></h4>
              <p><?= Yii::t('app','Teacher'); ?></p>
            </div>
            <div class="icon">
              <i class="fas fa-chalkboard-teacher"></i>
            </div>
         
          </div>
        </div> 
        
        <!-- postulants -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           <?php
                $postulantCount1 = 0; 
                $fe = 0; 
                $ma = 0;

                $modelPostulant= new SrcPostulant;
               //tout moun ki anrole
               $allPostulants = $modelPostulant->getPostulantForNextYear($acad);
               
               if($allPostulants!=null)
                {
                   foreach($allPostulants as $postulant)
                     {
                                  $postulantCount1 ++;
                                  
                                  if($postulant->gender ==0)
                                    $ma=$ma+1;
                                  elseif($postulant->gender ==1)
                                      $fe = $fe+1;
                                  
                            
                     }
                }
                        
               
               
               
               
       
       
               
           ?>
          <!-- small box -->
          <div class="small-box bg-purple-gradient"  data-html="true" rel="tooltip" href="#" title='<?= Yii::t('app','Male') ?> : <?= $ma ?> <?= Yii::t('app','Female') ?> : <?= $fe; ?>'>
            <div class="inner">
                <h3><span class="teks-blan"><?= $postulantCount1 ?></span> <span class='piti-ekriti teks-blan'> <?= Yii::t('app','Postulant').'(s)' ?></span></h3>
                <h4><span class='percent teks-blan'><br/></span></h4>
              <p><?= Yii::t('app','Admission'); ?></p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
         
          </div>
        </div> 
        
        
        
        
        </div>
        <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><?= Yii::t('app','Students in program by level'); ?></h5>
                                
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                
                                    <div class="col-lg-9">
                                    <div class="flot-chart">
                                        <div class="flot-chart-content" id="morris-bar-chart"></div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <ul class="stat-list">
                                        
                            <?php       foreach($modelShifts as $shift)
                                        {

                               ?>           
                                        <li>
                                            <h2 class="no-margins">
                                            <?php $modelShiftSutdentEnrolled= new SrcStudentHasCourses;
                                    
                                          $modelStudentInShift = $modelShiftSutdentEnrolled->searchStudentsEnrolledByShift($shift->id, $acad);
                                     
                                          $studentCount_in_shift = sizeof($modelStudentInShift->getModels() ); 
                                          
                                          echo $studentCount_in_shift;
                                     ?>
                                            </h2>
                                            <small><?= Yii::t('app','Total student in').' '.$shift->shift_name; ?></small>
                                            <div class="stat-percent"><?php if($total_student_all_program!=0) echo round(($studentCount_in_shift/$total_student_all_program*100), 2); else echo 0; ?>% </div>
                                            <div class="progress progress-mini">
                                                <div style="width: <?php if($total_student_all_program!=0) echo round(($studentCount_in_shift/$total_student_all_program*100), 2); else echo 0; ?>%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                              <?php
                                        }
                              ?>          
                                        </ul>
                                    </div>
                                
                                
                                </div>
                                
                                
                                </div>

                            </div>
                        
                      </div>
                      
            
            
            
                    </div>


               
                </div>
       
  
    


<?php
      
   $data=null;
   $data_pie=null;
    $pass=0;
    $l_1 = Yii::t('app','Level').' 1';
    $l_2 = Yii::t('app','Level').' 2';
    $l_3 = Yii::t('app','Specialisation');
    
    $i=0;
   // $color=["#d3d3d3", "#bababa", "#79d2c0", "#1ab394", "#aedd2c"];
    
           
   foreach($modelProgram as $program)
     {
       $l1 =0;
       $l2 =0;
       $l3 =0;
       
       if($program->is_special==0)
        {
           $modelSutdentEnrolled= new SrcStudentHasCourses;
       
            $modelStudentInProgram = $modelSutdentEnrolled->searchStudentsEnrolledByProgram($program->id, $acad);

            $studentCount = $modelStudentInProgram->getModels();

            foreach($studentCount as $studentInProgram)
             {
                if(isset($studentInProgram->student0->studentLevel->level) )
                {
                    if($studentInProgram->student0->studentLevel->level ==1)
                      $l1=$l1+1;
                    elseif($studentInProgram->student0->studentLevel->level ==2)
                        $l2 = $l2+1;
                }


              }
            
             //gade si program sa gen specialisation poun konte konbye etidyan ki ladan
               $modelSpecialisation = SrcProgram::find()->where('is_special=1 and program_base='.$program->id)->all();
               if($modelSpecialisation!=null)
                {
                   foreach($modelSpecialisation as $specialisation)
                    {
                        $modelStudentInProgram1 = $modelSutdentEnrolled->searchStudentsEnrolledByProgram($specialisation->id, $acad);

                         $studentCount1 = $modelStudentInProgram1->getModels();

                         foreach($studentCount1 as $studentInProgram1)
                          {
                            if(isset($studentInProgram1->student0->studentLevel->level) )
                              {
                                if($studentInProgram1->student0->studentLevel->level ==3)
                                  $l3 = $l3+1;
                              }


                           }
                      
                    }
                
                }
              
              if($pass==0)
               { $data= $data. '{ y: "'.$program->short_name.'", s: '.$l3.', f: '.$l2.', m: '.$l1.' }';

                  $pass=1;
                 }
              else
                { $data= $data. ', { y: "'.$program->short_name.'", s: '.$l3.', f: '.$l2.', m: '.$l1.' }';

                }
        }
        
           $i++;
         
        
      }
   
 ?>

<?php
$baseUrl = Yii::$app->request->BaseUrl;
   $script = <<< JS
    
    $(document).ready(function(){
        $('.small-box').tooltip({    
            selector: "div[rel=tooltip]"
        });    
        Morris.Bar({
        element: 'morris-bar-chart',
        data: [ $data ],
        xkey: 'y',
        ykeys: ['s', 'f', 'm'],
        labels: ['$l_3', '$l_2', '$l_1'],
        hideHover: 'auto',
        resize: true,
       // barColors: ["#d3d3d3","#79d2c0","#1ab394", "#cacaca"],
         barColors: ["#EC4758","#21B9BB","#F7A54A", "#69B8CA"],  
         });
     
    
        $('#no-stud').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_students; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idstud-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "$baseUrl/index.php/fi/persons/moredetailsstudent?id="+id_stud+"&is_stud=1";
            
            }
           
            });
           
          

        });
           
       $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "$baseUrl/index.php/fi/persons/moredetailsstudent?id="+id_stud+"&is_stud=1";
            
            }

        }

    }); 

JS;
$this->registerJs($script);

?>
        
   