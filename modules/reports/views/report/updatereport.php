   <?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\reports\models\RptCustom;
use app\modules\reports\models\RptCustomCat;




$this->title = Yii::t('app', 'Update report');

?>


 <div class="row">
        <div class="col-lg-2">
            <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
             ?>
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List report'), ['list', 'from1' => 'rpt','cat'=>$cat], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-10">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>





    
<div class="wrapper wrapper-content ">
    
      <div class="">

            <?php $form = ActiveForm::begin(); ?>
        
        
        <div class="row">
            <div class="col-lg-3">
                  <?= $form->field($model, 'title')->label(Yii::t('app','Report title'))->textInput() ?>
                
            </div>
            
            <div class="col-lg-3">
                    
            <?php  
                   if(isset($_GET['cat'])&&($_GET['cat']!=null))
                   {
                       $modelCustomCat_ = RptCustomCat::find()->where('cat=\''.$_GET['cat'].'\'')->all();
                       if($modelCustomCat_!=null)
                         {  foreach ($modelCustomCat_ as $categ)
                             $model->categorie = $categ->id;
                         }
                   }  
                       
                       
                  echo $form->field($model, 'categorie')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(RptCustomCat::find()->orderBy(['categorie_name'=>'ASC'])->all(),'id','categorie_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', '-- Please select categorie --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
             
             
            ?>
        </div>  
        
        
        
        
        </div>
        <div class="row">
            <div class="col-lg-12">
                    
                        
                        <label><?php echo Yii::t('app','Code SQL'); ?></label>
                        <div class="alert alert-danger"><?= Yii::t('app','Type your SQL code in the box bellow, use "AS" to specify column labelling. <br/>If you want to make your SQL code parametrable please use this format "{%param1%}". <br/>If your parameter is two words use '
                                . '"_", example {%param_avec_plusieurs_mots%} !'); ?></div>
                       
                        
        
           <?= $form->field($model, 'data')->textarea(['rows' => 8 ])  ?>
        
                    
                </div>
        </div>
        
        
        
        <div class="span2">
            
        </div>
        
        <div class="span2">
            <p></p>
            <?= Html::submitButton(Yii::t('app', 'Update report'), ['name' => 'btnUpdate', 'class' =>'btn btn-success' ]) ?>
            
           
        </div>
          
<input type="hidden" name="parameters" id="parameters"/>
        <input type="hidden" name="parameter" id="parameter" />
        <input type="hidden" name="variable" id="variable"/>
       
        <div class="span2">
            
        </div>
        
      
    </div>
    
    <?php $form = ActiveForm::end(); ?>
</div>

<div id="list_zone"></div>



<?php

    $script = <<< JS
    $(document).ready(function(){
        $("#btn-config-parameter").hide();
            
        var valeur = $('#rptcustom-data').val();
        var result_param = getFromBetween.get(valeur,"{{","}}");
        var result_variable = getFromBetween.get(valeur,"{%","%}");
        if (result_variable == undefined || result_variable.length == 0) {
                 
            }
        else{
               $("#variable").val(result_variable);
                
            }
        
       
        
        $("#parameter").val(result_param); 
     
    });
      
   
   $("#rptcustom-data").keyup(function(){
        var valeur = $('#rptcustom-data').val();
        var result_param = getFromBetween.get(valeur,"{{","}}");
        var result_variable = getFromBetween.get(valeur,"{%","%}");
        if (result_variable == undefined || result_variable.length == 0) {
                 
            }else{
                // $("#btn-config-parameter").show();
                
                /*
                var div = document.createElement('div');

                div.className = 'param-config';
                
            for(var i = 0; i<result_variable.length; i++){
                 div.innerHTML += '<a href="#" class="btn btn-success param" data-paramvalue='+result_variable[i]+'><span class="fa fa-config">'+result_variable[i]+'</span></a>';
            }
            

                document.getElementById('param-config').appendChild(div);
                
                */
               
                $("#variable").val(result_variable);
                
            }
        
       
        
        $("#parameter").val(result_param); 
           
    });
            
       var getFromBetween = {
    results:[],
    string:"",
    getFromBetween:function (sub1,sub2) {
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var SP = this.string.indexOf(sub1)+sub1.length;
        var string1 = this.string.substr(0,SP);
        var string2 = this.string.substr(SP);
        var TP = string1.length + string2.indexOf(sub2);
        return this.string.substring(SP,TP);
    },
    removeFromBetween:function (sub1,sub2) {
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var removal = sub1+this.getFromBetween(sub1,sub2)+sub2;
        this.string = this.string.replace(removal,"");
    },
    getAllResults:function (sub1,sub2) {
        // first check to see if we do have both substrings
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;

        // find one result
        var result = this.getFromBetween(sub1,sub2);
        // push it to the results array
        this.results.push(result);
        // remove the most recently found one from the string
        this.removeFromBetween(sub1,sub2);

        // if there's more substrings
        if(this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
            this.getAllResults(sub1,sub2);
        }
        else return;
    },
    get:function (string,sub1,sub2) {
        this.results = [];
        this.string = string;
        this.getAllResults(sub1,sub2);
        return this.results;
    }
   };

JS;
$this->registerJs($script);

?>
                      
          
