<?php

use app\modules\reports\models\RptCustom;
use app\modules\reports\models\RptCustomCat;




?>
<div class="row">
        <div class="col-lg-6">
    
           <h2><?= Yii::t('app','{nameReport}',array('nameReport'=>$model->findOne($id)->title));  ?></h2>
       </div>
</div>

<div class="wrapper wrapper-content ">
<div class="">


 <div class="col-md-12 table-responsive">
  <table class='table table-striped table-bordered table-hover dataTables-example'>
    <thead>
        <tr>
            <th><?php echo Yii::t('app','Report title'); ?></th>
            <th><?php echo Yii::t('app','Category'); ?></th>
            <th><?php echo Yii::t('app','Create date'); ?></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
       </thead>
       <tbody>     
        
        <?php
            $rpt = $model->findOne($id);
            $cat = new RptCustomCat(); 
        ?>
        <tr>
            <td><?= $rpt->title; ?></td>
            <td><?= $cat->findOne($rpt->categorie)->categorie_name; ?></td>
            <td><?= $rpt->create_date; ?></td>
            <td>
                <?php if($rpt->variables != "" || $rpt->variables != NULL){ ?>
                <a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/configparam?id='.$rpt->id.'&cat='.$_GET['cat'].'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Parameter configuration'); ?>"><i class="fa fa-gear"></i></a>
                <?php } ?>
            </td>
            <td><?php if(($rpt->setup_variable != NULL || $rpt->setup_variable != "") || ($rpt->variables==NULL || $rpt->variables=="")) {?>
                <a href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/executereport?id='.$rpt->id.'&cat='.$_GET['cat'].'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Execute Report'); ?>"><i class="fa fa-play"></i></a>
           <?php 
            } ?></td>
            <?php
                if(Yii::$app->user->can('superadmin') ) {
                 ?> 
                <td><a Onclick="return ConfirmDelete()" href="<?php echo Yii::getAlias('@web').'/index.php/reports/report/delete?id='.$rpt->id.'&cat='.$_GET['cat'].'&from1=rpt' ?>" title="<?php echo Yii::t('app','Delete report'); ?>"><i class="fa fa-trash-o"></i></a></td>
             <?php    
                }
                ?>
        </tr>
   </tbody>
 </table>
        
   </div>
</div>
    
    </div>

<script>
    function ConfirmDelete()
        {
          var x = confirm("<?php echo Yii::t('app','Are you sure you want to delete?'); ?>");
          if (x)
              return true;
          else
            return false;
        }
</script>