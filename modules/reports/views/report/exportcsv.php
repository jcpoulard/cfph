<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;


$this->title = Yii::t('app', 'Liste des notes');


?>



<div clas="row">


</div>

 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['/reports/report/list?cat='.$_GET['cat'].'&from=peda', ], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content export-grade">

    <?php
         
                echo $this->render('_exportcsv', [
                 'model' => $model,
                 'program'=>$program,
                        'shift'=>$shift,
                        'room'=>$room,
                        'level'=>$level,
             ]); 
          
                  
         
          
        ?>

</div>




