<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;


$this->title = Yii::t('app', 'ONA Taxes');


?>



<div clas="row">


</div>

 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['/reports/report/list?cat='.$_GET['cat'].'&from=bil', ], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content ">

<div class="billings-services-form">

    <?php $form = ActiveForm::begin(); ?> 
   <div class=""> 
     <div class="col-lg-2">
      <?php
              echo $form->field($modelPayroll, 'payroll_month')->widget(Select2::classname(), [
					                       'data'=>$modelPayroll->getSelectedLongMonthValueReport(),
					                       'size' => Select2::MEDIUM,
					                       'theme' => Select2::THEME_CLASSIC,
					                       'language'=>'fr',
					                       'options'=>['placeholder'=>Yii::t('app', '-- Search --'),],
					                                        
					                       'pluginOptions'=>[
					                             'allowclear'=>true,
					                         ],
					                       ]) ;
              ?>
    </div>
    <div class="col-lg-1">
        <?= $form->field($modelPayroll, 'year')->textInput() ?>    
        
        </div>
    
    <div class="col-lg-2" style="margin-top: 20px;">
        <?= Html::submitButton(Yii::t('app', 'Execute') , ['name' => 'execute', 'class' =>'btn btn-success']) ?>
    </div>
       
 </div>
                                          <div id="bouton" class="row">
						        <div class="col-sm-2"  style="float:right">
						            <div class="form-group">
						        
						               <?= Html::submitButton(Yii::t('app', 'PDF') , ['name' => 'viewPDF', 'class' =>'btn btn-success']) ?>
        
                                                             <!--   <button onclick="printContent('print')" class = 'btn btn-success'><?= Yii::t('app', 'Print') ?></button>   -->
						      						        
						            </div>
						        </div>
						        
						   </div>

    
    <?= $this->render('ona_taxes', [
        'searchModel' => $searchModel,
        'modelPayroll'=>$modelPayroll,
        'month_'=>$month_,'year_'=>$year_
    ]) ?>

    
     <?php ActiveForm::end(); ?>
  </div>
</div>




