<?php


//echo "TESTY";

//echo $post_json;

$a_variables = explode(",", $rpt->variables); 
$re_write_variable = array(); 
for($i=0;$i<count($a_variables);$i++){
    $re_write_variable[$i] = "{%".$a_variables[$i]."%}";  
}

$str_sql =  $rpt->data;
$str_final = str_replace($re_write_variable, $array_param, $str_sql);
?>


<?php
// Pour afficher les dates au format francais 
    $locale = "fr_FR";
    if(isset($_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']) && $_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']=="fr" ){
        $locale = "fr_FR";
    }else{
       $locale = 'fr_FR';
    }
    Yii::$app->getDb()->createCommand("SET lc_time_names = '$locale'")->execute();
$name = Yii::$app->getDb()->createCommand($str_final)->queryAll();
    $clean_data = array();
    $i=0;
    foreach ($name as $n){
        $clean_data[$i] = $n;
        $i++;
    }
    
 $comp =1; 
 
    ?>

<div class="col-lg-12 table-responsive">
<?php if (count($clean_data) > 0): ?>
<?php if(Yii::$app->user->can('superadmin') ) { ?>
<div class="col-lg-12 fa fa-database alert alert-info alert-dismissible">
<span class="">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?= $str_final; ?>
</span>
</div>
<?php } ?>


  <table class='table table-striped table-bordered table-hover dataTables-example'>
  <thead>
    <tr>
      <th></th><th><?php echo implode('</th><th>', array_keys(current($clean_data))); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($clean_data as $row): array_map('htmlentities', $row); ?>
      
    <tr>
      <td><?= $comp ?></td><td><?php echo implode('</td><td>', $row); ?></td>
    </tr>
   
<?php $comp++; endforeach; ?>
  </tbody>
</table>
    
   
</div>

<?php elseif($clean_data==null):  
        echo Yii::t('app','No result found');
    endif;

?>


<?php
    $search_text = Yii::t('app','Search');
    $processing_text = Yii::t('app','Processing ...');
    $script = <<< JS
      $(document).ready(function(){
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'List Report'},
                    //{extend: 'pdf', title: 'List Report'},

                    
                ]

            });

        });
            
JS;
$this->registerJs($script);



?>
