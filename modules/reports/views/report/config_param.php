<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\reports\models\RptCustom;
use app\modules\reports\models\RptCustomCat;

//echo $json_value; 

?>




 <div class="row">
        <div class="col-lg-2">
            <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
             ?>
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List report'), ['list', 'from1' => 'rpt','cat'=>$cat], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-10">
             <h3><?= Yii::t('app','Setup parameter(s) for report').': <br/>'.$model->findOne($id)->title;  ?></h3>
        </div>
    </div>



<div class="wrapper wrapper-content ">
   <?php 
        
       //echo $json_value; 
       $param_value = explode(",",$model->findOne($id)->variables); 
    
            
   ?>
<div>
<?php $form = ActiveForm::begin(); ?>
    <p><?= Yii::t('app','Choose data type for the parameter(s) below').' :'; ?></p>
   
    
<div class="col-md-12 table-responsive">
  <table class='table table-striped table-bordered table-hover dataTables-example'>
   
    
        <?php 
        for($i=0; $i < count($param_value); $i++){
            
        ?>
        <tr>
            <td style=" vertical-align: middle;" >
        <?php  echo '<i class="config">'.$param_value[$i].'</i>'; ?> 
            </td>
       
            <td style=" vertical-align: middle;" >
        <select name="<?php  echo $param_value[$i]; ?>" id="<?php  echo $param_value[$i]; ?>" class="konfig">
            <option value="txt"><?= Yii::t('app','Text');?></option>
            <option value="date"><?= Yii::t('app','Date'); ?></option>
            <option value="static-combobox"><?= Yii::t('app','Static combobox'); ?></option>
            <option value="dynamic-combobox"><?= Yii::t('app','Dynamic combobox');?></option>
        </select>
            </td>
            <td class="static">
                <div class="static-combo<?=$i?>" style="margin-top:-7px;" ><?= Yii::t('app','Please build your dropdownlist by typing key:value separate by coma. Example : 1:Enable, 2:Disabled'); ?> <br/>  
                <input id="static-combo<?=$i?>" name="static-combo<?=$i?>" type="text" size="100" />  </div> 
                <div class="dynamic-combo<?=$i?>" style="margin-top:-7px;" ><?= Yii::t('app','Please build your dropdownlist by typing your SQL code.'); ?> <br/> 
                <input id="dynamic-combo<?=$i?>" name="dynamic-combo<?=$i?>" type="text" size="100" /> </div> 
                
            </td>
        
    </tr>
        
        
        
    <?php         
            
        }
        ?>
     </table> 
  </div>
        <div>
             <?= Html::submitButton(Yii::t('app', 'Save Configuration parameter'), ['name' => 'btnSave', 'class' =>'btn btn-primary' ]) ?>
            
        </div>
    
   <?php $form = ActiveForm::end(); ?>
    
    <div id="data-param">
        
    </div>
  
</div></div>



<?php
for($i=0; $i < count($param_value); $i++){ 
    $script[$i] = <<< JS
  
            
    $(document).ready(function(){
        $("#static-combo$i").hide(); 
        $(".static-combo$i").hide();
        $(".dynamic-combo$i").hide();
        $("#dynamic-combo$i").hide(); 
     
    });
      
   $("#$param_value[$i]").click(function(){
        var combo_value = $("#$param_value[$i]").val(); 
        if(combo_value === "static-combobox"){
            $("#static-combo$i").show();
            $(".static-combo$i").show();
            $(".dynamic-combo$i").hide();
            $("#dynamic-combo$i").hide();
        }else if(combo_value==="dynamic-combobox"){
            $("#static-combo$i").hide();
            $(".static-combo$i").hide();
            $(".dynamic-combo$i").show();
            $("#dynamic-combo$i").show();
        }else{
            $("#static-combo$i").hide();
            $("#dynamic-combo$i").hide();
            $(".static-combo$i").hide();
            $(".dynamic-combo$i").hide();
        }
    });
   
    

JS;
$this->registerJs($script[$i]);
}
?>
