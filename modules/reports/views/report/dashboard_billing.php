<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 
use yii\widgets\Pjax;



use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcShifts;

use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcDevises;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\SrcScholarshipHolder;
use app\modules\billings\models\SrcBillingsServices;
use app\modules\billings\models\SrcOtherIncomes;


$this->title = Yii::t('app', 'Courses/Modules enrolled');

$acad = Yii::$app->session['currentId_academic_year'];

if(Yii::$app->session['$default_currency_dashboard']==null)
    $default_currency = getDefaultCurrency()->id;
else 
  {
    $default_currency = Yii::$app->session['$default_currency_dashboard'];
 }

 $modelShifts = SrcShifts::find()->all();

 $modelProgram = SrcProgram::find()->where('is_special=0')->all();
 
  $modelProgramGraf = SrcProgram::find()->all();//graf
 
 $modelAllTuitionFee= new SrcBillings;
     $totalTuitionFee = $modelAllTuitionFee->searchTuitionFeeAllProgram($acad);
     
     $totalTuitionFeeExpected = $modelAllTuitionFee->searchTuitionFeeExpectedAllProgram($acad);
    
 
 
 
?>


   
        
<div class="wrapper wrapper-content">
 <div class='row'>
    <div style="margin-left:auto;margin-right:auto; width:50%;">
        <div class="col-xs-9">
        <div class="input-group">
            <input type="text" class="form-control" id="no-stud" name="no-stud" placeholder="<?= Yii::t('app','Search a student'); ?>" >
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idstud-selected'>
        </div>
        
        </div>
    
    <div  style="width:auto;float:left; margin-top:-8px;">
            

    </div>
    </div>
     <div  style="width:auto;float:right; margin-right:40px;">
         <?php $form = ActiveForm::begin(); ?>
            <?= Html::submitButton(Yii::t('app', 'Switch currency'), ['class' => 'btn btn-primary', 'value'=>'s_currency', 'name'=>'switch_currency']) ?>
            <input type='hidden' id='switch_default_currency' name='switch_default_currency' value='<?= $default_currency ?>' >
        <?php $form = ActiveForm::end(); ?>
     </div>
</div>        
    
    <div class="row" style="padding: 8px;">
             
                    
 <?php     if($modelProgram!=null)
            {   
             foreach($modelProgram as $program)
              {
     
     ?>
         <div class="col-lg-3 col-xs-6 ti-bwat">
           
          <!-- small box -->
          <div class="small-box <?= getDesignByProgram($program->short_name)['koule']; ?>"  data-html="true" rel="tooltip" href="#"  >
            <div class="inner">
                <h3 style="font-size:25px;"><span class="teks-blan">
                    <?php
                             //afficher par devise
                             $total_truition_fee = 0;
                             $totalTuitionFeeProg = 0;
                       $modelDevise = SrcDevises::findOne($default_currency);
                      
                         
                         
                                        //$default_currency = $devise->id;
                                        
                                             $modelProgramIncome= new SrcBillings;
                                                  //$total_truition_fee = 0;
                                                  //return a double-> sum_amount_pay
                                                  $totalTuitionFeeProg = $modelProgramIncome->searchTuitionFeeByProgram($program->id, $modelDevise->id, $acad);

                                                   

                                                  echo $modelDevise->devise_symbol.'<br/> '.numberAccountingFormat($totalTuitionFeeProg);
                                                  
                                                  
                                                  
                                              if($totalTuitionFee!=null)
                                                {   
                                                 foreach($totalTuitionFee as $totalTuition)
                                                  {
                                                      if($totalTuition["devise"]==$modelDevise->id)
                                                          $total_truition_fee = $total_truition_fee + $totalTuition["sum_amount_pay"];
                                                  }
                                                  
                                                }
                                                  
                                    
                                             ?>
                                       
                                
                                   
                       <?php
                             
                       ?>
                    
                    </span> </h3>
                <h4><span class='percent teks-blan'><?= Yii::t('app','Tuition fees') ?>, <?php if($total_truition_fee!=0) echo round(($totalTuitionFeeProg/$total_truition_fee*100), 2); else  echo 0; ?>%</span></h4>
              <p><?= $program->short_name; ?></p>
            </div>
            <div class="icon">
              <i class="<?= getDesignByProgram($program->short_name)['fa']; ?>"></i>
            </div>
         
          </div>
        </div> 
        
        
                  
                    
                    
  <?php
                }
                
             }
                 
  ?>
        <!-- specialisation -->   
        <div class="col-lg-3 col-xs-6 ti-bwat">

            <!-- small box -->
            <div class="small-box bg-blue"  data-html="true" rel="tooltip" href="#"  >
              <div class="inner">
                  <h3 style="font-size:25px;"><span class="teks-blan">
                      <?php
                              
                             $total_truition_fee = 0;
                             $totalTuitionFeeLevel = 0;
                             $level =3;
                             
                       $modelDevise = SrcDevises::findOne($default_currency);
                      
                         $modelProgramIncome= new SrcBillings;
                                                  
                          //return a double-> sum_amount_pay
                          $totalTuitionFeeLevel = $modelProgramIncome->searchTuitionFeeByLevelDevise($level, $modelDevise->id, $acad);

                          echo $modelDevise->devise_symbol.'<br/> '.numberAccountingFormat($totalTuitionFeeLevel);
                                                  
                      
                             

                       ?>



                         <?php

                         ?>

                      </span> </h3>
                  <h4><span class='percent teks-blan'><?= Yii::t('app','Total Income') ?> (<?= Yii::t('app','Specialisation') ?>)</span></h4>

              </div>
              <div class="icon">
                <i class="fa fa-space-shuttle"></i>
              </div>

            </div>
          </div> 
        
        <!-- other income -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           
          <!-- small box -->
          <div class="small-box bg-info"  data-html="true" rel="tooltip" href="#"  >
            <div class="inner">
                <h3 style="font-size:25px;"><span class="teks-blan">
                    <?php
                           $modelServices = new SrcBillingsServices;       
                            $totall_billing_service = $modelServices->getTotalAmountForServices($modelDevise->id, $acad);
                            
                            $modelOtherIncome = new SrcOtherIncomes;       
                            $total_other_income = $modelOtherIncome->getTotalAmountOtherIncome($modelDevise->id, $acad);
                            
                            $_total = $totall_billing_service + $total_other_income;
                        
                          echo $modelDevise->devise_symbol.'<br/> '.numberAccountingFormat($_total);
                                                  
                     ?>
                                       
                                
                                   
                       <?php
                         
                       ?>
                    
                    </span> </h3>
                <h4><span class='percent teks-blan'><?= Yii::t('app','Total') ?> <?= Yii::t('app','Other Income').' & '.Yii::t('app','Services') ?></span></h4>
             
            </div>
            <div class="icon">
              <i class="fas fa-dollar-sign"></i>
            </div>
         
          </div>
        </div>
        
         <!-- loan -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           
          <!-- small box -->
          <div class="small-box bg-fuchsia"  data-html="true" rel="tooltip" href="#"  >
            <div class="inner">
                <h3 style="font-size:25px;"><span class="teks-blan">
                    <?= Yii::t('app','Loan') ?>
                    <?php
                            $modelLoan = new SrcLoanOfMoney;       
                            $loan_persons = $modelLoan->getTotalPersonsHavingLoan($acad);
                          echo '<br/> '.$loan_persons;
                                                  
                     ?>
                                       
                                
                                   
                       <?php
                         
                       ?>
                    
                    </span> </h3>
                <h4><span class='percent teks-blan'><?= Yii::t('app','Person(s)') ?></span></h4>
             
            </div>
            <div class="icon">
              <i class="fas fa-hand-holding-usd "></i>
            </div>
         
          </div>
        </div>
         
          <!-- scholarship holder -->
         <div class="col-lg-3 col-xs-6 ti-bwat">
           
          <!-- small box -->
          <div class="small-box bg-purple-gradient"  data-html="true" rel="tooltip" href="#"  >
            <div class="inner">
                <h3 style="font-size:25px;"><span class="teks-blan">
                    <?= Yii::t('app','Scholarship holder') ?>
                    <?php
                          $modelScholarshipHolder = new SrcScholarshipHolder;       
                            $scholarship_persons = $modelScholarshipHolder->getTotalScholarshipHolders($acad);
                               echo '<br/> '.$scholarship_persons;
                                                  
                     ?>
                                       
                                
                                   
                       <?php
                         
                       ?>
                    
                    </span> </h3>
                <h4><span class='percent teks-blan'><?= Yii::t('app','Student').'(s)' ?></span></h4>
             
            </div>
            <div class="icon">
              <i class="fas fa-funnel-dollar"></i>
            </div>
         
          </div>
        </div>
    
    </div>  
                
               
            
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5><?= Yii::t('app','Program income by shift'); ?> ( <?= $modelDevise->devise_symbol ?> )</h5>
                                
                            </div>
                            <div class="ibox-content">
                               

                                <div class="m-t-sm">

                                    <div class="row">
                                        <div class="col-lg-9">
                                          <div class="flot-chart">
                                            <div class="flot-chart-content" id="morris-bar-chart"></div>
                                          </div>
                                        </div>
                                        
                                        <div class="col-lg-3">
                                    <ul class="stat-list">
                                        
                            <?php       foreach($modelShifts as $shift)
                                        {

                               ?>           
                                        <li>
                                            <h2 class="no-margins">
                                            <?php $modelIcomeFeeByShift= new SrcBillings;
                                    
                                          $totalIcomeFeeShift = $modelIcomeFeeByShift->getTotalIncomeFeeByShift($shift->id, $default_currency, $acad);
                                     
                                           $whole_icome_fee = $modelIcomeFeeByShift->getWholeIncomeFee($default_currency, $acad);
                                          
                                          echo numberAccountingFormat($totalIcomeFeeShift);
                                     ?>
                                            </h2>
                                            <small><?= Yii::t('app','Total income in').' '.$shift->shift_name; ?></small>
                                            <div class="stat-percent"><?php if($whole_icome_fee!=0) echo round(($totalIcomeFeeShift/$whole_icome_fee*100), 2); else echo 0; ?>% </div>
                                            <div class="progress progress-mini">
                                                <div style="width: <?php if($whole_icome_fee!=0) echo round(($totalIcomeFeeShift/$whole_icome_fee*100), 2); else echo 0; ?>%;" class="progress-bar"></div>
                                            </div>
                                        </li>
                              <?php
                                        }
                              ?>          
                                        </ul>
                                    </div>
                                       
                                    </div>

                                </div>

                                

                            </div>
                        </div>
                    </div>
                  
                    

                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                          <div class="ibox-title">
                             
                           <?php
                                   //cheche denye date update la
                                     $modelLastBilling = new SrcBillings;
                                     $modelLastDateBilling = $modelLastBilling->lastTransactionDate();
                                     
                                     
                                     $last_date=date('Y-m-d');
                                     
                                     if($modelLastDateBilling!=null)
                                     {
                                         foreach($modelLastDateBilling as $lastDateBilling)
                                         {     
                                             $last_date= $lastDateBilling["denyedatecreate"];
                                              if($lastDateBilling["denyedatupdate"] >= $last_date )
                                                  $last_date = $lastDateBilling["denyedatupdate"];
                                         }
                                     }
                              ?>
                              
                              <h5><?php Yii::$app->formatter->locale='fr-FR';  echo Yii::t('app','Fee total income on').':  '.Yii::$app->formatter->asDate($last_date); 
                                
                                $modelIcomeFee= new SrcBillings;
                                
                                $whole_icome_fee=0;
                                
                                $modelDevise = SrcDevises::find()->all();
                                
                                $pase=0;
                                 
                                echo '<div id="en-tete" class="span12 btn disabled" style="text-align:center; background: #EE6539 opacity: 0.6"> ';
                                    foreach($modelDevise as $devise)
                                      {
                                        $whole_icome_fee = $modelIcomeFee->getDailyWholeIncomeFee($devise->id, $last_date );
                                        
                                        if($pase!=0)
                                            echo ' | ';
                                        else {
                                            $pase=1;
                                           }
                                        
                                        echo '<span style="margin: 0 10px;">'.Yii::t('app','Total <u style="color : #000"> {name}</u>',array('name'=>$devise->devise_name) ).' '.$devise->devise_symbol.' '.numberAccountingFormat(round($whole_icome_fee,2) ).'</span>';
                                        
                                         
                            
                                      }
                                      
                                 echo '</div>';   
                                ?>
                                </h5>
                               
                            </div>
                            <div class="ibox-content">
                                 <div class="col-md-14 table-responsive"  style="margin-top:-12px; margin-bottom:55px;" >
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Fee'); ?></th>
				            <th><?= Yii::t('app','Amount To Pay'); ?></th>
				            <th><?= Yii::t('app','Amount Pay'); ?></th>
				            <th><?= Yii::t('app','Balance'); ?></th>
				            <th><?= Yii::t('app','Payment Method'); ?></th>
                                            <th><?= Yii::t('app','Received by'); ?></th>
				            

				            </tr>
				        </thead>
				        <tbody>
                            <?php

                                $dataBilling = $modelIcomeFee->searchTodayIncomeFee($last_date);


                                      foreach($dataBilling as $billing)
                                       {


                                       echo '  <tr >
                                                                        <td >'.Html::a($billing->student0->id_number, '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').' </td>
                                                                        <td >'.Html::a($billing->student0->first_name, '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').' </td>
                                                                        <td >'.Html::a($billing->student0->last_name, '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').' </td>
                                                                        <td >'.Html::a($billing->feePeriod->fee0->fee_label.' ('.$billing->feePeriod->program0->short_name.')', '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').' </td>
                                                                        <td >'.Html::a($billing->getAmountToPay(), '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').'</td>
                                                                        <td >'.Html::a($billing->getAmountPay(), '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').'</td>
                                                                        <td >'.Html::a($billing->getBalance(), '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').'</td>
                                                                        <td >';
                                                                                if(isset($billing->paymentMethod))
                                                                                    echo Html::a($billing->paymentMethod->method_name, '../../billings/view?id='.$billing->student.'&ri=0&part=rec');
                                                                       
                                                                        $resupar ='';
                                                                           if($billing->updated_by!=null)
                                                                               $resupar = $billing->updated_by;
                                                                           else
                                                                           {
                                                                               if($billing->created_by=='SIGES')
                                                                                   $resupar = Yii::t('app', 'System');
                                                                               else
                                                                                   $resupar = $billing->created_by;
                                                                           }
                                                                               
                                                                  echo '</td>
                                                                        <td >'.Html::a($resupar , '../../billings/billings/view?id='.$billing->student.'&ri=0&part=rec').'</td>
                                                                        ';

 
                                                                         

                                                                 echo '

                                                                    </tr>';
                                            }
                           ?>
                                                 </tbody>
                                        </table>


                                     </div>

                            </div>
                        </div>
                    </div>

             

            </div>

       
 <?php

 $title_chart = Yii::t('app','Monthly total tuition fees by program');
 
 
 $data_program=null;
 
 $data =null;
 

 
   
    
    $data_=null;
       $pass=0;
       $pass1=0;
      
    foreach($modelProgram as $program)
      {
       
       $modelShiftProgramIncome= new SrcBillings;
        $total_truition_fee = 0;
        
        $data_ = null;
        $pass=0;
        
      for($i=1;$i<=12;$i++)
        {
          //return a double-> sum_amount_pay
         $totalTuitionFeeProg = $modelShiftProgramIncome->searchTuitionFeeByProgramAndMonth($program->id, $i, $default_currency, $acad);
   
         if($pass==0)
          { $data_= $data_. $totalTuitionFeeProg;
             $pass=1;
            }
         else
           { $data_= $data_. ', '.$totalTuitionFeeProg;
             
           }
        
        
        }  
        
         if($pass1==0)
          { $data= $data. '{ name: \''.$program->short_name.'\', data: ['.$data_.'] }';
          
             $pass1=1;
            }
         else
           { $data= $data. ', { name: \''.$program->short_name.'\', data: ['.$data_.'] }';
             
           }
          
  
           
       }
       
      
   
    
    $i=0;
    $pass=0;
    $data_bil =null;
    
    $color=["#d3d3d3", "#bababa", "#79d2c0", "#1ab394", "#aedd2c"];
    
 foreach($modelShifts as $shift)
   {
     $value = null;
     $ykeys=null;
     $l = null;
     $i=0;
     $pass_value=0;
     $sp=0;
     $total_income_sp=0;
     
   foreach($modelProgramGraf as $program)
     {
        
     
       
       //cheche kantite kob us ki antre pou programb sa 
       
       if($program->is_special==0)
        {
          // $l = $program->short_name;
          // $ykeys= $ykeys.',\''.$program->short_name.'\'';
           
           $modelBilling= new SrcBillings;
       
           //return a double 
           $total_income_in_shift_by_Program = $modelBilling->getTotalIncomeInShiftByProgramDevise($shift->id, $program->id, $default_currency, $acad);

            
            if($pass_value==0)
               { 
                  $value = $program->short_name.': \''.$total_income_in_shift_by_Program.'\'';
                  
                   $ykeys= '\''.$program->short_name.'\'';
                   
                   $l= '"'.$program->short_name.'"';
                   
                   
                 $pass_value=1;
               }
            else
                {
                   $value= $value.', '.$program->short_name.': \''.$total_income_in_shift_by_Program.'\'';
                   
                    $ykeys= $ykeys.',\''.$program->short_name.'\'';
                    
                    $l = $l.', "'.$program->short_name.'"';
                    
                    
                }
              
             
        }
      elseif($program->is_special==1)
        {
           $modelBilling= new SrcBillings;
       
           //return a double 
           $total_income_in_shift_by_Program = $modelBilling->getTotalIncomeInShiftByProgramDevise($shift->id, $program->id, $default_currency, $acad);

          $total_income_sp = $total_income_sp + $total_income_in_shift_by_Program;
          
         
        }
      
         
        
      }
      
         if($value==null)
               { 
                  $value = 'SP: \''.$total_income_sp.'\'';
                  
                  if($sp==0)
                    {   
                        $ykeys= '\'SP\'';

                         $l = '"'.Yii::t('app','SP').'"';
                    }
                   
                   $sp=1;
                 $pass_value=1;
               }
            else
                {
                   $value= $value.', SP: \''.$total_income_sp.'\'';
                   
                    if($sp==0)
                    {   
                        $ykeys= $ykeys.',\'SP\'';
                    
                        $l = $l.', "'.Yii::t('app','SP').'"';
                    }
                    
                    $sp=1;
                }
                
      
         if($pass==0)
               { $data_bil=  '{ y: "'.$shift->shift_name.'", '.$value.' }';

                  $pass=1;
                 }
              else
                { $data_bil= $data_bil.', { y: "'.$shift->shift_name.'", '.$value.' }';

                }
                
           
           
      
   }
   
   
 
 
 
 
 
//echo $data_bil;


 $script1 = <<< JS
    $(document).ready(function(){
         
      $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Recette scolarite du jour'},
                    {extend: 'pdf', title: 'Recette scolarite du jour'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });
                
   
        $('#no-stud').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_students; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idstud-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/billings/billings/view?id="+id_stud+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }
           
            });
         
        
         
        Morris.Bar({
        element: 'morris-bar-chart',
        data: [$data_bil] ,
        xkey: 'y',
        ykeys: [$ykeys],
        labels: [$l],
        hideHover: 'auto',
        resize: true,
        barColors: ["#EC4758", "#21B9BB", "#F7A54A", "#6561AA", "#aedd2c"],
         });

         
         
    });
    
    
   $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/billings/billings/view?id="+id_stud+"&month_=&year_=&wh=inc_bil&ri=0";
            
            }

        }

    });
         
         
JS;

 $this->registerJs($script1); 
 
   
?>