<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use app\modules\reports\models\RptCustom;
use app\modules\reports\models\RptCustomCat;


$a_variables = explode(",", $rpt->variables); 
$re_write_variable = array(); 
for($i=0;$i<count($a_variables);$i++){
    $re_write_variable[$i] = "{%".$a_variables[$i]."%}";  
}

$str_sql =  $rpt->data;
$str_final = str_replace($re_write_variable, $a_variables, $str_sql);

?>



<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?= Yii::t('app','{nameReport}',array('nameReport'=>$rpt->title));  ?>
                        
         </h3>
    </div>

<?php
       
 
 ?>   

    <div class="col-lg-5">
        <p>
                <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
                 ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List report'), ['list', 'from1' => 'rpt','cat'=>$cat], ['class' => 'btn btn-info btn-sm']) ?>

    </p>
    </div>

<?php
          
?>

</div> 



<div class="wrapper wrapper-content ">  
<div class="col-lg-12 row">
<?php
    if($rpt->variables == NULL || $rpt->variables == ""){
?>

   
    <?php 
    // Pour afficher les dates au format francais 
    $locale = "fr_FR";
    if(isset($_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']) && $_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']=="fr" ){
        $locale = "fr_FR";
    }else{
       $locale = 'fr_FR';
    }
    Yii::$app->getDb()->createCommand("SET lc_time_names = '$locale'")->execute();
    $name = Yii::$app->getDb()->createCommand($rpt->data)->queryAll();
    $clean_data = array();
    $i=0;
    foreach ($name as $n){
        $clean_data[$i] = $n;
        $i++;
    }
   
    
 $comp =1;   
    ?>
    
 <div class="col-md-12 table-responsive">   
    <?php if (count($clean_data) > 0): ?>

 <table class='table table-striped table-bordered table-hover dataTables-example'>
  <thead>
    <tr>
      <th></th><th><?php echo implode('</th><th>', array_keys(current($clean_data))); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($clean_data as $row): array_map('htmlentities', $row); ?>
      
    <tr>
      <td><?= $comp ?></td><td><?php echo implode('</td><td>', $row); ?></td>
    </tr>
   
<?php $comp++; endforeach; ?>
  </tbody>
</table>

<?php elseif($clean_data==null):  
        echo Yii::t('app','No result found');
    endif;

?>
</div>




    

    <?php }else{
        ?>
    
  <?php $form = ActiveForm::begin(['id'=>'runReport','action'=>['/reports/report/runreport?id='.$id.'&from1=rpt'] ]); ?> 
   <?php 
       
        $decodedText = html_entity_decode($rpt->setup_variable);
        $myArray = json_decode($decodedText, true);
        $data_type = $myArray['data_type']; 
        $param_value = $myArray['param_value']; 
       ?>
   
    
   <?php 
        for($i = 0; $i<count($a_variables); $i++){
            ?>
       <div style="margin-right: 10px;" >
        <?php 
            switch ($data_type[$i]){
                case "txt":
                {
                
                    ?>  
            <div class="" style="float:left; margin-right: 23px;  margin-bottom: 15px;" ><b><?= ucfirst(str_replace('_',' ',$a_variables[$i])); ?></b></div>
                    <div class="" style="float:left; margin-right: 20px;  margin-bottom: 15px;" ><input name="<?= 'field'.$i ?>" type="text"/></div>

    <?php 
             
                }
                break;
                case "date":{
                    ?>
                    <div class="" style="float:left; margin-right: 23px;  margin-bottom: 15px;" ><b><?= ucfirst(str_replace('_',' ',$a_variables[$i])); ?>
                              </b></div>
                    <div class="" style="float:left; margin-right: 20px; margin-top: -5px; margin-bottom: 15px;" >
                    <?php 
                             echo DatePicker::widget([
                                        'name'  => 'field'.$i,
                                 'id'  => 'field'.$i,
                                        'value'  => '',
                                        'language'=>'fr',
                                        //'dateFormat' => 'yyyy-MM-dd',
                                        'pluginOptions'=>[
                                           'autoclose'=>true,
                                          'format'=>'yyyy-mm-dd',
                                            'yearRange'=>'1900:2100',
                                          ],
                                    ]);
                        
                    ?>
                    </div>
    <?php 
                }
                break;
                case "static-combobox":{
                    
                     if($param_value['static-combo'.$i]!=NULL){
                    ?>
                    <div class="" style="float:left; margin-right: 23px;  margin-bottom: 15px;" ><b>
                        <?php 
                           
                            echo ucfirst(str_replace('_',' ',$a_variables[$i])); 
                    ?>
                    </b></div>
                    <div class="" style="float:left; margin-right: 20px;  margin-bottom: 15px;" >
    <?php 
                        $pair_index_value = explode(",",$param_value['static-combo'.$i]);
                        echo "<select name=field$i>"; 
                            for($j=0;$j<count($pair_index_value); $j++){
                                $list_final = explode(":",$pair_index_value[$j]); 
                                echo "<option value='$list_final[0]'>$list_final[1]</option>";
                            }
                        echo "</select>"; 
                        
                     ?>
                       </div>
                  <?php 
                        }  
                }
                break;
                case "dynamic-combobox":{
                   
                      if($param_value['dynamic-combo'.$i]!=NULL){
                    ?>
                       <div class="" style="float:left; margin-right: 23px;  margin-bottom: 15px;" ><b>
                        <?php 
                          
                            echo ucfirst(str_replace('_',' ',$a_variables[$i])); 
                    ?>
                    </b></div>
                   <div class="" style="float:left; margin-right: 20px;  margin-bottom: 15px;" > 
                <?php 
                    $str_sql = $param_value['dynamic-combo'.$i]; 
                    preg_match('~T(.*?)F~', $str_sql, $output);
                    $title = explode(",",$output[1]);
                    
                    $tag_id = str_replace(' ','',$title[0]);
                    $tag_value = str_replace(' ','',$title[1]);
                            
                    //pou id SELECT la
                    preg_match("/DISTINCT(.*)/", $title[0], $results_id);
                    
                     if($results_id!=null)
                     {  
                       $tag_id = str_replace(' ','',$results_id[1]);
   //print_r('<br/><br/><br/>***********************************************'.$tag_id.'****************'.$results_id[0].'********');                      
                          
                     }
                    
                      
                     
                    $data = Yii::$app->getDb()->createCommand($str_sql)->queryAll();
                    $clean_ = array();
                    $k=0;
                   
                    foreach($data as $n){
                        $clean_[$k] = $n;
                      
                        $k++;
                       
                    }
                   echo "<select name='field$i'>";
                        foreach($clean_ as $c){
                            // $tag_id = str_replace(' ','',$title[0]);
                            // $tag_value = str_replace(' ','',$title[1]);
                            echo "<option value='".$c[$tag_id]."'>".$c[$tag_value]."</option>";
                            
                        }
                   
                      echo "</select>";  
                    
                     ?>
                      </div>
                          
                    <?php
                      
                      
                        }   
                }
            }
            ?>
    </div>
            <?php 
        }
            
           
        ?>
         
            <div class="col-sm-2" style="margin-top: -5px;">            
                        
                <a id= "btnSave" name="btnSave" class="btn btn-primary" ><?= Yii::t('app', 'Run report') ?></a>  
            
                    <?php
                    echo "<br/>";
       
            ?>
             </div>
     
                  
                     
 
    
<div style="clear:both; margin-bottom: 30px;"></div>

    <div id="roule-rapo">
    <div id="default-message"><?= Yii::t('app', 'Set parameters and run the report') ?></div>
   </div>

<?php $form = ActiveForm::end(); ?>

<?php 
    
    } ?>
                     
                     
   
    
</div>

</div>

    <?php
    $search_text = Yii::t('app','Search');
    $processing_text = Yii::t('app','Processing ...');
    $script = <<< JS
    
   
   $('#btnSave').on('click', function(e){
           
           var lru = "runreport?id=$id&from1=rpt";
            
             var form = $('#runReport');
             var formData = form.serialize();
            
            $("#default-message").hide();
            
              $.ajax({

                    url: form.attr("action"),

                    type: form.attr("method"),

                    data: formData,

                    success: function (data) {

                      document.getElementById("roule-rapo").innerHTML = data;

                    },

                    error: function () {

                        alert("Something went wrong");

                    }

                });
          
            
    });
          
     $(document).ready(function(){
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'List Report'},
                    //{extend: 'pdf', title: 'List Report'},

                    
                ]

            });

        });

JS;
$this->registerJs($script);



?>
            