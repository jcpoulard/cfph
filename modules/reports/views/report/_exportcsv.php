<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\rbac\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
$acad =3;// Yii::$app->session['currentId_academic_year'];

Yii::$app->session['tab_index']=0;
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<?php 
					

 $modelShift_ = new SrcShifts();
 $modelCourse = new SrcCourses();
	
	
?>

<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>
      
      <div class="row">
 
          
        <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
           <?php
                    
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       	$data_shift = [];
		       
        
           ?>
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-shift").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-room").html("");
                                        	
                                        });',
                                     
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
          
        
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         
              if($model->shift[0]!='')

                {       $modelShift_ = new SrcShifts();
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                  }      
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&level='.'"+$("#srcstudenthascourses-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        $data_room = [];
              if($model->room[0]!='')
                {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                      //   ->all(),'room0.id','room0.room_name' );
                                           $from = 'grades';
                     $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift[0],$model->program,$model->level,$acad);
                  }      
                 ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                    
                                   'onchange'=>'submit()',
  
                       ],
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  
     
      </div>
       
</div>
    
<ul class="nav nav-tabs"></ul>  
<br/>  

<div class="wrapper wrapper-content">
             
     <div class="col-md-14 table-responsive">
<?php

      $room_name = '';
      
    if($room!='')
       {   
            $room_name = SrcRooms::findOne($room)->room_name.'_'.date('Y-m-d');

       $moduleGrade_ = new SrcGrades; 
			
	$dataProvider = $moduleGrade_->getAllSubjectsByLevel($program,$shift,$level,$room);
			
				
	//liste des etudiants
	$dataProvider_student = $model->getInfoStudentsEnrolledGroup($room, $level, $shift, $acad);
			 
     $couleur=1;
     $k=0;
       echo '<table class="table table-striped table-bordered table-hover dataTables-example">
             <thead>
                <tr>
                <th></th>
                <th></th>';
                       //liste des cours
		while(isset($dataProvider[$k][0]))
				{
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {  $id_course = $dataProvider[$k][4];
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
							  if($old_subject_evaluated)
								 {
								 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
									}
		
						 }
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
						}
						
						         
			      
			      if($careAbout)
				     {			
						
				// $dataProvider[$k][1] subject_name
				// $dataProvider[$k][3] short_subject_name
                                  // $dataProvider[$k][6] teacher_name
				 
				 echo ' <th >'.$dataProvider[$k][5].'</th>';
				   
				      }
				      
				      $k++;
		        }
                
    echo '</tr>';
    
                        
       echo '</thead>
	     <tbody> '; 
        
       //pou chak elev
			  
                            if(isset($dataProvider_student)&&($dataProvider_student!=null))
			       {  
				  $students=$dataProvider_student;
								
				     foreach($students as $stud)   
				       {  
				           $k=0;
				              	 
				           echo '<tr>
						   <td><b>'.$stud->first_name.'</b></td>
				  		   <td><b>'.$stud->last_name.'</b></td>';
				  								
				  	        while(isset($dataProvider[$k][0]))
				                  {
	                                               if($dataProvider[$k][4]!=NULL) //reference_id
                                                         {  $id_course = $dataProvider[$k][4];
                                                                         //si kou a evalye pou peryod sa
                                                                        $old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
                                                                          if($old_subject_evaluated)
                                                                                 {
                                                                                        $careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
                                                                                  }
                                                                           else
                                                                                 {  
                                                                                        $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
                                                                                        }

                                                                 }
                                                         else
                                                                {  $id_course = $dataProvider[$k][0];
                                                                    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
                                                                }
													
													         
			  
			                                 if($careAbout)
				                           {			
							                           
                                                            //calculer grade puis afficher
                                                             $debase_passingGrade=0;
                                                            $debase='';
                                                             $weight='';
                                                             $id_course=0;
                                                             $passing_value=0;

                                                              if($dataProvider[$k][4]!=NULL) //reference_id
                                                                 {  $id_course = $dataProvider[$k][4];
                                                                                 //si kou a evalye pou peryod sa
                                                                                $old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($id_course);         
                                                                                  if($old_subject_evaluated)
                                                                                         {
                                                                                                $id_course = $dataProvider[$k][4];						                        
                                                                                          }
                                                                                   else
                                                                                         {  
                                                                                                $id_course = $dataProvider[$k][0];						                       															                       
                                                                                                }

                                                                         }
                                                                 else
                                                                        {  $id_course = $dataProvider[$k][0];

                                                                        }


                                                                //note de passage du cours
                                                                $modelCourse = new SrcCourses();
                                                              $modelCourse=SrcCourses::findOne($id_course); 
                                                                 $passing_value =  $modelCourse->passing_grade;//note de passage pour le cours

                                                                  $grades= $moduleGrade_->searchForReportCard($stud->id,$id_course);

                                                              if(isset($grades))
                                                               {
                                                              $r=$grades ;//return a list of  objects
                                                               if($r!=null)
                                                                 { foreach($r as $grade)
                                                                    {
                                                                      echo '<td>';


                                                                      if($grade->grade_value!=null)
                                                                         echo $grade->grade_value;
                                                                      else
                                                                         echo 'N/A';

                                                                      echo '</td>';

                                                                     }
                                                                  }
                                                               else
                                                                 { echo '<td>';
                                                                          echo 'N/A';
                                                                      echo '</td>';
                                                                  }

                                                               }//fin if(isset($grades))
															
							   }//fin careAbout
														
						  $k++;
					                    
				                  }//fin while(isset($dataProvider[$k][0]))								  
									  
	                                 echo '</tr>';
			
                                   }// fin foreach($students as $stud)
							  
					
		            }// fin if(isset($dataProvider_student))
							  
							  
		
        
        echo '   </tbody>
              </table>';
        
        
       }
        
 ?> 

     </div>
  

</div>

 <?php ActiveForm::end(); ?>

</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: '<?= $room_name ?>'},
                    
                ]

            });

        });

JS;
$this->registerJs($script);

?>

   




		   
			

	

