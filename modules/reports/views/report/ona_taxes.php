<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcPayrollSettings;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



?>
    


    
    
<div id="payroll_receipt" class="" style=" width:100%;">
       

   <?php
	
           //Extract school name 
           $school_name = infoGeneralConfig('school_name');
            //Extract school address
            $school_address = infoGeneralConfig('school_address');
            //Extract  email address 
           $school_email_address = infoGeneralConfig('school_email_address');
                                                            //Extract Phone Number
            $school_phone_number = infoGeneralConfig('school_phone_number');

             $school_acronym = infoGeneralConfig('school_acronym');

            $school_name_school_acronym = $school_name; 

            if($school_acronym!='')
               $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
            
     $acad=Yii::$app->session['currentId_academic_year'];


	$payroll_month =$month_;
        
        $year = $year_;
        
        $month = '';
        if($payroll_month!='')
            $month = $modelPayroll->getSelectedLongMonth($payroll_month);
		
?>  
        <div id="print" style=" width:100%;">
			<?php
         	
         				             
           echo ' <div id="header" style=" width:110%;">
                 
                  
                  <div  style="text-align:center; "> <b>'.strtoupper('Centre de Formation Professionnelle d\'Haïti <br/>Canado-technique<br/><br/>'.Yii::t('app','Paiement de taxes a l\'ONA') ).'<br/>'.Yii::t('app','NIF : 000-634-202-1').'</b></div>
                  </div> <br/>'; 
                  
        
           echo ' <div id="p_month" style="margin-left:20px; "> <b>'.Yii::t('app','Month').' '.strtoupper($month ).' '.$year.'</b> </div> ';
           
           echo '  <table  class="table table-striped table-bordered table-hover dataTables-example" style="width:100%;  ">
		       <tr style="width:100%; background-color: lightgrey; "> 
			   <th style="text-align:center;  "> '.Yii::t('app',' No ').' </th>
                           <th style="text-align:center;  ">'.Yii::t('app','NOM').' </th>
                           <th style="text-align:center;  "> '.Yii::t('app','PRENOM').'</th>
                           <th style="text-align:center;  "> '.Yii::t('app','SAL. BRUT').'</th>
                           <th style="text-align:center;  "> '.Yii::t('app','EMPLOYÉ').'</th>
                           <th style="text-align:center;  "> '.Yii::t('app','EMPLOYEUR').'</th>
                       </tr>
                 ';
     $total_taxe =0;
     $total_sal_brut =0;     
           
       //cheche payroll la
         $comptuer=1;
     	 $modelPayroll1 = $modelPayroll->searchByMonthYearForReport($payroll_month, $year);
     	 $modelPayroll2 = $modelPayroll1->getModels();
     	 if($modelPayroll2!=null)
     	   {
     	   	    foreach($modelPayroll2 as $payroll_)
     	   	      {
     	   	      	      $id_payroll_set ='';
                                $id_payroll_set2 ='';
                               $payment_date ='';
                               $payroll_date = '';
                               $net_salary = 0;
                               $taxe = 0;
                               $frais = 0;
                               $plus_value = 0;
                               $total_deduction = 0;
                               $number_of_hour = null;
                               $missing_hour = 0;
                               $gross_for_hour = 0;

                               $assurance = 0;
                               $display = 0;

                               $employee = $payroll_->idPayrollSet->person->fullName;
                               $gross_salary= $payroll_->getGrossSalaryIndex_value($payroll_->idPayrollSet->devise,$payroll_->idPayrollSet->person->id,$payroll_->payroll_month,getYear($payroll_->payment_date));
                               $currency = '';
                            /*   $currency_result = Fees::model()->getCurrency($acad);
                               foreach($currency_result as $result)
                                {
                                       $currency = $result["devise_name"].'('.$result["devise_symbol"].')';
                                       break;
                                        }

                              $currency = $currency_name.' '.$currency_symbol;
                              */  

          	 
                             $currency_symbol = $payroll_->idPayrollSet->devise0->devise_symbol;
                        
                        
                        
                             $payment_date = $payroll_->payment_date;
     	   	      	     $payroll_date = $payroll_->payroll_date;
     	   	      	     $net_salary = $payroll_->net_salary;
     	   	      	     $id_payroll_set = $payroll_->id_payroll_set;
     	   	      	     $id_payroll_set2 = $payroll_->id_payroll_set2;
     	   	      	     $assurance = $payroll_->idPayrollSet->assurance_value;
     	   	      	     $frais = $payroll_->idPayrollSet->frais;
     	   	      	     $plus_value = $payroll_->plus_value;
     	   	      	     $taxe = $payroll_->taxe;
     	   	      	     $missing_hour = $payroll_->missing_hour;
   	   	      	     
     	   	   
	  /*   if($missing_hour!=0)
	       {
	       	   $number_of_hour = $modelPS->getSimpleNumberHourValue($model->idPayrollSet->person->id);
	       	   $gross_for_hour = $gross_salary;
	       	 }
	     */
           
           
			     if($taxe != 0)  
                               { 
                                    $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set;

                                                      $command__ = Yii::$app->db->createCommand($sql__);
                                                      $result__ = $command__->queryAll(); 

                                                            if($result__!=null) 
                                                             { foreach($result__ as $r)
                                                                 { 
                                                                     $sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];

                                                                              $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
                                                                              $result_tx_des = $command_tx_des->queryAll(); 

                                                                                    foreach($result_tx_des as $tx_desc)
                                                                                         {   if( ($tx_desc['taxe_description']=='ONA') )
                                                                                              { $display = 1;
                                                                                                 break;
                                                                                              }
                                                                                             
                                                                                           }


                                                                 }

                                                               }
                                 
                              if($display == 1)
                                {
                                 
                                 echo ' <tr> 
                       
                                            <td style="text-align:center; "> '.$comptuer.'  </td>
                                            <td style="padding-left:20px; "> '.$payroll_->idPayrollSet->person->last_name.'  </td>
                                            <td style="padding-left:20px; "> '.$payroll_->idPayrollSet->person->first_name.'  </td>
                                            <td style="text-align:center; "> '.numberAccountingFormat($gross_salary).'  </td>';
                                                                                                                        
			        	$total_sal_brut = $total_sal_brut + $gross_salary;
                                        
                                                           foreach($result__ as $r)
                                                                 { 
                                                                      $deduction = 0;
                                                                     $tx_des='';
                                                                       $tx_val='';

                                                                     $sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];

                                                                              $command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
                                                                              $result_tx_des = $command_tx_des->queryAll(); 

                                                                                    foreach($result_tx_des as $tx_desc)
                                                                                         {   $tx_des= $tx_desc['taxe_description'];
                                                                                             $tx_val= $tx_desc['taxe_value'];
                                                                                           }


                                                                     if( ($tx_des=='ONA') ) //, 
                                                                          {
                                                                               $deduction = ( ($gross_salary * $tx_val)/100);
                                                                                             
                                                                                  echo '<td style="text-align:center; "> '.numberAccountingFormat($deduction).'  </td>
                                                                                        <td style="text-align:center; ">'.numberAccountingFormat($deduction).' </td>';
                                                                           
                                                                                  $total_taxe = $total_taxe + $deduction;
                                                                                 
                                                                           }



                                                                 }

                                                               

                                          
                                             echo ' </tr>';                   
                                       $comptuer++;                        
                                        }
                                        
                                    }
                                    
			           	
                  

			      
			 
                            
                   }
     	   	      	
     	     }
	      
              echo ' <tr> 
                       
                                                <th colspan ="3" style="text-align:center; "> '.Yii::t('app',' TOTAL ').'    </th>
                                                 <th style="text-align:center;  "> '.numberAccountingFormat($total_sal_brut).'</th>';
                   
                                        echo '  <th style="text-align:center;  "> '.numberAccountingFormat($total_taxe).'</th>
                                                <th style="text-align:center;  "> '.numberAccountingFormat($total_taxe).'</th>';
                                  echo ' </tr>'; 
                      
             echo '</table> ';      
                            
																			
	   ?>
	
               </div>
               
                           
			 
   
</div>


    
   

<script>
      function printContent(el)
      {
          document.getElementById("header").style.display = "block";
		    document.getElementById("p_month").style.display = "block";
		     
     
         var restorepage = document.body.innerHTML;
         var printcontent = document.getElementById(el).innerHTML;
         document.body.innerHTML = printcontent;
         window.print();
         document.body.innerHTML = restorepage;
         
          
     document.getElementById("header").style.display = "none";
      document.getElementById("p_month").style.display = "none";
    
     }
   </script>