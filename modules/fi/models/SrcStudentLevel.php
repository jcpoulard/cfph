<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\StudentLevel;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcPersons;

/**
 * SrcStudentLevel represents the model behind the search form about `app\modules\fi\models\StudentLevel`.
 */
class SrcStudentLevel extends StudentLevel
{
   
   public $program;
   
   public $shift;
   
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'level', 'room', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
      public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'program' =>Yii::t('app','Program'),
			               'shift' =>Yii::t('app','Shift'),
			               
		));
	}	

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStudentLevel::find()->joinWith(['student'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>10000000000000,
        ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'student_id' => $this->student_id,
            'level' => $this->level,
            'room' => $this->room,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
    
public function searchByProgram($program)
    {
        $query = SrcStudentLevel::find()->joinWith(['student.studentOtherInfo0'])->where(['apply_for_program'=>$program])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>10000000000000000,
        ],
        ]);


        return $dataProvider;
    }
    
   
    
   public function getStatus()
    {
    	if($this->status==0)
    	  return Yii::t('app', 'Opened');
    	elseif($this->status==1)
    	  return Yii::t('app', 'Hold');
    	elseif($this->status==2)
    	  return Yii::t('app', 'Closed');
    	
    	
     }
    
    public function searchStudentsWithoutLevel()
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0')->select('student_id');
                
                
     	$query = SrcPersons::find()->where('is_student=1')->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','id',$query_student_in]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }
    
      
  public function searchStudentsWithoutGroup()
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0')->select('student_id');
                
                
     	$query = SrcPersons::find()->where('is_student=1')->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','id',$query_student_in]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }
    

    
    public function searchStudentsWithoutLevelByProgram($program)
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0')->select('student_id');
                
                
     	$query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }

      
 
    public function searchStudentsWithoutGroupByProgram($program)
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0')->select('student_id');
                
                
     	$query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }

    public function searchStudentsWithoutLevelOrGroupByProgramShift($program,$shift)
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0 AND level IS NOT NULL')->select('student_id');
                
        if( (($program!='')&&($program!=0)) && (($shift!='')&&($shift!=0))  )        
     	  {
     	  	  $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program.' and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	  }
     	elseif( (($program!='')&&($program!=0))  )        
     	  {
     	  	    $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	 elseif( (($program=='')&&($program==0)) && (($shift!='')&&($shift!=0))  )        
     	  {
     	  	   $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	    

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }
      
    public function searchStudentsWithoutGroupByProgramShift($program,$shift)
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0 AND level IS NOT NULL')->select('student_id');
                
        if( (($program!='')&&($program!=0)) && (($shift!='')&&($shift!=0))  )        
     	  {
     	  	  $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program.' and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	  }
     	elseif( (($program!='')&&($program!=0))  )        
     	  {
     	  	    $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	 elseif( (($program=='')&&($program==0)) && (($shift!='')&&($shift!=0))  )        
     	  {
     	  	   $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	    

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }
   
 public function searchStudentsWithoutGroupByProgramShiftLevel($program,$shift,$level)
     {
     	$query_student_in = SrcStudentLevel::find()->where('room<>0 AND level IS NOT NULL')->select('student_id');
                
        if( (($program!='')&&($program!=0)) && (($shift!='')&&($shift!=0)) && (($level!='')&&($level!=0))  )        
     	  {
     	  	  $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0','studentLevel'])->where('is_student=1 and apply_for_program='.$program.' and apply_shift='.$shift.' and level='.$level)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	  }
        elseif( (($program!='')&&($program!=0)) && (($shift!='')&&($shift!=0))&& (($level=='')&&($level==0))  )        
     	  {
     	  	  $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program.' and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	  }
     	 elseif( (($program!='')&&($program!=0)) && (($shift=='')&&($shift==0))&& (($level=='')&&($level==0))   )        
     	  {
     	  	    $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_for_program='.$program)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	 elseif( (($program=='')&&($program==0)) && (($shift!='')&&($shift!=0))&& (($level!='')&&($level!=0))  )        
     	  {
     	  	  $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0','studentLevel'])->where('is_student=1 and apply_shift='.$shift.' and level='.$level)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	  }
     	elseif( (($program=='')&&($program==0)) && (($shift!='')&&($shift!=0))&& (($level=='')&&($level==0))  )        
     	  {
     	  	   $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0'])->where('is_student=1 and apply_shift='.$shift)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
          elseif( (($program=='')&&($program==0)) && (($shift=='')&&($shift==0))&& (($level!='')&&($level!=0))  )        
     	  {
     	  	   $query = SrcPersons::find()->alias('p')->joinWith(['studentOtherInfo0','studentLevel'])->where('is_student=1 and level='.$level)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
                 ->andWhere(['not in','p.id',$query_student_in]);
     	    }
     	    

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' =>100000000000,
        ],
        ]);

         
        return $dataProvider;
      
      
      }

    
    
}
