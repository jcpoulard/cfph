<?php

namespace app\modules\fi\models;

use Yii;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcScholarshipHolder;

use app\modules\fi\models\SrcContactInfo;
use app\modules\fi\models\SrcPersonsHasTitles;
use app\modules\fi\models\SrcCoursesSrcGrades;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcDepartmentHasPerson;
use app\modules\fi\models\SrcEmployeeInfo;
use app\modules\fi\models\SrcCities;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\planning\models\SrcStudentAttendance;
use app\modules\planning\models\SrcTeacherAttendance;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\StudentLevelHistory;
use app\modules\rbac\models\searchs\User as UserSearch;


/**
 * This is the model class for table "persons".
 *
 * @property integer $id
 * @property string $last_name
 * @property string $first_name
 * @property string $gender
 * @property string $blood_group
 * @property string $birthday
 * @property string $id_number
 * @property integer $is_student
 * @property string $adresse
 * @property string $phone
 * @property string $email
 * @property string $nif_cin
 * @property integer $cities
 * @property string $citizenship
 * @property integer $paid
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 * @property integer $active
 * @property string $image
 * @property string $comment
 *
* @property Balance[] $balances
		* @property Billings[] $billings
		* @property ContactInfo[] $contactInfos
		* @property Courses[] $courses
		* @property DepartmentHasPerson[] $departmentHasPeople
		* @property EmployeeInfo[] $employeeInfos
		* @property Grades[] $grades
		* @property LoanOfMoney[] $loanOfMoneys
		* @property PayrollSettings[] $payrollSettings
		* @property PendingBalance[] $pendingBalances
		* @property Cities $cities0
		* @property PersonsHasTitles[] $personsHasTitles
		* @property ScholarshipHolder[] $scholarshipHolders
		* @property StudentAttendance[] $studentAttendances
		* @property StudentHasCourses[] $studentHasCourses
		* @property StudentLevel $studentLevel
		* @property StudentOtherInfo[] $studentOtherInfos
		* @property TeacherAttendance[] $teacherAttendances
		* @property User[] $users
 */
class Persons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }
    
    public $file;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name'], 'required'],
            [['id', 'is_student', 'cities', 'paid', 'active'], 'integer'],
            [[ 'birthday','date_created', 'date_updated'], 'safe'],
            [['last_name', 'gender', 'phone', 'email', 'citizenship', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['first_name'], 'string', 'max' => 120],
            [['blood_group'], 'string', 'max' => 10],
            [['id_number', 'image'], 'string', 'max' => 50],
            [['id_number'], 'string', 'max' => 50],
            [['adresse', 'comment'], 'string', 'max' => 255],
            [['nif_cin'], 'string', 'max' => 100],
		   [['cities'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['cities' => 'id']],
            
            
            [['file'], 'file', 'extensions' => 'gif, jpg,jpeg,png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'last_name' => Yii::t('app', 'Last Name'),
            'first_name' => Yii::t('app', 'First Name'),
            'fullName' => Yii::t('app', 'Full Name'),
            'gender' => Yii::t('app', 'Gender'),
            'blood_group' => Yii::t('app', 'Blood Group'),
            'birthday' => Yii::t('app', 'Birthday'),
            'id_number' => Yii::t('app', 'Id Number'),
            'is_student' => Yii::t('app', 'Is Student'),
            'adresse' => Yii::t('app', 'Adresse'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'nif_cin' => Yii::t('app', 'Nif Cin'),
            'cities' => Yii::t('app', 'City'),
            'citizenship' => Yii::t('app', 'Citizenship'),
            'paid' => Yii::t('app', 'Paid'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
            'active' => Yii::t('app', 'Active'),
            'image' => Yii::t('app', 'Image'),
            'file' => Yii::t('app', 'Photo'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

     /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getBalances()
		   {
		       return $this->hasMany(SrcBalance::className(), ['student' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getBillings()
		   {
		       return $this->hasMany(SrcBillings::className(), ['student' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getContactInfos()
		   {
		       return $this->hasMany(SrcContactInfo::className(), ['person' => 'id']);
		   }
	
	   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getLoanOfMoneys()
		   {
		       return $this->hasMany(SrcLoanOfMoney::className(), ['person_id' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getPayrollSettings()
		   {
		       return $this->hasMany(SrcPayrollSettings::className(), ['person_id' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getPendingBalances()
		   {
		       return $this->hasMany(SrcPendingBalance::className(), ['student' => 'id']);
		   }
	 /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getScholarshipHolders()
		   {
		       return $this->hasMany(SrcScholarshipHolder::className(), ['student' => 'id']);
		   }
		   
		
		   
    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getPersonsHasTitles() 
		   { 
		       return $this->hasMany(SrcPersonsHasTitles::className(), ['persons_id' => 'id']); 
		   } 
		 
		   
		
		
		 /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(SrcCourses::className(), ['teacher' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades0()
    {
        return $this->hasMany(SrcGrades::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentHasCourses()
    {
        return $this->hasMany(SrcStudentHasCourses::className(), ['student' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses0()
    {
        return $this->hasMany(SrcCourses::className(), ['id' => 'course'])->viaTable('student_has_courses', ['student' => 'id']);
    }
    
      /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getDepartmentHasPeople()
		   {
		       return $this->hasMany(SrcDepartmentHasPerson::className(), ['employee' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getEmployeeInfos()
		   {
		       return $this->hasMany(SrcEmployeeInfo::className(), ['employee' => 'id']);
		   }
		   
		     /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getCities0()
		   {
		       return $this->hasOne(SrcCities::className(), ['id' => 'cities']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		public function getStudentOtherInfo0() 
		 { 
			 return $this->hasMany(SrcStudentOtherInfo::className(), ['student' => 'id']); 
		  } 
		  
	      /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentAttendances()
		   {
		       return $this->hasMany(SrcStudentAttendance::className(), ['student' => 'id']);
		   }
		   
		   
		    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentLevel()
		   {
		       return $this->hasOne(SrcStudentLevel::className(), ['student_id' => 'id']);
		   }
                   
                   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentLevelHistories()
		   {
		       return $this->hasMany(StudentLevelHistory::className(), ['student_id' => 'id']);
		   }
		   
		    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getTeacherAttendances()
		   {
		       return $this->hasMany(SrcTeacherAttendance::className(), ['teacher' => 'id']);
		   }
		   
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getUsers()
		   {
		       return $this->hasMany(UserSearch::className(), ['person_id' => 'id']);
		   }
	
			 /**
		     * @return \yii\db\ActiveQuery
		     */
		    public function getStockroomStockUsagesStockkeeper()
		    {
		        return $this->hasMany(SrcStockroomStockUsage::className(), ['stockkeeper' => 'id']);
		    }
		
		    /**
		     * @return \yii\db\ActiveQuery
		     */
		    public function getStockroomStockUsagesProductUser()
		    {
		        return $this->hasMany(SrcStockroomStockUsage::className(), ['product_user' => 'id']);
		    }
		 		
}
