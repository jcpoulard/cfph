<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "employee_info".
 *
 * @property integer $id
 * @property integer $employee
 * @property string $hire_date
 * @property string $country_of_birth
 * @property string $university_or_school
 * @property integer $number_of_year_of_study
 * @property integer $field_study
 * @property integer $qualification
 * @property integer $job_status
 * @property string $permis_enseignant
 * @property string $leaving_date
 * @property string $comments
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property JobStatus $jobStatus
 * @property FieldStudy $fieldStudy
 * @property Persons $employee0
 * @property Qualifications $qualification0
 */
class EmployeeInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee'], 'required'],
            [['employee', 'number_of_year_of_study', 'field_study', 'qualification', 'job_status'], 'integer'],
            [['hire_date', 'leaving_date', 'date_created', 'date_updated'], 'safe'],
            [['comments'], 'string'],
            [['country_of_birth', 'university_or_school', 'permis_enseignant', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['job_status'], 'exist', 'skipOnError' => true, 'targetClass' => JobStatus::className(), 'targetAttribute' => ['job_status' => 'id']],
            [['field_study'], 'exist', 'skipOnError' => true, 'targetClass' => FieldStudy::className(), 'targetAttribute' => ['field_study' => 'id']],
            [['employee'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['employee' => 'id']],
            [['qualification'], 'exist', 'skipOnError' => true, 'targetClass' => Qualifications::className(), 'targetAttribute' => ['qualification' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'employee' => Yii::t('app', 'Employee'),
            'hire_date' => Yii::t('app', 'Hire Date'),
            'country_of_birth' => Yii::t('app', 'Country Of Birth'),
            'university_or_school' => Yii::t('app', 'University Or School'),
            'number_of_year_of_study' => Yii::t('app', 'Number Of Year Of Study'),
            'field_study' => Yii::t('app', 'Field Study'),
            'qualification' => Yii::t('app', 'Qualification'),
            'job_status' => Yii::t('app', 'Job Status'),
            'permis_enseignant' => Yii::t('app', 'Permis Enseignant'),
            'leaving_date' => Yii::t('app', 'Leaving Date'),
            'comments' => Yii::t('app', 'Comments'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobStatus()
    {
        return $this->hasOne(SrcJobStatus::className(), ['id' => 'job_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFieldStudy()
    {
        return $this->hasOne(SrcFieldStudy::className(), ['id' => 'field_study']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'employee']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQualification0()
    {
        return $this->hasOne(SrcQualifications::className(), ['id' => 'qualification']);
    }
}
