<?php

namespace app\modules\fi\models;

use Yii;
use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Academicperiods;


/**
 * SrcAcademicperiods represents the model behind the search form about `app\modules\fi\models\Academicperiods`.
 */
class SrcAcademicperiods extends Academicperiods
{
  public $globalSearch;
  
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'checked', 'is_year', 'previous_academic_year', 'year'], 'integer'],
            [['name_period', 'globalSearch', 'date_start', 'date_end', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['weight'], 'number'],
            [['name_period'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcAcademicperiods::find()->orderBy(['date_end'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'weight' => $this->weight,
            'checked' => $this->checked,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'is_year' => $this->is_year,
            'previous_academic_year' => $this->previous_academic_year,
            'year' => $this->year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'name_period', $this->name_period])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcAcademicperiods::find()->orderBy(['date_end'=>SORT_DESC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->orFilterWhere(['like', 'name_period', $this->globalSearch])
            ->orFilterWhere(['like', 'is_year', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }

    
   public function getIsYear()
    {
        switch ($this->is_year)
         {
         	case 0: return Yii::t('app', 'No');
         	         break;
         	case 1: return Yii::t('app', 'Yes');
         	         break; 
         	
         }
  
     }
    
  public function searchCurrentAcademicYear()
		{      
			$query_previous_academic_year = SrcAcademicperiods::find()->select('previous_academic_year')
	                ->where(['<>','previous_academic_year',0]);
	                
			$query = (new \yii\db\Query())
			              ->select('id,name_period,weight,checked,date_end')
			              ->from('academicperiods')
			              ->where(['is_year'=>1])
			              ->andWhere(['not in', 'id', $query_previous_academic_year])
			              //->andWhere(['<', 'date_start', $currentDate])
			            //  ->andWhere(['>', 'date_end', $currentDate])
			              ->all();
				
		
	                
	        foreach( $query as $result)
         
        return $result;
        
        
	    }  
  
  
function getPreviousAcademicYear($acad)
   {   //
   	    
   	     $modelPrevious = SrcAcademicperiods::findOne($acad);  
   	           	          
    	  
         return $modelPrevious->previous_academic_year;
   	   
   	    
   	 }


  
  
  
  
  
  
    
    
}
