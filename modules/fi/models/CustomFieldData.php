<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "custom_field_data".
 *
 * @property integer $id
 * @property integer $field_link
 * @property string $field_data
 * @property integer $object_id
 */
class CustomFieldData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_field_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_link'], 'required'],
            [['field_link', 'object_id'], 'integer'],
            [['field_data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field_link' => Yii::t('app', 'Field Link'),
            'field_data' => Yii::t('app', 'Field Data'),
            'object_id' => Yii::t('app', 'Object ID'),
        ];
    }
    
    
    
 public function getCustomFieldValue($id_object,$id_field){
            $customFieldValue = null;
            
            $criteria = ['field_link' =>$id_field, 'object_id' =>$id_object];
            $data = CustomFieldData::find()->where($criteria)->all();
            
            foreach($data as $d){
                $customFieldValue = $d->field_data;
            }
            if($customFieldValue!=null){
                return $customFieldValue; 
            }else
                return '';
        }
        
        public function loadCustomFieldValue($id_object, $id_field){
            $fieldDataId = null;
            
            $criteria = ['field_link' => $id_field, 'object_id' => $id_object];
            $data = CustomFieldData::find()->where($criteria)->all();
            foreach($data as $d){
                $fieldDataId = $d->id;
            }
            
            
            $model=CustomFieldData::findOne($fieldDataId);
                if($model===null){
                    
                }
			//throw new CHttpException(404,'The requested page does not exist.');
		      return $model;
            
        }
        
        public function loadCustomFieldDataByPersonId($id_object){
             $fieldDataId = null;
            
            $criteria = ['object_id' => $id_object];
            $data = CustomFieldData::find()->where($criteria)->all();
            
            
            
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $data;
        }

	   
    
    
    
    
    
    
    
    
    
    
}
