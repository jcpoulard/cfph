<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "relations".
 *
 * @property integer $id
 * @property string $relation_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 */
class Relations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['relation_name'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['relation_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['relation_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'relation_name' => Yii::t('app', 'Relation Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
