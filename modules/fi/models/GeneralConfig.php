<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "general_config".
 *
 * @property integer $id
 * @property string $item_name
 * @property string $name
 * @property string $item_value
 * @property string $description
 * @property string $english_comment
 * @property string $category
 * @property string $date_create
 * @property string $date_update
 * @property string $create_by
 * @property string $update_by
 */
class GeneralConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'general_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'name'], 'required'],
            [['item_value', 'description', 'english_comment'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['item_name', 'name'], 'string', 'max' => 64],
            [['category'], 'string', 'max' => 12],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['item_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_name' => Yii::t('app', 'Item Name'),
            'name' => Yii::t('app', 'Name'),
            'item_value' => Yii::t('app', 'Item Value'),
            'description' => Yii::t('app', 'Description'),
            'english_comment' => Yii::t('app', 'English Comment'),
            'category' => Yii::t('app', 'Category'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
