<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\GeneralConfig;

/**
 * SrcGeneralConfig represents the model behind the search form about `app\modules\fi\models\GeneralConfig`.
 */
class SrcGeneralConfig extends GeneralConfig
{
    
    public $globalSearch;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'visibility'], 'integer'],
            [['item_name', 'name', 'item_value', 'description', 'english_comment', 'category', 'date_create', 'date_update', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcGeneralConfig::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'visibility' => $this->visibility,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'item_value', $this->item_value])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'english_comment', $this->english_comment])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    
       public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcGeneralConfig::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }
  // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'visibility' => $this->visibility,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->orFilterWhere(['like', 'item_name', $this->globalSearch])
            ->orFilterWhere(['like', 'name', $this->globalSearch]);

   

    return $dataProvider;
    }  
    
    
    
    public function newSearch($condition)
    {     
      

    $query = SrcGeneralConfig::find()->where($condition);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    
   

    return $dataProvider;
    }
 
    
    
    
    
}
