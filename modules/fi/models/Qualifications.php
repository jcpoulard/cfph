<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "qualifications".
 *
 * @property integer $id
 * @property string $qualification_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property EmployeeInfo[] $employeeInfos
 */
class Qualifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'qualifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'qualification_name'], 'required'],
            [['id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['qualification_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['qualification_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'qualification_name' => Yii::t('app', 'Qualification Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeInfos()
    {
        return $this->hasMany(SrcEmployeeInfo::className(), ['qualification' => 'id']);
    }
}
