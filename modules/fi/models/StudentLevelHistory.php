<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcPersons;

/**
 * This is the model class for table "student_level_history".
 *
 * @property integer $student_id
 * @property integer $program
 * @property integer $level
 * @property integer $room
 * @property string $comment
 * @property integer $status
 * @property integer $nbr_hold_module
 * @property integer $is_pass
 * @property integer $academic_year
 * @property string $create_by
 * @property string $date_created
 *
 * @property Academicperiods $academicYear
 * @property Persons $student
 * @property Program $program0
 */
class StudentLevelHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_level_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'level', 'room', 'comment', 'academic_year', 'create_by', 'date_created'], 'required'],
            [['student_id', 'program', 'level', 'room', 'status', 'nbr_hold_module', 'is_pass', 'academic_year'], 'integer'],
            [['date_created'], 'safe'],
            [['comment'], 'string', 'max' => 200],
            [['create_by'], 'string', 'max' => 45],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student_id' => 'id']],
            [['program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_id' => Yii::t('app', 'Student ID'),
            'program' => Yii::t('app', 'Program'),
            'level' => Yii::t('app', 'Level'),
            'room' => Yii::t('app', 'Room'),
            'comment' => Yii::t('app', 'Comment'),
            'status' => Yii::t('app', 'Status'),
            'nbr_hold_module' => Yii::t('app', 'Nbr Hold Module'),
            'is_pass' => Yii::t('app', 'Is Pass'),
            'academic_year' => Yii::t('app', 'Academic Year'),
          //  'create_by' => Yii::t('app', 'Create By'),
           // 'date_created' => Yii::t('app', 'Date Created'),
        ];
    }

    
   public function searchStudentsByGroupForReportcard_past_year($program,$level,$shift,$group,$acad) //
   {
	                
	       $query = StudentLevelHistory::find()->select('student_id')
	                ->joinWith(['student'])
	                ->where(['program'=>$program])
	                ->andWhere(['shift'=>$shift])
	                ->andWhere(['room'=>$group])
	                ->andWhere(['level'=>$level])
	                ->andWhere(['academic_year'=>$acad]);
                
      
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
	    
         
        return $dataProvider;
    } 
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(SrcProgram::className(), ['id' => 'program']);
    }
}
