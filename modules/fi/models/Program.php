<?php

namespace app\modules\fi\models;

use Yii;

use app\modules\billings\models\SrcEnrollmentIncome;
use app\modules\billings\models\SrcFees;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\SrcPostulant;
use app\modules\fi\models\SrcStudentOtherInfo;

/**
 * This is the model class for table "program".
 *
 * @property integer $id
 * @property string $label
 * @property string $short_name
 * @property integer specialisation
 * @property string $description
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Module[] $modules
 */
class Program extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label','short_name'], 'required'],
            [['is_special'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['label', 'short_name', 'description', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['label','short_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'label' => Yii::t('app', 'Label'),
            'short_name' => Yii::t('app', 'Short Name'),
            'is_special'=> Yii::t('app', 'Specialisation'),
            'description' => Yii::t('app', 'Description'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

     /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getEnrollmentIncomes() 
		   { 
		       return $this->hasMany(SrcEnrollmentIncome::className(), ['apply_level' => 'id']); 
		   } 
		 
		   /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getFees() 
		   { 
		       return $this->hasMany(SrcFees::className(), ['program' => 'id']); 
		   } 
		 
		   /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getModules()
		   {
		       return $this->hasMany(SrcModule::className(), ['program' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getPostulants()
		   {
		       return $this->hasMany(SrcPostulant::className(), ['apply_for_program' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getPostulants0()
		   {
		       return $this->hasMany(SrcPostulant::className(), ['previous_program' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentOtherInfos()
		   {
		       return $this->hasMany(SrcStudentOtherInfo::className(), ['apply_for_program' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentOtherInfos0()
		   {
		       return $this->hasMany(SrcStudentOtherInfo::className(), ['previous_program' => 'id']);
		   }
}
