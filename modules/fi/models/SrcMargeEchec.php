<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\MargeEchec;
use app\modules\fi\models\SrcMargeEchec;

/**
 * SrcMargeEchec represents the model behind the search form about `app\modules\fi\models\MargeEchec`.
 */
class SrcMargeEchec extends MargeEchec
{
    
    public $all_programs;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'program', 'academic_year', 'quantite_module', 'borne_sup'], 'integer'],
            [['program', 'academic_year'], 'unique', 'targetAttribute' => ['academic_year', 'program'],  'message' => Yii::t('app', 'This combinaison "program - academic year" has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
     public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'all_programs' =>Yii::t('app','All programs'),
			               
			               
		));
	}	


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcMargeEchec::find()->joinWith(['program0'])->orderBy('label ASC ');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'program' => $this->program,
            'academic_year' => $this->academic_year,
            'quantite_module' => $this->quantite_module,
            'borne_sup' => $this->borne_sup,
        ]);

        return $dataProvider;
    }
    
        public function searchByAcad($acad)
    {
        $query = SrcMargeEchec::find()->joinWith(['program0'])->where('academic_year='.$acad)->orderBy('label ASC ');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

     
        return $dataProvider;
    }
    
    
}
