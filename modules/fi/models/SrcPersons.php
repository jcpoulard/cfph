<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\Titles; 
use app\modules\fi\models\PersonsHasTitles;
use app\modules\fi\models\EmployeeInfo;
use app\modules\fi\models\DepartmentHasPerson;
use app\modules\fi\models\JobStatus;

//use app\modules\billings\models\ScholarshipHolder;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcScholarshipHolder;

/**
 * SrcPersons represents the model behind the search form about `app\modules\fi\models\Persons`.
 */
class SrcPersons extends Persons
{
    /**
     * @inheritdoc
     */
     
    public $globalSearch;
    public $full_name;
    public $program_label;
    public $program_short_name;
    
    
    public function rules()
    {
        return [
            [['id', 'is_student', 'cities', 'paid', 'active'], 'integer'],
             ['email', 'email'],
            [['last_name', 'full_name', 'globalSearch', 'first_name', 'gender', 'blood_group', 'birthday', 'id_number', 'adresse', 'phone', 'email', 'nif_cin', 'citizenship', 'date_created', 'date_updated', 'create_by', 'update_by', 'image', 'comment'], 'safe'],
            [['file'], 'file', 'extensions' => 'gif, jpg,jpeg,png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcPersons::find()->where(['active'=>[1,2] ])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'is_student' => $this->is_student,
            'cities' => $this->cities,
            'paid' => $this->paid,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'blood_group', $this->blood_group])
            ->andFilterWhere(['like', 'id_number', $this->id_number])
            ->andFilterWhere(['like', 'adresse', $this->adresse])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nif_cin', $this->nif_cin])
            ->andFilterWhere(['like', 'citizenship', $this->citizenship])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
    
/*    
public function searchGlobalStudent($params)
    {     

        
    $query = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);
 
    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->joinWith(['studentHasCourses','studentHasCourses.course0.room0','studentHasCourses.course0.shift0','studentOtherInfo0.applyForProgram']);  
    $query->orFilterWhere(['like', 'first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'last_name', $this->globalSearch])
            //->orFilterWhere(['like', 'gender', $this->globalSearch])
            ->orFilterWhere(['like', 'birthday', $this->globalSearch])
            //->orFilterWhere(['like', 'blood_group', $this->globalSearch])
            ->orFilterWhere(['like', 'id_number', $this->globalSearch])
            //->orFilterWhere(['like', 'adresse', $this->globalSearch])
            //->orFilterWhere(['like', 'phone', $this->globalSearch])
            //->orFilterWhere(['like', 'email', $this->globalSearch])
            ->orFilterWhere(['like', 'nif_cin', $this->globalSearch])
            //->orFilterWhere(['like', 'student_has_courses.course', $this->globalSearch])
            ->orFilterWhere(['like', 'rooms.room_name', $this->globalSearch])
            ->orFilterWhere(['like', 'shifts.shift_name', $this->globalSearch])
            ->orFilterWhere(['like', 'label', $this->globalSearch])
            ->andFilterWhere(['like', 'is_student', 1])
            ->andFilterWhere(['in', 'active', [1,2] ]);

 
   return $dataProvider;
    
    
    }
    */
public function searchStudents()
  {           
    $query = SrcPersons::find()->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC]);
    //$query->joinWith(['studentHasCourses','studentHasCourses.course0.room0','studentHasCourses.course0.shift0','studentOtherInfo0.applyForProgram']);
    $query->where('is_student= 1 and active in(1,2)');
            
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000000000000000000,
        ],
    ]);  
 
   return $dataProvider;
        
    }




public function searchGlobalTeacher($params)
    {     
        

    $query = SrcPersons::find()->innerJoinWith(['courses'])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC]);
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000000000000,
        ],
 
     ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    
    $query->orFilterWhere(['like', 'first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'last_name', $this->globalSearch])
            //->orFilterWhere(['like', 'gender', $this->globalSearch])
            //->orFilterWhere(['like', 'blood_group', $this->globalSearch])
            ->orFilterWhere(['like', 'id_number', $this->globalSearch])
            //->orFilterWhere(['like', 'adresse', $this->globalSearch])
            //->orFilterWhere(['like', 'phone', $this->globalSearch])
            //->orFilterWhere(['like', 'email', $this->globalSearch])
            ->orFilterWhere(['like', 'nif_cin', $this->globalSearch])
            //->orFilterWhere(['like', 'citizenship', $this->globalSearch])
            ->andFilterWhere(['like', 'is_student', 0])
            ->andFilterWhere(['in', 'active', [1,2] ]);

    return $dataProvider;
    }


public function searchGlobalEmployee($params)
    {     
       


    $query = SrcPersons::find()->innerJoinWith(['personsHasTitles'])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC]);
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000000000000,
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    
    $query->orFilterWhere(['like', 'first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'last_name', $this->globalSearch])
           //->orFilterWhere(['like', 'gender', $this->globalSearch])
           // ->orFilterWhere(['like', 'blood_group', $this->globalSearch])
            ->orFilterWhere(['like', 'id_number', $this->globalSearch])
           // ->orFilterWhere(['like', 'adresse', $this->globalSearch])
           // ->orFilterWhere(['like', 'phone', $this->globalSearch])
            //->orFilterWhere(['like', 'email', $this->globalSearch])
            ->orFilterWhere(['like', 'nif_cin', $this->globalSearch])
           // ->orFilterWhere(['like', 'citizenship', $this->globalSearch])
            ->andFilterWhere(['like', 'is_student', 0])
            ->andFilterWhere(['in', 'active', [1,2] ]);

    return $dataProvider;
    }


    public function searchStudentsInProgram($program)
      {
       $query = SrcPersons::find()
                ->joinWith(['studentOtherInfo0'])
                ->where(['apply_for_program'=>$program, 'active'=>[1,2] ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],
        ]);

         
        return $dataProvider;
    }
   
 
 public function searchStudentsInProgramNotInCourse($program,$course)
   {
	    if($course!='')
	     {    
		/* $query_student_in_course = StudentHasCourses::find()->select('student')
	                ->where(['course'=>$course]);
	                */
	           $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0'])
	                ->where(['subject_name'=>$subject_name]);//$query_course->module0->subject0->subject_name]);
	                
	       $query = SrcPersons::find()
	                ->joinWith(['studentOtherInfo0'])
	                ->where(['apply_for_program'=>$program])
	                ->andWhere(['not in','student',$query_student_in_module_subject])
                    ->andWhere(['in', 'active', [1,2] ]);
	
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 100000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   // else
	    //  return SrcStudentOtherInfo::model()->searchStudentsInProgram($program); 
     
    }
    
 
 public function searchStudentsInProgramNotInShiftAndCourse($program,$hift,$course)
   {
        
	 $query_student_in_course = SrcStudentHasCourses::find()->select('student')
                ->where(['course'=>$course]);
                
       $query = SrcStudentOtherInfo::find()
                ->where(['apply_for_program'=>$program])
                ->andWhere(['not in','student',$query_student_in_course]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 100000000,
        ],

        ]);
	    
         
        return $dataProvider;
    }
    
    
  public function searchEmployeeForPayroll($condition)
	{    
       if($condition!=null)
        {         
	       $query = SrcPersons::find()
	                 ->alias('p')
	                 ->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
	                ->where($condition);
	
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;
        }
      else
         return null;
	   
	
    }
    
    
   public function searchForAllEmployees()
	{    
          $query = SrcPersons::find()
	                 ->alias('p')
                         ->joinWith(['personsHasTitles'])
	                 ->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])
	                ->where('is_student=0 AND active in(1,2) AND academic_year='.$acad);
	
	        // add conditions that should always apply here
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
	            'pageSize' => 1000000,
	        ],
	
	        ]);
		    
	         
	        return $dataProvider;
        
	
    }

public function searchPersonsForShowingPayrollSetting($condition,$acad)
	{      
			$criteria = new CDbCriteria;
			
			$criteria->condition = $condition.' AND ps.academic_year='.$acad ;
			   
            $criteria->alias = 'p';
			$criteria->select = ' ps.id, ps.person_id, p.last_name , p.first_name, p.id_number, p.birthday, p.nif_cin, p.cities, p.gender, p.blood_group, ps.amount, ps.number_of_hour, ps.an_hour, ps.academic_year';
                       
			$criteria->join = 'left join payroll_settings ps on(ps.person_id=p.id) ';
                        
			
						 $criteria->order = 'last_name ASC';
			
		    		 
    return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=> 10000,
    			),
				
		'criteria'=>$criteria,
		
		
    ));
 	
    }
   
 public function searchPersonsForShowingPayroll($condition,$acad)
	{      
			$criteria = new CDbCriteria;
			
			$criteria->condition = $condition.' AND ps.academic_year='.$acad ;
			   
                        $criteria->alias = 'p';
			$criteria->select = ' p.id, p.last_name , p.first_name, p.id_number, p.birthday, p.nif_cin, p.cities, p.gender, p.blood_group, ps.amount, ps.an_hour, ps.academic_year, pl.net_salary, pl.payment_date, pl.taxe';
                       
			$criteria->join = 'left join payroll_settings ps on(ps.person_id=p.id) left join payroll pl on (pl.id_payroll_set = ps.id)';
                        
			
						 $criteria->order = 'last_name ASC';
			
		    
		    		 
    return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=> 10000,
    			),
				
		'criteria'=>$criteria,
		
		
    ));
 	
    }
    
	
public function getStudentsForBillings($fee_period_id,$program,$level)
    {
       $query_student_inbilling = SrcBillings::find()->select('student')
	                ->where(['fee_period'=>$fee_period_id])
                        ->groupBy('student');
	
	             $query = SrcPersons::find()                             
  	              ->select(['persons.id', 'persons.last_name' , 'persons.first_name', 'persons.id_number','persons.gender', 'persons.blood_group'])
  	              ->joinWith(['studentHasCourses','studentLevel','studentOtherInfo0'])
  	              ->orderBy(['persons.last_name'=>SORT_ASC])
  	              ->where(['persons.is_student'=>1])
  	              ->andWhere(['student_level.level'=>$level])
  	              ->andWhere(['student_other_info.apply_for_program'=>$program])
  	              ->andWhere(['not in','persons.id',$query_student_inbilling])
  	              ->andWhere(['in', 'active', [1,2] ]);
                     
           
  	              
  	              
  	      //1-yon fre ki nan billing balans >0 epi fee_totally_paid=0,         
  	       
  	      
  	     $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 50000,
             ],
           ]);
  	     	
  	 return $dataProvider;    

		       	
    }
  


//return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
public function isEmployeeOrTeacher($person_id, $acad)  
  { 		
  	    $return_value =-1;   
  	 
  	          //  $title=SrcPersonsHasTitles::find()->where('persons_id='.$person_id.' AND academic_year='.$acad)->all();
                    $modelPH=new SrcPersonsHasTitles;
		$title = SrcPersonsHasTitles::find()
		               ->select(['titles_id'])
		               ->where('persons_id='.$person_id.' AND academic_year='.$acad)
                        ->all();
		                      
		                     if($title!=null)
		                       { 	
		                       	 foreach($title as $title)
		                       	   { $return_value = 0;	                       	 
		                       	    
		                       	    //tchecke sil fe kou
                                     $course=new SrcCourses;
                  	                 
                                           
                                       $idCourse = SrcCourses::find()->where('old_new=1 AND teacher='.$person_id.' AND academic_year='.$acad)->all();  

								  		  
                                       if($idCourse!=null)
                                         {
                                         	 $return_value = 2;
                                         	
                                         	}
		                       	   }
			
											
		                        }
		                      else
		                       { 
		                       	    //tchecke sil fe kou
                                     $course=new Courses;
                  	                
                                    $idCourse = SrcCourses::find()->where('old_new=1 AND teacher='.$person_id.' AND academic_year='.$acad)->all(); 
                                       		
                                       	if($idCourse!=null)
	                                         {   
	                                         	$return_value = 1;
	                                         	
	                                         	}
			 
		                        }



           return $return_value;
  	
   }


public function isEmployeeTeacher($person_id, $acad)  
  { 
  	  	$title=SrcPersonsHasTitles::find()-> where('persons_id='.$person_id.' and academic_year='.$acad)->all();
		                      
		                     if($title!=null)
		                       { 
		                       	   foreach($title as $tit)
		                       	     $title_id= $tit->titles_id;	
						
									 //tchecke sil fe kou
                                     //$course=new SrcCourses;
                  	                 
                                $idCourse = SrcCourses::find()
                                               ->select(['courses.id'])
                                               ->joinWith(['academicYear'])
                                               ->where('courses.old_new=1 AND courses.teacher='.$person_id.' AND (courses.academic_year='.$acad.' OR academicperiods.year='.$acad.')')
                                               ->all();
                  	  				 

								  								  
                                       if($idCourse!=null)
                                         {

                                         	 return true;
                                         	
                                         	}
			
		                        }

           return false;
  	
   }
   
   
/*
  public function searchInactivePerson()
  {
        $criteria = new CDbCriteria;
			
        $criteria->condition = 'p.active = 0';
		
		$criteria->alias='p';
        $criteria->select = 'p.id, p.last_name, p.first_name, p.gender, p.id_number, p.blood_group, p.citizenship, p.birthday, p.adresse, p.phone, p.email, p.cities';
      
        	
      
        $criteria->order = 'last_name ASC';
			
        return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
		'criteria'=>$criteria,
        ));
}

  public function searchTotalInactivePerson()
  {
        $criteria = new CDbCriteria;
			
        $criteria->condition = 'p.active = 0';
		
		$criteria->alias='p';
        $criteria->select = 'p.id, p.last_name, p.first_name, p.gender, p.id_number, p.blood_group, p.citizenship, p.birthday, p.adresse, p.phone, p.email, p.cities';
       
        	
      
        $criteria->order = 'last_name ASC';
			
        return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=>1000000,
    			),
		'criteria'=>$criteria,
        ));
}


 public function searchStudentsToDisable($condition,$shift,$section,$level,$room,$acad)
	{    
        
			$criteria = new CDbCriteria;
			
			
			$criteria->condition = $condition.' p.is_student=1  AND h.room IN (Select r.id from rooms r left join levels l on(l.id=r.level) where r.shift=:idShift AND l.section=:idSection AND r.level=:idLevel AND r.id=:roomId) AND h.academic_year=:acad';
			$criteria->params = array(':idShift' => $shift,':idSection'=>$section,':idLevel'=>$level,':roomId'=>$room,':acad'=>$acad);
			
			$criteria->alias = 'p';
			$criteria->select = 'p.id, p.last_name , p.first_name, p.gender, p.blood_group, p.id_number';
			$criteria->join = 'left join room_has_person h on (p.id = h.students)';
			
			$criteria->order = 'last_name ASC';
           
		    
    return new CActiveDataProvider($this, array(
        'pagination'=>array(
        			'pageSize'=>10000,
    			),
				
		'criteria'=>$criteria,
    ));
          
			   
	
    }


 
 //movement
 public function searchStudentsToMove($condition,$shift,$section,$level,$room,$acad)
	{      
			$criteria = new CDbCriteria;
			
			
			if(($shift!=null)&&($section!=null)&&($level!=null)&&($room!=null))
			 { $criteria->condition = $condition.' p.is_student=1  AND h.room IN (Select r.id from rooms r left join levels l on(l.id=r.level) where r.shift=:idShift AND l.section=:idSection AND r.level=:idLevel AND r.id=:roomId) AND h.academic_year=:acad';
			   $criteria->params = array(':idShift' => $shift,':idSection'=>$section,':idLevel'=>$level,':roomId'=>$room,':acad'=>$acad);
	         }
			 elseif(($shift!=null)&&($section!=null)&&($level!=null)&&($room==null))
			 {  $criteria->condition = $condition.' p.is_student=1  AND h.room IN (Select r.id from rooms r left join levels l on(l.id=r.level) where r.shift=:idShift AND l.section=:idSection AND r.level=:idLevel) AND h.academic_year=:acad';
			    $criteria->params = array(':idShift' => $shift,':idSection'=>$section,':idLevel'=>$level,':acad'=>$acad);
			 }
			 elseif(($shift!=null)&&($section!=null)&&($level==null)&&($room==null))
			 { $criteria->condition = $condition.' p.is_student=1  AND h.room IN (Select r.id from rooms r left join levels l on(l.id=r.level) where r.shift=:idShift AND l.section=:idSection) AND h.academic_year=:acad';
			   $criteria->params = array(':idShift' => $shift,':idSection'=>$section,':acad'=>$acad);
	         }
			 elseif(($shift!=null)&&($section==null)&&($level==null)&&($room==null))
			 { $criteria->condition = $condition.' p.is_student=1  AND h.room IN (Select id from rooms where shift=:idShift) AND h.academic_year=:acad';
			   $criteria->params = array(':idShift' => $shift,':acad'=>$acad);
	         }
		
			 $criteria->alias = 'p';
			$criteria->distinct='true';
			$criteria->select = 'p.id, p.last_name , p.first_name, p.gender, p.blood_group, p.id_number';
			$criteria->join = 'left join room_has_person h on (p.id = h.students)';
			$criteria->order = 'last_name ASC';
			
		    
		    		
    return new CActiveDataProvider(get_class($this), array(
        'pagination'=>array(
        			'pageSize'=> 1000,
    			),
				
		'criteria'=>$criteria,
    ));
          
			   
	
    }
    
    
 
 //Admission
 public function searchStudentsAdmission($condition)
	{    
	$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
			
			$criteria = new CDbCriteria;
			
			
			$criteria->condition = $condition.' p.is_student=1  AND ( p.id NOT IN(SELECT students FROM level_has_person WHERE academic_year='.$acad_sess.' ) AND ( p.paid is not null) ) AND p.id IN(SELECT student FROM student_other_info) ';
			 
			$criteria->alias = 'p';
			$criteria->join = 'inner join student_other_info soi on(soi.student=p.id)';
			$criteria->select = 'p.id, p.last_name, p.first_name, p.gender,  p.blood_group, p.id_number, p.comment, p.phone, p.email, p.paid, soi.apply_for_level';
			
			$criteria->order = 'last_name ASC';
           
		    
			
    return new CActiveDataProvider($this, array(
         'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
	
		'criteria'=>$criteria,
    ));
          
    }
	
	
	
	
*/
    
        
/* Getter for person full name */
public function getFullName() {
    return $this->first_name . ' ' .strtr( strtoupper($this->last_name), capital_aksan() );
}


 public function getGenders()
    {
        switch($this->gender)
          {
          	case 0: return Yii::t('app', 'Male');
          	
          	          break;
          	case 1: return Yii::t('app', 'Female');
          	
          	          break;
          	
           }
        
        
       
    }


    
 public function getActive()
    {
        switch($this->active)
          {
          	case 0: return Yii::t('app', 'Inactive');
          	
          	          break;
          	case 1: return Yii::t('app', 'Active');
          	
          	          break;
          	case 2: return Yii::t('app', 'New');
          	
          	          break;
          	
          	
          }
        
        
       
    }
    
    /*
     * Retourne le titre du poste d'un employee
     */
    public function getTitle(){
        $acad = Yii::$app->session['currentId_academic_year'];
        $id_title = PersonsHasTitles::find()->select('titles_id')->where('persons_id='.$this->id.' AND academic_year='.$acad)->scalar(); 
        $title_name = Titles::find()->select('title_name')->where(['id'=>$id_title])->scalar();
        return $title_name;
    }
    
    /*
     * Retourn la date d'embauche d'un employee
     */
    public function getHiredate(){
       $hire_date = EmployeeInfo::find()->select('hire_date')->where(['employee'=>$this->id])->scalar();
       return $hire_date; 
    }
    
    /*
     * Retourne le statut de travail d'un employee 
     * 
     */
    public function getJobstatus(){
        $id_status = EmployeeInfo::find()->select('job_status')->where(['employee'=>$this->id])->scalar();
        $job_status_name = JobStatus::find()->select('status_name')->where(['id'=>$id_status])->scalar();
        return $job_status_name; 
    }
    
    /*
     * Retourne le departement de travail de l'employee 
     */
    public function getWorkingDepartment(){
        $id_department = DepartmentHasPerson::find()->select('department_id')->where(['employee'=>$this->id])->scalar();
        $working_department = DepartmentInSchool::find()->select('department_name')->where(['id'=>$id_department])->scalar();
        return $working_department; 
    }

     
	public function getIsScholarshipHolder($stud,$acad) //
	  {
			  $model_scholarship = SrcScholarshipHolder::find()
			                             ->where(['student'=>$stud])
			                             ->andWhere(['academic_year'=>$acad])->all();
														           	  
			  if($model_scholarship != null)
				 return 1;
			  else
				 return 0;
				   
			
                
			}
			

public function getTitles($emp,$acad)
			{
			
				$modelPH=new SrcPersonsHasTitles;
		$idTit = SrcPersonsHasTitles::find()
		               ->select(['titles_id'])
		               ->where('persons_id='.$emp.' AND academic_year='.$acad)
                        ->all();
                        
		$tit = new SrcTitles;
		$tit = '';
                if($idTit !=null){
                     foreach($idTit as $t)
                       {  $tit = $t->titles_id;
                       }
                       
                        $result= SrcTitles::find()
                                        ->select(['id','title_name'])
                                        ->where('id='.$tit)
                                        ->all();
			
                     
						   

				if($result!=null)		   
				  {  foreach($result as $r)
				        return $r->title_name;
				  }
				 else 
				   return null;
				   
                }
                else //chek if teacher
                  {   
                  	  //$course=new SrcCourses;
                  	  $idCourse = SrcCourses::find()
                  	                   ->joinWith(['academicYear'])
                                       ->where('courses.old_new=1 AND courses.teacher='.$emp.' AND (courses.academic_year='.$acad.' OR academicperiods.year='.$acad.')')
                                       ->all();
                            
                        if($idCourse!=null)
                            return Yii::t('app','Teacher');
                        else
                           return null;
                  	  
                  	}
                    
                
			}
            
			
 public function coursesByStudent($stud){
        
        // Start 
          $query = SrcStudentHasCourses::find()->joinWith(['course0','student0'])->where(['in','student',[$stud]])->andWhere(['in', 'persons.active', [1,2] ])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
           $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
                ],
            ]);

          

            return $dataProvider;
        // END 
        
        
    }        
    
    


}
