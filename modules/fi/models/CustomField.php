<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "custom_field".
 *
 * @property integer $id
 * @property string $field_name
 * @property string $field_label
 * @property string $field_type
 * @property string $value_type
 * @property string $field_option
 * @property string $field_related_to
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 */
class CustomField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_name'], 'required'],
            [['field_option'], 'string'],
            [['date_created', 'date_updated'], 'safe'],
            [['field_name'], 'string', 'max' => 64],
            [['field_label', 'field_type', 'field_related_to', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['value_type'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field_name' => Yii::t('app', 'Field Name'),
            'field_label' => Yii::t('app', 'Field Label'),
            'field_type' => Yii::t('app', 'Field Type'),
            'value_type' => Yii::t('app', 'Value Type'),
            'field_option' => Yii::t('app', 'Field Option'),
            'field_related_to' => Yii::t('app', 'Field Related To'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
    
 
    
    
}
