<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Rooms;
use app\modules\fi\models\SrcShifts;

/**
 * SrcRooms represents the model behind the search form about `app\modules\fi\models\Rooms`.
 */
class SrcRooms extends Rooms
{
  
  public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'shift' ], 'integer'],
            [['room_name', 'short_room_name', 'create_by', 'update_by'], 'string', 'max' => 225],
            [['room_name', 'globalSearch', 'short_room_name', 'shift', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['room_name'], 'unique'],
            [['shift'], 'exist', 'skipOnError' => true, 'targetClass' => SrcShifts::className(), 'targetAttribute' => ['shift' => 'id']],
 
        ];
    }



    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcRooms::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'shift' => $this->shift,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'room_name', $this->room_name])
            ->andFilterWhere(['like', 'short_room_name', $this->short_room_name])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
  public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcRooms::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],

    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

   
    $query->orFilterWhere(['like', 'room_name', $this->globalSearch])
            ->orFilterWhere(['like', 'short_room_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }

    
 public function getRoominshiftprogram($shift,$program,$acad){
       
      $data=[];
        $countRooms = SrcCourses::find()->alias('c')->select(['room'])
                         ->joinWith(['module0','room0'])
                         ->where(['shift'=>$shift,'program'=>$program,'academic_year'=>$acad])
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select(['room'])
                         ->joinWith(['module0','room0'])
                         ->where(['shift'=>$shift,'program'=>$program,'academic_year'=>$acad])
                         ->all();      
       
         if($countRooms > 0)
          {  
          	 foreach($allRooms as $room)
         	    {     
         	   	   if($room->room0->room_name!='')
         	   	     $data[ $room->room0->id ] =  $room->room0->room_name;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
    
 public function getRoominshift($shift){
       
      $data=[];
        $countRooms = SrcRooms::find()
                         ->where(['shift'=>$shift])
                         ->count();
                          
        $allRooms = SrcRooms::find()
                         ->where(['shift'=>$shift])
                         ->all();      
       
         if($countRooms > 0)
          {  
          	 foreach($allRooms as $room)
         	    {     
         	   	   if($room->room_name!='')
         	   	     $data[ $room->id ] =  $room->room_name;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
    
  
  
  
  
  
  
    
    
    
    
    
    
   
    
    
    
    
    
    
}
