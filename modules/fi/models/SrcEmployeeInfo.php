<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\EmployeeInfo;

/**
 * SrcEmployeeInfo represents the model behind the search form about `app\modules\fi\models\EmployeeInfo`.
 */
class SrcEmployeeInfo extends EmployeeInfo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employee', 'number_of_year_of_study', 'field_study', 'qualification', 'job_status'], 'integer'],
            [['hire_date', 'country_of_birth', 'university_or_school', 'permis_enseignant', 'leaving_date', 'comments', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcEmployeeInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'employee' => $this->employee,
            'hire_date' => $this->hire_date,
            'number_of_year_of_study' => $this->number_of_year_of_study,
            'field_study' => $this->field_study,
            'qualification' => $this->qualification,
            'job_status' => $this->job_status,
            'leaving_date' => $this->leaving_date,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'country_of_birth', $this->country_of_birth])
            ->andFilterWhere(['like', 'university_or_school', $this->university_or_school])
            ->andFilterWhere(['like', 'permis_enseignant', $this->permis_enseignant])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
