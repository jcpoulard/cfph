<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "persons_has_titles".
 *
 * @property integer $persons_id
 * @property integer $titles_id
 * @property integer $academic_year
 *
 * @property Persons $persons
 * @property Academicperiods $academicYear
 * @property Titles $titles
 */
class PersonsHasTitles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons_has_titles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['persons_id', 'titles_id', 'academic_year'], 'required'],
            [['persons_id', 'titles_id', 'academic_year'], 'integer'],
            [['persons_id'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['persons_id' => 'id']],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['titles_id'], 'exist', 'skipOnError' => true, 'targetClass' => Titles::className(), 'targetAttribute' => ['titles_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'persons_id' => Yii::t('app', 'Persons ID'),
            'titles_id' => Yii::t('app', 'Titles ID'),
            'academic_year' => Yii::t('app', 'Academic Year'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersons()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'persons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitles()
    {
        return $this->hasOne(SrcTitles::className(), ['id' => 'titles_id']);
    }
}
