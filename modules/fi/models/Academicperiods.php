<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "academicperiods".
 *
 * @property integer $id
 * @property string $name_period
 * @property double $weight
 * @property integer $checked
 * @property string $date_start
 * @property string $date_end
 * @property integer $is_year
 * @property integer $previous_academic_year
 * @property integer $year
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Courses[] $courses
 */
class Academicperiods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'academicperiods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_period', 'date_start', 'date_end'], 'required'],
            [['weight'], 'number'],
            [['name_period'], 'unique'],
            [['checked', 'is_year', 'previous_academic_year', 'year'], 'integer'],
            [['date_start', 'date_end', 'date_created', 'date_updated'], 'safe'],
            [['name_period', 'create_by', 'update_by'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_period' => Yii::t('app', 'Name Period'),
            'weight' => Yii::t('app', 'Weight'),
            'checked' => Yii::t('app', 'Checked'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'is_year' => Yii::t('app', 'Is Year'),
            'previous_academic_year' => Yii::t('app', 'Previous Academic Year'),
            'year' => Yii::t('app', 'Year'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(Courses::className(), ['academic_year' => 'id']);
    }
    
        
  
    
}
