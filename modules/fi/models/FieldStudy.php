<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "field_study".
 *
 * @property integer $id
 * @property string $field_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property EmployeeInfo[] $employeeInfos
 */
class FieldStudy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'field_study';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field_name'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['field_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['field_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'field_name' => Yii::t('app', 'Field Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeInfos()
    {
        return $this->hasMany(SrcEmployeeInfo::className(), ['field_study' => 'id']);
    }
}
