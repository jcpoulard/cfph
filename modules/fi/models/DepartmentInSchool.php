<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "department_in_school".
 *
 * @property integer $id
 * @property string $department_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 */
class DepartmentInSchool extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_in_school';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_name'], 'required'],
            [['date_created', 'date_updated'], 'safe'],
            [['department_name'], 'string', 'max' => 200],
            [['created_by', 'updated_by'], 'string', 'max' => 45],
            [['department_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_name' => Yii::t('app', 'Department Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
