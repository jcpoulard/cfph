<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\ContactInfo;
use app\modules\fi\models\Relations;
use app\modules\fi\models\SrcRelations;

/**
 * SrcContactInfo represents the model behind the search form about `app\modules\fi\models\ContactInfo`.
 */
class SrcContactInfo extends ContactInfo
{
   
   public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'person', 'contact_relationship', 'one_more'], 'integer'],
             ['email', 'email'],
            [['contact_name', 'globalSearch',  'profession', 'phone', 'address', 'email', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContactInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'person' => $this->person,
            'contact_relationship' => $this->contact_relationship,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'one_more' => $this->one_more,
        ]);

        $query->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
 public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = ContactInfo::find()->joinWith(['person0'])->orderBy(['last_name'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->orFilterWhere(['like', 'first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'last_name', $this->globalSearch])
             ->orFilterWhere(['like', 'contact_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
 
   public function getRelationName(){
        return SrcRelations::find()->select('relation_name')->where(['id'=>$this->contact_relationship])->scalar(); 
    }  
   
    
    
}
