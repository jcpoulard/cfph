<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "grades".
 *
 * @property integer $id
 * @property integer $student
 * @property integer $course
 * @property double $grade_value
 * @property integer $validate
 * @property integer $publish
 * @property string $comment
 * @property string $date_created
 * @property string $date_updated
 * @property string $date_validation 
 * @property string $validated_by 
 * @property string $create_by
 * @property string $update_by
 *
 * @property Persons $student0
 * @property Courses $course0
 */
class Grades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'course'], 'required'],
            [['student', 'course', 'validate', 'publish'], 'integer'],
            [['grade_value'], 'number'],
            [['date_created', 'date_updated', 'date_validation'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['validated_by'], 'string', 'max' => 100],
            [['course','student'], 'unique', 'targetAttribute' => ['course','student'],  'message' => Yii::t('app', 'This combinaison "course - student " has already been taken.')]
           // [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']],
            //[['course'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'course' => Yii::t('app', 'Course'),
            'grade_value' => Yii::t('app', 'Grade Value'),
            'validate' => Yii::t('app', 'Validate'),
            'publish' => Yii::t('app', 'Publish'),
            'comment' => Yii::t('app', 'Comment'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'date_validation' => Yii::t('app', 'Date Validation'), 
		    'validated_by' => Yii::t('app', 'Validated By'), 
		    'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse0()
    {
        return $this->hasOne(SrcCourses::className(), ['id' => 'course']);
    }
}
