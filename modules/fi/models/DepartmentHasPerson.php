<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "department_has_person".
 *
 * @property integer $id
 * @property integer $department_id
 * @property integer $employee
 * @property integer $academic_year
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property string $updated_by
 *
 * @property Academicperiods $academicYear
 * @property DepartmentInSchool $department
 * @property Persons $employee0
 */
class DepartmentHasPerson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_has_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['department_id', 'employee', 'academic_year'], 'required'],
            [['department_id', 'employee', 'academic_year'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 45],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentInSchool::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['employee'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['employee' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'employee' => Yii::t('app', 'Employee'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(DepartmentInSchool::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee0()
    {
        return $this->hasOne(Persons::className(), ['id' => 'employee']);
    }
}
