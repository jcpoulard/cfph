<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "subjects".
 *
 * @property integer $id
 * @property string $subject_name
 * @property string $short_subject_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Module[] $modules
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_name', 'short_subject_name'], 'required'],
            [['id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['subject_name', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['short_subject_name'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_name' => Yii::t('app', 'Subject Name'),
            'short_subject_name' => Yii::t('app', 'Short Subject Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModules()
    {
        return $this->hasMany(SrcModule::className(), ['subject' => 'id']);
    }
}
