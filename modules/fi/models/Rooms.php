<?php

namespace app\modules\fi\models;

use Yii;

use app\modules\fi\models\SrcShifts;

/**
 * This is the model class for table "rooms".
 *
 * @property integer $id
 * @property string $room_name
 * @property string $short_room_name
 * @property integer $shift
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Courses[] $courses
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_name', 'short_room_name','shift'], 'required'],
            [['id','shift'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['room_name', 'short_room_name', 'create_by', 'update_by'], 'string', 'max' => 225],
            [['room_name'], 'unique'],
            [['shift'], 'exist', 'skipOnError' => true, 'targetClass' => SrcShifts::className(), 'targetAttribute' => ['shift' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'room_name' => Yii::t('app', 'Room Name'),
            'short_room_name' => Yii::t('app', 'Short Room Name'),
            'shift' => Yii::t('app', 'Shift'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(SrcCourses::className(), ['room' => 'id']);
    }
    
     public function getShift0() 
		   { 
		       return $this->hasOne(Shifts::className(), ['id' => 'shift']); 
		   } 
    
     

}
