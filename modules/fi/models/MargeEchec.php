<?php

namespace app\modules\fi\models;

use Yii;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcProgram;

/**
 * This is the model class for table "marge_echec".
 *
 * @property integer $id
 * @property integer $program
 * @property integer $academic_year
 * @property integer $quantite_module
 * @property integer $borne_sup
 *
 * @property Academicperiods $academicYear
 * @property Program $program0
 */
class MargeEchec extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marge_echec';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program', 'academic_year', 'quantite_module', 'borne_sup'], 'required'],
            [['program', 'academic_year', 'quantite_module', 'borne_sup'], 'integer'],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => SrcAcademicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['program'], 'exist', 'skipOnError' => true, 'targetClass' => SrcProgram::className(), 'targetAttribute' => ['program' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'program' => Yii::t('app', 'Program'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'quantite_module' => Yii::t('app', 'Quantite Module'),
            'borne_sup' => Yii::t('app', 'Borne Sup'),
            'borne_sup' => Yii::t('app', 'Borne Sup'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(Program::className(), ['id' => 'program']);
    }
}
