<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "contact_info".
 *
 * @property integer $id
 * @property integer $person
 * @property string $contact_name
 * @property integer $contact_relationship
 * @property string $profession
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 * @property integer $one_more
 *
 * @property Relations $contactRelationship
 * @property Persons $person0
 */
class ContactInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person', 'profession'], 'required'],
            [['person', 'contact_relationship', 'one_more'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['contact_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['profession'], 'string', 'max' => 100],
            [['phone', 'email'], 'string', 'max' => 64],
            [['address'], 'string', 'max' => 255],
            [['contact_relationship'], 'exist', 'skipOnError' => true, 'targetClass' => Relations::className(), 'targetAttribute' => ['contact_relationship' => 'id']],
            [['person'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['person' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'person' => Yii::t('app', 'Person'),
            'contact_name' => Yii::t('app', 'Contact Name'),
            'contact_relationship' => Yii::t('app', 'Contact Relationship'),
            'profession' => Yii::t('app', 'Profession'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
            'one_more' => Yii::t('app', 'One More'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactRelationship()
    {
        return $this->hasOne(SrcRelations::className(), ['id' => 'contact_relationship']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'person']);
    }
}
