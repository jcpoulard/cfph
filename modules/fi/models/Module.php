<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "module".
 *
 * @property integer $id
 * @property integer $subject
 * @property integer $program
 * @property string $code
 * @property integer $duration
 * @property integer $in_use
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Courses[] $courses
 * @property Subjects $subject0
 * @property Program $program0
 */
class Module extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'module';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'program', 'code'], 'required'],
            [['subject', 'program', 'duration', 'in_use'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['code'], 'string', 'max' => 12],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['code'], 'unique'],
             [['subject', 'program'], 'unique', 'targetAttribute' => ['subject', 'program'],  'message' => Yii::t('app', 'This combinaison "subject - program" has already been taken.')]
            //[['subject'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['subject' => 'id']],
           // [['program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'program' => Yii::t('app', 'Program'),
            'code' => Yii::t('app', 'Code'),
            'duration' => Yii::t('app', 'Duration'),
            'in_use' => Yii::t('app', 'In Use'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(SrcCourses::className(), ['module' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject0()
    {
        return $this->hasOne(SrcSubjects::className(), ['id' => 'subject']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram0()
    {
        return $this->hasOne(SrcProgram::className(), ['id' => 'program']);
    }
    
    
    
    
    
    
}
