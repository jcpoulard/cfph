<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "titles".
 *
 * @property integer $id
 * @property string $title_name
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property PersonsHasTitles[] $personsHasTitles
 */
class Titles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'titles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'title_name'], 'required'],
            [['id'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['title_name', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['title_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title_name' => Yii::t('app', 'Title Name'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonsHasTitles()
    {
        return $this->hasMany(PersonsHasTitles::className(), ['titles_id' => 'id']);
    }
}
