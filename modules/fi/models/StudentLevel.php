<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "student_level".
 *
 * @property integer $student_id
 * @property integer $level
 * @property integer $room
 * @property integer $status
 *
 * @property Persons $student
 */
class StudentLevel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_id', 'level', 'room'], 'required'],
            [['student_id', 'level', 'room', 'status'], 'integer'],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'student_id' => Yii::t('app', 'Student ID'),
            'level' => Yii::t('app', 'Degree'),
            'room' => Yii::t('app', 'Room'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom0()
    {
        return $this->hasOne(SrcRooms::className(), ['id' => 'room']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student_id']);
    }
}
