<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\CustomField;

/**
 * SrcCustomField represents the model behind the search form about `app\modules\fi\models\CustomField`.
 */
class SrcCustomField extends CustomField
{
    public $globalSearch;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
       return [
            [['id'], 'integer'],
            [['field_name', 'globalSearch', 'field_label', 'field_type', 'value_type', 'field_option', 'field_related_to', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcCustomField::find()->orderBy(['field_label'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'field_name', $this->field_name])
            ->andFilterWhere(['like', 'field_label', $this->field_label])
            ->andFilterWhere(['like', 'field_type', $this->field_type])
            ->andFilterWhere(['like', 'value_type', $this->value_type])
            ->andFilterWhere(['like', 'field_option', $this->field_option])
            ->andFilterWhere(['like', 'field_related_to', $this->field_related_to])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcCustomField::find()->orderBy(['field_label'=>SORT_DESC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->orFilterWhere(['like', 'field_name', $this->globalSearch])
            ->orFilterWhere(['like', 'field_label', $this->globalSearch])
            ->orFilterWhere(['like', 'field_type', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }

      
    
       
     public function getFieldType(){
            return array(
                'txt'=>Yii::t('app','Text field'),
                'date'=>Yii::t('app','Date field'),
                'combo'=>Yii::t('app','Combo list'),
                //'checkbox'=>Yii::t('app','Checkbox'),
            );
        }
        
        public function getValueType(){
            return array(
                'int'=>Yii::t('app','Integer'),
                'float'=>Yii::t('app','Decimal value'),
                'text'=>Yii::t('app','Text value'),
            );
        }
        
        public function getPersonType(){
            return array(
                //'postu'=>Yii::t('app','Postulant'),
                'stud'=>Yii::t('app','Students'),
                'emp'=>Yii::t('app','Employees'),
                'teach'=>Yii::t('app','Teachers'),
                
            );
        }
  
   public function getPersonTypeVal(){
            switch($this->field_related_to)
            {
              //  case 'postu':
              //       return Yii::t('app','Postulant');
                case 'stud':
                    return Yii::t('app','Students');
                case 'emp':
                    return Yii::t('app','Employees');
                case 'teach':
                    return Yii::t('app','Teachers');
               
            }
        }
	  
    
    
    
    
    
    
    
}
