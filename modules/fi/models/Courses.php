<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property integer $id
 * @property integer $module
 * @property integer $teacher
 * @property integer $room
 * @property integer $academic_year
 * @property integer $shift
 * @property double $weight
 * @property integer $debase
 * @property integer $optional
 * @property integer $old_new
 * @property double $passing_grade
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Module $module0
 * @property Persons $teacher0
 * @property Rooms $room0
 * @property Academicperiods $academicYear
 * @property Shifts $shift0
 * @property Grades[] $grades
 * @property StudentHasCourses[] $studentHasCourses
 * @property Persons[] $students
 */
class Courses extends \yii\db\ActiveRecord
{
    
    public $program;
    public $shift_select;
    
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'teacher', 'academic_year', 'shift'], 'required'],//[['module', 'teacher', 'room', 'academic_year', 'shift'], 'required'],
            [['module', 'teacher',  'academic_year', 'shift', 'debase', 'optional', 'old_new'], 'integer'],//[['module', 'teacher', 'room', 'academic_year', 'shift', 'debase', 'optional', 'old_new'], 'integer'],
            [['weight', 'passing_grade'], 'number'],
            [['date_created', 'date_updated'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            
            
            [['module','teacher','shift','academic_year'], 'unique', 'targetAttribute' => ['module','teacher','shift','academic_year'],  'message' => Yii::t('app', 'This combinaison "module - shift - teacher - academic_year" has already been taken.')],
            //[['module','room','shift','academic_year'], 'unique', 'targetAttribute' => ['module','room','shift','academic_year'],  'message' => Yii::t('app', 'This combinaison "module - shift - room - academic_year" has already been taken.')]
             
            //[['module'], 'exist', 'skipOnError' => true, 'targetClass' => Module::className(), 'targetAttribute' => ['module' => 'id']],
           // [['teacher'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['teacher' => 'id']],
           // [['room'], 'exist', 'skipOnError' => true, 'targetClass' => Rooms::className(), 'targetAttribute' => ['room' => 'id']],
            //[['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
           // [['shift'], 'exist', 'skipOnError' => true, 'targetClass' => Shifts::className(), 'targetAttribute' => ['shift' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'module' => Yii::t('app', 'Module'),
            'teacher' => Yii::t('app', 'Teacher'),
            'room' => Yii::t('app', 'Room'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'shift' => Yii::t('app', 'Shift'),
            'weight' => Yii::t('app', 'Weight'),
            'debase' => Yii::t('app', 'De base'),
            'optional' => Yii::t('app', 'Ne figure pas dans le bulletin'),   // Yii::t('app', 'Optional'),
            'old_new' => Yii::t('app', 'Old New'),
            'passing_grade' => Yii::t('app', 'Passing Grade'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule0()
    {
        return $this->hasOne(SrcModule::className(), ['id' => 'module']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'teacher']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom0()
    {
        return $this->hasOne(SrcRooms::className(), ['id' => 'room']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(SrcAcademicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShift0()
    {
        return $this->hasOne(SrcShifts::className(), ['id' => 'shift']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(SrcGrades::className(), ['course' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentHasCourses()
    {
        return $this->hasMany(SrcStudentHasCourses::className(), ['course' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(SrcPersons::className(), ['id' => 'student'])->viaTable('student_has_courses', ['course' => 'id']);
    }
    
    
       
    
    
    
    
    
}
