<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\modules\fi\models\StudentOtherInfo;
use app\modules\fi\models\SrcStudentOtherInfo;


/**
 * SrcStudentOtherInfo represents the model behind the search form about `app\modules\fi\models\StudentOtherInfo`.
 */
class SrcStudentOtherInfo extends StudentOtherInfo
{
   public $globalSearch;
   
    /**
     * @inheritdoc
     */
   
   
    public function rules()
    {
        return [
            [['id', 'student', 'previous_program', 'apply_for_program', 'apply_shift'], 'integer'],
            [['school_date_entry','student', 'globalSearch', 'leaving_date', 'previous_school', 'health_state', 'father_full_name', 'mother_full_name', 'person_liable', 'person_liable_phone', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStudentOtherInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>100000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'school_date_entry' => $this->school_date_entry,
            'leaving_date' => $this->leaving_date,
            'previous_program' => $this->previous_program,
            'apply_for_program' => $this->apply_for_program,
            'apply_shift' => $this->apply_shift, 
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'previous_school', $this->previous_school])
            ->andFilterWhere(['like', 'health_state', $this->health_state])
            ->andFilterWhere(['like', 'father_full_name', $this->father_full_name])
            ->andFilterWhere(['like', 'mother_full_name', $this->mother_full_name])
            ->andFilterWhere(['like', 'person_liable', $this->person_liable])
            ->andFilterWhere(['like', 'person_liable_phone', $this->person_liable_phone])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
     public function search_()
    {
       $query = SrcStudentOtherInfo::find()
                ->where(['student'=>0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
        ]);

         
        return $dataProvider;
    }
      
 //pou le kou nan tri a   
  public function searchStudentsInProgramLevel($program,$level) //
    {
       if($level==0)
        {  $query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                ->where(['apply_for_program'=>$program]);
                
          }
       elseif($level!=0)
          { 
          	$query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                ->where(['apply_for_program'=>$program])
                ->andWhere('student in (select student_id from student_level where level='.$level.')');
          	}

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
            
        ]);

         
        return $dataProvider;
    }

//pou le tri a rete nan sal
  public function searchStudentsInProgramLevelShift($program,$level,$shift) //
    { 
      if(infoGeneralConfig('first_year_use')==1)
      {
       if($program==0)
       {
       	   $query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
       	}
      elseif($program!=0)
        {
	       if($level==0)
	        {  $query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where(['apply_for_program'=>$program]);
	                
	          }
	       elseif($level!=0)
	          { 
	          	 if($shift==0)
	              {      
	              	  $query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
					                ->where(['apply_for_program'=>$program])
					                ->andWhere('student in (select student_id from student_level where level='.$level.')');
	                }
	             elseif($shift!=0)
	               {  
			        	  $query = SrcStudentOtherInfo::find()->joinWith(['student0'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
					                ->where(['apply_for_program'=>$program])
					                ->andWhere(['apply_shift'=>$shift])
					                ->andWhere('student in (select student_id from student_level where level='.$level.')');
					      
					      
	               }
	          	}
	          	
         }
      }
    elseif(infoGeneralConfig('first_year_use')==0)
    {
        if($program==0)
       {
       	   $query = SrcStudentOtherInfo::find()->joinWith(['student0','student0.studentLevel'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['room'=>0]);
       	}
      elseif($program!=0)
        {
	       if($level==0)
	        {  $query = SrcStudentOtherInfo::find()->joinWith(['student0','student0.studentLevel'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where(['apply_for_program'=>$program]);
                        //->andWhere(['room'=>0]);
	                
	          }
	       elseif($level!=0)
	          { 
	          	 if($shift==0)
	              {      
	              	  $query = SrcStudentOtherInfo::find()->joinWith(['student0','student0.studentLevel'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
					                ->where(['apply_for_program'=>$program])
					                //->andWhere(['room'=>0])
					                ->andWhere('student in (select student_id from student_level where level='.$level.')');
	                }
	             elseif($shift!=0)
	               {  
			        	  $query = SrcStudentOtherInfo::find()->joinWith(['student0','student0.studentLevel'])->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
					                ->where(['apply_for_program'=>$program])
					                ->andWhere(['apply_shift'=>$shift])
                                                       //->andWhere(['room'=>0])
					                ->andWhere('student in (select student_id from student_level where level='.$level.')');
					      
					      
	               }
	          	}
	          	
         }
    }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
            
        ]);

         
        return $dataProvider;
    }   
 
//pou le kou nan tri a
  public function searchStudentsInProgramNotInCourse($program,$level,$course,$acad)  //le wap bay etidyan kou (swiv kou nan nenpot vakasyon)
   {
	    if($course!='')
	     {    
		
	         /*  $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0'])
	                ->where(['like','subject_name',$subject_name])
	                ->andWhere(['like','courses.academic_year',$acad]);
	                
	       $query = SrcStudentOtherInfo::find()
	                ->where(['apply_for_program'=>$program])
	                ->andWhere(['not in','student',$query_student_in_module_subject]);
	
	        // add conditions that should always apply here
	        
	        */
	        
	       $modelCourse = new SrcCourses();
	       
	           $query_course = SrcCourses::findOne($course);
	           
	           $room_course  = $query_course->room;
	           
	           
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	   /*    $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','subject_name',$subject_name])
	                // ->andWhere(['like','courses.shift',$shift])
	                ->andWhere(['like','courses.academic_year',$acad]);
                
       $query = SrcStudentOtherInfo::find()
                ->where(['apply_for_program'=>$program])
                ->andWhere(['not in','student',$query_student_in_module_subject]);
	        // add conditions that should always apply here
	
	       */

	                	               
	               //yo nan nenpot vakasyon yap swiv kou a  
	           	  $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','subject_name',$subject_name])
	                ->andWhere(['like','courses.academic_year',$acad]);
	                
	                
	            //retire elev ki gentan nan lot room(tout ki nan yon sal deja men pa sa->$room_course)
	                 $query_student_in_other_room = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                 ->where(['not in','courses.room',[$room_course] ])
	                ->andWhere(['like','courses.academic_year',$acad]);
               
                 //yap swiv kou nan lot sal deja
	              $query_student_already_in_other_room = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student',$query_student_in_other_room]);
	                
	             //tout moun ki nan nivo a
	              $query_student_already_has_level = SrcStudentLevel::find()->select('student_id')
	                ->where('level='.$level);
	         
	           
	       
	        if(infoGeneralConfig('course_room')==1)  //swiv kou nan yon sel sal
	           {
                  
			       $query = SrcStudentOtherInfo::find()
			                ->joinWith(['student0'])
			                ->where(['apply_for_program'=>$program])
			                ->andWhere(['not in','student',$query_student_in_module_subject])
			                ->andWhere(['not in','student',$query_student_already_in_other_room])
			                ->andWhere(['in','student',$query_student_already_has_level])
			                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
			                
	             }
	          elseif(infoGeneralConfig('course_room')==0)  //swiv kou nan plizye sal
	           {
	           	
			       $query = SrcStudentOtherInfo::find()
			                ->joinWith(['student0'])
			                ->where(['apply_for_program'=>$program])
			                ->andWhere(['not in','student',$query_student_in_module_subject])
			                ->andWhere(['in','student',$query_student_already_has_level])
			                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
	            
	            
	            }

	
	
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 1000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	    
     
    }
    

//pou le tri a rete nan sal
  public function searchStudentsInProgramNotInRoom($program,$level,$room,$acad)  //le wap bay etidyan kou (swiv kou nan nenpot vakasyon)
   {
	    if($room!='')
	     {    
		
	         /*  $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0'])
	                ->where(['like','subject_name',$subject_name])
	                ->andWhere(['like','courses.academic_year',$acad]);
	                
	       $query = SrcStudentOtherInfo::find()
	                ->where(['apply_for_program'=>$program])
	                ->andWhere(['not in','student',$query_student_in_module_subject]);
	
	        // add conditions that should always apply here
	        
	        */
	        
	       $modelCourse = new SrcCourses();
	       
	           $query_course = SrcCourses::find()
	                            ->joinWith(['module0'])
	                            ->select('coourses.id')
	                            ->where('room='.$room)
	                            ->andWhere('module.program='.$program)
	                            ->andWhere('academic_year='.$acad);
	           
	              	               
	               //yo nan nenpot vakasyon yap swiv kou sa  yo
	           	  $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0'])
	                ->where(['in','courses.id',$query_course ])
	                ->andWhere(['like','courses.academic_year',$acad]);
	          
                  //tout moun ki nan nivo a
	              $query_student_already_has_level = SrcStudentLevel::find()->select('student_id')
	                ->where('level='.$level);
	                
	                
       $query = SrcStudentOtherInfo::find()
                ->joinWith(['student0'])
                ->where(['apply_for_program'=>$program])
                ->andWhere(['not in','student',$query_student_in_module_subject])
                ->andWhere(['in','student',$query_student_already_has_level])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);

	
	
	
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 1000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	    
     
    }
    
//pou le kou nan tri a     
  public function searchStudentsInProgramInShiftNotInCourse($program,$level,$shift,$course,$acad) //le wap bay etidyan kou(chwa kou nan yon sel vaksyon)
   {
        
	           $modelCourse = new SrcCourses();
	          /* $query_course = SrcCourses::findOne($course);
	           
	           $room_course  = $query_course->room; 
                    *  //obsolet
                    */
	           
	       $subject_name = $modelCourse->getSubjectName($course); 
	       
	   /*    if(infoGeneralConfig('course_shift')==0)  //pa teni kont de shift
	          {   $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','subject_name',$subject_name])
	                 ->andWhere(['like','courses.shift',$shift])
	                ->andWhere(['like','courses.academic_year',$acad]);
	          }
	        elseif(infoGeneralConfig('course_shift')==1)  //tenikont de shift  
	           {  
	         */   
                        //sousi a se eske etidyan swiv kou a deja
                          /*	
	           	 //retire elev ki gentan ap swiv kou nan lot shift
	                 $query_student_in_other_shift = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                 ->where(['not in','courses.shift',[$shift] ]);
	               // ->andWhere(['like','courses.academic_year',$acad]); //ID kou yo chanje chak ane nap tenikont de non kou a
	               */ 	               
	               //yo nan lot vakasyon yap swiv kou a  
	           	  $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','subject_name',$subject_name]);
	               // ->andWhere(['like','courses.academic_year',$acad]) //ID kou yo chanje chak ane nap tenikont de non kou a
	               // ->orWhere(['in','student',$query_student_in_other_shift]);
	                
	               
	            /* //retire elev ki gentan nan lot room(tout ki nan yon sal deja men pa sa->$room_course)
	                 $query_student_in_other_room = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                 ->where(['not in','courses.room',[$room_course] ])
	                ->andWhere(['like','courses.academic_year',$acad]);
                     *  //obsolet
                     
               
                 //yap swiv kou nan lot sal deja
	              $query_student_already_in_other_room = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student',$query_student_in_other_room]);
	                 *  //obsolet
                     */
	                
	         // }
	         
	         //tout moun ki nan nivo a
	              $query_student_already_has_level = SrcStudentLevel::find()->select('student_id')
	                ->where('level='.$level);
	           
	       
            if(infoGeneralConfig('course_room')==1)  //swiv kou nan yon sel sal
	           {
	           	     $query = SrcStudentOtherInfo::find()
	                ->joinWith(['student0'])
	                ->where(['apply_for_program'=>$program])
	                ->andWhere(['apply_shift'=>$shift])
	                ->andWhere(['not in','student',$query_student_in_module_subject])
	               // ->andWhere(['not in','student',$query_student_already_in_other_room])
	                ->andWhere(['in','student',$query_student_already_has_level])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
                
	             }
	          elseif(infoGeneralConfig('course_room')==0)  //swiv kou nan plizye sal
	            {
	            	$query = SrcStudentOtherInfo::find()
	                ->joinWith(['student0'])
	                ->where(['apply_for_program'=>$program])
	                ->andWhere(['apply_shift'=>$shift])
	                ->andWhere(['not in','student',$query_student_in_module_subject])
	                ->andWhere(['in','student',$query_student_already_has_level])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
	             
	              }     
       

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    } 
 


//pou le tri a rete nan sal
  public function searchStudentsInProgramInShiftNotInRoom($program,$level,$shift,$room,$acad) //le wap bay etidyan kou(chwa kou nan yon sel vaksyon)
   {
        
	     $modelCourse = new SrcCourses();
	       
	           $query_course = SrcCourses::find()
	                            ->joinWith(['module0'])
	                            ->select('courses.id')
	                            ->where('room='.$room)
	                            ->andWhere('shift='.$shift)
	                            ->andWhere('module.program='.$program)
	                            ->andWhere('academic_year='.$acad);
	           
	             //yo nan vakasyon sa yap swiv kou sa  yo
	           	  $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0'])
	                ->where(['in','courses.id',$query_course ])
	                ->andWhere(['like','courses.academic_year',$acad]);
	          
                   //tout moun ki nan nivo a
	              $query_student_already_has_level = SrcStudentLevel::find()->select('student_id')
	                ->where('level='.$level);
	                
	                
       $query = SrcStudentOtherInfo::find()
                ->joinWith(['student0'])
                ->where(['apply_for_program'=>$program])
                ->andWhere(['not in','student',$query_student_in_module_subject])
                ->andWhere(['in','student',$query_student_already_has_level])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }






    
 public function searchStudentsInProgramInCourse($program,$course,$acad) //le wap met not
   {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	                
	                
	       $query = SrcStudentHasCourses::find()
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['like','subject_name',$subject_name])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student_has_courses.student',$query_student_in_program]);
	           
	
	 	
	      
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 10000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }
 
 
  public function searchStudentsInProgramInLevelGroupCourse($program,$level,$group,$course,$acad) //le wap met not
   {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	                
	                
	       $query = SrcStudentHasCourses::find()
	                ->joinWith(['course0.module0.subject0','course0','students0.studentLevel'])
	                ->where(['like','subject_name',$subject_name])
	                ->andWhere(['in','student_level.level',[$level] ])
	                ->andWhere(['in','student_level.room',[$group] ])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student_has_courses.student',$query_student_in_program]);
	           
	
	 	
	      
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 1000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }

  public function searchStudentsInProgramInLevelGroupCourse_delayed($program,$level,$group,$course,$acad) //le wap met not
   {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	                
	                
	       $query = SrcStudentHasCourses::find()
	                ->alias('shc')
                        ->join('INNER JOIN','courses', 'shc.course=courses.id') 
                        ->join('INNER JOIN','module', 'courses.module=module.id') 
                        ->join('INNER JOIN','subjects', 'module.subject=subjects.id') 
                          ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=shc.student') 
                        ->where(['like','subject_name',$subject_name])
	                ->andWhere(['student_level_history.level'=>$level ])
	                ->andWhere(['student_level_history.room'=>$group ])
	                ->andWhere(['courses.academic_year'=>$acad])
	                ->andWhere(['student_level_history.academic_year'=>$acad])
	                ->andWhere(['student_level_history.program'=>program]);
	           
	
	 	
	      
	        $dataProvider = new ActiveDataProvider([
	            'query' => $query,
	            'pagination' => [
            'pageSize' => 1000000,
        ],
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }

  
 public function searchStudentInCourseNotInGradeTable_withoutShift($program,$course,$acad) //le wap met not
   {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
	       
	       $query_grade_course = SrcGrades::find()
	               ->select(['student'])
	               ->where(['like','course',$course]);     
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	           
	                
	        $query = SrcStudentHasCourses::find()
	                ->joinWith(['course0.module0.subject0','course0'])
	                ->where(['course'=>$course])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student_has_courses.student',$query_student_in_program])
	                ->andWhere(['not in','student_has_courses.student',$query_grade_course]);
	           
	
	 	
	       	
	
	
	        $dataProvider = new ActiveDataProvider([
	            'pagination' => [
            'pageSize' => 1000000,
        ],
                 'query' => $query,
	            
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }    

    
 public function searchStudentInLevelGroupAndCourseNotInGradeTable_delayed($program,$level,$group,$course,$previous)
                         {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
	     
               //elev ki gen not pou kou sa deja
	       $query_grade_course = SrcGrades::find()
	               ->select(['student'])
	               ->where(['like','course',$course]);     
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	        
	                
	        $query = SrcStudentHasCourses::find()
	                ->joinWith(['course0.module0.subject0','course0','student0.studentLevelHistories'])
	                ->where(['course'=>$course])
	                ->andWhere(['student_level_history.level'=>$level])
	                ->andWhere(['student_level_history.room'=>$group])
	                ->andWhere(['courses.academic_year'=>$previous])
	                ->andWhere(['student_level_history.program'=>$program])
	                ->andWhere(['not in','student_has_courses.student',$query_grade_course]);
                
//                ->andWhere(['in','student_has_courses.student',$query_student_in_program])
//	                ->andWhere(['not in','student_has_courses.student',$query_grade_course]);
	           
	
	 	
	       	
	
	
	        $dataProvider = new ActiveDataProvider([
	            'pagination' => [
            'pageSize' => 1000000,
        ],
                 'query' => $query,
	            
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }
    

public function searchStudentInLevelGroupAndCourseNotInGradeTable($program,$level,$group,$course,$acad) //le wap met not
   {
	    if($course!='')
	     {    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
	       
	       $query_grade_course = SrcGrades::find()
	               ->select(['student'])
	               ->where(['like','course',$course]);     
	                
	       $query_student_in_program = SrcStudentOtherInfo::find()->select('student')
                     ->where(['apply_for_program'=>$program]);
	        
	                
	        $query = SrcStudentHasCourses::find()
	                ->joinWith(['course0.module0.subject0','course0','student0.studentLevel'])
	                ->where(['course'=>$course])
	                ->where(['student_level.level'=>$level])
	                ->where(['student_level.room'=>$group])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['in','student_has_courses.student',$query_student_in_program])
	                ->andWhere(['not in','student_has_courses.student',$query_grade_course]);
	           
	
	 	
	       	
	
	
	        $dataProvider = new ActiveDataProvider([
	            'pagination' => [
            'pageSize' => 1000000,
        ],
                 'query' => $query,
	            
	        ]);
		    
	         
	        return $dataProvider;
	     }
	   
     
    }
    
    

public function searchStudentNotInGradeTable_forTeacher($group,$course,$acad) //le wap met not
   {
	   // if( ($course!='')&&($group!='')  )
	    // { 
	    if($course=='')
	   	  $course = 0;
	   	  
	   	if($group=='')
	   	    $group = 0;	
	    	   
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
	       
	       $query_grade_course = SrcGrades::find()
	               ->distinct(true)
	               ->select(['student'])
	               ->where(['like','course',$course]);     
	                
	                
	     $query = SrcStudentHasCourses::find()
	                ->distinct(true)
	                ->select(['student'])
	                ->joinWith(['course0','student0.studentLevel'])
	                ->where(['course'=>$course])
	                ->andWhere(['student_level.room'=>$group])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['not in','student_has_courses.student',$query_grade_course])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
	         
	
	
	        $dataProvider = new ActiveDataProvider([
	            'pagination' => [
            'pageSize' => 1000000,
        ],
                 'query' => $query,
	            
	        ]);
		    
	         
	        return $dataProvider;
	   //  }
	   
     
    }


public function searchStudentsForTeacher($group,$course,$acad) //le wap met not    
 {
	  //  if( ($course!='')&&($group!='')  )
	   //  {
	   	if($course=='')
	   	  $course = 0;
	   	  
	   	if($group=='')
	   	    $group = 0;
	   	    
			 $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
	       
	                
	     $query = SrcStudentHasCourses::find()
	                ->distinct(true)
	                ->select(['student'])
	                ->joinWith(['course0','student0.studentLevel'])
	                ->where(['course'=>$course])
	                ->andWhere(['student_level.room'=>$group])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);
	         
	
	
	        $dataProvider = new ActiveDataProvider([
	            'pagination' => [
            'pageSize' => 1000000,
        ],
                 'query' => $query,
	            
	        ]);
		    
	         
	        return $dataProvider;
	    // }
	   
     
    }
    
    
           
public function searchStudentsInProgramInShiftAndCourse($program,$shift,$course,$acad)  //le wap met not
   {
                
                //retire elev ki gentan nan lot shift
	                 $query = SrcStudentHasCourses::find()->select('student')
	                ->distinct(TRUE)
	                ->joinWith(['course0.module0.subject0','course0','student0'])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                 ->where(['in','courses.shift',[$shift] ])
	                ->andWhere(['in','course',[$course] ])
	                ->andWhere(['in','module.program',[$program] ])
	                ->andWhere(['like','courses.academic_year',$acad]);
	                 
	          
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }

public function searchStudentsInProgramInShiftLevelGroupAndCourse($program,$shift,$level,$group,$course,$acad)  //le wap met not
   {
	                 $query = SrcStudentHasCourses::find()->select('student')
	                ->distinct(TRUE)
	                ->joinWith(['course0.module0.subject0','course0','student0.studentLevel'])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                 ->where(['in','courses.shift',[$shift] ])
	                 ->andWhere(['in','course',[$course] ])
	                 ->andWhere(['in','module.program',[$program] ])
	                 ->andWhere(['in','student_level.level',[$level] ])
	                 ->andWhere(['in','student_level.room',[$group] ])
	                ->andWhere(['like','courses.academic_year',$acad]);
	                
               
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }     

public function searchStudentsInProgramInShiftLevelGroupAndCourse_delayed($program,$shift,$level,$group,$course,$acad)  //le wap met not
   {
        
	$modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course); 
	         
                //retire elev ki gentan nan lot shift
	                 $query = SrcStudentHasCourses::find()->select('shc.student')
	                ->alias('shc')
                        ->distinct(TRUE)
	                ->join('INNER JOIN','persons', 'shc.student=persons.id') 
                         ->join('INNER JOIN','courses', 'shc.course=courses.id') 
                        ->join('INNER JOIN','grades', 'grades.course=shc.course AND grades.student=shc.student') 
                        //->join('INNER JOIN','module', 'courses.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                          ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=shc.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                 ->where('grade_value IS NULL')
	                 ->andWhere(['courses.shift'=>$shift ])
	                 ->andWhere(['shc.course'=>$course ])
	                ->andWhere(['student_level_history.program'=>$program ])
	                ->andWhere(['student_level_history.level'=>$level ])
	                 ->andWhere(['student_level_history.room'=>$group ])
	                ->andWhere(['student_level_history.academic_year'=>$acad ])
	                ->andWhere(['courses.academic_year'=>$acad]);
	            
              /*  $query = SrcStudentOtherInfo::find()
                ->joinWith(['student0'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                ->where(['apply_for_program'=>$program])
                ->andWhere(['in','student',$query_student_in_module_subject]);
                */
         

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    } 
    
    
public function searchStudentInCourseNotInGradeTable($program,$shift,$course,$acad)  //le wap met not
   { 
	       $query_grade_course = SrcGrades::find()
	               ->select(['student'])
	               ->where(['like','course',$course]);         
   
	            $query = SrcStudentHasCourses::find()->select('student')
	                ->distinct(TRUE)
	                ->joinWith(['course0.module0.subject0','course0','student0'])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                        ->where(['course'=>$course])
	                ->andWhere(['shift'=>$shift ])
	                ->andWhere(['module.program'=>$program ])
	                ->andWhere(['like','courses.academic_year',$acad])
	               ->andWhere(['not in','student',$query_grade_course]);
             
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }

public function searchStudentInShiftLevelGroupAndCourseNotInGradeTable($program,$shift,$level,$group,$course,$acad)  //le wap met not
   {
               $query_grade_course = SrcGrades::find()
	               ->select(['student'])
	               ->where(['like','course',$course]);         
 
                   $query = SrcStudentHasCourses::find()->select('student')
	                ->distinct(TRUE)
	                ->joinWith(['course0.module0.subject0','course0','student0.studentLevel'])
	                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where(['course'=>$course])
	                ->andWhere(['shift'=>$shift ])
	                 ->andWhere(['level'=>$level ])
	                 ->andWhere(['module.program'=>$program ])
	                 ->andWhere(['student_level.room'=>$group ])
	                ->andWhere(['like','courses.academic_year',$acad])
	                ->andWhere(['not in','student',$query_grade_course]);
                

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }

    
    public function searchStudentInShiftLevelGroupAndCourseNotInGradeTable_delayed($program,$shift,$level,$group,$course,$acad)  //le wap met not
   {                                                                                                                            //pou kou ki pa nan tab grades lan ki ke yo poko met not
	     /*  $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
              */ 
               $query_grade_course = SrcGrades::find()
	               ->joinWith(['course0.module0.subject0','course0'])
                       ->select(['student'])
	               ->where(['course'=>$course]);
                      // ->orWhere(['subjects.subject_name'=>$subject_name]);         
              
               $query = SrcStudentHasCourses::find()->select('shc.student')
	                ->distinct(TRUE)
	                 ->alias('shc')
                        ->join('INNER JOIN','persons', 'shc.student=persons.id') 
                        ->join('INNER JOIN','courses', 'shc.course=courses.id') 
                        //->join('INNER JOIN','module', 'courses.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                          ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=shc.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where(['courses.shift'=>$shift])
                        ->andWhere(['shc.course'=>$course])
                        ->andWhere(['student_level_history.program'=>$program])
                        ->andWhere(['student_level_history.level'=>$level])
                        ->andWhere(['student_level_history.room'=>$group])
                        ->andWhere(['student_level_history.academic_year'=>$acad])
                        ->andWhere(['courses.academic_year'=>$acad])
                        ->andWhere(['NOT IN','shc.student',$query_grade_course]);
             
                
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }
 public function searchStudentInShiftLevelGroupAndCourseInGradeTable_delayedPS($program,$shift,$level,$group,$course,$acad)  //le wap met not
   {                                                                                                                          //pou kou ki nan tab grades lan ki gen kek student ki pa gen not
	      $modelCourse = new SrcCourses();
	           //$query_course = Courses::findOne($course);
	       $subject_name = $modelCourse->getSubjectName($course);
               
               $query_grade_course = SrcGrades::find()
	               ->joinWith(['course0.module0.subject0','course0','student0'])
                       ->select(['student'])
	               ->where(['course'=>$course]);
                       //->orWhere(['subject_name'=>$subject_name]);        
                  
	         if($query_grade_course!=NULL)
                    {
	              $query = SrcStudentHasCourses::find()->select('shc.student')
                        ->distinct(TRUE)
	                 ->alias('shc')
                        ->join('INNER JOIN','grades', 'shc.student=grades.student AND shc.course=grades.course') 
                        ->join('INNER JOIN','persons', 'shc.student=persons.id') 
                        ->join('INNER JOIN','courses', 'shc.course=courses.id') 
                        //->join('INNER JOIN','module', 'courses.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                          ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=shc.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where('grade_value IS NULL')
                        ->andWhere(['shc.course'=>$course])
                        ->andWhere(['courses.shift'=>$shift])
                        ->andWhere(['student_level_history.program'=>$program])
                        ->andWhere(['student_level_history.level'=>$level])
                        ->andWhere(['student_level_history.room'=>$group])
                        ->andWhere(['student_level_history.academic_year'=>$acad])
                        ->andWhere(['courses.academic_year'=>$acad]);
                    }
                   else
                       {
	              $query = SrcStudentHasCourses::find()->select('shc.student')
                        ->distinct(TRUE)
	                 ->alias('shc')
                        ->join('INNER JOIN','persons', 'shc.student=persons.id') 
                        ->join('INNER JOIN','courses', 'shc.course=courses.id') 
                        //->join('INNER JOIN','module', 'courses.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                          ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=shc.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
	                ->where(['courses.shift'=>$shift])
                        ->andWhere(['shc.course'=>$course])
                        ->andWhere(['student_level_history.program'=>$program])
                        ->andWhere(['student_level_history.level'=>$level])
                        ->andWhere(['student_level_history.room'=>$group])
                        ->andWhere(['student_level_history.academic_year'=>$acad])
                        ->andWhere(['courses.academic_year'=>$acad]);
                    }
                    
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    }




 
 public function searchStudentsForReportcard($program,$level,$shift,$room,$acad) //
   {
	                
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0','student0.studentLevel'])
	                ->where(['shift'=>$shift])
	                ->andWhere(['room'=>$room])
	                ->andWhere(['student_level.level'=>$level])
	                ->andWhere(['academic_year'=>$acad]);
                
       $query = SrcStudentOtherInfo::find()
                ->where(['apply_for_program'=>$program])
                ->andWhere(['in','student',$query_student_in_module_subject]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],
        ]);
	    
         
        return $dataProvider;
    } 
    
  public function searchStudentsByGroupForReportcard($program,$level,$shift,$group,$acad) //
   {
	$current_acad = Yii::$app->session['currentId_academic_year'];
        
        if($current_acad==$acad){
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0','student0.studentLevel'])
	                ->where(['shift'=>$shift])
	                ->andWhere(['student_level.room'=>$group])
	                ->andWhere(['student_level.level'=>$level])
	                ->andWhere(['academic_year'=>$acad]);
                
       $query = SrcStudentOtherInfo::find()
                ->where(['apply_for_program'=>$program])
                ->andWhere(['in','student',$query_student_in_module_subject]);

        // add conditions that should always apply here
        }
     elseif($current_acad!=$acad)
     { 
         $query = StudentLevelHistory::find()->select('student_id')
	                ->joinWith(['student'])
	                ->where(['program'=>$program])
	                ->andWhere(['shift'=>$shift])
	                ->andWhere(['room'=>$group])
	                ->andWhere(['level'=>$level])
	                ->andWhere(['academic_year'=>$acad]);
     }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
	    
         
        return $dataProvider;
    } 


    
  public function searchStudentsByProgramLevelShiftGroupAcad($program,$level,$shift,$group,$acad) //
   {
	                
	       $query_student_in_module_subject = SrcStudentHasCourses::find()->select('student')
	                ->joinWith(['course0','student0.studentLevel'])
	                ->where(['shift'=>$shift])
	                ->andWhere(['student_level.room'=>$group])
	                ->andWhere(['student_level.level'=>$level])
	                ->andWhere(['academic_year'=>$acad]);
                
       $query = SrcStudentOtherInfo::find()
                ->where(['apply_for_program'=>$program])
                ->andWhere(['in','student',$query_student_in_module_subject])
                ->all();
         
        return $query;
    } 
    
    
  public function getOtherInfo($student)
    {
       
     $query = SrcStudentOtherInfo::find()->joinWith(['applyForProgram','applyShift'])
                ->where(['student'=>$student]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]); 

         
        return $dataProvider->getModels();
    }
      
    public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcStudentOtherInfo::find()->orderBy(['school_date_entry'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

    $query->orFilterWhere(['like', 'apply_for_program', $this->globalSearch])
           ->orFilterWhere(['like', 'previous_program', $this->globalSearch])
           ->orFilterWhere(['like', 'previous_school', $this->globalSearch]);
           
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    } 
    
    
    
    
    
    
    
    
    
    
    
    
         
    
}
