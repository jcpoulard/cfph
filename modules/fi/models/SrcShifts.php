<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Shifts;

/**
 * SrcShifts represents the model behind the search form about `app\modules\fi\models\Shifts`.
 */
class SrcShifts extends Shifts
{
   public $globalSearch;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['shift_name', 'globalSearch', 'time_start', 'time_end', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['shift_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcShifts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'shift_name', $this->shift_name])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
          public function searchGlobal($params){     
       
            $query = SrcShifts::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],

            ]);

            $this->load($params);

            if (!$this->validate()) {
                // uncomment the following line if you do not want to any records when validation fails
                // $query->where('0=1');
                return $dataProvider;
            }


            $query->orFilterWhere(['like', 'shift_name', $this->globalSearch]);
                    //->andFilterWhere(['like', '', x]);

            return $dataProvider;
    }

 
     
public function getShiftinprogram($program,$acad){
       
      $data=[];
        $countShifts = SrcCourses::find()->alias('c')->select(['shift'])
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad])
                         ->count();
                          
        $allShifts = SrcCourses::find()->alias('c')->select(['shift'])
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad])
                         ->all();      
       
         if($countShifts > 0)
          {  
          	 foreach($allShifts as $shift)
         	    {     
         	   	   if($shift->shift0->shift_name!='')
         	   	     $data[ $shift->shift0->id ] =  $shift->shift0->shift_name ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
    

public function sortByShiftinprogram($program,$acad){
       
      $data[]=Yii::t('app', ' --  select shift  --');
        $countShifts = SrcCourses::find()->alias('c')->select(['shift'])
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad])
                         ->count();
                          
        $allShifts = SrcCourses::find()->alias('c')->select(['shift'])
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad])
                         ->all();      
       
         if($countShifts > 0)
          {  
          	 foreach($allShifts as $shift)
         	    {     
         	   	   if($shift->shift0->shift_name!='')
         	   	     $data[ $shift->shift0->id ] =  $shift->shift0->shift_name ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }

   
 
    
    
}
