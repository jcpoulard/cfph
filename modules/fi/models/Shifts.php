<?php

namespace app\modules\fi\models;

use Yii;
use app\modules\fi\models\SrcStudentAttendance;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcTeacherAttendance;

/**
 * This is the model class for table "shifts".
 *
 * @property integer $id
 * @property string $shift_name
 * @property string $time_start
 * @property string $time_end
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Courses[] $courses
 * @property Rooms[] $rooms
 * @property StudentAttendance[] $studentAttendances 
 * @property StudentOtherInfo[] $studentOtherInfos 
 * @property TeacherAttendance[] $teacherAttendances 
 */
class Shifts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shifts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shift_name'], 'required'],
            [['time_start', 'time_end', 'date_created', 'date_updated'], 'safe'],
            [['shift_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['shift_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shift_name' => Yii::t('app', 'Shift Name'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(SrcCourses::className(), ['shift' => 'id']);
    }
    
    
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(SrcRooms::className(), ['shift' => 'id']);
    }
    
    
       public function getStudentAttendances()
		   {
		       return $this->hasMany(SrcStudentAttendance::className(), ['shift' => 'id']);
		   }
		   
		   
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStudentOtherInfos()
		   {
		       return $this->hasMany(SrcStudentOtherInfo::className(), ['apply_shift' => 'id']);
		   }
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getTeacherAttendances()
		   {
		       return $this->hasMany(SrcTeacherAttendance::className(), ['shift' => 'id']);
		   }
    
}
