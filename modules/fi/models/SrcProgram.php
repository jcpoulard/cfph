<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\SrcProgram;

/**
 * SrcProgram represents the model behind the search form about `app\modules\fi\models\Program`.
 */
class SrcProgram extends Program
{
    
    public $globalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','is_special'], 'integer'],
            [['label', 'short_name','is_special', 'description','globalSearch', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['label', 'short_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcProgram::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
           ->andFilterWhere(['like', 'is_special', $this->is_special])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
     public function searchGlobal($params){     
       
            $query = SrcProgram::find();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],

            ]);

            $this->load($params);

            if (!$this->validate()) {
                // uncomment the following line if you do not want to any records when validation fails
                // $query->where('0=1');
                return $dataProvider;
            }


            $query->orFilterWhere(['like', 'label', $this->globalSearch]);
            $query->orFilterWhere(['like', 'short_name', $this->globalSearch]);
            $query->orFilterWhere(['like', 'is_special', $this->globalSearch]);
            $query->orFilterWhere(['like', 'description', $this->globalSearch]);
                    //->andFilterWhere(['like', '', x]);

            return $dataProvider;
    }

public function sortByProgram(){
       
      $data[]=Yii::t('app', ' --  select program  --');
        $allPrograms = SrcProgram::find()->all();      
        
          	 foreach($allPrograms as $program)
         	    {     
         	   	   if($program->label!='')
         	   	     $data[ $program->id ] =  $program->label ;	            	   	    
         	   	 }
           
         
         return $data;
          
    }

   
public function getIsSpecialisation()
{
    if($this->is_special==0)
        return Yii::t('app', 'No');
    elseif($this->is_special==1)
        return Yii::t('app', 'Yes');
}
   
   
   
   
   
   


    
}
