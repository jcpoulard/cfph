<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "persons_has_titles".
 *
 * @property integer $persons_id
 * @property integer $titles_id
 * @property integer $academic_year
 *
 * @property Persons $persons
 * @property Academicperiods $academicYear
 * @property Titles $titles
 */
class SrcPersonsHasTitles  extends PersonsHasTitles
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons_has_titles';
    }

   

    
    }
