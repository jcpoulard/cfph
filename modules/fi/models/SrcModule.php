<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Module;

/**
 * SrcModule represents the model behind the search form about `app\modules\fi\models\Module`.
 */
class SrcModule extends Module
{
   public $globalSearch;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'program', 'code'], 'required'],
            [['id', 'subject', 'program', 'duration', 'in_use'], 'integer'],
            [['code', 'globalSearch', 'date_created', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['code'], 'unique'],
             [['subject', 'program'], 'unique', 'targetAttribute' => ['subject', 'program'],  'message' => Yii::t('app', 'This combinaison "subject - program" has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcModule::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subject' => $this->subject,
            'program' => $this->program,
            'duration' => $this->duration,
            'in_use' => $this->in_use,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
 public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;


    $query = SrcModule::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 1000000000000,
        ],

    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }
     
      $query->joinWith(['subject0','program0']);
    $query->orFilterWhere(['like', 'code', $this->globalSearch])
            ->orFilterWhere(['like', 'Subjects.subject_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Subjects.short_subject_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Program.label', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }

    
  public function searchByProgram($program)
    {     
        //$this->globalSearch = $params;


    $query = SrcModule::find()
                    ->where(['program'=>$program]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 1000000000000,
        ],

    ]);

          return $dataProvider;
    }
 
  
    
    
    
    
}
