<?php

namespace app\modules\fi\models;

use Yii;

use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;

/**
 * This is the model class for table "student_other_info".
 *
 * @property integer $id
 * @property integer $student
 * @property string $school_date_entry
 * @property string $leaving_date
 * @property string $previous_school
 * @property integer $previous_program
 * @property integer $apply_for_program
 * @property integer $apply_shift 
 * @property string $health_state
 * @property string $father_full_name
 * @property string $mother_full_name
 * @property string $person_liable
 * @property string $person_liable_phone
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Persons $student0 
 * @property Shifts $applyShift
 * @property Program $applyForProgram
 * @property Program $previousProgram
 */
class StudentOtherInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_other_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'apply_for_program', 'apply_shift'], 'required'],
            [['id', 'student', 'previous_program', 'apply_for_program', 'apply_shift'], 'integer'],
            [['school_date_entry', 'leaving_date', 'date_created', 'date_updated'], 'safe'],
            [['previous_school', 'health_state'], 'string', 'max' => 255],
            [['father_full_name', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['mother_full_name', 'person_liable'], 'string', 'max' => 100],
            [['person_liable_phone'], 'string', 'max' => 65],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']], 
		    [['apply_for_program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['apply_for_program' => 'id']],
		    [['apply_shift'], 'exist', 'skipOnError' => true, 'targetClass' => Shifts::className(), 'targetAttribute' => ['apply_shift' => 'id']],
            [['previous_program'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['previous_program' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'school_date_entry' => Yii::t('app', 'School Date Entry'),
            'leaving_date' => Yii::t('app', 'Leaving Date'),
            'previous_school' => Yii::t('app', 'Previous School'),
            'previous_program' => Yii::t('app', 'Previous Program'),
            'apply_for_program' => Yii::t('app', 'Apply For Program'),
            'apply_shift' => Yii::t('app', 'Apply Shift'),
            'health_state' => Yii::t('app', 'Health State'),
            'father_full_name' => Yii::t('app', 'Father Full Name'),
            'mother_full_name' => Yii::t('app', 'Mother Full Name'),
            'person_liable' => Yii::t('app', 'Person Liable'),
            'person_liable_phone' => Yii::t('app', 'Person Liable Phone'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

  
   /**
    * @return \yii\db\ActiveQuery
		    */
		   public function getApplyShift()
		   {
		       return $this->hasOne(SrcShifts::className(), ['id' => 'apply_shift']);
		   }
		   
     /**
		    * @return \yii\db\ActiveQuery
		    */
	public function getStudent0() 
	 { 
		 return $this->hasOne(SrcPersons::className(), ['id' => 'student']); 
	  } 
		   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplyForProgram()
    {
        return $this->hasOne(SrcProgram::className(), ['id' => 'apply_for_program']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousProgram()
    {
        return $this->hasOne(SrcProgram::className(), ['id' => 'previous_program']);
    }
}
