<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "student_has_courses".
 *
  * @property integer $id
 * @property integer $student
 * @property integer $course
 * @property integer $is_pass
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Persons $student0
 * @property Courses $course0
 */
class StudentHasCourses extends \yii\db\ActiveRecord
{
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_has_courses';
    }
    
    
    
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'course'], 'required'],
            [['id','student', 'course', 'is_pass'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            //[['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']],
            //[['course'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course' => 'id']],
            [['course','student'], 'unique', 'targetAttribute' => ['course','student'],  'message' => Yii::t('app', 'This combinaison "course - student " has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'course' => Yii::t('app', 'Course'),
            'is_pass' => Yii::t('app', 'Is Pass'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse0()
    {
        return $this->hasOne(SrcCourses::className(), ['id' => 'course']);
    }
    
    
 
    
    
    
    
}
