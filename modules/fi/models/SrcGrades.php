<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Grades;
 
/**
 * SrcGrades represents the model behind the search form about `app\modules\fi\models\Grades`.
 */
class SrcGrades extends Grades
{
   public $globalSearch;
   public $program; 
   public $shift;
   public $room;
   public $level; 
   public $academic_year;
   public $previous_level_module;
   public $is_particular_case;
   public $average;  
   
   public $transcriptItems;
   public $transcript_note_text; 
   public $transcript_signature; 
   public $administration_signature_text; 
   public $header_text_date;
   public $transcript_footer_text;
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'course', 'validate', 'publish','is_particular_case'], 'integer'],
            [['grade_value'], 'number'],
            [['transcript_note_text'], 'string', 'max' => 255],
            [['transcript_footer_text','header_text_date','administration_signature_text','transcript_signature','transcript_note_text','comment', 'globalSearch', 'date_created', 'date_updated', 'date_validation', 'validated_by', 'create_by', 'update_by'], 'safe'],
            [['course','student'], 'unique', 'targetAttribute' => ['course','student'],  'message' => Yii::t('app', 'This combinaison "course - student " has already been taken.')]
        ];
    }
    
     public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'program' =>Yii::t('app','Program'),
			               'shift' =>Yii::t('app','Shift'),
			               'room' =>Yii::t('app','Room'),
			               'level' =>Yii::t('app','Level'),
                                       'is_particular_case'=> Yii::t('app', 'Particular case'),
                                       'previous_level_module' =>Yii::t('app','Include modules of previous level'),
			               'academic_year' =>Yii::t('app','Academic Year'),
			               'average' =>Yii::t('app','Average'),
                                       'transcript_note_text'=>Yii::t('app','transcript note text'),
					'header_text_date'=>Yii::t('app','Header text date'),
					'transcript_footer_text'=>Yii::t('app','Footer text'),
					'transcript_signature'=>Yii::t('app','Signature'),
					'administration_signature_text'=>Yii::t('app','Administration Signature Text')
		));
	}		


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

  
  


	
public function checkGradeDataByCourse($course)
  {
	   $result = SrcGrades::find()->joinWith(['student0'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['course'=>$course])->all();
			   
			 return $result;
  }


	
public function checkGradeDataByCourseGroup($course,$group)
  {
	   $result = SrcGrades::find()->joinWith(['student0.studentLevel'])
	            ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['course'=>$course,'student_level.room'=>$group])->all();
			   
			 return $result;
  }
  
  
  public function checkGradeDataByCourseGroup_delayed($course,$group)
  {
	   $result = SrcGrades::find()
	               ->alias('g')
                        ->join('INNER JOIN','persons', 'g.student=persons.id') 
                        ->join('INNER JOIN','student_has_courses', 'g.course=student_has_courses.course') 
                        ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['g.course'=>$course,'student_level_history.room'=>$group])->all();
			   
			 return $result;
  }

	
public function checkGradeDataByStudentCourse($student, $course)
 {
	       $result = SrcGrades::find()->where(['student'=>$student, 'course'=>$course])->all();
	        		
			   return $result;
  }

  
  
public function searchStudentsGradeForCourse($course)
  {
	      $query = SrcGrades::find()->joinWith(['student0'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['course'=>$course]);
			   
			 $dataProvider = new ActiveDataProvider([
                             'pagination' => [
                                   'pageSize' => 1000000,
                                ],
                             'query' => $query,
                             
                                  ]);

         
        return $dataProvider;
  }
 
 
public function searchStudentsGradeForCourseByGroup_delayed($course,$group,$shift,$acad) 
  {
	      $query = SrcGrades::find()
                        ->alias('g')
                        ->join('INNER JOIN','courses', 'g.course=courses.id') 
                        ->join('INNER JOIN','persons', 'g.student=persons.id') 
                        ->join('INNER JOIN','student_has_courses', 'g.course=student_has_courses.course AND g.student=student_has_courses.student') 
                        ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=g.student') 
                        ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                      ->where('grade_value IS NULL')
                      ->andWhere(['g.course'=>$course,'student_level_history.room'=>$group,'courses.shift'=>$shift,'courses.academic_year'=>$acad,'student_level_history.academic_year'=>$acad]);
			   
			 $dataProvider = new ActiveDataProvider([
                             'pagination' => [
                                   'pageSize' => 1000000,
                                ],
                             'query' => $query,
                             
                                  ]);

         
        return $dataProvider;
  }
  
  
  
public function searchStudentsGradeForCourseByGroup($course,$group,$shift,$acad)
  {
	      $query = SrcGrades::find()->joinWith(['student0.studentLevel','course0'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['course'=>$course,'student_level.room'=>$group,'shift'=>$shift,'academic_year'=>$acad]);
			   
			 $dataProvider = new ActiveDataProvider([
                             'pagination' => [
                                   'pageSize' => 1000000,
                                ],
                             'query' => $query,
                             
                                  ]);

         
        return $dataProvider;
  }

 
public function searchGradeById($id)
  {
	      $query = SrcGrades::find()->where(['id'=>$id]);
			   
			 $dataProvider = new ActiveDataProvider([
                                                'query' => $query,
                                  ]);

         
        return $dataProvider;
  }
  
  
public function searchByCourse($course)
  {
	      $query = SrcGrades::find()->joinWith(['student0'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['course'=>$course]);
			   
			 $dataProvider = new ActiveDataProvider([
                                                'query' => $query,
                                                'pagination' => [
            'pageSize' => 10000000,
        ],
                                  ]);

         
        return $dataProvider;
  }

public function searchByShiftLevelGroupCourse($shift,$level,$group,$course)
  {
	      $query = SrcGrades::find()->joinWith(['course0','student0.studentLevel'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['courses.shift'=>$shift,'student_level.level'=>$level,'student_level.room'=>$group,'course'=>$course]);
			   
			 $dataProvider = new ActiveDataProvider([
                                                'query' => $query,
                                                'pagination' => [
            'pageSize' => 10000000,
        ],
                                  ]);

         
        return $dataProvider;
  }


public function searchByGroupCourse($group,$course)
  {
	      $query = SrcGrades::find()->joinWith(['course0','student0.studentLevel'])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])->where(['student_level.room'=>$group,'course'=>$course]);
			   
			 $dataProvider = new ActiveDataProvider([
                                                'query' => $query,
                                                'pagination' => [
            'pageSize' => 10000000,
        ],
                                  ]);

         
        return $dataProvider;
  }


//1: tout not valide,  0: gen not ki pa valide	
 public function if_all_grades_validated($student,$acad)
  {
  	  
     //$acad = Yii::$app->session['currentId_academic_year'];
  	      
  	       $there_is=1;
  	       
  	        $gradesInfo = SrcGrades::find()->joinWith('course0')->where(['courses.optional'=>0,'student'=>$student, 'courses.academic_year'=>$acad])->all();
  	        
  	        if(isset($gradesInfo)&&($gradesInfo!=null))
			  {
				  foreach($gradesInfo as $grade)
					 {
					 	if($grade['validate']==0)
					 	  $there_is=0;
					 }
					 
					 
			  }
			  else
			     $there_is=1;
			     
			     
	
	       return $there_is;
  
  
  } 
  
public function getAllSubjects($program,$shift,$room)
	{    
       	  
          $code= array();
          $code[null][null]= Yii::t('app','-- Select --');
		  
         $modelCourse= new SrcCourses();
	       //$result=$modelCourse->searchCourseByProgramShiftRoom($program,$shift,$room);
	         $result=$modelCourse->searchCourseByProgramShiftGroup($program,$shift,$room);
			
			 if(isset($result))
			  {  
			            //$k is a counter
						$k=0;
			    foreach($result as $i){			   
					  
					  $code[$k][0]= $i->id;
					  $code[$k][1]= $i->subject_name;
					  $code[$k][2]= $i->weight;
					  $code[$k][3]= $i->short_subject_name;
					   $code[$k][4]= $i->reference_id;
					   $code[$k][5]= $i->code_module; //modue
					  $k=$k+1;
					  
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	
public function getAllSubjectsByLevel($program,$shift,$level,$room)
	{    
       	  
          $code= array();
          $code[null][null]= Yii::t('app','-- Select --');
		  
         $modelCourse= new SrcCourses();
	       //$result=$modelCourse->searchCourseByProgramShiftLevelRoom($program,$shift,$level,$room);
	       $result=$modelCourse->searchCourseByProgramShiftLevelGroup($program,$shift,$level,$room);
			
			 if(isset($result))
			  {   
			            //$k is a counter
						$k=0;
			    foreach($result as $i){			   
					  
					  $code[$k][0]= $i->id;
					  $code[$k][1]= $i->subject_name;
					  $code[$k][2]= $i->weight;
					  $code[$k][3]= $i->short_subject_name;
					   $code[$k][4]= $i->reference_id;
					   $code[$k][5]= $i->code_module; //modue
                                           $code[$k][6]= $i->teacher_name; //teacher
					  $k=$k+1;
					  
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	

	public function isSubjectEvaluated($course)
	{    
       	  
                $bool=false;
		  
		 $modelCourse= new SrcCourses();
	       $result=$modelCourse->evaluatedSubject($course);
			
			 if(isset($result)&&($result!=null))
			  {  			            //$k is a counter
						$k=0;
						
			    foreach($result as $i)
			       {		
					  if($i->id!=null)
					       $bool=true;
					 }
			     
			      	 
			       	 
			 }
			
			  	 					 
		    
			
		return $bool;
         
	}
	
public function isOldSubjectEvaluated($course)
	{    
       	  
                $bool=false;
		  
		 $modelCourse= new SrcCourses();
	       $result=$modelCourse->evaluatedOldSubject($course);
			
			 if(isset($result))
			  {  
			            //$k is a counter
						$k=0;
						 
			    foreach($result as $i){			   
					  
					  
					  if($i->id!=null)
					       $bool=true;
					  					  
				}  
			 }	 					 
		      
			
		return $bool;
         
	}
			
  
  
  
  
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcGrades::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
 
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'course' => $this->course,
            'grade_value' => $this->grade_value,
            'validate' => $this->validate,
            'publish' => $this->publish,
            'is_particular_case' => $this->is_particular_case,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'date_validation' => $this->date_validation,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'validated_by', $this->validated_by])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    

public function searchGlobal($params,$acad)
    {     
        //$this->globalSearch = $params;


    $query = SrcGrades::find()->joinWith(['course0'])->where('academic_year='.$acad);//->orderBy(['Persons.last_name'=>SORT_ASC, 'Rooms.room_name'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000000,
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

   $query->joinWith(['course0', 'course0.module0.subject0', 'course0.module0.program0', 'student0','course0.room0','course0.shift0',]);
    $query->orFilterWhere(['like', 'Subjects.subject_name', $this->globalSearch])
           ->orFilterWhere(['like', 'Subjects.short_subject_name', $this->globalSearch])
           ->orFilterWhere(['like', 'Program.label', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Rooms.room_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Shifts.shift_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
 


public function searchGradesForTeacher($person_id)
    {     
        //$this->globalSearch = $params;


    $query = SrcGrades::find()->joinWith(['course0'])->where('teacher='.$person_id);//->orderBy(['last_name'=>SORT_ASC, 'first_name'=>SORT_ASC]);
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000000,
        ],
    ]);

    
    return $dataProvider;
    }



public function searchGradesForTeacherAcad($person_id,$acad)
    {     
        //$this->globalSearch = $params;


    $query = SrcGrades::find()->joinWith(['course0'])->where('teacher='.$person_id.' and academic_year='.$acad);//->orderBy(['last_name'=>SORT_ASC, 'first_name'=>SORT_ASC]);
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000000,
        ],
    ]);

    
    return $dataProvider;
    }
    

public function searchReprise()
    {     
        //$this->globalSearch = $params;


    $query = SrcGrades::find()->joinWith(['student0','course0'])->where('grade_value < passing_grade ')->orderBy(['last_name'=>SORT_ASC, 'first_name'=>SORT_ASC])->groupBy('student');
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000000,
        ],
    ]);

    
    return $dataProvider;
    }
    
  public function searchRepriseForTeacher($person_id)
    {     
        //$this->globalSearch = $params;


    $query = SrcGrades::find()->joinWith(['course0'])->where('teacher='.$person_id);//->orderBy(['last_name'=>SORT_ASC, 'first_name'=>SORT_ASC]);
    
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 10000000,
        ],
    ]);

    
    return $dataProvider;
    }
    
    
public function searchByProgram($program)
    {       
        //$this->globalSearch = $params;


    $query = SrcGrades::find()
              //->orderBy(['Persons.last_name'=>SORT_ASC, 'Rooms.room_name'=>SORT_ASC])
              ->joinWith(['course0', 'course0.module0.subject0', 'course0.module0.program0', 'student0','course0.room0','course0.shift0',])
               ->orFilterWhere(['like', 'program.id', $program]);
               
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000,
        ],
    ]);


    return $dataProvider;
    }
    
    
    
   public function searchByProgramAcad($program,$acad)
    {       
        //$this->globalSearch = $params;


    $query = SrcGrades::find()
              //->orderBy(['Persons.last_name'=>SORT_ASC, 'Rooms.room_name'=>SORT_ASC])
              ->joinWith(['course0', 'course0.module0.subject0', 'course0.module0.program0', 'student0','course0.room0','course0.shift0',])
              ->where(['program.id'=>$program, 'academic_year'=>$acad]);// ->orFilterWhere(['like', 'program.id', $program]);
               
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000,
        ],
    ]);


    return $dataProvider;
    }
    
    
    
public function checkDataSubjectAverage($course)   
    	{  
    		$sql='SELECT course FROM subject_average sa inner join academicperiods a ON(a.id=sa.academic_year) WHERE sa.course='.$course ;

	
		 		  $is_there = Yii::$app->db->createCommand($sql)->queryAll();
            return $is_there;
 	
	}
 
 
public function searchForReportCard($student,$course)
	{   
          $query = SrcGrades::find()->select(['g.id','g.grade_value', 'g.validate','g.date_validation','g.validated_by', 'g.comment','g.publish',]   )
                ->alias('g')
                ->joinWith(['course0','student0',])
                ->distinct(true)
                ->where(['student'=>$student, 'course'=>$course]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
 
         
        return $dataProvider->getModels();
	}

   
       
public function AVGcourse($course)  
    {
       
     $query = SrcGrades::find()->select("AVG('grade_value') as 'average'"   )
                ->joinWith('course0')
                ->where(['course'=>$course]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
 
         
        return $dataProvider->getModels();
    }
        
    
public function AVGcourseByGroup($course,$shift,$level,$group)  
    {
       
     $query = SrcGrades::find()->select("AVG('grade_value') as 'average'"   )
                ->joinWith('course0.students.studentLevel')
                ->where(['course'=>$course])
                ->andWhere(['courses.shift'=>$shift])
                ->andWhere(['student_level.level'=>$level])
                ->andWhere(['student_level.room'=>$group]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],
        ]);
 
         
        return $dataProvider->getModels();
    }
  
    
 public function getGradeInfoByStudentCourse($student,$course){
       
      $modelGrade = SrcGrades::find()->select(['id','grade_value','validate','publish','date_validation','validated_by','comment'])
                         ->where(['student'=>$student, 'course'=>$course])
                         ->all();
                          
       foreach($modelGrade as $result)
         return  $result;    
       
        
         
    }

    
    
  public function getValidateGrade(){
            switch($this->validate)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
                
            }
        }
        
        public function getPublishGrade(){
            switch($this->publish)
            {
                case 0:
                    return Yii::t('app','No');
                case 1:
                    return Yii::t('app','Yes');
                
            }
        }
  
    
    
    
    
    
    
   
    
    
    
    
    
    
    
    
}
