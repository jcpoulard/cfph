<?php

namespace app\modules\fi\models;

use Yii;

use app\modules\fi\models\SrcPersons; 
use app\modules\fi\models\SrcCourses; 

/**
 * This is the model class for table "reprise_grades".
 *
 * @property integer $id
 * @property integer $student
 * @property integer $course
 * @property double $grade_value_1
 * @property double $grade_value_2
 * @property double $grade_value_3
 * @property string $date_updated
 * @property string $update_by
 *
 * @property Courses $course0
 * @property Persons $student0
 */
class RepriseGrades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reprise_grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'student', 'course'], 'required'],
            [['id', 'student', 'course'], 'integer'],
            [['grade_value_1', 'grade_value_2', 'grade_value_3'], 'number'],
            [['update_by'], 'string', 'max' => 45],
            [['date_updated'], 'safe'],
            [['course'], 'exist', 'skipOnError' => true, 'targetClass' => SrcCourses::className(), 'targetAttribute' => ['course' => 'id']],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['student' => 'id']],
            [['course','student'], 'unique', 'targetAttribute' => ['course','student'],  'message' => Yii::t('app', 'This combinaison "course - student " has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'course' => Yii::t('app', 'Course'),
            'grade_value_1' => Yii::t('app', 'Reprise 1'),
            'grade_value_2' => Yii::t('app', 'Reprise 2'),
            'grade_value_3' => Yii::t('app', 'Reprise 3'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse0()
    {
        return $this->hasOne(SrcCourses::className(), ['id' => 'course']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'student']);
    }
}
