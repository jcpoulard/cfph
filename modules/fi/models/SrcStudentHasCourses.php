<?php

namespace app\modules\fi\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\modules\fi\models\StudentHasCourses;

/** 
 * SrcStudentHasCourses represents the model behind the search form about `app\modules\fi\models\StudentHasCourses`.
 */
class SrcStudentHasCourses extends StudentHasCourses
{
   public $globalSearch;
   public $program; 
   public $shift;
   public $room;
   public $level; 
   public $group;
   
   public $total; 
   public $succes;
   public $echec;
   
   
   public $first_name;
   public $last_name;
   public $total_stud=0;
  
   public $studentByCourse;
   public $past_courses;
   public $academic_year;
   
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'course', 'is_pass','past_courses','academic_year'], 'integer'],
            [['date_created', 'globalSearch','studentsByCourse', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['course','student'], 'unique', 'targetAttribute' => ['course','student'],  'message' => Yii::t('app', 'This combinaison "course - student " has already been taken.')],
        ];
    }
      
   public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array_merge(
		    	parent::attributeLabels(), array(
			               'program' =>Yii::t('app','Program'),
			               'shift' =>Yii::t('app','Shift'),
			               'room' =>Yii::t('app','Room'),
			               'level' =>Yii::t('app','Level'),
                                      'past_courses' => Yii::t('app', 'Past courses'),
                                      'academic_year' => Yii::t('app', 'Academic Year'),
			               
		));
	}		

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $acad = Yii::$app->session['currentId_academic_year'];
        
        $query = SrcStudentHasCourses::find()->orderBy(['Persons.last_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       //$query->joinWith(['course0', 'course0.module0.subject0', 'student0', 'student0.studentLevel', 'course0.room0','course0.shift0',]);
        $query->joinWith(['course0', 'course0.module0.subject0', 'student0', 'student0.studentLevel', 'course0.shift0',]);
        // grid filtering conditions
        $query->andFilterWhere([
            'student' => $this->student,
            'course' => $this->course,
            'is_pass' => $this->is_pass,
            'past_courses' => $this->past_courses,
            'academic_year' => $this->academic_year,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andWhere(['courses.academic_year'=>$acad]);
        
        $query->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
 
 
      
  public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;
                $acad = Yii::$app->session['currentId_academic_year'];

    $query = SrcStudentHasCourses::find()->groupBy('student');//->orderBy(['Subjects.subject_name'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => 100000000,//Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]); 

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

   //$query->joinWith(['course0', 'course0.module0.subject0', 'student0', 'student0.studentLevel', 'course0.room0','course0.shift0']);
    $query->joinWith(['course0', 'course0.module0.subject0', 'student0', 'student0.studentLevel', 'course0.shift0']);
    
    $query->andWhere(['courses.academic_year'=>$acad]);
    
    $query->orFilterWhere(['like', 'Subjects.subject_name', $this->globalSearch])
           ->orFilterWhere(['like', 'Subjects.short_subject_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch]);
            //->orFilterWhere(['like', 'Rooms.room_name', $this->globalSearch])
           // ->orFilterWhere(['like', 'Shifts.shift_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }
      

 public function searchStudentsInProgram($program)
  {
  	  $acad = Yii::$app->session['currentId_academic_year'];
      
		           	    //tout kou ki nan program lan
				        $query_course_in_program = SrcStudentHasCourses::find()
		                ->joinWith(['course0','course0.module0','student0.studentLevel'])
		                ->where(['module.program'=>$program])
		                ->andWhere(['courses.academic_year'=>$acad])
		                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                                ->groupBy('student');
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query_course_in_program,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}
   
    
  public function getCourseByIdStudent($student)
    {
       
     $query = SrcStudentHasCourses::find()->joinWith('course0')
                ->where(['student'=>$student]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],

        ]);

         
        return $dataProvider->getModels();
    }
    
   
 /*       
public function getStudentsEnrolled($room, $shift, $course)  
    {
       
     $query = SrcStudentHasCourses::find()->select("COUNT('student') as 'total_stud'"   )
                ->joinWith(['course0','student0'])
                ->where(['course'=>$course])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['room'=>$room]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }
*/
public function getStudentsEnrolled($level, $shift, $course)  
    {
       
     $query = SrcStudentHasCourses::find()->select("COUNT('student') as 'total_stud'"   )
                ->joinWith(['course0','student0','student0.studentLevel'])
                ->where(['course'=>$course])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['student_level.level'=>$level]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }

public function getStudentsEnrolledGroup($group, $level, $shift, $acad)  
    {
       
     $query = SrcStudentHasCourses::find()->select("COUNT('student') as 'total_stud'"   )
                ->joinWith(['course0','student0.studentLevel'])
                ->where(['academic_year'=>$acad])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['student_level.level'=>$level])
                ->andWhere(['student_level.room'=>$group]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }


/*
public function getStudentsEnrolledRoom($room, $shift, $acad)  
    {
       
     $query = SrcStudentHasCourses::find()->select("COUNT('student') as 'total_stud'"   )
                ->joinWith(['course0','student0'])
                ->where(['academic_year'=>$acad])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['room'=>$room]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }
 */      
/*    
public function getInfoStudentsEnrolled($room, $shift, $course)  
    {
       
     $query = SrcStudentHasCourses::find()->select(['persons.id','first_name','last_name',]   )
                ->joinWith(['course0','student0' ])
                ->where(['course'=>$course])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['room'=>$room]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }  
  */  
 public function getInfoStudentsEnrolled($level, $shift, $course)  
    {
       
     $query = SrcStudentHasCourses::find()->select(['persons.id','first_name','last_name',]   )
                ->joinWith(['course0','student0','student0.studentLevel' ])
                ->where(['course'=>$course])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['student_level.level'=>$level]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }     

public function getInfoStudentsEnrolledGroup($group, $level, $shift, $acad)   
    {
       
     $query = SrcStudentHasCourses::find()->select(['persons.id','first_name','last_name',]   )
                ->joinWith(['course0','student0.studentLevel', ])
                ->where(['academic_year'=>$acad])
                ->andWhere(['courses.shift'=>$shift])
                ->andWhere(['student_level.level'=>$level])
                ->andWhere(['student_level.room'=>$group])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }

public function searchStudentsEnrolledSpecialisation($acad)
  {
  	
			$query = SrcStudentHasCourses::find()
		                ->joinWith(['course0.module0.program0','student0'])
		                ->select(['student','persons.gender'])
                                ->where('old_new=1 AND academic_year='.$acad.' AND in_use=1 AND is_special=1')
		                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                                ->groupBy('student');
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}
        
        
public function searchStudentsEnrolledByProgram($program,$acad)
  {
  	
			$query = SrcStudentHasCourses::find()
                                ->joinWith(['course0.module0','student0'])
		                ->select(['student','persons.gender'])
		                ->where('old_new=1 AND academic_year='.$acad.' AND in_use=1 AND program='.$program)
		                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                                ->groupBy('student');
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}
        
        
 public function searchStudentsEnrolledByShift($shift,$acad)
   {
  	
			$query = SrcStudentHasCourses::find()
		                ->joinWith(['course0.module0','student0'])
		                ->select(['student',])
                                ->where('old_new=1 AND academic_year='.$acad.' AND in_use=1 AND shift='.$shift)
		                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                                ->groupBy('student');
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}        
         
         
 public function searchStudentsEnrolledAllProgram($acad)
    {
  	
			$query = SrcStudentHasCourses::find()
		                ->joinWith(['course0.module0','student0'])
		                ->where('old_new=1 AND in_use=1 ')
		                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC])
                                ->groupBy('student');
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
   }
   
public function searchFailedModuleByStudent($student, $acad)
 {
    $query = SrcStudentHasCourses::find()->select("course" )
                ->joinWith(['course0','student0.studentLevel'])
                ->where('student='.$student.' and is_pass=0 and academic_year='.$acad);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
  }
    
/*
public function getInfoStudentsEnrolledRoom($room, $shift, $acad)   
    {
       
     $query = SrcStudentHasCourses::find()->select(['persons.id','first_name','last_name',]   )
                ->joinWith(['course0','student0', ])
                ->where(['academic_year'=>$acad])
                ->andWhere(['courses.shift'=>$shift])
                ->andWhere(['courses.room'=>$room])
                ->orderBy(['persons.last_name'=>SORT_ASC,'persons.first_name'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);
 
         
        return $dataProvider->getModels();
    }     
*/    
     

 public function getLevelByStudentId($student) {
 	
 	$modelStudentLevel = SrcStudentLevel::find()
                         ->where(['student_id'=>$student])
                         ->all();
                          
           
        foreach($modelStudentLevel as $result)
         return  $result->level;
 	
 	
  } 
   
    
 public function getIsPassByStudentCourse($student,$course){
       
      $modelCourse = SrcStudentHasCourses::find()
                         ->where(['student'=>$student])
                         ->andWhere(['course'=>$course])
                         ->all();
                          
           
        foreach($modelCourse as $result)
         return  $result->is_pass;
        
         
    }    
    
    
 public function getIsPass()
    {
        switch($this->is_pass)
          {
          	case 0: return Yii::t('app', 'Fail');
          	
          	          break;
          	case 1: return Yii::t('app', 'Success');
          	
          	          break;
          	
          	
          	
          }
        
        
       
    }
    
 public function getValueIsPass($value)
    {
        switch($value)
          {
          	case 0: return Yii::t('app', 'Fail');
          	
          	          break;
          	case 1: return Yii::t('app', 'Success');
          	
          	          break;
          	
          	
          	
          }
        
        
        
    }    
    


 public function getTotalCourseForStudent($student){
       
      $countCourse = SrcStudentHasCourses::find()->alias('shc')->select(['course'])
                         ->where(['student'=>$student])
                         ->count();               
           
         return  $countCourse;
        
         
    }    

 public function getTotalCoursePassForStudent($student){
       
      $countCoursePass = SrcStudentHasCourses::find()->alias('shc')->select(['course'])
                         ->where(['is_pass'=>1, 'student'=>$student])
                         ->count();               
           
         return  $countCoursePass;
        
         
    }    



 public function isCycleCompleted($student){
       
       $modelStudCourse  = new SrcStudentHasCourses;
       
     $countCourse = $modelStudCourse->getTotalCourseForStudent($student);
      $countCoursePass = $modelStudCourse->getTotalCoursePassForStudent($student);            
           
         
      if($countCourse==$countCoursePass)
         return  1;
      else
         return 0;
        
         
    }    


 public function isCompleted($value)
    {
        switch($value)
          {
          	case 0: return Yii::t('app', 'Not completed');
          	
          	          break;
          	case 1: return Yii::t('app', 'Completed');
          	
          	          break;
          	
          	
          	
          }
        
        
       
    }    

     public function isCompleted2($value)
    {
        switch($value)
          {
          	case 0: return Yii::t('app', 'NON DIPLÔMÉ(E)');
          	
          	          break;
          	case 1: return Yii::t('app', 'DIPLÔMÉ(E)');
          	
          	          break;
          	
          	
          	
          }
        
        
       
    }  
    
/*
 public function searchStudentsInProgramLevelShiftAndRoom($program,$level,$shift,$room)
  {
  	  $acad = Yii::$app->session['currentId_academic_year'];
      
		           	    //tout kou ki nan program lan
				        $query_course_in_program_level_shift_room = SrcStudentHasCourses::find()
		                ->joinWith(['course0','course0.module0','student0.studentLevel'])
		                ->where(['module.program'=>$program])
		                ->andWhere(['student_level.level'=>$level])
		                ->andWhere(['courses.shift'=>$shift])
		                ->andWhere(['courses.room'=>$room]);
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query_course_in_program_level_shift_room,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}
 */
 
  public function searchStudentsInProgramLevelShiftAndGroup($program,$level,$shift,$group)
  {
  	  $acad = Yii::$app->session['currentId_academic_year'];
      
		           	    //tout kou ki nan program lan
				        $query_course_in_program_level_shift_room = SrcStudentHasCourses::find()
		                ->joinWith(['course0','course0.module0','student0.studentLevel'])
		                ->where(['module.program'=>$program])
		                ->andWhere(['student_level.level'=>$level])
		                ->andWhere(['student_level.room'=>$group])
		                ->andWhere(['courses.shift'=>$shift]);
		                   

        $dataProvider = new ActiveDataProvider([
            'query' => $query_course_in_program_level_shift_room,
            'pagination' => [
            'pageSize' => 10000000,
        ],

        ]);

         
        return $dataProvider;
  	
  	}
 	
  	
/*  	
 public function searchCoursesInProgramShiftRoomForStudentToAdd($student,$program,$shift,$room)
    {
      $acad = Yii::$app->session['currentId_academic_year'];
      
       if($program==NULL)
        { $program=0;
              
              //tout kou ki nan program lan
             $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
	                ->joinWith(['course0.module0'])
	                ->where(['program'=>$program])
	                ->andWhere(['student'=>$student]);
          }
       else
	      { 
	      	if($shift==NULL)
		         { $shift=0; 
		           $room = 0;
		           
		            //tout kou ki nan program lan
	                $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['course0','course0.module0'])
		                ->where(['program'=>$program])
		                ->andWhere(['student'=>$student])
		                ->andWhere(['shift'=>$shift])
		                ->andWhere(['room'=>$room]);
		                
		          }
		         else
		           {
		           	    //tout kou ki nan program lan
				        $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['course0','course0.module0'])
		                ->where(['program'=>$program])
		                ->andWhere(['student'=>$student])
		                ->andWhere(['shift'=>$shift])
		                ->andWhere(['room'=>$room]);
		           	}
		           	
	      	   	         
	       }
                
                 
	  //kou li poko pran nan program lan
     $query = SrcCourses::find()
                  ->joinWith(['module0'])
                  ->where(['program'=>$program])
                  ->andWhere(['shift'=>$shift])
	              ->andWhere(['room'=>$room])
                  ->andWhere(['academic_year'=>$acad])
                  ->andWhere(['not in','courses.id',$query_course_in_program_shift_room]);

        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000000000000000000000,
        ],

        ]);

         
        return $dataProvider;
    }
*/
    
    
 public function searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,$shift,$level,$acad)
    {
      
      
       if($program==NULL)
        { $program=0;
              
              //tout kou ki nan program lan
             $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
	                ->joinWith(['course0.module0'])
	                ->where(['program'=>$program])
	                ->andWhere(['student'=>$student]);
          }
       else
	      { 
	      	if($shift==NULL)
		         { $shift=0; 
		           $level = 0;
		           
		            //tout kou ki nan program lan
	                $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['course0','course0.module0'])
		                ->where(['program'=>$program])
		                ->andWhere(['student'=>$student])
		                ->andWhere(['shift'=>$shift]);
		              //  ->andWhere(['room'=>$room]);
		                
		          }
		         else
		           {
		           	    //tout kou ki nan program lan
				        $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['course0','course0.module0'])
		                ->where(['program'=>$program])
		                ->andWhere(['student'=>$student])
		                ->andWhere(['shift'=>$shift]);
		                //->andWhere(['room'=>$room]);
		           	}
		           	
	      	   	         
	       } 
                
                 
	  //kou li poko pran nan program lan
     $query = SrcCourses::find()
                  ->joinWith(['module0'])
                  ->where(['program'=>$program])
                  ->andWhere(['shift'=>$shift])
	             // ->andWhere(['room'=>$room])
                  ->andWhere(['academic_year'=>$acad])
                  ->andWhere(['not in','courses.id',$query_course_in_program_shift_room]);

        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 1000000,
        ],

        ]);

         
        return $dataProvider;
    }
    
    
 public function getStudentsForDecision($group, $level, $shift,$program, $acad)  
    {
       
     $query = SrcStudentHasCourses::find()->select("student,COUNT(course) AS 'total',COUNT(CASE WHEN is_pass = 1 THEN 1 END) AS 'succes'"   )
                ->joinWith(['course0.module0','student0.studentLevel'])
                ->where(['academic_year'=>$acad])
                ->andWhere(['program'=>$program])
                ->andWhere(['shift'=>$shift])
                ->andWhere(['student_level.level'=>$level])
                ->andWhere(['student_level.room'=>$group])
                ->andWhere(['optional'=>0])
                ->groupBy('student');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            'pageSize' => 10000,
        ],

        ]);
 
         
        return $dataProvider;   //  $dataProvider->getModels();
    }

  

				       
    
    
    
        
    
    
}
