<?php

namespace app\modules\fi\models;

use Yii;

/**
 * This is the model class for table "year_migration_check".
 *
 * @property integer $id
 * @property integer $academic_year
 * @property integer $postulant
 * @property integer $course
 * @property integer $fees
 * @property integer $taxes
 * @property integer $pending_balance
 * @property integer $marge_echec
 * @property integer $payroll_setting
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * @property Academicperiods $academicYear
 */
class YearMigrationCheck extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'year_migration_check';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['academic_year', 'postulant', 'course', 'fees', 'taxes', 'pending_balance', 'marge_echec', 'payroll_setting', 'date_created', 'date_updated', 'create_by', 'update_by'], 'required'],
            [['academic_year', 'postulant', 'course', 'fees', 'taxes', 'pending_balance', 'marge_echec', 'payroll_setting'], 'integer'],
            [['date_created', 'date_updated'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 65],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'postulant' => Yii::t('app', 'Postulant'),
            'course' => Yii::t('app', 'Course'),
            'fees' => Yii::t('app', 'Fees'),
            'taxes' => Yii::t('app', 'Taxes'),
            'pending_balance' => Yii::t('app', 'Pending Balance'),
            'marge_echec' => Yii::t('app', 'Marge Echec'),
            'payroll_setting' => Yii::t('app', 'Payroll Setting'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }
    
    
    public function getValueYearMigrationCheck($acad)
{
   $command= Yii::$app->db->createCommand('SELECT postulant,course,fees,taxes,pending_balance,marge_echec,payroll_setting,date_created FROM year_migration_check WHERE academic_year='.$acad); 
   		
	     $data_ = $command->queryAll();
    
	return $data_;
       
  }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
