<?php

namespace app\modules\fi\models;
  
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\fi\models\Courses;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\SrcPersons;

/**
 * SrcCourses represents the model behind the search form about `app\modules\fi\models\Courses`.
 */
class SrcCourses extends Courses
{
   public $globalSearch;
   //public $studentsByCourse;
   
   public $subject_name;
   public $short_subject_name;
   
   public $teacher_name;
   
   public $code_module;
   
   public $shift_name;
   public $room_name;
   
   
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'module', 'reference_id', 'teacher', 'room', 'academic_year', 'shift', 'debase', 'optional', 'old_new'], 'integer'],
            [['weight', 'passing_grade'], 'number'],
            [['date_created', 'globalSearch', 'reference_id', 'studentByCourse', 'date_updated', 'create_by', 'update_by'], 'safe'],
            [['module','teacher','shift','academic_year'], 'unique', 'targetAttribute' => ['module','teacher','shift','academic_year'],  'message' => Yii::t('app', 'This combinaison "module - shift - teacher - academic_year" has already been taken.')]  //[['module','room','shift','academic_year'], 'unique', 'targetAttribute' => ['module','room','shift','academic_year'],  'message' => Yii::t('app', 'This combinaison "module - shift - room - academic_year" has already been taken.')]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

public function search($params)
    {
        $acad=Yii::$app->session['currentId_academic_year'];
        
        $query = SrcCourses::find()->joinWith(['module0'])->where('academic_year='.$acad)->orderBy(['module.code'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'module' => $this->module,
            'teacher' => $this->teacher,
           // 'room' => $this->room,
            'academic_year' => $this->academic_year,
            'shift' => $this->shift,
            'weight' => $this->weight,
            'debase' => $this->debase,
            'optional' => $this->optional,
            'old_new' => $this->old_new,
            'reference_id' => $this->reference_id,
            'passing_grade' => $this->passing_grade,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
        ]);

        $query->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
 

 public function searchDashboard()
    {
        $acad=Yii::$app->session['currentId_academic_year'];
        
        $query = SrcCourses::find()->joinWith(['module0'])->select(['courses.id'])->where('academic_year='.$acad)->orderBy(['module.code'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000,
    			],
        ]);

       
        return $dataProvider;
    }
 
   
    
public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;
                $acad=Yii::$app->session['currentId_academic_year'];

    $query = SrcCourses::find()->where('academic_year='.$acad)->orderBy(['module.code'=>SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    $this->load($params);

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }

   //$query->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','room0','shift0',]);
   $query->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','shift0',]);
    $query->orFilterWhere(['like', 'Subjects.subject_name', $this->globalSearch])
           ->orFilterWhere(['like', 'Subjects.short_subject_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
             ->orFilterWhere(['like', 'Program.label', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch])
           // ->orFilterWhere(['like', 'Rooms.room_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Shifts.shift_name', $this->globalSearch]);
            //->andFilterWhere(['like', '', x]);

    return $dataProvider;
    }



 
public function searchCoursesForTeacher($person_id)
    {
        $acad=Yii::$app->session['currentId_academic_year'];
        
        $query = SrcCourses::find()->joinWith(['module0'])->where('academic_year='.$acad.' and teacher='.$person_id)->orderBy(['module.code'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        return $dataProvider;
        
    }

 
public function getCourseinroomforgrades($room,$shift,$program,$level,$acad){
       
       $data=[];
   
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->all();      
         
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }

 
public function getCourseingroupforgrades($group,$shift,$program,$level,$acad){
       
       $data=[];
   
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level.room'=>$group,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level.room'=>$group,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->all();      
         
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
    

    
public function getCourseingroupfordelayedgrades($group,$shift,$program,$level,$acad){
       
       $data=[];
                        
        /* $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                        ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad])
                         ->andWhere('c.id not in(select distinct course from grades)')
                        ->count();
        
      
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                          ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad])
                         ->andWhere('c.id not in(select distinct course from grades)')
                        ->all(); 
         * 
         */     
         
         $command = Yii::$app->db->createCommand('SELECT DISTINCT c.id, c.module, teacher FROM courses c INNER JOIN module m ON(c.module=m.id) INNER JOIN student_has_courses shc ON(shc.course=c.id) WHERE c.shift='.$shift.' AND m.program='.$program.' AND c.academic_year='.$acad.' AND c.id NOT IN(SELECT course FROM grades g WHERE g.student=shc.student) AND shc.student in(SELECT student_id FROM student_level_history slh WHERE slh.room='.$group.' and slh.level='.$level.' and slh.academic_year='.$acad.')' )->queryAll(); 
              
              
      if($command!=null)
       { 
          	 foreach($command as $course)
         	    {     
                           $modelModule = SrcModule::findOne($course['module']);
                       $modelPers = SrcPersons::findOne($course['teacher']);
         	   	   
                       if($modelModule->subject0->subject_name!='')
         	   	     $data[ $course['id']  ] =  $modelModule->subject0->subject_name." (".$modelPers->getFullName().") [".$modelModule->code."]";
         	   	 
                    }
           }
        
         
         return $data;
          
    }

    
 public function getCourseingroupfordelayedgradespc($group,$shift,$program,$level,$acad){   // Particlar Case
       
       $data=[];
                        
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                        ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                        // ->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad])
                         //->andWhere('grade_value IS not NULL')
                         ->count();
        
      
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         //->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad])
                         //->andWhere('grade_value IS not NULL')
                         ->all();      
         
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }   
    
public function getCourseinroom($room,$shift,$program,$acad){
       
       $data=[];
   
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','module0.program0.studentOtherInfos'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','module0.program0.studentOtherInfos'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->all();      
         
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }


public function getCourseinshift($shift,$program,$acad){
       
       $data=[];
   
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentOtherInfo0'])
                         ->where(['c.shift'=>$shift,'program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentOtherInfo0'])
                         ->where(['c.shift'=>$shift,'program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->all();      
         
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
    

public function getRoominshiftforgrades_($from,$shift,$program,$level,$acad)
{
    $current_acad = Yii::$app->session['currentId_academic_year'];
    
	 $data=[];
	 
	 if($from=='grades')
        { Yii::$app->session['grades_program']= $program;
        Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_shift']= $shift;
         Yii::$app->session['grades_room'] = '';
        }
      elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
        Yii::$app->session['studentHasCourses_level']= $level;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_room'] = '';
        }
      elseif($from=='idcards')
        { Yii::$app->session['idcard_program']= $program;
        Yii::$app->session['idcard_level']= $level;
           Yii::$app->session['idcard_shift']= $shift;
         Yii::$app->session['idcard_room'] = '';
        }
      
     if($current_acad==$acad){   
        $countRooms = SrcCourses::find()->alias('c')->select(['student_level.room'])
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['c.shift'=>$shift,'program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select('student_level.room')
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['c.shift'=>$shift,'program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
        
         if($countRooms > 0)
          {
         	   
         	   foreach($allRooms as $room)
         	    { 
         	    	//chache room_name
         	    	if($room->room!=0)
         	    	  {  $room_name = SrcRooms::findOne($room->room)->room_name;
         	   	         $data[$room->room]=$room_name;
         	   	         
         	    	  }
         	   	    
         	   	 }
           }
     }
     elseif($current_acad!=$acad)
     {    $countRooms = 0;
          $allRooms = Yii::$app->db->createCommand('SELECT DISTINCT room FROM student_level_history slh INNER JOIN student_other_info soi ON(soi.student=slh.student_id) INNER JOIN rooms r ON(r.id=slh.room) WHERE apply_shift='.$shift.' AND program='.$program.'  AND level='.$level.' AND academic_year='.$acad.' ORDER BY room_name ASC')->queryAll();
                        if(($allRooms!=null))
                          {
                            foreach($allRooms as $result)
                              {
                                $countRooms ++;
                              }
                          }
                          
        if($countRooms > 0)
          {
         	   
         	   foreach($allRooms as $room)
         	    { 
         	    	//chache room_name
         	    	if($room['room']!=0)
         	    	  {  $room_name = SrcRooms::findOne($room['room'])->room_name;
         	   	         $data[$room['room']]=$room_name;
         	   	         
         	    	  }
         	   	    
         	   	 }
           }
           
     }
         
         return $data;

	}
 
        
public function getRoominshiftfordelayedgrades_($from,$shift,$program,$level,$acad)
{
	 $data=[];
	 
	// if($from=='grades')
       // { 
            Yii::$app->session['grades_program']= $program;
            Yii::$app->session['grades_level']= $level;
               Yii::$app->session['grades_shift']= $shift;
             Yii::$app->session['grades_room'] = '';
       // }
     /* elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
        Yii::$app->session['studentHasCourses_level']= $level;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_room'] = '';
        }
      elseif($from=='idcards')
        { Yii::$app->session['idcard_program']= $program;
        Yii::$app->session['idcard_level']= $level;
           Yii::$app->session['idcard_shift']= $shift;
         Yii::$app->session['idcard_room'] = '';
        }
      * 
      */
      
        
        $countRooms = SrcCourses::find()->alias('c')->select(['student_level_history.room'])
                         ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year'=>$acad])
                          ->distinct()
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select('student_level_history.room')
                         ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year'=>$acad])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
        
         if($countRooms > 0)
          {
         	   
         	   foreach($allRooms as $room)
         	    { 
         	    	//chache room_name
         	    	if($room->room!=0)
         	    	  {  $room_name = SrcRooms::findOne($room->room)->room_name;
         	   	         $data[$room->room]=$room_name;
         	   	         
         	    	  }
         	   	    
         	   	 }
           }
         
         return $data;

	}
    
        
        
//kou li poko pran nan sal la
public function getCourseNottakenInroom($student,$room,$shift,$program,$level,$acad){
       
                     
          $data=[];
        
        $query_course_in_program_shift_room = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['course0','course0.module0'])
		                ->where(['program'=>$program])
		                ->andWhere(['student'=>$student])
		                ->andWhere(['shift'=>$shift])
		                ->andWhere(['room'=>$room]);
        
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['student_level.room'=>$room,'c.shift'=>$shift,'program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->andWhere(['not in','c.id',$query_course_in_program_shift_room])
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->joinWith(['module0','students.studentOtherInfo0','students.studentLevel'])
                         ->where(['student_level.room'=>$room,'c.shift'=>$shift,'program'=>$program,'stuudent_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->andWhere(['not in','c.id',$query_course_in_program_shift_room])
                         ->all();      
       
         if($countCourses > 0)
          { 
          	 foreach($allCourses as $course)
         	    {     
         	   	   if($course->module0->subject0->subject_name!='')
         	   	     $data[ $course->id ] =  $course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]" ;	            	   	    
         	   	 }
           }
        
         
         return $data;
          
    }
      
    
 public function getDebase()
    {
        switch ($this->debase)
         {
         	case 0: return Yii::t('app', 'No');
         	         break;
         	case 1: return Yii::t('app', 'Yes');
         	         break; 
         	
         }
  
     }
     
  public function getOptional()
    {
        switch ($this->optional)
         {
                case 0: return Yii::t('app', 'Yes');  //Yii::t('app', 'No');
         	         break;
                case 1: return Yii::t('app', 'No');  //Yii::t('app', 'Yes');
         	         break; 
         	
         }
  
     }
    
    
  
  public function getSubjectName($course)
    {
       if (($modelCourse = SrcCourses::findOne($course)) !== null) 
        {
           return $modelCourse->module0->subject0->subject_name; 
       
        }
       else
          return null;
        
     }


 public function getCourseName()
    {
       if (($modelCourse = SrcCourses::findOne($this->id)) !== null) 
        {
           return $modelCourse->module0->subject0->subject_name." (".$modelCourse->teacher0->getFullName().") [".$modelCourse->module0->code."]" ; 
       
        }
       else
          return null;
        
     }
     
public function getCompleteCourseName()
    {
       if (($modelCourse = SrcCourses::findOne($this->id)) !== null) 
        {
           return $modelCourse->module0->subject0->subject_name." (".$modelCourse->teacher0->getFullName().") [".$modelCourse->module0->code."] - ".$modelCourse->shift0->shift_name ; 
        }
       else
          return null;
        
     }     
     
      
 public function getWeight($course){
            
	if (($modelCourse = SrcCourses::findOne($course)) !== null) 
        {
           return $modelCourse->weight; 
       
        }
       else
          return null;
         
	}
	
	
 public function getPassingGrade($course){
            
	if (($modelCourse = SrcCourses::findOne($course)) !== null) 
        {
           return $modelCourse->passing_grade; 
       
        }
       else
          return 0;
         
	}
	
        
 public function studentsByCourse($course){
        /*
        $query = SrcStudentHasCourses::find()
                ->joinWith('student0')
                ->where(['course'=>$course]);
        
         * 
         */
        // Start 
          $query = SrcStudentHasCourses::find()->joinWith(['course0','student0'])->where(['in','course',[$course]])->orderBy(['persons.last_name'=>SORT_ASC]);
           $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
                ],
            ]);

          

            return $dataProvider;
        // END 
        
        
    }        
 
  
public function searchCourseByProgramShiftRoom($program,$shift,$room) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
       
     $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(last_name," ", first_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])
                ->joinWith(['module0', 'module0.subject0', 'teacher0', ])
                ->where(['academic_year'=>$acad,'old_new'=>1, 'program'=>$program, 'shift'=>$shift,'room'=>$room]);

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }  
    

public function searchCourseByProgramShiftGroup($program,$shift,$group) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
       
     $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(last_name," ", first_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])
                ->joinWith(['module0', 'module0.subject0', 'teacher0', ])
                ->where(['academic_year'=>$acad,'old_new'=>1, 'program'=>$program, 'c.shift'=>$shift ])
                ->andWhere('c.id in ( select course from student_has_courses shc inner join student_level sl on(sl.student_id=shc.student) where sl.room='.$group.' )' ); ;

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }     
   

public function searchCourseByProgramShift($program,$shift) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
       
     $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(last_name," ", first_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])
                ->joinWith(['module0', 'module0.subject0', 'teacher0', ])
                ->where(['academic_year'=>$acad,'old_new'=>1,  'program'=>$program, 'shift'=>$shift]);

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }     
 
 
 
   
public function searchCourseByProgramShiftLevelRoom($program,$shift,$level,$room) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
       
     $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(last_name," ", first_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])   
                ->joinWith(['module0', 'module0.subject0', 'teacher0' ])
                ->where(['academic_year'=>$acad,'old_new'=>1,  'program'=>$program, 'shift'=>$shift])// ->where(['old_new'=>1, 'optional'=>0, 'program'=>$program, 'shift'=>$shift, 'room'=>$room])
                ->andWhere('c.id in ( select course from student_has_courses shc inner join student_level sl on(sl.student_id=shc.student) where level='.$level.' and room='.$room.')' );   

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }     


   
public function searchCourseByProgramShiftLevelGroup($program,$shift,$level,$group) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
           $query_course_in_program_shift_group = SrcStudentHasCourses::find()->select('course')
		                ->joinWith(['student0.studentLevel'])
		                ->where(['academic_year'=>$acad,'student_level.level'=>$level])
		                ->andWhere(['student_level.room'=>$group]);
		                
		    $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(first_name," ", last_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])   
                ->joinWith(['module0', 'module0.subject0', 'teacher0' ])
                ->where(['academic_year'=>$acad,'old_new'=>1, 'program'=>$program, 'c.shift'=>$shift ])
                ->andWhere(['in','c.id',$query_course_in_program_shift_group]);   

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }     

 
   
public function searchCourseByProgramShiftLevel($program,$shift,$level) 
    {
       $acad=Yii::$app->session['currentId_academic_year'];
     $query = SrcCourses::find()->orderBy(['module.code'=>SORT_ASC]) //(['subject_name'=>SORT_ASC]) 
                ->alias('c')
                ->distinct(true)
                ->select(['c.id','CONCAT(last_name," ", first_name) as teacher_name', 'subject_name', 'short_subject_name', 'weight', 'debase',  'reference_id',  'module.code as code_module' ])   
                ->joinWith(['module0', 'module0.subject0', 'teacher0' ])
                ->where(['academic_year'=>$cad,'old_new'=>1,  'program'=>$program, 'shift'=>$shift])
                ->andWhere('c.id in ( select course from student_has_courses shc inner join student_level sl on(sl.student_id=shc.student) where level='.$level.')' );   

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
    }     
  

	
public function evaluatedSubject($course)
	{   
          
			
		  $query = SrcCourses::find() 
                ->alias('c')
                ->distinct(true)
                ->select(['grades.id' ])
                ->joinWith(['grades' ])
                ->where(['old_new'=>1, 'c.id'=>$course]);

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
	}

public function evaluatedOldSubject($course)
	{   
            $query = SrcCourses::find() 
                ->alias('c')
                ->distinct(true)
                ->select(['grades.id' ])
                ->joinWith(['grades' ])
                ->where(['old_new'=>0,  'c.id'=>$course]);

 

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
 
         
        return $dataProvider->getModels();
	}
	


public function getCourseInfoByShiftRoomAcadProgram($shift,$room,$acad,$program){
       
      $current_acad=Yii::$app->session['currentId_academic_year'];
      
      if($current_acad==$acad)
        {
          $modelCourse = SrcCourses::find()->joinWith(['module0','students.studentLevel'])->orderBy(['module.code'=>SORT_ASC])
                         ->where(['old_new'=>1, 'academic_year'=>$acad,'shift'=>$shift, 'student_level.room'=>$room, 'program'=>$program])
                         ->all();
        }
        elseif($current_acad!=$acad)
        {
            $modelCourse = SrcCourses::find()->joinWith(['module0','students.studentLevelHistories'])->orderBy(['module.code'=>SORT_ASC])
                         ->where(['old_new'=>1, 'courses.academic_year'=>$acad,'courses.shift'=>$shift, 'student_level_history.room'=>$room, 'module.program'=>$program])
                         ->all();
        }
       return $modelCourse;    
       
        
         
    }    

public function getCourseInfoByShiftAcadProgram($shift,$acad,$program){
       
      $modelCourse = SrcCourses::find()->joinWith(['module0'])
                         ->where(['old_new'=>1, 'academic_year'=>$acad,'shift'=>$shift, 'program'=>$program])
                         ->all();
                          
       return $modelCourse;    
       
        
         
    }   
 
    
  public function getCourseInfoForTranscriptNotes($student,$program)  {
      $modelCourse = SrcCourses::find()->joinWith(['grades','module0'])
                         ->where(['student'=>$student, 'program'=>$program])
                         ->orderBy(['module.code'=>SORT_ASC])
                         ->all();
                          
       return $modelCourse;    
       
        
         
    
  }
     
     
    
public function getCourseByStudentID($student){
       
      $modelCourse = SrcCourses::find()->joinWith(['module0','studentHasCourses'])
                         ->where(['student'=>$student])
                         ->orderBy('module.code ASC')
                         ->all();
                          
       return $modelCourse;    
    
         
    }      
    
    
public function getCourseByStudentIDAcad($student,$acad){
       
      $modelCourse = SrcCourses::find()->joinWith(['module0','studentHasCourses'])->orderBy(['module.code'=>SORT_ASC])
                         ->where(['student'=>$student,'academic_year'=>$acad])
                         ->all();
                          
       return $modelCourse;    
    
         
    }      
    
    
    
public function getCourseByTeacherID($teacher){
       
    $acad=Yii::$app->session['currentId_academic_year'];  
    $modelCourse = SrcCourses::find()->joinWith(['academicYear','module0','room0','shift0'])
                         ->where(['academic_year'=>$acad,'old_new'=>1, 'teacher'=>$teacher])
                         ->orderBy(['date_end'=>SORT_DESC])
                         ->all();
                          
       return $modelCourse;    
    
         
    }  
    
     
public function getCourseByTeacherIDAcad($teacher,$acad){
      
      $acad = Yii::$app->session['currentId_academic_year'];
       
      $modelCourse = SrcCourses::find()->joinWith(['academicYear','module0','room0','shift0'])
                         ->where(['old_new'=>1, 'teacher'=>$teacher,'academic_year'=>$acad])
                         ->orderBy(['date_end'=>SORT_DESC])
                         ->all();
                          
       return $modelCourse;    
    
         
    }
        
     
public function sortByProgramShift($program,$shift)
     {     
        //$this->globalSearch = $params;
$acad=Yii::$app->session['currentId_academic_year'];
                       
		    $query = SrcCourses::find()->joinWith(['module0'])->where('academic_year='.$acad)->orderBy(['module.code'=>SORT_ASC]);
		    
		    if( ($program!=null) && ($shift!=null) )
              {
              	     $query = SrcCourses::find()->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','room0','shift0',])->orderBy(['module.code'=>SORT_ASC])->where(['academic_year'=>$acad,'program.id' => $program, 'shifts.id'=>$shift]);
              	     
              	}
             elseif( ($program!=null) && ($shift==null) )
               {
               	      $query = SrcCourses::find()->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','room0','shift0',])->orderBy(['module.code'=>SORT_ASC])->where(['academic_year'=>$acad,'program.id' => $program,]);
               	      
               	 }
              elseif( ($program==null) && ($shift!=null) )
                {
                	   $query = SrcCourses::find()->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','room0','shift0',])->orderBy(['module.code'=>SORT_ASC])->where(['academic_year'=>$acad,'shifts.id'=>$shift]);
                	    
                  }
		    
		    
		    $dataProvider = new ActiveDataProvider([
		        'query' => $query,
		        'pagination' => [
		            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
		        ],
		    ]);
		
		   // $this->load($params);
		
		    if (!$this->validate()) {
		        // uncomment the following line if you do not want to any records when validation fails
		        // $query->where('0=1');
		        return $dataProvider;
		    }
		
		  // $query->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','room0','shift0',]);  //le kou lye ak sal
		   $query->joinWith(['module0', 'module0.program0', 'module0.subject0', 'teacher0','shift0',]);
		   
		    		  
		    $query->orFilterWhere(['like', 'subjects.subject_name', $this->globalSearch])
		           ->orFilterWhere(['like', 'subjects.short_subject_name', $this->globalSearch])
		           ->orFilterWhere(['like', 'program.label', $this->globalSearch])
		            ->orFilterWhere(['like', 'persons.first_name', $this->globalSearch])
		            ->orFilterWhere(['like', 'persons.last_name', $this->globalSearch])
		            //->orFilterWhere(['like', 'rooms.room_name', $this->globalSearch])
		            ->orFilterWhere(['like', 'shifts.shift_name', $this->globalSearch]);
		            //->andFilterWhere(['like', '', x]);
		if( ($program!=null) && ($shift!=null) )
              {
              	     $query->andFilterWhere([ 'program.id' => $program, 'shifts.id'=>$shift ]);
              	}
             elseif( ($program!=null) && ($shift==null) )
               {
               	      $query->andFilterWhere([ 'program.id' => $program,]);
               	 }
              elseif( ($program==null) && ($shift!=null) )
                {
                	    $query->andFilterWhere([ 'shifts.id'=>$shift]);
                  }


		    return $dataProvider;
    }



 public function getAllTeachersByAcad($acad)
 {
     $query = SrcCourses::find()->joinWith(['teacher0',])
                                ->select(['teacher','persons.gender'])
                                ->groupBy(['teacher'])
                                ->where('old_new=1 AND academic_year='.$acad.' AND active in(1,2)')
                                ->all();
          
     return $query;
 }
 
 

    
    
}
