<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\CustomField */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>

   <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'field_name')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-6">
             <?= $form->field($model, 'field_label')->textInput(['maxlength' => true]) ?>
     </div>
  </div>
    
   <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'field_type')->widget(Select2::classname(), [
                       'data'=>$model->getFieldType(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select field type  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
         </div>
        
       
                        
<?php 
 if(isset($_GET['id']))
   {
     if($model->field_type === "combo") { ?>
       <div class="col-lg-6">
       <?= $form->field($model, 'field_option')->textArea(['maxlength' => true,'size'=>60,'maxlength'=>1024,'placeholder'=>Yii::t('app','Type each element of the list separate by a comma')]) ?>
       
		
        <?php 
       echo '     </div>
         </div>
        <div class="row">
        <div class="col-lg-6">';
     }
    else
      {
      	     echo ' <div class="col-lg-6">';
      	}
   } 
else
  {   
   ?>            

   
        <div class="col-lg-6">
<?php
    }   
   ?>     
   <?= $form->field($model, 'field_related_to')->widget(Select2::classname(), [
                       'data'=>$model->getPersonType() ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select field related to  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
   
     </div>
  </div>  
  
  <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

