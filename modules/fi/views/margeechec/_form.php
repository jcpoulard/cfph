<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\SrcProgram;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\MargeEchec */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="rooms-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
         <?php
               if(!isset($_GET['id']) )
                 {
        ?>
        <div class="col-lg-2">
            <?php  echo $form->field($model, 'all_programs')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()' ]); ?> 
        </div>
        
        <?php
        
                 }
                 
               if($model->all_programs==0)
                 {
        ?>
        
        <div class="col-lg-4">			
				
		<?php   echo $form->field($model, 'program')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
			                                                 // 'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>		
					
	</div>
           
            <?php
                   }
              ?>
        
        <div class="col-lg-3">
            <?= $form->field($model, 'quantite_module')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'borne_sup')->textInput(['maxlength' => true]) ?>
        </div>
         
    </div>
    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
       
    
    <?php ActiveForm::end(); ?>

</div>
