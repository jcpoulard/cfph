<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcMargeEchec */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Marge Echecs');

?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content marge-echec-index">
            
               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
                                            <th><?= Yii::t('app','Program'); ?></th>
				            <th><?= Yii::t('app','Quantite Module'); ?></th>
                                            <th><?= Yii::t('app','Borne Sup'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataMarge = $dataProvider->getModels();
    
          foreach($dataMarge as $marge)
           {
           	   echo '  <tr >
                                                                                                        
                                                    <td >'.$marge->program0->label.' </td>
                                                    <td >'.$marge->quantite_module.' </td>
                                                    <td >'.$marge->borne_sup.' </td>
                                                    
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                         if(Yii::$app->user->can('fi-margeechec-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', '../margeechec/update?id='.$marge->id, [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('fi-margeechec-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', '../margeechec/delete?id='.$marge->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>




</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Marge echec List'},
                  //  {extend: 'pdf', title: 'Marge echec List'},

                    
                ]

            });

        });

JS;
$this->registerJs($script);

?>

