<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\SrcProgram;


?>

<div class="courses-form">
   <?php $form = ActiveForm::begin(
            [
            'id' => $model->formName(),
            'enableClientValidation'=> false
            ]
            ); 
            
          
            
    ?>
    <div class="row">
        <table class="table table-striped table-bordered table-hover tablo">
            <thead>
            <th>#</th>
            <th><?= Yii::t('app','Program'); ?></th>
            <th><?= Yii::t('app','Quantite Module'); ?></th>
            <th><?= Yii::t('app','Borne Sup')?></th>
   
            </thead>
                
            <tbody>
                <?php 
                    for($i=1; $i<=10; $i++){
                ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td>
                    <?php   echo $form->field($model, 'program')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
			                                                 // 'onchange'=>'submit()',
                                                               'name'=>'program'.$i,
                                                               'id'=>'program'.$i,
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ])->label(false);
            ?>
                    </td>
                    <td>
                        <?= $form->field($model, 'quantite_module')->textInput(['maxlength' => true]) ?>
                    </td>
                    <td>
                         <?= $form->field($model, 'borne_sup')->textInput(['maxlength' => true]) ?>
                    </td>
            
                </tr>
                <?php 
                    }
                ?>
            </tbody>
        </table>
        
    </div>
    
       <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>
