<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\fi\models\GeneralConfig;
$email = infoGeneralConfig('email_portal_relay');
$host = infoGeneralConfig('email_host_relay');
$port = infoGeneralConfig('email_port_relay');

    ?>

<div class="col-lg-6">
         <h3><?= Yii::t('app','Mail setup')?></h3>
</div>

<div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>All form elements <small>With custom checbox and radion elements.</small></h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#">Config option 1</a>
                    </li>
                    <li><a href="#">Config option 2</a>
                    </li>
                </ul>
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
    <div class="ibox-content">
        
        
            <form method="post" class="form-horizontal">
                <div class="form-group"><label class="col-sm-2 control-label"><?= Yii::t('app','Email');?></label>

                    <div class="col-sm-10"><input type="text" class="form-control required" value="<?= $email; ?>" id="email"></div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group"><label class="col-sm-2 control-label"><?= Yii::t('app','Email host relay')?></label>
                    <div class="col-sm-10"><input type="text" class="form-control required" value="<?= $host; ?>" id="host"> 
                        <span class="help-block m-b-none">
                            <?= Yii::t('app','Host is provide by the email service provider. Example for gmail is : smtp.gmail.com');?>
                        </span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group"><label class="col-sm-2 control-label"><?= Yii::t('app','Password')?></label>

                    <div class="col-sm-10"><input type="password" class="form-control required" name="password" id="pass"></div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group"><label class="col-sm-2 control-label"><?= Yii::t('app','Port'); ?></label>

                    <div class="col-sm-10"><input type="text" class="form-control required" value="<?= $port; ?>" id="port">
                        <span class="help-block m-b-none">
                            <?= Yii::t('app','Port is provide by the email service provider. Example for gmail is : 465');?>
                        </span>
                    </div>
                </div>
                
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-4 col-sm-offset-2">
                        
                        <span class="btn btn-primary" id="modifye"><?= Yii::t('app','Update'); ?></span>
                    </div>
                </div>
            </form>
      </div>
</div>


<?php
    $baseUrl = Yii::$app->request->BaseUrl;
    
    
    $script = <<< JS
    $(document).ready(function(){
        $("#modifye").hide();
        $(".required").on("keypress keyup change", function () {
            var show_flag = true;
                    $('.required').each( function(i) {
                            if ($(this).val() == "") {
                                    show_flag = false;
                            } 
                    });
            if (show_flag){
                    $("#modifye").show();
            } else {
                    $("#modifye").hide();
            }
               
            });
          $('#modifye').click(function(){
                        var e_mail = $('#email').val();
                        var host = $('#host').val(); 
                        var port = $('#port').val();
                        var pass = $('#pass').val();    
                        $.post('$baseUrl/index.php/fi/setupmail/save-mail',
                            {
                            email:e_mail,
                            host:host,
                            port:port,
                            pass:pass
                            },function(data){
                                if(data==1){
                                        alert("Configuration de l'email reussi !");
                                    }else{
                                        alert("La configuration de l'email a echouee !");
                                        }
                            });
              });  
            
       });

JS;
$this->registerJs($script);

?>
