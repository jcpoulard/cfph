<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Program */

$this->title = Yii::t('app', 'Create Program');

?>

<div clas="row">


</div>

 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'pro'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
 </div>

<div class="wrapper wrapper-content program-create">
 <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
