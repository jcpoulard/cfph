<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\SrcSubjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subjects-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<div class="col-lg-7" style="float:left; margin-right:0px;padding:0px;">
  <div class="col-lg-7" style="float: left; margin-right:0px;padding:0px;">
    <?= $form->field($model, 'subject_name')->textInput(['placeholder'=> Yii::t('app', 'Subject Name'), 'class' => 'input form-control', 'style'=>'height:27px'])->label(false)  ?>
</div> 
<div class="col-lg-5"style="float: left; margin-right:-25px;padding:0px;">
    <?= $form->field($model, 'short_subject_name')->textInput(['placeholder'=> Yii::t('app', 'Short Subject Name'), 'class' => 'input form-control', 'style'=>'height:27px'])->label(false)  ?>

 </div>   
</div>    
    
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary fa fa-search']) ?>
        <!-- <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>  -->
   
    <?php ActiveForm::end(); ?>

</div>
