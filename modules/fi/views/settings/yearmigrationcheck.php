<?php

use yii\helpers\Html;


$this->title = Yii::t('app', 'Data migration from previous year.');

?>

<div class="row">
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content yearmigration-create">

    <?= $this->render('_yearmigrationcheck', [
        'model' => $model,
    ]) ?>

</div>







