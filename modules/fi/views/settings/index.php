<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
?>

<div class="animated fadeIn">
    <div class="row small">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs nav-tabs-sm">
                            <li class="active"><a data-toggle="tab" href="#academic-year"><?php echo Yii::t("app","Academic year"); ?></a></li>
                            <li class=""><a data-toggle="tab" href="#shift"><?php echo Yii::t("app","Shift") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#program"><?php echo Yii::t("app","Program") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#subject"><?php echo Yii::t("app","Subject") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#room"><?php echo Yii::t("app","Room") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#module"><?php echo Yii::t("app","Module") ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="academic-year" class="tab-pane active">
                                 
                                <div class="panel-body" style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="create?tabid=acad" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                   <p>
                                        <?php
                                        $pageSize=Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']); // set controller and model for that before
                                         Pjax::begin(['id'=>'academicperiod-c', 'timeout' => false, 'enablePushState' => false,  ]); ?> 
                                            <?= GridView::widget([
                                            'id'=>'academicperiod-grid',
                                                    'dataProvider' => $dataProviderAcad,
                                                    //'filterModel' => $srcModelAcad,
                                                    'summary'=>'',
                                                    'showFooter'=>true,
                                                    'showHeader' =>true,
                                                    'columns' => [
                                                        ['class' => 'yii\grid\SerialColumn'],

                                                       
                                                        'name_period',
                                                        'date_start',
                                                        'date_end',
                                                        
                                                        ['class' => 'yii\grid\ActionColumn',
                                                           'template'=>'{view}{update}{delete}',
															'buttons'=>[
												       
												    ],
                                                           'header'=>Html::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                       'onchange'=>"$.pjax.defaults.timeout = false; $.pjax.reload({container: '#academicperiod-c', data:{pageSize: $(this).val() }, url: $('#academicperiod-c li.active a').attr('href') })",
                    )),                        

                                                        
                                                        ],
                                                    ],
                                                ]); 
                                        ?>
                                        
                                        <?php  Pjax::end(); ?>
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="shift" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="create?tabid=shift" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                    <p>
                                        <?php Pjax::begin(); ?>  
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProviderShifts,
                                                 'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    
                                                    'shift_name',
                                                    'time_start',
                                                    'time_end',
                                                    
                                                    ['class' => 'yii\grid\ActionColumn'],
                                                ],
                                            ]); 
                                        ?>
                                        <?php Pjax::end(); ?>
                                    </p>
                                </div>
                            </div>
                            <div id="program" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                    <p>
                                        <?php Pjax::begin(); ?> 
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProviderProgram,
                                                
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    'label',
                                                    'description',
                                                    ['class' => 'yii\grid\ActionColumn'],
                                                ],
                                            ]); 
                                        ?>
                                        
                                        <?php Pjax::end(); ?>
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="subject" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                    <p>
                                        <?php Pjax::begin(); ?> 
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProviderSubjects,
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    'subject_name',
                                                    'short_subject_name',
                                                    ['class' => 'yii\grid\ActionColumn'],
                                                ],
                                            ]); 
                                        ?>
                                        <?php Pjax::end(); ?>
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="room" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                    <p>
                                        <?php Pjax::begin(); ?> 
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProviderRooms,
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    'room_name',
                                                    'short_room_name',
                                                    
                                                    ['class' => 'yii\grid\ActionColumn'],
                                                ],
                                            ]); 
                                        ?>
                                        <?php Pjax::end(); ?>
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="module" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                    <p>
                                        <?php Pjax::begin(); ?> 
                                            <?= GridView::widget([
                                                'dataProvider' => $dataProviderModule,
                                                'columns' => [
                                                    ['class' => 'yii\grid\SerialColumn'],
                                                    'subject',
                                                    'program',
                                                    'code',
                                                    'duration',
                                                    
                                                    ['class' => 'yii\grid\ActionColumn'],
                                                ],
                                            ]); 
                                        ?>
                                        <?php Pjax::end(); ?>
                                    </p>
                                    
                                </div>
                            </div>
                            
                        </div>


                    </div>
                </div>
               
            </div>
</div>