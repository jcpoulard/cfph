<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcMargeEchec;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcContactInfo;
use app\modules\fi\models\YearMigrationCheck;
use app\modules\rbac\models\form\Signup;
use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\PayrollSettingTaxes;



$acad=Yii::$app->session['currentId_academic_year'];
  

$SrcAcademicPeriods = new SrcAcademicperiods;
         $previous_year= $SrcAcademicPeriods->getPreviousAcademicYear($acad);
?>



  
    <div class="yearmigration-form">

<?php 
         $form = ActiveForm::begin(); 
    
   
        $all_step= ''; 
					                
   ////-1: migration not yet done; 1: migration is not completed; 2: migration done; 0: no migration to do 
							//migration check to display link 
							
							   $check= getYearMigrationCheck($previous_year);
							   $YearMigrationCheck = new YearMigrationCheck;
							
							$display = true;
							$no_result=false;
							
							if( ($check==-1) || ($check== 1) )
							  { //nap jere afichaj link lan pou 2 mwa
							      $start_date =''; // dat ane akademik lan t kreye
							      $nonb_de_jou =0;
							       
                                                              $data_migrationCheck = $YearMigrationCheck->getValueYearMigrationCheck($previous_year);
							        
							    
							         if($data_migrationCheck!=null)
							           {  foreach($data_migrationCheck as $d)
							               {
							    	         if($d['date_created']!='')
							    	            $start_date = $d['date_created'];
							    	            
							                  //postulant  
							                   if($d['postulant']==0)
							                   $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Postulant').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Postulant').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   //course  
							                   if($d['course']==0)
							                   $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Course').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Course').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   // marge_echec 
							                   if($d['marge_echec']==0)
							                   $all_step = $all_step. '<div > <div  style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Marge Echecs').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Marge Echecs').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   //fees 
							                   if($d['fees']==0)
							                   $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Fees').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Fees').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   //pending balance  
							                   if($d['pending_balance']==0)
							                   $all_step = $all_step. '<div > <div  style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Pending balance').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Pending balance').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                     
							    	          //taxes  
							                   if($d['taxes']==0)
							                   $all_step = $all_step. '<div > <div  style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Taxes').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                     $all_step = $all_step. '<div > <div  style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Taxes').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   //payroll_Settings 
							                   if($d['payroll_setting']==0)
							                   $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Payroll Settings').' <i class="fa fa-square-o" aria-hidden="true"></i> </div></div></div>';
							                  else
							                   $all_step = $all_step. '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Payroll Settings').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>';
							                   
							                   
							    	        
				 			                      
							                }
							           }
							           
							    
							       if($start_date!='')
							           $nonb_de_jou = date_diff ( date_create($start_date)  , date_create(date('Y-m-d') ))->format('%R%a');
								
								  if($nonb_de_jou > 90) //60 jou
							         {  $display = false;
							  	     	$message_year_migration= Yii::t('app','Migration is not allowed after 90 days start from the begining of the academic year.');
							  	     }
							  	     
							     
							  
							  }
							elseif($check==2)
							  { $display = false; 
							    
							  
							      $all_step = '<div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Postulant').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div> 
							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Course').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Marge Echecs').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Fees').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Taxes').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Pending balance').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
                                                                        							                 
							                 <div > <div style="float:left; margin-left:15px; width:auto;"> <div class="l" >'.Yii::t('app','Payroll Settings').' <i class="fa fa-check-square-o" aria-hidden="true"></i> </div></div></div>
							                 
							    
							    ';
							      
							      
							
							    
							  	$message_year_migration= Yii::t('app','All migration already done.');
							                          
							   }
							 elseif($check==0)
							  { $display = false; 
							     $no_result=true;
							  	$message_year_migration= Yii::t('app','No migration to do.');
							  	
							                          
							   } 
							
   
   ?>
 
    
      <div id="transac" class="" style="width:99%; background-color: #fff; min-height: 180px; padding: 5px;">
        
       <div class="box box-primary" style="  background-color: #e20f0f">
            <div class="  with-border " style=' padding:8px 0px 8px 0; margin-left:-7px;'>
                <h3 class="box-title" style='color: #D5F5DE'>
                    <?php 
                        echo "<span class='badge' style=' padding:5px 45px 5px 8px; background: #fff; color: #EE6539; font-size:16pt; font-weight: bold;'>".Yii::t('app','Warning')."</span>";
                    ?>
                </h3>
            </div>
        </div>     
            <div class="box-body" style='height: 330px; overflow: scroll'>
             La migration de données permet de transférer la plupart des données et configurations de l’année précédente vers la nouvelle année que vous venez de créer... <!-- Spécifiquement, les données qui seront migrées sont : Postulants, Cours, Marge d'echec, Paramètres payroll, Frais et Balance antérieure. Une fois migrées, toutes ces informations pourront être modifiées pour refléter la réalité de l’année en cours. C’est un processus assez important, il faut être prêt pour le faire. Donc, il faut absolument vérifier que :<br/>
1.	<b>Les décisions concernant ceux qui ont été postulés pour cette année soient prises</b>   <br/>
En exécutant le point 1, le logiciel saura migrer les postulants dans le programme et niveau respectif conformément aux décisions qui auront été prises. L’utilisateur devra par la suite les distribuer dans un groupe en utilisant la fonctionnalité « Groupes & Niveaux » dans le module de Education Initiale.
<br/>
2.	<b>Les décisions de fin d’année soient prises</b>   <br/>
En exécutant le point 2, le logiciel saura migrer les étudiants dans le niveau respectif conformément aux décisions qui auront été prises. L’utilisateur devra par la suite distribuer les étudiants dans un groupe en utilisant la fonctionnalité « Groupes & Niveaux » dans le module de Education Initiale.
-->
    <hr/>
  
 
            </div>
            
             <?php
                   if($no_result==false)
                    {
                ?>
                <div class="box-footer with-border" style=" font-size:9pt; font-weight: bold;">
               
	                <div class="alert alert-success" style="text-align: center; ">
	                    <strong><?php  echo $all_step.'.'; ?></strong>
	                </div>
                               
            </div>
            
             <?php
                    }
                ?>
                
        </div>
        
      

        
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
                   <?php 
               if($display==true)
                 {
              
                 echo Html::submitButton( Yii::t('app', 'Process'), ['name' =>'create', 'class' =>'btn btn-warning btn-large']);

              
                   } 
                else
                  {
              
                      echo $message_year_migration; 
                    
               
                    }
               
                  ?>
          
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

    <?php ActiveForm::end(); ?>

</div>

<?php 
    if(!empty($course_array)){
        //print_r($course_array[0]);
       // echo "SELECT * from course Where id IN (".implode(',',$course_array[0]).")";
        // A developper apres un rapport detaille de la migration des cours 
    }
?>