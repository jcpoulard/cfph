<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
   // use dosamigos\datepicker\DatePicker;
  
   // use kartik\date\DatePicker;
    use kartik\widgets\DatePicker;
    use kartik\widgets\TimePicker;
     
?>

<div class="animated fadeIn">
    <div class="row small">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs nav-tabs-sm">
                            <li class="<?php if(isset($_GET['tabid']) && $_GET['tabid']=='acad') echo "active"; ?>"><a data-toggle="tab" href="#academic-year"><?php echo Yii::t("app","Academic year"); ?></a></li>
                            <li class="<?php if(isset($_GET['tabid']) && $_GET['tabid']=='shift') echo "active"; ?>"><a data-toggle="tab" href="#shift"><?php echo Yii::t("app","Shift") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#program"><?php echo Yii::t("app","Program") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#subject"><?php echo Yii::t("app","Subject") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#room"><?php echo Yii::t("app","Room") ?></a></li>
                            <li class=""><a data-toggle="tab" href="#module"><?php echo Yii::t("app","Module") ?></a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="academic-year" class="tab-pane <?php if(isset($_GET['tabid']) && $_GET['tabid']=='acad') echo "active"; ?>">
                                 
                                <div class="panel-body" style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="index" class="btn btn-primary btn-sm"><i class="fa fa-arrow-left"></i> <?php  echo Yii::t("app","Back");?></a></div>
                                     <?php $form = ActiveForm::begin(); ?>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <?= $form->field($modelAcad, 'name_period')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-lg-4">
                                            <?= $form->field($modelAcad, 'weight')->textInput() ?>
                                        </div>
                                        <div class="col-lg-4" style="text-align: center">
                                            <label class="control-label" for="academicperiods-checked"><?php echo Yii::t('app','Checked') ?></label>
                                            <?= $form->field($modelAcad, 'checked')->checkbox(['label'=>null]) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <?php
                                                echo $form->field($modelAcad, 'date_start')->widget(DatePicker::classname(), [
                                                        'options' => ['placeholder' => ''],
                                                        'language'=>'fr',
                                                        'pluginOptions' => [
                                                            'autoclose'=>true
                                                        ]
                                                    ]);
                                            ?>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <?php
                                                echo $form->field($modelAcad, 'date_end')->widget(DatePicker::classname(), [
                                                        'options' => ['placeholder' => ''],
                                                        'language'=>'fr',
                                                        'pluginOptions' => [
                                                            'autoclose'=>true
                                                        ]
                                                    ]);
                                            ?>
                                            
                                        </div>
                                        <div class="col-lg-4" style="text-align: center">
                                            <label class="control-label" for="academicperiods-is-year"><?php echo Yii::t('app','Is year') ?></label>
                                            <?= $form->field($modelAcad, 'is_year')->checkbox(['label'=>null]) ?>
                                        </div>
                                    </div>
                                   
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <?= $form->field($modelAcad, 'previous_academic_year')->textInput() ?>
                                        </div>
                                        <div class="col-lg-4">
                                            <?= $form->field($modelAcad, 'year')->textInput() ?>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group">
                                            <?= Html::submitButton($modelAcad->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $modelAcad->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                        </div>

                                    <?php ActiveForm::end(); ?>
                                   <p>
                                       
                                   </p>
                                    
                                </div>
                            </div>
                            <div id="shift" class="tab-pane<?php if(isset($_GET['tabid']) && $_GET['tabid']=='shift') echo "active"; ?>">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    <?php $form = ActiveForm::begin(); ?>

                                            <?= $form->field($modelShift, 'shift_name')->textInput(['maxlength' => true]) ?>
                                          <?php   echo $form->field($modelShift, 'time_start')->widget(TimePicker::classname(), []); ?>
                                            <?= $form->field($modelShift, 'time_start')->textInput() ?>

                                            <?= $form->field($modelShift, 'time_end')->textInput() ?>
                                            <?= $form->field($modelShift, 'time_start')->textInput() ?>
                                            <?= $form->field($modelShift, 'date_created')->textInput() ?>

                                            <?= $form->field($modelShift, 'date_updated')->textInput() ?>

                                            <?= $form->field($modelShift, 'create_by')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($modelShift, 'update_by')->textInput(['maxlength' => true]) ?>

                                            <div class="form-group">
                                                <?= Html::submitButton($modelShift->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $modelShift->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                            </div>

                                    <?php ActiveForm::end(); ?>
                                    
                                </div>
                            </div>
                            <div id="program" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                   
                                    <p>
                                        
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="subject" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    
                                    <p>
                                        
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="room" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    
                                    <p>
                                        
                                    </p>
                                    
                                </div>
                            </div>
                            <div id="module" class="tab-pane">
                                <div class="panel-body"  style="padding-top: 2px;">
                                    
                                    <p>
                                        
                                    </p>
                                    
                                </div>
                            </div>
                            
                        </div>


                    </div>
                </div>
               
            </div>
</div>