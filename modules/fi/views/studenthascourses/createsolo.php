<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentHasCourses */


$student=$_GET['id'];

$modelPers = SrcPersons::findOne($student);
$this->title = Yii::t('app', 'Add courses to {student}',['student'=>$modelPers->getFullName()]).' : '.$modelPers->getFullName();

?>

<div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>
    
<div class="wrapper wrapper-content student-has-courses-createsolo">


    <?= $this->render('_solo', [
        'model' => $model,
        'dataProvider'=>$dataProvider,
        
        
    ]) ?>
    
     
</div>
