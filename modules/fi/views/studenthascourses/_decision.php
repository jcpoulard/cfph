<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\rbac\models\User;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
$acad = Yii::$app->session['currentId_academic_year'];

Yii::$app->session['tab_index']=0;
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<?php 
					

        
 $modelShift_ = new SrcShifts();
 $modelCourse = new SrcCourses();

 
	 $item_array_1= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                                                [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
                                                          [
			             //'attribute'=>'total',
						 'label'=>Yii::t("app","Total Module "),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->total;
							 }
						  ],
                                            
                                                          [
			             //'attribute'=>'total',
						 'label'=>Yii::t("app","Total Success"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->succes;
							 }
						  ],
                                                          
                                                          [
			             //'attribute'=>'total',
						 'label'=>Yii::t("app","Total Fail"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
                                                     
                                                      $acad=Yii::$app->session['currentId_academic_year']; 
                                                      $command= Yii::$app->db->createCommand('SELECT nbr_hold_module FROM student_level_history WHERE student_id='.$data->student.' AND academic_year='.$acad ); 
                                                      $data = $command->queryAll();
                                                      
                                                      $echec = null;
                                                      foreach($data as $d)
                                                      {
                                                          $echec= $d['nbr_hold_module'];
                                                      }
                                                      
							 return $echec;  //$data->echec;
							 }
						  ],
                                                          
                                                          
						  [
			             //'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Decision"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
                                                    $acad=Yii::$app->session['currentId_academic_year']; 
                                                    $program_ = Yii::$app->session['program_decision'];
                                                      $command= Yii::$app->db->createCommand('SELECT nbr_hold_module FROM student_level_history WHERE student_id='.$data->student.' AND academic_year='.$acad ); 
                                                      $data1 = $command->queryAll();
                                                      
                                                      
                                                      $echec = null;
                                                      foreach($data1 as $d)
                                                      {
                                                          $echec= $d['nbr_hold_module'];
                                                      }
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     //<input name="decision['.$data->student.']" type=text tabindex="'.$i.'" value="5" style="width:97%;" />
	$val ='<select name="decision['.$data->student.']" tabindex="'.$i.'" >'.decisionSelectedOption(true,$echec,$program_,NULL).'</select>
			          
					   <input name="id_stud['.$data->student.']" type="hidden" value="'.$data->student.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
  
 	
	
?>

<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>
      
      <div class="row">
 
          
        <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
           <?php
                    
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       	$data_shift = [];
		       
        
           ?>
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-shift").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-room").html("");
                                        	
                                        });',
                                     
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
          
        
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         
              if($model->shift[0]!='')

                {       $modelShift_ = new SrcShifts();
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                  }      
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&level='.'"+$("#srcstudenthascourses-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        $data_room = [];
              if($model->room[0]!='')
                {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                      //   ->all(),'room0.id','room0.room_name' );
                                           $from = 'grades';
                     $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift[0],$model->program,$model->level,$acad);
                  }      
                 ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                    
                                   'onchange'=>'submit()',
  
                       ],
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  
     
      </div>
       
</div>
    
<ul class="nav nav-tabs"></ul>  
<br/>  
<?php
//error message  

    /*	        
				  
        if($success)
             { 
                 $success=false;
                   Yii::$app->getSession()->setFlash('warning', [
                                                            'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                            'duration' => 36000,
                                                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                            'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
                                                            'title' => Html::encode(Yii::t('app','Warning') ),
                                                            'positonY' => 'top',   //   top,//   bottom,//
                                                            'positonX' => 'center'    //   right,//   center,//  left,//
                                                        ]);

              }

	*/			    
										
	        	
?>
 


<?php



  
  echo GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array_1, 
                          
       ]);

    
  

?>

<?php

if( ($dataProvider->getModels()!=null) )
 {
?>   
<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?php
           
         echo Html::submitButton(Yii::t('app', 'Validate'), ['name' => 'create', 'class' => 'btn btn-success']);
            
                                                       
        ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
         // echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

 <?php
 
 }
 
  
  ?>
    <?php ActiveForm::end(); ?>

</div>




<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    
        // e.keyCode = 37(left), 38(up), 39(right), 40(down)
        
  $('input').keypress(function(e){
   var key=e.keyCode || e.which;
       
   //alert("@-"+key);
        if( (key==13)||(key==40) ){ 
        var x = 1;
         var ind = $(this).attr("tabindex");  
          var next_ind =0;
             next_ind = Number(ind) + Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
        else if(key==38){  //up
        
            var x = 1;
            var ind = $(this).attr("tabindex");  
            var next_ind =0;
             next_ind = Number(ind) - Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
    });  		
				
	

JS;
$this->registerJs($script);

?> 



		   
			

	

