<?php

use yii\helpers\Html;

use app\modules\fi\models\SrcPersons;

use app\modules\fi\models\SrcCourses;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */


   $this->title = Yii::t('app', 'Validate Decisions');

?>

     
<div class="row">
    <div class="col-lg-1">
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['decisionlist'], ['class' => 'btn btn-info btn-sm']) ?>
    </div>
 
    <div class="col-lg-8">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>   
</div>    
<div class="wrapper wrapper-content decisions-validate">

    <?php
         
                echo $this->render('_decision', [
                 'model' => $model,
                 'dataProvider'=>$dataProvider, 
                 
             ]); 
          
                  
         
          
        ?>

</div>
