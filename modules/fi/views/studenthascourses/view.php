<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use yii\grid\GridView;
use yii\helpers\Url;

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcStudentHasCourses;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentHasCourses */


$modelPers = new SrcPersons;
 
 $acad = Yii::$app->session['currentId_academic_year'];
 
 
      	 $birthday = '0000-00-00';
     $image='';
    
   
      	 $person_id = $model->student0->id;
      	 $birthday = $model->student0->birthday;
      	 $image= $model->student0->image;
      	 $full_name = $model->student0->getFullName();
     



    
    

?>

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php  
                    
             if(isset($model->student0))
                       echo Html::a($model->student0->getFullName(), [ '/fi/persons/moredetailsstudent', 'id' => $model->student,'is_stud'=>1,'from'=>''], ['class' => '']).', ';                    
                      else
                        echo $full_name.', ';
                      echo Yii::t('app','Courses/Modules enrolled');  
                  ?>
    
    </h3>
    </div>

<?php
        
 
 ?>   

    <div class="col-lg-5">
        <p>
       
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'wh'=>'mod'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id, 'wh'=>'mod'], ['class' => 'btn btn-info btn-sm']) ?>
        
    </p>
    </div>

<?php
          
?>

</div> 




   

<div style="clear:both"></div>

<!-- La ligne superiure  -->
<div class="row">
   
     <div class="col-lg-9 payroll-view">
   
	 
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Course'); ?></th>
				            <th><?= Yii::t('app','Teacher'); ?></th>
				            <th><?= Yii::t('app','Program'); ?></th>
				            <th><?= Yii::t('app','Shift'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $searchModel = new SrcStudentHasCourses();
    
         $dataStudCourses = $searchModel->getCourseByIdStudent($model->student);
         
    
    
          foreach($dataStudCourses as $stud_course)
           {
           	   echo '  <tr >
                                                  ';                          ?>
                    <?php   echo '
                                                    <td >'.$stud_course->course0->module0->subject0->subject_name.' </td>
                                                    <td >'.$stud_course->course0->teacher0->fullName.' </td>
                                                    <td ><span data-toggle="tooltip" title="'.$stud_course->course0->module0->program0->label.'">'.$stud_course->course0->module0->program0->short_name.' </span></td>
                                                    
                                                    <td >'.$stud_course->course0->shift0->shift_name.'</td>';
                                                  //  <td >'.$stud_course->course0->room0->room_name.'</td>
                                                   echo ' <td >'; 
                                                            
                                                          
                                                  /*        if(Yii::$app->user->can('fi-studenthascourses-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', '../studenthascourses/update?id='.$stud_course->id, [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                     */
                                                              
                                                         if(Yii::$app->user->can('fi-studenthascourses-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', '../studenthascourses/delete?id='.$stud_course->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              } 
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                
	    
		
		</div>
		
		<div class="col-lg-2 text-center">
                                                    <br/><br/>    
                                                       <?php 
                                                        $program='';
                                                       if(isset($stud_course->student0->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $stud_course->student0->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($stud_course->student0->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($stud_course->student0->birthday).Yii::t('app',' yr old').' )';
                                                        }
					         	else
					         	  echo '';
                                                         ?>
                                                            </strong>
                                                        </span>
                                                        <br/>
                                                       
                                                            <?php if($stud_course->student0->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/".$stud_course->student0->image)?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>


                                                      <span><br/>
                                                            <strong>
                                                        <?php 
					         	  								echo Yii::t('app', 'Level').' : ';
					         	  								if(isset($stud_course->student0->studentLevel->level) )
					         	  								  echo $stud_course->student0->studentLevel->level;
					         	  						 ?>
					         	  						   </strong>
                                                        </span>

                             
                                                       
          </div>
    
</div>






  
 
 
 