<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use kartik\select2\Select2; 

use yii\bootstrap\Modal; 
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcStudentHasCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Has Courses');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 




   
<div class="wrapper wrapper-content student-has-courses">   


               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				           <!--      <th><?= Yii::t('app','Course'); ?></th>
				            <th><?= Yii::t('app','Teacher'); ?></th>    -->
				            <th><?= Yii::t('app','Program'); ?></th>
				            <th><?= Yii::t('app','Level'); ?></th>
                                            <th><?= Yii::t('app','Room'); ?></th> 
				            <th><?= Yii::t('app','Shift'); ?></th>
				        
				            				            
				           <!--      <th></th> -->
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataStudCourses = $dataProvider->getModels();
    
          foreach($dataStudCourses as $stud_course)
           {
           	   echo '  <tr >
                                                    
                                                    <td >';                          ?>
                    <a href="#" title="<?= Yii::t('app', 'View {stud} course',['stud'=>$stud_course->student0->getFullName()]);?>" class='popopKou' data-idstudent="<?= $stud_course->student; ?>"><span class="fa fa-book"></span></a>
                    <?php   echo '</td >
                                                    <td ><a style="color:#676A74;" href="../studenthascourses/view?id='.$stud_course->id.'&" >'.$stud_course->student0->first_name.' </a></td>
                                                    <td ><a style="color:#676A74;" href="../studenthascourses/view?id='.$stud_course->id.'&" >'.$stud_course->student0->last_name.' </a></td>';
                                                   // <td >'.$stud_course->course0->module0->subject0->subject_name.' </td>
                                                   // <td >'.$stud_course->course0->teacher0->fullName.' </td>
                                           echo '  <td ><span data-toggle="tooltip" title="'.$stud_course->course0->module0->program0->label.'">'.$stud_course->course0->module0->program0->short_name.' </span></td>
                                                    <td >';
                                                         if(isset($stud_course->student0->studentLevel->level))
                                                               echo Yii::t('app','Level').'-'.$stud_course->student0->studentLevel->level;
                                                   echo '</td>
                                                    <td >';
                                                         if(isset($stud_course->student0->studentLevel->room0->id))
                                                               echo $stud_course->student0->studentLevel->room0->room_name;
                                                   echo '</td>
                                                    <td >'.$stud_course->course0->shift0->shift_name.'</td>';
                                                  //  <td >'.$stud_course->course0->room0->room_name.'</td>
                                                  // echo ' <td >'; 
                                                            
                                                          
                                                  /*        if(Yii::$app->user->can('fi-studenthascourses-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', '../studenthascourses/update?id='.$stud_course->id, [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                     
                                                              
                                                         if(Yii::$app->user->can('fi-studenthascourses-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', '../studenthascourses/delete?id='.$stud_course->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              } 
                                                             */ 
                                           //  echo ' </td>';
                                                    
                                                echo '  </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>

<?php
//  3/05/2018, komante pou pa anpeche signout. a resourdre 
    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Student courses ').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md',
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            ]
            ); 
    echo '<div id="modalContent"><div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';
    
    Modal::end(); 
 //*/   
    ?>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Students-Courses List'},
                    {extend: 'pdf', title: 'Students-Courses List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>





<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(function(){
    $(document).on('click','.popopKou',function(){
        var idstudent = $(this).attr('data-idstudent');
        $.get('$baseUrl/index.php/fi/persons/viewcourse',{'id':idstudent},function(data){
           $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });
      
    });
    
});
JS;
$this->registerJs($script2);

?>

