<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentLevel;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentHasCourses */
/* @var $form yii\widgets\ActiveForm */

$acad = Yii::$app->session['currentId_academic_year'];

?>

 <input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div class="student-has-courses-form">

    <?php $form = ActiveForm::begin(); ?>
    
   <?php
   if(Yii::$app->user->can('data-migration'))
     {
  ?> 
    <div class="row">  
       
    <div class="col-lg-3">
            <?= $form->field($model, 'past_courses')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=>'submit()',]) ?>
        </div>
        
   </div>
    <br/>
  <?php
     }
  ?>   
    
    
    <div class="row">
        
 <?php
   if(Yii::$app->user->can('data-migration'))
     {
       if($model->past_courses==1)
        {
  ?> 
    <div class="col-lg-3">
            <?php        
         
             
	                    
	                   echo $form->field($model, 'academic_year')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcAcademicperiods::find()->where('is_year=1 AND id<>'.$acad)->all(),'id','name_period' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select academic year  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
			                                   
		      
            ?>
    
    </div>
        
   
  <?php
             }
     }
  ?>
            
        
        <div class="col-lg-3">
            
         <?php        
         // It's not clear for me I comment it 
         /*
         $data_room = [];
         if($model->room[0]!='')
          * 
          */
          
         echo $form->field($model, 'program')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'submit()', 
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                             
                         ],
                       ]) 
            ?>
        </div>
        
        <div class="col-lg-3">
            
         <?php        
         
               $data_level = ArrayHelper::map(SrcStudentLevel::find()->where('student_id='.$_GET['id'])->all(),'level','level' );
               
                if( ($model->past_courses==1)&&($model->academic_year!='') )
                  {  $command = Yii::$app->db->createCommand();
                     $command1= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$_GET['id'].' AND academic_year='.$model->academic_year ); 
                     $data = $command1->queryAll();
                  $level=null;
                     if($data!=NULL) 
                     {
                         foreach ($data as $d)
                         { $level= $d['level']; 
                         
                         }
                     }
                    $data_level = [$level];
                  }
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>$data_level, 
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>


           <div class="col-lg-3">
            <?php  echo $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcShifts::find()->joinWith(['studentOtherInfos'])->where('student='.$_GET['id'])->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshift?from=studenthascourses&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&level='.'"+$("#srcstudenthascourses-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
                       
                  
                 ?>

        </div>    
        
    
   <!--     <div class="col-lg-3">
            <?php 
      /*   $data_room = [];
              if($model->shift!='')

                {   
                  if(infoGeneralConfig('course_room')==1)  //swiv kou nan yon sel sal
	               {
                	 //sal li ladan deja
                	 $data_room = ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0','students.studentLevel'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program,'student_level.level'=>$model->level,'student_level.student_id'=>$_GET['id'] ])
                         ->all(),'room0.id','room0.room_name' );
                         
                    //si l poko nan sal, affiche tout sal ki nan shift li ye a
                      if($data_room==null)
                         {
                         	$data_room = ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program ])
                         ->all(),'room0.id','room0.room_name' );
                         }
                         
	                }
	             elseif(infoGeneralConfig('course_room')==0)  //swiv kou nan plizye sal
	                {
	                	$data_room = ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program ])
                         ->all(),'room0.id','room0.room_name' );
	                  }
	                  
	                  
                  }
                        
                  echo $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>'submit()',
                                     //'onchange'=>'
                                      //  $.post("../../fi/courses/courseinroom?from=studenthascourses&room='.'"+$(this).val()+"&shift='.'"+$("#srcstudenthascourses-shift").val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                     //   $("#srcstudenthascourses-course").html(data);	
                                        	
                                     //    });',
                                         

                       ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
                  */ ?>

    
        </div>   -->
           </div>
    
    

    <?php 
    //$modelCourse_ = new Courses::findOne();
    //echo $modelCourse_->fullCourse; 
   
   if(isset($_GET['id']))
   {
    ?> 
    
    <?php Pjax::begin(); ?>
    
    <?= GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				        
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['id'] ];
                                                 },
         ],
         
         [
             'attribute'=>'id',
			 'label'=>Yii::t("app","Course"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->module0->subject0->subject_name." (".$data->teacher0->getFullName().")";//." (".$data->room0->room_name." / ".$data->shift0->shift_name.")" ;
				 
				 }
			  ],
            
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
        <?php Pjax::end(); 
    
   }
    
    
//if( ( ( isset($dataProvider)&&($dataProvider->getModels()!=null) )&&($model->room!='') )  )
if( ( ( isset($dataProvider)&&($dataProvider->getModels()!=null) )  )  )
   {    
    ?>
    


    
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      
 <?php } ?>
    <?php ActiveForm::end(); ?>

</div>