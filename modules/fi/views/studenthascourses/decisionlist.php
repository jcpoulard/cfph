<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use app\modules\fi\models\SrcRooms;

use kartik\select2\Select2; 

use yii\bootstrap\Modal; 
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcStudentHasCourses */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List of decisions');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['decision'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Validate Decisions')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 




   
<div class="wrapper wrapper-content student-has-courses">   


               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				           <th><?= Yii::t('app','Level'); ?></th>
                                            <th><?= Yii::t('app','Room'); ?></th> 
				            <th><?= Yii::t('app','Comment'); ?></th>
                                            <th><?= Yii::t('app','Created By'); ?></th>
				            <th><?= Yii::t('app','Created Date'); ?></th>  
				            
				            
				        
				            				            
				           <!--      <th></th> -->
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataStudDecisions = $dataProvider->getModels();
    
          foreach($dataStudDecisions as $stud_decision)
           {
           	   echo '  <tr >
                                                    
                                                    
                                                    <td >'.$stud_decision['first_name'].'</td>
                                                    <td >'.$stud_decision['last_name'].' </td>
                                                    <td >'.$stud_decision['level'].'</td>
                                                    <td >'; $modelRoom = SrcRooms::findOne($stud_decision['room']);
                                              echo  $modelRoom->room_name.'</td>
                                                    <td >'.$stud_decision['comment'].'</td>
                                                    <td >'.$stud_decision['create_by'].'</td>
                                                    <td >'.$stud_decision['date_created'].'</td>';
                                                            
                                                          
                                                if(Yii::$app->user->can('fi-studenthascourses-rollbackdecision')) 
                                                  {
                                                    echo '<td >';
								 echo '&nbsp'.Html::a('<span class="fa fa-refresh"></span>', '../studenthascourses/rollbackdecision?id='.$stud_decision['student_id'], [
                                    'title' => Yii::t('app', 'Rollback decision'),
                        ]); 
                                                       echo ' </td>';      
                                                 }
                                                     
                                                              
                                                          
                                                              
                                           
                                                    
                                                echo '  </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Students-Decisions List'},
                   
                    
                ]

            });

        });

JS;
$this->registerJs($script);

?>



