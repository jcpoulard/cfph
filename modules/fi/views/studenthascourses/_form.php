<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentHasCourses */
/* @var $form yii\widgets\ActiveForm */

$acad = Yii::$app->session['currentId_academic_year'];

?>

 <input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div class="student-has-courses-form">

    <?php $form = ActiveForm::begin(); ?>
 
  <?php
   if(Yii::$app->user->can('data-migration'))
     {
  ?> 
    <div class="row">  
       
    <div class="col-lg-6">
            <?= $form->field($model, 'past_courses')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=>'submit()',]) ?>
        </div>
        
   </div>
    <br/>
  <?php
     }
  ?>  
    <div class="row">
        
  <?php
   if(Yii::$app->user->can('data-migration'))
     {
       if($model->past_courses==1)
        {
  ?> 
    <div class="col-lg-2">
            <?php        
         
            
	                    
	                   echo $form->field($model, 'academic_year')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcAcademicperiods::find()->where('is_year=1 AND id<>'.$acad)->all(),'id','name_period' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '--  select academic year  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
			                                   
		      
            ?>
    
    </div>
        
   
  <?php
             }
     }
  ?>
    
    <div class="col-lg-2">
            
         <?php        
             //return program_id or null value
             $program = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program==null)
		       {
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       }
		     else
		       {
		       	  $data_program = ArrayHelper::map(SrcProgram::find()->where('id='.$program)->all(),'id','label' );
		       	}
        
          
           if(isset($_GET['id'])&&($_GET['id']!=''))
		      {
		      	echo $form->field($model, 'program')->widget(Select2::classname(), [

                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'), 'disabled'=>'disabled',
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ;
		      }
		    else
		      {
                echo $form->field($model, 'program')->widget(Select2::classname(), [

                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ;
                       
		      }
		      
            ?>
        </div>
        
        <div class="col-lg-2">
            
         <?php        
         
             if(isset($_GET['id'])&&($_GET['id']!=''))
		      {
		      	echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'), 'disabled'=>'disabled',
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
		      }
		    else
		      {
	                    
	                   echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
			                                   
		      }
            ?>
        </div>


        <div class="col-lg-2">
            <?php
            
                if(isset($_GET['id'])&&($_GET['id']!=''))
			      {
			      	echo $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'), 'disabled'=>'disabled' ,
                                    'onchange'=>'submit()',
                                    /* 'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshift?from=studenthascourses&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });', 
                                        */
                                        
                                      
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ;
			      }
			    else
			      {
              
                    echo $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'submit()',
                                     /* '
                                       //chanje room
                                        $.post("../../fi/courses/roominshift?from=studenthascourses&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });',
                                     */
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ;
               
			      }        
                       
               ?>

        </div>
        
    
 <!--       <div class="col-lg-2">
            <?php 
  /*       $data_room = [];
             // if($model->room!='')
              //   {
            //    	$data_room = ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program])
            //             ->all(),'room0.id','room0.room_name' );
             //    }
              // else
                //  {
                //  	  $data_room = ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program])
              //           ->all(),'room0.id','room0.room_name' );
                  //	}      
                  	
                
      echo $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcCourses::find()->alias('c')->joinWith(['module0','room0'])->where(['c.shift'=>$model->shift,'module.program'=>$model->program])
                         ->all(),'room0.id','room0.room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=> '
                                        $.post("../../fi/courses/courseinroom?from=studenthascourses&room='.'"+$(this).val()+"&shift='.'"+$("#srcstudenthascourses-shift").val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-course").html(data);	
                                        	
                                         });', 
                                         

                       ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
                       
                       */
               ?>

    
        </div>  -->
 
 
        <div class="col-lg-2">
            <?php        
       
      $data_course = [];
      
            //if($model->course!='')
             // {    
              	   $modelCourse_ = new SrcCourses();
                 	   
                    //  if(!isset($_GET['id']))
	                //  {   
	             //       $data_course = $modelCourse_->getCourseinroom($model->room,$model->shift,$model->program,$acad); 
	                    
	               //   }
	               //else //update
	             //     {     //kou li poko pran nan sal la
	             //     	   $data_course = $modelCourse_->getCourseNottakenInroom($model->student,$model->room,$model->shift,$model->program,$acad);
	              //    	}
	                  	
                //  }
                          
            $modelCourse_ = new SrcCourses();    	
	 
            if($model->past_courses==0)
                 $data_course = $modelCourse_->getCourseinshift($model->shift,$model->program,$acad);             
             elseif($model->past_courses==1)
                 $data_course = $modelCourse_->getCourseinshift($model->shift,$model->program,$model->academic_year);             
         
                  
                 
                 
                  
      echo $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>$data_course,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    'onchange'=>'submit()', ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],


                       ]);
                 
      

         
                 
              ?>          
        </div>  
    </div>
    
    

    <?php 
    //$modelCourse_ = new Courses::findOne();
    //echo $modelCourse_->fullCourse; 
   
   if(!isset($_GET['id']))
   {
    ?> 
    
    <?php ?>
    
    <?= GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				        
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['student'] ];
                                                 },
         ],
         
	   [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Last Name"),
			 'format'=>'text', // 'raw', // 'html', //
			  'options' => ['width' => '700px'],
			 'content'=>function($data){
				 return $data->student0->last_name;
				 }
			  ],
            
              [
             'attribute'=>'student',
			 'label'=>Yii::t("app","First Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student0->first_name;
				 }
			  ],
	      
            
            [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Id Number"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student0->id_number;
				 }
			  ],
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
        <?php  
    
   }
    
    ?>
    

  <?php 
//if( ( ( isset($dataProvider)&&($dataProvider->getModels()!=null) )&&($model->room[0]!='') ) || (isset($_GET['id'])) )
if( ( ( isset($dataProvider)&&($dataProvider->getModels()!=null) )&&($model->course!='') ) || (isset($_GET['id'])) )
   {
   ?> 
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

 <?php } ?>
    
    <?php ActiveForm::end(); ?>

</div>