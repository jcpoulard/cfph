<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\SrcStudentOtherInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-other-info-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'student') ?>

    <?= $form->field($model, 'school_date_entry') ?>

    <?= $form->field($model, 'leaving_date') ?>

    <?= $form->field($model, 'previous_school') ?>

    <?php // echo $form->field($model, 'previous_program') ?>

    <?php // echo $form->field($model, 'apply_for_program') ?>

    <?php // echo $form->field($model, 'health_state') ?>

    <?php // echo $form->field($model, 'father_full_name') ?>

    <?php // echo $form->field($model, 'mother_full_name') ?>

    <?php // echo $form->field($model, 'person_liable') ?>

    <?php // echo $form->field($model, 'person_liable_phone') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_updated') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
