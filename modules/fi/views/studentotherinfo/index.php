<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcStudentOtherInfo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Student Other Infos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-other-info-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
   <div class="col-lg-12"style="float: left;">
    <div class="col-lg-9"style="float: left; padding-left:0px;">
       <?php  echo $this->render('_globalSearch', ['model' => $searchModel]); ?>
    </div>
 </div>   
    

<div class="panel-body" style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="create" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>
                                   <p>
           <?php
             $pageSize=Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']); // set controller and model for that before
               Pjax::begin(['id'=>'student-other-info', 'timeout' => false, 'enablePushState' => false,  ]); ?> 
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
          'showFooter'=>true,
          'showHeader' =>true,
          'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'school_date_entry',
            ['attribute'=>'apply_for_program',
               'value'=>'applyForProgram.label',
             ],
            
            ['attribute'=>'previous_program',
               'value'=>'previousProgram.label',
             ],
             
             ['attribute'=>'apply_shift',
               'value'=>'pplyShift.shift_name',
             ],
            'previous_school',
            
            
             
             
             'health_state',
            // 'father_full_name',
            // 'mother_full_name',
             'person_liable',
             'person_liable_phone',
            'leaving_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </p>
</div>
