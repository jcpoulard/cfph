<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;

use app\modules\fi\models\Persons;
use app\modules\fi\models\Program;
use app\modules\fi\models\Shift;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentOtherInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-other-info-form">

    <?php $form = ActiveForm::begin(); ?>

    
     <?= $form->field($model, 'apply_for_program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Program::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select apply program  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
     
     <?= $form->field($model, 'previous_program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Program::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select previous program  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                       
     <?= $form->field($model, 'apply_shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Shift::find()->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select apply shift  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
    <?=
         $form->field($model, 'school_date_entry')->widget(DatePicker::classname(), [
                'options'=>['placeholder'=>'' ],
                'language'=>'fr',
                'pluginOptions'=>[
                     'autoclose'=>true, ],
                
                ] )
               
    ?>

    <?= $form->field($model, 'previous_school')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'health_state')->textInput(['maxlength' => true]) ?>
    
    
    <?= $form->field($model, 'father_full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mother_full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_liable')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'person_liable_phone')->textInput(['maxlength' => true]) ?>
    
        

    <div class="form-group">
         <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-secondary']) ?>
        
        <?php //back button   
                                              $url=Yii::$app->request->referrer; //Yii::app()->request->urlReferrer;//(<-yii1)
                                             // $explode_url= explode("php",substr($url,0));
				             
                                              echo ' <a href="'.$url.'" class="btn btn-default"> Back </a>';
        
        ?>     </div>

    <?php ActiveForm::end(); ?>

</div>
