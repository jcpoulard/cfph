<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Module */

$this->title =$model->code.' - '.$model->subject0->subject_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    
</div>

<div class="row">
    <div class="col-lg-7">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
<p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'mod'], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', 'wh'=>'mod'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'mod'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
</p>


    </div>
</div>
<div class="module-view">
 

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'subject0.subject_name',
            'program0.label',
            'code',
            'duration',
            'in_use:boolean',
            
        ],
    ]) ?>

</div>
