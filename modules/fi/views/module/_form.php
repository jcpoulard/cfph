<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper; 
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\Subjects;
use app\modules\fi\models\Program;

use yii\bootstrap\Modal; 

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="module-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
       
          <div class="col-lg-4">
             <?php
                    if(isset($_GET['id_sub'])&&($_GET['id_sub']!=''))
                       {
                       	    $subject_id = preg_replace("/[^0-9]/","",$_GET['id_sub']);
                       	    
                       	    $model->subject = [$subject_id];
                       	 }
             ?>
             
            <?= $form->field($model, 'subject')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Subjects::find()->all(),'id','subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select subject  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ;
            ?>
         </div>
         <div class="col-lg-1" style="margin-left:-18px; margin-top:4px;">
         <br/>
          <?php 
            // echo '<a  onclick="addSubject()" class="btn btn-primary btn-sm" title= "'.Yii::t('app','Add subject').'" > <i class="fa fa-plus"></i></a>';
             ?>
          <?= Html::button('<i class="fa fa-plus"></i>', ['value' => Url::to(['../fi/subjects/create','wh'=>'mod','from'=>'mod']), 'title' => 'Add subject', 'class' => 'showModalButton btn btn-primary btn-sm']); ?>
        </div>
      
     <div class="col-lg-6">
            <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Program::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'duration')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>

<?php
    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Subjects').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md',
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            ]
            ); 
    echo '<div id="modalContent"><div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';
    
    Modal::end(); 
    
    ?>

