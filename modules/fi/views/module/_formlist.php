<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper; 
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\Subjects;
use app\modules\fi\models\SrcSubjects;
use app\modules\fi\models\Program;
use app\modules\fi\models\SrcProgram;

use yii\bootstrap\Modal; 

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Module */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="module-form">

    <?php $form = ActiveForm::begin(
            [
            'id' => $model->formName(),
            'enableClientValidation'=> false
            ]
            ); ?>
    
    <div class="row">
        <div class="row">
            <div class="col-lg-6">
            
            <?php
                   //return program_id or null value
		             $program = isProgramsLeaders(Yii::$app->user->identity->id);
				     if($program==null)
				       {
				       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
				       }
				     else
				       {
				       	  $data_program = ArrayHelper::map(SrcProgram::find()->where('id='.$program)->all(),'id','label' );
				       	}
		        
            ?>
                <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                        'id'=>'program',
                       'name'=>'program',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false) 
            ?>
            </div>
            <div class="col-lg-6">
           <div class="form-group bouton">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        </div>
        <table class="table table-striped table-bordered table-hover tablo">
            <thead>
                <tr>
                    <th>#</th>
                    <th>
                        <?=  Yii::t('app','Subject'); ?>
                    </th>
                    <th>
                        <?= Yii::t('app','Code'); ?>
                    </th>
                    <th>
                        <?= Yii::t('app','Duration') ?>
                    </th>
                </tr>
            </thead>
            <?php
                for($i=1; $i<=30; $i++){
                ?>
            <tr>
                <td><?= $i ?></td>
                <td>
                    <?= 
                        
                        $form->field($model, 'subject')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcSubjects::find()->all(),'id','subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select subject  --'),
                       'id'=>'subject'.$i,
                       'name'=>'subject'.$i,   
                           ],
                        
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false) ;
                         
            ?>
                </td>
                <td>
                    <?= $form->field($model, 'code')->textInput(['maxlength' => true,
                        'name'=>'code'.$i,
                        ])->label(false) ?>
                </td>
                <td>
                    <?= $form->field($model, 'duration')->textInput(['name'=>'duration'.$i])->label(false) ?>
                </td>
            </tr>
            <?php 
                    
                }
            
            ?>
        </table>
       
    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group bouton">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div> 

    <?php ActiveForm::end(); ?>

</div>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;

$script = <<< JS
      $('#program').change(function(){
        $('.bouton').show();
        $('.tablo').show();
        
   });
        
   $(document).ready(function(){
        $('.bouton').hide(); 
        $('.tablo').hide();
        
   });      
JS;
$this->registerJs($script);
?>
