<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcStudentLevel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Students Degree');

?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content student-level-index">
             
             <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Degree'); ?></th>
				            <th><?= Yii::t('app','Room'); ?></th>
				         <!--   <th><?= Yii::t('app','Status'); ?></th>  -->
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody> 
    
    <?php 


    $dataStudentLevel= $dataProvider->getModels();
    
          foreach($dataStudentLevel as $student_level)
           {
           	   echo '  <tr >
                                                    <td >'.$student_level->student->id_number.' </td>
                                                    <td >'.$student_level->student->first_name.' </td>
                                                    <td >'.$student_level->student->last_name.' </td>
                                                    <td >'.$student_level->level.' </td>
                                                    <td >';
                                                        if($student_level->room!=0)
                                                            echo $student_level->room0->room_name;
                                                        
                                                    echo ' </td>';
                                                   // <td >'.$student_level->getStatus().' </td>
                                                    
                                                  echo '  <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                     /*    if(Yii::$app->user->can('fi-studentlevel-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/fi/studentlevel/view?id='.$student_level->student_id, [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              */
                                                              
                                                         if(Yii::$app->user->can('fi-studentlevel-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/fi/studentlevel/update?id='.$student_level->student_id, [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('fi-studentlevel-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/fi/studentlevel/delete?id='.$student_level->student_id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>
  

</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Students Degree List'},
                    
                ]

            });

        });

JS;
$this->registerJs($script);

?>


