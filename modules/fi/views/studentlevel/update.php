<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentLevel */

$this->title = Yii::t('app', 'Update Level for '.$model->student->getFullName() );

?>

<div class="row">
    <div class="col-lg-7">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', ], ['class' => 'btn btn-primary btn-sm']) ?>
      <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
       
            </p>
    </div>
        
    </div>
<div class="wrapper wrapper-content student-level-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
