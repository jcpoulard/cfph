<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcRooms;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentLevel */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="student-level-form">

       <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-3">
            
         <?php        
            $name = $model->student->getFullName().' ('.$model->student->id_number.')';
                   
        echo $form->field($model, 'student_id')->widget(Select2::classname(), [
			                                   'data'=>[$model->student_id => $name ],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>''],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
         <div class="col-lg-3">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --')],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
         <div class="col-lg-3">
            
         <?php        
         
                   
        echo $form->field($model, 'room')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcRooms::find()->select(['id','room_name'])->all(),'id','room_name' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select room  --')],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

       <div class="col-lg-3">
            
         <?php        
         
                   
        echo $form->field($model, 'status')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'Opened'), '1'=>Yii::t('app', 'Hold'), '2'=>Yii::t('app', 'Closed')],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>'', 'disabled'=>'disabled'],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
 
 </div>
    
       
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

    <?php ActiveForm::end(); ?>


</div>
