<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\grid\CheckBoxColumn;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcProgram;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentLevel */
/* @var $form yii\widgets\ActiveForm */

 $modelShift_ = new SrcShifts();
  $modelCourse = new SrcCourses();

?>

<div class="student-level-form">

       <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
<div class="col-lg-2" style="margin-left:18px; padding:0px;">
              
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'submit()',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
            
       
      </div>
      
        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
            
         <?php        
                  $onchange = '';
                     if(infoGeneralConfig('first_year_use')==0)
                      $onchange ='submit()';
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
                                                                   'onchange'=>$onchange,
                                                               ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

         <div class="col-lg-2" style="margin-left:18px; padding:0px;">
            
         <?php        
                $modelRoom_ = new SrcRooms();    	
	    $data_room = $modelRoom_->getRoominshift($model->shift);
           
                   
        echo $form->field($model, 'room')->widget(Select2::classname(), [
			                                   'data'=>$data_room, // ArrayHelper::map(SrcRooms::find()->select(['id','room_name'])->all(),'id','room_name' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select room  --')],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
<!--       <div class="col-lg-2" style="margin-left:18px; padding:0px;">
            
         <?php        
         /*
                   
        echo $form->field($model, 'status')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'Opened'), '1'=>Yii::t('app', 'Hold'), '2'=>Yii::t('app', 'Closed')],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select status  --')],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
			    */
            ?>
        </div>
 -->
 </div>
    

    
    <?php Pjax::begin(); ?>
    
    <?= GridView::widget([
        'id'=>'students-level-list',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['id'] ];
                                                 },
         ],
           
		[
             'attribute'=>'student_id',
			 'label'=>Yii::t("app","Last Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->last_name;
				 }
			  ],
                                  
               [
             'attribute'=>'student_id',
			 'label'=>Yii::t("app","First Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->first_name;
				 }
			  ],
                                  
                [
             'attribute'=>'id_number',
			 'label'=>Yii::t("app","Id Number"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->id_number;
				 }
			  ],
		
            
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
        <?php Pjax::end(); 
    
  
  if(isset($dataProvider)&&($dataProvider->getModels()!=null))
  {  
    ?>
    


    
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

    <?php } ?>
    
    
    <?php ActiveForm::end(); ?>


</div>
