<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\EmployeeInfo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Employee Info',
]);

?>
<div class="row">
    
</div>
<div class="row">
    <div class="col-lg-7">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
<p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create',], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', ], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
</div>
<div class="wrapper wrapper-content employee-info-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
