<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\EmployeeInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'employee')->textInput() ?>

    <?= $form->field($model, 'hire_date')->textInput() ?>

    <?= $form->field($model, 'country_of_birth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'university_or_school')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_of_year_of_study')->textInput() ?>

    <?= $form->field($model, 'field_study')->textInput() ?>

    <?= $form->field($model, 'qualification')->textInput() ?>

    <?= $form->field($model, 'job_status')->textInput() ?>

    <?= $form->field($model, 'permis_enseignant')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'leaving_date')->textInput() ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_created')->textInput() ?>

    <?= $form->field($model, 'date_updated')->textInput() ?>

    <?= $form->field($model, 'create_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'update_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
