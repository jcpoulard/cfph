<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Courses */

$this->title = $model->courseName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="courses-view">

    <h4><?= Html::encode($this->title) ?></h4>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'module0.subject0.subject_name',
            //'teacher0.last_name',
            //'teacher0.first_name',
            //'room0.room_name',
            'academicYear.name_period',
            'shift0.shift_name',
            'weight',
            //'debase:boolean',
            //'optional:boolean',
            'passing_grade',
            
        ],
    ]) ?>

</div>
