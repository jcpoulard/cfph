<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\SrcPersons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persons-search">

    
    
    <?php 
    if(isset($_GET['id'])) $course = $_GET['id'];
    $form = ActiveForm::begin([
        'action' => ['viewstudent','id'=>$course],
        'method' => 'get',
    ]); ?>
    
    <div class="row">
        <div class="col-lg-5">
             <?= $form->field($model, 'studentsByCourse')->textInput(['placeholder'=> Yii::t('app', 'Search student'), 'class' => 'input form-control'])->label(false) ?>
        </div>
        <div class="col-lg-2">
            <?= Html::submitButton('<i class="fa fa-search"></i> '.Yii::t('app', 'Search'), ['class' => 'btn btn-info']) ?>
        </div>
        <div class="col-lg-5">
            
        </div>
        
    </div>

   

    <?php ActiveForm::end(); ?>

</div>
