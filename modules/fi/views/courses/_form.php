<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\Shifts;
use app\modules\fi\models\Rooms;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\Module;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Courses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-form">

    <?php $form = ActiveForm::begin();
    
    $display_course_weight = infoGeneralConfig('display_course_weight');  
    
     ?>
     
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'module')->widget(Select2::classname(), [
                       'data'=>loadModule(),//ArrayHelper::map(Module::find()->innerJoin('subjects')->all(),'id','subject0.subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select module  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
            ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'teacher')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcPersons::find()->where('is_student=0 and active in(1,2)')->all(),'id','fullName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select teacher  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>

        </div>
    </div>
    <div class="row">
      <!--  <div class="col-lg-6">
            <?php /* echo $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                       ;
                       */
            ?>
        </div>
      -->
        <div class="col-lg-6">
            <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Shifts::find()->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
            ?>
        </div>
    </div>
    <div class="row">
      <?php if($display_course_weight==1){  ?>  
        <div class="col-lg-6">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
      <?php }  ?>  
        
<!--        <div class="col-lg-6">
             <?php //echo $form->field($model, 'debase')->checkbox() ?>
        </div>
    </div>
   <div class="row">
        <div class="col-lg-6">
            <?php //echo $form->field($model, 'optional')->checkbox() ?>
        </div>  
--> 
        <div class="col-lg-6">
            <?= $form->field($model, 'passing_grade')->textInput() ?>
        </div>
   <div class="col-lg-6">
            <?= $form->field($model, 'optional')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;',]) ?>
        </div>
    </div>
    <?php // echo $form->field($model, 'academic_year')->textInput() ?>

    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

    <?php ActiveForm::end(); ?>

</div>
