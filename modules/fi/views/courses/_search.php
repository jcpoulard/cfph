<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\SrcCourses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="courses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    
    <?= $form->field($model, 'module') ?>

    <?= $form->field($model, 'teacher') ?>

    <?= $form->field($model, 'room') ?>

   <?php  echo $form->field($model, 'shift') ?>

  
 <div class="col-lg-7" style="float:left; margin-top:-10px;padding-left:0px;padding-right:0px;">
    <?= $form->field($model, 'globalSearch')->textInput(['placeholder'=> Yii::t('app', 'Search room').'('.Yii::t('app', 'Global Search').')', 'class' => 'input form-control', 'style'=>'height:27px'])->label(false) ?>
    </div>
    
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary fa fa-search']) ?>
        <!-- <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>  -->

    <?php ActiveForm::end(); ?>

</div>
