<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;

use app\modules\fi\models\Shifts;
use app\modules\fi\models\Rooms;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\Module;

?>

<div class="courses-form">
   <?php $form = ActiveForm::begin(
            [
            'id' => $model->formName(),
            'enableClientValidation'=> false
            ]
            ); 
            
 $display_course_weight = infoGeneralConfig('display_course_weight');           
            
    ?>
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                   'data'=>ArrayHelper::map(Shifts::find()->all(),'id','shift_name' ),
                   'size' => Select2::MEDIUM,
                   'theme' => Select2::THEME_CLASSIC,
                   'language'=>'fr',
                   'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                        'name'=>'shift',
                        'id'=>'shift',
                       ],
                   'pluginOptions'=>[
                         'allowclear'=>true,
                         ],
                       ]);
            ?>
        </div>
    <!--    <div class="col-md-6 col-lg-6 room">
            <?php /*  echo $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                           'name'=>'room',
                           'id'=>'room',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
                   */
            ?>
        </div>
    -->
    </div>
    
    <div class="row">
        <table class="table table-striped table-bordered table-hover tablo">
            <thead>
            <th>#</th>
            <th><?= Yii::t('app','Module'); ?></th>
            <th><?= Yii::t('app','Teacher'); ?></th>
   <?php if($display_course_weight==1){  ?>         
            <th><?= Yii::t('app','Weight')?></th>
   <?php }  ?>         
            <th><?= Yii::t('app','Passing Grade'); ?></th>
            </thead>
                
            <tbody>
                <?php 
                    for($i=1; $i<=30; $i++){
                ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td>
                    <?= $form->field($model, 'module')->widget(Select2::classname(), [
                       'data'=>loadModule(),//ArrayHelper::map(Module::find()->innerJoin('subjects')->all(),'id','subject0.subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select module  --'),
                           'name'=>'module'.$i,
                           'id'=>'module'.$i,
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(false); 
                    ?>
                    </td>
                    <td>
                        <?= 
                        $form->field($model, 'teacher')->widget(Select2::classname(), [
                           'data'=>ArrayHelper::map(SrcPersons::find()->where('is_student=0 and active in(1,2)')->all(),'id','fullName' ),
                           'size' => Select2::MEDIUM,
                           'theme' => Select2::THEME_CLASSIC,
                           'language'=>'fr',
                           'options'=>['placeholder'=>Yii::t('app', ' --  select teacher  --'),
                               'name'=>'teacher'.$i,
                               'id'=>'teacher'.$i,
                               ],
                           'pluginOptions'=>[
                                 'allowclear'=>true,
                             ],
                       ])->label(false); ?>
                    </td>
           <?php if($display_course_weight==1){  ?>  
                    <td>
                        <?= $form->field($model, 'weight')->textInput(['name'=>'weight'.$i])->label(false); ?>
                    </td>
          <?php }  ?>  
          
                    <td>
                        <?= $form->field($model, 'passing_grade')->textInput(['name'=>'passing_grade'.$i])->label(false); ?>
                    </td>
                </tr>
                <?php 
                    }
                ?>
            </tbody>
        </table>
        
    </div>
    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4 bouton">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;

$script = <<< JS
      $('#shift').change(function(){
       //$('.room').show();
       
        $('.bouton').show();
        $('.tablo').show();
        
        
   });
   
  // $('#room').change(function(){
  //      $('.bouton').show();
  //      $('.tablo').show();
  //      }
  //     );
        
   $(document).ready(function(){
      //  $('.room').hide();
        $('.bouton').hide(); 
        $('.tablo').hide();
        
   });      
JS;
$this->registerJs($script);
?>