<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\JobStatus */

$this->title = Yii::t('app', 'Create Job Status');

?>
<div clas="row">
<?= $this->render("//layouts/settingsLayout") ?>

</div>
<p></p>
 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'job'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content job-status-create">

       <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
