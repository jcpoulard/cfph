<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\DatePicker;

use app\modules\fi\models\SrcGeneralConfig;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\SrcGeneralConfig */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="generalconfig-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'disabled'=>true]) ?>

        </div>
        
         <div class="col-lg-4">
            <?= $form->field($model, 'item_value')->textInput(['maxlength' => true]) ?>

        </div>
        
        <div class="col-lg-4">
            <?= $form->field($model, 'description')->textArea(['maxlength' => true,'size'=>60,'placeholder'=>Yii::t('app','Description')]) ?>
       
        </div>
        
     <!--   <div class="col-lg-4">
           <?= $form->field($model, 'english_comment')->textArea(['maxlength' => true,'size'=>60,'placeholder'=>Yii::t('app','Description')]) ?>
       

        </div>
 -->
    </div>
    
       

    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
