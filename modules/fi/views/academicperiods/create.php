<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Academicperiods */

$this->title = Yii::t('app', 'Create academic year');

?>

 <?php
    if( (!isset($_GET['from']))||($_GET['from']!='gol') )
      {
 ?>       
 
<div clas="row">
<?= $this->render("//layouts/settingsLayout") ?>

</div>
 <?php
      }
 ?>       
 
<p>
    
</p>
 <div class="row">
 <?php
    if( (!isset($_GET['from']))||($_GET['from']!='gol') )
      {
 ?>       
         <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,'wh'=>'ap'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
  <?php
      }
 ?>       
 
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>
    
<div class="wrapper wrapper-content academicperiods-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
