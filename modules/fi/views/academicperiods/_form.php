<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\DatePicker;

use app\modules\fi\models\Academicperiods;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Academicperiods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="academicperiods-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name_period')->textInput(['maxlength' => true]) ?>

        </div>
 <?php
    if( (!isset($_GET['from']))||($_GET['from']!='gol') )
      {
 ?>       
        <div class="col-lg-6">
            <?= $form->field($model, 'is_year')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=>'submit()',]) ?>
        </div>
 <?php
      }
 ?>  
    </div>
    
        <div class="row">
        <div class="col-lg-6">
            <?php
                echo $form->field($model, 'date_start')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        </div>
        <div class="col-lg-6">
            <?php
            echo $form->field($model, 'date_end')->widget(DatePicker::classname(), [
                'options'=>['placeholder'=>'' ],
                'language'=>'fr',
                'pluginOptions'=>[
                     'autoclose'=>true, 
                     'format'=>'yyyy-mm-dd',   
                    ],
                
                ] );
               
            ?>
        </div>
        </div>
        <div class="row">
<?php
    if( (!isset($_GET['from']))||($_GET['from']!='gol') )
      {
      	if( $model->is_year !=1 )
      	  {
 ?>       
           
        <div class="col-lg-6">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
<?php
      	  }
      }
?>        
        <div class="col-lg-6">
             <?= $form->field($model, 'previous_academic_year')->dropDownList(ArrayHelper::map(Academicperiods::findAll(['is_year'=>1]),'id','name_period' ), [ 'prompt'=> Yii::t('app', ' --  select previous year  --')] ) ?>
        </div>
        </div>
    
    <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
