<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;









$validated_by = '';
$date_validation = '';

  $id_teacher='';   
      
//            $program= Yii::$app->session['grades_program'];
//	        $shift= Yii::$app->session['grades_shift'];
//	        $room = Yii::$app->session['grades_room'];
	       // $course = Yii::$app->session['grades_course'];
				
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div class="wrapper wrapper-content grades-form">

    <?php $form = ActiveForm::begin(); ?>
    
    
<?php   
 $modelShift_ = new SrcShifts();
  $modelCourse = new SrcCourses();
    
 /* 
    if( (Yii::$app->user->can("teacher")))
       {
       	   ?>
		<div class="b_m">

<div class="row" style="padding: 5px 10px;"> 
     
      <div class="span9" >	
			<!--evaluation-->
			<div class="span2" >
			    <label for="Evaluation_name"><?php echo Yii::t('app','Evaluation Period'); ?></label><?php //echo $form->labelEx($model,'Evaluation Period'); ?>
			           <?php 
					
					        $modelEvaluation= new EvaluationByYear();
							    if(isset($this->evaluation_id)&&($this->evaluation_id!=''))
							       echo $form->dropDownList($modelEvaluation,'evaluation',$this->loadEvaluation($acad_sess), array('onchange'=> 'submit()','options' => array($this->evaluation_id=>array('selected'=>true)))); 
							    else
								  { 
									echo $form->dropDownList($modelEvaluation,'evaluation',$this->loadEvaluation($acad_sess), array('onchange'=> 'submit()')); 
						           }					      
						  
						echo $form->error($modelEvaluation,'evaluation'); 
						
															
					   ?>
			</div>
       
		
		  
			<!--room-->
			<div class="span2" >
                           
			<?php  $modelRoom = new Rooms;
			                     echo $form->labelEx($modelRoom,Yii::t('app','Room')); ?>
			          <?php 
					
					     
						    
						  
		                           if(isset($this->room_id)&&($this->room_id!=''))
							   { 
						          echo $form->dropDownList($modelRoom,'room_name',$this->loadRoomByIdTeacher($id_teacher,$acad_sess), array('onchange'=> 'submit()','options' => array($this->room_id=>array('selected'=>true)) )); 
					             }
							   else
							      { echo $form->dropDownList($modelRoom,'room_name',$this->loadRoomByIdTeacher($id_teacher,$acad_sess), array('onchange'=> 'submit()')); 
							          
									 $this->room_id=0;
							      }	
		                    
		                    
							      
						echo $form->error($modelRoom,'room_name'); 
						
										
					   ?>
			</div>

			
			<!--Courses-->
			<div class="span2" >
			   
					  
					 <?php $modelCourse = new Courses;
					  ?></label><?php 
					        echo $form->labelEx($modelCourse,Yii::t('app','Course'));
					        
						
						if(isset($this->room_id)&&($this->room_id!='')){
						   	   
					 	    if(isset($this->course_id))
						        {   
					               echo $form->dropDownList($modelCourse,'subject',$this->loadSubjectByTeacherRoom($this->room_id,$id_teacher,$acad_sess), array('onchange'=> 'submit()','options' => array($this->course_id=>array('selected'=>true)) )); 
					             }
							  else
								{ $this->course_id=0;
								    echo $form->dropDownList($modelCourse,'subject',$$this->loadSubjectByTeacherRoom($this->room_id,$id_teacher,$acad_sess),array('onchange'=> 'submit()')); 
								}
								
                          
						  }
						else
							echo $form->dropDownList($modelCourse,'subject',$this->loadSubject(null,null,$acad_sess),array('onchange'=> 'submit()'));
						 
						echo $form->error($modelCourse,'subject'); 
						
					
					  ?>
					  
			   </div>
		</div>
													   

<br/><br/>

  <div  style="float:left; width:100%;" >     
<?php
       
       
   	  if(($this->course_id!='')&&($this->evaluation_id!=''))
 	    $dataProvider=$model->searchForTeacherUserRoomCourse($condition1, $this->evaluation_id,$this->course_id, $acad_sess);
 	  else
 	    $dataProvider=$model->searchForTeacherUserRoomCourse($condition1, 0,0, $acad_sess);
   	
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
   $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'grades-grid',
	'dataProvider'=>$dataProvider,
       
       'mergeColumns'=>array('s_full_name','evaluation0.examName'),
	
	'columns'=>array(
		array('header'=>Yii::t('app','Student name'),'name'=>'s_full_name',
                    'type' => 'raw','value'=>'CHtml::link($data->student0->fullName,Yii::app()->createUrl("academic/persons/viewForReport",array("id"=>$data->student0->id,"pg"=>"lr","isstud"=>1,"from"=>"stud")))',
                    'htmlOptions'=>array('style'=>'vertical-align: top'),
                        
                    ),
            
                array('name'=>'evaluation0.examName','header'=>Yii::t('app','Exam name'),'htmlOptions'=>array('style'=>'vertical-align: top')),    
                    
		
		'grade_value',
                'course0.weight',
                      
          array('header'=>Yii::t('app','Com. '),'name'=>'comment',
                    'type' => 'raw','value'=>'($data->comment == null) ? " " : "<span data-toggle=\"tooltip\" title=\"$data->comment\"><i class=\"fa fa-comment-o\"></i> </span>"',
                ),
   
            array('header' =>Yii::t('app','Validate'),
	         'name'=>'validate',
	        'value'=>'$data->validateGrade'
			),
		 	
	array('header' =>Yii::t('app','Publish'),
	         'name'=>'publish',
	        'value'=>'$data->publishGrade'
			),
                
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'',
			'buttons'=>array (
        'update'=> array(
            'label'=>'<i class="fa fa-pencil-square-o"></i>',
            'imageUrl'=>false,
            'url'=>'Yii::app()->createUrl("/academic/grades/update?id=$data->id&from=stud")',
            'options'=>array( 'title'=>Yii::t('app','Update') ),
        ),
        
        'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),

         'view'=>array(
            'label'=>'View',
            
            'url'=>'Yii::app()->createUrl("/academic/grades/view?id=$data->id&from=stud")',
            'options'=>array( 'class'=>'icon-search' ),
        ),
        
    ),
			
			'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                                  'onchange'=>"$.fn.yiiGridView.update('grades-grid',{ data:{pageSize: $(this).val() }})",
                    )),
		),
	),
)); 
          $content=$dataProvider->getData();
	        if((isset($content))&&($content!=null)) 
			   $this->renderExportGridButton($gridWidget, '<i class="fa fa-arrow-right"></i>'.Yii::t('app',' CSV'),array('class'=>'btn-info btn'));
		
   ?>
	

   </div>

<?php  
  
   }  
else
   {	    
 */
 ?>

	 <div class="row">
        <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

     <div class="col-lg-2" style="margin-left:18px; padding:0px;">
      <?php
                    //return program_id or null value
             $program = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program==null)
		       {
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       	
		       	     if(isset($model->program[0]))
		       	       $program = $model->program[0];
		       	     else
		       	        $program = 0; 
		       	$data_shift = [];
		       }
		     else
		       {
		       	  $data_program = ArrayHelper::map(SrcProgram::find()->where('id='.$program)->all(),'id','label' );
		       	  
		       	 // Yii::$app->session['grades_program'] = $program; 
		       	  
		       	    
                  $data_shift = $modelShift_->getShiftinprogram($program,$acad);
		       	  
		       	  
		       	}
        
           ?>
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-shift").html(data);	
                                       //chanje course 
                                        $("#srcgrades-room").html("");
                                        	
                                        });',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         
              if($model->shift[0]!='')

                {      
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                  }      
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-room").html(data);	
                                       //chanje course 
                                        $("#srcgrades-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        $data_room = [];
              if($model->room[0]!='')
                {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                        // ->all(),'room0.id','room0.room_name' );
                        
                     $from = 'grades';
                     $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift[0],$model->program,$model->level,$acad);
                  }      
                 ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>'submit()',], /*'
                                        $.post("../../fi/courses/courseinroomforgrades?from=grades&room='.'"+$(this).val()+"&shift='.'"+$("#srcgrades-shift").val()+"&program='.'"+$("#srcgrades-program").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-course").html(data);	
                                        	
                                        });',
  
                       ],  */
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  <?php  //       $data_course = [];
         //     if($model->course[0]!='')
        //        {  $modelCourse_ = new SrcCourses();
                  /*	$data_course = ArrayHelper::map(SrcCourses::find()->joinWith(['module0'])->where(['room'=>$model->room,'shift'=>$model->shift,'program'=>$model->program])
                         ->all(),'id','module0.subject0.subject_name' );
                   */
               
                   
           //     $data_course = $modelCourse_->getCourseinroomforgrades($model->room[0],$model->shift[0],$model->program[0],$acad);
          //        }      
                 ?> 
     
      </div>
<!--       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?php /*   echo $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>$data_course,//ArrayHelper::map(Courses::find()->all(),'id','module0.subject0.subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    'onchange'=>'submit()', ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]);
                       */
                        ?>       
          
           </div>
 -->
    </div>
    
<ul class="nav nav-tabs"></ul>  
<br/>  


<?php
if($model->room[0]!=null)
{
      $level = $model->level[0];
?>
  <div style="float:left; width:100%;" >
		
       							

			<?php 	
			  $stud_order_desc_average = array();
				$max_average = 0;
				$max_average_ = 0;
				$min_average = 0;
				$success_f = 0;
				$success_m = 0;
				$average_base = 0;


                
			  // $person= new Persons;
			   $tot_stud=0;
			   $showButton=false;
			   $total_succ =0;
			   $total_fail=0;
			   $passing_grade='N/A';
			   $classAverage='N/A';
			   $gen_done=false;  
			   
			   $stud_has_courseModel = new SrcStudentHasCourses();
			   
			   $include_discipline=0;
			/*   //Extract 
			   $include_discipline = infoGeneralConfig("include_discipline_grade");
			   //Extract max grade of discipline
			    $max_grade_discipline = infoGeneralConfig('note_discipline_initiale');
 	     
			   $include_discipline_comment=Yii::t('app',"Behavior grade is included in this average.");
		     */
		     
	 if($room!='')
		{	//tcheke si gen not pou sal sa deja
			if(infoGeneralConfig('course_shift')==0)
	          { //$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	              $sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN student_has_courses shc ON(c.id=shc.course) INNER JOIN student_level sl ON(sl.student_id=shc.student) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND sl.level='.$level.' AND sl.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	          }
	        elseif(infoGeneralConfig('course_shift')==1)
	           { //$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.shift='.$shift.' AND c.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	              $sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN student_has_courses shc ON(c.id=shc.course) INNER JOIN student_level sl ON(sl.student_id=shc.student) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.shift='.$shift.' AND sl.level='.$level.' AND sl.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	          }
															 
			$command__ = Yii::$app->db->createCommand($sql__);
			$result__ = $command__->queryAll(); 
			
		 if($result__!=null) 
		  {														       	   
			foreach($result__ as $r)
			 {  if($r['id']!=null) 
			      $gen_done=true;
			       break;
			  }
		   }
						  
         if($gen_done)
		   {	 //totall etudiant ki nan sal sa kap swiv kou sila
			 $dataProvider_studentEnrolled= $stud_has_courseModel->getStudentsEnrolledGroup($room, $level, $shift, $acad);
			  if(isset($dataProvider_studentEnrolled))
			    { 
				   foreach($dataProvider_studentEnrolled as $tot)
				       $tot_stud=$tot->total_stud;
				}
			 
			 //moyenne de la classe
			 //$classAverage=$this->getClassAverage($this->room_id, $acad_sess);
			 
			 //total reussi, et echoue
			    //note de passage du cours
			 /*     $modelCourse = new SrcCourses();
			    $modelCourse=SrcCourses::findOne($course); 
			       $passing_grade =  $modelCourse->passing_grade;//note de passage pour le cours
			 */
			     			    
			   //Extract average base
			  $average_base = infoGeneralConfig('average_base');
								   				
								   				 
			 $dataProvider_studentEnrolledInfo = $stud_has_courseModel->getInfoStudentsEnrolledGroup($room, $level, $shift, $acad); 
			 
			/* if(isset($dataProvider_studentEnrolledInfo))
			    {  
			    	foreach($dataProvider_studentEnrolledInfo as $stud)
				      {
				      	  //moyenne yon elev
				      	 $averag=$this->getAverageForAStudent($stud->student_id, $room, $course);
				      	   
				      	 $stud_order_desc_average[$stud->student_id]= $averag;
				      	 
				      	 
				      	 if($passing_grade!='')
						  {
							   if($averag>=$passing_grade)
					      	    {  $total_succ ++;
					      	        
					      	        if($stud->gender == 1)
								        $success_f++;
								     elseif($stud->gender == 0)
								         $success_m++;
								         	   
					      	      }
					      	   elseif($averag<$passing_grade)
					      	      $total_fail ++;
				      	   
						   }
						 else
						   {   if($averag>=($average_base/2)) 
						         {  $total_succ ++;
					      	        
					      	        if($stud->gender == 1)
								        $success_f++;
								     elseif($stud->gender == 0)
								         $success_m++;
								         	   
					      	      }
					      	   elseif($averag<($average_base/2))
					      	      $total_fail ++;
						    }
				      	   				      	  
				      }
				   }
			     */
			     
			     
			//$min_average et $max_average_ 
		//	   arsort($stud_order_desc_average);
			     // get the first item in the $stud_order_desc_average
		//	     $max_average = reset($stud_order_desc_average);
			     
			     // get the last item in the $stud_order_desc_average
		//	     $min_average = end($stud_order_desc_average);
			
			 
			
			    $moduleGrade_ = new SrcGrades(); 
			
			$dataProvider = $moduleGrade_->getAllSubjectsByLevel($program,$shift,$model->level[0],$room);
			
				
				
				//Grades for the current period
										 							   				   
												  
				//liste des etudiants
			 $dataProvider_student = $stud_has_courseModel->getInfoStudentsEnrolledGroup($room, $level, $shift, $acad);
			 
		
		  }
			 
			 
			 
		}	
              $couleur=1;
			  $k=0;
			 //
/*		echo '<div class="row-fluid"  style=" floaft: left; vertical-align:bottom; padding-bottom:0px;  ">';
		 echo '<div class="span8" style="float:left; font-size: 0.9em; padding: 0px;  margin-bottom: -14px; ">
			      
			       
			   <table class="responstable" style="width:100%; height: 49px; background-color: #c9d4dd; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
					   <tr>
					   
					   <td style="text-align:center;">'.Yii::t('app','Students Enrolled').' : '.$tot_stud.'</td>
					       <td style="text-align:center; ">'.Yii::t('app','Passing Average').' : '; if($passing_grade!='') echo $passing_grade; else echo 'N/A'; echo'</td>
					       <td style="text-align:center; ">'.Yii::t('app','Max average').' : '.$max_average.'</td>
					       <td style="text-align:center; ">'.Yii::t('app','Min average').' : '.$min_average.'</td>
					       <td style="text-align:center; ">'.Yii::t('app',' Success(F)').' : '.$success_f.'</td>
					       <td style="text-align:center; ">'.Yii::t('app',' Success(M)').' : '.$success_m.'</td>
					       <td style="text-align:center; ">'.Yii::t('app',' Total Success').' : '.$total_succ.'</td>
					    </tr>
						</table>
				</div>
			      
			       
			       ';
	
	
			   
				echo '<div class="span4"  style=" color:blue; text-align: center; background-color: #F8F8C9;   padding-left:10px; padding-right:40px; padding-top:5px; padding-bottom:5px;  margin-left=-3px;">';

				  if(($room!=''))
					{   
					        echo Yii::t('app','- Weak grades are in <span style="color:red">Red</span>, <span style="color:black">Black</span> grades aren\'t validated yet. -');
					  					  }
					  else 
					    {
					    	 
							        echo '.<br/>';
							   
						
					    	}  
				 // echo '</div>';
					echo '</div>';
     
	echo '</div >';
*/			
				   
	echo '<div style="margin-top:0px;  margin-bottom: 20px; float:left;   width:100%;  overflow-x:scroll; overflow-y:hidden;  background-color:#EFF4F8;">  
	
	       <table class="" style="  width:100%; background-color:#EFF4F8; color: #1E65A4;">
  
      <thead class="" >
           <tr style="background-color:#E4E9EF;">
				    <th style="background-color:#E4E9EF; width:170px; color:#E4E9EF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. </th>
				    <th style="background-color:#E4E9EF; width:170px; color:#E4E9EF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. </th>
				<!--	<th class="" style="background-color:#E4E9EF; width:40px;">.</th>   -->
				          ';
		
              		//liste des cours
		while(isset($dataProvider[$k][0]))
				{
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {  $id_course = $dataProvider[$k][4];
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
							  if($old_subject_evaluated)
								 {
								 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
									}
		
						 }
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
						}
						
						         
			      
			      if($careAbout)
				     {			
						
				// $dataProvider[$k][1] subject_name
				// $dataProvider[$k][3] short_subject_name
                                  // $dataProvider[$k][6] teacher_name
				 
				 echo ' <th style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;">'.'<span data-toggle="tooltip" title="'.$dataProvider[$k][1].' ( '.$dataProvider[$k][6].' )"> '.$dataProvider[$k][5].'</span></th>';//<-code module  //.$dataProvider[$k][3].'</span></th>';
				   
				      }
				      
				      $k++;
		        }
echo '	
     </tr>
  
  <tr style=""> 
     <th style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:170px;">'.Yii::t('app',"First Name").' </th>
     <th style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:170px;">'.Yii::t('app',"Last Name").' </th>
	<!-- <th class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:40px;">'.Yii::t('app',"Average").' </th> -->
    ';   
    
		
		//coefficients
		$k=0;   		
		
		while(isset($dataProvider[$k][0]))
				{
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {  $id_course = $dataProvider[$k][4];
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
							  if($old_subject_evaluated)
								 {
								 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
									}
		
						 }
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
						}
						
						        
			  
			      if($careAbout)
				     {			
							 echo '<th class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px ">'.$dataProvider[$k][2].'</th>';
														
					}
					
					$k++;
		  }
 echo '</tr>
	     	
	   	</thead >
	   	
	   	<tbody class="">
          
  	';
  	
  	$style_tr="";
  	
  	    //pou chak elev
			if($gen_done)		       
			  {	if(isset($dataProvider_student)&&($dataProvider_student!=null))
			                 {  
				                 $students=$dataProvider_student;
								
				             foreach($students as $stud)    //foreach($stud_order_desc_average as $stud)
				              {  
				              	 $k=0;
				              	 
							    if($couleur===1)  //choix de couleur																
									 $style_tr="font-size: 1em; background-color: #F0F0F0; height:28px; padding-left:15px;";
								 elseif($couleur===2)
									$style_tr="font-size: 1em; background-color: #FAFAFA; height:28px; padding-left:15px;";
									
							     echo '<tr style="'.$style_tr.'">
								            <td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; width:170px; "> 
				  								<b>'.$stud->first_name.'</b></td>
				  								
				  							<td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; width:170px; "> 
				  								<b>'.$stud->last_name.'</b></td>';
				  								
				  								
								/*	$average_ = 0;//$moduleGrade_->getAverageForAStudent($stud->id, $room);
										
									 echo '<td class="" style="text-align:center; width:40px; border-right: 3px solid  #E4E9EF; "><b>';
									   	   
									   	    if($average_ < $passing_grade)  echo '<div style="color:red;">'.$average_.'</div>';
											 else echo '<div >'.$average_.'</div>';
											 
											 echo '</b> </td>';
                                     */
									   	 	
										
									 
											  while(isset($dataProvider[$k][0]))
				                                {
	                                               if($dataProvider[$k][4]!=NULL) //reference_id
										 	         {  $id_course = $dataProvider[$k][4];
														 //si kou a evalye pou peryod sa
														$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
														  if($old_subject_evaluated)
															 {
															 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
															  }
														   else
															 {  
															 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
																}
									
													 }
												 else
													{  $id_course = $dataProvider[$k][0];
													    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
													}
													
													         
			  
			                                      if($careAbout)
				                                     {			
							                           
							                            //calculer grade puis afficher
							                                    $debase_passingGrade=0;
							                            $debase='';
							                             $weight='';
							                             $id_course=0;
							                             $passing_value=0;
							                             
							                              if($dataProvider[$k][4]!=NULL) //reference_id
												 	         {  $id_course = $dataProvider[$k][4];
																 //si kou a evalye pou peryod sa
																$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($id_course);         
																  if($old_subject_evaluated)
																	 {
																	 	$id_course = $dataProvider[$k][4];						                        
																	  }
																   else
																	 {  
																	 	$id_course = $dataProvider[$k][0];						                       															                       
																		}
											
															 }
														 else
															{  $id_course = $dataProvider[$k][0];
															    
															}
							                            
							                            
							                              	//note de passage du cours
														      $modelCourse = new SrcCourses();
														    $modelCourse=SrcCourses::findOne($id_course); 
														       $passing_value =  $modelCourse->passing_grade;//note de passage pour le cours
																  
															$grades= $moduleGrade_->searchForReportCard($stud->id,$id_course);
															 
																	if(isset($grades))
																		{
														                        $r=$grades ;//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade)
																			      {
																			        echo '<td style="text-align:center; width:100px; border-right: 1px solid  #E4E9EF">';
																			        echo '<div class="" style="';		
			     	                                                              echo '">';
																			        if($grade->grade_value!=null)
																			           { //date_format($date,'Y-m-d H:i:s')
																			           	  $date_validation = Yii::$app->formatter->asDateTime($grade->date_validation);
																			           	  $validated_by = $grade->validated_by;
																			           	  
																			           	  echo '<div class="" style="';		
			     	                                                                        if($grade->validate==0)
																			           	      echo 'color:black;">';
																			           	    elseif($grade->grade_value< $passing_value )
																			           	     { 
																			           	        echo 'color:red;">';
																			           	      }
																			           	    elseif($grade->validate==1)
																			           	      echo 'color:green;">';
																			           	      
																			           	    	 echo '<span data-toggle="tooltip" title="'.$grade->comment.'"> '.$grade->grade_value.'</span>';
																			           	    	 
																			           	   echo '</div>';
																			           	     
																			           	    
																			           }
																		           	 else
																			           {  echo '<div class="" style="';		
			     	                                                                        if($grade->validate==0)
																			           	      echo 'black;">';
																			           	    elseif($grade->grade_value< $passing_value)
																			           	     { 																			           	       										 echo 'color:red;">';
																			           	      }
																			           	    elseif($grade->validate==1)
																			           	      echo '">';
																			           	   
																			           	         echo 'N/A';
																			           	         
																			           	   echo '</div>';
																			           	   
																			             } 
                                                                                     
                                                                                      echo '</div>';
                                                                                      
                                                                                     echo '</td>';
                                                                                                                                                        
                                                                                                                                                   
															                        }
																	             }
																                else
																                  { echo '<td style=" text-align:center;  border-right: 1px solid  #E4E9EF ">';echo '<div class="" style="';		
			     	                                                                       /// if($grade->validate==0)
																			           	   
																			           	      echo '">';
																			           	   
																			           	         echo 'N/A';
																			           	         
																			           	   echo '</div>'; 
																			           	   
																			           	echo '</td>';
																                  }
																	$showButton=true;
																	
																         }//fin if(isset($grades))
															
														}//fin careAbout
														
														$k++;
					                    
				                                }//fin while(isset($dataProvider[$k][0]))								  
									   $couleur++;
	                                if($couleur===3) //reinitialisation
				                       $couleur=1;
								echo '</tr>';
								}// fin foreach($students as $stud)
							  
						//class average
				    /*		    echo '<tr ><td></td></tr> <tr style="'.$style_tr.'"><td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; text-align:right;  width:170px; ">'.Yii::t('app','Class Average').' </td>';
                            echo ' <td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; text-align:center; width:170px; ">'; echo $classAverage;   echo '</td>';
						   
						   
								//liste des cours
								$k=0;
								while(isset($dataProvider[$k][0]))
										{
											 $id_course = $dataProvider[$k][0];
											 
										  if($dataProvider[$k][4]!=NULL) //reference_id
										 	         {  $id_course = $dataProvider[$k][4];
														 //si kou a evalye pou peryod sa
														$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($id_course);         
														  if($old_subject_evaluated)
															 {
															 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
															  }
														   else
															 {  $id_course = $dataProvider[$k][0];
															 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
																}
									
													 }
												 else
													{  $id_course = $dataProvider[$k][0];
													    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
													}
													
													         
									
										  if($careAbout)
											 {			
												
										 echo ' <td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; text-align:center; width:170px; ">';echo '<span data-toggle="tooltip" title="'.$dataProvider[$k][1].'"> course_average($id_course)</span>';echo '</td>';
										   
											  }
											  
											  $k++;
										}
						echo '	
							 </tr>';						
							  
							  */
							  
							  
							  
							  }// fin if(isset($dataProvider_student))
							  
							  
			          }
							  
                    echo '
                    
                     </tbody>
                          
                           </table>
                    
               </div>     
                          
                          
                           '; 	  
				   				  
		?>		  
	</div>			   	

<br/>
<?php if( ($gen_done)&&($dataProvider_student!=null) ) { ?>
<div class="row">
    
     <?php
         if( Yii::$app->user->can('fi-grades-validate')||Yii::$app->user->can('fi-grades-pdfgrade') )
                   {
    ?> 
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
           
           <?php
                 if( Yii::$app->user->can('fi-grades-validate') )
                   echo Html::submitButton( Yii::t('app', 'Validate'), ['name' => 'validate_publish', 'class' => 'btn btn-warning']);
                 
                 if( Yii::$app->user->can('fi-grades-pdfgrade') ) 
                   echo '&nbsp&nbsp'.Html::submitButton( Yii::t('app', 'View PDF'), ['name' => 'viewPDF', 'class' => 'btn btn-warning']);
               ?>
       <!-- <?= Html::submitButton( Yii::t('app', 'Validate & Publish'), ['name' => 'validate_publish', 'class' => 'btn btn-warning']) ?>
        
        <?= Html::submitButton( Yii::t('app', 'Validate'), ['name' => 'validate', 'class' => 'btn btn-warning']) ?>
        
        <?= Html::submitButton( Yii::t('app', 'Publish'), ['name' => 'publish', 'class' => 'btn btn-warning']) ?>  -->
       
      
     
                    </div>
           
             
        </div>
       
        <div class="col-lg-4">
            
        </div>
  <?php
                     
            }
  ?>  
    <div class="col-lg-8">
           <div class="form-group">
               <?=  Yii::t('app', 'Validated by').': '.$validated_by.',  '.Yii::t('app', 'Date/Time').': '.$date_validation ?>
           
            </div>
         </div>
         
         
    </div>
    
    



 

<?php 

}
   
 }
 
  //}//fen !='Teacher'	
 

     
?>
  <?php ActiveForm::end(); ?>
</div>

 
 
 <div style="clear:both;"></div>
 
