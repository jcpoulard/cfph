<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\widgets\ActiveForm;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcGrades;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrPlans */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "";
		    	 
$current_acad = Yii::$app->session['currentId_academic_year'];

$programModel = SrcProgram::findOne($program);
$acadModel = SrcAcademicperiods::findOne($acad);			


$premye_tou=0;
$tot_stud=0;
$stud_with_nonvalidated_grade=0;

$include_course_withoutGrade = infoGeneralConfig('include_course_withoutGrade');
$administration_signature_text = infoGeneralConfig('administration_signature_text');

//Extract average base
$average_base = infoGeneralConfig('average_base');

foreach($studentsSelected as $id)
 {
	$tot_stud++;
	
	$all_validated=false;
	$student=(int)$id;
	$modelStud = SrcPersons::findOne($student);
	

//check if all grades are validated for this stud
   $modelGrades=new SrcGrades();
   //1: tout not valide,  0: gen not ki pa valide
	$not_all_validated=$modelGrades->if_all_grades_validated($student,$acad);
									
	$grade_total=0;
  	$weight_total=0;
  	$average ='';
	

   
if($not_all_validated==1)
  {
  	
 if($premye_tou!=0)
  {
  	 echo '<pagebreak />  
           <br/>';
  	}
 else
   { $premye_tou=1; 
      echo '<br/> '; 
   }	
  	
  	
  	     	
?>
<br/><br/>                    	          	
<div class="content">
	<div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
					<span> <?php  echo strtr( strtoupper( Yii::t('app', 'Competency statements') ), capital_aksan() );   ?> 
						                 
					</span>
				</h4>
		   	</div>
		</div>
	
		<div class="row">
			<div class="container">
				<table class="table table-striped table-hover" width="100%" >
				  <tbody>
				    <tr>
				     <td colspan="5" style="background-color:#A79C9E; text-align:center; " ><strong> <?= $programModel->label ?> </strong>  </td>
				     
				  </tr>
				  <tr>
				     <th style="font-size:10px;  background-color:#F3F3F4;"> <?=  Yii::t('app', 'First name & Last name') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Degree') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Shift') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Year') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Printing date') ?> </th>
				  </tr>
			<?php  
			
			
			?>	  
				  <tr>
				     <td style="font-size:10px; "> <?= $modelStud->getFullName()  ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $level ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $modelStud->studentOtherInfo0[0]->applyShift->shift_name ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $acadModel->name_period ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  Yii::$app->formatter->asDate(date('Y-m-d') ) ?></td>
				  </tr>
				  
				 </tbody>  
			   </table>
			   
			   
			   <table class="table table-striped table-hover" style=" width:100%" >
				  <tbody> 
				  <tr >
				     <th style=" width:45%; font-size:10px;  background-color:#F3F3F4;" > <?=  Yii::t('app', 'Competency label') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Duration') ?> </th>
				     <th style=" width:13%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Passing Grade') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Grade') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Mention') ?> </th>
				  </tr>
			<?php  
                        
                         $complete_integer = 0;
                         $partialCourseFailed=0;
                         
                        $modelStudLev = SrcStudentLevel::findOne($modelStud->id);
                                                          
                          //$level = $modelStudLev->level;
                          
                         $modelStudCourse = new SrcStudentHasCourses;
                         $modelC = new SrcCourses;
                       
                        $allCourses = $modelC->getCourseInfoByShiftRoomAcadProgram($shift,$room,$acad,$program); 
                        
                       if($level==2)  //($modelStud->studentLevel->level==2)
                       {
                           
                               $complete_integer = $modelStudCourse->isCycleCompleted($student);
                               
                               if($complete_integer==1)
                                  $allCourses = $modelC->getCourseByStudentID($student); 
                               else
                                   $allCourses = $modelC->getCourseByStudentIDAcad($student,$acad);//getCourseInfoByShiftRoomAcadProgram($shift,$room,$acad,$program); 
                       }
                       
                             $total_rows = sizeof($allCourses);
			     $rows_counter=0;
                             
			     foreach($allCourses as $course)
			      { 
                                 if($course->optional==0)
                                 {
			         //gade si evaluation fet pou omwens 1moun
                                $is_evaluated= isCourseEvaluated($course->id,$level);
                                        
                               if($is_evaluated==true)
                                 {
                                    $modelGradeInfo = $modelGrades->getGradeInfoByStudentCourse($modelStud->id,$course->id);
			            
			            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($modelStud->id,$course->id);
			        
			       if($include_course_withoutGrade==0)
			        {
				        if(isset($modelGradeInfo->grade_value) ) //si li pa konpoze pou kou a pa metel nan kane a
				         {
                                            $rows_counter ++;
                                            
                                            if($rows_counter == 23)
                               { 
			
			?>
                                      <tr >
				     <th style=" width:45%; font-size:10px;  background-color:#F3F3F4;" > <?=  Yii::t('app', 'Competency label') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Duration') ?> </th>
				     <th style=" width:13%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Passing Grade') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Grade') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Mention') ?> </th>
				  </tr>
                     <?php             
			      }
				         	
				         	$grade_total= $grade_total + $modelGradeInfo->grade_value;
  	                         $weight_total= $weight_total + $course->weight;
			?>	  
				  <tr >
				     <td style=" font-size:10px;"> <?= $course->module0->subject0->subject_name  ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->code ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->duration ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->passing_grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelGradeInfo->grade_value ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			<?php
			                      if($modelIspass==0)
			                         $partialCourseFailed++;
                                              
                                             
			              }
			              
			         }
			       elseif($include_course_withoutGrade==1)
			          {  
                                   $rows_counter ++; 
                                   
                                   if($rows_counter == 23)
                               { 
			
			?>
                                      <tr >
				     <th style=" width:45%; font-size:10px;  background-color:#F3F3F4;" > <?=  Yii::t('app', 'Competency label') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Duration') ?> </th>
				     <th style=" width:13%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Passing Grade') ?> </th>
				     <th style=" width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Grade') ?> </th>
				     <th style=" width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Mention') ?> </th>
				  </tr>
                     <?php             
			      }
                                   
			          	  $grade= '-';
			          	 if(isset($modelGradeInfo->grade_value) )
			          	   { $grade= $modelGradeInfo->grade_value;
			          	     $grade_total= $grade_total + $modelGradeInfo->grade_value;
			          	    }
			          	   
  	                         $weight_total= $weight_total + $course->weight;
			     ?>
			       <tr >
				     <td style=" font-size:10px;"> <?= $course->module0->subject0->subject_name  ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->code ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->duration ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->passing_grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			   <?php      
			                        if($modelIspass==0)
			                            $partialCourseFailed++;
                                                
                                                	
			          	}
			      
                                  }//gen evalyasyon fet pou kou a
                                  
                                  if($rows_counter==22)
                                  {
                                  ?>    
				      </tbody>	   
		                      </table>
                                     
                           
		<div class="row">
			<div class="Container">
		    	<br/> <br/> <br/>  <br/>   
                        <h4 class="content-title" style="text-align:center;">
					<span> <?php  echo strtr( strtoupper( Yii::t('app', 'Competency statements') ), capital_aksan() );   ?> 
						                 
					</span>
				</h4>
                            
		   	</div>
		</div>
                                        <table class="table table-striped table-hover" width="100%" >
				  <tbody>
				    <tr>
				     <td colspan="5" style="background-color:#A79C9E; text-align:center; " ><strong> <?= $programModel->label ?> </strong>  </td>
				     
				  </tr>
				  <tr>
				     <th style="font-size:10px;  background-color:#F3F3F4;"> <?=  Yii::t('app', 'First name & Last name') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Degree') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Shift') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Year') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Printing date') ?> </th>
				  </tr>
				  
				  <tr>
				     <td style="font-size:10px; "> <?= $modelStud->getFullName()  ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $modelStud->studentLevel->level ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $modelStud->studentOtherInfo0[0]->applyShift->shift_name ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $acadModel->name_period ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  Yii::$app->formatter->asDate(date('Y-m-d') ) ?></td>
				  </tr>
                          
				  
				 </tbody>  
			   </table>
			  
			   
			   <table class="table table-striped table-hover" style="page-break-inside: auto; width:100%" >
				  <tbody> 
			
                         <?php     
                               
                               }
                               
                                 
                               
                             
                          }
                          
                          
  }
			      
			?>	  
			       <tr >
			          <td colspan="6" style=" font-size:10px; border-bottom: 1px solid white;">
			    
		<?php 
                                if($rows_counter==22)
                                    echo '<br/><br/>';

                          //$old_status = $modelStudLev->status;

                           switch ($level)
                            {
                               case 1:  $next_level = 2;
                                        $status =0;

                                        break;
                               case 2:  $next_level = 2;
                                        $status =1;

                                        break;

                               case 3:  $next_level = 3;
                                        $status =1;

                                        break;

                             }
                             
                             
                              //Extract transcript_note_text
			    $remarque = $transcript_note_text = infoGeneralConfig('transcript_note_text');
                             
                              if($level==2)
                                 $remarque = $reportcard2_note_text = infoGeneralConfig('reportcard2_note_text');                              
                             
                echo $remarque.'<br/>';
                
                    
                
                       if($level==1)
                       {
                            echo '<b>'.decisionSelectedOption(false,$partialCourseFailed,$programModel->id,NULL).'</b>';
                               
                              // $complete_integer = 0;
                       } 
                 
                        
                      if($level==1)
                       {  if($partialCourseFailed==0)
			    $complete_integer=1;
                        else
                           $complete_integer=0;
                       }  
                        
                        //voye done nan tab student_level_history
                        $is_pass =0;
                        $next_level = NULL;
                        $status = NULL;
                        
                        if(decisionSelectedOption(false,$partialCourseFailed,$programModel->id,'set')==1)
                           $is_pass =1;
                       elseif(decisionSelectedOption(false,$partialCourseFailed,$programModel->id,'set')==0)
                          $is_pass =0;
                        
                       
                          
                          $command= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$modelStud->id.' AND academic_year='.$acad ); 
                          $data = $command->queryAll();
                             
                          $command1 = Yii::$app->db->createCommand();
                          
                          if($data==NULL) 
                           {                          
                              $command1->insert('student_level_history', [
                                                'student_id'=>$modelStud->id,
                                                'program'=>$program,
                                                'shift'=>$shift,
                                                'level'=>$level,
                                                'room'=>$room,
                                                'status'=>$status, //$old_status,
                                                'nbr_hold_module'=>$partialCourseFailed,
                                                'is_pass' => $is_pass,
                                                'academic_year'=>$acad,
                                                'create_by'=>currentUser(),
                                                'date_created'=>date('Y-m-d'),

                                            ])->execute();
                           }
                         elseif($data!=NULL)
                         {
                             $command1->update('student_level_history', ['nbr_hold_module'=>$partialCourseFailed,'is_pass' => $is_pass,'status'=>$status, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$modelStud->id, 'academic_year'=>$acad])
                                      ->execute();
                         }
                        
                ?>	 
		              
		              </td> 
		           </tr> 
		           
		            <tr style="page-break-inside: avoid; page-break-after: auto; border: 1px solid white;">
			          <td colspan="6" align ="center" >
			   
			                                <?php $text1 = Yii::t('app', 'Cycle');
                                                               $text2 = Yii::t('app', 'Total module failed');
                                                               
                                                                $complete=$modelStudCourse->isCompleted( $complete_integer );
                                                                $close = $partialCourseFailed;
                                                               
                                                               if($level==2)
                                                               {
                                                                   $text1 = Yii::t('app', 'Sanction obtenue');
                                                                   $text2 = Yii::t('app', 'Programme terminé');
                                                                   
                                                                    $complete=$modelStudCourse->isCompleted2( $complete_integer );
                                                                    
                                                                    $close = Yii::t('app', 'OUI');
                                                               }
                                                        
                                                        ?>
						      <table class="table table-striped table-hover"  style="border:1px solid #A79C9E; width:50%; ">
							  
							  <tr>
							     <th style="background-color:#A79C9E; font-size:10px;text-align:center" > <?= $text1 ?> </th>
							     <th style="background-color:#A79C9E; font-size:10px;text-align:center" > <?=  $text2 ?> </th>
							  </tr>
							   
							  <tr>
							         <?php
							                
							                 //$totalCourseFailed =  $modelStudCourse->getTotalCourseForStudent($student) - $modelStudCourse->getTotalCoursePassForStudent($student) ;
							         ?>
							         
							     <td style="text-align:center" ><?= $complete  ?> </td>
							     <td style="text-align:center" > <?= $close ?> </td>
							    
							  </tr>
							  
						     </table>
					  
					 
					
			                   <hr style="width:25%; margin-bottom:2px; border:1px solid #A79C9E;" /><?= $administration_signature_text ?>
			          </td> 
		           </tr> 
		           
				 </tbody>	   
		       </table>
		       
			</div>
		</div>
	</div>
</div>
<?php

  }  //end all_validated
//else
// { $stud_with_nonvalidated_grade++;
// 
//      Yii::$app->session['grades_validated'] = false;
// } 


//if($stud_with_nonvalidated_grade==$tot_stud)
//{  Yii::$app->session['all_grades_not_validated'] = true;
//	}  
                   if(($average_base==10)||($average_base==100)) 
						 { if($weight_total!=0)  
							       $average=round(($grade_total/$weight_total)*$average_base,2);
						  }
					 else			
						 $average =null;
						 
						
						
						
						
						 
						 	

 }
 
?>