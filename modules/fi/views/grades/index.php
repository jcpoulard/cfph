<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\fi\models\SrcRooms;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcGrades */
/* @var $dataProvider yii\data\ActiveDataProvider */

$acad = Yii::$app->session['currentId_academic_year'];

$this->title = Yii::t('app', 'Grades');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'roo'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content grades-index">

                   <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Program'); ?></th>
                                            <th><?= Yii::t('app','Room'); ?></th>
				            <th><?= Yii::t('app','Course'); ?></th>
				            <th><?= Yii::t('app','Grade Value'); ?></th>
				            <th><?= Yii::t('app','Validate'); ?></th>
				      <!--      <th><?= Yii::t('app','Publish'); ?></th>    -->
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

            
    $dataGrade = $dataProvider->getModels();
    
          foreach($dataGrade as $grade)
           {  $href='';
           	   if(Yii::$app->session['profil_as'] ==0)
				  {	
				  	$href= '../persons/moredetailsstudent?id='.$grade->student.'&is_stud=1';
				  }
				elseif(Yii::$app->session['profil_as'] ==1)
				  {
				  	$href='#';
				  	}
           	
           	   echo '  <tr >
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$grade->student0->first_name.' </a></td>
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$grade->student0->last_name.' </a></td>
                                                    <td ><span data-toggle="tooltip" title="'.$grade->course0->module0->program0->label.'">'.$grade->course0->module0->program0->short_name.' </span> </td>
                                                    <td >';
                                                          if(isset($grade->student0->studentLevel->room0->room_name))
                                                              echo $grade->student0->studentLevel->room0->room_name;
                                                         /* else //al cheche l nan student_level_history
                                                            {
                                                              $room =0;
                                                              
                                                              $command = Yii::$app->db->createCommand('SELECT level, room FROM student_level_history where student_id='.$grade->student.' and academic_year='.$acad)->queryAll();
                                                              if($command!=  null)
                                                               {
                                                                  foreach($command as $r)
                                                                  {
                                                                     $room = $r["room"];
                                                                  }
                                                               }
                                                                
                                                               $modelroom = SrcRooms::findOne($room);
                                                               
                                                               echo $modelroom->room_name;
                                                            }
                                                          * 
                                                          */
                                                            
                                                     echo ' </td>
                                                    <td >'.$grade->course0->module0->subject0->subject_name.' </td>
                                                    <td >'.$grade->grade_value.' </td>
                                                    <td >'.$grade->getValidateGrade().' </td>';
                                                   // <td >'.$grade->getPublishGrade().' </td>
                             
                                                 echo '  <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                         if(Yii::$app->user->can('fi-grades-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/fi/grades/view?id='.$grade->id.'&wh=roo', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('fi-grades-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/fi/grades/update?id='.$grade->id.'&wh=roo', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }

                                                              
                                                         if(Yii::$app->user->can('fi-grades-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/fi/grades/delete?id='.$grade->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>



<?php
        if(Yii::$app->session['profil_as'] ==0)
            $buttons = 'buttons: [
                   // { extend: \'copy\'},
                   // {extend: \'csv\'},
                    {extend: \'excel\', title: \'Grades List\'},
                    {extend: \'pdf\', title: \'Grades List\'},

                    {extend: \'print\',
                     customize: function (win){
                            $(win.document.body).addClass(\'white-bg\');
                            $(win.document.body).css(\'font-size\', \'10px\');

                            $(win.document.body).find(\'table\')
                                    .addClass(\'compact\')
                                    .css(\'font-size\', \'inherit\');
                    }
                    }
                ]';
        elseif(Yii::$app->session['profil_as'] ==1)
             $buttons = 'buttons: []';
        
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                $buttons

            });

        });

JS;
$this->registerJs($script);

?>
    
