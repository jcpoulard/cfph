<?php 
use yii\helpers\Html;



?>


 <?php 
 
                      	 $this->title = Yii::t('app','Grades report');
                         $form = '_transcriptNotes';
                       
                    if($transcriptItems == 3)
                      { 
                      	 //$this->title = Yii::t('app','Certificate');
                        $form = '_report_reportcard';
                      } 
                    elseif($transcriptItems == 2)
                      { 
                      	 //$this->title = Yii::t('app','Certificate');
                        $form = '_report_delayedgrade';
                      } 
                    elseif($transcriptItems == 1)
                      { 
                      	 //$this->title = Yii::t('app','Certificate');
                        //$form = '_transcriptNotes';
                      } 
                    elseif($transcriptItems == 0)
                         { 
                      	    //$this->title = Yii::t('app','Transcript of notes');
                             $form = '_transcriptNotes';
                         }
                           
                   ?>

 <div class="row">
    
        <div class="" style="width:auto;float:left; margin-left:20px;">
            
            
    </div>
    
     <div class="col-lg-7">
         <h3><?= $this->title; ?></h3>
     </div>
               
</div>
<div class="wrapper wrapper-content grades-sheet">
    
    <?php
    
    if( in_array($transcriptItems,[0,1,3] ) )
      {
      echo $this->render($form, [
        'model' => $model,
        'transcriptItems'=>$transcriptItems,
         ]);
      }
    elseif($transcriptItems==2)
        {
          echo $this->render($form, [
        'model' => $model,
        'transcriptItems'=>$transcriptItems,
         'dataProvider' => $dataProvider,
         'course_weight'=> $course_weight,
         'use_update'=>$use_update,
         'is_save'=>$is_save,
          'success'=>$success,
              'message_course_id'=>$message_course_id,
         ]);
        }
      
      
      
      ?>
        
</div>

