

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */

   if( (Yii::$app->user->can("teacher")))
	 $this->title = Yii::t("app", "Grades By Rooms");
  else
	$this->title = Yii::t("app", "List student's grades by room");
																	 

 
?>

 <div class="row">
    
        <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-edit"></i> ', ['update','all'=>1,'from'=>'list','mn'=>'std' ], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Update')]) ?> 
        
            
    </div>
    
     <div class="col-lg-7">
         <h3><?= $this->title; ?></h3>
     </div>
               
</div>
<div class="wrapper wrapper-content grades-sheet">
    
    <?= $this->render('_listByRoom', [
        'model' => $model,
        'program'=>$program,
        'shift'=>$shift,
        'level'=>$level,
        'room'=>$room,
       
    ]) ?>
        
</div>

