<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */

$this->title = Yii::t('app', 'Grade');

?>

<p>
    
</p>
<p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', ], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id,], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h3><?= Html::encode($this->title) ?></h3>

<div class="grades-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            //'student',
            [ 'attribute'=>'student',
               'value'=>$model->student0->getFullName(),
            ],
            'course0.module0.subject0.subject_name',
            'grade_value',
            'validate:boolean',
           // 'publish:boolean', 
           // 'comment',
            'date_created:datetime',
            'date_updated:datetime',
            'create_by',
            'update_by',
        ],
    ]) ?>

</div>
