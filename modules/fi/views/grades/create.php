<?php

use yii\helpers\Html;

use app\modules\fi\models\SrcPersons;

use app\modules\fi\models\SrcCourses;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */

if(!isset($_GET['course']))
   $this->title = Yii::t('app', 'Create Grades');
else
  { 
  
      $modelPers = SrcPersons::findOne($_GET['stud']);
  
     $this->title = Yii::t('app', 'Create Grades').': '.$modelPers->getFullName();
     
     
   }

?>

     
<div class="row">
    <div class="col-lg-1">
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
    </div>
<?php
if($use_update)
{
?> 
         
    <div class="col-lg-1" style="margin-right:20px;" >
      <p> <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update','all'=>1,], ['class' => 'btn btn-success btn-sm']) ?>
      </p>
    </div>
<?php

}
?> 
    <div class="col-lg-8">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>   
</div>    
<div class="wrapper wrapper-content grades-create">

    <?php
          if(isset($_GET['stud'])&&($_GET['stud']!=''))
          {
              echo $this->render('_form', [
        'model' => $model,
        'dataProvider'=>$dataProvider, 
        'course_weight'=>$course_weight,
        'success'=>$success,
        'is_save'=>$is_save,
        'set_room'=>$set_room,
	    'use_update'=> $use_update,
	    'message_course_id'=> $message_course_id,
	    'message_GradeHigherWeight'=>$message_GradeHigherWeight,
	    'message_noGradeEntered'=>$message_noGradeEntered,
		'no_stud_has_grade'=>$no_stud_has_grade,
		
             ]);
          }
        else
          {
                echo $this->render('_create', [
                 'model' => $model,
                 'dataProvider'=>$dataProvider, 
                 'course_weight'=>$course_weight,
                 'success'=>$success,
                 'is_save'=>$is_save,
                     'use_update'=> $use_update,
                     'message_course_id'=> $message_course_id,
                     'message_GradeHigherWeight'=>$message_GradeHigherWeight,
                     'message_noGradeEntered'=>$message_noGradeEntered,
                         'no_stud_has_grade'=>$no_stud_has_grade,

             ]); 
          
                  
          }
          
        ?>

</div>
