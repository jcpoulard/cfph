<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\Academicperiods;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\StudentHasCourses */
/* @var $form yii\widgets\ActiveForm */



$current_acad = Yii::$app->session['currentId_academic_year'];



$acad = $model->academic_year[0];

$modelCourse = new SrcCourses();
 
?>

 <input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div class="student-has-courses-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
       
       <div class="col-lg-3">
            
         <?php                    $data_academic_year = ArrayHelper::map(Academicperiods::find()->where(['is_year'=>1])->orderby('id DESC')->all(),'id','name_period' );
              echo $form->field($model, 'academic_year')->widget(Select2::classname(), [
			                                   'data'=>$data_academic_year,
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '--  select academic year  --'),
			                                                 'onchange'=>'submit()',
			                                   ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
        
        <div class="col-lg-2">
            
         <?php                    
              echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                 'onchange'=>'submit()',
			                                   ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
        
        <div class="col-lg-2">
            
         <?php        
         
                //return program_id or null value
                 $program1 = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program1==null)
		       {
		       	$data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		       	$data_shift = [];
		       }
		     else
		       {
		       	  $data_program = ArrayHelper::map(SrcProgram::find()->where('id='.$program1)->all(),'id','label' );
		       	  
		       	    $modelShift_ = new SrcShifts();
                           $data_shift = $modelShift_->getShiftinprogram($program1,$acad);
		       	  
		       	}
        

         echo $form->field($model, 'program')->widget(Select2::classname(), [

                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-shift").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-room").html("");
                                        	
                                        });',
                                  
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
            ?>
        </div>
        <div class="col-lg-2">
         <?php 
         
              if($model->shift[0]!='')

                {       $modelShift_ = new SrcShifts();
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program[0],$acad);
                  }      
                 ?>   
            <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcstudenthascourses-program").val()+"&level='.'"+$("#srcstudenthascourses-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcstudenthascourses-room").html(data);	
                                       //chanje course 
                                        $("#srcstudenthascourses-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>

        </div>
        
    
        <div class="col-lg-2">
            <?php 
         $data_room = [];
              if($model->room[0]!='')

                {       $modelRoom_ = new SrcRooms();
                	/*  $data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0'])->where(['shift'=>$model->shift,'program'=>$model->program])
                         ->all(),'room0.id','room0.room_name' );
                        */
                         
                      //$data_room = $modelRoom_->getRoominshiftprogram($model->shift,$model->program,$acad);
                     $from = 'grades';
                     
                    
                          $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift[0],$model->program[0],$model->level[0],$acad);
                    
                  }      
                 ?>      
      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>'submit()',

                       ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>

    
        </div>
    </div>
    

    

<?php
if( ( $dataProvider->getModels()!=null )&&( ($model->program[0]!='')&&($model->room[0]!='') )  )
  {

    	        	echo '<div class="" style="margin-top:-10px; margin-bottom:10px;">';
				   	 
				    echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	  
				  
					      echo '<span style="color:blue;" ><b>'.Yii::t('app','Students whose grades have not been validated will not have reportcard.').'</b></span>';
					
					 echo'</td>
					    </tr>
						</table>';
					
           echo '</div>';



?>    
       
    <?php Pjax::begin(); ?>
    
    <?php if($current_acad==$acad)
            {
        ?>
    <?= GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['student'] ];
                                                 },
         ],
         [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Id Number"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student0->id_number;
				 }
			  ],
            
            
            
        
        [
             'attribute'=>'student',
			 'label'=>Yii::t("app","First Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student0->first_name;
				 }
			  ],
            
            
            
         [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Last Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student0->last_name;
				 }
			  ],
			  
			  
	     [
             'attribute'=>'student',
			 'label'=>Yii::t("app"," "),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				 }
			  ],
             
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
    <?php   
              }
             elseif($current_acad!=$acad)
             {
    ?>
        <?= GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn',
           // 'header' => 'No',
           'options' => ['width' => '0px'], 
        ],
        
        ['class' => 'yii\grid\CheckboxColumn',//ActionColumn', 
                        'header' => Html::checkBox('selection_all', false, [
				        'class' => 'select-on-check-all',
				        //'label' => Yii::t("app","All"),
				         ]), 
				        'options' => ['width' => '80px'], 
				         
                         'checkboxOptions' => function($model, $key, $index, $widget) {
                                                          return ['value' => $model['student_id'] ];
                                                 },
         ],
         [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Id Number"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student->id_number;
				 }
			  ],
            
            
            
        
        [
             'attribute'=>'student',
			 'label'=>Yii::t("app","First Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student->first_name;
				 }
			  ],
            
            
            
         [
             'attribute'=>'student',
			 'label'=>Yii::t("app","Last Name"),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return $data->student->last_name;
				 }
			  ],
			  
			  
	     [
             'attribute'=>'student',
			 'label'=>Yii::t("app"," "),
			 'format'=>'text', // 'raw', // 'html', //
			 'content'=>function($data){
				 return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				 }
			  ],
             
            
           ], 
           // ['class' => 'yii\grid\ActionColumn'],               
        
        
        
    ]); ?>
    
    <?php  }   ?>
        <?php Pjax::end(); 
    
    
 if($dataProvider->getModels()!=null)
   {   
    ?>
    


    
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Done reportcard'), ['name' => 'create', 'class' => 'btn btn-success']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div>      

<?php
   }
  
  }
?>

    <?php ActiveForm::end(); ?>

</div>
 
 <br/><br/>
 <div style="clear:both;"></div>