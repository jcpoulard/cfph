<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */

$this->title = Yii::t('app', 'Create Reportcard');

?>
<?= $this->render("//layouts/reportsLayout") ?>
<p></p>

 <div class="row">
    
        <div class="col-lg-11">
             <h3><?= $this->title; ?></h3>
        </div>
       
        
    </div>
<div class="wrapper wrapper-content reportcard-create">

    <?= $this->render('_reportcard', [
        'model' => $model,
        'dataProvider'=>$dataProvider, 
        
		
    ]) ?>

</div>
