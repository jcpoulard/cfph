<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;

use app\modules\fi\models\SrcCourses;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>
    
<?php 		
         $modCourse = new SrcCourses;
         
       $modelCourse = SrcCourses::findOne($_GET['course']);
         $course_weight =$modCourse->getWeight($_GET['course']); 
         
        $grades_comment = infoGeneralConfig('grades_comment');
 
 
//error message  
	        	echo '<div class="" style="margin-top:-10px; margin-bottom:10px;">';
				   	 
				    echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	  
				 /* 
					if($success)
				     { echo '<span style="color:green;" >'.Yii::t('app','Operation terminated successfully.').'</span>'.'<br/>';
					   $success=false;
				      }
*/
				    
					if($message_GradeHigherWeight)
					  {	 if(!isset($_GET['stud'])||($_GET['stud']==""))
		                     echo '<span style="color:red;" >'.Yii::t('app','Grades GREATER than COURSE WEIGHT are not saved.').'</span>'.'<br/>';
						 else
						   echo '<span style="color:red;" >'.Yii::t('app','Grade VALUE can\'t be GREATER than COURSE WEIGHT!').'</span>'.'<br/>';
					    $message_GradeHigherWeight=false;
					  }
                   
                          

					    echo '<span style="color:blue;" ><b>'.$modelCourse->module0->subject0->subject_name.'<br/>'.Yii::t('app','- COURSE WEIGHT : ').$course_weight.' - </b></span>';
					
					 echo'</td>
					    </tr>
						</table>';
					
           echo '</div>';	
           
    ?>    
    
    <div class="row">
        <div class="col-lg-6">
              <?= $form->field($model, 'grade_value')->textInput() ?>
        </div>
    <?php
                 if($grades_comment==1)
                  {
      ?>
                 
        <div class="col-lg-6">
           <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
      </div>
        
     <?php
                  }
     ?>
     
    </div>
    
    
<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
               
        <?php 
                if($set_room==0)
                {
               echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
        
               echo Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']);
                }
        
        ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
