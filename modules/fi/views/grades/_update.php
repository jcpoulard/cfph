<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcStudentOtherInfo;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
?>

<?php 

Yii::$app->session['tab_index']=0;


$grades_comment = infoGeneralConfig('grades_comment');	

if($grades_comment==0)
  {
	 $item_array= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				         [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	          
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	       $grade_value ='';
						 	      
						 	        if(isset($data->grade_value))
						 	          $grade_value = $data->grade_value;
						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
	  
	  
	  
  }
elseif($grades_comment==1)
{
	$item_array= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	          
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	      $grade_value ='';
						 	      
						 	        if(isset($data->grade_value))
						 	          $grade_value = $data->grade_value;
						 	       						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'"  type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  
                          [
			             'attribute'=>'comment',
						 'label'=>Yii::t("app","Comment"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	          
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	     $comment ='';
						 	      
						 	        if(isset($data->comment))
						 	          $comment = $data->comment;
						 	       
						 	      	$val ='<input name="comments['.$data->id.']" value ="'.$comment.'" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					                 <input name="id_grade_['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ], 
			  
			        ];
	  
	  
	  
			        
	
}
		 

?>



<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>
  											

			<?php 	
			
			 $weight = ' ';
			  $modelCourses = new SrcCourses();
			  $modelGrades = new SrcGrades();
			  $model = SrcGrades::findOne($_GET['id']);
			 // $_model= $modelGrades->findOne($_GET['id']);
			  $course_weight =$modelCourses->getWeight($model->course); 
                      
			$dataProvider= $modelGrades->searchGradeById($_GET['id']);
   ?>
			
<ul class="nav nav-tabs"></ul>  
<br/> 
<?php 		
//error message  
	        	echo '<div class="" style="margin-top:-10px; margin-bottom:10px;">';
				   	 
				    echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	  
				 /* 
					if($success)
				     { echo '<span style="color:green;" >'.Yii::t('app','Operation terminated successfully.').'</span>'.'<br/>';
					   $success=false;
				      }
*/
				    
					if($message_GradeHigherWeight)
					  {	 if(!isset($_GET['stud'])||($_GET['stud']==""))
		                     echo '<span style="color:red;" >'.Yii::t('app','Grades GREATER than COURSE WEIGHT are not saved.').'</span>'.'<br/>';
						 else
						   echo '<span style="color:red;" >'.Yii::t('app','Grade VALUE can\'t be GREATER than COURSE WEIGHT!').'</span>'.'<br/>';
					    $message_GradeHigherWeight=false;
					  }
                   
                          

					    echo '<span style="color:blue;" ><b>'.Yii::t('app','- COURSE WEIGHT : ').$course_weight.' - </b></span>';
					
					 echo'</td>
					    </tr>
						</table>';
					
           echo '</div>';	
           
           						
			 
echo GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array, 
                          
       ]); 

				
				
				

			 ?>
     
 <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Update'), ['name' => 'update', 'class' => 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

 
    <?php ActiveForm::end(); ?>

</div>



<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    
        // e.keyCode = 37(left), 38(up), 39(right), 40(down)
        
  $('input').keypress(function(e){
   var key=e.keyCode || e.which;
       
   //alert("@-"+key);
        if( (key==13)||(key==40) ){ 
        var x = 1;
         var ind = $(this).attr("tabindex");  
          var next_ind =0;
             next_ind = Number(ind) + Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
        else if(key==38){  //up
        
            var x = 1;
            var ind = $(this).attr("tabindex");  
            var next_ind =0;
             next_ind = Number(ind) - Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
    });  		
				
	

JS;
$this->registerJs($script);

?> 



		   
			
