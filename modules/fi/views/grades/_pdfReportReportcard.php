<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcAcademicperiods;



 $include_course_withoutGrade = infoGeneralConfig('include_course_withoutGrade');
$administration_signature_text = infoGeneralConfig('administration_signature_text');

//Extract average base
$average_base = infoGeneralConfig('average_base');


    

  $premye_tou=0;
 
        $acadModel = SrcAcademicperiods::findOne($acad);
				   
  
          $modelStud = SrcPersons::findOne($student);
          
          $program = getProgramByStudentId($student);
          
          $programModel = SrcProgram::findOne($program);
	

//check if all grades are validated for this stud
   $modelGrades=new SrcGrades();		
	$all_validated=$modelGrades->if_all_grades_validated($student,$acad);
									
	$grade_total=0;
  	$weight_total=0;
  	$average ='';
	

   

  	
 if($premye_tou!=0)
  {
  	 echo '<pagebreak />  
           <br/> ';
  	}
 else
   { $premye_tou=1; 
      echo '<br/> '; 
   }	
   	
  	
  	     	
?>
 
<br/><br/>                    	          	
<div class="content">
	<div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
					<span> <?php  echo strtr( strtoupper( Yii::t('app', 'Competency statements') ), capital_aksan() );   ?> 
						                 
					</span>
				</h4>
		   	</div>
		</div>
	
		<div class="row">
			<div class="container">
				<table class="table table-striped table-hover" width="100%" >
				  <tbody>
				    <tr>
				     <td colspan="5" style="background-color:#A79C9E; text-align:center; " ><strong> <?= $programModel->label ?> </strong>  </td>
				     
				  </tr>
				  <tr>
				     <th style="font-size:10px;  background-color:#F3F3F4;"> <?=  Yii::t('app', 'First name & Last name') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Degree') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Shift') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Year') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Printing date') ?> </th>
				  </tr>
			<?php  
			
			
			?>	  
				  <tr>
				     <td style="font-size:10px; "> <?= $modelStud->getFullName()  ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $level ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $modelStud->studentOtherInfo0[0]->applyShift->shift_name ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  $acadModel->name_period ?> </td>
				     <td style="font-size:10px; text-align: center;"> <?=  Yii::$app->formatter->asDate(date('Y-m-d') ) ?></td>
				  </tr>
				  
				 </tbody>  
			   </table>
			   
			   
			   <table class="table table-striped table-hover" width="100%" >
				  <tbody> 
				  <tr>
				     <th style="width:42%; font-size:10px;  background-color:#F3F3F4;" > <?=  Yii::t('app', 'Competency label') ?> </th>
				     <th style="width:12%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style="width:10%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Duration') ?> </th>
				     <th style="width:13%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Passing Grade') ?> </th>
				     <th style="width:11%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Grade') ?> </th>
				     <th style="width:12%; font-size:10px; text-align: center; background-color:#F3F3F4;" > <?=  Yii::t('app', 'Mention') ?> </th>
				  </tr>
                              
                                  <?php  
                                  
                                  $modelC = new SrcCourses;
			       $modelStudCourse = new SrcStudentHasCourses;
			        $allCourses = $modelC->getCourseInfoForTranscriptNotes($student,$program);//$modelC->getCourseInfoByShiftRoomAcadProgram($shift,$room,$acad,$program);
			      
			     $partialCourseFailed=0;
			     
			     foreach($allCourses as $course)
			      { 
			         //gade si evaluation fet pou omwens 1moun
                                $is_evaluated= isCourseEvaluated($course->id,$level);
                                      
                               if($is_evaluated==true)
                                 {
                                    $modelGradeInfo = $modelGrades->getGradeInfoByStudentCourse($modelStud->id,$course->id);
			            
			            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($modelStud->id,$course->id);
			        
			       if($include_course_withoutGrade==0)
			        {
				        if(isset($modelGradeInfo->grade_value) &&($modelGradeInfo->validate==1) ) //si li pa konpoze pou kou a pa metel nan kane a
				         {
				         	
				         	$grade_total= $grade_total + $modelGradeInfo->grade_value;
  	                         $weight_total= $weight_total + $course->weight;
			?>
                                  
                              <tr>
				     <td style=" font-size:10px;"> <?= $course->module0->subject0->subject_name  ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->code ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->duration ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->passing_grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelGradeInfo->grade_value ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			<?php
			                      if($modelIspass==0)
			                         $partialCourseFailed++;
			              }
			              
			         }
			       elseif($include_course_withoutGrade==1)
			          {  
			          	  $grade= '-';
			          	 if(isset($modelGradeInfo->grade_value) )
			          	   { $grade= $modelGradeInfo->grade_value;
			          	     $grade_total= $grade_total + $modelGradeInfo->grade_value;
			          	    }
			          	   
  	                         $weight_total= $weight_total + $course->weight;
			     ?>
			       <tr>
				     <td style=" font-size:10px;"> <?= $course->module0->subject0->subject_name  ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->code ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->module0->duration ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->passing_grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			   <?php      
			                        if($modelIspass==0)
			                            $partialCourseFailed++; 	
			          	}
			       
                                  }//gen evalyasyon fet pou kou a
			      }
			      
			?>	  
			       <tr>
                               <td colspan="6" style=" font-size:10px; border-bottom: 1px solid white;">
			     
		<?php 
                              //Extract transcript_note_text
			     $transcript_note_text = infoGeneralConfig('transcript_note_text');
                             
                echo $transcript_note_text.'<br/>';
                
                     $complete_integer = 0;
                
                       if($level==1)
                       {
                            echo '<b>'.decisionSelectedOption(false,$partialCourseFailed,$programModel->id,NULL).'</b>';
                               
                              // $complete_integer = 0;
                       } 
                      /* elseif($level==2)
                       {
                           
                               $complete_integer = $modelStudCourse->isCycleCompleted($student);
                       }
                       */
                      
                       if($partialCourseFailed==0)
			    $complete_integer=1;
                        else
                           $complete_integer=0;
                        
                        
                        
                        //voye done nan tab student_level_history
                        $is_pass =0;
                        $next_level = NULL;
                        $status = NULL;
                        
                        if(decisionSelectedOption(false,$partialCourseFailed,$programModel->id,'set')==1)
                           $is_pass =1;
                       elseif(decisionSelectedOption(false,$partialCourseFailed,$programModel->id,'set')==0)
                          $is_pass =0;
                        
                       //$modelStudLev = SrcStudentLevel::findOne($modelStud->id);
                                                          
                         // $level = $modelStudLev->level;

                          //$old_status = $modelStudLev->status;

                           switch ($level)
                            {
                               case 1:  $next_level = 2;
                                        $status =0;

                                        break;
                               case 2:  $next_level = 2;
                                        $status =1;

                                        break;

                               case 3:  $next_level = 3;
                                        $status =1;

                                        break;

                             }
                          
                               $command1 = Yii::$app->db->createCommand();
                             $command1->update('student_level_history', ['nbr_hold_module'=>$partialCourseFailed,'is_pass' => $is_pass,'status'=>$status, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$modelStud->id, 'academic_year'=>$acad])
                                      ->execute();
                         
                        
                ?>	 
		              
		              </td> 
		           </tr> 
		           
		            <tr style=" border: 1px solid white;">
			          <td colspan="6" align ="center" >
			   
			  
						      <table class="table table-striped table-hover"  style="border:1px solid #A79C9E; width:50%; ">
							  
							  <tr>
							     <th style="background-color:#A79C9E; font-size:10px;text-align:center" > <?=  Yii::t('app', 'Cycle') ?> </th>
							     <th style="background-color:#A79C9E; font-size:10px;text-align:center" > <?=  Yii::t('app', 'Total module failed') ?> </th>
							  </tr>
							   
							  <tr>
							         <?php
							                 $complete=$modelStudCourse->isCompleted( $complete_integer );
							                 //$totalCourseFailed =  $modelStudCourse->getTotalCourseForStudent($student) - $modelStudCourse->getTotalCoursePassForStudent($student) ;
							         ?>
							         
							     <td style="text-align:center" ><?= $complete  ?> </td>
							     <td style="text-align:center" > <?= $partialCourseFailed ?> </td>
							    
							  </tr>
							  
						     </table>
					  
					 
					
			                   <hr style="width:25%; margin-bottom:2px; border:1px solid #A79C9E;" /><?= $administration_signature_text ?>
			          </td> 
		           </tr> 
		           
				 </tbody>	   
		       </table>
		       
			</div>
		</div>
	</div>
</div>

 
