<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcAcademicperiods;


				
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 


  
    
    
<?php   
 $modelShift_ = new SrcShifts();
  $modelCourse = new SrcCourses();
    

  $premye_tou=0;
  
  $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
     
$school_name_school_acronym = $school_name; 

//if($school_acronym!='')
//  $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	


                

//                 $header_text_date 
//                 $transcript_note_text
//                 $transcript_signature_n_title

//                $programModel = SrcProgram::findOne($program);
//                $shiftModel = SrcShifts::findOne($shift);
//                $roomModel = SrcRooms::findOne($room);
        $acadModel = SrcAcademicperiods::findOne($acad);
        
        $SrcAcademicPeriods = new SrcAcademicperiods;
        $previous_acad= $SrcAcademicPeriods->getPreviousAcademicYear($acad);
				   
/*	echo '<table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> '.headerLogo().'
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
                                 
                 ';
      */  
          $modelStud = SrcPersons::findOne($student);
          
          $program = getProgramByStudentId($student);
          
          $programModel = SrcProgram::findOne($program);
	

//check if all grades are validated for this stud
   $modelGrades=new SrcGrades();		
	//$all_validated=$modelGrades->if_all_grades_validated($student);
									
	$grade_total=0;
  	$weight_total=0;
  	$average ='';
	$level = '';
        
       if(isset($modelStud->studentLevel->level) )
           $level = $modelStud->studentLevel->level;
       else  ///al gade nan student_level_history
        {
            $command = Yii::$app->db->createCommand('SELECT level, room, status, nbr_hold_module, is_pass FROM student_level_history WHERE student_id='.$student.' order by academic_year DESC')->queryAll();
                        if(($command!=null))
                          {
                            foreach($command as $result)
                              {
                                $level = $result['level'];
                                break;
                              }
                          }
                       
        }

   

  	
 if($premye_tou!=0)
  {
  	 echo '<pagebreak />  
           <br/> ';
  	}
 else
   { $premye_tou=1; 
      echo '<br/> '; 
   }	
  	
  	
  	     	
?>
 
<br/><br/>                    	          	
<div class="content">
    <div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
                            <span><b> <?php  echo strtr( strtoupper( Yii::t('app', 'Transcript of notes') ), capital_aksan() );   ?> 
                                </b>	                 
					</span>
				</h4>
		   	</div>
		</div>
                
                <div class="row">
			<div class="container">
				<table class="table table-striped table-hover" width="100%" >
				  <tbody>
				   <tr>
				     <th style="font-size:10px;  background-color:#F3F3F4;"> <?=  Yii::t('app', 'First name & Last name') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Option') ?> </th>
                                     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Degree') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Shift') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Year') ?> </th>
				  </tr>
				  <tr>
				     <td style="font-size:10px;  height: 10px;"> <?= $modelStud->getFullName()  ?> </td>
                                     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $programModel->label ?></td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $level ?> </td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $modelStud->studentOtherInfo0[0]->applyShift->shift_name ?> </td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $acadModel->name_period ?> </td>
				  </tr>
				  
				 </tbody>  
			   </table>
			   
			   <table class="table table-striped table-hover" width="100%" >
				  <tbody> 
				  <tr>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:10%" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; " > <?=  Yii::t('app', ''
                                             . 'Titre du Module') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:12%" > <?=  Yii::t('app', 'Seuil de réussite/100') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:8%" > <?=  Yii::t('app', 'Notes  sur 100') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:10%" > <?=  Yii::t('app', 'Verdict') ?> </th>
				  </tr>
                                  
                                  <?php  $modelC = new SrcCourses;
			       $modelStudCourse = new SrcStudentHasCourses;
			        $allCourses = $modelC->getCourseInfoForTranscriptNotes($student,$program);
			     
			     $partialCourseFailed=0;
			     
                             $total_rows = sizeof($allCourses);
			     $rows_counter=0;
                             
			     foreach($allCourses as $course)
			      { 
			            $modelGradeInfo = $modelGrades->getGradeInfoByStudentCourse($modelStud->id,$course->id);
			            
			            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($modelStud->id,$course->id);
			        
			      // if($include_course_withoutGrade==0)
			       // {
				        if(isset($modelGradeInfo->grade_value) &&($modelGradeInfo->validate==1) ) //si li pa konpoze pou kou a pa metel nan kane a
				         {
				         	$rows_counter ++; 
                                                
                                              if($rows_counter == 23)
                               { 
			
			?>
                                 <tr>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:10%" > <?=  Yii::t('app', 'Code') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; " > <?=  Yii::t('app', ''
                                             . 'Titre du Module') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:12%" > <?=  Yii::t('app', 'Seuil de réussite/100') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:8%" > <?=  Yii::t('app', 'Notes  sur 100') ?> </th>
				     <th style="font-size: 10px;background-color:#F3F3F4; width:10%" > <?=  Yii::t('app', 'Verdict') ?> </th>
				  </tr>  
                                      
                                      
                                      
                          <?php     
                               }  
				         	$grade_total= $grade_total + $modelGradeInfo->grade_value;
  	                         $weight_total= $weight_total + $course->weight;
			?>
                                  
                              <tr>
				     <td style=" font-size:10px;"> <?=  $course->module0->code ?> </td>
				     <td style=" font-size:10px;"> <?= $course->module0->subject0->subject_name  ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?=  $course->passing_grade ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelGradeInfo->grade_value ?> </td>
				     <td style=" font-size:10px; text-align: center;"> <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			<?php
			                      if($modelIspass==0)
			                         $partialCourseFailed++;
                                              
                                               
			              }
                                      
                                       if($rows_counter==22)
                                  {
                                  ?>    
				      </tbody>	   
		                      </table>
                            <div class="clear"><br/> <br/> <br/>  <br/><br/></div>  
                            
                   <div class="row">
			
                        <div class="Container">
                             
		    	<h4 class="content-title" style="text-align:center;">
                           <br/> <br/><br/> <br/><br/>
                            <span><b> <?php  echo strtr( strtoupper( Yii::t('app', 'Transcript of notes') ), capital_aksan() );   ?> 
                                </b>	                 
					</span>
				</h4>
		   	</div>
		</div>
                            <table class="table table-striped table-hover" width="100%" >
				  <tbody>
				   <tr>
				     <th style="font-size:10px;  background-color:#F3F3F4;"> <?=  Yii::t('app', 'First name & Last name') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Option') ?> </th>
                                     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Degree') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Shift') ?> </th>
				     <th style="font-size:10px; text-align: center; background-color:#F3F3F4;"> <?=  Yii::t('app', 'Year') ?> </th>
				  </tr>
				  <tr>
				     <td style="font-size:10px;  height: 10px;"> <?= $modelStud->getFullName()  ?> </td>
                                     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $programModel->label ?></td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $level ?> </td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $modelStud->studentOtherInfo0[0]->applyShift->shift_name ?> </td>
				     <td style="font-size:10px; text-align:center; height: 10px;"> <?=  $acadModel->name_period ?> </td>
				  </tr>
				  
				 </tbody>  
			   </table>
			   
			   <table class="table table-striped table-hover" width="100%" >
				  <tbody> 
                                      
                                     
			<?php     
                               
                               }
                               
                                 
                               if($rows_counter > 22)
                               { 
			
			?>
                                 
                                      
                                      
                                      
                          <?php     
                               }
			   /*      }
			       elseif($include_course_withoutGrade==1)
			          {  
			          	  $grade= '-';
			          	 if(isset($modelGradeInfo->grade_value) )
			          	   { $grade= $modelGradeInfo->grade_value;
			          	     $grade_total= $grade_total + $modelGradeInfo->grade_value;
			          	    }
			          	   
  	                         $weight_total= $weight_total + $course->weight;
			     ?>
			       <tr>
				     <td > <?= $course->module0->subject0->subject_name  ?> </td>
				     <td > <?=  $course->module0->code ?> </td>
				     <td > <?=  $course->module0->duration ?> </td>
				     <td > <?=  $course->passing_grade ?> </td>
				     <td > <?= $grade ?> </td>
				     <td > <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
				  </tr>
			   <?php      
			                        if($modelIspass==0)
			                            $partialCourseFailed++; 	
			          	}
			         */
			      }
			      
			?>
                                  
                                  
                              <tr>
                                <td colspan="5" align ="" style="border-top: 1px solid #000; border-bottom: 1px solid #000; background-color: #cacacb;" >
			   
			   <div style="float: left; ">
                               <b><span style="font-size: 10px;"><?=  Yii::t('app', 'Note').': ' ?></span></b>  <span style="font-size: 10px;"><?=  $transcript_note_text ?></span>
                           
                            <br/> <br/>
                                <span style="font-size: 10px;"><?=  $header_text_date ?></span>
                            
                           </div>
                           
                           <br/> 
						   <div style="width:25%; align-content: center; "> 
                                                          <table  style="width:35%; margin: auto; margin-bottom:-10px;  align-content: center; ">
							  
							      <tr>
							          <td style="text-align:center; background-color: #cacacb; border-top: 1px solid #000; font-size: 12px;" > <b> <?= $transcript_signature_n_title ?> </b> </td>
							    
							      </tr>
							  
						           </table>
                                                      </div>
					  
				
                               </td> 
		           </tr>    
                                  
                                  
                                  
                                  
                                  
                                  
                                  
    
                                  </tbody>
                           </table>
    
                     </div>
          </div>
    </div>
</div>






 
 
 
