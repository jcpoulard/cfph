<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcStudentOtherInfo;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */
/* @var $form yii\widgets\ActiveForm */
				
$acad = Yii::$app->session['currentId_academic_year'];

Yii::$app->session['tab_index']=0;

?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 


<?php 

$grades_comment = infoGeneralConfig('grades_comment');	

if($grades_comment==0)
  {
	 $item_array= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        /* [
			             'attribute'=>'student',
						 'label'=>Yii::t("app","Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->getFullName();
							 }
						  ],
                       */
                       
                        [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                         [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						   
                        [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	         
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	       $grade_value ='';
						 	      
						 	        if(isset($data->grade_value))
						 	          $grade_value = $data->grade_value;
						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
	  
	  
	  
  }
elseif($grades_comment==1)
{
	$item_array= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
					/*	[
			             'attribute'=>'student',
						 'label'=>Yii::t("app","Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->getFullName();
							 }
						  ],
					  */

					     [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                         [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						  
					    [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	          
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	      $grade_value ='';
						 	      
						 	        if(isset($data->grade_value))
						 	          $grade_value = $data->grade_value;
						 	       						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'"  type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  
                          [
			             'attribute'=>'comment',
						 'label'=>Yii::t("app","Comment"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	         
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	     $comment ='';
						 	      
						 	        if(isset($data->comment))
						 	          $comment = $data->comment;
						 	       
						 	      	$val ='<input name="comments['.$data->id.']" value ="'.$comment.'" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					                 <input name="id_grade_['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ], 
			  
			        ];
	  
	  
	  
			        
	
}
		 

?>



<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>
  	<?php  
        
      if( ( (isset($_GET['all'])&&($_GET['all']==1))&&(!isset($_GET['from']) ) ) )
       {  
	        $cours = SrcCourses::findOne(Yii::$app->session['grades_course']);
	        if(Yii::$app->session['grades_course']!='')
	            $model->course = [Yii::$app->session['grades_course']];
        }
    	
    	if( (!isset($_GET['all']))||($_GET['all']!=1) || ( (isset($_GET['all'])&&($_GET['all']==1))&&(isset($_GET['from'])&&($_GET['from']=='list')) ) )
    	   {
    	   	
    	   	$modelShift_ = new SrcShifts();
             $modelCourse = new SrcCourses();
        ?>
         <div class="row">
          <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-shift").html(data);	
                                       //chanje course 
                                        $("#srcgrades-room").html("");
                                        	
                                        });',
                                     
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
          
        
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         $data_shift = [];
              if($model->shift[0]!='')

                {       
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                  }      
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftforgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-room").html(data);	
                                       //chanje course 
                                        $("#srcgrades-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        $data_room = [];
              if($model->room[0]!='')
                {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                       //  ->all(),'room0.id','room0.room_name' );
                           $from = 'grades';
                         $data_room = $modelCourse->getRoominshiftforgrades_($from,$model->shift[0],$model->program,$model->level,$acad);
                  }      
                 ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>'
                                        $.post("../../fi/courses/courseingroupforgrades?from=grades&group='.'"+$(this).val()+"&shift='.'"+$("#srcgrades-shift").val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-course").html(data);	
                                        	
                                        });',
  
                       ],
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  <?php        $data_course = [];
              if($model->course[0]!='')
                { 
                  /*	$data_course = ArrayHelper::map(SrcCourses::find()->joinWith(['module0'])->where(['room'=>$model->room,'shift'=>$model->shift,'program'=>$model->program])
                         ->all(),'id','module0.subject0.subject_name' );
                   */
               
                   
                $data_course = $modelCourse->getCourseingroupforgrades($model->room[0],$model->shift[0],$model->program[0],$model->level,$acad);
                  }      
                 ?> 
     
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>$data_course,//ArrayHelper::map(Courses::find()->all(),'id','module0.subject0.subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    'onchange'=>'submit()', ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>       
          
           </div>
    </div>
    
 <?php  } 
 ?>

			<?php 	
			
			 //$weight = ' ';
			  $modelCourses = new SrcCourses();
			  $modelGrades = new SrcGrades();
			  //$model = SrcGrades::findOne($_GET['id']);
			 // $_model= $modelGrades->findOne($_GET['id']);
			  $course_weight =$modelCourses->getWeight(Yii::$app->session['grades_course']); 
                      
		if(Yii::$app->session['profil_as'] ==0)
         {	//$dataProvider= $modelGrades->searchByCourse(Yii::$app->session['grades_course']);
			$dataProvider= $modelGrades->searchByShiftLevelGroupCourse(Yii::$app->session['grades_shift'],Yii::$app->session['grades_level'],Yii::$app->session['grades_group'],Yii::$app->session['grades_course']);
			
         }
      elseif(Yii::$app->session['profil_as'] ==1)
         $dataProvider= $modelGrades->searchByGroupCourse(Yii::$app->session['grades_group'],Yii::$app->session['grades_course']);
   ?>
			
<ul class="nav nav-tabs"></ul>  
<br/> 

<?php
if($model->course[0]!=null)
{

?>

<?php 		
//error message  
	        	echo '<div class="" style="margin-top:-10px; margin-bottom:10px;">';
				   	 
				    echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	  
				 /* 
					if($success)
				     { echo '<span style="color:green;" >'.Yii::t('app','Operation terminated successfully.').'</span>'.'<br/>';
					   $success=false;
				      }
*/
				    
					if($message_GradeHigherWeight)
					  {	 if(!isset($_GET['stud'])||($_GET['stud']==""))
		                     echo '<span style="color:red;" >'.Yii::t('app','Grades GREATER than COURSE WEIGHT are not saved.').'</span>'.'<br/>';
						 else
						   echo '<span style="color:red;" >'.Yii::t('app','Grade VALUE can\'t be GREATER than COURSE WEIGHT!').'</span>'.'<br/>';
					    $message_GradeHigherWeight=false;
					  }
                   
                          

					    echo '<span style="color:blue;" ><b>'.Yii::t('app','- COURSE WEIGHT : ').$course_weight.' - </b></span>';
					
					 echo'</td>
					    </tr>
						</table>';
					
           echo '</div>';	
           
           						
			 
echo GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array, 
                          
       ]); 

				
				
				
if( ($dataProvider->getModels()!=null)&&($message_UpdateValidate==false) )
 {
			 ?>
     
 <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Update'), ['name' => 'update', 'class' => 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>


<?php

  }
  
}

?>
 
    <?php ActiveForm::end(); ?>

<br/><br/><br/>

</div>



<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    
        // e.keyCode = 37(left), 38(up), 39(right), 40(down)
        
  $('input').keypress(function(e){
   var key=e.keyCode || e.which;
       
   //alert("@-"+key);
        if( (key==13)||(key==40) ){ 
        var x = 1;
         var ind = $(this).attr("tabindex");  
          var next_ind =0;
             next_ind = Number(ind) + Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
        else if(key==38){  //up
        
            var x = 1;
            var ind = $(this).attr("tabindex");  
            var next_ind =0;
             next_ind = Number(ind) - Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
    });  		
				
	

JS;
$this->registerJs($script);

?> 



		   
			
