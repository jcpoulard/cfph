<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcAcademicperiods;


				
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div >  

  
    
    
<?php   
 $modelShift_ = new SrcShifts();
  $modelCourse = new SrcCourses();
    

  
  
  $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
     
$school_name_school_acronym = $school_name; 

//if($school_acronym!='')
//  $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	


	
 ?>

	
    


<center >

    
    <div style="width:100%; margin: auto;" >
		
        <?php
                
        $acadModel = SrcAcademicperiods::findOne($acad);
				   
	echo '<table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> '.headerLogo().'
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
                                 
                 ';

        
                                                     
//  	
// if($premye_tou!=0)
//  {
//  	 echo '<pagebreak />  
//           <br/> ';
//  	}
// else
//   { $premye_tou=1; 
//      echo '<br/> '; 
//   }	
//  	
  	
 

$modelGrades = new SrcGrades();                      	

$dataProvider = $modelGrades->searchStudentsGradeForCourseByGroup($course,$room);


$result = $dataProvider->getModels();
?>
                    	          	
<div class="content">
	<div class="container">
		
		<div class="row">
			<div class="Container">
		    	<h5 class="content-title" style="text-align:center;">
					<span><b> <?php  echo strtr( strtoupper( Yii::t('app', 'Fiche de passation d\'épreuve sommative') ), capital_aksan() );   ?> 
						                 
					</b></span>
				</h5>
		   	</div>
                    
                    <i>
                    <?php
                               $modelRoom = SrcRooms::findOne($room);
                               
                            $modelCourse = SrcCourses::findOne($course); 
         
                         echo $modelCourse->module0->subject0->subject_name." (".$modelCourse->teacher0->getFullName().") [".$modelCourse->module0->code."] - ".$modelRoom->room_name."/".$modelCourse->shift0->shift_name ; 

                    ?>
                    </i>
		</div>
	
		<div class="row">
			<div class="container">
                            
                            <table class="table table-bordered table-striped jtable" style="font-size: 9px;">
				         <thead>
				            <tr>
                                            <th>#</th>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Grade'); ?></th>
				            
				            </tr>
				        </thead>
				        <tbody>
				
			  <?php
        $i =1;
   foreach($result as $rest)
			      { 
			?>	  
				  <tr>  
				     <td > <?=  $i ?> </td>
                                     <td > <?=  $rest->student0->id_number ?> </td>
				     <td > <?=  $rest->student0->first_name ?> </td>
				     <td > <?= $rest->student0->last_name  ?> </td>
				     <td > <?php
                                                  $grade_value ='';
						 	      
						 	    /*    if(isset($rest->course0->students->grades0->id))
						 	          { foreach($data->course0->students->grades0 as $r)
						 	       	     $grade_value = $r->grade_value;//id;
						 	          }
                                                             * 
                                                             */
						 	       
						 echo $rest->grade_value;
                                           ?> </td>
				     
				  </tr>
			<?php
                        
                                  $i++;
			      }
                              

           
                          ?> 
			   
			  
                                </tbody>
                            </table>
                            
			</div>
                    
                    
                    
		</div>
            <div style="text-align: center;">  <hr style="margin-bottom: 0px; width: 30%; float: left;" >
                   <?php echo $modelCourse->teacher0->getFullName(); ?>
                    </div>
            
            <div > <?= Yii::$app->formatter->asDate(date('Y-m-d') ) ?>
                 </div>
            
	</div>
</div>
       






    </div>			   	

</center>	


</div>

 
 
 
