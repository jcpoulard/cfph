<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;


				
$acad = Yii::$app->session['currentId_academic_year'];
?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 

<div >  

  
    
    
<?php   
 $modelShift_ = new SrcShifts();
  $modelCourse = new SrcCourses();
    

  
  
  $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
     
$school_name_school_acronym = $school_name; 

//if($school_acronym!='')
//  $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	


	
 ?>

	
    


<center >

    
    <div style="width:100%; margin: auto;" >
		
       							

			<?php 	
			  $stud_order_desc_average = array();
				
                          $konte =1;
                
			  // $person= new Persons;
			   $tot_stud=0;
			   $showButton=false;
			   $passing_grade='N/A';
			   
			   $stud_has_courseModel = new SrcStudentHasCourses();
			   
			   $include_discipline=0;
			
		     
	 if($room!='')
		{	//tcheke si gen not pou sal sa deja
			if(infoGeneralConfig('course_shift')==0)
	          { //$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	              $sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN student_has_courses shc ON(c.id=shc.course) INNER JOIN student_level sl ON(sl.student_id=shc.student) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND sl.level='.$level.' AND sl.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	          }
	        elseif(infoGeneralConfig('course_shift')==1)
	           { //$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.shift='.$shift.' AND c.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	              $sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) INNER JOIN student_has_courses shc ON(c.id=shc.course) INNER JOIN student_level sl ON(sl.student_id=shc.student) INNER JOIN module m ON(m.id=c.module) WHERE m.program='.$program.' AND c.shift='.$shift.' AND sl.level='.$level.' AND sl.room='.$room.' AND c.academic_year='.$acad.' ORDER BY g.id DESC';
	          }
															 
			$command__ = Yii::$app->db->createCommand($sql__);
			$result__ = $command__->queryAll(); 
			
		 if($result__!=null) 
		  {														       	   
			foreach($result__ as $r)
			 {  if($r['id']!=null) 
			      $gen_done=true;
			       break;
			  }
		   }
						  
         if($gen_done)
		   {	 //totall etudiant ki nan sal sa kap swiv kou sila
			 $dataProvider_studentEnrolled= $stud_has_courseModel->getStudentsEnrolledGroup($room, $level, $shift, $acad);
			  if(isset($dataProvider_studentEnrolled))
			    { 
				   foreach($dataProvider_studentEnrolled as $tot)
				       $tot_stud=$tot->total_stud;
				}
			 
			     			    
			   //Extract average base
			  $average_base = infoGeneralConfig('average_base');
								   				
								   				 
			 $dataProvider_studentEnrolledInfo = $stud_has_courseModel->getInfoStudentsEnrolledGroup($room, $level, $shift, $acad); 
			 
			
			    $moduleGrade_ = new SrcGrades(); 
			
			$dataProvider = $moduleGrade_->getAllSubjectsByLevel($program,$shift,$level,$room);
			
				
				
				//Grades for the current period
										 							   				   
												  
				//liste des etudiants
			 $dataProvider_student = $stud_has_courseModel->getInfoStudentsEnrolledGroup($room, $level, $shift, $acad);
			 
		
		  }
			 
			 
			 
		}	
              $couleur=1;
			  $k=0;
			 //
		$programModel = SrcProgram::findOne($program);
                $shiftModel = SrcShifts::findOne($shift);
                $roomModel = SrcRooms::findOne($room);	
				   
	echo '<table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> '.headerLogo().'
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
                                 
                             
<div style="width:100%; text-align:center; ">  <b>'.strtr( strtoupper( Yii::t('app','Grade sheet') ), capital_aksan() ).'</b> </div>
                <div  ><div style="float:left; width:100%; font-size:12px;"> <b>'.$programModel->label.' / '.$shiftModel->shift_name.' / '.$roomModel->room_name.'</b>
                             </div>	     </div>
                             
<div style="margin-top:0px;  margin-bottom: 20px; float:left;   width:100%;  overflow-x:scroll; overflow-y:hidden;  background-color:#EFF4F8;">  
	
	       <table class="" style="  width:100%; background-color:#EFF4F8; color: #1E65A4; font-size:12px;">
  
      <thead class="" >
           <tr style="background-color:#E4E9EF;">
				    <th style="background-color:#E4E9EF; width:20px; color:#E4E9EF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. </th>
				    <th style="background-color:#E4E9EF; width:170px; color:#E4E9EF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. </th>
                                    <th style="background-color:#E4E9EF; width:170px; color:#E4E9EF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;. </th>
				
				          ';
		
              		//liste des cours
		while(isset($dataProvider[$k][0]))
				{
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {  $id_course = $dataProvider[$k][4];
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
							  if($old_subject_evaluated)
								 {
								 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
									}
		
						 }
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
						}
						
						         
			      
			      if($careAbout)
				     {			
						
				// $dataProvider[$k][1] subject_name
				// $dataProvider[$k][3] short_subject_name
                                  // $dataProvider[$k][6] teacher_name
				 
				 echo ' <th style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;">'.'<span data-toggle="tooltip" title="'.$dataProvider[$k][1].' ( '.$dataProvider[$k][6].' )"> '.$dataProvider[$k][5].'</span></th>';//<-code module  //.$dataProvider[$k][3].'</span></th>';
				   
				      }
				      
				      $k++;
		        }
echo '	
     </tr>
  
  <tr style=""> 
     <th style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:20px;">'.Yii::t('app',"#").' </th>'
        . '<th style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:170px;">'.Yii::t('app',"First Name").' </th>
     <th style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:170px;">'.Yii::t('app',"Last Name").' </th>
	<!-- <th class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 3px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:40px;">'.Yii::t('app',"Average").' </th> -->
    ';   
    
		
		//coefficients
		$k=0;   		
		
		while(isset($dataProvider[$k][0]))
				{
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {  $id_course = $dataProvider[$k][4];
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
							  if($old_subject_evaluated)
								 {
								 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
									}
		
						 }
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
						}
						
						        
			  
			      if($careAbout)
				     {			
							 echo '<th class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px ">'.$dataProvider[$k][2].'</th>';
														
					}
					
					$k++;
		  }
 echo '</tr>
	     	
	   	</thead >
	   	
	   	<tbody class="">
          
  	';
  	
  	$style_tr="";
  	
  	    //pou chak elev
			if($gen_done)		       
			  {		       if(isset($dataProvider_student)&&($dataProvider_student!=null))
			                 {  
				                 $students=$dataProvider_student;
								
				             foreach($students as $stud)    //foreach($stud_order_desc_average as $stud)
				              {  
				              	 $k=0;
				              	 
							    if($couleur===1)  //choix de couleur																
									 $style_tr="font-size: 12px; background-color: #F0F0F0; height:28px; padding-left:15px;";
								 elseif($couleur===2)
									$style_tr="font-size: 12px; background-color: #FAFAFA; height:28px; padding-left:15px;";
									
							     echo '<tr style="'.$style_tr.'">
								            <td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ;"> 
				  								'.$konte.'</td>
				  								
				  							<td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; width:170px; "> 
				  								<b>'.$stud->first_name.'</b></td>
				  								
				  							<td style="'.$style_tr.' border-right: 3px solid  #E4E9EF ; width:170px; "> 
				  								<b>'.$stud->last_name.'</b></td>';
				  								
				  								
								
									   	 	
										
									 
											  while(isset($dataProvider[$k][0]))
				                                {
	                                               if($dataProvider[$k][4]!=NULL) //reference_id
										 	         {  $id_course = $dataProvider[$k][4];
														 //si kou a evalye pou peryod sa
														$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($course);         
														  if($old_subject_evaluated)
															 {
															 	$careAbout=$moduleGrade_->isOldSubjectEvaluated($dataProvider[$k][4]);						                        
															  }
														   else
															 {  
															 	$careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);						                       															                       
																}
									
													 }
												 else
													{  $id_course = $dataProvider[$k][0];
													    $careAbout=$moduleGrade_->isSubjectEvaluated($dataProvider[$k][0]);
													}
													
													         
			  
			                                      if($careAbout)
				                                     {			
							                           
							                            //calculer grade puis afficher
							                                    $debase_passingGrade=0;
							                            $debase='';
							                             $weight='';
							                             $id_course=0;
							                             $passing_value=0;
							                             
							                              if($dataProvider[$k][4]!=NULL) //reference_id
												 	         {  $id_course = $dataProvider[$k][4];
																 //si kou a evalye pou peryod sa
																$old_subject_evaluated=$moduleGrade_->isOldSubjectEvaluated($id_course);         
																  if($old_subject_evaluated)
																	 {
																	 	$id_course = $dataProvider[$k][4];						                        
																	  }
																   else
																	 {  
																	 	$id_course = $dataProvider[$k][0];						                       															                       
																		}
											
															 }
														 else
															{  $id_course = $dataProvider[$k][0];
															    
															}
							                            
							                            
							                              	//note de passage du cours
														      $modelCourse = new SrcCourses();
														    $modelCourse=SrcCourses::findOne($id_course); 
														       $passing_value =  $modelCourse->passing_grade;//note de passage pour le cours
																  
															$grades= $moduleGrade_->searchForReportCard($stud->id,$id_course);
															 
																	if(isset($grades))
																		{
														                        $r=$grades ;//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade)
																			      {
																			        echo '<td style="text-align:center; width:100px; border-right: 1px solid  #E4E9EF">';
																			        echo '<div class="" style="';		
			     	                                                              echo '">';
																			        if($grade->grade_value!=null)
																			           { //date_format($date,'Y-m-d H:i:s')
																			           	  $date_validation = Yii::$app->formatter->asDateTime($grade->date_validation);
																			           	  $validated_by = $grade->validated_by;
																			           	  
																			           	  echo '<div class="" style="';		
			     	                                                                        if($grade->validate==0)
																			           	      echo 'color:black;">';
																			           	    elseif($grade->grade_value< $passing_value )
																			           	     { 
																			           	        echo 'color:red;">';
																			           	      }
																			           	    elseif($grade->validate==1)
																			           	      echo 'color:green;">';
																			           	      
																			           	    	 echo '<span data-toggle="tooltip" title="'.$grade->comment.'"> '.$grade->grade_value.'</span>';
																			           	    	 
																			           	   echo '</div>';
																			           	     
																			           	    
																			           }
																		           	 else
																			           {  echo '<div class="" style="';		
			     	                                                                        if($grade->validate==0)
																			           	      echo 'black;">';
																			           	    elseif($grade->grade_value< $passing_value)
																			           	     { 																			           	       										 echo 'color:red;">';
																			           	      }
																			           	    elseif($grade->validate==1)
																			           	      echo '">';
																			           	   
																			           	         echo 'N/A';
																			           	         
																			           	   echo '</div>';
																			           	   
																			             } 
                                                                                     
                                                                                      echo '</div>';
                                                                                      
                                                                                     echo '</td>';
                                                                                                                                                        
                                                                                                                                                   
															                        }
																	             }
																                else
																                  { echo '<td style=" text-align:center;  border-right: 1px solid  #E4E9EF ">';echo '<div class="" style="';		
			     	                                                                       /// if($grade->validate==0)
																			           	   
																			           	      echo '">';
																			           	   
																			           	         echo 'N/A';
																			           	         
																			           	   echo '</div>'; 
																			           	   
																			           	echo '</td>';
																                  }
																	$showButton=true;
																	
																         }//fin if(isset($grades))
															
														}//fin careAbout
														
														$k++;
					                    
				                                }//fin while(isset($dataProvider[$k][0]))								  
									   $couleur++;
	                                if($couleur===3) //reinitialisation
				                       $couleur=1;
								echo '</tr>';
								
                                                              $konte++;  
                                                                
                                                               }// fin foreach($students as $stud)
							  
						
							  
							  
							  
							  }// fin if(isset($dataProvider_student))
							  
							  
			          }
							  
                    echo '
                    
                     </tbody>
                          
                           </table>
                    
               </div>     
                          
                          
                           '; 	  
				   				  
		?>
        <div class="right" style="font-size: 8px; float: right;">
               <?=  Yii::t('app', 'Printing date').': '.Yii::$app->formatter->asDate(date('Y-m-d')) ?>
           
            </div>
	</div>			   	

</center>	


</div>

 
 
 
