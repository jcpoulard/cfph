<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcGrades */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reprise');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
         
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content grades-index">

                   <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
                                            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','Program'); ?></th>
                                            <th><?= Yii::t('app','Room'); ?></th>
				           
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

            
    $dataGrade = $dataProvider->getModels();
    
          foreach($dataGrade as $grade)
           {  $href='';
           	   if(Yii::$app->session['profil_as'] ==0)
				  {	
				  	$href= '../grades/viewreprise?stud='.$grade->student.'&is_stud=1';
				  }
				elseif(Yii::$app->session['profil_as'] ==1)
				  {
				  	$href='#';
				  	}
           	
           	   echo '  <tr >
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$grade->student0->first_name.' </a></td>
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$grade->student0->last_name.' </a></td>
                                                    <td ><a style="color:#676A74;" href="'.$href.'" >'.$grade->student0->id_number.' </a></td>
                                                    <td ><span data-toggle="tooltip" title="'.$grade->course0->module0->program0->label.'">'.$grade->course0->module0->program0->short_name.' </span> </td>
                                                    <td >'; if(isset($grade->student0->studentLevel->room0->room_name) ) echo $grade->student0->studentLevel->room0->room_name; else echo ''; echo ' </td>
                                                   
                                                 
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


</div>



<?php
        if(Yii::$app->session['profil_as'] ==0)
            $buttons = 'buttons: [
                   // { extend: \'copy\'},
                   // {extend: \'csv\'},
                    {extend: \'excel\', title: \'Grades List\'},
                    {extend: \'pdf\', title: \'Grades List\'},

                    {extend: \'print\',
                     customize: function (win){
                            $(win.document.body).addClass(\'white-bg\');
                            $(win.document.body).css(\'font-size\', \'10px\');

                            $(win.document.body).find(\'table\')
                                    .addClass(\'compact\')
                                    .css(\'font-size\', \'inherit\');
                    }
                    }
                ]';
        elseif(Yii::$app->session['profil_as'] ==1)
             $buttons = 'buttons: []';
        
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                $buttons

            });

        });

JS;
$this->registerJs($script);

?>
    
