<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;

use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

use kartik\editable\Editable;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\RepriseGrades;

/* @var $this yii\web\View */
/* @var $model app\modules\billings\models\LoanOfMoney */

$this->title = '';

$acad = Yii::$app->session['currentId_academic_year'];

$modelPerson = SrcPersons::findOne($_GET['stud']);
 
?>

<?php 
						
?>

<div class="loan-of-money-view">

  

<div class="row">
    <div class="col-lg-7">
         <h3> 
            <?php   
                       echo Yii::t('app','Reprise view for').' : '.Html::a($modelPerson->getFullName(), ['/fi/persons/moredetailstudent', 'id' => $modelPerson->id, 'is_stud'=>1, 'from' =>'stud' ], ['class' => '']);                   
                    
                        
                  ?>
    
    </h3>
    </div>
    <div class="col-lg-5">
        <p>
   
     <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['reprise', ], ['class' => 'btn btn-info btn-sm']) ?>
        

    </p>
    </div>
</div> 




   

<div style="clear:both"></div>   


<div class="row">
   
           
<div class="col-lg-10">
    
     <div class="col-md-14 table-responsive">
	 <table class='table table-striped table-bordered detail-view'>
				         
				            
 <?php
       foreach($data as $da)
        {
           echo ' <tr>
		  <th>'.$da->course0->module0->subject0->subject_name.'</th>
                     <td>';
                         echo ' <table class="table table-striped table-bordered detail-view">
                                 <tr>
                                     <th>Reprise 1</th>
                                     <th>Reprise 2</th>
                                     <th>Reprise 3</th>
                                 </tr>
                                 <tr>';
                                  
                             $modrelReprise = RepriseGrades::find()->where('student='.$modelPerson->id.' and course='.$da->course0->id)->all();
                          if($modrelReprise== NULL)
                              {
                               echo ' <td  >';
                                    if(Yii::$app->user->can('fi-grades-setreprise'))     
                                     echo ' <a href="#" class="r1" data-name=""  data-pk="0"  data-url="set-reprisegrades?set=r1&stud='.$modelPerson->id.'&course='.$da->course0->id.'" data-title="'.Yii::t('app','Reprise 1').'"></a>';
                                    else
                                        echo Yii::t('app', 'N/A');
                              echo '</td>
                            <td  ></td>
                            <td  ></td>
                            ';
                              
                            
                              }else{
                                  foreach ($modrelReprise as $reprise_grade)
                                    {
                                        if($reprise_grade->grade_value_1==null)
                                          {     
                                              echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r1" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r1" data-title="'.Yii::t('app','Reprise 1').'">
                                   
                               </a>';
                                                else
                                                    echo Yii::t('app', 'N/A');
                                          echo '</td>';

                                          }
                                        else 
                                          {  
                                              echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r1" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r1" data-title="'.Yii::t('app','Reprise 1').'">
                                   '.$reprise_grade->grade_value_1.'
                               </a>';
                                                else
                                                    echo $reprise_grade->grade_value_1;
                                          echo '</td>';
                                          
                                          }
                                        
                                        if($reprise_grade->grade_value_2==null)
                                         {  
                                              echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r2" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r2" data-title="'.Yii::t('app','Reprise 2').'">
                                   
                               </a>';
                                                else
                                                    echo Yii::t('app', 'N/A');
                                          echo '</td>';
                                         
                                         }
                                     else 
                                       {  
                                              echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r2" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r2" data-title="'.Yii::t('app','Reprise 2').'">
                                   '.$reprise_grade->grade_value_2.'
                               </a>';
                                                else
                                                    echo $reprise_grade->grade_value_2;
                                          echo '</td>';
                                          
                                       }
                                     
                                        if($reprise_grade->grade_value_3==null)
                                          {  
                                               echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r3" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r3" data-title="'.Yii::t('app','Reprise 3').'">
                                   
                               </a>';
                                                else
                                                    echo Yii::t('app', 'N/A');
                                          echo '</td>';
                                          
                                          }
                                     else 
                                       {  
                                            echo ' <td  >';
                                                if(Yii::$app->user->can('fi-grades-setreprise'))     
                                                 echo ' <a href="#" class="r3" data-name=""  data-type="text" data-pk="'.$reprise_grade->id.'"  data-url="set-reprisegrades?set=r3" data-title="'.Yii::t('app','Reprise 3').'">
                                   '.$reprise_grade->grade_value_3.'
                               </a>';
                                                else
                                                    echo $reprise_grade->grade_value_3;
                                          echo '</td>';
                                          
                                       }
                                     
                                    }
                                  
                              }
                       echo '</tr>
                           </table>';
               echo '</td>';
           echo '</tr>';
        }
 ?>	
   
	
        
    </table>

</div>



<div class="col-lg-6">



</div>
    
    
<div class="col-lg-2 text-center">
                                                     
          </div>    
      
</div>

<div style="clear:both"></div>
 <!-- Seconde ligne -->
 

<div class="row">
            
<div class="col-lg-12">

</div>


</div>

</div>


   
<?php

    
    $script = <<< JS
    $(document).ready(function(){
            
            $('.r1').editable();
            $('.r2').editable();
            $('.r3').editable();
            
            

        });

JS;
$this->registerJs($script);






?>


        <div class="modal fade" id="modal-default" >
          <div class="modal-dialog">
            <div class="modal-content" style="width: 56%;">
              <div class="modal-header" >
                <h3 class="modal-title">Reprise 1</h3>
              </div>
              <div class="modal-body">
                  <form class="form-inline editableform" style="">
                       <div class="control-group form-group">
                                 <div>
                                     <div class="editable-input" style="position: relative;">
                                         <input type="text" class="form-control input-sm" style="padding-right: 24px;">
                                           <span class="editable-clear-x"></span>
                                     </div>
                                     <div class="editable-buttons">
                                         <button type="submit" class="btn btn-primary btn-sm editable-submit">
                                             <i class="glyphicon glyphicon-ok"></i>
                                         </button>
                                         <button type="button" class="btn btn-default btn-sm editable-cancel" data-dismiss="modal">
                                             <i class="glyphicon glyphicon-remove"></i>
                                         </button>
                                     </div>
                                 </div>
                                 <div class="editable-error-block help-block" style="display: none;"></div>
                                     
                             </div>
                  </form>
              </div>
              
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->          
        
      