<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Grades */


if(!isset($_GET['all']))
      $this->title = Yii::t('app', 'Update grade {nameCourse} for {nameStudent} ', [
    'nameCourse' => $model->course0->module0->subject0->subject_name, 'nameStudent' => $model->student0->getFullName(),
    ]);
elseif($_GET['all']==1)
    { 
    	 //if( ( (isset($_GET['all'])&&($_GET['all']==1))&&(!isset($_GET['from']) ) ) )
    	     //$modelCourses->SrcCourses
		    	$cours = SrcCourses::findOne(Yii::$app->session['grades_course']);
		    	if($cours!=null)
			    	$this->title = Yii::t('app', 'Update grades {nameCourse}', [
			    'nameCourse' => $cours->module0->subject0->subject_name,
			    ]);
		        else 
		            $this->title = Yii::t('app','Update grades');
    
    }

?>

<div class="row">
    
</div>
<div class="row">
    <div class="col-lg-7">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
<p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create',], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', ], ['class' => 'btn btn-info btn-sm']) ?>
       
       
        <?php
        /*  if( ( (!isset($_GET['all'])||($_GET['all']!=1)) ) )
           echo Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        */ ?>
    </p>
    </div>
</div>
<div class="wrapper wrapper-content grades-update">

   
    <?php
    
         if(!isset($_GET['all']))
            echo  $this->render('_update', [
                     'model' => $model,
                     'message_UpdateValidate'=>$message_UpdateValidate,
				       'message_UpdateValidate1'=>$message_UpdateValidate1,
				       'message_UpdatePublish'=>$message_UpdatePublish,
				       'message_noGrades'=>$message_noGrades,
				       'message_gradesOk'=>$message_gradesOk,
				       'messageNoCheck'=>$messageNoCheck,
				       'message_GradeHigherWeight'=>$message_GradeHigherWeight,
                 ]);
          elseif($_GET['all']==1)
               echo  $this->render('_updateByCourse', [
                     'model' => $model,
                     'message_UpdateValidate'=>$message_UpdateValidate,
				       'message_UpdateValidate1'=>$message_UpdateValidate1,
				       'message_UpdatePublish'=>$message_UpdatePublish,
				       'message_noGrades'=>$message_noGrades,
				       'message_gradesOk'=>$message_gradesOk,
				       'messageNoCheck'=>$messageNoCheck,
				       'message_GradeHigherWeight'=>$message_GradeHigherWeight,
                 ]);
          
           
    ?>

</div>
