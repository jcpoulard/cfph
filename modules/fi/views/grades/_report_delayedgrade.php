<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcAcademicperiods;

use app\modules\rbac\models\User;


$acad_ = Yii::$app->session['currentId_academic_year'];

//previous acad
$SrcAcademicPeriods = new SrcAcademicperiods;
$acad= $SrcAcademicPeriods->getPreviousAcademicYear($acad_);


Yii::$app->session['report_tab_index']=0;

?>

<input id="acad" type="hidden" value="<?= $acad ?>" /> 



<?php 
					
$grades_comment = infoGeneralConfig('grades_comment');	

 $modelShift_ = new SrcShifts();
 $modelCourse = new SrcCourses();

 
if($grades_comment==0)
  { 
	 $item_array_1= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                                       [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	     
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	      $grade_value ='';
						 	      
						 	        if(isset($data->course0->students->grades0->id))
						 	          { foreach($data->course0->students->grades0 as $r)
						 	       	     $grade_value = $r->grade_value;//id;
						 	          }
						 	       
						 	       $grade_value = $data->grade_value;
						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'"  type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
	  
	  
	  $item_array_2= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			             [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                         [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
						 	        $val ='<input name="grades['.$data->student.']" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_stud['.$data->student.']" type="hidden" value="'.$data->student.'" />';
					               return $val;
						        },
						  ],
			  
			        ];
  }
elseif($grades_comment==1)
{
	$item_array_1= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			         /*    [
			             'attribute'=>'student',
						 'label'=>Yii::t("app","Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->getFullName();
							 }
						  ],
					*/  
						  [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                         [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	   
                                                      $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	       $grade_value ='';
						 	      
						 	        if(isset($data->grade_value))
						 	          $grade_value = $data->grade_value;
						 	       
						 	        $val ='<input name="grades['.$data->id.']" value ="'.$grade_value.'"  type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_grade['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ],
						  
						  
                          [
			             'attribute'=>'comment',
						 'label'=>Yii::t("app","Comment"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	  
                                                     $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
						 	     $comment ='';
						 	      
						 	        if(isset($data->comment))
						 	          $comment = $data->comment;
						 	       
						 	       						 	       
						 	      	$val ='<input name="comments['.$data->id.']" value ="'.$comment.'" disabled="disabled" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					                 <input name="id_grade_['.$data->id.']" type="hidden" value="'.$data->id.'" />';
					               return $val;
						        },
						  ], 
			  
			        ];
	  
	  
	  $item_array_2= [
				        ['class' => 'yii\grid\SerialColumn',
				           // 'header' => 'No',
				           'options' => ['width' => '50px'], 
				        ],
				        
				        
			            
			          /*   [
			             'attribute'=>'student',
						 'label'=>Yii::t("app","Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->getFullName();
							 }
						  ],
						*/ 
						  
						 [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","First Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->first_name;
							 }
						  ],

                         [
			             //'attribute'=>'student',
						 'label'=>Yii::t("app","Last Name"),
						 'format'=>'text', // 'raw', // 'html', //
						 'content'=>function($data){
							 return $data->student0->last_name;
							 }
						  ],
						  
						  [
			             'attribute'=>'grade_value',
						 'label'=>Yii::t("app","Grade Value"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	
                                                     $i = Yii::$app->session['tab_index'];
                                                     $i= $i+1;
                                                     Yii::$app->session['tab_index']=$i;
                                                     
                                                      $val ='<input name="grades['.$data->student.']" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					   <input name="id_stud['.$data->student.']" type="hidden" value="'.$data->student.'" />';
					               return $val;
						        },
						  ],
						  
						 [
			             'attribute'=>'comment',
						 'label'=>Yii::t("app","Comment"),
						 'format'=>'raw', //'raw', // 'text', //  
						 'value'=>function($data){
						 	    
                                                            $i = Yii::$app->session['tab_index'];
                                                            $i= $i+1;
                                                            Yii::$app->session['tab_index']=$i;
                                                     
						 	      	$val ='<input name="comments['.$data->student.']" type=text tabindex="'.$i.'" style="width:97%;" />
			          
					                 <input name="id_stud['.$data->student.']" type="hidden" value="'.$data->student.'" />';
					               return $val;
						        },
						  ], 
			  
			        ];
			        
	
}
		 
 
   
           
  if(isset($_GET['msguv'])&&($_GET['msguv']=='y'))
       {    $message_UpdateValidate=true;
              }
           
 	
	
?>

<div class="grades-form">

    <?php $form = ActiveForm::begin(); ?>

 <div class="row">
        <div class="col-lg-6">
            
         <?php        
         
                   
        echo $form->field($model, 'transcriptItems')->widget(Select2::classname(), [
			                                   'data'=>loadTranscriptItems(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
 </div> <br/>
 <div class="row">
     <div class="col-lg-6">
            <?= $form->field($model, 'is_particular_case')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;','onchange'=>'submit()',]) ?>
        </div>
 </div>
 
 <br/>
    
      <div class="row">
  
<?php
   if(Yii::$app->session['profil_as'] ==0)
     {
?>				                                   
        
        <div class="col-lg-2">
            
         <?php        
         
                   
        echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>

        <div class="col-lg-2" style="margin-left:18px; padding:0px;">
           <?php
                  $data_program = ArrayHelper::map(SrcProgram::find()->all(),'id','label' );
		      
                  if($model->program!='')
                   {
                       	$data_shift = [];
                     }
                   
		    
        
           ?>
               <?= $form->field($model, 'program')->widget(Select2::classname(), [
                       'data'=>$data_program,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    'onchange'=>'
                                       //chanje shift
                                        $.post("../../fi/courses/shiftinprogram?from=grades&program='.'"+$(this).val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-shift").html(data);	
                                       $("#srcgrades-room").html("");
                                        //chanje course 
                                        $("#srcgrades-course").html("");
                                        	
                                        });',
                                     
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          </div>
          
        
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
        <?php 
         
              //if($model->shift!='')

                //{ 
                    $modelShift_ = new SrcShifts();
                 	   
                      $data_shift = $modelShift_->getShiftinprogram($model->program,$acad);
                 // }
               
                 ?>  
                     <?= $form->field($model, 'shift')->widget(Select2::classname(), [
                       'data'=>$data_shift, //ArrayHelper::map(SrcShifts::find()->all(),'id','shift_name' ) ,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select shift  --'),
                                     'onchange'=>'
                                       //chanje room
                                        $.post("../../fi/courses/roominshiftfordelayedgrades_?from=grades&shift='.'"+$(this).val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                                        $("#srcgrades-room").html(data);	
                                       //chanje course 
                                        $("#srcgrades-course").html("");
                                        	
                                        });',
                           ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
                    
         <?php        
              $data_room = [];
              if($model->room!='')
                {
                	//$data_room = ArrayHelper::map(SrcCourses::find()->joinWith(['module0','students.studentLevel'])->where(['shift'=>$model->shift,'program'=>$model->program,'student_level.level'=>$model->level,'academic_year'=>$acad])
                      //   ->all(),'room0.id','room0.room_name' );
                                           $from = 'grades';
                     $data_room = $modelCourse->getRoominshiftfordelayedgrades_($from,$model->shift,$model->program,$model->level,$acad);
                  }
               
               
                     
               $onchange = '$.post("../../fi/courses/courseingroupfordelayedgrades?from=grades&group='.'"+$(this).val()+"&shift='.'"+$("#srcgrades-shift").val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                            $("#srcgrades-course").html(data);
                            

                            });';    
               if($model->is_particular_case==1)
                {
                    $onchange = '$.post("../../fi/courses/courseingroupfordelayedgradespc?from=grades&group='.'"+$(this).val()+"&shift='.'"+$("#srcgrades-shift").val()+"&program='.'"+$("#srcgrades-program").val()+"&level='.'"+$("#srcgrades-level").val()+"&acad='.'"+$("#acad").val(), function(data){
                            $("#srcgrades-course").html(data);
                            

                            });';
                }
                     
                   ?>      
       
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>$data_room,//ArrayHelper::map(Rooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                     'onchange'=>$onchange,
  
                       ],
                       'pluginOptions'=>[
                             //'allowclear'=>true,
                         ],
                       ]) ?>
                       
  <?php       $data_course =[]; 
              if($model->course!='')
                {  
                    $modelCourse_ = new SrcCourses();
                  /*	$data_course = ArrayHelper::map(SrcCourses::find()->joinWith(['module0'])->where(['room'=>$model->room,'shift'=>$model->shift,'program'=>$model->program])
                         ->all(),'id','module0.subject0.subject_name' );
                   */
               
                $data_course = $modelCourse_->getCourseingroupfordelayedgrades($model->room,$model->shift,$model->program,$model->level,$acad);
                  
                  if($model->is_particular_case==1)
                    {
                       $data_course = $modelCourse_->getCourseingroupfordelayedgradespc($model->room,$model->shift,$model->program,$model->level,$acad);
                
                    }
                
                
                } 
               
                  
                  
                 ?> 
     
      </div>
       <div class="col-lg-2" style="margin-left:10px; padding:0px;">
                      <?= $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>$data_course,//ArrayHelper::map(Courses::find()->all(),'id','module0.subject0.subject_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    'onchange'=>'submit()', ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>       
          
           </div>
<?php
        }
     
          
?>


    </div>
    
<ul class="nav nav-tabs"></ul>  
<br/>  
<?php
//error message  
										
		         
				  
				  if($success)
				     { 
				         $success=false;
				           Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
					   
				      }
                                      
                                   if($message_course_id)
				     {  
				        $message_course_id=false;
				        Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please, Choose a COURSE.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				         
				      }
                        

             
				    					
										
										
	        	echo '<div class="" style="margin-top:-10px; margin-bottom:10px;">';
				   	 
				    echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';	  
				             
                                            $course_name ='';
                                             
                                            if($model->course!=0)
                                               $course_name = $model->course0->module0->subject0->subject_name.' ('.$model->course0->teacher0->getFullName().") [".$model->course0->module0->code.']';
     					    echo '<span style="color:blue;" ><b>'.$course_name.' --- '.Yii::t('app','- COURSE WEIGHT : ').$course_weight.' - </b></span>';
					
					 echo'</td>
					    </tr>
						</table>';
					
           echo '</div>';
?>
<br/> 


<?php


if($dataProvider->getModels()!=null)
{
  if(!isset($_GET['id']))
   {


 

 if(!$use_update)
  {    
    echo GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array_2, 
                          
       ]); 
   }
elseif($use_update)
  {
    
       echo GridView::widget([
        'id'=>'list-students',
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'summary'=>'',
         'showFooter'=>true,
          'showHeader' =>true,
         'tableOptions' => ['class' => 'table table-hover'],
        'columns' => $item_array_1, 
                          
       ]);
 
   }
   

    
   }

?>

<?php

if( ($dataProvider->getModels()!=null) )
 {
?>   
<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?php
           
        if($is_save==0)
           echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
            
            if( (Yii::$app->session['profil_as'] ==1)&&($is_save==1) )  //se yon pwof
              echo Html::submitButton(Yii::t('app', 'PDF'), ['name' => 'pdf' , 'class' => 'btn btn-primary']);
                                                     
        ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
<br/><br/>
 <?php
 
   }
 else
  $use_update=false;
 
if( ($dataProvider->getModels()==null) )
   {
   	     Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','No students enrolled this course.' ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
   	
   	 } 
  
 } ?>
    <?php ActiveForm::end(); ?>

</div>




<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
    
        // e.keyCode = 37(left), 38(up), 39(right), 40(down)
        
  $('input').keypress(function(e){
   var key=e.keyCode || e.which;
       
   //alert("@-"+key);
        if( (key==13)||(key==40) ){ 
        var x = 1;
         var ind = $(this).attr("tabindex");  
          var next_ind =0;
             next_ind = Number(ind) + Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
        else if(key==38){  //up
        
            var x = 1;
            var ind = $(this).attr("tabindex");  
            var next_ind =0;
             next_ind = Number(ind) - Number(x);
   
            $("[tabindex='"+next_ind+"']").focus();
            e.preventDefault();
        }
    });  		
				
	

JS;
$this->registerJs($script);

?> 

   