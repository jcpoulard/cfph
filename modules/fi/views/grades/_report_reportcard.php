<?php 

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\fi\controllers\GradesController;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcGrades;

?>






<div class="wrapper wrapper-content grades-form">

    <?php $form = ActiveForm::begin(); ?>
    
       <div class="row">
        <div class="col-lg-6">
            
         <?php        
         
                   
        echo $form->field($model, 'transcriptItems')->widget(Select2::classname(), [
			                                   'data'=>loadTranscriptItems(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
          
 <?php
     if($transcriptItems==3)
	{
        
  ?>
        <div class="col-lg-6">			
				
		<?php        
         
                   
                    
                           
                  echo $form->field($model, 'academic_year')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcAcademicperiods::find()->select(['id','name_period'])->where('is_year=1')->all(),'id','name_period' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>		
					
	</div>
           
       </div>       
        
   <div class="row">		 
      
    <div class="col-lg-4">
            
         <?php                    
              echo $form->field($model, 'level')->widget(Select2::classname(), [
			                                   'data'=>loadLevels(),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select level  --'),
			                                                 'onchange'=>'submit()',
			                                   ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
     
  <?php
        if( ($model->level!='')&&($model->academic_year!='') )
          {
            
            
            if($model->level==2)
            {
   ?>
       <div class="col-lg-4">
            
         <?php                    
              echo $form->field($model, 'previous_level_module')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app','No'),'1'=>Yii::t('app','Yes')],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                 'onchange'=>'submit()',
			                                   ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>
        </div>
       
  <?php
            }
  ?>
       <div class="col-lg-4">			
				
		<?php        
         
                   
                    
                           
                  echo $form->field($model, 'student')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcPersons::find()->alias('p')->select(['p.id as id','concat(first_name," ",last_name," (",id_number,")") as full_name'])->join('INNER JOIN', 'student_level_history', 'student_id=p.id')->where('is_student=1 AND level='.$model->level.' AND academic_year='.$model->academic_year)->all(),'id','full_name' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>		
					
	</div>
     
 </div> 
    
  <?php
        if( ($model->student!=0) )
          {  
   ?>
    <div class="row">
      <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
           <div class="form-group">
           <br/><br/>
                <?php
                      echo Html::submitButton( Yii::t('app', 'Done reportcard'), ['name' => 'create', 'class' => 'btn btn-success']);
                 ?>
           </div>
           
        </div>
       
        <div class="col-lg-4">
          </div>
    </div>
    
    
  <?php
            }
          
          }
          
         }
     elseif($transcriptItems==0)
	{
  ?>		
				
	 <div class="col-lg-6">			
				
		<?php        
         
                   
                    
                           
                  echo $form->field($model, 'student')->widget(Select2::classname(), [
			                                   'data'=>ArrayHelper::map(SrcPersons::find()->select(['id','concat(first_name," ",last_name," (",id_number,")") as full_name'])->where('is_student=1')->all(),'id','full_name' ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', ' --  select  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
            ?>		
					
	</div>
				
   
       </div>


       	
  <div class="row">		 
      
    
      <div class="col-lg-8">
       <div class="col-lg-6">
          
           <?= $form->field($model, 'header_text_date')->textInput(['placeholder'=>Yii::t('app', 'City, Date')]) ?>
       </div> 
        <div class="col-lg-12">
           <?= $form->field($model, 'transcript_note_text')->textarea(['rows' => 1, 'cols'=>250,'style'=>'height:70px; width:100%; display:inline;'])  ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'transcript_signature')->textInput(['placeholder'=>Yii::t('app', '')]) ?>
        </div>
        <div class="col-lg-6">
            <div style="clear:both; margin-top: 5px;"></div><br/>
            <?= $form->field($model, 'administration_signature_text')->label(false)->textInput(['placeholder'=>Yii::t('app', '')]) ?>
        </div>
          
     </div>
 </div>   
	

<div class="row">
      <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
           <div class="form-group">
           
                <?php
                      echo Html::submitButton( Yii::t('app', 'Done'), ['name' => 'create', 'class' => 'btn btn-success']);
                 ?>
           </div>
           
        </div>
       
        <div class="col-lg-4">
          </div>
    </div>
   <?php
        }
 ?>
    
     
    
    
		
			
			
       
			
		
		
	<?php
			 
	?>		

    
    
		
 <?php ActiveForm::end(); ?>	   
  </div>           