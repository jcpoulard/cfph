<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\ProgramsLeaders */

$this->title = Yii::t('app', 'Create Programs Leaders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Programs Leaders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programs-leaders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
