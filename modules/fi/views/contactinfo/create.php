<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\ContactInfo */

$this->title = Yii::t('app', 'Create Contact Info');

?>
<div class="row">
    <div class="col-lg-1">
         <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
    </div>
    <div class="col-lg-10">
         <h3><?= Html::encode($this->title) ?></h3>
    </div>   
</div>

<br/>

<div class="wrapper wrapper-content contact-info-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
