<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcContactInfo */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contact Infos');

?>
<div class="contact-info-index">

   <h1><?= Html::encode($this->title) ?></h1>

<div class="col-lg-12"style="float: left;">
    <div class="col-lg-9"style="float: left; padding-left:0px;">
       <?php  echo $this->render('_globalSearch', ['model' => $searchModel]); ?>
    </div>
 </div>   
 

 <div class="panel-body" style="padding-top: 2px;">
                                    <div style="text-align: left;"> <a href="create" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a></div>

                                   <p>
           <?php
             $pageSize=Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']); // set controller and model for that before
               Pjax::begin(['id'=>'contact-info', 'timeout' => false, 'enablePushState' => false,  ]); ?> 

  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'id'=>'contact-info',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'person',
            'contact_name',
            'contact_relationship',
            'profession',
            // 'phone',
            // 'address',
            // 'email:email',
            // 'date_created',
            // 'date_updated',
            // 'create_by',
            // 'update_by',
            // 'one_more',

            ['class' => 'yii\grid\ActionColumn',
                 'template'=>'{view}{update}{delete}',
			      'buttons'=>[
												       
						],
                  'header'=>Html::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                       'onchange'=>"$.pjax.defaults.timeout = false; $.pjax.reload({container: '#contact-info', data:{pageSize: $(this).val() }, url: $('#contact-info li.active a').attr('href') })",
                    )), 
                    
                    
                    ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
