<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\ContactInfo */

$this->title = $model->person0->getFullName();

?>
<div class="contact-info-view">

    <h1><?= Yii::t('app', '{name} contact',['name'=>Html::encode($this->title)]) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-success btn-sm']) ?><?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create'], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->id], ['class' => 'btn btn-info btn-sm']) ?>
        
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
          //  'id',
            //'person',
            'contact_name',
            'contactRelationship.relation_name',
            'profession',
            'phone',
            'address',
            'email:email',
            //'date_created',
           // 'date_updated',
           // 'create_by',
           // 'update_by',
            //'one_more',
        ],
    ]) ?>

</div>
