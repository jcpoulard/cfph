<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\ContactInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
           <?= $form->field($model, 'person')->textInput() ?>
         </div>
        <div class="col-lg-6">
		    <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
		</div>
        
    </div>
		<div class="row">
        <div class="col-lg-6">    
        <?= $form->field($model, 'contact_relationship')->textInput() ?>
		</div>
        <div class="col-lg-6">
		    <?= $form->field($model, 'profession')->textInput(['maxlength' => true]) ?>
		
		</div>
        
    </div>
	<div class="row">
        <div class="col-lg-6">	
		    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?> 
		</div>
        <div class="col-lg-6">
		    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
		
	</div>
        
    </div>
    <div class="row">
        <div class="col-lg-6">
                   <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                   
      </div>
        
    </div>
		
    	<div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
		

    <?php ActiveForm::end(); ?>

</div>
