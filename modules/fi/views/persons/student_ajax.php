<?php

//ini_set('memory_limit','192M');
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\Persons;
use app\modules\fi\models\StudentOtherInfo;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;


use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcBillings;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcAcademicperiods */
/* @var $dataProvider yii\data\ActiveDataProvider */

$acad = Yii::$app->session['currentId_academic_year'];

$this->title = Yii::t('app', 'Students List');
?>
<?php
       //$total_person= 0;
      // if($allStudents!=null)
       //  $total_person= sizeOf($allStudents);
    ?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['createstudent', 'from' =>'stud','is_stud'=>1], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>


    </div>

    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>



</div>
<div class="wrapper wrapper-content  animated fadeInRight">


            <div class="row">


                <div class="col-xs-12 col-md-10 col-lg-8">

                     <div class="col-md-12 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
                                           <th>#</th>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Gender'); ?></th>
				            <th><?= Yii::t('app','Level'); ?></th>
				            <th><?= Yii::t('app','Room'); ?></th>
				            <th><?= Yii::t('app','Shift'); ?></th>

				            </tr>
				        </thead>
				        <tbody>

                <!-- kontni tab yo -->
                 <?php

                 echo Yii::t("app",'Test of power'); 
   $stud_id = 0;
   if(sizeOf($allStudents)!=0)
    {
                $cmpt=0;

           // Initialiser les variables a l'extérieur de la boucle
                  $room = '';
                  $shift = '';
                  $program = '';
                  $konte_liy = 1;
                    foreach($allStudents as $stud){


                              if($cmpt==0)
                               {
                                   $stud_id = $stud->id;

                                   $cmpt=1;
                               }


                          //get program and shift
                           $shift = '';

                           if(isset($stud->studentOtherInfo0[0]->apply_for_program))
                              $program = $stud->studentOtherInfo0[0]->applyForProgram->label;
                           if(isset($stud->studentOtherInfo0[0]->apply_shift))
                               $shift = $stud->studentOtherInfo0[0]->applyShift->shift_name;

                           //get shift and room

                           $room = '';
                            if(isset($stud->studentHasCourses[0]->course))
                              {
                                //$shift = $stud->studentHasCourses[0]->course0->shift0->shift_name;
                                $room = $stud->studentHasCourses[0]->course0->room0->room_name;
                              }
                            
                            
                            //get level

                           $level = '';
                            if(isset($stud->studentLevel->level))
                              {
                                 $level = $stud->studentLevel->level;
                              }




                                            ?>
                      <tr title="<?= $program?>" class="cheche" data-idstudent="<?= $stud->id ?>">

                         <td><a  class="client-link"><?= $konte_liy; ?></a></td>
                          <td><a  class="client-link"><?= $stud->id_number; ?></a></td>
                          <td><a  class="client-link"><?= $stud->first_name;?></a></td>
                          <td><a  class="client-link"><?= $stud->last_name; ?></a></td>
                          <td><?= $stud->geTGenders();?></td>
                          <td><?php if($level!='')
                                         echo Yii::t('app','Level').'-'.$level;  ?></td>
                          <td><?= $room; ?></td>
                          <td><?= $shift; ?></td>
                      </tr>


                      <?php
                        $konte_liy++;


                    }


                         } ?>


                        </tbody>
                    </table>


                 </div>



                  </div>


 <?php    if($stud_id!=0)
       {
       	?>
         <!-- Afficher le profil d'un etudiant avec AJAX -->
          <div id="pwofil-etidyan"></div>


  <?php
       }
  ?>

 </div>
</div>


<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",

                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: 'Students List'},
                    {extend: 'pdf', title: 'Students List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
<!--  Srcipt pour recuperer les donnees et les afficher via AJAX dans le volet -->
<?php
$baseUrl = Yii::$app->request->BaseUrl;

$script1 = <<< JS
      $('.cheche').click(function(){
        var idstudent = $(this).attr('data-idstudent');
        $.get('get-student-info',{id : idstudent},function(data){
            $('#pwofil-etidyan').html(data);
        });
   });

   $(window).load(function() {

         $.get('get-student-info',{id : $stud_id},function(data){
            $('#pwofil-etidyan').html(data);
        });
    });





JS;
$this->registerJs($script1);
?>
