<?php

ini_set('memory_limit','192M');
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\Persons;
use app\modules\fi\models\StudentOtherInfo;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;


use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcBillings;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcAcademicperiods */
/* @var $dataProvider yii\data\ActiveDataProvider */

$acad = Yii::$app->session['currentId_academic_year'];

$this->title = Yii::t('app', 'Students List');
?>
<?php    $total_person= 0;
       if($allStudents!=null)
         $total_person= sizeOf($allStudents);
    ?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['createstudent', 'from' =>'stud','is_stud'=>1], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>


    </div>

    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>



</div>
<div class="wrapper wrapper-content  animated fadeInRight">


            <div class="row">


                <div class="col-lg-8">



                    <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Id Number'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Gender'); ?></th>
				            <th><?= Yii::t('app','Room'); ?></th>
				            <th><?= Yii::t('app','Shift'); ?></th>

				            </tr>
				        </thead>
				        <tbody>

                <!-- kontni tab yo -->
                 <?php   $stud_id =0;
                           if($total_person!=0)
			                  {


                                    //  $all_students=Persons::find()->all();
                                    //voye tout done nap bezwen nan detail.php nan sa :onclick="getSummary('.$stud->id.')"

                                      $cmpt=0;

                                      $age= '';
                                      $contact='';
                                      $username='';
                                      $schoolarship='';
                                      $billing='';
                                      $stage='';

                                      $full_name1='';
                                      $src_img1='';
                                      $program1 = '';
                                      $age1= '';
                                      $bloodgroup1='';
                                      $gender1='';
                                      $birthday1='';
                                      $email1='';
                                      $phone1='';
                                      $contact1='';
                                      $username1='';
                                      $schoolarship1='';
                                      $billing1='';
                                      $stage1='';
                                      $comment1='';
                                      $info_module1 = '';
                                      $total_module1= 0;
                                      $total_module_success1= 0;

                                      $condition_fee_status = ['in','fees_label.status', [0,1] ]; //'fl.status IN(0,1) AND ';
                                      $modelBilling = new SrcBillings();



                                      $meta_str =Yii::t('app', 'Blood group').'   ,   '.Yii::t('app', 'Gender').'   ,   '.Yii::t('app', 'Age').'   ,   '.Yii::t('app', 'Email').'   ,   '.Yii::t('app', 'Phone').'   ,   '.Yii::t('app', 'Contact').'   ,   '.Yii::t('app', 'Username').'   ,   '.Yii::t('app', 'Schoolarship holder').'   ,   '.Yii::t('app', 'Billing').'   ,   '.Yii::t('app', 'Internship').'   ,   '.Yii::t('app', 'Notes').'   ,   '.Yii::t('app'   ,   ' yr old').'   ,   '.Yii::t('app', 'Progression').'   ,   '.Yii::t('app', 'Modules').'   ,   '.Yii::t('app', 'Update').'   ,   '.Yii::t('app', 'More details').'   ,   '.Yii::t('app', 'Course').'   ,   '.Yii::t('app', 'Informations');

                                      $data_str ='';
                            ?>
                      <!-- codage de la pagination -->

                             <?php
                                // $array = $allStudents;
                                /*
                                $total_element = 0;
                                if(isset($_GET['limit'])){
                                    $limit = $_GET['limit'];
                                }else{
                                    $limit = Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']);//3;
                                }
                                foreach ($array as $a){
                                    $total_element++;
                                }
                                $nombre_page = ceil($total_element/$limit);
                                if(isset($_GET['page'])){
                                    $page = $_GET['page'];
                                }else{
                                    $page = 1;
                                }
                                if(isset($_GET['SrcPersons']['globalSearch'])){
                                    $search_value = 'SrcPersons[globalSearch]='.$_GET['SrcPersons']['globalSearch'];
                                }else{
                                    $search_value = "";
                                }
                                $rows = array_chunk($array,$limit);
                                $result = $rows[$page-1];
                                 *
                                 */
                                $result = $allStudents;
                                $studCourse = new SrcStudentHasCourses;

                                // Initialiser les variables a l'esterieur de la boucle
                                $room = '';
                                  $shift = '';
                                  $program = '';
                                  $total_module= 0;
                                 $total_module_success= 0;

                                 $last_bil_id=0;
                                 $bal_id = 0;
                                $bal = 0;
                             ?>


                             <?php
                                        foreach($result as $stud){

                                          	 // $total_module = $studCourse->getTotalCourseForStudent($stud->id);

                                          	 // $total_module_success = $studCourse->getTotalCoursePassForStudent($stud->id);

                                          	  //billing: si elev la gen balanse li pa ajou
                                                  $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$stud->id.' AND balance >0')->all();

                                               //return a integer (id) or NULL
                                                  $last_bil_id = $modelBilling->getLastTransactionID($stud->id, $condition_fee_status, $acad);
                                                  if($last_bil_id==null)
                                                    $last_bil_id = 0;


                                          	  if($cmpt==0)
                                          	   {

                                                      $stud_id = $stud->id;
                                          	      $full_name1 = $stud->getFullName();
                                          	      $full_name1 = str_replace("'","\'",$full_name1);

                                                  $src_img1= Url::to("@web/documents/photo_upload/$stud->image");

                                                 $bloodgroup1=$stud->blood_group;
                                                 $gender1=$stud->geTGenders();

                                                //get info module
                                                 $total_module1= $studCourse->getTotalCourseForStudent($stud->id); //$total_module;
                                                 $total_module_success1= $studCourse->getTotalCoursePassForStudent($stud->id); //$total_module_success;

												    $percent_success1= 0;
												    if( ($total_module1!='')&&($total_module1!=0) )
												      $percent_success1= ($total_module_success1 * 100 ) /  $total_module1;

												   if( ($total_module1!=0)&&($total_module1!='') )
												     $info_module1 = $total_module_success1.'/'.$total_module1;

                                                //get program

                                                if(isset($stud->studentOtherInfo0[0]->apply_for_program))
                                                  $program1= $stud->studentOtherInfo0[0]->applyForProgram->label;

                                                  $program1 = str_replace("'","\'",$program1);

                                               if(ageCalculator($stud->birthday)!=null)
												  $age1= '( '.ageCalculator($stud->birthday).Yii::t('app',' yr old').' )';
											   else
												  $age1= '( )';


												    $schoolarship_ = $stud->getIsScholarshipHolder($stud->id,$acad);

												    if($schoolarship_==1)
												       $schoolarship1 = Yii::t('app','Yes');
												    elseif($schoolarship_==0)
												     $schoolarship1 = Yii::t('app','No');

                                                  $birthday1=$stud->birthday;
			                                      $email1=$stud->email;
                                                  $phone1=$stud->phone;

                                                  $comment1=$stud->comment;
                                                  $comment1 = str_replace("'","\'",$comment1);

                                                  if($modelBalance_stud!=null)
                                                       { foreach($modelBalance_stud as $modelBalance)
                                                         {  if($modelBalance->balance > 0)
                                                                {
                                                                	$bal_id = $modelBalance->id;

                                                                	if($last_bil_id==0)
                                                                	   $billing1 = Html::a('<span class="fa fa-2x fa-thumbs-o-down" style="color:red"></span>', Yii::getAlias('@web').'/billings/balance/view?id='.$bal_id.'&stud='.$stud_id.'&wh=', [ 'title' => Yii::t('app', ''),
                        ]);
                                                                     else
                                                                        $billing1 = Html::a('<span class="fa fa-2x fa-thumbs-o-down" style="color:red"></span>', Yii::getAlias('@web').'/billings/billings/view?id='.$last_bil_id.'&wh=inc_bil&ri=0', [ 'title' => Yii::t('app', ''),
                        ]);
                                                                }
                                                          }

                                                       }
                                                  else
                                                    { if($last_bil_id==0)
                                                            $billing1 ='-';
                                                       else
                                                         $billing1 = Html::a('<span class="fa fa-2x fa-thumbs-o-up" style="color:green"></span>', Yii::getAlias('@web').'/billings/billings/view?id='.$last_bil_id.'&wh=inc_bil&ri=0', [ 'title' => Yii::t('app', ''),
                        ]);


                                                    }

                                          	   	  $cmpt=1;
                                          	   	}



                                          	   if($modelBalance_stud!=null)
                                                     { foreach($modelBalance_stud as $modelBalance)
                                                         {  if($modelBalance->balance > 0)
                                                              {
                                                              	$bal_id = $modelBalance->id;
                                                              	 $bal = 1;//li gen balans
                                                              }
                                                         }
                                                     }
                                               else
                                                     $bal = 0;  //li pa gen balans


                                              $full_name=$stud->getFullName();
                                              $full_name = str_replace("'","\'",$full_name);

                                              $first_name= $stud->first_name;
                                              $last_name = $stud->last_name;

                                              $src_img= Url::to("@web/documents/photo_upload/$stud->image");

                                              //get program

                                               if(isset($stud->studentOtherInfo0[0]->apply_for_program))
                                                  $program= $stud->studentOtherInfo0[0]->applyForProgram->label;

                                                  $program = str_replace("'","\'",$program);

                                               //get shift and room

                                                if(isset($stud->studentHasCourses[0]->course))
                                                  { $shift = $stud->studentHasCourses[0]->course0->shift0->shift_name;
                                                   $room = $stud->studentHasCourses[0]->course0->room0->room_name;
                                                  }

                                               if(ageCalculator($stud->birthday)!=null)
												  $age= '( '.ageCalculator($stud->birthday).Yii::t('app',' yr old').' )';
											   else
												  $age= '( )';


										       $schoolarship_ = $stud->getIsScholarshipHolder($stud->id,$acad);

												    if($schoolarship_==1)
												       $schoolarship = Yii::t('app','Yes');
												    elseif($schoolarship_==0)
												     $schoolarship = Yii::t('app','No');

												   $comment = str_replace("'","\'",$stud->comment);

                                            $data_str = $stud->geTGenders().'   ,   '.$age.'   ,   '.$stud->email.'   ,   '.$stud->phone.'   ,   '.$contact.'   ,   '.$username.'   ,   '.$schoolarship.'   ,   '.$billing.'   ,   '.$stage.'   ,   '.$comment.'   ,   '.$total_module.'   ,   '.$age;





                                              echo '  <tr >
                                                    <td ><a href="#" onclick="getSummary(\''.$stud->id.'\',\'1\',\''.$full_name.'\',\''.$program.'\',\''.$src_img.'\',\''.$meta_str.'\',\''.$last_bil_id.'\',\''.$bal_id.'\',\''.$bal.'\',\''.$data_str.'\')" class="client-link" title ="'.$program.'">'.$stud->id_number.'</a> </td>
                                                    <td ><a href="#" onclick="getSummary(\''.$stud->id.'\',\'1\',\''.$full_name.'\',\''.$program.'\',\''.$src_img.'\',\''.$meta_str.'\',\''.$last_bil_id.'\',\''.$bal_id.'\',\''.$bal.'\',\''.$data_str.'\')" class="client-link" title ="'.$program.'">'.$stud->first_name.'</a></td>
                                                    <td ><a href="#" onclick="getSummary(\''.$stud->id.'\',\'1\',\''.$full_name.'\',\''.$program.'\',\''.$src_img.'\',\''.$meta_str.'\',\''.$last_bil_id.'\',\''.$bal_id.'\',\''.$bal.'\',\''.$data_str.'\')" class="client-link" title ="'.$program.'">'.$stud->last_name.'</a></td>

                                                    <td ><span style="font-weight:normal;">'.$stud->geTGenders().'</span></td>
                                                    <td >'.$room.'</td>
                                                    <td >'.$shift.'</td>

                                                </tr>';

                                          }












                         } ?>


				      </tbody>
                    </table>


                 </div>



                  </div>


 <?php    if($stud_id!=0)
       {
       	?>
     <!-- kolonn adwat la (detay sou...) -->
                <div class="col-sm-12 col-md-3 col-lg-4">
                    <div class="ibox">

                        <div class="ibox-content" >
                            <div class="tab-content">

                    <!-- kolonn adwat la pou 1e tab la(detay sou yon moun) -->
                              <div id="contact" >

                                   <div class="row m-b-lg" style="margin-bottom:10px;" >

		                                   <div class="row">
			                                        <div class="col-lg-12 text-center">
			                                        <?php $full_name1 = str_replace("\'","'",$full_name1);  ?>
			                                            <h4><?php echo $full_name1; ?> </h4>
			                                        </div>
		                                   </div>

		                                  <div class="row" style="margin: 0 auto;" >
		                                      <div class="col-sm-2"></div>
			                                      <div class="col-sm-8"  >
				                                     <div class="m-b-sm" >
				                                             <img alt="image" class="img-circle img-responsive" src="<?php echo $src_img1; ?>" />

				                                      </div>
			                                       </div>
			                                   <div class="col-sm-2"></div>
			                                 </div>
			                                 <div class="row" style="margin: 0 auto;" >
			                                      <div class="col-lg-12 text-center">
			                                         <div class="col-lg-12 text-center">

					                                            <strong>
					                                            <?php $program1 = str_replace("\'","'",$program1);  ?>
					                                               <?= $program1 ?>
					                                            </strong>  <br/>


					                                         <div class="ibox-content" style="height:75px;" >
					                                            <div>
					                                                <div>
					                                                    <span><?= Yii::t('app', 'Modules') ?></span>
					                                                    <small class="pull-right"><?= $info_module1 ?> </small>
					                                                </div>

					                                                <div class="progress progress-small">
					                                                        <div style="width: <?= $percent_success1 ?>%;" class="progress-bar progress-bar-striped"></div>
					                                                </div>
					                                             </div>
					                                          </div>

			                                           </div>
			                                        </div>

		                                     </div>



		                                       <div class="row" style="margin: 0 auto;" >

		                                     <!--      <div class="col-sm-4">
		                                               <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['updatestudent', 'id' => $stud_id,'is_stud'=>1], ['class' => 'btn btn-primary btn-sm']) ?>
		                                           </div>
		                                         -->
		                                           <div class="col-lg-12"> <div class="col-xs-12 col-lg-2"</div>
		                                               <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'More details'), ['moredetailsstudent', 'id' => $stud_id,'is_stud'=>1], ['class' => 'btn btn-primary btn-sm']) ?> <div class="col-sm-2"></div>
		                                           </div>

		                                       </div>

                                     </div><!-- fin class="row m-b-lg"  -->




                                <div class="client-detail">
                                  <div class="full-height-scroll">

                                      <ul class="list-group clear-list">

                                           <li class="list-group-item">
                                                <span class="pull-right"> <?= $age1 ?> </span>
                                               <?= Yii::t('app', 'Age') ?>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> <?= $email1 ?> </span>
                                                <?= Yii::t('app', 'Email') ?>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> <?= $phone1 ?> </span>
                                               <?= Yii::t('app', 'Phone') ?>
                                            </li>

                                            <li class="list-group-item">
                                                <span class="pull-right"> <?= $schoolarship1 ?> </span>
                                                <?= Yii::t('app', 'Schoolarship holder') ?>
                                            </li>
                                             <li class="list-group-item">
                                                <span class="pull-right"> <?php echo Html::a($billing1, Yii::getAlias('@web').'/billings/billings/view?id='.$stud_id.'&wh=inc_bil&ri=0', [ 'title' => Yii::t('app', ''),
                        ]);   ?> </span>
                                                <?= Yii::t('app', 'Billing') ?>
                                            </li>
                                        <!--     <li class="list-group-item">
                                                <span class="pull-right"> <?= $stage1 ?> </span>
                                                <?= Yii::t('app', 'Internship') ?>
                                            </li>
                                         -->
                                            <li class="list-group-item">
                                            <strong><?= Yii::t('app', 'Notes') ?></strong>
                                            <?php $comment1 = str_replace("\'","'",$comment1);  ?>
                                            <p><?= $comment1 ?></p>
                                            </li>

                                        </ul>
                                      </div>
                                    </div>
                           </div><!-- fen id="contact"-->

       <!-- fen kolonn adwat la pou 1e tab la(detay sou yon moun) -->



                            </div>
                        </div>
                    </div>
                </div>
           <!-- fen kolonn adwat la (detay sou...) -->
  <?php
       }
  ?>

 </div>
</div>


<script>

function getSummary(id,is_stud,full_name,program,src_img,meta_str,last_bil_id,bal_id,bal,data_str)
{


   $.ajax({

     type: "GET",
     url: "<?= Yii::$app->request->baseUrl ?>/../modules/fi/views/persons/pers_detail_student.php",
     data: "id=" + id + "&is_stud=" + is_stud + "&full_name=" + full_name + "&program=" + program + "&src_img=" + src_img + "&meta_str=" + meta_str + "&last_bil_id=" + last_bil_id + "&bal_id=" + bal_id + "&bal=" + bal + "&data_str=" + data_str, // appears as $_GET['id'] @ your backend side
     success: function(data) {
           // data is ur summary
          $('#contact').html(data);
    }


   });

}
</script>

<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",

                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: 'Students List'},
                    {extend: 'pdf', title: 'Students List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
