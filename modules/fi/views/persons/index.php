<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcPersons */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Persons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persons-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Persons'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'last_name',
            'first_name',
            'gender',
            'blood_group',
            // 'birthday',
            // 'id_number',
            // 'is_student',
            // 'adresse',
            // 'phone',
            // 'email:email',
            // 'nif_cin',
            // 'cities',
            // 'citizenship',
            // 'paid',
            // 'date_created',
            // 'date_updated',
            // 'create_by',
            // 'update_by',
            // 'active',
            // 'image',
            // 'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
