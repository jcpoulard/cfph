<?php
use yii\helpers\Url;


use app\modules\fi\models\CustomField;
use app\modules\fi\models\CustomFieldData;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcPersonsHasTitles;
use app\modules\fi\models\SrcTitles;
use app\modules\planning\models\SrcEvents; 

use app\modules\billings\models\SrcPayroll;

use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;

use app\modules\rbac\models\User;

use app\modules\idcards\models\SrcIdcard;

 
$acad=Yii::$app->session['currentId_academic_year'];


$criteria = [ 'field_related_to'=>'emp'];
$list_form = 'employee';
$tit = Yii::t('app', 'Employee');
if(isset($_GET["from"])&&($_GET["from"]!=''))
 {
 	if($_GET["from"] =='teach')
 	  { $criteria = ['field_related_to'=>'teach'];
 	      $list_form = 'teacher';
 	       
 	  }
 	elseif($_GET["from"]=='emp')
 	{
 	   $criteria = ['field_related_to'=>'emp'];
 	     $list_form = 'employee';
 	     
 	  }
 	
 }

//gad si moun sa fe kou 
$pers = $_GET["id"];
//return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
 $ki_ou = $modelEmployee->isEmployeeOrTeacher($pers, $acad) ;
 
 $main_list_form = 'employee';
 
 switch($ki_ou)
   {  case 0: $tit = Yii::t('app', 'Employee');
               $criteria = ['field_related_to'=>'emp'];
 	            $list_form = 'employee';
           break;
           
      case 1: $tit = Yii::t('app', 'Teacher');
               $criteria = ['field_related_to'=>'teach'];
 	            $list_form = 'teacher';
           break;
           
      case 2: $tit = Yii::t('app', 'Teacher');
               $criteria = ['field_related_to'=>'teach'];
 	            $list_form = 'teacher';
           break;
   	
   	}



?>

 <div class="row">
     <div class="col-lg-7">
         <h3><?= '<span style="color:#CD3D4C">'.$tit.' :</span> '.$modelEmployee->getFullName(); ?></h3>
     </div>
 
 <?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))  ) 
             {
    
    ?>
       
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['updateemployee', 'id' =>$modelEmployee->id,'is_stud'=>0,'from'=>$_GET['from'] ], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['createemployee', 'is_stud'=>0,'from'=>$_GET['from'] ], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), [$main_list_form], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$modelEmployee->id, 'is_stud'=>0,'from'=>$_GET['from']], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this {name}?',['name'=>$list_form]),
                'method' => 'post',
            ],
        ]) ?>
    </p>

   <!-- <h3><?= $modelEmployee->first_name.' '.strtoupper($modelEmployee->last_name); ?></h3> -->
    
        </div>
  <?php
             }
  ?>
    </div>


<div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class='fa fa-user'></i> Info perso</a></li>
                         <?php  
					if(Yii::$app->session['profil_as']!=1)
                                        {
                                               if(($ki_ou==1)||($ki_ou==2) ) //teacher
					     	     { 
		                  ?>	      
			
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i class='fa fa-book'></i> Modules</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i class='fa fa-calendar'></i> Horaire</a></li>
                         <?php  
					     	     } 
                                                     
                                        }
		                  ?> 
                            
      <?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administration économat"))  ) 
             {
    
    ?>
                      
                            <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i class='fa fa-dollar'></i> Économat</a></li>
                            
   <?php
             }
   ?>                         
                           <!-- <li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false"><i class='fa fa-briefcase'></i> Expériences</a></li>   -->
                        </ul>
                        <div class="tab-content">
                           
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                 <div class="col-lg-9">
                                    <table class='table table-responsive'>
                                        <tr>
                                             <th><?= Yii::t('app', 'First Name') ?></th>
                                                    <td><?= $modelEmployee->first_name ?></td>
                                                    <th><?= Yii::t('app', 'Last Name') ?></th>
                                                    <td><?= $modelEmployee->last_name ?></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Gender') ?></th>
                                                    <td>
                                                         <?php 
                                                                    if($modelEmployee->gender== NULL){
                                                                ?>
                                                                <a href="#" class="sexe" data-name="sexe"  data-type="select" data-source="genderlist" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=sexe" data-title="<?= Yii::t('app','Gender') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="sexe" data-name="sexe" data-type="select" data-source="genderlist" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=sexe" data-value="<?= $modelEmployee->gender ?>" data-title="<?= Yii::t('app','Gender') ?>">
                                                                     <?= $modelEmployee->getGenders() ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Blood Group') ?></th>
                                                    <td>
                                                    <?php 
                                                                    if($modelEmployee->blood_group== NULL){
                                                                ?>
                                                                <a href="#" class="gs" data-name="gs"  data-type="select" data-source="gslist" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=gs" data-title="<?= Yii::t('app','Blood Group') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="gs" data-name="gs" data-type="select" data-source="gslist" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=gs" data-value="<?= $modelEmployee->blood_group ?>" data-title="<?= Yii::t('app','Blood Group') ?>">
                                                                     <?= $modelEmployee->blood_group ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Birthday') ?></th>
                                                    <td><?= Yii::$app->formatter->asDate($modelEmployee->birthday) ?></td>
                                                    <th><?= Yii::t('app', 'Citizenship') ?></th>
                                                    <td><?= $modelEmployee->citizenship ?></td>
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Nif Cin') ?></th>
                                                    <td>
                                                       <?php 
                                                                    if($modelEmployee->nif_cin== NULL){
                                                                ?>
                                                                <a href="#" class="nif_cin" data-name="nif_cin"  data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=nif_cin" data-title="<?= Yii::t('app','Nif Cin') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="nif_cin" data-name="nif_cin" data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=nif_cin" data-title="<?= Yii::t('app','Nif Cin') ?>">
                                                                     <?= $modelEmployee->nif_cin ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Id Number') ?></th>
                                                    <td><?= $modelEmployee->id_number ?></td>
                                                </tr>
                                             
                                                <tr>
                                                            <th><?= Yii::t('app', 'Address') ?></th>
                                                            <td>
                                                                <?php 
                                                                    if($modelEmployee->adresse== NULL){
                                                                ?>
                                                                <a href="#" class="adresse" data-name="adresse"  data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=adresse" data-title="<?= Yii::t('app','Adresse') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="adresse" data-name="adresse" data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=adresse" data-title="<?= Yii::t('app','Adresse') ?>">
                                                                     <?= $modelEmployee->adresse ?>
                                                                 </a>
                                                                <?php } ?>
                                                            </td>
                                                            <th><?= Yii::t('app', 'Phone') ?></th>
                                                            <td>
                                                                <?php 
                                                                    if($modelEmployee->phone== NULL){
                                                                ?>
                                                                <a href="#" class="phone" data-name="phone"  data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=phone" data-title="<?= Yii::t('app','Phone') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="phone" data-name="phone" data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=phone" data-title="<?= Yii::t('app','Phone') ?>">
                                                                     <?= $modelEmployee->phone ?>
                                                                 </a>
                                                                <?php } ?>
                                                            </td>
                                                        </tr> 
                                                <tr>
                                                    <th><?= Yii::t('app', 'Email') ?></th>
                                                    <td>
                                                        <?php 
                                                                    if($modelEmployee->email== NULL){
                                                                ?>
                                                                <a href="#" class="email" data-name="email"  data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?set=email" data-title="<?= Yii::t('app','Email') ?>">
                                                                    ...
                                                                </a>

                                                                <?php 
                                                                    }else{
                                                                ?>
                                                                 <a href="#" class="email" data-name="email" data-type="text" data-pk="<?= $modelEmployee->id ?>"  data-url="set-infoemployee?is=up&set=email" data-title="<?= Yii::t('app','Email') ?>">
                                                                     <?= $modelEmployee->email ?>
                                                                 </a>
                                                                <?php } ?>
                                                    </td>
                                                    
                                                    <th><?= Yii::t('app', 'Status') ?></th>
                                                    <td><?= $modelEmployee->getActive() ?></td>
                              <?php
   if( (Yii::$app->user->can('Administration économat'))||(Yii::$app->user->can('superadmin'))||(Yii::$app->user->can('direction'))||(Yii::$app->user->can('Direction des études')) )
       {
                              ?>                      
                                                    <th><?= Yii::t('app', 'Username') ?></th>
                                                    <td><?php 
                                                              $username = '';
                                                              $emp_username = User::find()->where('person_id='.$modelEmployee->id )->all();
                                                        
                                                           foreach($emp_username as $emp_user)
                                                             {
                                                             	$username = $emp_user->username;
                                                             	}
                                                        
                                                        echo $username; 
                                                        
                                                        ?> </td>
     <?php
     
       }
     ?>                                               
                                        </tr>
                                  
                                  
       
   
					                <!-- Debut affichage des champs personalisables -->
					              <?php
					                    $modelCustomFieldData = new CustomFieldData();
					                    
					                    $data_custom_label = CustomField::find()->where($criteria)->all();
					                    $id_pers = $_GET['id'];
					                    $konte_liy=0;
					                    /*
					                    function evenOdd($num)
					                        {
					                            ($num % 2==0) ? $class = 'odd' : $class = 'even';
					                            return $class;
					                        }
					                     * 
					                     */
					              ?>
					              
					                      <?php  foreach($data_custom_label as $dcl){ ?>
					                      <tr class="<?php ($konte_liy % 2==0) ? $class = 'odd' : $class = 'even'; echo $class;  ?>">
					                          <th> <?php echo $dcl->field_label ?></th>
					                          <td><?php echo $modelCustomFieldData->getCustomFieldValue($id_pers,$dcl->id); ?></td>
					                          
					                          <td> </td>
					                          <td></td>
					                          
					                      </tr>
					                      <?php 
					                      $konte_liy++;
					                      
					                      } ?>
					                               
					              <!-- Fin affichage des champs personalisables  -->
					              
                                    
                                    </table>
                                  </div>
                                  
                                  
                                  <div class="col-lg-2 text-center">
                                                        <?php  
                                                        $hire_date=' ? ';
                                                       if(isset($modelEmployee->employeeInfos[0]->hire_date)&&($modelEmployee->employeeInfos[0]->hire_date!='') )  
                                                            $hire_date = Yii::$app->formatter->asDate( $modelEmployee->employeeInfos[0]->hire_date );
                                                        ?>
                                                        <strong> <?= Yii::t('app',' Hire date').': <br/>'.$hire_date ?></strong><br/>
                                                        
                                                        <span>
                                                            <strong>
                                                        <?php if(isset($modelEmployee->employeeInfos[0]->job_status)&&($modelEmployee->employeeInfos[0]->job_status!=null) )
                                                              echo $modelEmployee->employeeInfos[0]->jobStatus->status_name; ?>
                                                            </strong>
                                                        </span>
                                                          <?php if($modelEmployee->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/$modelEmployee->image")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>
                            
                                                             <span>
                                                            <strong>
                                                        <?php 
					         	  								//echo Yii::t('app', 'Title').' : ';
					         	  								
					         	  								$title_id = 0;
					         	  								$title='';
					         	  								
					         	  								$modelTitle=SrcPersonsHasTitles::find()-> where('persons_id='.$modelEmployee->id.' and academic_year='.$acad)->all();
		                      
											                     if($modelTitle!=null)
											                       { 
											                       	   foreach($modelTitle as $tit)
											                       	     $title_id= $tit->titles_id;
											                       }
		                       	     
					         	  								if( ($title_id != 0 )&&($title_id != null ) )
					         	  								  { $modelTitle = SrcTitles::findOne($title_id);
					         	  								      $title = $modelTitle->title_name;
					         	  								   }
					         	  								
					         	  								
					         	  								  echo $title;
					         	  						 ?>
					         	  						   </strong>
                                                        </span>
                           <?php
                                              // gad si kat moun sa prepare deja
                                                $modelIdcard=SrcIdcard::find()->where('person_id='.$modelEmployee->id)->all();
                                                $card_id = 0;

                                                if($modelIdcard!=null)
                                                {  foreach ($modelIdcard as $modIdcard)
                                                     $card_id =$modIdcard->id;
                                                }
                                                
                                            if($card_id != 0)
                                            {
                                               if(Yii::$app->user->can('idcards-idcard-print'))  
                                                 echo Html::a('<i class="fa fa-print"></i> '.Yii::t('app', 'Print card'), ['../idcards/idcard/printacard', 'id' =>$modelEmployee->id ], ['class' => 'btn btn-success btn-sm']);
                                            }
                                               ?>        

                                                    </div>
                                                    
                                                    
                         </div>       
                            </div>
 

<?php  
     	     if(($ki_ou==1)||($ki_ou==2) ) //teacher
     	     {  
     	       $modelC = new SrcCourses;
			      
			   $allCourses = $modelC->getCourseByTeacherID($_GET['id']);
		?>	      
			
	                           
                                   <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                
     			                 
                                
                                    <table class="table table-responsive">
                                        
                                        <thead>
                                         <th style="width:12%" > <?=  Yii::t('app', 'Code') ?> </th>
									     <th>
                                            <?= Yii::t('app', 'Modules') ?>
                                        </th>
                                        <th > <?=  Yii::t('app', 'Duration') ?> </th>
									     <th > <?=  Yii::t('app', 'Passing Grade') ?> </th>
									 <!--    <th > <?php //echo Yii::t('app', 'Room'); ?> </th>   -->
									     <th > <?=  Yii::t('app', 'Shift') ?> </th>
									    </thead>
                                        
                                      <?php
                                      
                                      		     foreach($allCourses as $course)
											      { 
											           
											       
											?>	  
												   
												  <tr>
												     <td ><a href="#" class='popopKou' data-idcourse="<?= $course->id; ?>"> <?=  $course->module0->code ?> </a></td>
												     <td > <?= $course->module0->subject0->subject_name  ?> </td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												      <!--    <td > <?php //echo $course->room0->room_name; ?> </td>  -->
												     <td > <?=  $course->shift0->shift_name ?> </td>
												  </tr>
											<?php
											        								         
											      }
											      
											?>
                                                                               
                                    </table>
                                </div>
                            </div>
                            
                            
                            
       <div id="tab-3" class="tab-pane">
            <div class="panel-body">
            <!-- Nap mete orè pwofesè a la a nan modèl kalandrye a -->
            <?php 
             if(isset($_GET['id'])){
                 $baseUrl = Yii::$app->request->BaseUrl;
                 $teacher = $_GET['id'];
                 $course = new SrcCourses(); 
                 $sql = "SELECT e.id, e.course, e.date_start, e.date_end, e.color, e.lesson_complete FROM events e INNER JOIN courses c ON (e.course = c.id) INNER JOIN persons p ON (c.teacher = p.id) WHERE p.id = $teacher";
                 $events = [];
                 $j = 0;
                 $data_schedules = SrcEvents::findBySql($sql)->all(); 
                 foreach($data_schedules as $ds){
                    $events[$j]['id'] = $ds->id; 
                    $events[$j]['title'] = $course::findOne(['id'=>$ds->course])->module0->subject0->subject_name;
                    $events[$j]['start'] = $ds->date_start; 
                    $events[$j]['end'] = $ds->date_end; 
                    $events[$j]['color'] = $ds->color; 
                    $events[$j]['allDay'] = 0; 
                    $events[$j]['type'] = "schedule";
                    $events[$j]['lessonStatus'] = $ds->lessoncomplete; 
                    $events[$j]['lesson_complete'] = $ds->lesson_complete;
                    $events[$j]['url_complete'] = $baseUrl."/index.php/planning/events/markascomplete?id=$ds->id";
                    $events[$j]['url_move'] = $baseUrl."/index.php/planning/events/movelesson?id=$ds->id&course=$ds->course";
                    $events[$j]['program'] = $ds->course0->module0->program0->label;
                    $events[$j]['shift'] = $ds->course0->shift0->shift_name; 
                    $events[$j]['url_move_choose'] = $baseUrl."/index.php/planning/events/movelessonchoose?id=$ds->id&course=$ds->course";
                    $events[$j]['url_attendance_teacher'] = $baseUrl."/index.php/planning/teacherattendance/create?id=$ds->id&course=$ds->course&teacher=".$ds->course0->teacher."&wh=te_att&shift=".$ds->course0->shift0->id;
                    $events[$j]['url_delete_event'] = $baseUrl."/index.php/planning/events/deleteevent?id=$ds->id";
                    if($ds->initial_date_course_end!=null){
                    $events[$j]['initial_date_course_end'] = $ds->initial_date_course_end;
                    }else{
                        $events[$j]['initial_date_course_end'] = "anyen";
                    }
                     $j++; 
                  }
                 
             }
            ?>
            <div id='calendar'></div>

            </div>
        </div>
<?php  
     	     }
		?>	      
			
	                            


<?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administration économat"))  ) 
             {
    
    ?>
                            
                            <div id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                   <!-- <strong>Economat</strong>  -->

                                              <div id="" class="">

												    <?php
												            $modelPayroll = new SrcPayroll;
												            
												            $month_ = 0;
                                                            
                                                           
	 
												 echo GridView::widget([
												        'dataProvider' => $modelPayroll->searchByPersonId($_GET['id'],$acad),
												        'id'=>'billing-grid',
												        //'filterModel' => $searchModel,
												        'summary'=>"",
												        'columns' => [
												           // ['class' => 'yii\grid\SerialColumn'],
												
												            ['attribute'=>'payment_date',
															'label' => Yii::t('app', 'Payment Date'),
												               'content'=>function($data,$url){
												               	      
												               	       $di= 1;
												               	       
													                return Html::a(Yii::$app->formatter->asDate($data->payment_date) , Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$data->id.'&from=pay&id_='.$data->id.'&month_='.$data->payroll_month.'&year_='.getYear($data->payment_date).'&di='.$di, [
                                    'title' => Yii::$app->formatter->asDate($data->payment_date),
                        ]);
												               }
												             ],
												             
												             
												             ['attribute'=>'payroll_month',
															'label' => Yii::t('app', 'Payroll Month'),
												               'content'=>function($data,$url){
												               	 
												               	 $di= 1;
												               	 
													                return Html::a($data->getLongMonth() , Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$data->id.'&from=pay&id_='.$data->id.'&month_='.$data->payroll_month.'&year_='.getYear($data->payment_date).'&di='.$di, [
                                    'title' => $data->getLongMonth(),
                        ]);
												               }
												             ],
												             
												                      
												              [//'attribute'=>'',
															   'label' => Yii::t('app', 'Gross salary'),
												               'content'=>function($data,$url){
													                
													                 $di= 1;
													                 
													                 return $data->getGrossSalaryInd($data->idPayrollSet->devise,$data->idPayrollSet->person_id,$data->payroll_month,getYear($data->payment_date));
												               }
												             ],         
																	    
																	    
																	    //'number_of_hour',
																		/*	array('name'=>'number_of_hour',
																			                'header'=>Yii::t('app','Number Of Hour'),
																				        'value'=>'$data->NumberHour',
																						),
																			*/
																	    /* array('header' =>Yii::t('app','Deduction').' ('.Yii::t('app','Missing hour').')', 
																	            'value'=>'$data->getMissingHourDeduction($data->idPayrollSet->person_id,$data->missing_hour)',
																	             ),
																	      */
																	      
																		//'taxe', 
															 [//'attribute'=>'',
															   'label' => Yii::t('app', 'Taxe'),
												               'content'=>function($data,$url){
													                return $data->getTaxe();
												               }
												             ], 
																
															/*	[//'attribute'=>'',
															   'label' => Yii::t('app', 'Loan(deduction)'),
												               'content'=>function($data,$url){
													                return $data->idPayrollSet->devise0->devise_symbol.' '.$data->getLoanDeduction($data->idPayrollSet->person_id,$data->getGrossSalaryIndex_value($data->idPayrollSet->devise,$data->idPayrollSet->person_id,$data->payroll_month,getYear($data->payment_date)),$data->number_of_hour,0,$data->net_salary,$data->taxe,$data->getAssurance() );
												               }
												             ],
												             */
												              
                                                            [//'attribute'=>'',
															   'label' => Yii::t('app', 'Assurance'),
												               'content'=>function($data,$url){
													                return $data->getAssurance();
												               }
												             ],
												             
												             [//'attribute'=>'',
															   'label' => Yii::t('app', 'Frais'),
												               'content'=>function($data,$url){
													                return $data->getFrais();
												               }
												             ],
												             
												             [//'attribute'=>'',
															   'label' => Yii::t('app', 'Plus Value'),
												               'content'=>function($data,$url){
													                return $data->getPlusValue();
												               }
												             ],
												             
												             [//'attribute'=>'',
															   'label' => Yii::t('app', 'Net Salary'),
												               'content'=>function($data,$url){
													                return $data->getNetSalary();
												               }
												             ], 			
																		 			
																],	
																	    ]);
												            
												            
												               
												 ?>
												
												   </div>
                                </div>
                            </div>
<?php
             }
?>                           
                        <!--    <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Experience</strong>

                                    <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                        and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                        sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </div>
                            </div>
                          -->
                            
                        </div>


 </div>


<!-- Modal pour la visualisation des evenements  -->

<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?= Yii::t('app','Close') ?></span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <span><b><?= Yii::t('app','Date start'); ?></b> : </span>
                <span id='dateStart'></span><br/>
                <span><b><?= Yii::t('app','Date end'); ?></b> : </span>
                <span id='dateEnd'></span><br/>
                <span class='kacheFet'><b><?= Yii::t('app','Lesson status')?></b> : </span>
                <span id='lessonStatus' class='kacheFet'></span><br/>
                <span class="kacheFet"><b><?= Yii::t('app','Program'); ?></b> : </span>
                <span id="program" class="kacheFet"></span><br/>
                <span class="kacheFet"><b><?= Yii::t('app','Shift'); ?></b> : </span>
                <span id="shift" class="kacheFet"></span><br/>
                
            </div>
             
            <div class="modal-footer">
                <div class="row">
                    <div class="col-lg-2">
                     <button type="button" class="btn btn-default fenmen" data-dismiss="modal"><?= Yii::t('app','Close'); ?></button>
                    </div>
                    
                    <div class="col-lg-2 kacheFet">
                     <?= Html::a('<i class="fa fa-trash"></i>', [''], ['class' => 'btn btn-info kacheFet', 'id'=>'eventDelete','title'=>Yii::t('app','Delete event'),
                        'data'=>[
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this lesson ?'),
                        'method' => 'post',
                        ]
                    ]) ?> 
                    </div>
               <div class="col-lg-8 fiti">
               <?= Html::a('<i class="fa fa-graduation-cap"></i>', [''], ['class' => 'btn btn-info kacheFet', 'id'=>'eventStudentAttendance','title'=>Yii::t('app','Take students attendance.'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to take students attendance for this lesson ?'),
                //'method' => 'post',
                ]
            ]) ?> 
                <?= Html::a('<i class="fa fa-user"></i>', [''], ['class' => 'btn btn-primary kacheFet', 'id'=>'eventTeacherAttendance','title'=>Yii::t('app','Take teacher attendance.'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to take teacher attendance for this lesson ?'),
                //'method' => 'post',
                ]
            ]) ?> 
                <?= Html::a('<i class="fa fa-check"></i>', [''], ['class' => 'btn btn-warning maske', 'id'=>'eventComplete','title'=>Yii::t('app','Mark as lesson complete'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to mark this lesson as complete ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-thumb-tack"></i>', [''], ['class' => 'btn btn-success maske', 'id'=>'eventMoveChoose','title'=>Yii::t('app','Postpone lesson to choosen date'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to postpone this lesson to specific date you choose ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-arrow-right"></i>', [''], ['class' => 'btn btn-danger maske', 'id'=>'eventMove','title'=>Yii::t('app','Postpone lesson'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to postpone this lesson to the next avalaible date ?'),
                //'method' => 'post',
                ]
            ]) ?>
               </div>
                </div>
                </div>
                </div>
        </div>
    </div>



<?php
        $str_json = "";
     if(isset($events) )
       {
        foreach($events as $e)
         {
           $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
          }
       }
        
        
$language = Yii::$app->params["language"];        
$script2 = <<< JS
$('#calendar').fullCalendar({
			locale: '$language',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek,month,listMonth'
			},
			defaultDate: Date.now(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, 
			events: [
                                $str_json
                            ],
                        eventClick:  function(event, jsEvent, view) {
                           $('#modalTitle').html(event.title);
                           $("#dateStart").html(moment(event.start).format('Do MMMM YYYY h:mm A'));
                           $("#dateEnd").html(moment(event.end).format('Do MMMM YYYY h:mm A'));
                           $("#lessonStatus").html(event.lessonStatus);
                           $("#program").html(event.program);
                           $("#shift").html(event.shift);
                           var lesson_complete = event.lesson_complete; 
                           if(lesson_complete == -1){
                               $('.kacheFet').hide();
                               $('.maske').hide();
                            }else{
                                $('.kacheFet').show(); 
                                $('.maske').show();
                                }
                           if(lesson_complete==1 || lesson_complete==2){
                                $('.kacheFet').show();
                                $('.maske').hide();
                                }else if(lesson_complete!=-1){
                                    $('.maske').show();
                                   
                                }
                           var jodia = new Date();
                           if(event.start >= jodia){
                               $('.fiti').hide();
                               $('fenmen').show(); 
                            }else{
                                $('.fiti').show();
                                $('fenmen').show(); 
                                }
                            var initial_date_course_end = event.initial_date_course_end;     
                            if(initial_date_course_end=='anyen'){
                                $('#eventDelete').hide();
                                }else{
                                    $('#eventDelete').show();
                                }    
                                 
                           $('#eventComplete').attr('href',event.url_complete);
                           $('#eventMove').attr('href',event.url_move);
                           $('#eventMoveChoose').attr('href',event.url_move_choose);
                           $('#eventTeacherAttendance').attr('href',event.url_attendance_teacher);
                           $('#eventDelete').attr('href',event.url_delete_event);
                           $('#fullCalModal').modal();
                    } 
		});	
JS;
$this->registerJs($script2);

?>

<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script3 = <<< JS
    $(function(){
    $(document).on('click','.popopKou',function(){
        var idcourse = $(this).attr('data-idcourse');
        $.get('$baseUrl/fi/courses/viewstudent',{'id':idcourse},function(data){
            
            $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });
      
    });
    
});
JS;
$this->registerJs($script3);

?>

   
<?php

    
    $script4 = <<< JS
    $(document).ready(function(){
             $('.email').editable();
            $('.phone').editable();
            
            $('.adresse').editable();
            $('.sexe').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            $('.gs').editable({
                emptytext:'N/A',
                placement: function (context, source) {
                var popupWidth = 50;
               if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
            $('.nif_cin').editable();
            
            
            
            
            
            

        });

JS;
$this->registerJs($script4);






?>
              