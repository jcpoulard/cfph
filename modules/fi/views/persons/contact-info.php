<?php
$i = 1;
foreach ($contactStudent as $mcs){
       // $modelContactInfo = ContactInfo::findOne($mcs->id);
?>
                                                    
<table id="w0" class="table table-striped table-bordered detail-view table-responsive">
    <thead>
        <tr>
            <th colspan="4"><?= Yii::t('app','Contact # {number}',['number'=>$i])?></th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <th><?= Yii::t('app','Contact name'); ?></th>
            <td class="modifye">
                
                <?= $mcs->contact_name; ?>
            </td>
            <th>
                <?= Yii::t('app','Relation contact'); ?>
            </th>
            <td class="modifye" data-type="select" data-source="list-relation" data-name="relation_name" data-pk="<?= $mcs->id ?>" data-url=""><?= $mcs->relationName; ?></td>
     </tr>
     <tr>
         <th><?= Yii::t('app','Profession'); ?></th>
         <td class="modifye"><?= $mcs->profession; ?></td>
         <th><?= Yii::t('app','Phone'); ?></th>
         <td class="modifye"><?= $mcs->phone; ?></td>
     </tr>
     <tr>
         <th><?= Yii::t('app','Email'); ?></th>
         <td class="modifye"><?= $mcs->email; ?></td>
         <th><?= Yii::t('app','Address'); ?></th>
         <td class="modifye"><?= $mcs->address; ?></td>
     </tr>
      </tbody>

</table>
<?php
$i++;
}
?>

     
         
         
<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'N/A',
          
         });
        });
JS;
$this->registerJs($script2);


?>         