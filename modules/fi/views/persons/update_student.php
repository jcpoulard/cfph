<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Persons */

$this->title = Yii::t('app', 'Update : {modelClass}', [
    'modelClass' =>$model->getFullName(),
]);

?>
<div class="row">
    <div class="col-lg-7">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['createstudent', 'is_stud'=>1], ['class' => 'btn btn-primary btn-sm']) ?>
      <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['/reports/report/dashboardpedago'], ['class' => 'btn btn-info btn-sm']) ?>
       
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this student?'),
                'method' => 'post',
            ],
        ]) ?>
            </p>
    </div>
        
    </div>
<div class="wrapper wrapper-content persons-update">


    <?= $this->render('_form_student', [
        'model' => $model,
        'modelStudentOtherInfo' => $modelStudentOtherInfo,
        'modelCustomFieldData'=>$modelCustomFieldData,
        'modelStudentLevel'=>$modelStudentLevel,
        'disable_field'=>$disable_field,
    ]) ?>

</div>
