<?php

/* 
 *  (c) 2017 LOGIPAM 
 *  Crée par Jean Came Poulard
 *  20/06/2017
 *  List all student for a course 
 
 */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\SrcPersons;

if(isset($_GET['id'])&&($_GET['id']!='')){
    $stud  = $_GET['id'];


$stud_info = SrcPersons::findOne($stud);
        
$title_part1 =$stud_info->getFullName();// Yii::t('app', 'List student for {coursename}',['coursename'=>$nom_cours]);
$title_part2 =$stud_info->studentOtherInfo0[0]->applyForProgram->label.' / '.$stud_info->courses0[0]->shift0->shift_name;//.' / '.$course_info->room0->room_name;
}else{
    $title_part1 = Yii::t('app','Student not found');
}
?>

   <span style="font-size:14px"><b><?= Html::encode($title_part1) ?> </b> ( </span><span style="font-size:12px"><?= Html::encode($title_part2) ?></span> <span style="font-size:14px"> )</span>
   
<br/><br/>

<div class="row">
    
    <div class="col-lg-12 table-responsive">
        <table class='table table-striped table-bordered table-hover dataTables-example' id="tablo-etidyan">
         <thead>
            <tr>
            <th><?= Yii::t('app','Module'); ?></th>
            <th><?= Yii::t('app','Shift'); ?></th>
            <th><?= Yii::t('app','Room'); ?></th>
            <th><?= Yii::t('app','Weight'); ?></th>
            <th><?= Yii::t('app','Passing Grade'); ?></th>
            
            
            </tr>
        </thead>
           <tbody>

 <?php 
 
         $dataProv = $dataProvider->getModels();
     foreach($dataProv as $data)
        {
            ?>
            <tr>
	            <td><?= $data->course0->module0->subject0->subject_name; ?></td>
	            <td><?= $data->course0->shift0->shift_name; ?></td>
	            <td><?php 
                        if(isset($data->student0->studentLevel->room0)){
                            echo $data->student0->studentLevel->room0->room_name;
                        }else{
                            echo ''; 
                        }
                    ?></td>
	            <td><?= $data->course0->weight; ?></td>
	            <td><?= $data->course0->passing_grade; ?></td>
	            
	            
	            
	
             </tr>
  <?php } 
  
  
  ?>
        
           </tbody>
        </table>


    </div>
    
</div>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('#tablo-etidyan').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Course For Student List'},
                    {extend: 'pdf', title: 'Course For Student List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);
 
 

?>


