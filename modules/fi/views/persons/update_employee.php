<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Persons */



$this->title = Yii::t('app', 'Update {employeeName}',['employeeName'=>$model->getFullName()]);

$list='employee';
$add='createemployee';

if(isset($_GET["from"])&&($_GET["from"]!=''))
 {
 	if($_GET["from"]=='teach')
 	  { $title = Yii::t('app', 'Update Teacher');
 	     $list='teacher';
          $add='createteacher';
 	  }
 	elseif($_GET["from"]=='emp')
 	{
 	  $title = Yii::t('app', 'Update Employee');
 	     $list='employee';
          $add='createemployee';
 	  }
 	
 }

 
?>
<div class="row">
    <div class="col-lg-7">
             <h3><?= $this->title; ?></h3>
        </div>
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), [$add, 'is_stud'=>0], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), [$list], ['class' => 'btn btn-info btn-sm']) ?>
       
   
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this employee ?'),
                'method' => 'post',
            ],
        ]) ?>
            </p>
    </div>
        
    </div>
    
    
<div class="wrapper wrapper-content persons-update">

    
<?php

     if(isset($_GET["from"])&&($_GET["from"]!=''))
		  {
			 if($_GET["from"]=='teach')
				{  
					echo $this->render('_form_employee', [ //echo $this->render('_form_teacher', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				}
			 elseif($_GET["from"]=='emp')
				{
					echo $this->render('_form_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				 }
		   }
?>

    

</div>
