<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kop\y2sp\ScrollPager;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Students List');


?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['createstudent', 'from' =>'stud','is_stud'=>1], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>


    </div>

    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
         
     </div>



</div>
<div  class="row">
    <div class="col-lg-8">
        <input type="text" id="txt-search" class="form-control input-sm" placeholder="<?= Yii::t('app','Search');?>"/>
    </div>
    <div class="col-lg-4">
    
    </div>
    
</div>

<div class="wrapper wrapper-content  animated fadeInRight">

<div class="row" >
    <div id="div-initial" class="col-xs-12 col-md-10 col-lg-8">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
        'dataProvider'  => $dataProvider,
            'rowOptions'=>function($data){
               
           return ['class' => 'cheche','data-idstudent'=>$data['id'],'data-key'=>$data['id'], 'title'=>$data['label']];
            
    },
        
        'summary'       => "<b>{totalCount, number}</b> {totalCount, plural, one{étudiant} other{étudiants}}.",
        'columns' => [
            ['class'=>'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_number',
                'label'=>Yii::t('app','Id Number'),
                'format' => 'raw',
                'value' =>function($data){
                        
                        return "<span class='cheche'>".$data['id_number']."</span>";
                    },
           
            ],
            [
                'attribute'=>'first_name',
                'label'=>Yii::t('app','First Name'),
            ],
            [
                'attribute'=>'last_name',
                'label'=>Yii::t('app','Last Name'),
            ],
            [
                'attribute'=>'gender',
                'label'=>Yii::t('app','Gender'),
            ],
            [
                'attribute'=>'level',
                'label'=>Yii::t('app','Level'),
            ],
            [
                'attribute'=>'room_name',
                'label'=>Yii::t('app','Room'),
            ], 
            [
                'attribute'=>'shift_name',
                'label'=>Yii::t('app','Shift'),
            ],
                
        ],
                            
        
    ]) ?>
        <?php Pjax::end(); ?>
        
    </div>
    <div id="searchBody" class="col-xs-12 col-md-10 col-lg-8">
        
    </div>
    
         <div id="pwofil-etidyan"></div>
    
</div>


</div>


<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<!--  Srcipt pour recuperer les donnees et les afficher via AJAX dans le volet -->

<?php
$baseUrl = Yii::$app->request->BaseUrl;

$script1 = <<< JS
        $(document).ready(function(){
            $('#searchBody').hide(); 
            $('#div-initial').show(); 
        });
        
        
        
      $('.cheche').click(function(){
        var idstudent = $(this).attr('data-idstudent');
        $.get('get-student-info',{id : idstudent},function(data){
            $('#pwofil-etidyan').html(data);
        });
   });
        
   $(window).load(function() {

         $.get('get-student-info',{id : $stud_id},function(data){
            $('#pwofil-etidyan').html(data);
        });
        
});

        $('#txt-search').keyup(function(){
            $('#div-initial').remove(); 
            $('#searchBody').show();
            var valeur = $('#txt-search').val();
            
            $.get('student-ajax-search',{search : valeur},function(data){
                $('#searchBody').html(data);
           
            });
            
        });




JS;
$this->registerJs($script1);
 
?>