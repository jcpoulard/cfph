<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Persons */

$title = Yii::t('app', 'Create Employee');

if(isset($_GET["from"])&&($_GET["from"]!=''))
 {
 	if($_GET["from"]=='teach')
 	  { $title = Yii::t('app', 'Create Teacher');
 	     
 	  }
 	elseif($_GET["from"]=='emp')
 	{
 	  $title = Yii::t('app', 'Create Employee');
 	    
 	  }
  	
 }

$this->title = $title;

?>

<div class="row">
        <div class="col-lg-1">
            <p>
            <?php 
            
            if(isset($_GET["from"])&&($_GET["from"]!=''))
             {
                  if($_GET["from"]=='emp')
                    {
                       echo  Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['employee'], ['class' => 'btn btn-info btn-sm']);

                      }

             }
            
            ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content persons-create">

    
<?php

     if(isset($_GET["from"])&&($_GET["from"]!=''))
		  {
			 if($_GET["from"]=='teach')
				{  
					echo $this->render('_form_teacher', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				}
			 elseif($_GET["from"]=='emp')
				{
					echo $this->render('_form_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				 }
		   }
?>
    
    

</div>
