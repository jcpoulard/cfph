<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use kop\y2sp\ScrollPager;
use yii\grid\GridView;
use yii\widgets\Pjax;



?>


        <?php Pjax::begin(); ?>
        <?= GridView::widget([
        'dataProvider'  => $dataProvider,
            'rowOptions'=>function($data){
               
           return ['class' => 'cheche','data-idstudent'=>$data['id'],'data-key'=>$data['id'], 'title'=>$data['label']];
            
    },
        
        'summary'       => "<b>{totalCount, number}</b> {totalCount, plural, one{étudiant} other{étudiants}}.",
        'columns' => [
            ['class'=>'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_number',
                'label'=>Yii::t('app','Id Number'),
                'format' => 'raw',
                'value' =>function($data){
                        
                        return "<span class='cheche'>".$data['id_number']."</span>";
                    },
           
            ],
            [
                'attribute'=>'first_name',
                'label'=>Yii::t('app','First Name'),
            ],
            [
                'attribute'=>'last_name',
                'label'=>Yii::t('app','Last Name'),
            ],
            [
                'attribute'=>'gender',
                'label'=>Yii::t('app','Gender'),
            ],
            [
                'attribute'=>'level',
                'label'=>Yii::t('app','Level'),
            ],
            [
                'attribute'=>'room_name',
                'label'=>Yii::t('app','Room'),
            ], 
            [
                'attribute'=>'shift_name',
                'label'=>Yii::t('app','Shift'),
            ],
                
        ],
                            
        
    ]) ?>
        <?php Pjax::end(); ?>
<?php
$baseUrl = Yii::$app->request->BaseUrl;

$script1 = <<< JS
        
        $('.cheche').click(function(){
        var idstudent = $(this).attr('data-idstudent');
        $.get('get-student-info',{id : idstudent},function(data){
            $('#pwofil-etidyan').html(data);
        });
   });
        
   $(window).load(function() {

         $.get('get-student-info',{id : $stud_id},function(data){
            $('#pwofil-etidyan').html(data);
        });
        
});

        




JS;
$this->registerJs($script1);
 
?>