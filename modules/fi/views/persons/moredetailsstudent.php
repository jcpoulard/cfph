<?php
use yii\helpers\Url;
use yii\widgets\DetailView;

use yii\grid\GridView;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\fi\models\SrcCustomField;
use app\modules\fi\models\CustomFieldData;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

use kartik\editable\Editable;
use app\modules\fi\models\Relations;
use yii\helpers\ArrayHelper;
use app\modules\fi\models\ContactInfo;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcBalance;

use app\modules\rbac\models\User;

use app\modules\idcards\models\SrcIdcard;

 



$acad = Yii::$app->session['currentId_academic_year'];
 
 $modelAcad = new SrcAcademicperiods();
 $previous_year= $modelAcad->getPreviousAcademicYear($acad);
 
 $pending_balance=null;

$tit = $modelStudent->getFullName();
?>

<!-- Menu general de creation - modification et suppression d'un étudiant --> 
    <div class="row">
        <div class="col-lg-7">
            <h3><?= '<div style="color:#CD3D4C; float:left;">'.$tit.'  </div>'; ?> <div class="col-xs-6"><div class="input-group">
            <input  type="text" class="form-control" id="no-stud" name="no-stud" placeholder="<?= Yii::t('app', 'Search a student'); ?>" >
                <span class="input-group-btn"> 
                        <button type="button" class="btn btn-primary" id="view-more-detail" title="<?= Yii::t('app', 'View') ?>" >
                            <i class="fa fa-eye"></i>
                    </button> 
                    
                </span>
                <input type='hidden' id='idstud-selected'>
        </div></div>   </h3>
        </div>

 <?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))  ) 
             {
    
    ?>
       
        <div class="col-lg-5">
            <p>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['updatestudent', 'id' =>$modelStudent->id,'is_stud'=>1], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['createstudent', 'is_stud'=>1], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' =>$modelStudent->id, 'is_stud'=>1], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this student?'),
                'method' => 'post',
            ],
        ]) ?>
            </p>

   <!-- <h3><?= $modelStudent->first_name.' '.strtoupper($modelStudent->last_name); ?></h3> -->
    
        </div>
  <?php
             }
  ?>
        
    </div>
<div class="tabs-container">
    
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class='fa fa-user'></i><?= Yii::t('app', 'Personal info') ?> </a></li>
                            <li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false"><i class='fa fa-phone'></i><?= Yii::t('app', 'Contact persons') ?> </a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i class='fa fa-book'></i><?= Yii::t('app', 'Modules') ?> </a></li>
  <?php     
       if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))  ) 
             {                   
   
   ?>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i class='fa fa-dollar'></i><?= Yii::t('app', 'Billing') ?> </a></li>
 
 <?php
             }
 ?>                           
                          <!--  <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i class='fa fa-briefcase'></i><?= Yii::t('app', 'Experiences') ?> </a></li>    -->
                        </ul>
                        <div class="tab-content">
                           
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    
                                 <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    <?= Yii::t('app','Basic info') ?>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-10">    
                                                <table id="w0" class="table table-striped table-bordered detail-view table-responsive">
                                                <tbody>
                                                <tr>
                                                    <th><?= Yii::t('app', 'First Name') ?></th>
                                                    <td><?= $modelStudent->first_name ?></td>
                                                    <th><?= Yii::t('app', 'Last Name') ?></th>
                                                    <td><?= $modelStudent->last_name ?></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Gender') ?></th>
                                                    <td><?= $modelStudent->getGenders() ?></td>
                                                    <th><?= Yii::t('app', 'Blood Group') ?></th>
                                                    <td><?= $modelStudent->blood_group ?></td>
                                                    
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Birthday') ?></th>
                                                    <td><?= Yii::$app->formatter->asDate($modelStudent->birthday) ?></td>
                                                    <th><?= Yii::t('app', 'Citizenship') ?></th>
                                                    <td><?= $modelStudent->citizenship ?></td>
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Nif Cin') ?></th>
                                                    <td><?= $modelStudent->nif_cin ?></td>
                                                    <th><?= Yii::t('app', 'Id Number') ?></th>
                                                    <td><?= $modelStudent->id_number ?></td>
                                                                                                    </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Shift') ?></th>
                                                    <td>
                                                   <?php 
                                                      if(isset($modelStudent->studentOtherInfo0[0]->apply_shift))
                                                         echo $modelStudent->studentOtherInfo0[0]->applyShift->shift_name; 
                                                    ?>
                                                    </td>
                                                    <th><?= Yii::t('app', 'Room') ?></th>
                                                    <td>
                                                    <?php
                                                      //if(isset($modelStudent->studentHasCourses[0]->course))
                                                         //echo $modelStudent->studentHasCourses[0]->course0->room0->room_name; 
                                                         
                                                       if(isset($modelStudent->studentLevel->room)&&($modelStudent->studentLevel->room!=0))
                                                         echo $modelStudent->studentLevel->room0->room_name; 
                                                    ?>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Schoolarship holder') ?></th>
                                                    <td><?php $schoolarship_ = $modelStudent->getIsScholarshipHolder($modelStudent->id,$acad);
												 
												    if($schoolarship_==1)
												       $schoolarship1 = Yii::t('app','Yes');
												    elseif($schoolarship_==0) 
												     $schoolarship1 = Yii::t('app','No');
												     ?>
												     <?= $schoolarship1 ?></td>
                                                    <th><?= Yii::t('app', 'Stage') ?></th>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th><?= Yii::t('app', 'Status') ?></th>
                                                    <td><?= $modelStudent->getActive() ?></td>
                                                     <th><?= Yii::t('app', 'Username') ?></th>
                                                    <td><?php 
                                                              $username = '';
                                                              $stud_username = User::find()->where('person_id='.$modelStudent->id )->all();
                                                        
                                                           foreach($stud_username as $stud_user)
                                                             {
                                                             	$username = $stud_user->username;
                                                             	}
                                                        
                                                        echo $username; 
                                                        
                                                        ?> </td>
                                                        
                                                </tr>
                                                </tbody>
                                        </table>
                                                    </div>
                                                    <div class="col-lg-2 text-center">
                                                        <?php 
                                                        $program='';
                                                       if(isset($modelStudent->studentOtherInfo0[0]->apply_for_program)) 
                                                            $program = $modelStudent->studentOtherInfo0[0]->applyForProgram->label;
                                                        ?>
                                                        <strong> <?= $program ?></strong><br/>
                                                        <?php
                                                        if(ageCalculator($modelStudent->birthday)!=null){
                                                            ?>
                                                        <span>
                                                            <strong>
                                                        <?php 
					         	  echo '( '.ageCalculator($modelStudent->birthday).Yii::t('app',' yr old').' )';
                                                        }
					         	else
					         	  echo '';
                                                         ?>
                                                            </strong>
                                                        </span>
                                                       
                                                            <?php if($modelStudent->image=='')
                                                                 {
                                                                 	?>
                                                                <img src="<?= Url::to("@web/img/no_pic.png")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>"> 	
                                                                 	 
                                                             <?php    	}
                                                                 else
                                                                   {
                                                                   	 ?>
                                                            <img src="<?= Url::to("@web/documents/photo_upload/$modelStudent->image")?>" class="img-circle img-responsive" alt="<?= Yii::t('app', 'profile') ?>">
                                                            <?php
                                                                   }
                                                                   ?>


                                                      <span>
                                                            <strong>
                                                        <?php 
					         	  								echo Yii::t('app', 'Level').' : ';
					         	  								if(isset($modelStudent->studentLevel->level) )
					         	  								  echo $modelStudent->studentLevel->level;
					         	  						 ?>
					         	  						   </strong>
                                                        </span>
                            
                          <?php
                                       
                                              // gad si kat moun sa prepare deja
                                                $modelIdcard=SrcIdcard::find()->where('person_id='.$modelStudent->id)->all();
                                                $card_id = 0;

                                                if($modelIdcard!=null)
                                                {  foreach ($modelIdcard as $modIdcard)
                                                     $card_id =$modIdcard->id;
                                                }
                                                
                                            if($card_id != 0)
                                            {
                                               if(Yii::$app->user->can('idcards-idcard-print'))  
                                                 echo Html::a('<i class="fa fa-print"></i> '.Yii::t('app', 'Print card'), ['../idcards/idcard/printacard', 'id' =>$modelStudent->id ], ['class' => 'btn btn-success btn-sm']);
                                            }
                                               ?>
                        
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                    <?= Yii::t('app','Student contact info'); ?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table id="w1" class="table table-striped table-bordered detail-view">
                                                    <tbody>
                                                        <tr>
                                                            <th><?= Yii::t('app', 'Student email') ?></th>
                                                            <td><?= $modelStudent->email ?></td>
                                                            <th><?= Yii::t('app', 'Student phone') ?></th>
                                                            <td><?= $modelStudent->phone ?></td>
                                                        </tr> 
                                                        
                                                        <tr>
                                                            <th><?= Yii::t('app', 'City') ?></th>
                                                            <td>
                                                            <?php 
                                                                if(isset($modelStudent->cities0->id))
                                                                    echo $modelStudent->cities0->city_name; 
                                                            ?>
                                                            </td>
                                                            <th><?= Yii::t('app', 'Adresse') ?></th>
                                                            <td><?= $modelStudent->adresse ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                    <?= Yii::t('app','Other info')?>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <table id="w1" class="table table-striped table-bordered detail-view">
                                                    <tbody>
                                                <?php
                                                $modelCustomFieldData = new CustomFieldData();
                                                $criteria = ['field_related_to'=>'stud'];
                                                $data_custom_label = SrcCustomField::find()->where($criteria)->all();
                                                $id_student = $_GET['id'];
                                                $konte_liy=0;
                                                    foreach($data_custom_label as $dcl){
                                                        $konte_liy++;
                                                        if($konte_liy==0)
                                                            echo '<tr>';
                                                ?>
                                                    <th><?=  $dcl->field_label; ?></th>
                                                    <td><?php echo $modelCustomFieldData->getCustomFieldValue($id_student,$dcl->id);  ?></td>
                                                    <?php 
                                                    if($konte_liy==2){
                                                        echo '</tr>';
                                                        $konte_liy = 0;
                                                    }
                                                    } ?>
                                                    <tr>
                                                        <th><?= Yii::t('app', 'Comment') ?></th>
                                                        <td colspan="3"><?= $modelStudent->comment ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           
                                </div>    
    <!-- Fin de l'accordeon  -->
                           
                         </div>       
                            
    
  
         <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                  
                        <div class="row">
                           <div style="width:auto; float:left;">
                              <h4><?= Yii::t('app', 'Modules list') ?> </h4>
                           </div>
                           
  <?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))  ) 
             {
    
    ?>                          
                           <div class="col-lg-1"  style="margin-top:-12px; margin-left: 25px;">
                               <?= Html::a('<i class="fa fa-plus"></i> ' , ['/fi/studenthascourses/createsolo','id'=>$_GET['id'],'from'=>'stud',], ['class' => 'btn btn-primary btn-sm', 'style'=>['height'=>'27px',],'title'=>Yii::t('app','Add')]) ?>
                           </div>
  <?php
             }
  ?>  
                           
                           
                         </div>
                                
     	<?php  $include_course_withoutGrade = infoGeneralConfig('include_course_withoutGrade');
     	       
     	       $modelC = new SrcCourses;
			      
			   $allCourses = $modelC->getCourseByStudentID($_GET['id']);
		?>	      
			
			                 
              <hr/>   
                 
                                
                                    <table class="table table-responsive">
                                        
                                        <thead>
                                        <th style="width:12%" > <?=  Yii::t('app', 'Code') ?> </th>
                                         <th>
                                            <?= Yii::t('app', 'Modules') ?>
                                        </th>
                                        <th > <?=  Yii::t('app', 'Duration') ?> </th>
                                        <th > <?=  Yii::t('app', 'Room') ?> </th> 
									     <th > <?=  Yii::t('app', 'Passing Grade') ?> </th>
									     <th > <?=  Yii::t('app', 'Grade') ?> </th>
									     <th > <?=  Yii::t('app', 'Mention') ?> </th> 
									                                            
									     </thead>
                                        
                                      <?php
                                      
                                      		     foreach($allCourses as $course)
											      { 
											           $modelGrade = new SrcGrades;
											           $modelStudCourse = new SrcStudentHasCourses;
											           
											           $room = '';
											           //if(isset($modelStudent->studentHasCourses[0]->course))
                                                          //$room=$modelStudent->studentHasCourses[0]->course0->room0->room_name;
                                                          
                                                       if(isset($modelStudent->studentLevel->room)&&($modelStudent->studentLevel->room!=0))
                                                         $room = $modelStudent->studentLevel->room0->room_name;  
							                              

											            $modelGradeInfo = $modelGrade->getGradeInfoByStudentCourse($_GET['id'],$course->id);
											            
											            $modelIspass = $modelStudCourse->getIsPassByStudentCourse($_GET['id'],$course->id);
											            
											            if($modelIspass==0)
											              {  $color="color:#ff0000;";
											              	}
											            else
											               $color="";
											        
											       if($include_course_withoutGrade==0)
											        {
												        if(isset($modelGradeInfo->grade_value)&&($modelGradeInfo->grade_value>=0) ) //si li pa konpoze pou kou a pa metel nan kane a
												         {
												         	$href='#';  // '../grades/update?id='.$modelGradeInfo->id;
											          	    
											?>	  
												   
												  <tr  style="<?= $color ?>" >
												     <td > <a href="<?= $href ?>" class='popopKou' ><span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"><?=  $course->module0->code ?> </span></a> </td>
												     <td ><span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"> <?= $course->module0->subject0->subject_name  ?> </span></td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $room  ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $modelGradeInfo->grade_value ?> </td>
												     <td > <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
												   
												  </tr>
											<?php
											              }
											              
											         }
											       elseif($include_course_withoutGrade==1)
											          {  
											                     $href='#';	 
                                                                                                            if( (Yii::$app->user->can('superadmin'))||(Yii::$app->user->can('fi-grades-update_validatedG')) )
                                                                                                              $href='../grades/create?stud='.$_GET['id'].'&course='.$course->id.'&from=studeview&part';
                                                                                                         
											          	  $grade= '-';
											          	 if(isset($modelGradeInfo->grade_value)&&($modelGradeInfo->grade_value>=0) )
											          	   { $grade= $modelGradeInfo->grade_value;
											          	       $href='#';  // '../grades/update?id='.$modelGradeInfo->id;
											          	    }
                                                                                                            
											          	 
								  	                    if($grade== '-')  
								  	                     {       
											     ?>
											       <tr>
												     <td > <a href="<?= $href ?>" class='popopKou' ><span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"><?=  $course->module0->code ?> </span></a> </td>
												     <td > <span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"><?= $course->module0->subject0->subject_name  ?> </span> </td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $room  ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $grade ?> </td>
												     <td > <?=  Yii::t('app', 'N/A') ?> </td>
												 
												  </tr>
											   <?php       	
								  	                     }  
                                                                                          else
                                                                                            {
											     ?>
											       <tr  style="<?= $color ?>" >
												     <td > <a href="<?= $href ?>" class='popopKou' ><span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"><?=  $course->module0->code ?> </span></a> </td>
												     <td ><span data-toggle="tooltip" title="<?= $course->teacher0->fullName ?>"> <?= $course->module0->subject0->subject_name  ?> </span></td>
												     <td > <?=  $course->module0->duration ?> </td>
												     <td > <?=  $room  ?> </td>
												     <td > <?=  $course->passing_grade ?> </td>
												     <td > <?= $grade ?> </td>
												     <td > <?= $modelStudCourse->getValueIsPass($modelIspass) ?> </td>
											
												  </tr>
											   <?php     
                                                                                            }
                                                                                           
											          	}
											         
											      }
											      
											?>
                                                                               
                                                                            
                                     
                                                                               
                                    </table>
                   
                                </div>
                            </div>
                            
  <?php     
       if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))  ) 
             {                   
   
   ?>

                     <div id="tab-3" class="tab-pane">
                                   
                                <div class="panel-body">
                                <div class="row">
                                        <div style="width:auto; float:left;">
                                            <h4><?= Yii::t('app', 'Transactions list') ?> </h4>
                                        </div>
  <?php
     
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))  ) 
             {
    
    ?>                                                                  
                                        <div class="col-lg-1" style="margin-top:-12px; margin-left: 25px;">                          
                                           <?= Html::a('<i class="fa fa-plus"></i> ' , ['/billings/billings/view','id'=>$_GET['id'],'ri'=>0,'part'=>'rec'], ['class' => 'btn btn-primary btn-sm', 'style'=>['height'=>'27px',],'title'=>Yii::t('app','Create Billings')]) ?>
                                        </div>
  <?php
             }
  ?>
                                        
                                    </div>
                                    
                                    
                                         <?php
							              $searchModel = new SrcBillings();
							              $dataProvider = $searchModel->searchForView($_GET['id'],$acad);
							 echo GridView::widget([
							        'dataProvider' => $dataProvider,
							        'id'=>'billing-grid',
							        //'filterModel' => $searchModel,
							        'summary'=>"",
							        'columns' => [
							            ['class' => 'yii\grid\SerialColumn'],
							
							            //'id',
							            'date_pay:date',
							            
							            
							         /*   ['attribute'=>'id',
										'label' => Yii::t('app', 'Transaction ID'),
							               'content'=>function($data,$url){
								                return Html::a($data->id, Yii::getAlias('@web').'/billings/billings/view?id='.$data->id.'&ri=0&wh=inc_bil', [
							                                    'title' => $data->id,
							                        ]);
							               }
							             ],
							          */ 
							            //'fee_period',
							            ['attribute'=>'fee_period',
										'label' => Yii::t('app', 'Fee'),
							               'content'=>function($data,$url){
								                return Html::a($data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->academicPeriod->name_period.')', Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$data->student.'&month_='.getMonth($data->date_pay).'&year_='.getYear($data->date_pay).'&ri=0&wh=inc_bil', [
							                                    'title' => $data->feePeriod->fee0->fee_label.' ('.$data->feePeriod->program0->label.')',
							                        ]);
							               }
							             ],
							
							            //'amount_to_pay',
							            ['attribute'=>'amount_to_pay',
										'label' => Yii::t('app', 'Amount To Pay'),
							               'content'=>function($data,$url){
								                return Html::a($data->getAmountToPay(), Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$data->student.'&month_='.getMonth($data->date_pay).'&year_='.getYear($data->date_pay).'&ri=0&wh=inc_bil', [
							                                    'title' => $data->getAmountToPay(),
							                        ]);
							               }
							             ],
							             
							            //'amount_pay',
							            ['attribute'=>'amount_pay',
										'label' => Yii::t('app', 'Amount Pay'),
							               'content'=>function($data,$url){
								                return Html::a($data->getAmountPay(), Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$data->student.'&month_='.getMonth($data->date_pay).'&year_='.getYear($data->date_pay).'&ri=0&wh=inc_bil', [
							                                    'title' => $data->getAmountPay(),
							                        ]);
							               }
							             ],
							             
							             ['attribute'=>Yii::t('app', 'Payment M.'),//'payment_method',
										   'label' => Yii::t('app', 'Payment M.'),
							               'content'=>function($data,$url){
								                return Html::a($data->paymentMethod->method_name, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$data->student.'&month_='.getMonth($data->date_pay).'&year_='.getYear($data->date_pay).'&ri=0&wh=inc_bil', [
							                                    'title' => $data->paymentMethod->method_name,
							                        ]);
							               }
							             ],
							
							             //'balance',
							             ['attribute'=>'balance',
										'label' => Yii::t('app', 'Balance'),
							               'content'=>function($data,$url){
								                return Html::a($data->getBalance(), Yii::getAlias('@web').'/index.php/billings/billings/view?id='.$data->student.'&month_='.getMonth($data->date_pay).'&year_='.getYear($data->date_pay).'&ri=0&wh=inc_bil', [
							                                    'title' => $data->getBalance(),
							                        ]);
							               }
							             ],
							           
							                       
							        ],
							    ]); 
							      
							 ?>

     	                        </div>
                            </div>
                            
  <?php
             }  
  ?>                          
                            
                             <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-1">
                                            <h2>
                                            <a><i id="add-new-contact" class="fa fa-plus" title="<?= Yii::t('app','Add new contact'); ?>"></i> </a>
                                            </h2>
                                        </div>
                                        <div class="col-lg-2">
                                            <h3><?= Yii::t('app', 'Contact Persons') ?> </h3>
                                        </div>
                                        <div class="col-lg-9">
                                            
                                            
                                            
                                            <?php
                                            /*
                                            $modelContactVide = new ContactInfo();
                                            $editable = Editable::begin([
                                                'model'=>$modelContactVide,
                                                'attribute'=>'contact_name',
                                                'asPopover' =>false,
                                                
                                                'type'=>'success',
                                                'inlineSettings' => [
                                                    'templateBefore' => Editable::INLINE_BEFORE_2, 
                                                    'templateAfter' =>  Editable::INLINE_AFTER_2
                                                ],
                                                'contentOptions' => ['style'=>''],
                                                'displayValue' =>Yii::t('app','Add new contact'),
                                                'options'=>['placeholder'=>Yii::t('app','Enter contact name'),'id'=>'vide']
                                            ]);
                                            $form = $editable->getForm();
                                            echo Html::hiddenInput('kv-complex', '1');
                                            $editable->afterInput = 
                                               "<div class='row'><div class='col-lg-6'>".     
                                                $form->field($modelContactVide, 'contact_relationship')->widget(\kartik\widgets\Select2::classname(), [
                                                    'data'=>ArrayHelper::map(Relations::find()->all(),'id','relation_name' ), //$data, // any list of values
                                                    'options'=>['placeholder'=>Yii::t('app','Enter relationship'),'id'=>'combovide'],
                                                    'pluginOptions'=>['allowClear'=>true]
                                                ])->label(false) . "</div><div class='col-lg-6'>" .
                                                $form->field($modelContactVide, 'profession')->textInput(['placeholder'=>Yii::t('app','Enter profession')])->label(false)."</div><div class='col-lg-6'>".
                                                $form->field($modelContactVide, 'phone')->textInput(['placeholder'=>Yii::t('app','Enter phone')])->label(false).
                                                    "</div><div class='col-lg-6'>".
                                                $form->field($modelContactVide, 'address')->textInput(['placeholder'=>Yii::t('app','Enter address')])->label(false)."</div><div class='col-lg-6'>".
                                                $form->field($modelContactVide, 'email')->textInput(['placeholder'=>Yii::t('app','Enter email')])->label(false)."</div></div>";
                                            Editable::end();
                                            
                                            
                                            */
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="place-contact"></div>
                                       <!--     
                                          <?php
                                         $data_relation_select = Relations::findBySql("SELECT id, relation_name FROM relations ORDER BY id")->asArray()->all(); 
                                         print_r($data_relation_select);
                                          $i = 1;
                                           foreach ($modelContactStudent as $mcs){
                                                $modelContactInfo = ContactInfo::findOne($mcs->id);
                                               ?>
                                                    
                                    <table id="w0" class="table table-striped table-bordered detail-view table-responsive">
                                        <thead>
                                            <tr>
                                                <th colspan="4"><?= Yii::t('app','Contact # {number}',['number'=>$i])?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr>
                                                <th><?= Yii::t('app','Contact name'); ?></th>
                                                <td>
                                            
                                            <?php 
                                            $editable = Editable::begin([
                                                'model'=>$modelContactInfo,
                                                'attribute'=>'contact_name',
                                                'asPopover' =>false,
                                                'id'=>'unique-id',
                                                'inlineSettings' => [
                                                    'templateBefore' => Editable::INLINE_BEFORE_2, 
                                                    'templateAfter' =>  Editable::INLINE_AFTER_2
                                                ],
                                                'contentOptions' => ['style'=>''],
                                                'displayValue' =>$mcs->contact_name,
                                                'options'=>['placeholder'=>Yii::t('app','Enter contact name'),'id'=>$i]
                                            ]);
                                            $form = $editable->getForm();
                                            echo Html::hiddenInput('contact_id', $mcs->id);
                                            $editable->afterInput = 
                                               "<div class='row'><div class='col-lg-6'>".     
                                                $form->field($modelContactInfo, 'contact_relationship')->widget(\kartik\widgets\Select2::classname(), [
                                                    'data'=>ArrayHelper::map(Relations::find()->all(),'id','relation_name' ), //$data, // any list of values
                                                    'options'=>['placeholder'=>Yii::t('app','Enter relationship'),'id'=>'combo'.$i],
                                                    'pluginOptions'=>['allowClear'=>true]
                                                ])->label(false) . "</div><div class='col-lg-6'>" .
                                                $form->field($modelContactInfo, 'profession')->textInput(['placeholder'=>Yii::t('app','Enter profession')])->label(false)."</div><div class='col-lg-6'>".
                                                $form->field($modelContactInfo, 'phone')->textInput(['placeholder'=>Yii::t('app','Enter phone')])->label(false).
                                                    "</div><div class='col-lg-6'>".
                                                $form->field($modelContactInfo, 'address')->textInput(['placeholder'=>Yii::t('app','Enter address')])->label(false)."</div><div class='col-lg-6'>".
                                                $form->field($modelContactInfo, 'email')->textInput(['placeholder'=>Yii::t('app','Enter email')])->label(false)."</div></div>";
                                            Editable::end();
                                          
                                          ?>
                                                </td>
                                                <th>
                                                    <?= Yii::t('app','Relation contact'); ?>
                                                </th>
                                                <td><?= $mcs->relationName; ?></td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Profession'); ?></th>
                                             <td><?= $mcs->profession; ?></td>
                                             <th><?= Yii::t('app','Phone'); ?></th>
                                             <td><?= $mcs->phone; ?></td>
                                         </tr>
                                         <tr>
                                             <th><?= Yii::t('app','Email'); ?></th>
                                             <td><?= $mcs->email; ?></td>
                                             <th><?= Yii::t('app','Address'); ?></th>
                                             <td><?= $mcs->address; ?></td>
                                         </tr>
                                          </tbody>
                                    
                                    </table>
                                          <?php
                                          $i++;
                                           }
                                          ?>
                                            
                                       -->
                                          </div>
                                    </div>
                                    </div>
                                   
                                    
                                   
                                    
                                    
                                </div>
                            </div>
                           
</div>   


 <div class="modal fade lg" id="modal-add-contact" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add new contact') ?></h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                       <!-- <label for="musee">Nom mus&eacute;e</label> -->
                       <input type="text" class="form-control" id="contact-name" name="contact-name" placeholder="<?= Yii::t('app','Enter contact name'); ?>">
                           
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                       <!-- <label for="titre-expo">Titre de l'exposition</label> -->
                       <select id="relation-name" name="relation-name" class="form-control">
                           <option value=""><?= Yii::t('app','Enter relationship'); ?></option>
                           <?php 
                                foreach($data_relation_select as $drs){
                                    ?>
                           <option value="<?= $drs['id'] ?>"><?= $drs['relation_name'] ?></option>
                           <?php 
                                }
                           ?>
                       </select>
                            
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="profession"></label>
                        <input type="text" class="form-control" id="profession" name="profession" placeholder="<?= Yii::t('app','Enter profession'); ?>">
                    </div>
                </div>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="phone-contact"></label>
                        <input type="text" class="form-control" id="phone-contact" name="phone-contact" placeholder="<?= Yii::t('app','Enter phone'); ?>">
                    </div>
                </div>
            </div>
            <div class='row'>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="email"></label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="<?= Yii::t('app','Enter email'); ?>">
                    </div>
                </div>
                <div class='col-xs-6'>
                    <div class="form-group">
                        <label for="contact-address"></label>
                        <input type="text" class="form-control" id="contact-address" name="contact-address" placeholder="<?= Yii::t('app','Enter address'); ?>">
                    </div>
                </div>
            </div>
            
         </div>
            
        <div class="modal-footer">
            <div class='row'>
                <div class='col-xs-2'>
                    
                </div>
                <div class='col-xs-4'>
                     <div class="form-group">
                        <button type="button" id='save-contact' class='btn btn-success'>Enregistrer</button>
                    </div> 
                </div>
                <div class='col-xs-4'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
                <div class='col-xs-2'>
                    
                </div>
            </div>
          
          
        </div>
           </div>
      </div>
      
    </div>



<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(function(){
    $(document).on('click','.popopKou',function(){
        var idcourse = $(this).attr('data-idcourse');
        $.get('$baseUrl/index.php/fi/courses/viewstudent',{'id':idcourse},function(data){
            
            $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });
        
       
      
    });
        
   $(document).ready(function(){
         $.get('$baseUrl/index.php/fi/persons/show-contact',{'id_student':$modelStudent->id},function(data){
                $('#place-contact').html(data);
            });
   });    
   
   $('#add-new-contact').click(function(){
        $("#modal-add-contact").modal();
       });
        
  $('#save-contact').click(function(){
        var id_student = $modelStudent->id;
        var nom_contact = $('#contact-name').val();
        var relation_name = $('#relation-name').val();
        var profession = $('#profession').val(); 
        var phone_contact = $('#phone-contact').val(); 
        var email = $('#email').val(); 
        var contact_address = $('#contact-address').val();
       
        $.get('$baseUrl/index.php/fi/persons/save-contact',{
            'id_student':id_student,
            'nom_contact':nom_contact,
            'relation_name':relation_name,
            'profession':profession,
            'phone_contact':phone_contact,
            'email':email,
            'contact_address':contact_address
            },
            function(data){
                $('#place-contact').html(data);
                $('#modal-add-contact').modal('toggle');
         });
     
   }); 
   
        
        
        
        
    $('#no-stud').typeahead(
            {  
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_all_students; 
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                   
                 $('#idstud-selected').val(map[item].id);
                
                return item;
            }
                 
               

             
            }); 
   
          
          $('#view-more-detail').click(function(){
            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/fi/persons/moredetailsstudent?id="+id_stud+"&is_stud=1";
            
            }
           
        
            });    
            
});
        
        
 $(document).on("keypress", "input", function(e){
        
           var key=e.keyCode || e.which;
           
        if(key == 13){

            var id_stud = $('#idstud-selected').val();
            if(id_stud!==''){
                window.location.href = "/cfph/web/index.php/fi/persons/moredetailsstudent?id="+id_stud+"&is_stud=1";
            
            }

        }

    });
        

JS;
$this->registerJs($script2);

?> 





   