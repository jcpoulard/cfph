<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\Persons;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcAcademicperiods */
/* @var $dataProvider yii\data\ActiveDataProvider */

$username ='oo';
        
// if(isset(Yii::$app->user->identity->username))
//   $username = Yii::$app->user->identity->username;


$acad = Yii::$app->session['currentId_academic_year'];

$this->title = Yii::t('app', 'Teachers List');
?>
<?php    $total_person= 0;
       if($allTeachers!=null)
         $total_person= sizeOf($allTeachers);
    ?>
 
 <div class="row">
       <div class="" style="width:auto;float:left; margin-left:20px;">
            <?php //echo Html::a('<i class="fa fa-plus"></i> ', ['createemployee', 'from' =>'teach','is_stud'=>0], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>


    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>



</div>
<div class="wrapper wrapper-content  animated fadeInRight">


            <div class="row">


                <div class="col-lg-8">
                     <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th>#</th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Working department'); ?></th>
				            <th><?= Yii::t('app','Gender'); ?></th>
				            <th></th>


				            </tr>
				        </thead>
				        <tbody>

                <!-- kontni tab yo -->
                 <?php   $emp_id =0;
                           if($total_person!=0)
			                  {


                                    //voye tout done nap bezwen nan detail.php nan sa :onclick="getSummary('.$teacher->id.')"

                                      $cmpt=0;
                                      $emp_id =0;
                                      $full_name1='';
                                      $src_img1='';
                                      $konte=1;
                                      $comment1='';
                                      $comment='';
                                      $poste1 = "";
                                      $idNumber = '';
                                      $idNumber1 = '';
                                      $meta_str = $idNumber = str_replace("'","\'",Yii::t('app', 'Id Number')).'   ,   '.Yii::t('app', 'Email').'   ,   '.Yii::t('app', 'Phone').'   ,   '.Yii::t('app', 'Hire date').'   ,   '.Yii::t('app', 'Working department').'   ,   '.Yii::t('app', 'Job status').'   ,   '.Yii::t('app', 'Notes').'   ,   '.Yii::t('app'   ,   ' yr old').'   ,   '.Yii::t('app', 'Progression').'   ,   '.Yii::t('app', 'Modules').'   ,   '.Yii::t('app', 'Update').'   ,   '.Yii::t('app', 'More details').'   ,   '.Yii::t('app', 'Course').'   ,   '.Yii::t('app', 'Informations');
                                      $data_str = "";

			              //    <!-- Codage de la pagination des employees  -->

			                                $array = $allTeachers;
			                                $total_element = 0;
			                                if(isset($_GET['limit'])){
			                                    $limit = $_GET['limit'];
			                                }else{
			                                    $limit = Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']);//3;
			                                }
			                                foreach ($array as $a){
			                                    $total_element++;
			                                }
			                                $nombre_page = ceil($total_element/$limit);
			                                if(isset($_GET['page'])){
			                                    $page = $_GET['page'];
			                                }else{
			                                    $page = 1;
			                                }
			                                if(isset($_GET['SrcPersons']['globalSearch'])){
			                                    $search_value = 'SrcPersons[globalSearch]='.$_GET['SrcPersons']['globalSearch'];
			                                }else{
			                                    $search_value = "";
			                                }
			                                $rows = array_chunk($array,$limit);
			                                $result = $rows[$page-1];


			                //  <!-- Fin du codage de la pagination -->

                                        foreach($result as $teacher)
                                          {
                                          	   if($cmpt==0)
                                          	   {  $emp_id = $teacher->id;
                                          	      $full_name1=$teacher->getFullName();//first_name." ".$teacher->last_name;
                                          	      $full_name1 = str_replace("'","\'",$full_name1);

                                                      $poste1 = $teacher->title;
                                                     $poste1 = str_replace("'","\'",$poste1);

                                                     $comment1 = $teacher->comment;
                                                     $comment1 = str_replace("'","\'",$comment1);

                                                     //$src_img1= Url::to("@web/$teacher->image");
                                                     if($teacher->image!='')
									                   $src_img1= Url::to("@web/documents/photo_upload/$teacher->image");
									                else
									                  $src_img1= Url::to("@web/img/no_pic.png");
								                  
                                                     if(ageCalculator($teacher->birthday)!=null)
                                                            $age1= '( '.ageCalculator($teacher->birthday).Yii::t('app',' yr old').' )';
                                                     else
                                                         $age1= "";
                                                     $email1 = $teacher->email;
                                                     $phone1 = $teacher->phone;
                                                     $hire_date = $teacher->hiredate;

                                                     $job_status1 = $teacher->jobstatus;
                                                     $job_status1 = str_replace("'","\'",$job_status1);

                                                     $working_department1 = $teacher->workingDepartment;
                                                     $working_department1 = str_replace("'","\'",$working_department1);
                                                     
                                                     $idNumber1= $teacher->id_number;
                                                     $idNumber1 = str_replace("'","\'",$idNumber1);

                                          	   	  $cmpt=1;
                                          	   	}

                                              $full_name=$teacher->getFullName();//first_name." ".$teacher->last_name;
                                              $full_name = str_replace("'","\'",$full_name);

                                              $first_name= $teacher->first_name;
                                              $last_name = $teacher->last_name;

                                              $poste = $teacher->title;
                                              $poste = str_replace("'","\'",$poste);

                                              $comment = $teacher->comment;
                                              $comment = str_replace("'","\'",$comment);

                                               if(ageCalculator($teacher->birthday)!=null)
                                                            $age = '( '.ageCalculator($teacher->birthday).Yii::t('app',' yr old').' )';
                                                     else
                                                         $age = "";
                                               $email = $teacher->email;
                                               $phone = $teacher->phone;
                                               $hire_date = $teacher->hiredate;

                                               $job_status = $teacher->jobstatus;
                                               $job_status = str_replace("'","\'",$job_status);

                                               $working_department = $teacher->workingDepartment;
                                               $working_department = str_replace("'","\'",$working_department);
                                               
                                               $idNumber= $teacher->id_number;
                                                     $idNumber = str_replace("'","\'",$idNumber);

                                              //$src_img= Url::to("@web/$teacher->image");
                                              if($teacher->image!='')
								                   $src_img= Url::to("@web/documents/photo_upload/$teacher->image");
								                else
								                  $src_img= Url::to("@web/img/no_pic.png");
								                  
                                              $data_str = $idNumber.'   ,   '.$full_name.'   ,   '.$poste.'   ,   '.$email.'   ,   '.$phone.'   ,   '.$hire_date.'   ,   '.$job_status.'   ,   '.$working_department.'   ,   '.$comment  ;


                                              $url = "";
                                                if(Yii::$app->session['profil_as'] ==0)
                                                    $url = "/../modules/fi/views/persons/pers_detail_employee.php";
                                                 elseif(Yii::$app->session['profil_as'] ==1)
                                                      $url = "/../modules/fi/views/persons/pers_detail_teacher.php";
                  
                                              echo '  <tr>

                                                    <td ><a class="client-link">'.$konte.' </a> </td>
                                                    <td ><a  onclick="getSummary(\''.$teacher->id.'\',\'0\',\''.$full_name.'\',\''.$src_img.'\',\''.$meta_str.'\',\''.$username.'\',\''.$data_str.'\',\''.$url.'\')" class="client-link">'.$teacher->first_name.'</a></td>
                                                    <td ><a  onclick="getSummary(\''.$teacher->id.'\',\'0\',\''.$full_name.'\',\''.$src_img.'\',\''.$meta_str.'\',\''.$username.'\',\''.$data_str.'\',\''.$url.'\')" class="client-link">'.$teacher->last_name.'</a></td>
                                                    <td >'.$working_department.'</td>
                                                    <td ><span class="">'.$teacher->getGenders().'</span></td>
                                                    <td ><a href="mailto:'.$teacher->email.'" title="'.$teacher->email.'"><i class="fa fa-envelope"> </i></a></td>

                                                </tr>';

                                            $konte++;
                                          }


                              } ?>

                                </tbody>
                    </table>


                 </div>



                  </div>



 <?php    if($emp_id!=0)
       {
       	?>
     <!-- kolonn adwat la (detay sou...) -->
                <div class="col-sm-12 col-md-3 col-lg-4">
                    <div class="ibox ">

                        <div class="ibox-content" >
                            <div class="tab-content">

                    <!-- kolonn adwat la pou 1e tab la(detay sou yon moun) -->
                              <div id="contact" >

                                   <div class="row m-b-lg" style="margin-bottom:10px;" >

		                                   <div class="row">
			                                        <div class="col-lg-12 text-center">
			                                        <?php $full_name1 = str_replace("\'","'",$full_name1);  ?>
                                                        <h4><?php echo $full_name1; ?> </h4>
			                                        </div>
		                                   </div>

		                                   <div class="row" style="margin: 0 auto;" >
		                                       <div class="col-sm-2"></div>
			                                      <div class="col-sm-8"  >
				                                     <div class="m-b-sm">
                                                         <img alt="image" class="img-circle img-responsive" src="<?php echo $src_img1; ?>" />

				                                      </div>
			                                       </div>
			                                     <div class="col-sm-2"></div>
			                                   </div>

			                                  <div class="row" style="margin: 0 auto;" >
			                                      <div class="col-lg-12 text-center">
			                                         <div class="col-lg-12 text-center">

					                                            <strong>
					                                            <?php $poste1 = str_replace("\'","'",$poste1);  ?>
                                                                    <?= $poste1; ?>
					                                            </strong>  <br/><br/>


			                                           </div>
			                                        </div>

		                                     </div>


<?php
        if(Yii::$app->session['profil_as'] ==0)
          {
   ?>

		                                       <div class="row" style="margin: 0 auto;" >
                     <?php
                    
                     if($username!='logipam')
                        {

                     ?>
		                                      <!--     <div class="col-sm-4">
                                               <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['updateemployee', 'id' =>$emp_id,'is_stud'=>0,'from'=>'teach'], ['class' => 'btn btn-primary btn-sm']) ?>
                                           </div>
                                           -->
                                           
                                           <div class='col-lg-12'> <div class='col-sm-2'></div>
                                               <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'More details'), ['moredetailsemployee', 'id' =>$emp_id,'is_stud'=>0,'from'=>'teach'], ['class' => 'btn btn-primary btn-sm']) ?>
		                                           <div class="col-sm-2"></div>
		                                        </div>
                        <?php
                        }
                        ?>
		                                       </div>
 <?php
          }
   ?>


                                     </div><!-- fin class="row m-b-lg"  -->




                                    <div class="client-detail">
                                    <div class="full-height-scroll">
                                           <ul class="list-group clear-list">
                                           <li class="list-group-item">
                                               <?php $idNumber1 = str_replace("\'","'",$idNumber1);  ?>
                                                <span class="pull-right"> <?= $idNumber1 ?> </span>
                                             <b>  <?= Yii::t('app', 'Id Number') ?>  </b>
                                            </li>
                                        
                                            <li class="list-group-item">
                                                <span class="pull-right"><?= $email1 ?> </span>
                                                <b>  <?= Yii::t('app', 'Email') ?>  </b> 
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> <?= $phone1 ?></span>
                                              <b>   <?= Yii::t('app','Phone'); ?> </b> 
                                            </li>
 
 <?php
        if(Yii::$app->session['profil_as'] ==0)
          {
   ?>
                                           
                                            <li class="list-group-item fist-item">
                                                <span class="pull-right"><?= ChangeDateFormat($hire_date); ?>  </span>
                                                 <b> <?= Yii::t('app','Hire date'); ?>  </b> 
                                            </li>

                                            <li class="list-group-item">
                                            <?php $working_department = str_replace("\'","'",$working_department);  ?>
                                                <span class="pull-right"> <?= $working_department ?> </span>
                                                <b>  <?= Yii::t('app','Working department') ?> </b> 
                                            </li>
                                            <li class="list-group-item">
                                            <?php $job_status = str_replace("\'","'",$job_status);  ?>
                                                <span class="pull-right"> <?= $job_status; ?> </span>
                                              <b>    <?= Yii::t('app','Job status')?> </b> 
                                            </li>

                                            <li class="list-group-item">
                                            <?php $comment1 = str_replace("\'","'",$comment1);  ?>
                                                <span class="pull-right"> <?= $comment1; ?> </span>
                                               <b> <?= Yii::t('app','Notes')?>  </b>
                                            </li>
<?php
          }
   ?>

                                        </ul>


                                    </div>
                                    </div>
                          </div><!-- fen id="contact"-->

       <!-- fen kolonn adwat la pou 1e tab la(detay sou yon moun) -->



                            </div>
                        </div>
                    </div>
                </div>
           <!-- fen kolonn adwat la (detay sou...) -->
  <?php
       }
  ?>



            </div>
        </div>

<script>

function getSummary(id,is_stud,full_name,src_img,meta_str,username,data_str,url_)
{
        
   $.ajax({

     type: "GET",
     url: "<?= Yii::$app->request->baseUrl ?>"+url_,
     data: "id=" + id + "&is_stud=" + is_stud + "&from=teach&full_name=" + full_name + "&src_img=" + src_img + "&meta_str=" + meta_str +"&username=" + username + "&data_str=" + data_str, // appears as $_GET['id'] @ your backend side
     success: function(data) {
           // data is ur summary
          $('#contact').html(data);
    }


   });

}
</script>

<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Teachers List'},
                    {extend: 'pdf', title: 'Teachers List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
