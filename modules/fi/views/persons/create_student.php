<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Persons */

$this->title = Yii::t('app', 'Create Student');

?>
<div class="row">
        <div class="col-lg-1">
            <p>
            <?php // echo Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['student'], ['class' => 'btn btn-info btn-sm']); ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
    </div>

<div class="wrapper wrapper-content persons-create">

    <?= $this->render('_form_student', [
        'model' => $model,
        'modelStudentOtherInfo' => $modelStudentOtherInfo,
        'modelCustomFieldData'=>$modelCustomFieldData,
        'modelStudentLevel'=>$modelStudentLevel,
        'disable_field'=>$disable_field,
    ]) ?>

</div>
