<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\fi\models\SrcCities;
use app\modules\fi\models\SrcTitles;
use app\modules\fi\models\SrcDepartmentInSchool;
use app\modules\fi\models\DepartmentHasPerson;

use app\modules\fi\models\JobStatus;
use app\modules\fi\models\SrcJobStatus;

use app\modules\fi\models\SrcProgram;

use app\modules\fi\models\CustomField;
use app\modules\fi\models\CustomFieldData;


$acad = Yii::$app->session['currentId_academic_year'];


$from=$_GET['from'];  //from teach or emp
$programs_leaders = infoGeneralConfig('programs_leaders');
$automatic_code_ = infoGeneralConfig('automatic_code');
$disabled = '';
$placeholder_code = Yii::t('app','Id Number');

if($automatic_code_ ==1)
 { $disabled = 'disabled';
  $placeholder_code = Yii::t('app','Id Number').' '.Yii::t('app',' automatic');
 }
  

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Persons */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persons-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data'] ]); ?>
    
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'gender')->widget(Select2::classname(), [
                       'data'=>gender_array(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select gender  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
            ?>
        </div>
        <div class="col-lg-6">
            <?php
              echo $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                        'options'=>['placeholder'=>'' ],
                        'language'=>'fr',
                        'pluginOptions'=>[
                             'autoclose'=>true,
                            'format'=>'yyyy-mm-dd',
                            ],

                        ] );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'cities')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcCities::find()->all(),'id','city_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select city  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'blood_group')->widget(Select2::classname(), [
                       'data'=>blood_array(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select blood group  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
             <?= $form->field($model, 'id_number')->textInput(['maxlength' => true, 'disabled'=>$disabled,'placeholder' => $placeholder_code ]) ?>
        </div> 
        <div class="col-lg-6">
            <?= $form->field($model, 'adresse')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'nif_cin')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'citizenship')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'active')->widget(Select2::classname(), [
                       'data'=>active2add_array(),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select status  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
        </div>
        <div class="col-lg-6">
            <?php if($model->image!='')
      {  
         $src_img= Url::to("@web/documents/photo_upload/$model->image");
      	   echo '<img alt="image" class="img-circle" src="'.$src_img.'" style="width: 62px; height:62px;" alt="'.Yii::t('app', 'profile').'">';
      	  
      	}
     ?><?= $form->field($model, 'file')->fileInput() ?>
        </div>
         
    </div>
    <div class="row"> 
        <div class="col-lg-6">
             <?= $form->field($modelPersonTitles, 'id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcTitles::find()->all(),'id','title_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select title  --'),
                                              'onchange'=>'submit()',
                                   ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Tile')) ?>
        </div>
        
        <?php
              if($modelPersonTitles->id!='')
                {
                	 $title_name ='';  
                	 
                   $title_model = SrcTitles::findOne($modelPersonTitles->id);
                   if(isset($title_model) &&($title_model!=null) )
                     $title_name = $title_model->title_name;
                       
                   if($title_name==$programs_leaders)
                     {
            ?>
        <div class="col-lg-6">
               <?php
               
               ?>
            <?= $form->field($modelProgram, 'id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcProgram::find()->all(),'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Program')); ?>
        </div>
    
        <?php
             
                     }
             
              }
        ?>
        
        <div class="col-lg-6">
               <?php
               
               ?>
            <?= $form->field($modelDepartmentPerson, 'id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcDepartmentInSchool::find()->all(),'id','department_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select department  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Working department')); ?>
        </div>
    
        <div class="col-lg-6">
            <?= $form->field($modelEmployeeInfo, 'job_status')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcJobStatus::find()->all(),'id','status_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select job status  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Job Status')) ?>
        </div>
        <div class="col-lg-6">
            <?php
            echo $form->field($modelEmployeeInfo, 'hire_date')->widget(DatePicker::classname(), [
                'options'=>['placeholder'=>'' ],
                'language'=>'fr',
                'pluginOptions'=>[
                     'autoclose'=>true,
                    'format'=>'yyyy-mm-dd',
                        ],
                
                ] );
               
            ?>      
        </div>
    
        <div class="col-lg-6">
            <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

  <!-- Champs personalisable ICI  -->    
       
        <?php
$criteria = ['field_related_to'=>'emp'];
                    
  if(!isset($_GET['id'])){  // Si on est en mode creation des champs personalisables 
                    
        $mCustomField = CustomField::find()->where($criteria)->all();
        $i=0;
        $j=0;
        $countCustomField =  CustomField::find()->where($criteria)->count();           
    foreach($mCustomField as $mc){
                      if($j==0)
                       echo '<div class="row">';
                          $j++; 
                          switch ($mc->field_type){
                              case "txt":{
                          ?>
                          
                <div class="col-lg-6">
                     <div class="form-group">
                        <label class="control-label" for="<?php echo $mc->field_name; ?>"><?php echo $mc->field_label; ?></label>
                        <input id="<?php echo $mc->field_name; ?>" class="form-control" name="<?php echo $mc->field_name; ?>" maxlength="45" type="text">
                    <div class="help-block"></div>
                     </div>   
                </div>
                    <?php 
                              }
                              break; 
                     case "date":{
                         
                    ?>
                    <div class="col-lg-6">
                     <div class="form-group">
                    <?php 
                    echo '<label class="control-label" for="'.$mc->field_name.'">'.$mc->field_label.'</label>';
                    echo DatePicker::widget([
                        'name' =>$mc->field_name, 

                        'options' => [],
                        'pluginOptions' => [
                            'format' => 'dd-M-yyyy',
                            'todayHighlight' => true
                        ]
                    ]);
                    ?>
                     <div class="help-block"></div>
                     </div>
                    </div>
                    <?php
                     }
                     break;
                  case "combo":{
                      ?>
                       <div class="col-lg-6">
                     <div class="form-group">
                        <label class="control-label" for="<?php echo $mc->field_name; ?>"><?php echo $mc->field_label; ?></label>
                      
                     <?php
                        echo '<select id="'.$mc->field_name.'" name="'.$mc->field_name.'" class="form-control">';
                        $field_value = explode(",", $mc->field_option);
                        foreach($field_value as $fv){
                           echo '<option value="'.$fv.'">'.$fv.'</option>';
                        }
                        echo '</select>';
                    ?>
                        <div class="help-block"></div>
                     </div>
                       </div>
                 <?php 

                  }
                    break; 
                              }
                    if($countCustomField==1){
                       echo '</div>'; 
                    }elseif($j==2){
                              echo '</div>';
                              $j=0;
                          }
                          
                        
                    }
                    
                 
                    
        }else{ // Mise à jour des champs personalisables 
            
            $mCustomField = CustomField::find()->where($criteria)->all();  
            $i = 0;
            $j = 0;
            $id_student = $_GET['id'];
            $countCustomField =  CustomField::find()->where($criteria)->count();   
             
            foreach($mCustomField as $mc){
                      if($j==0)
                       echo '<div class="row">';
                          $j++; 
                          switch ($mc->field_type){
                              case "txt":{
                          ?>
                          
                <div class="col-lg-6">
                     <div class="form-group">
                        <label class="control-label" for="<?php echo $mc->field_name; ?>"><?php echo $mc->field_label; ?></label>
                        <input value="<?php echo $modelCustomFieldData->getCustomFieldValue($id_student, $mc->id); ?>" id="<?php echo $mc->field_name; ?>" class="form-control" name="<?php echo $mc->field_name; ?>" maxlength="45" type="text">
                    <div class="help-block"></div>
                     </div>   
                </div>
                    <?php 
                              }
                              break; 
                     case "date":{
                         
                    ?>
                    <div class="col-lg-6">
                     <div class="form-group">
                    <?php 
                    echo '<label class="control-label" for="'.$mc->field_name.'">'.$mc->field_label.'</label>';
                    echo DatePicker::widget([
                        'name' =>$mc->field_name, 
                        'value'=>$modelCustomFieldData->getCustomFieldValue($id_student, $mc->id),
                        'options' => [],
                        'pluginOptions' => [
                            'format'=>'yyyy-mm-dd',
                            'todayHighlight' => true
                        ]
                    ]);
                    ?>
                     <div class="help-block"></div>
                     </div>
                    </div>
                    <?php
                     }
                     break;
                  case "combo":{
                      ?>
                       <div class="col-lg-6">
                     <div class="form-group">
                        <label class="control-label" for="<?php echo $mc->field_name; ?>"><?php echo $mc->field_label; ?></label>
                      
                     <?php
                        echo '<select id="'.$mc->field_name.'" name="'.$mc->field_name.'" class="form-control">';
                        $field_value = explode(",", $mc->field_option);
                        echo '<option value="'.$modelCustomFieldData->getCustomFieldValue($id_student, $mc->id).'" selected="selected">'.$modelCustomFieldData->getCustomFieldValue($id_student, $mc->id).'</option>';
                        foreach($field_value as $fv){
                           echo '<option value="'.$fv.'">'.$fv.'</option>';
                        }
                        echo '</select>';
                    ?>
                        <div class="help-block"></div>
                     </div>
                       </div>
                 <?php 

                  }
                    break; 
                              }
                        if($countCustomField==1){
                            echo '</div>';
                        }elseif($j==2){
                              echo '</div>';
                              $j=0;
                          }
                          
                        
                    }
        }
        
// Fin creation data champs personalisable    

                    ?>
             
                    <!-- FIN des champs perso  -->
    
     <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
      </div>      
    <?php ActiveForm::end(); ?>

</div>
