<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\fi\models\Relations; 
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\ContactInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-info-form">

    <?php $form = ActiveForm::begin(
            [
                'options' => [
                    'id' => 'create-contact-info'
                ]
    ]
            ); ?>

    <?php  //$form->field($model, 'person')->textInput() ?>
    <div class="row">
        <div class="col-lg-6">
             <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            
            <?= $form->field($model, 'contact_relationship')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Relations::find()->all(),'id','relation_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select relationship  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>
          
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'profession')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
