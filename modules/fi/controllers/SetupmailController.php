<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\modules\fi\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\helpers\Html;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

use app\modules\fi\models\GeneralConfig; 


class SetupmailController extends Controller
{
    public $layout = "/inspinia"; 
    
    public function actionIndex(){
        if(Yii::$app->user->can('setupmail'))
         {
            return $this->render('index');
         }
         else
            {
                if(Yii::$app->session['currentId_academic_year']=='')
                  {   
                      return $this->redirect(['/rbac/user/login']); 
                    }
                 else
                   {  
                  //throw new ForbiddenHttpException;
                  Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                          ]);
                        $this->redirect(Yii::$app->request->referrer);
                   }

              }
         
    }
    
    
    public function actionSaveMail(){
        if(isset($_REQUEST['email']) && isset($_REQUEST['host']) && isset($_REQUEST['port']) && isset($_REQUEST['pass'])){
            $email = $_REQUEST['email']; 
            $host = $_REQUEST['host']; 
            $port = $_REQUEST['port']; 
            $pass = $_REQUEST['pass'];
            $key = infoGeneralConfig('email_secret_key');
            $is_save = false; 
            $model_portal_email = GeneralConfig::findOne(['item_name'=>'email_portal_relay']); 
            $model_portal_email->item_value = $email; 
            
            $model_host_relay = GeneralConfig::findOne(['item_name'=>'email_host_relay']); 
            $model_host_relay->item_value = $host;
            
            $model_port_relay = GeneralConfig::findOne(['item_name'=>'email_port_relay']);
            $model_port_relay->item_value = $port;
            
            $model_pass_relay = GeneralConfig::findOne(['item_name'=>'email_password_relay']);
            //$val_krip  =  kode($pass,$key); 
            $model_pass_relay->item_value = $pass;//$val_krip['encodeb64']; 
            
            if($model_portal_email->save()){
                $is_save = true;
            }
            
            if($model_host_relay->save()){
                $is_save = true;
            }
            
            if($model_port_relay->save()){
                $is_save = true;
            }
            
            if($model_pass_relay->save()){
                $is_save = true;
            }
             
            
            if($is_save){
                return 1; 
            }else{
                return 0; 
            }
        }else{
            return 0;
        }
    }
    
}