<?php

namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\Academicperiods;
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcMargeEchec;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcContactInfo;
use app\modules\fi\models\YearMigrationCheck;
use app\modules\rbac\models\form\Signup;
use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\billings\models\PayrollSettingTaxes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * AcademicperiodsController implements the CRUD actions for Academicperiods model.
 */
class AcademicperiodsController extends Controller
{
    public $layout = "/inspinia"; 
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Academicperiods models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('fi-academicperiods-index'))
         {
	            $searchModel = new SrcAcademicperiods();
	        $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
	        
	        if(isset($_GET['SrcAcademicperiods']))
	          $dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
	          
	          if (isset($_GET['pageSize'])) 
		         	 {
				        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
				          unset($_GET['pageSize']);
					   }

	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);
 
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single Academicperiods model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       if(Yii::$app->user->can('fi-academicperiods-view'))
         {

	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Academicperiods model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('fi-academicperiods-create'))
         {
            set_time_limit(0);
	        $model = new Academicperiods();
	        
	       if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
	                
	                 if($model->is_year)
	                  $model->setAttribute('is_year',1);
	                 else
	                   $model->setAttribute('is_year',0);
	                 
	                  if( (isset($_GET['from']))&&($_GET['from']=='gol') )
	                      $model->setAttribute('is_year',1);
	                   
	                 if($model->previous_academic_year=='')
	                   $model->setAttribute('previous_academic_year',0);
	                  
	                $model->setAttribute('date_created',date('Y-m-d'));
					//$model->setAttribute('create_by',user);
			
                   if($model->date_end >= $model->date_start)
     	              {      
	               if($model->save()) 
                          {  
                                if($model->is_year==1) //to b sur that is a new academic year
                                 {  
                                    $model->setAttribute('year',$model->id);
                                    $model->save(); 

                                  if($model->previous_academic_year!=0)
                                    {  
                                        //kreye yon nouvo liy nan year_migration_check ak previous acad la  
                                        $command = Yii::$app->db->createCommand();
                                                  $command->insert('year_migration_check', [
                                                    'postulant'=>0, 
                                                    'course'=>0,
                                                      'marge_echec'=>0,
                                                      'fees'=>0,
                                                      'taxes'=>0,
                                                      'payroll_setting'=>0,
                                                      'pending_balance'=>0,
                                                      'academic_year'=>$model->previous_academic_year,
                                                    'create_by'=>currentUser(),
                                                    'date_created'=>date('Y-m-d'),

                                                ])->execute();                          
                                       }

                                  }

                               Yii::$app->session['currentId_academic_year'] = $model->id;
                               //$dbTrans->commit();  	
                                return $this->redirect(['index','wh'=>'ap']);
                          }
                        else
                           { //   $dbTrans->rollback();
                           }
                                   
                         }        
	               else  //$model->payment_date < $model->payroll_date
                                  {  //$this->message_PaymentDate=true;

                                         Yii::$app->getSession()->setFlash('warning', [
                                                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                    'duration' => 36000,
                                                                                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                    'message' => Html::encode( Yii::t('app','-End date- cannot be earlier than -start date-.' ) ),
                                                                                    'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                ]);	
                                  }
                                         
	            }
	            
	            
	        } 
	        
	        
	            return $this->render('create', [
	                'model' => $model,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing Academicperiods model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('fi-academicperiods-update'))
         {

	        $model = $this->findModel($id);
	
	        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
	                if($model->is_year)
	                  $model->setAttribute('is_year',1);
	                 else
	                   $model->setAttribute('is_year',0);
	                  
	                if($model->previous_academic_year=='')
	                   $model->setAttribute('previous_academic_year',0);
	                  
	                $model->setAttribute('date_updated',date('Y-m-d'));
					//$model->setAttribute('update_by',user);
							
	             if($model->date_end >= $model->date_start)
     	              {
                        if ($model->save()) 
			          {  
			               if($model->is_year==1) //to b sur that is a new academic year
							     {  
							    	$model->setAttribute('year',$model->id);
							        $model->save(); 
							      }
							 
							 //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'ap' ]);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
                                   
                       }        
	               else  //$model->payment_date < $model->payroll_date
                                  {  //$this->message_PaymentDate=true;

                                         Yii::$app->getSession()->setFlash('warning', [
                                                                                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                    'duration' => 36000,
                                                                                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                    'message' => Html::encode( Yii::t('app','-End date- cannot be earlier than -start date-.' ) ),
                                                                                    'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                    'positonY' => 'top',   //   top,//   bottom,//
                                                                                    'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                ]);	
                                  }
	               
	            }
	            
	            
	        } 
	        
	            return $this->render('update', [
	                'model' => $model,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Deletes an existing Academicperiods model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('fi-academicperiods-delete'))
         {
           try{
           	
		        $this->findModel($id)->delete();
		
		        return $this->redirect(['index','wh'=>'ap']);
		        
		      } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Academicperiods model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Academicperiods the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcAcademicperiods::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
