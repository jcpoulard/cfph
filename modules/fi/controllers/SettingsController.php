<?php

namespace app\modules\fi\controllers;
use Yii;
use yii\helpers\Html;
use app\modules\fi\models\SrcAcademicperiods; 
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcSubjects;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcCustomField;
use app\modules\fi\models\SrcContactInfo;
use app\modules\rbac\models\User; //pou tou kreye user depi ou ajoute/modifye moun
use app\modules\rbac\models\form\Signup;

use app\modules\fi\models\Academicperiods; 
use app\modules\fi\models\Shifts;
use app\modules\fi\models\YearMigrationCheck;
use app\modules\fi\models\SrcMargeEchec;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcTaxes;
use app\modules\billings\models\SrcPendingBalance;
use app\modules\billings\models\SrcPayrollSettings;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\PayrollSettingTaxes;
use app\modules\inscription\models\Postulant;
use app\modules\inscription\models\SrcPostulant;
use app\modules\inscription\models\PostulantLiablePerson;


use yii\web\ForbiddenHttpException;


class SettingsController extends \yii\web\Controller
{
    public function actionCreate()
    {
        if(Yii::$app->user->can('fi-settings-create'))
         {

	        $this->layout = "/inspinia";
	        $modelAcad = new Academicperiods(); 
	        $modelShift = new Shifts();
	        return $this->render('create',
	                ['modelAcad'=>$modelAcad, 
	                 'modelShift'=>$modelShift,   
	                 ]
	                );

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    public function actionIndex()
    {
       if(Yii::$app->user->can('fi-settings-index'))
         {
         	if (isset($_GET['pageSize'])) 
         	 {
		        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
		          unset($_GET['pageSize']);
			   }

		        $srcModelAcad = new SrcAcademicperiods();
		        $dataProviderAcad = $srcModelAcad->search(Yii::$app->request->queryParams);
		        $srcModelShifts = new SrcShifts(); 
		        $dataProviderShifts = $srcModelShifts->search(Yii::$app->request->queryParams);
		        $srcModelProgram = new SrcProgram(); 
		        $dataProviderProgram = $srcModelProgram->search(Yii::$app->request->queryParams);
		        $srcModelSubjects = new SrcSubjects(); 
		        $dataProviderSubjects = $srcModelSubjects->search(Yii::$app->request->queryParams);
		        $srcModelRooms = new SrcRooms(); 
		        $dataProviderRooms = $srcModelRooms->search(Yii::$app->request->queryParams);
		        $srcModelModule = new SrcModule(); 
		        $dataProviderModule = $srcModelModule->search(Yii::$app->request->queryParams);
		        
		        $this->layout = "/inspinia";
		        return $this->render('index',
		                [
		            'srcModelAcad' => $srcModelAcad,
		            'dataProviderAcad' => $dataProviderAcad,
		            'dataProviderShifts'=>$dataProviderShifts,
		            'srcModelShifts'=>$srcModelShifts,
		            'dataProviderProgram'=>$dataProviderProgram,
		            'srcModelProgram'=>$srcModelProgram,
		            'dataProviderSubjects'=>$dataProviderSubjects,
		            'srcModelSubjects'=>$srcModelSubjects,
		            'dataProviderRooms'=>$dataProviderRooms,
		            'srcModelRooms'=>$srcModelRooms,
		            'dataProviderModule'=>$dataProviderModule,
		            'srcModelModule'=>$srcModelModule,
		                    
		        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    public function actionList()
    {
        if(Yii::$app->user->can('fi-settings-list'))
         {

             return $this->render('list');
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    public function actionUpdate()
    {
        if(Yii::$app->user->can('fi-settings-update'))
         {

            return $this->render('update');
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    public function actionView()
    {
        if(Yii::$app->user->can('fi-settings-view'))
         {

             return $this->render('view');
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    
    
    
    
public function actionYearmigrationcheck()
{
    $this->layout = "/inspinia";
    $course_array = array(); 
	 $acad=Yii::$app->session['currentId_academic_year']; 
  
  $SrcAcademicPeriods = new SrcAcademicperiods;
         $previous_year= $SrcAcademicPeriods->getPreviousAcademicYear($acad);
     $model=new YearMigrationCheck;  
     
     
     //load model ki konsene ane akademik sa
       //$model_YearMigrationCheck = YearMigrationCheck::model()->findByAttributes(array('academic_year'=>$previous_year));
        
        
        if(isset($_POST['create']))
	     {
	     	 set_time_limit(0);
                 
                   // $previous_year= $model->previous_academic_year;
                                                                                
                                                $model_YearMigrationCheck = YearMigrationCheck::find(['academic_year'=>$previous_year])->all();
	     	               	                if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                     foreach ($model_YearMigrationCheck as $migrationCheck)
                                                         {      $yearMigrationCheck =$migrationCheck; 
                                                         }
                                                     }
                                                     
                                          /* */             //  courses
                                                  if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->course==0)
                                                         {
                                                                                $dbTrans = Yii::$app->db->beginTransaction();
                                                                                
                                                    $command= Yii::$app->db->createCommand('SELECT module, teacher, room, shift, weight, passing_grade FROM courses WHERE old_new=1 AND academic_year='.$previous_year ); 

                                                      $data = $command->queryAll();
                                                      
							
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
								      
								  if(($data!=null))
								  {
                                                                       
								    foreach($data as $d)
								     {
                                                                        
                                                                              $model1=new SrcCourses();
									      $model1->setAttribute('module',$d['module']);
									      $model1->setAttribute('teacher',$d['teacher']);
									      $model1->setAttribute('room',$d['room']);
									      $model1->setAttribute('weight',$d['weight']);
									      $model1->setAttribute('shift',$d['shift']);
									      $model1->setAttribute('passing_grade',$d['passing_grade']);
									      $model1->setAttribute('old_new',1);
									      $model1->setAttribute('academic_year',$acad);
                                                                              
									      $model1->setAttribute('date_created',date('Y-m-d'));
                                                                              $model1->setAttribute('create_by','SIGES');
									      
									      
									     if($model1->save())
									     {
									     	$one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                                    }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                          
                                                                            //echo '<br/><br/><br/><br/><br/><br/><br/>'.$kontwole.'-|'.$pass.'->'.$d['id'].'<br/>';
                                                                            $kontwole++;
									$kontwole_tot++;      
								     
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                        
                                                                     }
								     
								  }
								  
								     if( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET course=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('course'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')), 'academic_year=:acad', array(':acad'=>$acad) )->execute();
                                                                                  $dbTrans ->commit();
                                                                          
								       	}
								       else
								         $dbTrans ->rollback(); 
                                                                       
                                                                       
                                                                              
                                                                         
                                                         }
                                                         
                                                     }
                                                                       
				          //marge echec
                                                     
                                                  if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->marge_echec==0)
                                                         {
                                                            $dbTrans1 = Yii::$app->db->beginTransaction();

                                                             $command1= Yii::$app->db->createCommand('SELECT program, quantite_module, borne_sup FROM marge_echec WHERE academic_year='.$previous_year ); 

                                                               $data1 = $command1->queryAll();
                                                      
							
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
								      
								  if(($data1!=null))
								  {
                                                                       
								    foreach($data1 as $d)
								     {
                                                                        
                                                                              $model1=new SrcMargeEchec();
									      $model1->setAttribute('program',$d['program']);
									      $model1->setAttribute('quantite_module',$d['quantite_module']);
									      $model1->setAttribute('borne_sup',$d['borne_sup']);
									      $model1->setAttribute('academic_year',$acad);
                                                                              
									     if($model1->save())
									     {
									     	$one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                                 
									     }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                          
                                                                            //echo '<br/><br/><br/><br/><br/><br/><br/>'.$kontwole.'-|'.$pass.'->'.$d['id'].'<br/>';
                                                                            $kontwole++;
									$kontwole_tot++;      
								     
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
								     
								  }
								  
								     if( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
								       	  $dbTrans1 ->commit();
                                                                          
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET marge_echec=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('marge_echec'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                           //                                                                                        ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  
								       	   
								       	}
								       else
								         $dbTrans1 ->rollback(); 
                                                         }
                                                         
                                                     }
	
					 //frais
                                              if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->fees==0)
                                                         {
                                                            $dbTrans2 = Yii::$app->db->beginTransaction();

                                                             $command2= Yii::$app->db->createCommand('SELECT program, level, fee,amount,devise,date_limit_payment,description FROM fees WHERE academic_period='.$previous_year ); 

                                                               $data2 = $command2->queryAll();
                                                      
							
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
								      
								  if(($data2!=null))
								  {
                                                                       
								    foreach($data2 as $d)
								     {
                                                                        
                                                                              $model1=new SrcFees();
									      $model1->setAttribute('program',$d['program']);
                                                                              $model1->setAttribute('academic_period',$acad);
									      $model1->setAttribute('level',$d['level']);
									      $model1->setAttribute('fee',$d['fee']);
                                                                              $model1->setAttribute('amount',$d['amount']);
									      $model1->setAttribute('devise',$d['devise']);
                                                                              $model1->setAttribute('checked',0);
									      $model1->setAttribute('date_limit_payment',date("Y-m-d", strtotime($d['date_limit_payment']." + 1 year")));
									      $model1->setAttribute('description',$d['description']);
                                                                              $model1->setAttribute('date_create',date('Y-m-d'));
                                                                              $model1->setAttribute('create_by','SYSTEM');
									      
									      
                                                                              
									     if($model1->save())
									     {
									     	$one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                                 
									     }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                          
                                                                            //echo '<br/><br/><br/><br/><br/><br/><br/>'.$kontwole.'-|'.$pass.'->'.$d['id'].'<br/>';
                                                                            $kontwole++;
									$kontwole_tot++;      
								     
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
								     
								  }
								  
								     if( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
								       	  $dbTrans2 ->commit();
                                           
                                                                           $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET fees=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('fees'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                           //                                                                                        ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  
								       	   
								       	}
								       else
								         $dbTrans2 ->rollback(); 		        
                                                         }
                                                         
                                                     }
                                                                       
                                       //taxes
                                                     if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->taxes==0)
                                                         {
                                                            $dbTrans3 = Yii::$app->db->beginTransaction();

                                                             $command3= Yii::$app->db->createCommand('SELECT taxe_description, employeur_employe, taxe_value,particulier FROM taxes WHERE academic_year='.$previous_year ); 

                                                               $data3 = $command3->queryAll();
                                                      
							
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
								      
								  if(($data3!=null))
								  {
                                                                       
								    foreach($data3 as $d)
								     {
                                                                        
                                                                              $model1=new SrcTaxes();
									      $model1->setAttribute('taxe_description',$d['taxe_description']);
									      $model1->setAttribute('employeur_employe',$d['employeur_employe']);
									      $model1->setAttribute('taxe_value',$d['taxe_value']);
									      $model1->setAttribute('particulier',$d['particulier']);
									      $model1->setAttribute('academic_year',$acad);
                                                                              
									     if($model1->save())
									     {
									     	$one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                                 
									     }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                          
                                                                            //echo '<br/><br/><br/><br/><br/><br/><br/>'.$kontwole.'-|'.$pass.'->'.$d['id'].'<br/>';
                                                                            $kontwole++;
									$kontwole_tot++;      
								     
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
								     
								  }
								  
								     if( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
								       	  $dbTrans3 ->commit();
                                            
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET taxes=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('taxes'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                           //                                                                                        ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  
								       	   
								       	}
								       else
								         $dbTrans3 ->rollback(); 
                                                                       
                                                         }
                                                         
                                                     }
                                
                              /*   //pending balance  
                                                   if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->pending_balance==0)
                                                         {                      
                               
                                                            $dbTrans4 = Yii::$app->db->beginTransaction();

                                                             $command4= Yii::$app->db->createCommand('SELECT student, balance, devise FROM balance'); 

                                                               $data4 = $command4->queryAll();
                                                      
							
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
                                                                      $program_array= array();
                                                                      $level_array= array();
								      
								  if(($data4!=null))
								  {
                                                                       
								    foreach($data4 as $d)
								     {
                                                                              $model1=new SrcPendingBalance();
									      $model1->setAttribute('student',$d['student']);
									      $model1->setAttribute('balance',$d['balance']);
									      $model1->setAttribute('devise',$d['devise']);
									      $model1->setAttribute('is_paid',0);
									      $model1->setAttribute('academic_year',$previous_year);
                                                                              $model1->setAttribute('date_created',date('Y-m-d'));
                                                                           
									     if($model1->save())
									     {
									     	//jwenn nan ki klas elev la ye, tou kreye fee PENDING BALANCE pou klas la
									     	$lmodelPers= SrcStudentLevel::findOne($d['student']);
                                                                               if(isset($lmodelPers->level) )
                                                                               {
                                                                                $level= $lmodelPers->level;
                                                                                
                                                                                $modelOtherIinf = SrcStudentOtherInfo::find(['student'=>$d['student'] ])->all();
                                                                                  foreach($modelOtherIinf as $otherInf) 
                                                                                  { $program = $otherInf->applyForProgram->id;
                                                                                        break;
                                                                                  }
											
									     	if(  (! in_array($level,$level_array))&&(! in_array($program,$program_array) )  )
									     	 {        $label_id=0;
                                                                                
									     	 	 $model_feesLabel = SrcFeesLabel::find(['fee_label'=>'Pending balance'])->all();
                                                                                         foreach($model_feesLabel as $fee_label)
                                                                                           {  $label_id=$fee_label->id;
                                                                                               break;
                                                                                           }
                                                                                          
									     	 	//kreye fee pending-balance pou klas la
									     	 	  $model11=new SrcFees;
											      $model11->setAttribute('program',$program);
											      $model11->setAttribute('level',$level);
                                                                                              $model11->setAttribute('academic_period',$acad);
											      $model11->setAttribute('fee',$label_id);//pou labael "pending balance" lan
											      $model11->setAttribute('amount',0);
                                                                                              $model11->setAttribute('devise',$d['devise']);
											      $model11->setAttribute('date_limit_payment',date("Y-m-d", strtotime(date('Y-m-d')." - 1 year")));
											      $model11->setAttribute('checked',1);//dat limit pase epi checked=1 pou l pa double montan nan tab balance lan
											      
											      $model11->setAttribute('date_create',date('Y-m-d'));
							                                     $model11->setAttribute('create_by','SYSTEM');
											      
											     if($model11->save())
                                                                                               {	  $level_array[]=$level;
                                                                                                        $program_array[]=$program;
                                                                                               
                                                                                               }
                                                                                             else
                                                                                                 $pass=0;
									     	 	
									     	 }
                                                                                 
                                                                               }
                                                                                 $one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                             
                                                                                
                                                                                 
									     }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                         
                                                                            //echo '<br/><br/><br/><br/><br/><br/><br/>'.$kontwole.'-|'.$pass.'->'.$d['id'].'<br/>';
                                                                            $kontwole++;
									$kontwole_tot++;      
								     
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
								     
								  }
								  
								     if( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
								       	  $dbTrans4 ->commit();
                                           
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET pending_balance=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('pending_balance'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                           //                                                                                        ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  
								       	   
								       	}
								       else
								         $dbTrans4 ->rollback();            
                                                           }
                                 
                                                     }
                                                                                                    
                                 */                                                                  
                              //payroll setting
                                                   if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->payroll_setting==0)
                                                         {
                                                            $dbTrans5 = Yii::$app->db->beginTransaction();

                                                             $command5= Yii::$app->db->createCommand('SELECT id, person_id, amount, devise, an_hour,number_of_hour,frais,assurance_value,as_ FROM payroll_settings where old_new=1 and academic_year='.$previous_year); 

                                                               $data5 = $command5->queryAll();
                                                           
								      $pass=1;
								      $one_save_record=0;
                                                                      $kontwole_tot=0;
                                                                      $kontwole_pass=0;
                                                                      $kontwole=0;
                                                                      
								  if(($data5!=null))
								  {
                                                                       
								    foreach($data5 as $d)
								     {
                                                                        
                                                                              $model1=new SrcPayrollSettings();
									      $model1->setAttribute('person_id',$d['person_id']);
									      $model1->setAttribute('amount',$d['amount']);
									      $model1->setAttribute('devise',$d['devise']);
									      $model1->setAttribute('an_hour',$d['an_hour']);
									      $model1->setAttribute('number_of_hour',$d['number_of_hour']);
									      $model1->setAttribute('frais',$d['frais']);
									      $model1->setAttribute('assurance_value',$d['assurance_value']);
									      $model1->setAttribute('as_',$d['as_']);
									      $model1->setAttribute('academic_year',$acad);
                                                                              $model1->setAttribute('old_new',1);
                                                                              $model1->setAttribute('date_created',date('Y-m-d'));
                                                                              $model1->setAttribute('created_by','SYSTEM');
                                                                              
									     if($model1->save())
									     {  
                                                                                $payrollSettingTax = PayrollSettingTaxes::find()->where('id_payroll_set='.$d['id'])->all();
                                         
                                                                                  foreach($payrollSettingTax as $payrollTax) 
                                                                                    {  
                                                                                       //cheche non taxe la epi chache id l pou ane sa
                                                                                      $id_taxe = '';
                                                                                      $modelTaxe =  SrcTaxes::findOne($payrollTax->id_taxe);
                                                                                      $newModelTaxe =  SrcTaxes::find()->where('taxe_description="'.$modelTaxe->taxe_description.'" and academic_year='.$acad)->all();
                                                                                       if($newModelTaxe!=null)
                                                                                         {  foreach($newModelTaxe as $newTaxe)
                                                                                              { $id_taxe = $newTaxe->id;
                                                                                               }
                                                                                           }
                                                                                           
                                                                                      $modelPayrollSettingTaxe = new PayrollSettingTaxes;
																      	  
                                                                                       $modelPayrollSettingTaxe->setAttribute('id_payroll_set',$model1->id);
                                                                                       $modelPayrollSettingTaxe->setAttribute('id_taxe',$id_taxe);

                                                                                        $modelPayrollSettingTaxe->save();

                                                                                       unSet($modelPayrollSettingTaxe);
                                                                                    }
										 
                                                                                $one_save_record=1;
                                                                                $kontwole_pass++; 
                                                                                 
									     }
									   else{
									     $pass=0;
                                                                             
                                                                            // $kontwole++;
                                                                             
                                                                           }
                                                                          
                                                                            $kontwole++;
									$kontwole_tot++;      
                                                                       
								     }
                                                                     if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
								     
								  }
								  
								     if( ($one_save_record==1) ) //( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
								       	  $dbTrans5 ->commit();
                                                                          
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET payroll_setting=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('payroll_setting'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                           //                                                                                        ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  
								       	   
								       	}
								       else
								         $dbTrans5 ->rollback(); 
                                 
                                                           }
                                 
                                                       }
                                
                                                  
                                   
                                       
                                       //   persons_has_titles
                                                       if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->titles==0)
                                                         {
 /*                                                    INSERT INTO persons_has_titles (`persons_id`,`titles_id`,`academic_year`)
SELECT `persons_id`, `titles_id`, 4 FROM persons_has_titles
WHERE `academic_year`=3*/ 
                                         $command_persons_has_titles = Yii::$app->db->createCommand('INSERT INTO persons_has_titles (`persons_id`,`titles_id`,`academic_year`) SELECT `persons_id`, `titles_id`, '.$acad.' FROM persons_has_titles WHERE `academic_year`='.$previous_year)->execute();
                                         
                                                           }
                                                     
                                                     }
//                                    //postulant
//                                                      $command6= Yii::$app->db->createCommand('SELECT id, student FROM student_other_info soi WHERE apply_shift IS NULL;')->queryAll();
//                                                      foreach($command6 as $d)
//                                                        {
//                                                              $modelStudentOtherInfo = SrcStudentOtherInfo::findOne($d['id']);
//                                                                $id_postulant = $modelStudentOtherInfo->student0->comment;
//                                                              
//                                                                $modelPostulant = SrcPostulant::findOne($id_postulant);
//                                                                
//                                                                $modelStudentOtherInfo->apply_shift = $modelPostulant->shift;
//                                                                $modelStudentOtherInfo->save();
//                                                        }
                                                     
                                                                  
                                                     if(isset($model_YearMigrationCheck)&&($model_YearMigrationCheck!=null))
                                                     {  
                                                         if($yearMigrationCheck->postulant==0)
                                                         {
                                                             $date_debut_inscription= infoCmsConfig('date_debut_inscription');
                                      
                                                                       $automatic_code = infoGeneralConfig('automatic_code');
	     	               	
                                                                $dbTrans6 = Yii::$app->db->beginTransaction();

                                                                 $command6= Yii::$app->db->createCommand('SELECT id, first_name, last_name, gender,birthday,cities,phone,adresse,email,apply_for_program,shift,last_class,previous_school,school_date_entry,last_average FROM postulant where status=1 and is_validation_expire=3 and date_created>=\''.$date_debut_inscription.'\' and academic_year='.$acad); 

                                                                   $data6 = $command6->queryAll();

                                                                   //si gen postulan pou ane ankour pa tcheke nan ane anvan an 
                                                              if(($data6==null))
                                                               {       
                                                                 $command6= Yii::$app->db->createCommand('SELECT id, first_name, last_name, gender,birthday,cities,phone,adresse,email,apply_for_program,shift,last_class,previous_school,school_date_entry,last_average FROM postulant where status=1 and is_validation_expire=3 and date_created>=\''.$date_debut_inscription.'\' and academic_year='.$previous_year); 

                                                                   $data6 = $command6->queryAll();
                                                                }
                                                               $pass=1;
                                                               $one_save_record=0;
                                                               $kontwole_tot=0;
                                                               $kontwole_pass=0;
                                                               $kontwole=0;
                                                                 $status=0;

                                                           if(($data6!=null))
                                                           {
                                                            
                                                             foreach($data6 as $d)
                                                              {
                                                                 $_program = '';
                                                                  $_shift = '';

                                                                  $disabled ='0';
//print_r('<br/><br/><br/>*****************************************************************');
                                                                  $model1 = new SrcPersons();
                                                             //$modelCustomFieldData = new CustomFieldData();
                                                             $modelStudentLevel= new SrcStudentLevel;

                                                             $model1->setAttribute('first_name',$d['first_name']);
                                                             $model1->setAttribute('last_name',$d['last_name']);
                                                             $model1->setAttribute('gender',$d['gender']);
                                                             $model1->setAttribute('birthday',$d['birthday']);
                                                            // $model1->setAttribute('cities',$d['cities']);
                                                             $model1->setAttribute('adresse',$d['adresse']);
                                                             $model1->setAttribute('phone',$d['phone']);
                                                            // $model1->setAttribute('email',$d['email']);
                                                             $model1->setAttribute('comment',$d['id']);

                                                             // $model1->setAttribute('blood_group',NULL);
                                                             // $model1->setAttribute('nif_cin',NULL);
                                                             // $model1->setAttribute('citizenship',NULL);

                                                             $model1->setAttribute('active',2);

                                                             $model1->setAttribute('is_student',1);
                                                             $model1->setAttribute('date_created',date('Y-m-d'));
                                                             $model1->setAttribute('create_by','SYSTEM');

                                                             if ($model1->save())
                                                                  {
                                                                $username= '';
                                                         
                                                                 //code automatic
                                                                  if($automatic_code ==1)
                                                                    {
                                                                       $cf='';
                                                                       $cl='';

                                                                    //first_name
                                                                         $explode_firstname=explode(" ",substr( trim($model1->first_name),0));

                                                                      if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
                                                                          {
                                                                         $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

                                                                          }
                                                                         else
                                                                          {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

                                                                          }

                                                                          //last_name
                                                                         $explode_lastname=explode(" ",substr( trim($model1->last_name),0));

                                                                       if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                                                          {
                                                                         $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );
                                                                    $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model1->id;
                                                                             
                                                                          }
                                                                         else
                                                                          {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );
                                                                        $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model1->id;
 
                                                                          }

                                                                           $code_ = $cf.$cl.$model1->id;


                                                                                   $model1->setAttribute('id_number',$code_);

                                                                                   $model1->save();

                                                                       }
                                                                    else
                                                                        {
                                                                                $explode_lastname=explode(" ",substr( trim($model1->last_name),0));

                                                                             if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
                                                                               $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model1->id;
                                                                             else
                                                                               $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model1->id;
 
                                                                    }
                                                             // Demarrer enregistrement champs personalisables
                                                                      /*   $criteria_cf= ['field_related_to' => 'stud'];

                                                                         $cfData = SrcCustomField::find()->where($criteria_cf)->all();
                                                                         foreach($cfData as $cd){
                                                                             if(isset($_POST[$cd->field_name])){
                                                                                 $customFieldValue = $_POST[$cd->field_name];
                                                                                 $modelCustomFieldData->setAttribute('field_link', $cd->id);
                                                                                 $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
                                                                                 $modelCustomFieldData->setAttribute('object_id', $model1->id);
                                                                                 $modelCustomFieldData->save();
                                                                                 unset($modelCustomFieldData);
                                                                                 $modelCustomFieldData = new CustomFieldData;
                                                                             }
                                                                         }
                                                                        
                                                                       */
                                                                         // Terminer Enregistrement champs personalisables

        
                                                                 if( ($d['apply_for_program']!='')&&($d['shift']!='') )
                                                                   {  $modelStudentOtherInfo = new SrcStudentOtherInfo();
                                                                       $modelStudentOtherInfo->student = $model1->id;
                                                                       $modelStudentOtherInfo->apply_for_program =$d['apply_for_program'];
                                                                       $modelStudentOtherInfo->previous_school =$d['previous_school'];
                                                                       $modelStudentOtherInfo->apply_shift =$d['shift'];
                                                                       $modelStudentOtherInfo->school_date_entry =$d['school_date_entry'];
                                                                      // $modelStudentOtherInfo->father_full_name =$liable->first_name.' '.$liable->first_name;
                                                                      // $modelStudentOtherInfo->mother_full_name =$liable->first_name.' '.$liable->first_name;


                                                                       $modelStudentOtherInfo->save();

                                                                     }

                                                                     
                                                                     $password = "password";

                                                                     $user = new Signup();
                                                                     $user->username = $username;
                                                                     $user->person_id = $model1->id;
                                                                     $user->is_parent = 0;
                                                                        if($model1->email!='')
                                                                        $user->email = $model1->email;

                                                                     $user->password=$password;

                                                                     $user->signup_from_person();

                                                                    // student_level
                                                                              $modelStudentLevel->level=1;
                                                                              $modelStudentLevel->student_id = $model1->id;
                                                                              $modelStudentLevel->save();

                                                                     // personne contact
                                                                         $liable = PostulantLiablePerson::find()->where(['postulant'=>$d['id']])->one();

                                                                              if($liable->occupation!=null)
                                                                                  $profession = $liable->occupation;
                                                                              else
                                                                                  $profession = '???';
                                                                              
                                                                               $modelContact = new SrcContactInfo();

                                                                               $modelContact->setAttribute('person',$model1->id);
                                                                                 $modelContact->setAttribute('contact_relationship',$liable->relation_responsable);
                                                                                 $modelContact->setAttribute('contact_name',$liable->first_name.' '.$liable->last_name);
                                                                                 $modelContact->setAttribute('profession',$profession);
                                                                                 $modelContact->setAttribute('phone',$liable->phone);
                                                                                 $modelContact->setAttribute('address',$liable->address);
                                                                                 $modelContact->setAttribute('email',$liable->email_responsable);
                                                                                 $modelContact->setAttribute('date_created',date('Y-m-d') );
                                                                                 $modelContact->setAttribute('create_by','SYSTEM');

                                                                                 $modelContact->save();

                                                                        //update postulant, mete expire_validation=0
                                                                                 $modelPostulant = SrcPostulant::findOne($d['id']); 

                                                                                 $modelPostulant->is_validation_expire =0;
                                                                                 $modelPostulant->update_by ='SYSTEM';
                                                                                 $modelPostulant->save();
                                                                                 //$command_modelPostulant = Yii::$app->db->createCommand('UPDATE postulant SET is_validation_expire=0, update_by=\'SYSTEM\' WHERE id='.$d['id'])->execute();
                                                                          //$command_modelPostulant->update('postulant', array('is_validation_expire'=>0,'update_by'=>'SYSTEM'
                                                                             //                                                                                      ), 'id=:id', array(':id'=>$d['id']) )->execute();
								       	  

                                                                      $one_save_record=1;
                                                                      $kontwole_pass++; 

                                                                  }
                                                     $kontwole++;
							$kontwole_tot++;
                                                              }

                                                                        
                                                                 if($kontwole_tot!=0){
                                                                         $percent_pass = $kontwole_pass/$kontwole_tot; 
                                                                     }
                                                                
                                                            }
                                                            
                                                            if(($one_save_record==1) )  //( ($percent_pass > 0.5)&&($one_save_record==1) ) //$pass==1  //
								       {
                                                                          $command_yearMigrationCheck = Yii::$app->db->createCommand('UPDATE year_migration_check SET postulant=1, update_by=\''.currentUser().'\', date_updated=\''.date('Y-m-d').'\' WHERE academic_year='.$previous_year)->execute();
                                                                          //$command_yearMigrationCheck->update('year_migration_check', array('postulant'=>1,'update_by'=>currentUser(),'date_updated'=>date('Y-m-d')
                                                                            //                                                                                       ), 'academic_year=:acad', array(':acad'=>$previous_year) );
								       	  $dbTrans6 ->commit();
								       	   
								       	}
								       else
								         $dbTrans6 ->rollback(); 
                                                            
                                                            
                                                            
                                                            
                                                         }
                                                         
                                                     }
	     	               	                                       
                                                                       
                                    //student : li fet depi nan desizyon an                                  
                                                                       
                                                                       
                      //  ____________________     ______________________________	//			
				
				
	         return $this->render('yearmigrationcheck',
		                [
		            'model' => $model,
		            'course_array' => $course_array,
		                    
		        ]);	
                       
	     }else 
               {  
                 return $this->render('yearmigrationcheck',
                      [
		            'model' => $model,
		            'course_array' => $course_array,
		                    
		        ]);  
               }
	     
  } 
    
    
    
    
    
    
    
    
    
    

    
}
