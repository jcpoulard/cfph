<?php

namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\Rooms;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcShifts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * RoomsController implements the CRUD actions for Rooms model.
 */
class RoomsController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rooms models.
     * @return mixed
     */
    public function actionIndex()
    {
       if(Yii::$app->user->can('fi-rooms-index'))
         {

	        $searchModel = new SrcRooms();
	       // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	        
	        $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
	       
	       if(isset($_GET['SrcRooms']))
	          $dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
	          
	          if (isset($_GET['pageSize'])) 
		         	 {
				        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
				          unset($_GET['pageSize']);
					   }

	                 
	       
	        
	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single Rooms model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('fi-rooms-view'))
         {

	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Rooms model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('fi-rooms-create'))
         {

		        $model = new Rooms();
		
		         if ($model->load(Yii::$app->request->post()) ) 
		        {
		            // $dbTrans = Yii::app->db->beginTransaction(); 
		           
		           if(isset($_POST['create']))
		            {
		                 $model->setAttribute('date_created',date('Y-m-d'));
						//$model->setAttribute('create_by',user);
								
		               if ($model->save()) 
				          {   
				               //$dbTrans->commit();  	
				                return $this->redirect(['index','wh'=>'roo']);
				          }
				        else
				           { //   $dbTrans->rollback();
				           }
		               
		            }
		            
		            
		        } 
		        
		
		            return $this->render('create', [
		                'model' => $model,
		            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing Rooms model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('fi-rooms-update'))
         {

		        $model = $this->findModel($id);
		
		        if ($model->load(Yii::$app->request->post()) ) 
		        {
		            // $dbTrans = Yii::app->db->beginTransaction(); 
		           
		           if(isset($_POST['update']))
		            {
		                 $model->setAttribute('date_updated',date('Y-m-d'));
						//$model->setAttribute('update_by',user);
								
		               if ($model->save()) 
				          {   
				               //$dbTrans->commit();  	
				                return $this->redirect(['index','wh'=>'roo']);
				          }
				        else
				           { //   $dbTrans->rollback();
				           }
		               
		            }
		            
		            
		        } 
		
		            return $this->render('update', [
		                'model' => $model,
		            ]);
		        

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }








    /**
     * Deletes an existing Rooms model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('fi-rooms-delete'))
         {
           try{
		        $this->findModel($id)->delete();
		
		        return $this->redirect(['index','wh'=>'roo']);
		     } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Rooms model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rooms the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcRooms::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
