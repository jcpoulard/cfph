<?php
   
namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\Courses;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcStudentHasCourses; 
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\SrcPersons;

use app\modules\rbac\models\User;
use app\modules\rbac\models\Assignment; 
use app\modules\rbac\models\searchs\Assignment as AssignmentSearch; 


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * CoursesController implements the CRUD actions for Courses model.
 */
class CoursesController extends Controller
{
    
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Courses models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('fi-courses-index'))
         {
         	

	        $searchModel = new SrcCourses();
	        
	        
        //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
        if($program==null)
          {
          	if(Yii::$app->session['profil_as'] ==0)
                 $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
             elseif(Yii::$app->session['profil_as'] ==1)
          	{  
              	$userModel = User::findOne(Yii::$app->user->identity->id);
    
				    $dataProvider = $searchModel->searchCoursesForTeacher($userModel->person_id);
              }    
          	  
          	  
          	 /*  if(isset($_GET['SrcCourses']))
		         $dataProvider= $searchModel->search(Yii::$app->request->queryParams);
		      
		      if(isset($_POST['SrcCourses']['program']))
	              { $program= $_POST['SrcCourses']['program'];
	                $searchModel->program=[$program];
	                Yii::$app->session['courses_program'] = $program;
	               }
	            else
	              {   $searchModel->program=[ Yii::$app->session['courses_program'] ];
	              	}

          	    if(isset($_POST['SrcCourses']['shift_select']))
		              { $shift= $_POST['SrcCourses']['shift_select'];
		                 $searchModel->shift_select=[$shift]; 
		                   Yii::$app->session['courses_shift'] = $shift;
		              }
		            else
		              {
		              	 $searchModel->shift_select=[Yii::$app->session['courses_shift']]; 
		              	}
		              	
		             	
          	     if(  (($searchModel->program[0]!='')&&($searchModel->program[0]!=0)) && (($searchModel->shift_select[0]!='')&&($searchModel->shift_select[0]!=0))  )
		      {
		      	    $dataProvider = $searchModel->sortByProgramShift($searchModel->program[0],$searchModel->shift_select[0]);
		      	   
		      	}
		     elseif(  (($searchModel->program[0]!='')&&($searchModel->program[0]!=0)) && (($searchModel->shift_select[0]=='')||($searchModel->shift_select[0]==0))  )
		           {
		           	       $dataProvider = $searchModel->sortByProgramShift($searchModel->program[0],NULL);
		           	       
		           	 }
		          elseif(  (($searchModel->program[0]=='')||($searchModel->program[0]==0)) && (($searchModel->shift_select[0]!='')&&($searchModel->shift_select[0]!=0))  )
		              {
		              	      $dataProvider = $searchModel->sortByProgramShift(NULL,$searchModel->shift_select[0]);
		              	      
		              	 }
		              	 
		       */
          	  
          	}
         else //se yon responsab pwogram
           {
           	 if(Yii::$app->session['profil_as']==0)//se yon employee
                 {            
                              
                    $dataProvider = $searchModel->sortByProgramShift($program,NULL);
          	  
          	  if(isset($_GET['SrcCourses']))
		         $dataProvider= $searchModel->sortByProgramShift($program,NULL);
		         
		          if(isset($_POST['SrcCourses']['shift_select']))
		              { $shift= $_POST['SrcCourses']['shift_select'];
		                 $searchModel->shift_select=[$shift]; 
		                   Yii::$app->session['courses_shift'] = $shift;
		              }
		            else
		              {
		              	 $searchModel->shift_select=[Yii::$app->session['courses_shift']]; 
		              	}
		      
		        if(  (($searchModel->shift_select[0]!='')&&($searchModel->shift_select[0]!=0))  )
			      {
			      	    $dataProvider = $searchModel->sortByProgramShift($program,$searchModel->shift_select[0]);
			      	   
			      	}
			     elseif(  (($searchModel->shift_select[0]=='')||($searchModel->shift_select[0]==0))  )
			           {
			           	       $dataProvider = $searchModel->sortByProgramShift($program,NULL);
			           	       
			           	 }
                                         
                     }
                   elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                       {
                            $userModel = User::findOne(Yii::$app->user->identity->id);
    
				    $dataProvider = $searchModel->searchCoursesForTeacher($userModel->person_id);
                       }

           	 } 


	        
	      		              	 
	          
	       	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    /*
     * Retourne la liste des etudiants qui suivent un cours 
     */
    public function actionViewstudent(){
        if(Yii::$app->user->can("fi-courses-viewstudent")){
            $searchModel = new SrcCourses();
            //$model = new Courses(); 
           // if(isset($_GET['id'])){
            $course = $_GET['id']; 
            $dataProvider = $searchModel->studentsByCourse($course, Yii::$app->request->queryParams); 
            if(isset($_GET['SrcCourses']))
                $dataProvider = $searchModel->studentsByCourse($course, Yii::$app->request->queryParams);
           // }
             
            return $this->renderAjax('viewstudent',
                    [
                     'searchModel'=>$searchModel,
                     'dataProvider'=>$dataProvider, 
                    // 'model'=>$model,
                        
                    ]
                    ); 
        }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }
 
        }
        
    }
    

    /**
     * Displays a single Courses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('fi-courses-view'))
         {

	        return $this->renderAjax('view', [
	            'model' => $this->findModel($id),
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Courses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('fi-courses-create'))
         {

	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $display_course_weight = infoGeneralConfig('display_course_weight');  
            
	        $model = new Courses();
	
	         if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
	               /*   if($model->debase)
	                  $model->setAttribute('debase',1);
	                 else
	                   $model->setAttribute('debase',0);
	                   
	                   if($model->optional)
	                  $model->setAttribute('optional',1);
	                 else
	                   $model->setAttribute('optional',0); 
	                */
	               
	               if($display_course_weight==0)
	                 {
	                 	$model->setAttribute('weight',100);
	                 	}
	                 	
	                   $model->setAttribute('academic_year', $acad); 
	                   
	                   $model->setAttribute('date_created',date('Y-m-d'));
					$model->create_by=currentUser();
							
	               if ($model->save()) 
			          {   
			          	   //gad si li(user id) poko gen role teacher pou w ba li l
						          	  $already_assign =false;
						          	  $user_id = 0;
						          	  $item_name = 'teacher';
						          	  
						          	  $modelUser = new User;
						          	  $userId = $modelUser->findByPersonId($model->teacher);
						          	  
						          	  if($userId!=null)
						          	    $user_id = $userId->id;
						          	  
						          	  
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                        //return user_id
                                        $modelUser_assign = $modelUserAssign->searchByUseridItemname($user_id, $item_name);//return user_id 
						          	   
						          	   if($modelUser_assign!=null)
						          	     {
						          	     	  $already_assign = true;
						          	     	  
						          	     	}                   
						          	 
						          	 if($already_assign==false)
						          	   {  //$command = Yii::$app->db->createCommand();
									      //$command->insert('auth_assignment', array('item_name'=>'teacher','user_id'=>$user_id ))
									       //       ->execute();
						          	     
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $items = ['teacher'];
						          	       $modelAssignment->assign($items);
						          	       	        
						          	     }
                                       
			              
			               //$dbTrans->commit();  	
			                return $this->redirect(['index', ]);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        } 
	        
	
	            return $this->render('create', [
	                'model' => $model,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    
    
    public function actionCreatelist(){
        if(Yii::$app->user->can("fi-courses-create")){
            $model = new SrcCourses();
            $acad = Yii::$app->session['currentId_academic_year'];
            $display_course_weight = infoGeneralConfig('display_course_weight'); 
            if(isset($_POST['create'])){
                    $is_save = FALSE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    if(isset($_POST['shift'])){// && isset($_POST['room'])){
                        $shift = $_POST['shift'];
                       // $room = $_POST['room'];
                                             
                        for($i=1; $i<=30; $i++){
                            
                               
                  if($display_course_weight==0)   
                       $isset = ( isset($_POST["module$i"]) && isset($_POST["teacher$i"]) && isset($_POST["passing_grade$i"]) );
                  elseif($display_course_weight==1) 
                     $isset = ( isset($_POST["module$i"]) && isset($_POST["teacher$i"]) && isset($_POST["weight$i"]) && isset($_POST["passing_grade$i"]) );

                            
                            if($isset){
                              // echo "module$i";
                                $data_form_js =  json_encode($_POST);
                                $data_form_array = json_decode($data_form_js,true); 
                                //print_r($data_form_array);
                                //$module = (int)$_POST['module'.$i];
                               //$teacher = $data_form_array['teacher1'];
                               // echo $teacher;
                                $model = new Courses();
                                $model->shift = $shift;
                               // $model->room = $room;
                                $model->academic_year = $acad;
                                $model->module = $data_form_array["module$i"];
                                $model->teacher = $data_form_array["teacher$i"];
                                 
                             if($display_course_weight==0)
	                            $model->weight= 100;
	                         elseif($display_course_weight==1)
	                            $model->weight = $data_form_array["weight$i"];
	                 	
                                $model->passing_grade = $data_form_array["passing_grade$i"];
                                $model->date_created = date('Y-m-d');
                                $model->create_by=currentUser();
                               
                               if(($model->module!='')&&($model->teacher!='')&&($model->passing_grade!=''))
                                {
                                if($model->save())
                                  {    $is_save = TRUE;
                                     
                                      //gad si li(user id) poko gen role teacher pou w ba li l
						          	  $already_assign =false;
						          	  $user_id = 0;
						          	  $item_name = 'teacher';
						          	  
						          	  $modelUser = new User;
						          	  $userId = $modelUser->findByPersonId($model->teacher);
						          	  
						          	  if($userId!=null)
						          	    $user_id = $userId->id;
						          	  
						          	  
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                        //return user_id
                                        $modelUser_assign = $modelUserAssign->searchByUseridItemname($user_id, $item_name);//return user_id 
						          	   
						          	   if($modelUser_assign!=null)
						          	     {
						          	     	  $already_assign = true;
						          	     	  
						          	     	}                   
						          	 
						          	 if($already_assign==false)
						          	   {  //$command = Yii::$app->db->createCommand();
									      //$command->insert('auth_assignment', array('item_name'=>'teacher','user_id'=>$user_id ))
									       //       ->execute();
						          	     
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $items = ['teacher'];
						          	       $modelAssignment->assign($items);
						          	       	        
						          	     }
                                   
                                      }
                                     
                                   } 
                            }
                            }
                            if(!$is_save){
                                $dbTrans->rollback();
                                
                            }
                            else{
                                $dbTrans->commit();
                                return $this->redirect(['index']);
                            }
                        }
                    
                    
                }
            
            return $this->render('createlist',[
                'model'=>$model,
            ]); 
            
        }
        else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                'duration' =>120000,
                'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                'title' => Html::encode(Yii::t('app','Unthorized access') ),
                'positonY' => 'top',   //   top,//   bottom,//
                'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing Courses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('fi-courses-update'))
         {
         	$display_course_weight = infoGeneralConfig('display_course_weight'); 

	        $model = $this->findModel($id);
	
	        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
	               /*   if($model->debase)
	                  $model->setAttribute('debase',1);
	                 else
	                   $model->setAttribute('debase',0);
	                   
	                   if($model->optional)
	                  $model->setAttribute('optional',1);
	                 else
	                   $model->setAttribute('optional',0); 
	                */ 
	                 
	                if($display_course_weight==0)
	                 {
	                 	$model->setAttribute('weight',100);
	                 	}
	                 	 
	                   $model->setAttribute('date_updated',date('Y-m-d'));
					$model->create_by=currentUser();
							
	               if ($model->save()) 
			          {   
			                 //gad si li(user id) poko gen role teacher pou w ba li l
						          	  $already_assign =false;
						          	  $user_id = 0;
						          	  $item_name = 'teacher';
						          	  
						          	  $modelUser = new User;
						          	  $userId = $modelUser->findByPersonId($model->teacher);
						          	  
						          	  if($userId!=null)
						          	    $user_id = $userId->id;
						          	  
						          	  
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                        //return user_id
                                        $modelUser_assign = $modelUserAssign->searchByUseridItemname($user_id, $item_name);//return user_id 
						          	   
						          	   if($modelUser_assign!=null)
						          	     {
						          	     	  $already_assign = true;
						          	     	  
						          	     	}                   
						          	 
						          	 if($already_assign==false)
						          	   {  //$command = Yii::$app->db->createCommand();
									      //$command->insert('auth_assignment', array('item_name'=>'teacher','user_id'=>$user_id ))
									       //       ->execute();
						          	     
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $items = ['teacher'];
						          	       $modelAssignment->assign($items);
						          	       	        
						          	     }
                                       
			               
			               //$dbTrans->commit();  	
			                return $this->redirect(['index', ]);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        } 
	
	            return $this->render('update', [
	                'model' => $model,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }


  public function actionShiftinprogram($from,$program,$acad){
         
      if($from=='grades')
        { Yii::$app->session['grades_program']= $program;
           Yii::$app->session['grades_shift']= '';
         Yii::$app->session['grades_room'] = '';
        }
      elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
           Yii::$app->session['studentHasCourses_shift']= '';
           Yii::$app->session['studentHasCourses_room'] = '';
        }
      elseif($from=='idcards')
        { Yii::$app->session['idcard_program']= $program;
           Yii::$app->session['idcard_shift']= '';
           Yii::$app->session['idcard_room'] = '';
        }
      
         
        $countShifts = Courses::find()->select('shift')
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->count();
                          
        $allShifts = Courses::find()->select('shift')
                         ->joinWith(['module0','shift0'])
                         ->where(['program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
       
         if($countShifts > 0)
          {
         	   foreach($allShifts as $shift)
         	    {
         	   	      echo "<option ></option>"; 
         	   	      
         	   	      echo "<option value='".$shift->shift0->id."'>".$shift->shift0->shift_name."</option>";
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
         
    }



public function actionRoominshiftforgrades($from,$shift,$program,$level,$acad){
       
      if($from=='grades')
        { Yii::$app->session['grades_program']= $program;
        Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_shift']= $shift;
         Yii::$app->session['grades_room'] = '';
        }
      elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
        Yii::$app->session['studentHasCourses_level']= $level;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_room'] = '';
        }
      
        
        $countRooms = SrcCourses::find()->alias('c')->select('room')
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['apply_shift'=>$shift,'apply_for_program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select('room')
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['apply_shift'=>$shift,'apply_for_program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
        
         if($countRooms > 0)
          {
         	   echo "<option ></option>"; 
         	   
         	   foreach($allRooms as $room)
         	    { 
         	   	      echo "<option value='".$room->room0->id."'>".$room->room0->room_name."</option>";
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
         
    }


public function actionRoominshiftforgrades_($from,$shift,$program,$level,$acad){
       
    $current_acad = Yii::$app->session['currentId_academic_year'];
    
      if($from=='grades')
        { Yii::$app->session['grades_program']= $program;
        Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_shift']= $shift;
         Yii::$app->session['grades_group'] = '';
        }
      elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
        Yii::$app->session['studentHasCourses_level']= $level;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_group'] = '';
        }
      elseif($from=='idcards')
        { Yii::$app->session['idcard_program']= $program;
        Yii::$app->session['idcard_level']= $level;
           Yii::$app->session['idcard_shift']= $shift;
         Yii::$app->session['idcard_group'] = '';
        }
      
 
     if($current_acad==$acad){
        $countRooms = SrcCourses::find()->alias('c')->select(['student_level.room'])
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['apply_shift'=>$shift,'apply_for_program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select('student_level.room')
                         ->joinWith(['module0','students.studentLevel','students.studentOtherInfo0'])
                         ->where(['apply_shift'=>$shift,'apply_for_program'=>$program,'student_level.level'=>$level,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
        
        if($countRooms > 0)
          {
         	   echo "<option ></option>"; 
         	   
         	   foreach($allRooms as $room)
         	    { 
         	    	//chache room_name
         	    	if($room->room!=0)
         	    	 {  $room_name = SrcRooms::findOne($room->room)->room_name;
         	   	        echo "<option value='".$room->room."'>".$room_name."</option>";
         	    	 }
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
         
         
     }
     elseif($current_acad!=$acad)
     {    $countRooms = 0;
          $allRooms = Yii::$app->db->createCommand('SELECT DISTINCT room FROM student_level_history slh INNER JOIN student_other_info soi ON(soi.student=slh.student_id) INNER JOIN rooms r ON(r.id=slh.room) WHERE apply_shift='.$shift.' AND program='.$program.'  AND level='.$level.' AND academic_year='.$acad.' ORDER BY room_name ASC')->queryAll();
                        if(($allRooms!=null))
                          {
                            foreach($allRooms as $result)
                              {
                                $countRooms ++;
                              }
                          }
                          
         if($countRooms > 0)
          {
         	   echo "<option ></option>"; 
         	   
         	   foreach($allRooms as $room)
         	    { 
         	    	//chache room_name
         	    	if($room['room']!=0)
         	    	 {  $room_name = SrcRooms::findOne($room['room'])->room_name;
         	   	        echo "<option value='".$room['room']."'>".$room_name."</option>";
         	    	 }
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
     }
        
        
         
    }
    
 public function actionRoominshiftfordelayedgrades_($from,$shift,$program,$level,$acad){
       
      //if($from=='grades')
        //{ 
          Yii::$app->session['delayedgrades_program']= $program;
          Yii::$app->session['delayedgrades_level']= $level;
          Yii::$app->session['delayedgrades_shift']= $shift;
          Yii::$app->session['delayedgrades_group'] = '';
        //}
   /*   elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
        Yii::$app->session['studentHasCourses_level']= $level;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_group'] = '';
        }
      elseif($from=='idcards')
        { Yii::$app->session['idcard_program']= $program;
        Yii::$app->session['idcard_level']= $level;
           Yii::$app->session['idcard_shift']= $shift;
         Yii::$app->session['idcard_group'] = '';
        }
     */ 
 
      //$command = Yii::$app->db->createCommand('SELECT DISTINCT room FROM student_level_history slh INNER JOIN student_other_info soi ON(soi.student=slh.student_id) WHERE academic_year='.$acad.' and level='.$level.' and apply_shift='.$shift.' and apply_for_program='.$program )->queryAll(); 
      $command = Yii::$app->db->createCommand('SELECT DISTINCT room FROM student_level_history slh INNER JOIN student_has_courses shc ON(shc.student=slh.student_id) WHERE slh.academic_year='.$acad.' and slh.level='.$level.' and course in(Select c.id from courses c inner join module m on(m.id=c.module) where academic_year='.$acad.' and program='.$program.' and shift='.$shift.')' )->queryAll(); 
              
              
      if($command!=null)
       {
          echo "<option ></option>"; 
          
          foreach($command as $room)
         	    { 
         	    	//chache room_name
         	    	if($room['room']!=0)
         	    	 {  $room_name = SrcRooms::findOne($room['room'])->room_name;
         	   	        echo "<option value='".$room['room']."'>".$room_name."</option>";
         	    	 }
         	   	    
         	   	 }
       }  
      else
            echo "<option ></option>"; 
      
         
    }

 
 public function actionRoominshift($from,$shift,$program,$acad){
       
      if($from=='grades')
        { Yii::$app->session['grades_program']= $program;
           Yii::$app->session['grades_shift']= $shift;
         Yii::$app->session['grades_room'] = '';
        }
      elseif($from=='studenthascourses')
        { Yii::$app->session['studentHasCourses_program']= $program;
           Yii::$app->session['studentHasCourses_shift']= $shift;
           Yii::$app->session['studentHasCourses_room'] = '';
        }
      
        
        $countRooms = SrcCourses::find()->alias('c')->select('room')
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->count();
                          
        $allRooms = SrcCourses::find()->alias('c')->select('room')
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();
       //return  $allCourses;    
        
         if($countRooms > 0)
          {
         	   echo "<option ></option>"; 
         	   
         	   foreach($allRooms as $room)
         	    { 
         	   	      echo "<option value='".$room->room0->id."'>".$room->room0->room_name."</option>";
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
         
    }



 public function actionCourseinroomforgrades($from,$room,$shift,$program,$level,$acad){
        
      
        if($from=='grades')
         { 
           Yii::$app->session['grades_shift']= $shift;
            Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_room'] = $room;
           Yii::$app->session['grades_program']= $program;
         
          }
      elseif($from=='studenthascourses')
         {  
         	Yii::$app->session['studentHasCourses_level']= $level;
         	Yii::$app->session['studentHasCourses_shift']= $shift;
         	Yii::$app->session['studentHasCourses_room'] = $room;
            Yii::$app->session['studentHasCourses_program']= $program;
          
          }
        
 
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'student_level.level'=>$level,'academic_year'=> $acad,'old_new'=>1])
                         ->count();
                           
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'student_level.level'=>$level,'academic_year'=> $acad,'old_new'=>1])
                         ->all();
       //return  $allCourses;    
   
         if($countCourses > 0)
          {
          	
         	   echo "<option ></option>"; 
         	   
         	   foreach($allCourses as $course)
         	    {
         	   	     echo "<option value='".$course->id."'>".$course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]</option>";
         	   	    
         	   	 }
           }
         else 
            echo "<option ></option>"; 
         
          
    }
    
public function actionCourseingroupforgrades($from,$group,$shift,$program,$level,$acad){
        
      
        if($from=='grades')
         { 
           Yii::$app->session['grades_shift']= $shift;
            Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_group'] = $group;
           Yii::$app->session['grades_program']= $program;
         
          }
      elseif($from=='studenthascourses')
         {  
         	Yii::$app->session['studentHasCourses_level']= $level;
         	Yii::$app->session['studentHasCourses_shift']= $shift;
         	Yii::$app->session['studentHasCourses_group'] = $group;
            Yii::$app->session['studentHasCourses_program']= $program;
          
          }
        
 
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level.room'=>$group,'student_level.level'=>$level,'academic_year'=> $acad,'old_new'=>1])
                         ->count();
                           
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level.room'=>$group,'student_level.level'=>$level,'academic_year'=> $acad,'old_new'=>1])
                         ->all();
       //return  $allCourses;    
  
         if($countCourses > 0)
          {
          	
         	   echo "<option ></option>"; 
         	   
         	   foreach($allCourses as $course)
         	    {
         	   	   echo "<option value='".$course->id."'>".$course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]</option>";
         	   	    
         	   	 }
           }
         else 
            echo "<option ></option>"; 
         
          
    }
    

public function actionCourseingroupfordelayedgrades($from,$group,$shift,$program,$level,$acad){ // kou ki pa nan tab grades yo poko met not pou yo
        
      
       // if($from=='grades')
        // { 
           Yii::$app->session['grades_shift']= $shift;
            Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_group'] = $group;
           Yii::$app->session['grades_program']= $program;
         
        //  }
    /*  elseif($from=='studenthascourses')
         {  
         	Yii::$app->session['studentHasCourses_level']= $level;
         	Yii::$app->session['studentHasCourses_shift']= $shift;
         	Yii::$app->session['studentHasCourses_group'] = $group;
            Yii::$app->session['studentHasCourses_program']= $program;
          
          }
     * 
     */
        
    
       /* $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                        ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         //->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year='=>$acad])
                         //->andWhere('c.id not in(select distinct course from grades)')
                         //->orWhere('grade_value IS not NULL')
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                        // ->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year='=>$acad])
                         //->andWhere('c.id not in(select distinct course from grades)')
                         //->orWhere('grade_value IS not NULL')
                         ->all();      
         
        //return  $allCourses;    
      */
        $command = Yii::$app->db->createCommand('SELECT DISTINCT c.id, c.module, teacher FROM courses c INNER JOIN module m ON(c.module=m.id) INNER JOIN student_has_courses shc ON(shc.course=c.id) WHERE c.shift='.$shift.' AND m.program='.$program.' AND c.academic_year='.$acad.' AND c.id NOT IN(SELECT course FROM grades g WHERE g.student=shc.student) AND shc.student in(SELECT student_id FROM student_level_history slh WHERE slh.room='.$group.' and slh.level='.$level.' and slh.academic_year='.$acad.')' )->queryAll(); 
              
              
      if($command!=null)
       {
          	
         	   echo "<option ></option>"; 
         	   
         	   foreach($command as $course)
         	    {
                       $modelModule = SrcModule::findOne($course['module']);
                       $modelPers = SrcPersons::findOne($course['teacher']);
         	   	   echo "<option value='".$course['id']."'>".$modelModule->subject0->subject_name." (".$modelPers->getFullName().") [".$modelModule->code."]</option>";
         	   	    
         	   	 }
           }
         else 
            echo "<option ></option>"; 
         
          
    }
    

 public function actionCourseingroupfordelayedgradespc($from,$group,$shift,$program,$level,$acad){    //Particular Case
                                                                                                  // kou ki nan tab grades yo met not pou yo epi kek student pa gen not
                                                                                                  
      
       // if($from=='grades')
        // { 
           Yii::$app->session['grades_shift']= $shift;
            Yii::$app->session['grades_level']= $level;
           Yii::$app->session['grades_group'] = $group;
           Yii::$app->session['grades_program']= $program;
         
        //  }
    /*  elseif($from=='studenthascourses')
         {  
         	Yii::$app->session['studentHasCourses_level']= $level;
         	Yii::$app->session['studentHasCourses_shift']= $shift;
         	Yii::$app->session['studentHasCourses_group'] = $group;
            Yii::$app->session['studentHasCourses_program']= $program;
          
          }
     * 
     */
        
 
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                        ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year'=>$acad])
                         //->andWhere('grade_value IS not NULL')
                         ->count();
                          
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','teacher'])
                         ->distinct() 
                        ->join('INNER JOIN','module', 'c.module=module.id') //['module0','module0.program0.studentOtherInfos','students.studentLevel'])
                         ->join('INNER JOIN','program', 'program.id=module.program')
                         ->join('INNER JOIN','student_has_courses', 'student_has_courses.course=c.id') 
                         ->join('INNER JOIN','student_level_history', 'student_level_history.student_id=student_has_courses.student') 
                         ->join('INNER JOIN','grades', 'c.id=grades.course') 
                         ->where(['c.shift'=>$shift,'module.program'=>$program,'student_level_history.room'=>$group,'student_level_history.level'=>$level,'c.academic_year'=>$acad,'student_level_history.academic_year'=>$acad])
                         //->andWhere('grade_value IS not NULL')
                         ->all();      
         
        //return  $allCourses;    
  
         if($countCourses > 0)
          {
          	
         	   echo "<option ></option>"; 
         	   
         	   foreach($allCourses as $course)
         	    {
         	   	   echo "<option value='".$course->id."'>".$course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]</option>";
         	   	    
         	   	 }
           }
         else 
            echo "<option ></option>"; 
         
          
    }
    


 public function actionCourseinroom($from,$room,$shift,$program,$acad){
        
      
        if($from=='grades')
         { 
           Yii::$app->session['grades_shift']= $shift;
           Yii::$app->session['grades_room'] = $room;
           Yii::$app->session['grades_program']= $program;
         
          }
      elseif($from=='studenthascourses')
         {  
         	
         	Yii::$app->session['studentHasCourses_shift']= $shift;
         	Yii::$app->session['studentHasCourses_room'] = $room;
            Yii::$app->session['studentHasCourses_program']= $program;
          
          }
        
 
           
        $countCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'academic_year'=> $acad,'old_new'=>1])
                         ->count();
                           
        $allCourses = SrcCourses::find()->alias('c')->select(['c.id','c.module','c.teacher'])
                         ->joinWith(['module0','students.studentLevel'])
                         ->where(['c.room'=>$room,'c.shift'=>$shift,'module.program'=>$program,'academic_year'=> $acad,'old_new'=>1])
                         ->all();
       //return  $allCourses;    
   
         if($countCourses > 0)
          {
          	
         	   echo "<option ></option>"; 
         	   
         	   foreach($allCourses as $course)
         	    {
         	   	     echo "<option value='".$course->id."'>".$course->module0->subject0->subject_name." (".$course->teacher0->getFullName().") [".$course->module0->code."]</option>";
         	   	    
         	   	 }
           }
         else 
            echo "<option ></option>"; 
         
          
    }
    
    
   
    
    
    
    /**
     * Deletes an existing Courses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       if(Yii::$app->user->can('fi-courses-delete'))
         {
           try{
           	      $acad = Yii::$app->session['currentId_academic_year'];
           	      $teacher = 0;
           	      
			        $modelCourse = $this->findModel($id);
			        
			        $teacher = $modelCourse->teacher;
			        
			        $modelCourse->delete();
			        
			          //gad si moun sa gen lot kou li fe pou ane a, sil pa genyen retire role teacher a pou user_id li a
			          $modelCourse = new SrcCourses;
			          $teacherAllCourses = $modelCourse->getCourseByTeacherIDAcad($teacher,$acad);
			          
			          if($teacherAllCourses==null) //retire role teacher pou user li a
			           { 
			          	 //gad si li(user id) poko gen role teacher pou w ba li l
						          	  $user_id = 0;
						          	  $item_name = 'teacher';
						          	  
						          	  $modelUser = new User;
						          	  $userId = $modelUser->findByPersonId($teacher);
						          	  
						          	  if($userId!=null)
						          	    $user_id = $userId->id;
						          	  
						          	  
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                        //return user_id
                                        $modelUser_assign = $modelUserAssign->searchByUseridItemname($user_id, $item_name);//return user_id 
						          	   
						          	   if($modelUser_assign!=null)
						          	     {
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $items = ['teacher'];
						          	       $modelAssignment->revoke($items);
						          	       
						          	     }
						          	       	        
						          	     
                                       
			              }
			           
			           
			
			        return $this->redirect(['index']);
			        
			        
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Courses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Courses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcCourses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
