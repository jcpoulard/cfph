<?php
namespace app\modules\fi\controllers;


use Yii;
use yii\helpers\Html;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcShifts;
use app\modules\fi\models\SrcRooms;
use app\modules\fi\models\Grades;
use app\modules\fi\models\SrcGrades;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\StudentHasCourses;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\RepriseGrades;
use app\modules\fi\models\SrcAcademicperiods;

use app\modules\rbac\models\User;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

use yii\helpers\Url;

use kartik\mpdf\Pdf;

use kartik\widgets\Alert;


use yii\data\ActiveDataProvider;



/**
 * GradesController implements the CRUD actions for Grades model.
 */
class GradesController extends Controller
{
    
    public $layout = "/inspinia";
    
    public $transcriptItems=0;
    
    
    
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Grades models.
     * @return mixed
     */
    public function actionIndex()
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

        if(Yii::$app->user->can('fi-grades-index'))
         { 
             $acad = Yii::$app->session['currentId_academic_year'];
              
	        $searchModel = new SrcGrades();
	        
	        //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
        if($program==null)
          { 
          	 if(Yii::$app->session['profil_as'] ==0)
               $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams,$acad);
              elseif(Yii::$app->session['profil_as'] ==1)
          	    {  
          	    	$person_id = 0;
          	          $modelUser = User::findOne(Yii::$app->user->identity->id); 

				  if($modelUser->person_id!=NULL)
				    $person_id = $modelUser->person_id;
          	    	
          	    	 $dataProvider = $searchModel->searchGradesForTeacher($person_id,$acad);
          	    }
          	     
          	  
		      		              	      
		              	 
          	  
          	}
         else //se yon responsab pwogram
           {
           	     $dataProvider = $searchModel->searchByProgramAcad($program,$acad);
          	  
          	  if(isset($_GET['SrcGrades']))
		         $dataProvider= $searchModel->searchByProgramAcad($program,$acad);
			           	 

           	 } 

	    
	          

	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    public function actionReprise()
    {
        ini_set("memory_limit", "-1");
        set_time_limit(0);

      //  if(Yii::$app->user->can('fi-grades-reprise'))
       //  { 
 
	        $searchModel = new SrcGrades();
	        
	 
             if(Yii::$app->session['profil_as'] ==0)
               $dataProvider = $searchModel->searchReprise();
              elseif(Yii::$app->session['profil_as'] ==1)
          	    {  
          	    	$person_id = 0;
          	          $modelUser = User::findOne(Yii::$app->user->identity->id); 

				  if($modelUser->person_id!=NULL)
				    $person_id = $modelUser->person_id;
          	    	
          	    	 $dataProvider = $searchModel->searchRepriseForTeacher($person_id);
          	    }
          	     
          	   

	
	        return $this->render('reprise', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);

     /*   }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
      * */
      

    }
    
      public function actionViewreprise($stud)
    {  
    	//if(Yii::$app->user->can('fi-grades-viewreprise'))
        // {
              $query = SrcGrades::find()->joinWith(['student0','course0'])->where('student='.$stud.' and grade_value < passing_grade ')->all();
              
        return $this->render('viewreprise', [
            'data' => $query,
            
        ]);
        
      /*   }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       * 
       */
    }

    
 public function actionSetReprisegrades(){
      
   if(Yii::$app->user->can('fi-grades-setreprise'))
      {  
          $acad = Yii::$app->session['currentId_academic_year'];
       
         $modelReprise = new RepriseGrades;
        
         
       
         if(isset($_POST))
             {
                       $grade_value =null;
               
               if(isset($_POST['pk'])&&($_POST['pk']!=0))
                {
                   $modelReprise = RepriseGrades::findOne($_POST['pk']);
                   
                      if($_GET['set']=='r1')
                         { $grade_value = $_POST['value'];
                              $modelReprise->grade_value_1 = $grade_value;
                            
                          }
                        elseif($_GET['set']=='r2')
                         { $grade_value = $_POST['value'];
                              $modelReprise->grade_value_2 = $grade_value;
                          
                          }
                        elseif($_GET['set']=='r3')
                         { $grade_value = $_POST['value'];
                              $modelReprise->grade_value_3 = $grade_value;
                          
                          }
                      
                        $modelReprise->update_by = Yii::$app->user->identity->username;
                        $modelReprise->date_updated = date('Y-m-d H:i:s'); 
                        
                        if($modelReprise->save() )
                         {  $grade_id ='';
                            //al update not la nan tab grades la
                            $findrow = SrcGrades::find()->where('student='.$modelReprise->student.' and course='.$modelReprise->course)->all();
                            
                            foreach($findrow as $row)
                             {
                                $grade_id = $row->id;
                                break;
                               }
                              $modelGrades  = SrcGrades::findOne($grade_id);
                              
                              $modelGrades->grade_value = $grade_value;
                              $modelGrades->update_by = Yii::$app->user->identity->username;
                              $modelGrades->date_updated = date('Y-m-d H:i:s');
                              
                              if($modelGrades->save())
                                {
                                   $is_pass = 0;
                                   $modelCourse = SrcCourses::findOne($modelGrades->course);
                                   
                                   if($modelGrades->grade_value >= $modelCourse->passing_grade)
                                   {
                                     $modelIs_pass = SrcStudentHasCourses::find()->where('student='.$modelGrades->student.' and course='.$modelGrades->course)->all();
                                     foreach($modelIs_pass as $modelIspass)
                                      {
                                         $is_pass = $modelIspass->id;
                                      }
                                      
                                      $set_is_pass = SrcStudentHasCourses::findOne($is_pass);
                                      
                                       $set_is_pass->is_pass = 1;
                                       
                                       $set_is_pass->save();
                                    }
                                     
                                  }
                                  
                         }
               
                 }
              elseif(isset($_POST['pk'])&&($_POST['pk']==0))
                { 
                        $modelReprise = new RepriseGrades;
                     //si poko gen not ditou,kelkeswa kote w klike li pral nan grade_value_1 
                       // if($_GET['set']=='r1')
                        // { 
                            $grade_value = $_POST['value'];
                              $modelReprise->setAttribute('grade_value_1', $grade_value);
                             
                      /*    }
                        elseif($_GET['set']=='r2')
                         { $modelReprise->setAttribute('grade_value_2', $grade_value);
                             
                          }
                        elseif($_GET['set']=='r3')
                         { $modelReprise->setAttribute('grade_value_3', $grade_value);
                              
                          }
                       * 
                       */
                        $modelReprise->setAttribute('student', $_GET['stud']);
                        $modelReprise->setAttribute('course', $_GET['course']);
                        $modelReprise->setAttribute('update_by', Yii::$app->user->identity->username);
                        $modelReprise->setAttribute('date_updated', date('Y-m-d H:i:s') ); 
                        
                        if($modelReprise->save() )
                         { $grade_id ='';
                            //al update not la nan tab grades la
                            $findrow = SrcGrades::find()->where('student='.$modelReprise->student.' and course='.$modelReprise->course)->all();
                            
                            foreach($findrow as $row)
                             {
                                $grade_id = $row->id;
                                break;
                               }
                              $modelGrades  = SrcGrades::findOne($grade_id);
                              
                              $modelGrades->grade_value = $grade_value;
                              $modelGrades->update_by = Yii::$app->user->identity->username;
                              $modelGrades->date_updated = date('Y-m-d H:i:s');
                              
                              if($modelGrades->save())
                                {
                                   $is_pass = 0;
                                   $modelCourse = SrcCourses::findOne($modelGrades->course);
                                   
                                   if($modelGrades->grade_value >= $modelCourse->passing_grade)
                                   {
                                     $modelIs_pass = SrcStudentHasCourses::find()->where('student='.$modelGrades->student.' and course='.$modelGrades->course)->all();
                                     foreach($modelIs_pass as $modelIspass)
                                      {
                                         $is_pass = $modelIspass->id;
                                      }
                                      
                                      $set_is_pass = SrcStudentHasCourses::findOne($is_pass);
                                      
                                       $set_is_pass->is_pass = 1;
                                       
                                       $set_is_pass->save();
                                    }
                                    
                                    return $this->redirect(['viewreprise?stud='.$_GET['stud'].'&is_stud=1']);
                                     
                                  }
                              
                           }
                   
                   
                   
                  }
               
               
              }
              
              
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
       
    } 
    

    /**
     * Displays a single Grades model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       if(Yii::$app->user->can('fi-grades-view'))
         {

	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }


    /**
     * Creates a new Grades model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( (Yii::$app->user->can('fi-grades-create'))||( (Yii::$app->user->can('fi-grades-teacher_create'))&&(Yii::$app->user->can('teacher'))&&(Yii::$app->session['profil_as']==1) ) )  
         {
	
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = new SrcGrades();
	        
	        $modelCourses = new SrcCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	        $program= 0;
	        $shift= 0;
	        $room = 0;
	        $level=0;
	        $course = 0;
                $is_save = 0;
	        $course_weight ='';
	        
                $set_room=0;
                
	        $success=false;
	        $use_update=false;
	        $message_course_id=false;
	        $message_GradeHigherWeight=false;
	        $message_noGradeEntered=false;
			$no_stud_has_grade=true;
	         
	         $programs_leaders = 0;
                 
                 if( (isset($_GET['course']) )&&( isset($_GET['stud']) ))
                    {                
                       $modelPers = SrcPersons::findOne($_GET['stud']);
  
                        if( (! isset($modelPers->studentLevel->room) )||($modelPers->studentLevel->room==0))
                          {
                             Yii::$app->getSession()->setFlash('warning', [
                                   'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                   'duration' => 36000,
                                   'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                   'message' => Html::encode( Yii::t('app','Set room for this student first.') ),
                                   'title' => Html::encode(Yii::t('app','Warning') ),
                                   'positonY' => 'top',   //   top,//   bottom,//
                                   'positonX' => 'center'    //   right,//   center,//  left,//
                               ]);
                           $set_room=1;
                          }
                          
                    }

	          //return program_id or null value
             $program = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program==null)
		       {
			       //$dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0); //pou le kou nan tri a
	       
	             	      $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0); 
		       }
		      else
		        {   $programs_leaders = 1;
		            //$model->program=$program; 
		        	 $dataProvider = $searchModelStudent->searchStudentsInProgramLevel($program,0); 
		        }
	        
	        
	
	      if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	            
	    if(Yii::$app->session['profil_as'] ==0)
             {
    				                                   
       
	               if(isset($_POST['SrcGrades']['program']))
	                { $program= $_POST['SrcGrades']['program'];
	                  $model->program=[$program];
	                }
	                
	              
	             
		         if(isset($_POST['SrcGrades']['level']))
		           { $level= $_POST['SrcGrades']['level'];
		                 $model->level=[$level]; 
		                 
		            }
		              
		             if(isset($_POST['SrcGrades']['shift']))
		              { $shift= $_POST['SrcGrades']['shift'];
		                 $model->shift=[$shift]; 
		         
		              }
		            
	        }  
		             if(isset($_POST['SrcGrades']['room']))
		              {  $room = $_POST['SrcGrades']['room'];
		                   $model->room=[$room];
		               
		              }
		              
		             if(isset($_POST['SrcGrades']['course']))
		              {  $course = $_POST['SrcGrades']['course'];
		                   $model->course=[$course];
		               }
	               
	         /*  //retire sa poutet yo pa submit onchange
	               if($program != Yii::$app->session['grades_program'])
	                 {  Yii::$app->session['grades_program']= $program;
	                    
	                     $course = '';
	                     $model->course=[$course];
	                     $room='';
	                     Yii::$app->session['grades_room'] = $room;
	                     $model->room=[$room];
	                     $shift='';
	                     Yii::$app->session['grades_shift']= $shift;
	                     $model->shift=[$shift];
	                     //$level='';
	                     //Yii::$app->session['grades_level'] = $level;
	                     //$model->shift=[$level];
	                  }
	                else
	                  {  
	                  	
	                  	if($level != Yii::$app->session['grades_level'])
			                 {  Yii::$app->session['grades_level']= $level;
			                    
			                    // $course = '';
			                   //  $model->course=[$course];
			                   //  $room='';
			                    // Yii::$app->session['grades_room'] = $room;
			                    // $model->room=[$room];
			                    // $shift='';
			                    // Yii::$app->session['grades_shift']= $shift;
			                    // $model->shift=[$shift];
			                     //
			                      $model->program=0;
		                           $model->shift=0; 
		                           Yii::$app->session['grades_shift']= 0;
		                           Yii::$app->session['grades_course']= 0;
		                           Yii::$app->session['grades_room']= 0;
			                     
			                  }
	                  //	else
	                  	//   {
			              //     if($shift != Yii::$app->session['grades_shift'])
				                 {  Yii::$app->session['grades_shift']= $shift;
				                    
				                     $course = '';
				                     $model->course=[$course];
				                     $room='';
				                     Yii::$app->session['grades_room'] = $room;
				                     $model->room=[$room];
				                     
				                  }
				                  
	                  	  //  }
	                  	    
			          }
                */
                
	                  
	          
	           
	         
        if(Yii::$app->session['profil_as'] ==0)
          {
             if(($program!='')&&($program!=0)&&($course!='')&&($course!=0))
	             {
	             	Yii::$app->session['grades_course']= $course;
	             	Yii::$app->session['grades_shift']=$shift;
	             	Yii::$app->session['grades_level']=$level;
	             	Yii::$app->session['grades_group']=$room;
	             	
	             	if(infoGeneralConfig('course_shift')==0)
	             	   {
	             	   	 //tcheke si gen not deja pou kou sa
	             	   	  $use_update=false;
		                    
				            $check = $this->_checkGradeDataByCourseGroup($course,$room); //$check = $this->_checkGradeDataByCourse($course);
							
							if($check)
							  { 
							  	 $searchModelStudentGrade = new SrcGrades();
							  	 
							  	 //gad si gen student ki nan kou a ki poko gen not
							  	   //$dataProvider= $searchModelStudent->searchStudentInCourseNotInGradeTable_withoutShift($program,$course,$acad);
							  	 $dataProvider= $searchModelStudent->searchStudentInLevelGroupAndCourseNotInGradeTable($program,$level,$room,$course,$acad);
							  	
							  	   if($dataProvider->getModels()==null)
							  	     { $use_update=true; 
							  	        Yii::$app->session['grades_course']  = $course; 
							            $dataProvider = $searchModelStudentGrade->searchStudentsGradeForCourseByGroup($course,$room,$shift,$acad);   //searchStudentsGradeForCourse($course);
							  	     }
							     
							  }
							else			  	
						      {  //$dataProvider = $searchModelStudent->searchStudentsInProgramInCourse($program,$course,$acad);
						           $dataProvider = $searchModelStudent->searchStudentsInProgramInLevelGroupCourse($program,$level,$room,$course,$acad);
						      }
	             	   }
	             	 elseif(infoGeneralConfig('course_shift')==1)
	             	   { 
	             	   	
	             	   	   //tcheke si gen not deja pou kou sa
	             	   	     $use_update=false;
		                    
				            $check = $this->_checkGradeDataByCourseGroup($course,$room);  //$check = $this->_checkGradeDataByCourse($course);
							
							if($check)
							  {  
							  	$searchModelStudentGrade = new SrcGrades();
							  	
							  	//gad si gen student ki nan kou a ki poko gen not
							  	  //$dataProvider= $searchModelStudent->searchStudentInCourseNotInGradeTable($program,$shift,$course,$acad);
							  	$dataProvider= $searchModelStudent->searchStudentInShiftLevelGroupAndCourseNotInGradeTable($program,$shift,$level,$room,$course,$acad);
	
							  	
							  	   if($dataProvider->getModels()==null)
							  	     { $use_update=true; 
							  	        Yii::$app->session['grades_course']  = $course;
							            $dataProvider = $searchModelStudentGrade->searchStudentsGradeForCourseByGroup($course,$room,$shift,$acad);   //searchStudentsGradeForCourse($course);
							  	     } 
							  }
							else			  	
						       { //$dataProvider = $searchModelStudent->searchStudentsInProgramInShiftAndCourse($program,$shift,$course,$acad);
						         $dataProvider = $searchModelStudent->searchStudentsInProgramInShiftLevelGroupAndCourse($program,$shift,$level,$room,$course,$acad);
						       
						       }
	             	   
	             	    }
	              }
	              
	              
            }
         elseif(Yii::$app->session['profil_as'] ==1)  //se yon pwof
           {
           	        Yii::$app->session['grades_course']= $course;
	             	Yii::$app->session['grades_group']=$room;
	             	
	             	
	             	   	 //tcheke si gen not deja pou kou sa
	             	   	  $use_update=false;
		                    
				            $check = $this->_checkGradeDataByCourseGroup($course,$room); //$check = $this->_checkGradeDataByCourse($course);
							
							if($check)
							  { 
							  	 $searchModelStudentGrade = new SrcGrades();
							  	 
							  	 //gad si gen student ki nan kou a ki poko gen not
							  	 $dataProvider= $searchModelStudent->searchStudentNotInGradeTable_forTeacher($room,$course,$acad);
							  	
							  	   if($dataProvider->getModels()==null)
							  	     { $use_update=true; 
							  	        Yii::$app->session['grades_course']  = $course; 
							            $dataProvider = $searchModelStudentGrade->searchStudentsGradeForCourseByGroup($course,$room,$shift,$acad);   //searchStudentsGradeForCourse($course);
       
							  	     }
							     
							  }
							else			  	
						      {  
                                $dataProvider = $searchModelStudent->searchStudentsForTeacher($room,$course,$acad);
						      }
	             	   
	             	 
	             	   
           	
           	}
                  if(isset($_GET['course']))
                    { $course = $_GET['course'];
                          
                    }
                    
                    //$course=(int)$model->course;
                   
                  if($course!=0)
                    { $course_weight = $modelCourses->getWeight($course);
                       //Yii::$app->session['grades_course']  = $course;
                    }
                     
                     	
	           
	           if(isset($_POST['create']))
	            {
	                 	                   
	                $success=false;
					$temwen=false;
			        
					$message_GradeHigherWeight=false;	
					  
                    $weight = ' ';
                    
                    $passingGrade = 0; 
                    
                    
                   
                  if($course!=0)
                   { 
                     $weight = $modelCourses->getWeight($course);
                     
                     $passingGrade = $modelCourses->getPassingGrade($course);
                     
	                  $ok=true;
	                    $model_new = new Grades();
	                   
					
					 $message_noGradeEntered=false;
					 $no_stud_has_grade=true;
			 	
                              if(isset($_GET['stud']))
                                {
                                  
                                  $grades_comment = infoGeneralConfig('grades_comment');
                                  $comment='';
                                  
                                  if($grades_comment==1)
                                        $comment=$model->comments;
                                                
                                   $grade = $model->grade_value;                     
                                   
                                    if($grade <= $weight)  
				     { 
                                        //gade si li nan grades deja
                                        $modelG = new SrcGrades();
                                       $info_grade = $modelG->getGradeInfoByStudentCourse($_GET['stud'],$course);
                                     
                                       if($info_grade!=NULL) //update li
                                         {
                                           $model_new= SrcGrades::findOne($info_grade->id);
                                           $model_new->grade_value=$grade;
                                           $model_new->setAttribute('validate',1);
		                                $model_new->setAttribute('publish',1);
                                           $model_new->date_updated=date('Y-m-d H:i:s');
					   $model_new->update_by=currentUser();
                                           
                                         }
                                       else
                                        {
                                      $model_new->setAttribute('student',$_GET['stud']);
		                      $model_new->setAttribute('course',$course);
                                      $model_new->setAttribute('grade_value',$grade);
                                      
                                      
                                      $model_new->setAttribute('comment',$comment);
						                $model_new->setAttribute('validate',0);
		                                $model_new->setAttribute('publish',0);
						                $model_new->date_created=date('Y-m-d H:i:s');
						                $model_new->create_by=currentUser();
                                        }
						                
                                        
                                        if($model_new->save() )
						                  { 
						                  	 //if course is passed
						                  	   if( $grade >= $passingGrade )
						                  	     {  //it's done
						                  	          $command = Yii::$app->db->createCommand();
						                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$course])->execute(); 
						                  	     
						                  	     	} 
						                  	
						                  	 $temwen=true;
						                     $ok=true;
						         
						                  	
						                  	}
                                                                        
                                                 
                                                 
                                                 
                                              }                          
                                           else
                                              {
                                                 $message_GradeHigherWeight=true;

                                               }                             
                                                                        
                                          
                                          
                                          
                                          
                                          
                                         if ($ok==true) 
					    {  
                                               if($message_GradeHigherWeight==false)
                                                              return $this->redirect(['../fi/persons/moredetailsstudent?id='.$_GET['stud'].'&is_stud=1', ]);
                                             }
                                                                        
                                }
                                else
                                  {	
                                     
                                 		   
				     foreach($_POST['id_stud'] as $id)
		                        {   	   
                                           if(isset($_POST['grades'][$id])&&($_POST['grades'][$id]!=''))
                                                $no_stud_has_grade=false;

                                        }
                                  
			             
					      if($no_stud_has_grade==false) // omwen 1 etidyan gen not
						    {   foreach($_POST['id_stud'] as $id)
						         {
						            
	                                  if(isset($_POST['grades'][$id]))
								                $grade=$_POST['grades'][$id];
											else
											 $grade=0;
												
												
											if(isset($_POST['comments'][$id]))
								                $comment=$_POST['comments'][$id];
											else
												$comment='';
							           
							                   //check if grade is higher than the course weight
						           if($grade <= $weight)  
						             { 

		                                $model_new->setAttribute('student',$id);
		                                $model_new->setAttribute('course',$course);
							            
							            
						                $model_new->setAttribute('grade_value',$grade);
									    $model_new->setAttribute('comment',$comment);
						                $model_new->setAttribute('validate',0);
		                                $model_new->setAttribute('publish',0);
						                $model_new->date_created=date('Y-m-d H:i:s');
						                $model_new->create_by=currentUser();
			
						                if($model_new->save() )
						                  { 
						                  	 //if course is passed
						                  	   if( $grade >= $passingGrade )
						                  	     {  //it's done
						                  	          $command = Yii::$app->db->createCommand();
						                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$course])->execute(); 
						                  	     
						                  	     	} 
						                  	
						                  	unset($model_new);
						                     $model_new = new Grades();
						                     
						                     $temwen=true;
						                     $ok=true;
						                  	
						                  	}
					                 
					                      }
						            else
						              {
						              	 unset($model_new);
						                 $model_new = new Grades();
						                 
						              	 $message_GradeHigherWeight=true;
						              	 
						               }
						            										   
								} 
						             
						      }
						    else 
						     { $ok=false;
						       $message_noGradeEntered=true;
						         $model->program=0;
						     }
                                                     
                                                     
									
			               if ($ok==true) 
					          {  
                                                      if(Yii::$app->session['profil_as'] ==1)  //se yon pwof
                                                        { $is_save =1;
                                                           
                                                           $dataProvider = $model->searchStudentsGradeForCourseByGroup($model->course,Yii::$app->session['grades_group'],$shift,$acad);
                                                        
                                                           Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                        } 
                                                      else
                                                        {    //$dbTrans->commit();  	
                                                            if($message_GradeHigherWeight==false)
                                                              return $this->redirect(['index', ]);
                                                            
                                                          }
					          }
					        else
					           { //   $dbTrans->rollback();
					           }
                                                   
                    
                                                   
                                                   }
                   }
                  else
                    { $message_course_id=true;
                      $model->program=0;
                    }
	            }
	            
                    
                    if(isset($_POST['pdf']))
	            {
                        $content = $this->renderPartial('_pdfNotes', ['course' => Yii::$app->session['grades_course'],'room' => Yii::$app->session['grades_group']]);

                                                            //$heading = pdfHeading();  //$this->renderPartial('_pdf_heading');

                                                                 $pdf = new Pdf();


                                                                 $pdf->filename = Yii::t("app","Teacher notes").'_'.'.pdf';


                                                                 $pdf->content = Pdf::MODE_CORE;
                                                                 $pdf->mode = Pdf::MODE_BLANK;
                                                                 $pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                                                 $pdf->defaultFontSize = 10;
                                                                 $pdf->defaultFont = 'helvetica';
                                                                 $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER;//or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                                                                 $pdf->orientation = Pdf::ORIENT_PORTRAIT;//or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                                                                 $pdf->destination = Pdf::DEST_DOWNLOAD;//or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F

                                                                 $pdf->content = $content;
                                                                 $pdf->options = ['title' => 'Teacher\'s notes'];
                                                                 $pdf->methods = [
                                                                         'SetHeader'=>[''], // $heading,
                                                                         'SetFooter'=>['{PAGENO}'],
                                                                 ];

                                                                 $pdf->options = [
                                                                         'title' => 'Teacher\s notes',
                                                                         'autoScriptToLang' => true,
                                                                         'ignore_invalid_utf8' => true,
                                                                         'tabSpaces' => 4
                                                                 ];


                                                                 // return the pdf output as per the destination setting
                                                                  return $pdf->render();
                    }
	            
	        } 
	        
	
	            return $this->render('create', [
	                'model' => $model,
	                'dataProvider'=>$dataProvider, 
	                'course_weight'=>$course_weight,
	                'success'=>$success,
                        'is_save'=>$is_save,
                        'set_room'=>$set_room,
	                'use_update'=> $use_update,
	                'message_course_id'=> $message_course_id,
	                'message_GradeHigherWeight'=>$message_GradeHigherWeight,
	                'message_noGradeEntered'=>$message_noGradeEntered,
					'no_stud_has_grade'=>$no_stud_has_grade,
	            ]);
	            
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing Grades model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        if( (Yii::$app->user->can('fi-grades-update'))||( (Yii::$app->user->can('fi-grades-teacher_update'))&&(Yii::$app->user->can('teacher'))&&(Yii::$app->session['profil_as']==1) ) )  
         {
            $acad = Yii::$app->session['currentId_academic_year'];
            
	           $message_UpdateValidate=false;
		       $message_UpdateValidate1=false;
		       $message_UpdatePublish=false;
		       $message_noGrades=false;
		       $message_gradesOk=false;
		       $messageNoCheck=false;
		       
		        $model = new SrcGrades;
		       
		       $grades_comment = infoGeneralConfig('grades_comment');	
		       
		       $update_validate = false;
		       
		       $message_GradeHigherWeight=false;
		       
		       $modelCourses= new SrcCourses();
                       
               $passingGrade = 0;
	        
	
	        	    
	    if(!isset($_GET['all']))
		  {  $model = $this->findModel($_GET['id']);
		      
		       $weight = $modelCourses->getWeight($model->course);  
		       
		       $passingGrade = $modelCourses->getPassingGrade($model->course); 
		       
		     
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	          if(isset($_POST['update']))
	            {
	             	 //profil admin
		         if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) || (Yii::$app->user->can("fi-grades-update_validatedG")) )			              
				 
		          {
	          				        
					$message_GradeHigherWeight=false;	
					  
                        $grade=0;
									 $comment='';
									 					
						       foreach($_POST['id_grade'] as $id)
						         {
						            
	                                  if(isset($_POST['grades'][$id]))
								                $grade=$_POST['grades'][$id];
											else
												$grade=0;
												
												
											if(isset($_POST['comments'][$id]))
								                $comment=$_POST['comments'][$id];
											else
												$comment='';
							           
							                   //check if grade is higher than the course weight
						           if($grade <= $weight)  
						             { 
						                $model->setAttribute('grade_value',$grade);
									    $model->setAttribute('comment',$comment);
						                $model->date_updated=date('Y-m-d H:i:s');
						                $model->update_by=currentUser();
			
						                if($model->save() )
						                  {  
						                  	  //if course is passed
						                  	   if( $grade >= $passingGrade )
						                  	     {  //it's done
						                  	          $command = Yii::$app->db->createCommand();
						                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model->student, 'course'=>$model->course])->execute(); 
						                  	     
						                  	     	}
						                  	//$dbTrans->commit(); 
						                  			 return $this->redirect(['view', 'id'=>$model->id,'from'=>'stud']);				                  	
						                  	} 
						                  else
									           { //   $dbTrans->rollback();
									           }
					                     
					                   }
						            else
						              {
						              	 $message_GradeHigherWeight=true;
						              	 
						               }
						            										   
								} 
						             
						     									
			               
                   
	                   
			          }
			        else
			          {  
			     	    if(($model->validate!=1))   //
				          {
				             			        
							    $message_GradeHigherWeight=false;	
							  
		                        $grade=0;
											 $comment='';
											 					
						       foreach($_POST['id_grade'] as $id)
						         {
						            
	                                  if(isset($_POST['grades'][$id]))
								                $grade=$_POST['grades'][$id];
											else
												$grade=0;
												
												
											if(isset($_POST['comments'][$id]))
								                $comment=$_POST['comments'][$id];
											else
												$comment='';
							           
							                   //check if grade is higher than the course weight
						           if($grade <= $weight)  
						             { 
						                $model->setAttribute('grade_value',$grade);
									    $model->setAttribute('comment',$comment);
						                $model->date_updated=date('Y-m-d H:i:s');
						                $model->update_by=currentUser();
			
						                if($model->save() )
						                  { 
						                  	//if course is passed
						                  	   if( $grade >= $passingGrade )
						                  	     {  //it's done
						                  	          $command = Yii::$app->db->createCommand();
						                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model->student, 'course'=>$model->course])->execute(); 
						                  	     
						                  	     	}
						                  	     	
						                  	 //$dbTrans->commit(); 
						                        
						                  			 return $this->redirect(['view', 'id'=>$model->id,'from'=>'stud']);				                  	
						                  	} 
						                  else
									           { //   $dbTrans->rollback();
									           }
					                     
					                   }
						            else
						              {
						              	 $message_GradeHigherWeight=true;
						              	 
						               }
						            										   
								} 
						             
						     									
			               
                   
	                     
		             
				             }
			     	      else
						     {
						       	//$url=Yii::$app->request->referrer;
						       	//return $this->redirect($url.'&msguv=y');
						       	Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','Validated Grades can\'t be updated.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

						     }

	     	
	     	    
	     	
	     	
	                 }
	                 
	            }
	            
	             // if(isset($_POST['cancel']))
	           
	           
	           
	           
	                       
	        } 
	      elseif($_GET['all']==1)
		  {
                $ok = false; 
                $ok_update_validate = false;
                $program= 0;
	        $shift= 0;
	        $room = 0;
	        $course = 0;
	        $level = 0;
	        $passingGrade = 0;
	        $course_weight ='';
                
                
	        //  $model = new SrcGrades();
	          
	          
	          $modelCourses = new SrcCourses();
	          
	       if(isset($_GET['from']) )
	         {
	          
	           if(isset($_POST['SrcGrades']['level']))
		              { $level= $_POST['SrcGrades']['level'];
		              Yii::$app->session['grades_level'] = $level;
		                 $model->level=[$level]; 
		                 
		                 Yii::$app->session['grades_program']= 0;
		                  $model->program=0;
		              }
		              
	          if(isset($_POST['SrcGrades']['program']))
	              { $program= $_POST['SrcGrades']['program'];
	              Yii::$app->session['grades_program']= $program;
	                $model->program=[$program];
	               }
	             
		             if(isset($_POST['SrcGrades']['shift']))
		              { $shift= $_POST['SrcGrades']['shift'];
		              Yii::$app->session['grades_shift']= $shift;
		                 $model->shift=[$shift]; 
		         
		              }
		              
		             if(isset($_POST['SrcGrades']['room']))
		              {  $room = $_POST['SrcGrades']['room'];
		              Yii::$app->session['grades_group']= $room;
		                   $model->room=[$room];
		               
		              }
		              
		            
		              
		             if(isset($_POST['SrcGrades']['course']))
		              {  $course = $_POST['SrcGrades']['course'];
		                   $model->course=[$course];
                                   Yii::$app->session['grades_course']= $course;
		               }
		             else
		               {
		               	   Yii::$app->session['grades_course'] =0;
		               	 }
	                 
	               if($program != Yii::$app->session['grades_program'])
	                 {  Yii::$app->session['grades_program']= $program;
	                    
	                     //$course = '';
	                     //$model->course=[$course];
	                     $room='';
	                     Yii::$app->session['grades_group'] = $room;
	                     $model->room=[$room];
	                     $shift='';
	                     Yii::$app->session['grades_shift']= $shift;
	                     $model->shift=[$shift];
	                     
	                     $level='';
	                     Yii::$app->session['grades_level']= $level;
	                     $model->shift=[$level];
	                     
	                  }
	                else 
	                  {  
	                  	
	                  	if($level != Yii::$app->session['grades_level'])
			                 {  Yii::$app->session['grades_level']= $level;
			                    
			                     //$course = '';
			                     //$model->course=[$course];
			                     $room='';
			                     Yii::$app->session['grades_group'] = $room;
			                     $model->room=[$room];
			                     $shift='';
			                     Yii::$app->session['grades_shift']= $shift;
			                     $model->shift=[$shift];
			                     
			                  }
	                  //	else
	                  	//   {
			                   if($shift != Yii::$app->session['grades_shift'])
				                 {  Yii::$app->session['grades_shift']= $shift;
				                    
				                     $course = '';
				                     $model->course=[$course];
				                     $room='';
				                     Yii::$app->session['grades_group'] = $room;
				                     $model->room=[$room];
				                     
				                  }
				                  
	                  	  //  }
	                  	    
			          }
                
                
		        }
		  
		          
               $shift =Yii::$app->session['grades_shift'];
               $level =Yii::$app->session['grades_level'];
               $room =Yii::$app->session['grades_group'];
               $course = Yii::$app->session['grades_course'];    
               
               $weight = $modelCourses->getWeight($course); 
	          
	          $passingGrade = $modelCourses->getPassingGrade($course);
	                         
	          //gad si pat gen omwen 1 not valide
	        if(Yii::$app->session['profil_as'] ==0)  
	          $result = $model->searchByShiftLevelGroupCourse($shift,$level,$room,$course)->getModels();  //$result = $model->searchByCourse($course)->getModels();
	         elseif(Yii::$app->session['profil_as'] ==1)
	             $result = $model->searchByGroupCourse($room,$course)->getModels();
                  
                 
                 if( (!Yii::$app->user->can("superadmin")) && (!Yii::$app->user->can("fi-grades-update_validatedG")) )
                   {
                 
                   if($result!=null)
					 { 
					   //if((Yii::app()->user->profil!='Admin')&&(Yii::app()->user->profil!='Manager'))
			              
						   foreach($result as $g)
							 {
							   if($g->validate==1)
								 {
									$message_UpdateValidate=true;
									Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','Validated Grades can\'t be updated.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

								  }
							  }
			              
			           }
                       }
							              
			if(isset($_POST['update']))
			  {				              
				  $model_new = new Grades;
				  
				//if((Yii::app()->user->profil=='Admin')||(Yii::app()->user->profil=='fi-grades-update_validatedG'))
				 if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) || (Yii::$app->user->can("fi-grades-update_validatedG")) )			              
				  {  
			  				        
							$message_GradeHigherWeight=false;	
							  
		                        $grade=0;
											 $comment='';
											 $average=0;
										 $nbr_grade=0;
											 					
						       foreach($_POST['id_grade'] as $grade_id)
						         {
						         	$nbr_grade++;
						            
	                                  //if(isset($_POST['grades'][$grade_id]))
								                $grade=$_POST['grades'][$grade_id];
											//else
											//	$grade='';
												
												
										if($grades_comment==1)//	if(isset($_POST['comments'][$grade_id]))
								                $comment=$_POST['comments'][$grade_id];
										//	else
										//		$comment='';
												
							         
							          
							           $model_new= $this->findModel($grade_id);	   
							
                                           if(($model_new->publish!=1))
		                                    {
							                   //check if grade is higher than the course weight
									           if($grade <= $weight)  
									             { 
									             	$average=$average+ $grade;
									             	
									                $model_new->setAttribute('grade_value',$grade);
												    $model_new->setAttribute('comment',$comment);
									                $model_new->date_updated=date('Y-m-d H:i:s');
									                $model_new->update_by=currentUser();
						
								                   }
									            else
									              {
									              	 $message_GradeHigherWeight=true;
									              	 
									               }
									               
									            
									                if($model_new->save() )
									                  { 
									                  	//if course is passed
								                  	   if( $grade >= $passingGrade )
								                  	     {  //it's done
								                  	          $command = Yii::$app->db->createCommand();
								                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
								                  	     
								                  	     	} 
								                  	    else
								                  	       {  //it's done
								                  	          $command = Yii::$app->db->createCommand();
								                              $command->update('student_has_courses', ['is_pass' => 0, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
								                  	     
								                  	     	}
								                  	     	
									                  	//$dbTrans->commit(); 
									                  		unset($model_new);
															   $model_new= new SrcGrades;
															   $success_=1;
															   $ok=true;			                  	
									                  	} 
									                  else
												           { //   $dbTrans->rollback();
												           }
								                     
									               
		                                    }
		                                  else
									         {      
					 								
					 							
					 							if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) || (Yii::$app->user->can("fi-grades-update_validatedG")) )	
					 							  {
							 							
							 							
							 							if($grade<=$weight) 
													      {   
												     	     $average=$average+ $grade;
												     	    
												     	    $model_new->setAttribute('date_updated',date('Y-m-d H:i:s'));
													        $model_new->setAttribute('grade_value',$grade);	
													         $model_new->setAttribute('comment',$comment);									     	                                                                          
													         $model_new->setAttribute('update_by',currentUser());
													        
												      
													       if($model_new->save())
															 {  
															 	//if course is passed
											                  	   if( $grade >= $passingGrade )
											                  	     {  //it's done
											                  	          $command = Yii::$app->db->createCommand();
											                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
											                  	     
											                  	     	}
											                  	    else
											                  	       {  //it's done
											                  	          $command = Yii::$app->db->createCommand();
											                              $command->update('student_has_courses', ['is_pass' => 0, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
											                  	     
											                  	     	}
											                  	     	
															   unset($model_new);
															   $model_new= new SrcGrades;
															   $success_=1;
															   $ok=true;
															   $ok_update_validate=true;
															   
															 }
													      
													      
													       }
													     else
														   $message_GradeHigherWeight=true;
                                          
													   
													  

					 							   }
					 							 else	
					 							  {

										         	
										             $url=Yii::$app->request->referrer;
			       	    							 
			       	    							 $update_validate=true;
			       	    							 $message_UpdateValidate=true;
			       	    							 $message_UpdatePublish=true;
			       	    							 
					 							   }
					 							   
					 							   
									          }
						            										   
								} 
						             
						    		
						if(($ok==true)||($ok_update_validate==true))	     
						  { $success_=1;
                                                  $acad = Yii::$app->session['currentId_academic_year'];
							         //------------- SUBJECT AVERAGE BY PERIOD  -------------//
                            if($nbr_grade>0)
                             {  $average=$average/$nbr_grade;
										 
			      	           //save subject average for this period			  
						   $command = Yii::$app->db->createCommand();
						   
         
         
						   //check if already exit
						      $modelGra =  new SrcGrades();
							  $data =  $modelGra->checkDataSubjectAverage($course);
							  $is_present=false;
							       if($data)
								     $is_present=true;
								  
                                if($is_present){// yes, update
								
								    $command->update('subject_average', ['average' => $average, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['course'=>$course])->execute();
                                   }
								 else{// no, insert
                                                                     
								  			 $command->insert('subject_average', [
											    'academic_year'=>$acad,
										'course'=>$course,
										'average'=>$average,
										'create_by'=>currentUser(),
										'date_created'=>date('Y-m-d'),
										
											])->execute();
									
									
								     
								     }
								 
                             }
							       

							      
							      
							  return $this->redirect (array('index?from=stud')); // return $this->redirect (array('update?all=1&from=stud&mn=std&ok=yes&greater='.$message_GradeHigherWeight));
						  }
						if($update_validate)
						  { //$message_UpdateValidate=true;
						     //return $this->redirect (array('update?all=1&from=stud&mn=std&msguv=y'));
						     Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','Validated Grades can\'t be updated.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

						   }
						else
						  return $this->redirect (array('index?from=stud')); 									
			               
                     
	                     
	  
	  
	   
		            }
			     else ///pa admin ni manager
			        {	
		   	
		   	
			        if($message_UpdateValidate==false)   //rasire ke se pou pwofese li tcheke
				       {
				 
									 //reccuperer le ligne modifiee
								$message_UpdateValidate=false;
								
								$message_UpdatePublish=false;
								$success_=0;
								$update_validate=false;
								$ok=false;
								$message_GradeHigherWeight=false;
								
								$ok_update_validate=false;
								
												 $grade=0;
											 $comment='';
												 $average=0;
												 $nbr_grade=0;
								
									   foreach($_POST['id_grade'] as $grade_id)
									     {  
									     	$nbr_grade++;
									     	
									     	
									     	if(isset($_POST['grades'][$grade_id]))
												$grade=$_POST['grades'][$grade_id];
												
											if(isset($_POST['comments'][$grade_id]))
										        $comment=$_POST['comments'][$grade_id];
									     	
									     	
									     	$model_new= $this->findModel($grade_id);		   
														
		                                           if(($model_new->publish!=1))
				                                    {                                                
													   
													   if($grade<=$weight) 
													      {   
												     	     $average=$average+ $grade;
												     	    
												     	    $model_new->setAttribute('date_updated',date('Y-m-d H:i:s'));
													        $model_new->setAttribute('grade_value',$grade);	
													         $model_new->setAttribute('comment',$comment);	
													         $model_new->setAttribute('update_by',currentUser());								     	   
													         if($model_new->save())
																 {  
																 	
																 	//if course is passed
												                  	   if( $grade >= $passingGrade )
												                  	     {  //it's done
												                  	          $command = Yii::$app->db->createCommand();
												                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
												                  	     
												                  	     	}
												                  	    else
												                  	       {  //it's done
												                  	          $command = Yii::$app->db->createCommand();
												                              $command->update('student_has_courses', ['is_pass' => 0, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
												                  	     
												                  	     	}
						                  	     	
																   unset($model_new);
																   $model_new= new SrcGrades;
																   $success_=1;
																   $ok=true;
																  
																 }

													       }
													     else
														   $message_GradeHigherWeight=true;
		
		                                                                                           
													  }
				                                   else
											         {     							 								
							 							
							 							if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) || (Yii::$app->user->can("fi-grades-update_validatedG")) )
							 							  {
									 							if($grade<=$weight) 
															      {   
														     	     $average=$average+ $grade;
														     	    
														     	    $model_new->setAttribute('date_updated',date('Y-m-d H:i:s'));
															        $model_new->setAttribute('grade_value',$grade);	
															         $model_new->setAttribute('comment',$comment);
															         $model_new->setAttribute('update_by',currentUser());									     	   
															          if($model_new->save())
																		 {  
																		 	//if course is passed
														                  	   if( $grade >= $passingGrade )
														                  	     {  //it's done
														                  	          $command = Yii::$app->db->createCommand();
														                              $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
														                  	     
														                  	     	}
														                  	    else
														                  	      {  //it's done
														                  	          $command = Yii::$app->db->createCommand();
														                              $command->update('student_has_courses', ['is_pass' => 0, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$model_new->course])->execute(); 
														                  	     
														                  	     	}
																		 	
																		   unset($model_new);
																		   $model_new= new SrcGrades;
																		   $success_=1;
																		   $ok=true;
																		   $ok_update_validate=true;
																		   
																		 }

															       
															       }
															     else
																   $this->message_GradeHigherWeight=true;
		                                          
															   
															   		
							 							   }
							 							 else
							 							  {												         	
												             $url=Yii::$app->request->referrer;
					       	    							 
					       	    							 $update_validate=true;
					       	    							 $message_UpdateValidate=true;
					       	    							 $message_UpdatePublish=true;
					       	    							 
							 							   }
							 							   
							 							   
											          }
											}
						 			     
										
								if(($ok==true)||($ok_update_validate==true))	     
								  { $success_=1;
									         //------------ SUBJECT AVERAGE BY PERIOD  ------------//
		                            if($nbr_grade>0)
		                             {  $average=$average/$nbr_grade;
												 
					      	//save subject average for this period			  
									$command = Yii::$app->db->createCommand();
						   
         						   //check if already exit
								      $modelGra =  new SrcGrades();
									  $data =  $modelGra->checkDataSubjectAverage($course);
									  $is_present=false;
									       if($data)
										     $is_present=true;
										  
		                                if($is_present){// yes, update
										
										    $command->update('subject_average', ['average' => $average, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['course'=>$course])->execute();
		                                   }
										 else{// no, insert
										  			 $command->insert('subject_average', [
													    'academic_year'=>$acad,
												'course'=>$course,
												'average'=>$average,
												'create_by'=>currentUser(),
												'date_created'=>date('Y-m-d'),
												
													])->execute();
											
											
										     
										     }
                                    
										 
		                             }
									       
		
									      
									      
									   return $this->redirect (array('index?from=stud'));  //return $this->redirect (array('update?all=1&from=stud&mn=std&ok=yes&greater='.$message_GradeHigherWeight));
								  }
								if($update_validate)
								  { $message_UpdateValidate=true;
								      return $this->redirect (array('update?all=1&from=stud&mn=std&msguv=y'));
								   }
								else
								  return $this->redirect (array('index?from=stud'));
									
							
					
					
							  }
							else
							  {
							      
							      //$url=Yii::$app->request->referrer;
							      //$this->redirect($url.'&msguv=y');
							      Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','Validated Grades can\'t be updated.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							       
							       	 
							    }
				    
				   
				   
		              }
			        	
		              
			  
			  }    
		              
		        // if(isset($_POST['cancel']))      
		              	   
		    
		  }
		  
		  
		  
	  
	
	            return $this->render('update', [
	                'model' => $model,
	                'message_UpdateValidate'=>$message_UpdateValidate,
				       'message_UpdateValidate1'=>$message_UpdateValidate1,
				       'message_UpdatePublish'=>$message_UpdatePublish,
				       'message_noGrades'=>$message_noGrades,
				       'message_gradesOk'=>$message_gradesOk,
				       'messageNoCheck'=>$messageNoCheck,
				       'message_GradeHigherWeight'=>$message_GradeHigherWeight,
	            ]);


        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

  

public function _checkGradeDataByCourse($course)
	{    
		$gradeModel = new SrcGrades();
		$data= $gradeModel->checkGradeDataByCourse($course);
           
           if(isset($data)&&($data!=null))
           		return true;
           	else
           	   return false;
         
	}

public function _checkGradeDataByCourseGroup($course,$group)
	{    
		$gradeModel = new SrcGrades();
		$data= $gradeModel->checkGradeDataByCourseGroup($course,$group);
           
           if(isset($data)&&($data!=null))
           		return true;
           	else
           	   return false;
         
	}

	
public function _checkGradeDataByCourseGroup_delayed($course,$group)
	{    
		$gradeModel = new SrcGrades();
		$data= $gradeModel->checkGradeDataByCourseGroup_delayed($course,$group);
           
           if(isset($data)&&($data!=null))
           		return true;
           	else
           	   return false;
         
	}

public function _checkGradeDataByStudentCourse($student,$course)	
	{    
		$gradeModel = new SrcGrades();
		$data= $gradeModel->checkGradeDataByStudentCourse($student, $course);
           
           if(isset($data)&&($data!=null))
           		return true;
           	else
           	   return false;
         
	}
 


	 
  
public function actionListbyroom()
    {
        if(Yii::$app->user->can('fi-grades-listByRoom'))
         {
         	$acad = Yii::$app->session['currentId_academic_year'];
         	
         	$full_name = getPersonFullNameByUserId(Yii::$app->user->identity->id);
            if($full_name=='')
              {
              	  $full_name = "SIGES";
              	}      
	
	        $model = new SrcGrades();
	        
	        $modelCourses = new SrcCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	        $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0);
	        
	        $success=false;
	        $use_update=false;
	        $message_course_id=false;
	        $message_GradeHigherWeight=false;
	        $message_noGradeEntered=false;
			$no_stud_has_grade=true;
	        
	        
	        $program= 0;
	        $shift= 0;
	        $room = 0;
	        $course = 0;
	        $level = 0;
	        $course_weight ='';
                $fe_pdf_la= 1;
	
	      if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	            
	            if(isset($_POST['SrcGrades']['level']))
		              { $level= $_POST['SrcGrades']['level'];
		              Yii::$app->session['grades_level'] = $level;
		                 $model->level=[$level]; 
		                 
		                 Yii::$app->session['grades_program']= 0;
		                  $model->program=0;
		              }
		              
		        if(isset($_POST['SrcGrades']['program']))
	              { $program= $_POST['SrcGrades']['program'];
	              Yii::$app->session['grades_program'] = $program;
	                $model->program=[$program];
	               }
	             
	             

		             if(isset($_POST['SrcGrades']['shift']))
		              { $shift= $_POST['SrcGrades']['shift'];
		              Yii::$app->session['grades_shift'] = $shift;
		                 $model->shift=[$shift]; 
		         
		              }
		              
		             		              
		             if(isset($_POST['SrcGrades']['room']))
		              {  $room = $_POST['SrcGrades']['room'];
   		                  
		                   $model->room=[$room];
                                  
		              }
                              
                              if($room != Yii::$app->session['grades_group']) 
                                {    $fe_pdf_la= 0;
                                     Yii::$app->session['grades_group'] = $room;
                                     
                                }
                                  else
		                    $fe_pdf_la= 1;
		              
		            /* if(isset($_POST['SrcGrades']['course']))
		              {  $course = $_POST['SrcGrades']['course'];
		                 Yii::$app->session['grades_course'] = $course;
		                   $model->course=[$course];
		               }
		            */
	                 
	               if($program != Yii::$app->session['grades_program'])
	                 {  Yii::$app->session['grades_program']= $program;
	                    
	                     //$course = '';
	                     //$model->course=[$course];
	                     $room='';
	                     Yii::$app->session['grades_room'] = $room;
	                     $model->room=[$room];
	                     $shift='';
	                     Yii::$app->session['grades_shift']= $shift;
	                     $model->shift=[$shift];
	                     
	                  }
	                else
	                  {  
	                  	
	                  	if($level != Yii::$app->session['grades_level'])
			                 {  Yii::$app->session['grades_level']= $level;
			                    
			                     //$course = '';
			                     //$model->course=[$course];
			                     $room='';
			                     Yii::$app->session['grades_room'] = $room;
			                     $model->room=[$room];
			                     $shift='';
			                     Yii::$app->session['grades_shift']= $shift;
			                     $model->shift=[$shift];
			                     
			                  } 
	                  	//else
	                  	//   {
			                   if($shift != Yii::$app->session['grades_shift'])
				                 {  Yii::$app->session['grades_shift']= $shift;
				                    
				                    // $course = '';
				                     //$model->course=[$course];
				                     $room='';
				                     Yii::$app->session['grades_room'] = $room;
				                     $model->room=[$room];
				                     
				                  }
				                  
	                  	   // }
	                  	    
			          }
                      
	                  
	          
	           						   
	                if(isset($_POST['validate_publish']))
					{   //on vient de presser le bouton
									
						//$moduleGrade_ = new SrcGrades();
			
						//$dataProvider_c = $model->getAllSubjects($program,$shift,$room);
						$dataProvider_c = $model->getAllSubjectsByLevel($program,$shift,$level,$room);
						
						
						$k=0;	
						//tcheke si yo valide-publiye deja
						  while(isset($dataProvider_c[$k][0]))
				             {
	                           
	                           
	                           $careAbout=$model->isSubjectEvaluated($dataProvider_c[$k][0]);         
			  
			                   if($careAbout)
				                 {		
				                 	//valide tout not kou sa
				                 	$command = Yii::$app->db->createCommand();
				                 	
								    //$command->update('grades', ['validate' =>1,'publish'=>1, 'date_updated'=>date('Y-m-d'), 'date_validation'=>date('Y-m-d H:i:s'),'validated_by'=>$full_name,'update_by'=>currentUser()  ], ['course'=>$dataProvider_c[$k][0] ])
                                     //          ->execute();
									
									//valide kou sa pou tout student ki nan gwoup sa
									$group_students = $searchModelStudent->searchStudentsByProgramLevelShiftGroupAcad($program,$level,$shift,$room,$acad);		                          
									foreach($group_students as $stud)
									  {
									  	    $command->update('grades', ['validate' =>1,'publish'=>1, 'date_updated'=>date('Y-m-d'), 'date_validation'=>date('Y-m-d H:i:s'),'validated_by'=>$full_name,'update_by'=>currentUser()  ], ['student'=>$stud->student, 'course'=>$dataProvider_c[$k][0] ])
                                               ->execute();
									  	}  
           
                                     //tout kalkile wayenn kou sa
                               /*      $subject_average = null;
                                    //$model->AVGcourse($dataProvider_c[$k][0]);
                                    $average_course = $model->AVGcourseByGroup($dataProvider_c[$k][0],$shift,$level,$room);  
                                    
                                     foreach($average_course as $r)
							            $subject_average = $r->average;   
                    
                                     if($subject_average =!null)
                                      {
					                 	 $command1 = Yii::$app->db->createCommand();
									      //check if already exit
									       $data = $model->checkDataSubjectAverage($dataProvider_c[$k][0]);
									       
										  $is_present=false;
										       if($data)
											     $is_present=true;
											  
			                                if($is_present){// yes, update
											
											    $command1->update('subject_average', [
														'average'=>$subject_average,'date_updated'=>date('Y-m-d'),'update_by'=>currentUser(), ], ['course'=>$dataProvider_c[$k][0] ])
														->execute();
			                                   }
											 else{// no, insert
											   $command1->insert('subject_average', [
													'academic_year'=>$acad,
													'course'=>$dataProvider_c[$k][0],
													'average'=>$subject_average,
													'create_by'=>currentUser(),
													'date_created'=>date('Y-m-d'),
												]);
												
											     
											     }
                                         }
				                 	   */
				                 	  }
				                 
				                 $k++;
				                 
				               }	
							
							
							
					}
			elseif(isset($_POST['validate']))
			     {     //on vient de presser le bouton
									
						$dataProvider_c =  $model->getAllSubjectsByLevel($program,$shift,$level,$room);
						
						$k=0;	
						//tcheke si yo valide-publiye deja
						  while(isset($dataProvider_c[$k][0]))
				             {
	                           $careAbout=$model->isSubjectEvaluated($dataProvider_c[$k][0]);         
			  
			                   if($careAbout)
				                 {		
				                 	//valide tout not kou sa
				                 	$command = Yii::$app->db->createCommand();
				                 	
								    //$command->update('grades', ['validate' =>1, 'date_updated'=>date('Y-m-d'),'date_validation'=>date('Y-m-d H:i:s'),'validated_by'=>$full_name,'update_by'=>currentUser()  ], ['course'=>$dataProvider_c[$k][0] ])
                                     //          ->execute();
                                     
                                     //valide kou sa pou tout student ki nan gwoup sa
									$group_students = $searchModelStudent->searchStudentsByProgramLevelShiftGroupAcad($program,$level,$shift,$room,$acad);		                          
									foreach($group_students as $stud)
									  {
									  	    $command->update('grades', ['validate' =>1, 'date_updated'=>date('Y-m-d'), 'date_validation'=>date('Y-m-d H:i:s'),'validated_by'=>$full_name,'update_by'=>currentUser()  ], ['student'=>$stud->student, 'course'=>$dataProvider_c[$k][0] ])
                                               ->execute();
									  	}  
           

											                            
           
                                     //tout kalkile wayenn kou sa
                                  /*   $subject_average = null;
                                     
                                     //$average_course = $model->AVGcourse($dataProvider_c[$k][0]);
                                     $average_course = $model->AVGcourseByGroup($dataProvider_c[$k][0],$shift,$level,$room);
                                        
                                        foreach($average_course as $r)
							              $subject_average = $r->average;  
                                     
                                     if($subject_average =!null)
                                      {
					                 	 $command1 = Yii::$app->db->createCommand();
									      //check if already exit
									       $data = $model->checkDataSubjectAverage($dataProvider_c[$k][0]);
									       
										  $is_present=false;
										       if($data)
											     $is_present=true;
											  
			                                if($is_present){// yes, update
											
											    $command1->update('subject_average', [
														'average'=>$subject_average,'date_updated'=>date('Y-m-d'),'update_by'=>currentUser(), ], [ 'course'=>$dataProvider_c[$k][0] ])
														->execute();
			                                   }
											 else{// no, insert
											   $command1->insert('subject_average', [
													'academic_year'=>$acad,
													'course'=>$dataProvider_c[$k][0],
													'average'=>$subject_average,
													'create_by'=>currentUser(),
													'date_created'=>date('Y-m-d'),
												]);
												
											     
											     }
                                         }
				                 	    */
				                 	  }
				                 
				                 $k++;
				                 
				               }	
								
			     	
			     	
			     	}
	                 elseif(isset($_POST['viewPDF']))
	                  {
                             if( $fe_pdf_la== 1)
                                 {
                                    $programModel = SrcProgram::findOne($program);
                                    $shiftModel = SrcShifts::findOne($shift);
                                    $roomModel = SrcRooms::findOne($room);
                        
                                    $content = $this->renderPartial('_pdfListByRoom',['program'=>$program,'shift'=>$shift,'level'=>$level,'room'=>$room] );
                                    
                                    //$heading = pdfHeading();  //$this->renderPartial('_pdf_heading');
						   
                                        $pdf = new Pdf();
                                        $pdf->filename = Yii::t('app','Grade sheet').'_'.$programModel->label.'_'.$roomModel->room_name.'_'.$shiftModel->shift_name.'.pdf';
                                        $pdf->content = Pdf::MODE_CORE;
                                        $pdf->mode = Pdf::MODE_BLANK;
                                        //$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                                        $pdf->defaultFontSize = 10;
                                        $pdf->defaultFont = 'helvetica';
                                        $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER; //or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                                        $pdf->orientation =  Pdf::ORIENT_LANDSCAPE;  // Pdf::ORIENT_PORTRAIT; //or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                                        $pdf->destination = Pdf::DEST_DOWNLOAD; //or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F
                                    //  $pdf->cssInline =  $css;
                                        $pdf->marginLeft = 10;
                                        $pdf->marginTop = 10;
                                        $pdf->marginRight = 10;
                                        $pdf->marginBottom = 15;
                                     //   $pdf->marginHeader = ;
                                     //   $pdf->marginFooter = ;

                                        $pdf->content = $content;
                                        $pdf->options = ['title' => Yii::t('app','Grade sheet')];
                                        $pdf->methods = [
							    	'SetHeader'=>[''],//$heading,//[''Payroll Receipt''],
							    	'SetFooter'=>['{PAGENO}'],
						    	];
                                        
                                        $pdf->options = [
                                                'title' => Yii::t('app','Grade sheet'),
                                                'autoScriptToLang' => true,
                                                'ignore_invalid_utf8' => true,
                                                'tabSpaces' => 4
                                        ];

                                       // return the pdf output as per the destination setting
                                         return $pdf->render();
                       
                                  }
                                else
		                  $fe_pdf_la= 1;
                                
                           	
	                  }//fin isset($_POST['viewPDF'])
	            
                         
                         
	          
	            
	             
	            
	        } 
	        
	
	            return $this->render('listByRoom', [
	                'model' => $model,
                        'program'=>$program,
                        'shift'=>$shift,
                        'level'=>$level,
                        'room'=>$room,
	              
	            ]);
	            
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    

//------------------------- TranscriptNotes ------------------------------//
   public function actionTranscriptnotes()
	 {
	    if(Yii::$app->user->can('fi-grades-transcriptnotes'))
             {	
		$acad = Yii::$app->session['currentId_academic_year'];
                //previous acad
                $SrcAcademicPeriods = new SrcAcademicperiods;
                $previous= $SrcAcademicPeriods->getPreviousAcademicYear($acad);
                
                $message_course_id = false;
                
                $model = new SrcGrades();
	        
	        $modelCourses = new SrcCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	        $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0);
                
                if(isset($_GET['save']))
                 {
                    $this->transcriptItems = 2;
                    
                    
                   }
	        
	        $model->transcriptItems=$this->transcriptItems;
	        $level = 0;
                $i=0;
	        
                $fe_pdf_la= 1;
                
                if($model->transcriptItems==2)
                       {
                               $program= 0;
                               $shift= 0;
                               $room = 0;
                               $course = 0;
                               $is_save = 0;
                               $course_weight ='';

                               $set_room=0;

                               $success=false;
                               $use_update=false;
                               
                               
                               
                          }
	
	      if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	            
	            if(isset($_POST['SrcGrades']['transcriptItems']))
		              { $this->transcriptItems= $_POST['SrcGrades']['transcriptItems'];
		              
		                 $model->transcriptItems=$this->transcriptItems; 
		              }
		    
                     if($model->transcriptItems==2)  //delayed-Grades
                       {      
                           $course_weight =''; 
                           $program= 0;
                           $course = 0;
                           $room = 0;
                           $use_update=false;
                           $success=false;
                           $message_course_id = false;
                           $is_save=0;
                           
                           if(isset($_GET['save']))
                              {
                                  $program= Yii::$app->session['delayedgrades_program'];
                                  $shift= Yii::$app->session['delayedgrades_shift'];
                                   $room = Yii::$app->session['delayedgrades_room'];
                                   $level = Yii::$app->session['delayedgrades_level'];
                                   
                                   $model->program =$program;
                                    
                                   $model->shift =$shift;
                                   
                                   $model->room =$room;
                                   
                                   $model->level=$level;
                               
                               }
                                  
                            if(isset($_POST['SrcGrades']['level']))
                             { $level= $_POST['SrcGrades']['level'];
                                
                                $model->level=$level;
                               
                                if(Yii::$app->session['delayedgrades_level']!=$model->level)
                                { 
                                    $_POST['SrcGrades']['program']='';
                                    $model->program =$_POST['SrcGrades']['program'];
                                    $_POST['SrcGrades']['shift']='';
                                    $model->shift =$_POST['SrcGrades']['shift'];
                                    $_POST['SrcGrades']['room']='';
                                    $model->room =$_POST['SrcGrades']['room'];
                                    $_POST['SrcGrades']['course']='';
                                    $model->course =$_POST['SrcGrades']['course'];
                                    
                                    Yii::$app->session['delayedgrades_level'] = $model->level;
                                    
                                }
                                    

                              }
                              
                             if(isset($_POST['SrcGrades']['program']))
                                { $program= $_POST['SrcGrades']['program'];
                                  $model->program=$program;
                                  
                                  /* if(Yii::$app->session['delayedgrades_program']!=$model->program)
                                    { 
                                        $_POST['SrcGrades']['shift']='';
                                        $model->shift =$_POST['SrcGrades']['shift'];
                                        $_POST['SrcGrades']['room']='';
                                        $model->room =$_POST['SrcGrades']['room'];
                                        $_POST['SrcGrades']['course']='';
                                        $model->course =$_POST['SrcGrades']['course'];
                                     */
                                        Yii::$app->session['delayedgrades_program'] = $model->program;

                                    //}
                                }

		              
		             if(isset($_POST['SrcGrades']['shift']))
		              { $shift= $_POST['SrcGrades']['shift'];
		                 $model->shift=$shift; 
                                 
                                /* if(Yii::$app->session['delayedgrades_shift']!=$model->shift)
                                    { 
                                        $_POST['SrcGrades']['room']='';
                                        $model->room =$_POST['SrcGrades']['room'];
                                        $_POST['SrcGrades']['course']='';
                                        $model->course =$_POST['SrcGrades']['course'];
                                   */
                                        Yii::$app->session['delayedgrades_shift'] = $model->shift;

                                   // }
                                 
		         
		              }
		            
	          
		             if(isset($_POST['SrcGrades']['room']))
		              {  $room = $_POST['SrcGrades']['room'];
		                   $model->room=$room;
                                   
                                   
                                        Yii::$app->session['delayedgrades_room'] = $model->room;

                                   
		               
		              }
		              
		             if(isset($_POST['SrcGrades']['course']))
		              {  $course = $_POST['SrcGrades']['course'];
		                   $model->course=$course;
		               }
                          
                          
                          if(($program!='')&&($program!=0)&&($course!='')&&($course!=0))
			    {
                               
                              
                                Yii::$app->session['delayedgrades_course']= $course;
                                Yii::$app->session['delayedgrades_shift']=$shift;
                                Yii::$app->session['delayedgrades_level']=$level;
                                //Yii::$app->session['delayedgrades_group']=$room;  
                                Yii::$app->session['delayedgrades_program']= $program;
                            
                                if(infoGeneralConfig('course_shift')==0) //ka sa poko revize,li pa ok
			          {
                                    //tcheke si gen not deja pou kou sa
			             $use_update=false;
		                    
				       $check = $this->_checkGradeDataByCourseGroup_delayed($course,$room); //$check = $this->_checkGradeDataByCourse($course);
							
                                      if($check)
                                        { 
                                             $searchModelStudentGrade = new SrcGrades();

                                             //gad si gen student ki nan kou a ki poko gen not
                                               //$dataProvider= $searchModelStudent->searchStudentInCourseNotInGradeTable_withoutShift($program,$course,$acad);
                                             $dataProvider= $searchModelStudent->searchStudentInLevelGroupAndCourseNotInGradeTable_delayed($program,$level,$room,$course,$previous);
                                             
                                               if($dataProvider->getModels()==null)
                                                 { $use_update=true; 
                                                    Yii::$app->session['delayedgrades_course']  = $course; 
                                                $dataProvider = $searchModelStudentGrade->searchStudentsGradeForCourseByGroup_delayed($course,$room,$shift,$previous);   //searchStudentsGradeForCourse($course);
                                                 }

                                           }
                                       else			  	
                                        {  //$dataProvider = $searchModelStudent->searchStudentsInProgramInCourse($program,$course,$acad);
                                             $dataProvider = $searchModelStudent->searchStudentsInProgramInLevelGroupCourse_delayed($program,$level,$room,$course,$previous);
                                             
                                        }
                                  
                                    }
                                elseif(infoGeneralConfig('course_shift')==1)
			          {
                                    //tcheke si gen not deja pou kou sa
			              $use_update=false;
		                    
					 $check = $this->_checkGradeDataByCourseGroup_delayed($course,$room);  //$check = $this->_checkGradeDataByCourse($course);
							
                                        if($check)
                                          {    //pou kou ki nan tab grades lan ki gen kek student ki pa gen not
                                                $searchModelStudentGrade = new SrcGrades();

                                                //gad si gen student ki nan kou a ki poko gen not
                                                  //$dataProvider= $searchModelStudent->searchStudentInCourseNotInGradeTable($program,$shift,$course,$acad);
                                                
                                                $dataProvider= $searchModelStudent->searchStudentsInProgramInShiftLevelGroupAndCourse_delayed($program,$shift,$level,$room,$course,$previous);
                                         
                                                   
                                                   if($dataProvider->getModels()==null)
                                                     { $use_update=true; 
                                                        Yii::$app->session['delayedgrades_course']  = $course;
                                                      $dataProvider = $searchModelStudentGrade->searchStudentsGradeForCourseByGroup_delayed($course,$room,$shift,$previous);   //searchStudentsGradeForCourse($course),$shift,$previous;
                                                     
                                                      } 
                                          }
                                        else			  	
                                       { //$dataProvider = $searchModelStudent->searchStudentsInProgramInShiftAndCourse($program,$shift,$course,$acad);
                                         //$dataProvider = $searchModelStudent->searchStudentsInProgramInShiftLevelGroupAndCourse_delayed($program,$shift,$level,$room,$course,$previous);  
                                                if($model->is_particular_case==0)//pou kou ki pa nan tab grades lan ki ke yo poko met not
                                                  {   $dataProvider= $searchModelStudent->searchStudentInShiftLevelGroupAndCourseNotInGradeTable_delayed($program,$shift,$level,$room,$course,$previous);
                                         
                                   
                                                     }
                                                elseif($model->is_particular_case==1) //pou kou ki nan tab grades lan ki gen kek student ki pa gen not
                                                  {   $dataProvider= $searchModelStudent->searchStudentInShiftLevelGroupAndCourseInGradeTable_delayedPS($program,$shift,$level,$room,$course,$previous);
                                                       
                                                  
                                                  }
                                       }
                                  
                                    
                                  } 
                                
                             }
                           
                           
                            
                            if($course!=0)
                                { $course_weight = $modelCourses->getWeight($course);
                                   //Yii::$app->session['grades_course']  = $course;
                                }
                          
                        }
                     elseif($model->transcriptItems==3)    //bulletin
                       { 
                        
                            if(isset($_POST['SrcGrades']['academic_year']))
                             { $_acad= $_POST['SrcGrades']['academic_year'];

                               $model->academic_year=$_acad;
                               
                                if(Yii::$app->session['_academic_year_']!=$model->academic_year)
                                { 
                                    $_POST['SrcGrades']['student']='';
                                    $_POST['SrcGrades']['level']='';
                                    $model->previous_level_module =0;
                                   Yii::$app->session['_academic_year_'] = $model->academic_year;
                                    
                                }
                                
                              }
                              
                            if(isset($_POST['SrcGrades']['level']))
                             { $_level= $_POST['SrcGrades']['level'];

                                $model->level=$_level;
                                
                                if(Yii::$app->session['_level_']!=$model->level)
                                { 
                                    $_POST['SrcGrades']['student']='';
                                    $model->previous_level_module =0;
                                   Yii::$app->session['_level_'] = $model->level;
                                    
                                }
                                
                              }
                            
                              $model->previous_level_module =0;
                              
                              if(isset($_POST['SrcGrades']['previous_level_module']))
                             { $_previous_level_module= $_POST['SrcGrades']['previous_level_module'];

                               $model->previous_level_module=$_previous_level_module;
                              }
                              
                       }
                       
                      if(isset($_POST['SrcGrades']['student']))
	              { $student= $_POST['SrcGrades']['student'];
	              
	                 
                           $model->student=$student;
	               }
	             
		
		
		 	//Extract school name 
                                $school_name = infoGeneralConfig('school_name');
                        //Extract school address
                                  $school_address = infoGeneralConfig('school_address');
                         //Extract  email address 
                                   $school_email_address = infoGeneralConfig('school_email_address');
                        //Extract Phone Number
                                   $school_phone_number = infoGeneralConfig('school_phone_number');
                        //Extract school Director
                                         $school_director_name = infoGeneralConfig('school_director_name');
                                 $transcript_signature = $school_director_name;
							    //Extract transcript_note_text
						          $transcript_note_text = infoGeneralConfig('transcript_note_text');
								 //Extract transcript_footer_text
						          $transcript_footer_text = infoGeneralConfig('transcript_footer_text');
						          
						        $administration_signature_text = infoGeneralConfig('administration_signature_text');;
						          
						          
						  
								
								
										
			 $transcript_note = Yii::t('app',$transcript_note_text);
			 
								
								
			
		if($i==0)
		  {	$i++;
			$model->transcript_note_text=$transcript_note;
			$model->transcript_signature=$transcript_signature;
			$model->administration_signature_text=$administration_signature_text;
		  }
			
			
			 
	   if(isset($_POST['create']))
              {
				  //$model->attributes=$_POST['SrcGrades'];
				   
			$model->load(Yii::$app->request->post());  
			
                  if($this->transcriptItems == 2)   //delayed grade
                    {
                       $success=false;
		       $temwen=false;
			        
		       $message_GradeHigherWeight=false;	
					  
                            $weight = ' ';

                            $passingGrade = 0; 
                       
                          //pou evite li retounen save pou id_course li kenbe nan tanpon  
                            
                            if(isset($_POST['SrcGrades']['course']))
		              {  $course = $_POST['SrcGrades']['course'];
		                   $model->course=$course;
		               }
                            
                            


                          if($course!=0)
                           { 
                             $weight = $modelCourses->getWeight($course);

                             $passingGrade = $modelCourses->getPassingGrade($course);

                                  $ok=true;
                                    $model_new = new Grades();


                                                 $message_noGradeEntered=false;
                                                 $no_stud_has_grade=true;

                                      
                                 	if($model->is_particular_case==0)
                                          {
                                               foreach($_POST['id_stud'] as $id)
                                                   {   	   
                                                      if(isset($_POST['grades'][$id])&&($_POST['grades'][$id]!=''))
                                                           $no_stud_has_grade=false;

                                                   }


                                                         if($no_stud_has_grade==false) // omwen 1 etidyan gen not
                                                               {   foreach($_POST['id_stud'] as $id)
                                                                    {

                                                                        if(isset($_POST['grades'][$id]))
                                                                           $grade=$_POST['grades'][$id];
                                                                                   else
                                                                                    $grade=0;


                                                                       if(isset($_POST['comments'][$id]))
                                                                           $comment=$_POST['comments'][$id];
                                                                        else
                                                                            $comment='';

                                                                                      //check if grade is higher than the course weight
                                                                            if($grade <= $weight)  
                                                                              { 

                                                                                    $model_new->setAttribute('student',$id);
                                                                                    $model_new->setAttribute('course',$course);


                                                                                            $model_new->setAttribute('grade_value',$grade);
                                                                                                        $model_new->setAttribute('comment',$comment);
                                                                                            $model_new->setAttribute('validate',1);
                                                                                 $model_new->setAttribute('publish',1);
                                                                                 $model_new->date_created=date('Y-m-d H:i:s');
                                                                                 $model_new->create_by=currentUser();

                                                                                 if($model_new->save() )
                                                                                   { 
                                                                                          //if course is passed
                                                                                            if( $grade >= $passingGrade )
                                                                                              {  //it's done
                                                                                                   $command = Yii::$app->db->createCommand();
                                                                                               $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$course])->execute(); 

                                                                                                 } 

                                                                                         unset($model_new);
                                                                                      $model_new = new Grades();

                                                                                      $temwen=true;
                                                                                      $ok=true;
                                                                                      $success=true;

                                                                                         }

                                                                               }
                                                                            /* else
                                                                               {
                                                                                  unset($model_new);
                                                                                  $model_new = new Grades();

                                                                                  $message_GradeHigherWeight=true;

                                                                                }
                                                                             * 
                                                                             */
						            										   
								         } 
						             
                                                            }
                                                          else 
                                                           { $ok=false;
                                                             $message_noGradeEntered=true;
                                                               $model->program=0;
                                                           }
                                                     
                                                     
									
                                                        if($ok==true) 
                                                           {  
                                                                   //$dbTrans->commit();  	
                                                                   //  if($message_GradeHigherWeight==false)
                                                                    return $this->redirect(['transcriptnotes?save=ok', ]);
                                                                     $success=true;

                                                                   
                                                           }
                                                         else
                                                            { //   $dbTrans->rollback();
                                                            }
                                                            
                                                }
                                              elseif($model->is_particular_case==1)
                                               {
                                                 
				  
                                                    if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) || (Yii::$app->user->can("fi-grades-update_validatedG")) )			              
                                                      {  


                                                            $grade=0;
                                                             $comment='';
                                                             $average=0;
                                                            $nbr_grade=0;
                                                            $course = $model->course;
                                                            
                                                            $ok=true;
                                                             $model_new1 = new Grades();
                                                            
                                                            $grades_comment = infoGeneralConfig('grades_comment');
							
                                                        if($use_update==true)
                                                          {
                                                               foreach($_POST['id_grade'] as $grade_id)
                                                                 {
                                                                        $nbr_grade++;

                                                                     //if(isset($_POST['grades'][$grade_id]))
                                                                        $grade=$_POST['grades'][$grade_id];
                                                                                //else
                                                                                //	$grade='';


                                                                        if($grades_comment==1)//	if(isset($_POST['comments'][$grade_id]))
                                                                        $comment=$_POST['comments'][$grade_id];
                                                                        //	else
                                                                        //		$comment='';



                                                                           $model_new1=  $this->findModel($grade_id);
                                                                        
                                                                   //check if grade is higher than the course weight
                                                                           if($grade <= $weight)  
                                                                             { 
                                                                                $average=$average+ $grade;

                                                                                $model_new1->grade_value=$grade;
                                                                                            $model_new1->setAttribute('comment',$comment);
                                                                                $model_new1->date_updated=date('Y-m-d H:i:s');
                                                                                $model_new1->update_by=currentUser();

                                                                           }
                                                                            else
                                                                              {
                                                                                 $message_GradeHigherWeight=true;

                                                                               }
                                                                 
                                                                              
                                                                            if($model_new1->save() )
                                                                              { 
                                                                                //if course is passed
                                                                               if( $grade >= $passingGrade )
                                                                                 {  //it's done
                                                                                      $command = Yii::$app->db->createCommand();
                                                                                  $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new1->student, 'course'=>$model_new1->course])->execute(); 

                                                                                    } 
                                                                                else
                                                                                   {  //it's done
                                                                                      $command = Yii::$app->db->createCommand();
                                                                                  $command->update('student_has_courses', ['is_pass' => 0, 'date_updated'=>date('Y-m-d H:i:s'),'update_by'=>currentUser()  ], ['student'=>$model_new1->student, 'course'=>$model_new1->course])->execute(); 

                                                                                    }

                                                                                    //$dbTrans->commit(); 
                                                                                           // unset($model_new1);
                                                                                                               $model_new1= new Grades;
                                                                                                               $success=true;
                                                                                                               $ok=true;			                  	
                                                                                    } 
                                                                              else
                                                                                               { //   $dbTrans->rollback();
                                                                                               }
                                                                                               
                                                                             
                                                                        
                                                                         

                                                            
                                                                        } 
                                                                        
                                                                     }
                                                                   elseif($use_update==false)
                                                                     {
                                                                       foreach($_POST['id_stud'] as $id)
                                                                    {

                                                                        if(isset($_POST['grades'][$id]))
                                                                           $grade=$_POST['grades'][$id];
                                                                                   else
                                                                                    $grade=0;


                                                                       if(isset($_POST['comments'][$id]))
                                                                           $comment=$_POST['comments'][$id];
                                                                        else
                                                                            $comment='';

                                                                                      //check if grade is higher than the course weight
                                                                            if($grade <= $weight)  
                                                                              { 

                                                                                    $model_new->setAttribute('student',$id);
                                                                                    $model_new->setAttribute('course',$course);


                                                                                            $model_new->setAttribute('grade_value',$grade);
                                                                                                        $model_new->setAttribute('comment',$comment);
                                                                                            $model_new->setAttribute('validate',1);
                                                                                 $model_new->setAttribute('publish',1);
                                                                                 $model_new->date_created=date('Y-m-d H:i:s');
                                                                                 $model_new->create_by=currentUser();

                                                                                 if($model_new->save() )
                                                                                   { 
                                                                                     
                                                                                          //if course is passed
                                                                                            if( $grade >= $passingGrade )
                                                                                              {  //it's done
                                                                                                   $command = Yii::$app->db->createCommand();
                                                                                               $command->update('student_has_courses', ['is_pass' => 1, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$course])->execute(); 

                                                                                                 } 

                                                                                         unset($model_new);
                                                                                      $model_new = new Grades();

                                                                                      $temwen=true;
                                                                                      $ok=true;
                                                                                      $success=true;

                                                                                         }

                                                                               }
                                                                            /* else
                                                                               {
                                                                                  unset($model_new);
                                                                                  $model_new = new Grades();

                                                                                  $message_GradeHigherWeight=true;

                                                                                }
                                                                             * 
                                                                             */
						            										   
								         } 
                                                                     }
						             
						    		
                                                    if(($ok==true))	     
                                                      { $success=true;
                                                       $acad = Yii::$app->session['currentId_academic_year'];
                                                                     //------------- SUBJECT AVERAGE BY PERIOD  -------------//
				                             if($nbr_grade>0)
				                              {  $average=$average/$nbr_grade;
										 
				 			      	           //save subject average for this period			  
                                                               $command = Yii::$app->db->createCommand();



                                                               //check if already exit
                                                                  $modelGra =  new SrcGrades();
                                                                      $data =  $modelGra->checkDataSubjectAverage($course);
                                                                      $is_present=false;
                                                                           if($data)
                                                                                 $is_present=true;
								  
				                                 if($is_present){// yes, update
								
				 				$command->update('subject_average', ['average' => $average, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['course'=>$course])->execute();
				                                    }
                                                                 else{// no, insert

                                                                                         $command->insert('subject_average', [
                                                                                            'academic_year'=>$acad,
                                                                                'course'=>$course,
                                                                                'average'=>$average,
                                                                                'create_by'=>currentUser(),
                                                                                'date_created'=>date('Y-m-d'),

                                                                                        ])->execute();



                                                                     }
								 
				                              }
                                                              
                                                              return $this->redirect(['transcriptnotes?save=ok', ]);
							       

							      
							}
				 						
	                     
	                                            
	  
	   
				 		   }
                                                   
                                                   
                                               }
                                                            
                                       
                                                   
                                                   
                            }
                           else
                             { $message_course_id=true;
                               $model->program=0;
                             } 
                            
                                
                                        
                      }
                  elseif($this->transcriptItems == 3)    //bulletin
                    {
                        $modelGrade = new SrcGrades();
                        $program= 0;
			$shift= 0;
			$room = 0;
                        
                        $modelPerson = SrcPersons::findOne($model->student);
                        
                        $student_name= $modelPerson->first_name.'_'.$modelPerson->last_name;
                        
                        $program = $modelPerson->studentOtherInfo0[0]->applyForProgram->id;
                        //$modelPerson->studentOtherInfo0[0]->previousProgram->id; //le se specialisation,fe test sou program pou pran previous la
                        $shift = $modelPerson->studentOtherInfo0[0]->applyShift->id;
                        
                        $command = Yii::$app->db->createCommand('SELECT room FROM student_level_history WHERE level='.$model->level.' AND academic_year='.$model->academic_year)->queryAll();
                        if(($command!=null))
                          {
                            foreach($command as $result)
                              {
                                $room = $result['room'];
                              }
                          }
                        
                        
                        if($room!=0)
                         {  
                            $reportcard_format = infoGeneralConfig('reportcard_format');

                            $programModel = SrcProgram::findOne($program);
                            $shiftModel = SrcShifts::findOne($shift);
                            $roomModel = SrcRooms::findOne($room);

                            $form = '_pdfReportReportcard';
                            if($reportcard_format==1)
                              $form = '_pdfReportReportcard';
                            elseif($reportcard_format==2)
                                 $form = '_pdfReportReportcard1';

                            $content = $this->renderPartial($form, ['student' => $model->student,'level'=>$model->level, 'shift'=>$shift,'room'=>$room,'acad'=>$model->academic_year,'program'=>$program, 'modelGrade'=>$modelGrade]);

                            $heading = pdfHeading_reportcard();  //$this->renderPartial('_pdf_heading');

                            $pdf = new Pdf();


                            $pdf->filename = 'reportcard'.'_'.$student_name.'.pdf';


                            $pdf->content = Pdf::MODE_CORE;
                            $pdf->mode = Pdf::MODE_BLANK;
                            $pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
                            $pdf->defaultFontSize = 10;
                            $pdf->marginLeft = 6; 
                            $pdf->marginTop = 12; 
                            $pdf->marginRight = 6; 
                            $pdf->marginBottom = 2;
                            $pdf->defaultFont = 'helvetica';
                            $pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER;//or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
                            $pdf->orientation = Pdf::ORIENT_PORTRAIT;//or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
                            $pdf->destination = Pdf::DEST_DOWNLOAD;//or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F

                            $pdf->content = $content;
                            $pdf->options = ['title' => 'Grades Report'];
                            $pdf->methods = [
                                    'SetHeader'=>$heading,//[''Grades Report''],
                                    'SetFooter'=>[''], //['{PAGENO}'],
                            ];

                            $pdf->options = [
                                    'title' => 'Grades Report',
                                    'autoScriptToLang' => true,
                                    'ignore_invalid_utf8' => true,
                                    'tabSpaces' => 4
                            ];

                            // return the pdf output as per the destination setting
                            return $pdf->render();
                         }
                        
                        
                     }
                  elseif($this->transcriptItems == 0)    //relvenot
                      { 
                      	
                        if($model->header_text_date!='') 
                         {
                             $header_text_date = $model->header_text_date;
                            $transcript_note_text = $model->transcript_note_text;
                            $transcript_signature_n_title = $model->transcript_signature.'<br/>'.$model->administration_signature_text;

      
					           $content = $this->renderPartial('_pdfTranscriptNotes', ['student' => $student,'header_text_date'=>$header_text_date,'transcript_note_text'=>$transcript_note_text,'transcript_signature_n_title'=>$transcript_signature_n_title]);
					         
					           $heading = pdfHeading_reportcard();  //$heading = pdfHeading();  //$this->renderPartial('_pdf_heading');
                                                  
						    	
						    	$pdf = new Pdf();
	  
						    	
						    	$pdf->filename = Yii::t("app","Transcript of notes").'_'.'.pdf';
						    	
						    	 
						    	$pdf->content = Pdf::MODE_CORE;
						    	$pdf->mode = Pdf::MODE_BLANK;
						    	$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
						    	$pdf->defaultFontSize = 10;
                                                        $pdf->marginLeft = 6; 
                                                        $pdf->marginTop = 12; 
                                                        $pdf->marginRight = 6; 
                                                        $pdf->marginBottom = 2;
						    	$pdf->defaultFont = 'helvetica';
						    	$pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER;//or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
						    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;//or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
						    	$pdf->destination = Pdf::DEST_DOWNLOAD;//or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F
						         
						        $pdf->content = $content;
						    	$pdf->options = ['title' => 'Transcript of notes'];
						    	$pdf->methods = [
							    	'SetHeader'=>$heading,//[''Grades Report''],
							    	'SetFooter'=>['{PAGENO}'],
						    	];
						    	
						    	$pdf->options = [
							    	'title' => 'Grades Report',
							    	'autoScriptToLang' => true,
							    	'ignore_invalid_utf8' => true,
							    	'tabSpaces' => 4
						    	];
						 
						    	
						    	// return the pdf output as per the destination setting
						         return $pdf->render();
                                                         
                                 
                                    }
                              else
                                      {
                                             Yii::$app->getSession()->setFlash('warning', [
                                                                                               'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                                                                                               'duration' => 12000,
                                                                                               'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                                                                                               'message' => Html::encode(Yii::t('app','Add the date.') ),
                                                                                               'title' => Html::encode(Yii::t('app','Warning') ),
                                                                                               'positonY' => 'top',   //   top,//   bottom,//
                                                                                               'positonX' => 'center'    //   right,//   center,//  left,//
                                                                                           ]);

                                      }	

														   
                            }	
						   
                  
                          	
				
              }
              
               
                          
		
		
	 
              
              
         }

             if( in_array($this->transcriptItems,[0,1,3] ) )
                {
                    return $this->render('transcriptNotes', [
	                'model' => $model,
                        'transcriptItems'=>$this->transcriptItems,
                        
	              
	            ]);
                    
                  }
                elseif($this->transcriptItems==2)
                 {
                    return $this->render('transcriptNotes', [
	                'model' => $model,
                        'transcriptItems'=>$this->transcriptItems,
                        'dataProvider'=> $dataProvider,
                        'course_weight'=> $course_weight,
                        'use_update'=>$use_update,
                        'is_save'=>$is_save,
                        'success'=>$success,
                        'message_course_id'=>$message_course_id,
	              
	            ]);
                   }
                
             }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
                
	}
        
        

//------------------------- REPORTCARD ------------------------------//
 public function actionReportcard() {
    	
        $current_acad = Yii::$app->session['currentId_academic_year'];
        
        $model = new SrcStudentHasCourses();
        
        $modelGrade = new SrcGrades();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
                
    	        $program= 0;
	        $shift= 0;
	        $room = 0;
                $acad = 0;
	    $model->academic_year = $current_acad;
                
	           $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0);
	        
	
	       if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	            
	              
                   if(isset($_POST['SrcStudentHasCourses']['academic_year']))
		              {  $acad = $_POST['SrcStudentHasCourses']['academic_year'];
		                   $model->academic_year=[$acad];

		              }
                   
                   if(isset($_POST['SrcStudentHasCourses']['level']))
		              {  $level = $_POST['SrcStudentHasCourses']['level'];
		                   $model->level=[$level];

		              }
                
	                if(isset($_POST['SrcStudentHasCourses']['program']))
		              { $program= $_POST['SrcStudentHasCourses']['program'];
		                $model->program=[$program];
		               }
	             
		             if(isset($_POST['SrcStudentHasCourses']['shift']))
		              { $shift= $_POST['SrcStudentHasCourses']['shift'];
		                 $model->shift=[$shift]; 
		              }
		            
	               if(isset($_POST['SrcStudentHasCourses']['room']))
		              {  $room = $_POST['SrcStudentHasCourses']['room'];
		                   $model->room=[$room];
                                   
		              }
	               
	           /*     if( (Yii::$app->session['grades_program']!='')&&($program != Yii::$app->session['grades_program']) )
	                 {  Yii::$app->session['grades_program']= $program;
	                    
	                     $room='';
	                     Yii::$app->session['grades_group'] = $room;
	                     $model->room=[$room];
	                     $shift='';
	                     Yii::$app->session['grades_shift']= $shift;
	                     $model->shift=[$shift];
	                     
	                  }
	               else
	                  {  if( (Yii::$app->session['grades_shift']!='')&&($shift != Yii::$app->session['grades_shift']) )
			                 {  Yii::$app->session['grades_shift']= $shift;
			                    
			                    $room='';
			                     Yii::$app->session['grades_group'] = $room;
			                     $model->room=[$room];
			                     
			                  }
			          }
			          */
			             	
	            //$this->program = $model->program ;
	           
	           if(($program!='')&&($room!=''))
	             {
                       	$dataProvider = $searchModelStudent->searchStudentsByGroupForReportcard($program,$level,$shift,$room,$acad);
	              }
       
                     	
	           
	           if(isset($_POST['create']))
	            { 
                       $all_grades_not_validated=0;
                       
                       if(Yii::$app->session['reportcard_group'] != $room)
                         {
                           Yii::$app->session['reportcard_group'] = $room;
                          }
                       else
                         {
                        
                       $reportcard_format = infoGeneralConfig('reportcard_format');
	            
	                $programModel = SrcProgram::findOne($program);
	                $shiftModel = SrcShifts::findOne($shift);
	                $roomModel = SrcRooms::findOne($room);
	            	  
	            	$selection=(array)Yii::$app->request->post('selection');//typecasting
				       if($selection!=null)
					    {  
                                              //check at least one validated not from selected students
                                               foreach($selection as $id)
                                                 {
                                                       $student=(int)$id;
                                                  //check if all grades are validated for this stud
                                                   $modelGrades=new SrcGrades();
                                                   //1: tout not valide,  0: gen not ki pa valide
                                                        $not_all_validated=$modelGrades->if_all_grades_validated($student,$acad);
                                                       
                                                        if($not_all_validated==0)
                                                            $all_grades_not_validated=1;
                                                 }
                                                 
					        $form = '_reportcardDone1';
//					        if($reportcard_format==1)
//					          $form = '_reportcardDone1';
//					        elseif($reportcard_format==2)
//					             $form = '_reportcardDone2';
					        
					         $content = $this->renderPartial($form, ['studentsSelected' => $selection,'level'=>$level,'shift'=>$shift,'room'=>$room,'acad'=>$acad,'program'=>$program, 'modelGrade'=>$modelGrade]);
					         
					           
					       if($all_grades_not_validated==0 )
					        {      
					           
					           $heading = pdfHeading_reportcard();  //$this->renderPartial('_pdf_heading');
						    	
						    	$pdf = new Pdf();
	  
						    	
						    	$pdf->filename = 'reportcard'.'_'.$programModel->label.'_'.$roomModel->room_name.'_'.$shiftModel->shift_name.'.pdf';
						    	
						    	 
						    	$pdf->content = Pdf::MODE_CORE;
						    	$pdf->mode = Pdf::MODE_BLANK;
						    	$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
						    	$pdf->defaultFontSize = 10;
                                                        $pdf->marginLeft = 6; 
                                                        $pdf->marginTop = 12; 
                                                        $pdf->marginRight = 6; 
                                                        $pdf->marginBottom = 2;
						    	$pdf->defaultFont = 'helvetica';
						    	$pdf->format = Pdf::FORMAT_LETTER;//or Letter    //   Pdf::FORMAT_LEGAL;//or Legal     //    Pdf::FORMAT_FOLIO;//or Folio     //    Pdf::FORMAT_LEDGER;//or Ledger-L    //   Pdf::FORMAT_TABLOID;//or Tabloid    //array(216,285);
						    	$pdf->orientation = Pdf::ORIENT_PORTRAIT;//or P   //  Pdf::ORIENT_LANDSCAPE;//or L     //  
						    	$pdf->destination = Pdf::DEST_DOWNLOAD;//or D   //Pdf::DEST_BROWSER;//or I    // Pdf::DEST_FILE;//or F
						         
						        $pdf->content = $content;
						    	$pdf->options = ['title' => 'Grades Report'];
						    	$pdf->methods = [
							    	'SetHeader'=>$heading,//[''Grades Report''],
							    	'SetFooter'=>[''], //['{PAGENO}'],
						    	];
						    	
						    	$pdf->options = [
							    	'title' => 'Grades Report',
							    	'autoScriptToLang' => true,
							    	'ignore_invalid_utf8' => true,
							    	'tabSpaces' => 4
						    	];
						  /*  
						    if(Yii::$app->session['grades_validated']==false)
						       {
						       	    Yii::$app->session['grades_validated']=true;
						       	    
						       	    
						       	    Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						       	}
						      elseif(Yii::$app->session['grades_evaluated']==true)
						        {
						        	Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).') ),
										    'title' => Html::encode(Yii::t('app','Success') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						          }
						          */
						    	
						    	// return the pdf output as per the destination setting
						         return $pdf->render();
						         
						         
						         
					        }
					      elseif($all_grades_not_validated==1)
					        {     
					             
									
									Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app','Grades are not validated yet.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
					          }
				    	
					    }
					  else 
					    {
					    	Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',   // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You must check at least a student.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
					      }
                                              
                                 }
					    
				    	
	            }
    	
    	

            } 
	        
	
	            return $this->render('reportcard', [
	                'model' => $model,
	                'dataProvider'=>$dataProvider, 
	                
	            ]);
        
        
        
    	
    }
    
    
    

    
     
    /**
     * Deletes an existing Grades model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if( (Yii::$app->user->can('fi-grades-delete'))||( (Yii::$app->user->can('fi-grades-teacher_delete'))&&(Yii::$app->user->can('teacher'))&&(Yii::$app->session['profil_as']==1) ) )  
          {
           try{
	               
				if(Yii::$app->session['profil_as'] ==0)
				  {						 
	                $this->findModel($id)->delete();
	
	                
				  }
				 elseif(Yii::$app->session['profil_as'] ==1)  // se yon pwof
				   {
				   	    $model2delete= $this->findModel($id);
				   	    
				   	    if($model2delete->validate==1)
				   	     {
				   	           Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode( Yii::t('app','Validated Grades can\'t be updated.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   	     }
				   	    else
				   	       $model2delete->delete();
	
	               	}
	            
	             return $this->redirect(['index']);
	               	
	        
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Grades model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Grades the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcGrades::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
?>
