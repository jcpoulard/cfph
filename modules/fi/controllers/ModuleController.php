<?php

namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\Module;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\SrcSubjects;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * ModuleController implements the CRUD actions for Module model.
 */
class ModuleController extends Controller
{
    // Application du thème 
    public $layout = "/inspinia";


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Module models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('fi-module-index'))
         {

	        $searchModel = new SrcModule();
	        $model = new Module();
	        
	        //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
        if($program==null)
          {
          	  $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
          	  
          	  if(isset($_GET['SrcModule']))
		         $dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
		      		              	      
		              	 
          	  
          	}
         else //se yon responsab pwogram
           {
           	     $dataProvider = $searchModel->searchByProgram($program);
          	  
          	  if(isset($_GET['SrcModule']))
		         $dataProvider= $searchModel->searchByProgram($program);
			           	 

           	 } 
	        	          
	          
	
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	            'model'=>$model,
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single Module model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('fi-module-view'))
         {

	        return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Module model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         if(Yii::$app->user->can('fi-module-create'))
         {

	        $model = new SrcModule();
	        $modelSubject = new SrcSubjects();
	
	        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
	                 $model->setAttribute('date_created',date('Y-m-d'));
					//$model->setAttribute('create_by',user);
							
	               if ($model->save()) 
			          {   
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'mod']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	           
	        } 
	
	     
	            return $this->render('create', [
	                'model' => $model,
	                'modelSubject'=>$modelSubject,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
    
    
    /**
     * Creates a new Module model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatelist()
    {
         if(Yii::$app->user->can('fi-module-create'))
         {

	        $model = new SrcModule();
	        $modelSubject = new SrcSubjects();
                
                if(isset($_POST['create'])){
                    $is_save = FALSE;
                    $dbTrans = Yii::$app->db->beginTransaction(); 
                    if(isset($_POST['program'])){
                        for($i=1; $i<=30; $i++){
                            if((isset($_POST["subject$i"])&&(($_POST["subject$i"]!=NULL))) && (isset($_POST["code$i"])&&(($_POST["code$i"]!=NULL)) ) && (isset($_POST["duration$i"])&&(($_POST["duration$i"]!=NULL)) ) ){
                                $model = new SrcModule();
                                $model->program = $_POST['program'];
                                $model->subject = $_POST["subject$i"];
                                $model->code = $_POST["code$i"]; 
                                $model->duration = $_POST["duration$i"];
                                $model->date_created = date('Y-m-d');
                                if($model->save())
                                      $is_save = TRUE;
                                else
                                    $is_save = FALSE;
                            }
                            }
                            if(!$is_save){
                                $dbTrans->rollback();
                                
                            }
                            else{
                                $dbTrans->commit();
                                return $this->redirect(['index','wh'=>'mod']);
                            }
                        }
                    
                    
                }
                return $this->render('createlist', [
	                'model' => $model,
	                'modelSubject'=>$modelSubject,
	            ]);
        
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing Module model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
         if(Yii::$app->user->can('fi-module-update'))
         {
         	$modelSubject = new SrcSubjects();

	        $model = $this->findModel($id);
	
	        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
	                 $model->setAttribute('date_updated',date('Y-m-d'));
					//$model->setAttribute('update_by',user);
							
	               if ($model->save()) 
			          {   
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'mod']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        } 
	
	
	            return $this->render('update', [
	                'model' => $model,
	                'modelSubject'=>$modelSubject,
	            ]);
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Deletes an existing Module model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         if(Yii::$app->user->can('fi-module-delete'))
         {
            try{
	            	
		        $this->findModel($id)->delete();
		
		        return $this->redirect(['index','wh'=>'mod']);
             
              } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the Module model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Module the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcModule::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
