<?php

namespace app\modules\fi\controllers;

use Yii;
use app\modules\fi\models\MargeEchec;
use app\modules\fi\models\SrcMargeEchec;
use app\modules\fi\models\SrcProgram;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MargeechecController implements the CRUD actions for MargeEchec model.
 */
class MargeechecController extends Controller
{
   public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MargeEchec models.
     * @return mixed
     */
    public function actionIndex()
    {
         if(Yii::$app->user->can('fi-margeechec-index'))
         {
           $acad=Yii::$app->session['currentId_academic_year'];
                $searchModel = new SrcMargeEchec();
               $dataProvider = $searchModel->searchByAcad($acad);  //search(Yii::$app->request->queryParams);

               return $this->render('index', [
                   'searchModel' => $searchModel,
                   'dataProvider' => $dataProvider,
               ]);
               
                }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Displays a single MargeEchec model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MargeEchec model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        if(Yii::$app->user->can('fi-margeechec-create'))
         {
            $acad = Yii::$app->session['currentId_academic_year'];
            
	        $model = new SrcMargeEchec();
	        
	       if ($model->load(Yii::$app->request->post()) ) 
	        {
                   if(isset($_POST['SrcMargeEchec']['all_programs']) )
                   {   
                       $model->all_programs=$_POST['SrcMargeEchec']['all_programs'];
                    }
	            
	           if(isset($_POST['create']))
	            {
                       if($model->all_programs==0) 
                        {
                           $model->setAttribute('academic_year',$acad);
                            if($model->save()) 
			       return $this->redirect(['index']);
                        }
                       elseif($model->all_programs==1)
                       {  
                           $dbTrans = Yii::$app->db->beginTransaction(); 
                           $pass=true;
                           
                           $modelProgram = SrcProgram::find()->all();
                           foreach($modelProgram as $progr)
                           {
                               $model_new = new SrcMargeEchec();
                               $model_new->setAttribute('program',$progr->id);
                               $model_new->setAttribute('academic_year',$acad);
                               $model_new->setAttribute('quantite_module',$model->quantite_module);
                               $model_new->setAttribute('borne_sup',$model->borne_sup);
                               
                               if($model_new->save())
                                {}
                               else {
                                     $pass=false;
                                  }
			            
                           }
                           
                           if($pass==true)
                             {
                               $dbTrans->commit(); 
                               return $this->redirect(['index']);
                             }
                            else {
                                 $dbTrans->rollback();
                            }
                       }
                           
                    }
                    
                }
        
            return $this->render('create', [
                'model' => $model,
            ]);
            
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
      
          
    }

    /**
     * Updates an existing MargeEchec model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('fi-margeechec-update'))
         {
            $acad = Yii::$app->session['currentId_academic_year'];
            
	        $model = $this->findModel($id);
	        
	       if ($model->load(Yii::$app->request->post()) ) 
	        {
                    
	           if(isset($_POST['update']))
	            {
                       
                           $model->setAttribute('academic_year',$acad);
                            if($model->save()) 
			       return $this->redirect(['index']);
                           
                    }
                    
                }
        
            return $this->render('update', [
                'model' => $model,
            ]);
             

        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing MargeEchec model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         if(Yii::$app->user->can('fi-margeechec-create'))
         {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Finds the MargeEchec model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MargeEchec the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcMargeEchec::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
