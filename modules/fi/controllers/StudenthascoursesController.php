<?php

namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\StudentHasCourses;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcGrades;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
 
use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * StudenthascoursesController implements the CRUD actions for StudentHasCourses model.
 */
class StudenthascoursesController extends Controller
{
    public $layout = "/inspinia";
    
    
    public $program;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentHasCourses models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('fi-studenthascourses-index'))
         {
        	
	        	 $searchModel = new SrcStudentHasCourses();
	        	     
        //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
        if($program==null)
          {
          	  $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
          	  
          	  if(isset($_GET['SrcStudentHasCourses']))
		         $dataProvider= $searchModel->searchGlobal(Yii::$app->request->queryParams);
		      		              	      
		              	 
          	  
          	}
         else //se yon responsab pwogram
           {
           	     $dataProvider = $searchModel->searchStudentsInProgram($program);
          	  
          	  if(isset($_GET['SrcStudentHasCourses']))
		         $dataProvider= $searchModel->searchStudentsInProgram($program);
			           	 

           	 } 


	 
	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);
          
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }

    /**
     * Displays a single StudentHasCourses model.
     * @param integer $student
     * @param integer $course
     * @return mixed
     */
    public function actionView($id)
    {
         if(Yii::$app->user->can('fi-studenthascourses-view'))
         {
	           return $this->render('view', [
	            'model' => $this->findModel($id),
	        ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new StudentHasCourses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
         if(Yii::$app->user->can('fi-studenthascourses-create'))
         {

	        $acad = Yii::$app->session['currentId_academic_year'];

	        $model = new SrcStudentHasCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	         $program= 0;
	        $shift= 0;
	        $room = 0;
	         $level = 0;
	        $course = 0;
	        
	        $programs_leaders = 0;

	          //return program_id or null value
             $program = isProgramsLeaders(Yii::$app->user->identity->id);
		     if($program==null)
		       {
			       //$dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0); //pou le kou nan tri a
	       
	             	      $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift(0,0,0); 
		       }
		      else
		        {   $programs_leaders = 1;
		            $model->program=$program; 
		        	 $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift($program,0,0); 
		        }
	        
	        

          	       	        
	
	       if ($model->load(Yii::$app->request->post()) ) 
	        {
	             
	            if($programs_leaders ==1)
	               $model->program=$program;
	            else
	            {
	              if(isset($_POST['SrcStudentHasCourses']['program']))
	              { $program= $_POST['SrcStudentHasCourses']['program'];
	                $model->program=$program;
	               }
	            }
	             
		            if(isset($_POST['SrcStudentHasCourses']['level']))
	              { $level= $_POST['SrcStudentHasCourses']['level'];
	                $model->level=$level;
	               }
	             
		             if(isset($_POST['SrcStudentHasCourses']['shift']))
		              { $shift= $_POST['SrcStudentHasCourses']['shift'];
		                 $model->shift=$shift; 
		              }
		              
		           /*  if(isset($_POST['SrcStudentHasCourses']['room']))
		              {  $room = $_POST['SrcStudentHasCourses']['room'];
		                   $model->room=$room;
		              }
		          */ 
		             if(isset($_POST['SrcStudentHasCourses']['course']))
		              {  $course = $_POST['SrcStudentHasCourses']['course'];
		                   $model->course=$course;
		               }
	                
	               
	               if($program != Yii::$app->session['studentHasCourses_program'])
	                 {  Yii::$app->session['studentHasCourses_program']= $program;
	                    
	                     $course = '';
	                     $model->course=$course;
	                   /*  $room='';
	                     Yii::$app->session['studentHasCourses_room'] = $room;
	                     $model->room=$room;
	                    */
	                     $shift='';
	                     Yii::$app->session['studentHasCourses_shift']= $shift;
	                     $model->shift=$shift;
	                     $level='';
	                     Yii::$app->session['studentHasCourses_level'] = $level;
	                     $model->level=$level;
	                     
	                  }
	                else
	                  {  
	                  	if($level != Yii::$app->session['studentHasCourses_level'])
			                 {  Yii::$app->session['studentHasCourses_level']= $level;
			                    
			                     $course = '';
			                     $model->course=$course;
			                    /* $room='';
			                     Yii::$app->session['studentHasCourses_room'] = $room;
			                     $model->room=$room;
			                    */
			                     $shift='';
			                     Yii::$app->session['studentHasCourses_shift']= $shift;
			                     $model->shift=$shift;
			                     
			                  }
	                  	else
	                  	   {
			                  	if($shift != Yii::$app->session['studentHasCourses_shift'])
					                 {  Yii::$app->session['studentHasCourses_shift']= $shift;
					                    
					                     $course = '';
					                     $model->course=$course;
					                    /* $room='';
					                     Yii::$app->session['studentHasCourses_room'] = $room;
					                     $model->room=$room;
					                     */
					                  }
					                  
	                  	   }
	                  	   
			          }
			             	
	           if($programs_leaders ==0)
	            {
	               //$dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0);  //pou le kou nan tri a
	               $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift(0,0,0);
	              } 
	           else
	             {
	             	$dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift($program,0,0); 
	             	}
	           Yii::$app->session['studentHasCourses_level'] = $level;
	          // Yii::$app->session['studentHasCourses_room'] = $room;
	           Yii::$app->session['studentHasCourses_shift']= $shift;
	            //$this->program = $model->program ;
	             
	          if(($program!='')&&($level=='')&&($course==''))//&&($room=='')) //
	             {
	             	 //$dataProvider = $searchModelStudent->searchStudentsInProgramLevel($program,0); //pou le kou nan tri a
	             	 $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift($program,0,0);
	              }
	           elseif(($program!='')&&($level!='')&&($course==''))//&&($room=='')) //
	             {
	             	 //$dataProvider = $searchModelStudent->searchStudentsInProgramLevel($program,$level);  //pou le kou nan tri a
	             	if($shift==0)
	             	   $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift($program,$level,0);
	             	elseif($shift!=0)
	             	   $dataProvider = $searchModelStudent->searchStudentsInProgramLevelShift($program,$level,$shift);
	             	   
	              }
	            elseif(($program!='')&&($course!=''))//&&($room!='')) //
	             {
	             	 if(infoGeneralConfig('course_shift')==0)
	             	   { // le kou nan tri a 
	             	       if($model->past_courses==0)
                                   $dataProvider = $searchModelStudent->searchStudentsInProgramNotInCourse($program,$level,$course,$acad);//pa tenu kont de shift la,yon kou mw pran nan maten fok mw pa jwenn li nan apre midi
	             	        elseif($model->past_courses==1)
                                   $dataProvider = $searchModelStudent->searchStudentsInProgramNotInCourse($program,$level,$course,$model->academic_year);//pa tenu kont de shift la,yon kou mw pran nan maten fok mw pa jwenn li nan apre midi
	             	        
	             	      //le tri a rive nan sal
	             	      // $dataProvider = $searchModelStudent->searchStudentsInProgramNotInRoom($program,$level,$romm,$acad);
	             	   }
	             	elseif(infoGeneralConfig('course_shift')==1)
	             	   { // le kou nan tri a 
	             	       if($model->past_courses==0)
                                   $dataProvider = $searchModelStudent->searchStudentsInProgramInShiftNotInCourse($program,$level,$shift,$course,$acad); //tenu kont de shift la. yon kou mw pran nan maten fok mw pa jwenn li nan apre midi
                               elseif($model->past_courses==1)
                                  $dataProvider = $searchModelStudent->searchStudentsInProgramInShiftNotInCourse($program,$level,$shift,$course,$model->academic_year); //tenu kont de shift la. yon kou mw pran nan maten fok mw pa jwenn li nan apre midi
                                
	             	       //le tri a rive nan sal
	             	      // $dataProvider = $searchModelStudent->searchStudentsInProgramInShiftNotInRoom($program,$level,$shift,$room,$acad); 
	             	   }
	              }

	           
	           if(isset($_POST['create']))
	            {
	                  $dbTrans = Yii::$app->db->beginTransaction(); 
	                  
	                   $ok=true;
	                    $model_new = new SrcStudentHasCourses();
	                   
	                   					
					  //$action=Yii::$app->request->post('action');
				       $selection=(array)Yii::$app->request->post('selection');//typecasting
				       if($selection!=null)
					    {  
					    	//gade si gen elev ki pran kou nan sal sa deja, bay elev yo kou sa selman. si poko genyen bay elev yo tout kou ki gen nan sal sa
	                    //$student_in_room = $model_new->searchStudentsInProgramLevelShiftAndRoom($program,$level,$shift,$room);
	                    
					           
					          //chache tout kou ki nan sal sa pou program sa
					         /*   $query_course = SrcCourses::find()
	                            ->joinWith(['module0'])
	                            ->select('courses.id')
	                            ->where('room='.$room)
	                            ->andWhere('shift='.$shift)
	                            ->andWhere('module.program='.$program)
	                            ->andWhere('academic_year='.$acad)
	                            ->all();
	                            */
	                            
					    	 foreach($selection as $id)
					         {
					           /*  if($student_in_room->getModels()==null)
							      {
							      	  foreach($query_course as $course)
					                   { 
						              	    $model_new->student=(int)$id;
						              	 
						              	    $model_new->course= $course->id;
						            
							                $model_new->is_pass=0;
							                $model_new->date_created=date('Y-m-d');
							                $model_new->create_by=currentUser();
				
							                if($model_new->save() )
							                  { unset($model_new);
							                     $model_new = new SrcStudentHasCourses();
							                  	
							                  	}
							                 else
							                    {   
							                    	$ok=false;
							                      }
						                      
					                     }
					              
							      	}
							     else
							       {   */
								       	 //pou le kou nan tri a
						                $model_new->course=(int)$course;
						                $model_new->student=(int)$id;
					     
						                $model_new->is_pass=0;
						                $model_new->date_created=date('Y-m-d');
						                $model_new->create_by=currentUser();
			
						                if($model_new->save() )
						                  { if($model->past_courses==1)//mete -1 kom not nan tab grade pou kou a pou li ka paret nan repriz
                                                                        {
                                                                            $command = Yii::$app->db->createCommand();
                                                                               $command->insert('grades', [
                                                                           
                                                                                                            'student'=>$model_new->student,
                                                                                                            'course'=>$model_new->course,
                                                                                                            'grade_value'=>-1,
                                                                                                            'validate'=> 0,
                                                                                                            'publish' => 0,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();

                                                                        }
                                                                    unset($model_new);
						                     $model_new = new SrcStudentHasCourses();
						                  	
						                  	}
						                 else
						                    {   
						                    	$ok=false;
						                      }
							       //  }
		                                    
                                   
					          
					           
					           
					        /*   //le tri a rive nan sal
					            foreach($query_course as $course)
					              { 
					              	 $model_new->student=(int)$id;
					              	 
					              	    $model_new->course= $course->id;
					            
						                $model_new->is_pass=0;
						                $model_new->date_created=date('Y-m-d');
						                $model_new->create_by=currentUser();
			
						                if($model_new->save() )
						                  { unset($model_new);
						                     $model_new = new StudentHasCourses();
						                  	
						                  	}
						                 else
						                    {   
						                    	$ok=false;
						                      }
						                      
					              }
					              */
				                      
					           } 
					             
					      }
					    else 
					      {   $ok=false;
					          Yii::$app->getSession()->setFlash('info', [
												    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Please, select at least one student.') ),
												    'title' => Html::encode(Yii::t('app','Info') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
					      }
							
	               if ($ok==true) 
			          {   
			               $dbTrans->commit();  	
			                return $this->redirect(['index', ]);
			          }
			        else
			           {    $dbTrans->rollback();
			           }
	               
	            }
	            
	             
	        } 
	        
	        
	          return $this->render('create', [
	                'model' => $model,
	                'dataProvider'=>$dataProvider, 
	            ]);
        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Updates an existing StudentHasCourses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('fi-studenthascourses-update'))
         {
         	$acad = Yii::$app->session['currentId_academic_year'];
         	
         	$program= 0;
	        $shift= 0;
	        $room = 0;
	        $level = 0;
	        $course = 0;
	        $student=0;
	        $all_courses =[];
           
           if(isset($_GET['from'])&&($_GET['from']=='list'))
             { 
             	$model = new SrcStudentHasCourses();
             	$student = $_GET['id'];
                $model->student = $student;
             }
           else
	         { $model = $this->findModel($id);
	        
		        $program= $model->course0->module0->program;
		        $shift= $model->course0->shift;
		        $room = $model->student0->studentLevel->room;
		        $level = $model->getLevelByStudentId($model->student);
		        $course =  $model->course0->id;
		        
		        $all_courses = SrcCourses::find()
	                            ->joinWith(['module0'])
	                            ->select('courses.id')
	                            ->where('room='.$room)
	                            ->andWhere('shift='.$shift)
	                            ->andWhere('module.program='.$program)
	                            ->andWhere('academic_year='.$acad)
	                            ->all();
	         }
	        
	            $model->program=$program;
	              
	             $model->shift=$shift; 
	             
	              $model->room=$room;
	              
	               $model->level=$level;
	               
	               $model->course=$course;
	
	      if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	              if(isset($_POST['SrcStudentHasCourses']['program']))
	              $program= $_POST['SrcStudentHasCourses']['program'];
	              
	             if(isset($_POST['SrcStudentHasCourses']['shift']))
	              $shift= $_POST['SrcStudentHasCourses']['shift'];
	              
	             if(isset($_POST['SrcStudentHasCourses']['room']))
	              $room = $_POST['SrcStudentHasCourses']['room'];
	              
	             if(isset($_POST['SrcStudentHasCourses']['course']))
	              $course = $_POST['SrcStudentHasCourses']['course'];
	              
	              
	             $model->program=$program;
	              
	             $model->shift=$shift; 
	             
	              $model->room=$room;
	              
	               $model->course=$course;
	            
	             //$this->program = $model->program ;
	             
	            
	          if(isset($_GET['from'])&&($_GET['from']=='list'))
	            {  
	            	if(isset($_POST['create']))
	                 {
	                   
	                  
	                          $model_new = new SrcStudentHasCourses();
	                           
	                           $model_new->course=(int)$course;
					            $model_new->student=$student;
					            
				                $model_new->is_pass=0;
				                $model_new->date_created=date('Y-m-d');	
				                $model_new->create_by=currentUser();
				                
				                 if ($model_new->save()) 
						          {   
						               //$dbTrans->commit();  	
						                return $this->redirect(['persons/moredetailsstudent?id='.$student.'&is_stud=1', ]);
						          }
						        else
						           { //   $dbTrans->rollback();
						           }
	                 }
	            	
	            }
	           else
	           {
	           if(isset($_POST['update']))
	            {
	                    //pou le kou nan tri a
	                      $model->setAttribute('course',(int)$course);
		                   $model->setAttribute('date_updated',date('Y-m-d'));
						   $model->setAttribute('update_by',currentUser());
	                   
							
			               if ($model->save()) 
					          {   
					               //$dbTrans->commit();  	
					                return $this->redirect(['index', ]);
					          }
					        else
					           { //   $dbTrans->rollback();
					           }
					           
					 
					 /*     //le tri a rive nan sal
					   $dbTrans = Yii::$app->db->beginTransaction(); 
					     $ok=true;
					      //delete kou li te genyen nan lot sal la
					       foreach($all_courses as $course)
					         { $command2 = Yii::$app->db->createCommand();
						        $command2->delete('student_has_courses',[ 'and', ['student'=>$_GET['id']], ['course'=>$course->id ] ])->execute();
							        
					         }
							        
							  $model_new = new StudentHasCourses();
					      
					      //chache tout kou ki nan sal sa pou program sa
					            $query_course = SrcCourses::find()
	                            ->joinWith(['module0'])
	                            ->select('courses.id')
	                            ->where('room='.$room)
	                            ->andWhere('shift='.$shift)
	                            ->andWhere('module.program='.$program)
	                            ->andWhere('academic_year='.$acad)
	                            ->all();
	                            
					    	    foreach($query_course as $course)
					              { 
					              	 $model_new->student = $_GET['id'];
					              	 
					              	    $model_new->course= $course->id;
					            
						                $model_new->is_pass=0;
						                $model_new->date_created=date('Y-m-d');
						                $model_new->create_by=currentUser();
			
						                if($model_new->save() )
						                  { unset($model_new);
						                     $model_new = new StudentHasCourses();
						                  	
						                  	}
						                 else
						                    {   
						                    	$ok=false;
						                      }
						                      
					              }
                            
			           
					           if ($ok==true) 
					          {   
					               $dbTrans->commit();  	
					                return $this->redirect(['index', ]);
					          }
					        else
					           {    $dbTrans->rollback();
					                Yii::$app->getSession()->setFlash('warning', [
												    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','A problem occured during the process and cannot be saved.') ),
												    'title' => Html::encode(Yii::t('app','Warning') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
					           }
					           
					        */
			           
			       }
	               
	            }
	            
	            
	        } 
	
	            return $this->render('update', [
	                'model' => $model,
	            ]);
         
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    
public function actionCreatesolo($id)
    {
        if(Yii::$app->user->can('fi-studenthascourses-createsolo'))
         {
         	$acad = Yii::$app->session['currentId_academic_year'];
         	Yii::$app->session['alreadyHasCourses']= false;
         	
         	$shift_name= '';
         	
         	$program= 0;
	        $shift= 0;
	        $old_shift= 0;
	        $room = 0;
	        $level = 0;
	        $course = 0;
	        $student=$id;
	        
	         $modelStudent=SrcPersons::findOne($student );
	         
	        $searchModelCourse = new SrcStudentHasCourses();
	       
                $model = new SrcStudentHasCourses();
	        //tout kou ki nan program sa
            //$dataProvider = $searchModelCourse->searchCoursesInProgramShiftRoomForStudentToAdd($student,NULL,NULL,NULL);
            if($model->past_courses==0)
                $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,NULL,NULL,NULL,$acad);
             elseif($model->past_courses==1)
                  $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,NULL,NULL,NULL,$model->academic_year);
                     
	       
	        
	        //gad si stud sa gen kou pou ane a
	        $modelStud = SrcStudentHasCourses::find()->joinWith(['course0','student0.studentLevel'])->where('student='.$student)->orderBy(['courses.academic_year'=>SORT_DESC])->all();
	        if($modelStud!=null)
	         {   
	         	foreach($modelStud as $modelS)
	         	 {
	         	   $program= $modelS->course0->module0->program;
		           $shift= $modelS->course0->shift;
		          // $room = $modelS->course0->room;
		           
		          /* if( isset($modelS->course0->students[0]->studentLevel->room) )
		             $room = $modelS->course0->students[0]->studentLevel->room;
		            */ 
		           if( isset($modelS->course0->students[0]->studentLevel->level) )
		             $level = $modelS->course0->students[0]->studentLevel->level;
		           
		           $shift_name= $modelS->course0->shift0->shift_name; 
		           $old_shift= $shift;
		            //Yii::$app->session['studentHasCourses_room'] = $room;
	                Yii::$app->session['studentHasCourses_shift']= $shift;
	                
	                Yii::$app->session['alreadyHasCourses']= true;
		           
		           //tout kou ki nan program - shift - room sa li poko pran
                      if($model->past_courses==0)
                          $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,$shift,$level,$acad);
		       elseif($model->past_courses==1)
                          $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,$shift,$level,$model->academic_year);
		          
                      break;
	         	  }
	      //   print_r('<br/><br/><br/>****************************************************************************A');   	
	          }
	         else
	          {    //cheche pwogram li enskri ladan nan "student_other_info"
	          	   
	          	     
	          	    if(isset($modelStudent->studentOtherInfo0[0]->apply_for_program)) 
                      $program = $modelStudent->studentOtherInfo0[0]->applyForProgram->id;
                      
                    if(isset($modelStudent->studentOtherInfo0[0]->apply_shift)) 
                      $shift = $modelStudent->studentOtherInfo0[0]->apply_shift;
                      
                     if(isset($modelStudent->studentLevel->level)) 
                      $level = $modelStudent->studentLevel->level;
 //print_r('<br/><br/><br/>******************************'.$level.'**********'.$shift.'*****************'.$program.'*******************B');                      
                      //tout kou ki nan program sa
                      //$dataProvider = $searchModelCourse->searchCoursesInProgramShiftRoomForStudentToAdd($student,$program,NULL,NULL);
                      if($model->past_courses==0)
                          $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,NULL,NULL,$acad);
                      elseif($model->past_courses==0)
                        $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,NULL,NULL,$model->academic_year);
                                                             
                     //  unset(Yii::$app->session['studentHasCourses_room'] );
	                   unset( Yii::$app->session['studentHasCourses_shift'] );
	          	}
	        
	      	          	
	      if($level!=0)
	       { 
	       	
	       	 if($shift!=0)
	          {  
	            $model->program=$program;
	            
	            $model->level=$level;
	              
	             $model->shift=$shift; 
	             
	            //  $model->room=$room;
	              
	              // $model->course=$course;
	 
	      if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	                 //if(isset($_POST['StudentHasCourses']['program']))
	                      //$program= $_POST['StudentHasCourses']['program'];
	 if( (isset(Yii::$app->session['alreadyHasCourses'])&&(Yii::$app->session['alreadyHasCourses']==true))&&($old_shift!=$model->shift) )
	             { 
	             	//elev sa t gentan nan vakasyon deja, verifye setting sistem lan pou chanjman l
	             	if(infoGeneralConfig('course_shift')==0)
	             	  {  //pa tenu kont de shift la (li ka modifye)
	             	      if(isset($_POST['SrcStudentHasCourses']['shift']))
	                         $shift= $_POST['SrcStudentHasCourses']['shift'];
	             	   }
	             	 elseif(infoGeneralConfig('course_shift')==1)
	             	    { //tenu kont de shift la (li pa ka modifye)
	             	        Yii::$app->getSession()->setFlash('info', [
												    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','{student} already has course(s) in the {shift} so he/she cannot have course in other shift.',['student'=>$modelStudent->getFullName(), 'shift'=>$shift_name]) ),
												    'title' => Html::encode(Yii::t('app','Info') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
	             	      }
	             	    
	             	    
	             	} 
	              else
	                {   if(isset($_POST['SrcStudentHasCourses']['shift']))
	                        $shift= $_POST['SrcStudentHasCourses']['shift'];
	               	
	               	  }
	               	  
	               	   
	            
	              
	             //if(isset($_POST['SrcStudentHasCourses']['room']))
	             // $room = $_POST['SrcStudentHasCourses']['room'];
	              
	             
	              
	              
	             //$model->program=$program;
	              
	             //$model->level=$level; 
	             
	             $model->shift=$shift; 
	             
	             // $model->room=$room;
	              
	               //$model->course=$course;
	            
	          
	           //Yii::$app->session['studentHasCourses_room'] = $room;
	           Yii::$app->session['studentHasCourses_shift']= $shift;
	            //$this->program = $model->program ;
	             
	             if(($program!='')&&($level==''))
	             {
	             	 //$dataProvider = $searchModelCourse->searchCoursesInProgramShiftRoomForStudentToAdd($student,$program,NULL,NULL);
	             	 if($model->past_courses==0)
                             $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,NULL,NULL,$acad);
                         elseif($model->past_courses==1)        
                             $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,NULL,NULL,$model->academic_year);
                         
	              }
	             elseif(($program!='')&&($level!=''))
	                 {
	                 	//$dataProvider = $searchModelCourse->searchCoursesInProgramShiftRoomForStudentToAdd($student,$program,$shift,$level);
	                 	if($model->past_courses==0)
                                    $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,$shift,$level,$acad);	
	                 	elseif($model->past_courses==1)
                                    $dataProvider = $searchModelCourse->searchCoursesInProgramShiftLevelForStudentToAdd($student,$program,$shift,$level,$model->academic_year);	
	                 	
	                 }              
   
	            
	         // if(isset($_GET['from'])&&($_GET['from']=='list'))
	         //   {  
	            	if(isset($_POST['create']))
	                 {           
						         $ok=true;
				                    $model_new = new SrcStudentHasCourses();
				                   
								
								
								  //$action=Yii::$app->request->post('action');
							       $selection=(array)Yii::$app->request->post('selection');//typecasting
							       if($selection!=null)
								    {   foreach($selection as $course)
								         {
								            $model_new->course=(int)$course;
								            $model_new->student=$student;
								            
							                $model_new->is_pass=0;
							                $model_new->date_created=date('Y-m-d');
							                $model_new->create_by=currentUser();
				
							                if($model_new->save() )
							                  { 
                                                                            if($model->past_courses==1)//mete -1 kom not nan tab grade pou kou a pou li ka paret nan repriz
                                                                                {
                                                                                    $command = Yii::$app->db->createCommand();
                                                                                       $command->insert('grades', [

                                                                                                                    'student'=>$model_new->student,
                                                                                                                    'course'=>$model_new->course,
                                                                                                                    'grade_value'=>-1,
                                                                                                                    'validate'=> 0,
                                                                                                                    'publish' => 0,
                                                                                                                    'create_by'=>currentUser(),
                                                                                                                    'date_created'=>date('Y-m-d'),

                                                                                                                ])->execute();

                                                                                }    
                                                                            unset($model_new);
							                     $model_new = new StudentHasCourses();
							                  	
							                  	}
							                 else
							                    {   
							                    	$ok=false;
							                      }
							                      
								           } 
								             
								      }
								    else 
								      { $ok=false;
								           Yii::$app->getSession()->setFlash('info', [
												    'type' => 'info', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-info-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Please, select at least one course.') ),
												    'title' => Html::encode(Yii::t('app','Info') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
								       }
										
				               if ($ok==true) 
						          {   
						               //$dbTrans->commit(); 
						                unset(Yii::$app->session['alreadyHasCourses']); 	
						                return $this->redirect(['persons/moredetailsstudent?id='.$student.'&is_stud=1', ]);
						          }
						        else
						           { //   $dbTrans->rollback();
						           }


	                 }
	            	
	           // }
	          
	        
	           }
	            
	           } //fen shift!=0
	      else
	        {
	        	Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please set SHIFT for this student first.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
	        	} 
	        	
	        	
	        }  //fen level!=0
	      else   
	        {
	        	Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Please set LEVEL for this student first.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
	        	} 
	 
	            return $this->render('createsolo', [
	                'model' => $model,
	                'dataProvider'=>$dataProvider,
	            ]);
         
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing StudentHasCourses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      if(Yii::$app->user->can('fi-studenthascourses-delete'))
       {
       	  try{
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
            
            } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    
     public function actionDecision()
    {
        if( (Yii::$app->user->can('fi-studenthascourses-decision')) )  
         {
	
	        $acad = Yii::$app->session['currentId_academic_year'];
	        
	        $model = new SrcStudentHasCourses();
	        
	        $searchModelStudent = new SrcStudentOtherInfo();
	        
	        $program= 0;
	        $shift= 0;
	        $room = 0;
	        $level=0;
	        
                
                
                $dataProvider = $searchModelStudent->searchStudentsInProgramLevel(0,0); 
		     
	      
	      if($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction();
	            
	    
	               if(isset($_POST['SrcStudentHasCourses']['program']))
	                { $program= $_POST['SrcStudentHasCourses']['program'];
	                  $model->program=[$program];
                          
                          Yii::$app->session['program_decision']= $program;
	                }
	                
	              
	             
		         if(isset($_POST['SrcStudentHasCourses']['level']))
		           { $level= $_POST['SrcStudentHasCourses']['level'];
		                 $model->level=[$level]; 
		                 
		            }
		              
		             if(isset($_POST['SrcStudentHasCourses']['shift']))
		              { $shift= $_POST['SrcStudentHasCourses']['shift'];
		                 $model->shift=[$shift]; 
		         
		              }
		            
	          
		             if(isset($_POST['SrcStudentHasCourses']['room']))
		              {  $room = $_POST['SrcStudentHasCourses']['room'];
		                   $model->room=[$room];
		               
		              }
		   
	         
        
             if(($program!='')&&($program!=0)&&($room!='')&&($room!=0))
	             {
	             	Yii::$app->session['decision_shift']=$shift;
	             	Yii::$app->session['decision_level']=$level;
	             	Yii::$app->session['decision_group']=$room;
	             	
			$dataProvider = $model->getStudentsForDecision($room,$level,$shift,$program,$acad);
	           	    
	              }
	              
	              
                  if(isset($_GET['room']))
                    { $room = $_GET['room'];
                          
                    }
                    
	           if(isset($_POST['create']))
	            {
	                 	                   
	                $success=false;
			 
                        $temwen=false;
			
                   
                  if($room!=0)
                   { 
	                  $ok=false;
	                          
			             
					  foreach($_POST['id_stud'] as $id)
					    {
						$comment='';
                                                $level='';
                                                $next_level='';
                                                $room='';
                                                $status='';
                                                $old_status ='';
                                                
	                                          if(isset($_POST['decision'][$id]))
                                                    { 
                                                        $command = Yii::$app->db->createCommand();
                                                        
                                                         //rekipere done student_level yo pou history
                                                          $modelStudLev = SrcStudentLevel::findOne($id);
                                                          
                                                          $level = $modelStudLev->level;
                                                          
                                                          $room = $modelStudLev->room;
                                                          
                                                          $old_status = $modelStudLev->status;
                                                          
                                                           switch ($level)
                                                            {
                                                               case 1:  $next_level = 2;
                                                                        $status =0;

                                                                        break;
                                                               case 2:  $next_level = 2;
                                                                        $status =1;

                                                                        break;

                                                               case 3:  $next_level = 3;
                                                                        $status =1;

                                                                        break;

                                                             }
                                                          
                                                          
											
                                                        $decision=$_POST['decision'][$id];
                                                        
                                                       switch ($decision)
                                                         {
                                                            case 1:  $comment = Yii::t('app','Licensed to higher level');
                                                                    $command1= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$id.' AND academic_year='.$acad ); 
                                                                    $data = $command1->queryAll();

                                                                //$command->update('student_level', ['is_pass' => 1, 'date_updated'=>date('Y-m-d'),'update_by'=>currentUser()  ], ['student'=>$model_new->student, 'course'=>$course])->execute();
                                                            if($status==1)
                                                                 {
                                                                           if($data==NULL) 
                                                                            {
                                                                               $command->insert('student_level_history', [
                                                                           
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 1,
                                                                                                            'status'=>$status, //$old_status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                            }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                      
                                                                           $temwen=true;
						                           $ok=true;
                                                                                         
                                                                         $modelStudLev->delete();
                                                                         
                                                                 }
                                                            elseif($status==0)
                                                                 {
                                                                      //update student_level
                                                                      $modelStudLev->setAttribute('level',$next_level);
                                                                      $modelStudLev->setAttribute('room',NULL);
                                                                      $modelStudLev->setAttribute('status',$status);
                                                                      
                                                                       if($modelStudLev->save() )
                                                                         {
                                                                             if($data==NULL) 
                                                                            {     
                                                                                $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 1,
                                                                                                            'status'=>$status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                                 }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                                   
                                                                                 
                                                                                   
                                                                                   $temwen=true;
						                                         $ok=true;
                                                                              }
                                                                 }
                                                                 
                                                            
                                                                      
                                                                 break;
                                                             
                                                            case 2:  $comment = Yii::t('app','Licensed to higher level conditionally');
                                                            
                                                                    $command1= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$id.' AND academic_year='.$acad ); 
                                                                    $data = $command1->queryAll();
                                                                        
                                                                    if($status==1)
                                                                      {
                                                                          if($data==NULL) 
                                                                            {
                                                                               $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 1,
                                                                                                            'status'=>$status, //$old_status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                            }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                           
                                                                           $temwen=true;
						                                         $ok=true;
                                                                                         
                                                                         $modelStudLev->delete();
                                                                         
                                                                 }
                                                            elseif($status==0)
                                                                 {    //update student_level
                                                                              $modelStudLev->setAttribute('level',$next_level);
                                                                              $modelStudLev->setAttribute('room',0);
                                                                              $modelStudLev->setAttribute('status',$status);

                                                                               if($modelStudLev->save() )
                                                                              {
                                                                                 if($data==NULL) 
                                                                                  {
                                                                                     $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 1,
                                                                                                            'status'=>$status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                                  }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                                    
                                                                                   
                                                                                   $temwen=true;
						                                         $ok=true;
                                                                              }
                                                                    }
                                                                    
                                                                   break;
                                                             
                                                            case 3:   $comment = Yii::t('app','Maintained at the current level for all modules');
                                                                
                                                                      $command1= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$id.' AND academic_year='.$acad ); 
                                                                    $data = $command1->queryAll();
                                                                    
                                                                   if($status==1)
                                                                     {
                                                                          if($data==NULL) 
                                                                            {  
                                                                                $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 0,
                                                                                                            'status'=>$status, //$old_status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                             }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                           
                                                                           $temwen=true;
						                                         $ok=true;
                                                                                         
                                                                         $modelStudLev->delete();
                                                                         
                                                                 }
                                                            elseif($status==0)
                                                                 {       //update student_level
                                                                              $modelStudLev->setAttribute('level',$level);
                                                                              $modelStudLev->setAttribute('room',NULL);
                                                                              $modelStudLev->setAttribute('status',$status);

                                                                               if($modelStudLev->save() )
                                                                              {
                                                                                  if($data==NULL) 
                                                                                   {
                                                                                     $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 0,
                                                                                                            'status'=>$status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                                   }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                                    
                                                                                   
                                                                                   $temwen=true;
						                                         $ok=true;
                                                                              }
                                                                 }
                                                                 
                                                                 break;
                                                             
                                                            case 4:   $comment = Yii::t('app','Maintained at the current level for some modules');
                                                                
                                                                      $command1= Yii::$app->db->createCommand('SELECT * FROM student_level_history WHERE student_id='.$id.' AND academic_year='.$acad ); 
                                                                    $data = $command1->queryAll();
                                                                    
                                                                    if($status==1)
                                                                      {
                                                                          if($data==NULL) 
                                                                            {  
                                                                                $command->insert('student_level_history', [
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 0,
                                                                                                            'status'=>$status, //$old_status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                            }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                           
                                                                           $temwen=true;
						                                         $ok=true;
                                                                                         
                                                                         $modelStudLev->delete();
                                                                         
                                                                 }
                                                            elseif($status==0)
                                                                 {     //update student_level
                                                                              $modelStudLev->setAttribute('level',$level);
                                                                              $modelStudLev->setAttribute('room',NULL);
                                                                              $modelStudLev->setAttribute('status',$status);

                                                                              if($modelStudLev->save() )
                                                                              {
                                                                                  if($data==NULL) 
                                                                                   {
                                                                                      $command->insert('student_level_history', [
                                                                                  
                                                                                                            'student_id'=>$id,
                                                                                                            'level'=>$level,
                                                                                                            'room'=>$room,
                                                                                                            'comment'=>$comment,
                                                                                                            'is_pass' => 0,
                                                                                                            'status'=>$status,
                                                                                                            'academic_year'=>$acad,
                                                                                                            'create_by'=>currentUser(),
                                                                                                            'date_created'=>date('Y-m-d'),

                                                                                                        ])->execute();
                                                                                  }
                                                                           else
                                                                           {
                                                                               $command->update('student_level_history', ['comment'=>$comment, 'date_created'=>date('Y-m-d'),'create_by'=>currentUser()  ], ['student_id'=>$id, 'academic_year'=>$acad])
                                      ->execute();
                                                                           }
                                                                                    
                                                                                   
                                                                                   $temwen=true;
						                                         $ok=true;
                                                                              }
                                                                 }
                                                                 
                                                                 break;
                                                             
                                                            
                                                        }

		                                         
					                 
					         }
						           
						            										   
					  } 
						             
						     
                                                     
                                                     
									
			               if ($ok==true) 
					          {  
                                                       $is_save =1;
                                                           
                                                           
                                                           Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                         
                                                      
					          }
					        else
					           { //   $dbTrans->rollback();
					           }
                                                   
                    
                                                   
                                                   
                    }
                   else
                     { //$message_course_id=true;
                       $model->program=0;
                     }
	           
                    }// end post create
	            
                   
	            
	        } 
	        
	
	            return $this->render('decision', [
	                'model' => $model,
                        'dataProvider'=>$dataProvider, 
	                
	            ]);
	            
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
             return $this->redirect(Yii::$app->request->referrer);
               }

          }
          

    }
   
    
    public function actionDecisionlist()
    {
       if(Yii::$app->user->can('fi-studenthascourses-decisionlist'))
         {
         $acad = Yii::$app->session['currentId_academic_year'];
        	
	 $count =  Yii::$app->db->createCommand('
                    SELECT COUNT(*) FROM student_level_history WHERE academic_year=:acad', [':acad' => $acad])->queryScalar();
         $sql = "SELECT DISTINCT student_id, p.last_name, p.first_name, slh.comment, room, level, slh.create_by, slh.date_created FROM student_level_history slh
 INNER JOIN persons p ON (p.id = slh.student_id)
 WHERE academic_year=$acad ORDER BY last_name ASC, first_name ASC, level ASC";
         
         $dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    'pagination' => [ 'pageSize' => 100000000 ],
                    ]);
	        	     
        


	 
	        return $this->render('decisionlist', [
	            'dataProvider' => $dataProvider,
	        ]);
          
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }
      
        
    }  
    
    
   public function actionRollbackdecision($id)
    {
      if(Yii::$app->user->can('fi-studenthascourses-rollbackdecision'))
       {
       	  $acad = Yii::$app->session['currentId_academic_year'];
          
          $level='';
          $room ='';
          $status='';
          
          $sql = "SELECT student_id, comment, room, level, status, create_by, date_created FROM student_level_history slh
  WHERE student_id=$id AND academic_year=$acad";
            
          $level_history = Yii::$app->db->createCommand($sql)->queryAll();
          
          foreach ($level_history as $history)
           {
                 $level=$history['level'];
                $room =$history['room'];
                $status=$history['status'];
                
           }
          
         $modelLevel= SrcStudentLevel::findOne($id);
        
         if($modelLevel!=null)
         {
            $modelLevel->setAttribute('level', $level);
            $modelLevel->setAttribute('room', $room);
            $modelLevel->setAttribute('status', $status);
             
            if($modelLevel->save())
             {
                //$sql1 = "DELETE FROM student_level_history WHERE student_id=$id ";
               // Yii::$app->db->createCommand($sql1)->execute();
                
                Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                
               
              }
              
              
           }
         else
         {
             $sql2 = "INSERT INTO student_level (student_id,level,room,status) VALUES($id,$level,$room,0) ";
                Yii::$app->db->createCommand($sql2)->execute();
                
                 Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Operation terminated successfully.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                
                
         }
         
           
         return $this->redirect(['decisionlist']);
         
            
            

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }

          }

    }
  
    
    
    
    /**
     * Finds the StudentHasCourses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentHasCourses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStudentHasCourses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}