<?php

namespace app\modules\fi\controllers;

use yii\web\Controller;

/**
 * Default controller for the `fi` module
 */
class DefaultController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
