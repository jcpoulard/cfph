<?php

namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\StudentLevel;
use app\modules\fi\models\SrcStudentLevel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * StudentlevelController implements the CRUD actions for StudentLevel model.
 */
class StudentlevelController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentLevel models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('fi-studentlevel-index'))
         { 
         	 $searchModel = new SrcStudentLevel();
         	 //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
	        if($program==null)
	          {
	          	  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	          	  
	          	  if(isset($_GET['SrcStudentLevel']))
			         $dataProvider= $searchModel->search(Yii::$app->request->queryParams);
			      		              	      
			              	 
	          	  
	          	}
	         else //se yon responsab pwogram
	           {
	           	     $dataProvider = $searchModel->searchByProgram($program);
	          	  
	          	  if(isset($_GET['SrcStudentLevel']))
			         $dataProvider= $searchModel->searchByProgram($program);
				           	 
	
	           	 } 
		        
		        return $this->render('index', [
		            'searchModel' => $searchModel,
		            'dataProvider' => $dataProvider,
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Displays a single StudentLevel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentLevel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		 if(Yii::$app->user->can('fi-studentlevel-create'))
         { 
		        $model = new SrcStudentLevel();
		         
		         $status =0;
		         $shift=0;
		         $level =0;
		         $room =0;
		         
		     	 //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
	        if($program==null)
	          {
                      if(infoGeneralConfig('first_year_use')==1)
                          $dataProvider = $model->searchStudentsWithoutLevel(); 
                      else if(infoGeneralConfig('first_year_use')==0)
                          $dataProvider = $model->searchStudentsWithoutGroup(); 
	           }
	        else
	          {
	          	   if(infoGeneralConfig('first_year_use')==1)
                               $dataProvider = $model->searchStudentsWithoutLevelByProgram($program);
                           else if(infoGeneralConfig('first_year_use')==0)
                               $dataProvider = $model->searchStudentsWithoutGroupByProgram($program);
	          	}
		         
		         if ($model->load(Yii::$app->request->post()) ) 
				        {
				            // $dbTrans = Yii::app->db->beginTransaction(); 
				            
						       if(isset($_POST['SrcStudentLevel']['program']))
				              { $program= $_POST['SrcStudentLevel']['program'];
				                $model->program=[$program];
				               }
				               
				              if(isset($_POST['SrcStudentLevel']['shift']))
				              { $shift= $_POST['SrcStudentLevel']['shift'];
				                $model->shift=[$shift];
				               }
				               
				              if(isset($_POST['SrcStudentLevel']['level']))
				              { $level= $_POST['SrcStudentLevel']['level'];
				                $model->level=[$level];
				               }
				               
				             if(isset($_POST['SrcStudentLevel']['room']))
				              { $room= $_POST['SrcStudentLevel']['room'];
				                $model->room=[$room];
				               }
				               
				      /*       if(isset($_POST['SrcStudentLevel']['status']))
				              { $status= $_POST['SrcStudentLevel']['status'];
				                $model->status=[$status];
				               }
				          */
				               
				      if(infoGeneralConfig('first_year_use')==1)
                                          $dataProvider = $model->searchStudentsWithoutLevelOrGroupByProgramShift($program,$shift);
                                      elseif(infoGeneralConfig('first_year_use')==0)
                                          $dataProvider = $model->searchStudentsWithoutGroupByProgramShiftLevel($program,$shift,$level);                                     
				               
				            if(isset($_POST['create']))
				            { 
							    $ok=true;
			                    $model_new = new SrcStudentLevel();
			                    $model_search = new SrcStudentLevel();
			                   
							
							
							  //$action=Yii::$app->request->post('action');
						       $selection=(array)Yii::$app->request->post('selection');//typecasting
						       if($selection!=null)
							    {   foreach($selection as $id)
							         {
							            //gade si elev sa la deja pou update
							            $stud_id = (int)$id;
							           
							        $model_new = SrcStudentLevel::findOne($stud_id);
							            if($model_new==null)
							            {
                                                                      if( ($level!=null)&&($room!=null) )
                                                                        {
								        $model_new = new SrcStudentLevel();
                                                                        
                                                                        $model_new->student_id=$stud_id;
								            
							                $model_new->level= $level;
							                $model_new->room= $room;
							                $model_new->status= $status;
							                
							                if($model_new->save() )
							                  { unset($model_new);
							                     $model_new = new SrcStudentLevel();
							                  	
							                  	}
							                 else
							                    {   
							                    	$ok=false;
							                      }
                                                                              
                                                                        }
                                                                      else
                                                                       {  $ok=false;
                                                                         Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"Level and Room cannot be null.") ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                                      }
							                      
							            }
							          else
							            {
							              if( ($level!=null)&&($room!=null) )
                                                                        {	
                                                                          $model_new->level= $level;
							            	  $model_new->room= $room;
								                $model_new->status= $status;
								                
								                if($model_new->save() )
								                  { unset($model_new);
								                     $model_new = new SrcStudentLevel();
								                  	
								                  	}
								                 else
								                    {   
								                    	$ok=false;
								                      }
                                                                                      
                                                                        }
                                                                      else
                                                                      {  $ok=false;
                                                                         Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"Level and Room cannot be null.") ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                                                                      }


							              }
							                      
							        } 
							             
							      }
							    else 
							      $ok=false;
									
			               if ($ok==true) 
					          {   
					               //$dbTrans->commit();  	
					                return $this->redirect(['index', ]);
					          }
					        else
					           { //   $dbTrans->rollback();
					           }
		
				               
				            }
				            
				            
				        } 
				        
		
		            return $this->render('create', [
		                'model' => $model,
		                'dataProvider'=>$dataProvider,
		            ]);
		            
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }

    /**
     * Updates an existing StudentLevel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      if(Yii::$app->user->can('fi-studentlevel-update'))
         { 
              $acad = Yii::$app->session['currentId_academic_year'];
              $old_level = '';
              $old_room = '';
              
		        $model = $this->findModel($id);
                        
                        $old_level = $model->level;
                         $old_room = $model->room;
              
		         if ($model->load(Yii::$app->request->post()) ) 
				    {
				            // $dbTrans = Yii::app->db->beginTransaction(); 
				          if(isset($_POST['update']))
				            { 
				                if($model->save() )
						                  {    //delete toutkou li te pran nan gwoup oswa nivo anvan an
                                                                       if( ($model->level!= $old_level) || ($model->room!= $old_room ) ) 
                                                                        {
                                                                           
                                                                        }
						                  	//$dbTrans->commit();  	
					                           return $this->redirect(['index', ]);				                  	
						                  	}
						                 else
						                    {   
						                    	//   $dbTrans->rollback();
						                      }
				            }
				            
				     }
		
		            return $this->render('update', [
		                'model' => $model,
		            ]);
        
          }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
    }

    /**
     * Deletes an existing StudentLevel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       if(Yii::$app->user->can('fi-studentlevel-delete'))
         { 
	         try{
	          $this->findModel($id)->delete();
	
	           return $this->redirect(['index']);
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
		}
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
          
          
    }

    /**
     * Finds the StudentLevel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StudentLevel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStudentLevel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
