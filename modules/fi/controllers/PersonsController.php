<?php
namespace app\modules\fi\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Imagine\Image\Box;
use yii\web\ForbiddenHttpException;
use yii\db\IntegrityException;

use app\modules\fi\models\StudentOtherInfo;
use app\modules\fi\models\SrcStudentOtherInfo;

use app\modules\fi\models\StudentLevel;
use app\modules\fi\models\SrcStudentLevel;
 
use app\modules\fi\models\EmployeeInfo;
use app\modules\fi\models\SrcEmployeeInfo;

use app\modules\fi\models\Titles;
use app\modules\fi\models\SrcTitles;
use app\modules\fi\models\SrcProgram;
use app\modules\fi\models\SrcProgramsLeaders;

use app\modules\fi\models\PersonsHasTitles;
use app\modules\fi\models\SrcPersonsHasTitles;

use app\modules\fi\models\DepartmentHasPerson;

use app\modules\fi\models\DepartmentInSchool;
use app\modules\fi\models\SrcDepartmentInSchool;

use app\modules\fi\models\CustomField;
use app\modules\fi\models\SrcCustomField;

use app\modules\fi\models\CustomFieldData;

use app\modules\fi\models\ContactInfo;
use app\modules\fi\models\SrcContactInfo;

use app\modules\fi\models\SrcCourses;

use app\modules\idcards\models\SrcIdcard;

use yii\helpers\Url;

use kartik\mpdf\Pdf;
use yii\data\ActiveDataProvider;
use yii\helpers\json;


use app\modules\rbac\models\User; //pou tou kreye user depi ou ajoute/modifye moun
use app\modules\rbac\models\form\Signup;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcBalance;
use app\modules\fi\models\SrcStudentHasCourses;

/**
 * PersonsController implements the CRUD actions for Persons model.
 */
class PersonsController extends Controller
{
    public $layout = "/inspinia";

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Persons models.
     * @return mixed
     */
    public function actionIndex()
    {
         if(Yii::$app->user->can('fi-persons-index'))
         {

	        $searchModel = new SrcPersons();
	        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	        return $this->render('index', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,
	        ]);

         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Displays a single Persons model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('fi-persons-view'))
         {
        	return $this->render('view', [
            'model' => $this->findModel($id),
        ]);

     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Creates a new Persons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatestudent()
    {

        if(Yii::$app->user->can('fi-student-create')){
                    $ageCalculate= false;
             $_program = '';
             $_shift = '';

             $disabled ='0';

        $model = new SrcPersons();
        $modelCustomFieldData = new CustomFieldData();
        $modelStudentOtherInfo = new SrcStudentOtherInfo();
        $modelStudentLevel= new SrcStudentLevel;


      if ($model->load(Yii::$app->request->post()) )
        {
             if(ageCalculator($model->birthday)>=1)
			  	    $ageCalculate= true;

			  	 $age_message= Yii::t('app','Student may have at least').' 1 ';


           if(isset($_POST['SrcStudentOtherInfo']['apply_shift']) && ($_POST['SrcStudentOtherInfo']['apply_for_program']!=null) )
             {
                $_shift = $_POST['SrcStudentOtherInfo']['apply_shift'];

                $modelStudentOtherInfo->apply_shift = $_shift;
              }


           if(isset($_POST['SrcStudentOtherInfo']['apply_for_program']) && ($_POST['SrcStudentOtherInfo']['apply_for_program']!=null) )
             {
                $_program = $_POST['SrcStudentOtherInfo']['apply_for_program'];

                $modelStudentOtherInfo->apply_for_program = $_program;
              }


            // $dbTrans = Yii::app->db->beginTransaction();

           if(isset($_POST['create']))
            {


		        if((($model->birthday!='')&&($ageCalculate==true ))||($model->birthday==''))
				  {
		            $automatic_code = infoGeneralConfig('automatic_code');

						if($model->active=='')
						 $model->setAttribute('active',2);

						$model->setAttribute('is_student',1);
						$model->setAttribute('date_created',date('Y-m-d'));
						$model->setAttribute('create_by',currentUser());

						   $model->file = UploadedFile::getInstance($model,'file');
						   $fileName = '';

		               if($model->file)
				              {

				              	$imageName = strtr( strtolower( trim(str_replace(" ","",$model->first_name) )), pa_daksan() ).strtr( strtolower( trim(str_replace(" ","",$model->last_name) )), pa_daksan() ).$model->id;



				              	  //save non image la nan baz la
				              	  $model->image = $imageName.'.'.$model->file->extension;
				              	  $fileName = $model->image;

				              	  //resize imaj la


                                                      $last_name = $model->last_name;
		                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		         if( ($model->first_name!='')&&($model->last_name!='') ) 
		          {   
		            if ($model->save())
		                { 
                               	//code automatic
					     if($automatic_code ==1)
						   {
					        // if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
						    // {

								$cf='';
					         $cl='';

                               //first_name
							$explode_firstname=explode(" ",substr( trim($model->first_name),0));

				            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
							 {
						        $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

							 }

							 //last_name
							$explode_lastname=explode(" ",substr( trim($model->last_name),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
							 {
						        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

							 }
							else
							 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

							 }

							  $code_ = $cf.$cl.$model->id;


								  $model->setAttribute('id_number',$code_);

								  $model->save();

							  // }

						   }
                               $model->file->saveAs(Yii::getAlias('documents/photo_upload/'.$imageName.'.'.$model->file->extension));
		                	    // Demarrer enregistrement champs personalisables
						        $criteria_cf= ['field_related_to' => 'stud'];

						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						            }
						        }
						        // Terminer Enregistrement champs personalisables

                                if( ($_program!='')&&($_shift!='') )
                                  {  $modelStudentOtherInfo = new StudentOtherInfo();
                                      $modelStudentOtherInfo->student = $model->id;
                                      $modelStudentOtherInfo->apply_for_program =$_program;
                                      $modelStudentOtherInfo->apply_shift =$_shift;

                                      $modelStudentOtherInfo->save();

                                    }


		                	  $explode_lastname=explode(" ",substr( trim($model->last_name),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
				              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
				            else
				              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;

                            $password = "password";

                            $user = new Signup();
                            $user->username = $username;
				            $user->person_id = $model->id;
				            $user->is_parent = 0;
				               if($model->email!='')
				               $user->email = $model->email;
				            $user->password=$password;

				            $user->signup_from_person();

                                         if(isset($_POST['SrcStudentLevel']['level']) && ($_POST['SrcStudentLevel']['level']!=null) )
				             {
				             	     $modelStudentLevel->load(Yii::$app->request->post());
				             	     $modelStudentLevel->student_id = $model->id;
				             	     $modelStudentLevel->save();

				               }
				                                 //$dbTrans->commit();
		                	      return $this->redirect(['/fi/persons/moredetailsstudent', 'id' => $model->id,'is_stud'=>1]);

		                }
		              else
		                { //   $dbTrans->rollback();
		                     $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName;
                                   if(file_exists($file_to_delete))
                                       unlink($file_to_delete);
		                 }
		                 
		             }
                                  }else{

		                $last_name = $model->last_name;
		                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		           
		        if( ($model->first_name!='')&&($model->last_name!='') ) 
		          {   
		            if ($model->save())
		                {
		                	     	//code automatic
					     if($automatic_code ==1)
						   {
					        // if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
						    // {

								$cf='';
					         $cl='';

                               //first_name
							$explode_firstname=explode(" ",substr( trim($model->first_name),0));

				            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
							 {
						        $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

							 }

							 //last_name
							$explode_lastname=explode(" ",substr( trim($model->last_name),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
							 {
						        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

							 }
							else
							 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

							 }

							  $code_ = $cf.$cl.$model->id;


								  $model->setAttribute('id_number',$code_);

								  $model->save();

							  // }

						   }
		                	    // Demarrer enregistrement champs personalisables
						        $criteria_cf= ['field_related_to' => 'stud'];

						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						            }
						        }
						        // Terminer Enregistrement champs personalisables

                                if( ($_program!='')&&($_shift!='') )
                                  {  $modelStudentOtherInfo = new StudentOtherInfo();
                                      $modelStudentOtherInfo->student = $model->id;
                                      $modelStudentOtherInfo->apply_for_program =$_program;
                                      $modelStudentOtherInfo->apply_shift =$_shift;

                                      $modelStudentOtherInfo->save();

                                    }

		                    $explode_lastname=explode(" ",substr( trim($model->last_name),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
				              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
				            else
				              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;

                            $password = "password";

                            $user = new Signup();
                            $user->username = $username;
				            $user->person_id = $model->id;
				            $user->is_parent = 0;
				               if($model->email!='')
				               $user->email = $model->email;

				            $user->password=$password;

				            $user->signup_from_person();

				           if(isset($_POST['SrcStudentLevel']['level']) && ($_POST['SrcStudentLevel']['level']!=null) )
				             {
				             	     $modelStudentLevel->load(Yii::$app->request->post());
				             	     $modelStudentLevel->student_id = $model->id;
				             	     $modelStudentLevel->save();

				               }

				            //$dbTrans->commit();
		                	return $this->redirect(['/fi/persons/moredetailsstudent', 'id' => $model->id,'is_stud'=>1]);

		                }
		              else
		                { //   $dbTrans->rollback();
		                     $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName;
                                   if(file_exists($file_to_delete))
                                       unlink($file_to_delete);
		                 }
		                 
                    }

                                  }

		           }
			     else
				    { //Yii::$app->getSession()->setFlash(Yii::t('app','Error'), $age_message.Yii::t('app',' yr old'));
				          Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($age_message.Yii::t('app',' yr old') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				    }


             }


        }


            return $this->render('create_student', [
                'model' => $model,
                'modelStudentOtherInfo' => $modelStudentOtherInfo,
                'modelCustomFieldData'=>$modelCustomFieldData,
                'modelStudentLevel'=>$modelStudentLevel,
                'disable_field'=>$disabled,
            ]);

        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing Persons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdatestudent($id)
    {
     if(Yii::$app->user->can('fi-student-update'))
      {
         $acad = Yii::$app->session['currentId_academic_year'];
         
        $ageCalculate= false;
        $IdOtherInfo= 0;

        $username="";
        $disabled =0;
        
        $programShift_denied = false;



		$modelStudentLevel=new SrcStudentLevel;

        $modelCustomFieldData = new CustomFieldData();
        $modelCustomFieldData1 = new CustomFieldData();

        $model = $this->findModel($id);

        $fileName_old = $model->image;

        $modelStudentOtherInfo = new SrcStudentOtherInfo();

        $result = $modelStudentOtherInfo->getOtherInfo($model->id);
        $_program = '';
        $_shift = '';
        $IdOtherInfo = 0;

        if($result!=null)
        {
          foreach($result as $r)
           {
               $IdOtherInfo= $r->id;
            }

            $modelStudentOtherInfo_ = SrcStudentOtherInfo::findOne($IdOtherInfo);
            $modelStudentOtherInfo->apply_for_program = $modelStudentOtherInfo_->apply_for_program;
            $modelStudentOtherInfo->apply_shift = $modelStudentOtherInfo_->apply_shift;
         }

        $studentLevel = SrcStudentLevel::findOne($model->id);
        if($studentLevel!=null)
         {
            $modelStudentLevel->level = $studentLevel->level;
            $disabled =1;
         }

       if ($model->load(Yii::$app->request->post()) )
        {

              if(ageCalculator($model->birthday)>=1)
			  	    $ageCalculate= true;

			  	 $age_message= Yii::t('app','Student may have at least').' 1 ';



			if(isset($_POST['SrcStudentOtherInfo']['apply_for_program']) && ($_POST['SrcStudentOtherInfo']['apply_for_program']!=null) )
             {
                $_program = $_POST['SrcStudentOtherInfo']['apply_for_program'];

                $modelStudentOtherInfo->apply_for_program = $_program;
              }

            if(isset($_POST['SrcStudentOtherInfo']['apply_shift']) && ($_POST['SrcStudentOtherInfo']['apply_shift']!=null) )
             {
                $_shift = $_POST['SrcStudentOtherInfo']['apply_shift'];

                $modelStudentOtherInfo->apply_shift = $_shift;
              }


            // $dbTrans = Yii::app->db->beginTransaction();

           if(isset($_POST['update']))
             {

		        if((($model->birthday!='')&&($ageCalculate==true ))||($model->birthday==''))
				  {
		            $automatic_code = infoGeneralConfig('automatic_code');
						//code automatic
					     if($automatic_code ==1)
						   {
					        // if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
						     //{

								$cf='';
					         $cl='';

                               //first_name
							$explode_firstname=explode(" ",substr( trim( trim($model->first_name)),0));

				            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
							 {
						        $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

							 }

							 //last_name
							$explode_lastname=explode(" ",substr( trim( trim($model->last_name)),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
							 {
						        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

							 }
							else
							 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

							 }

							  $code_ = $cf.$cl.$model->id;


								  $model->setAttribute('id_number',$code_);

							  // }

						   }


						$model->setAttribute('date_updated',date('Y-m-d'));
						$model->setAttribute('update_by',currentUser());

						   $model->file = UploadedFile::getInstance($model,'file');

		               if($model->file)
				              {
				              	  if($fileName_old!='')
				              	   {  $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName_old;
                                        if(file_exists($file_to_delete))
                                           unlink($file_to_delete);

				              	     }

				              	$imageName = strtr( strtolower( trim(str_replace(" ","",$model->first_name) )), pa_daksan() ).strtr( strtolower( trim(str_replace(" ","",$model->last_name))), pa_daksan() ).$model->id;





				              	  //save path la nan baz la
				              	  $model->image = $imageName.'.'.$model->file->extension;

				              	  //resize imaj la



                                                   $last_name = trim($model->last_name );
                                                   $first_name = trim($model->first_name );
                                                    $model->setAttribute('first_name',$first_name );
                                                    $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

                                                if ($model->save())
                                                    {
                                                          $modelCard = SrcIdcard::find()->where('person_id='.$model->id)->all();
                                                        if($modelCard!=null)
                                                        { foreach($modelCard as $card)
                                                           {
                                                               $newModelCard = SrcIdcard::findOne($card->id);
                                                               $newModelCard->prenom = $model->first_name;
                                                               $newModelCard->nom = $model->last_name;
                                                               $newModelCard->sexe = $model->gender;
                                                               $newModelCard->image_name = $model->image;

                                                               $newModelCard->save();

                                                           }
                                                        }         
                                                                  $model->file->saveAs(Yii::getAlias('documents/photo_upload/'.$imageName.'.'.$model->file->extension));
                                                                            // Demarrer mise a jours champs personalisables

                                                                            $criteria_cf = ['field_related_to' => 'stud'];

                                                                            $cfData = SrcCustomField::find()->where($criteria_cf)->all();
                                                                            if(isset($_GET['id'])){$id_student = $_GET['id'];}
                                                                            foreach($cfData as $cd){
                                                                                if(isset($_POST[$cd->field_name])){
                                                                                    $customFieldValue = $_POST[$cd->field_name];
                                                                                    $modelCustomFieldData = $modelCustomFieldData->loadCustomFieldValue($id_student,$cd->id);
                                                                                    if($modelCustomFieldData!=null){ // S'il y a deja des donnees on fait la mise a jour
                                                                                    $modelCustomFieldData->setAttribute('field_link', $cd->id);
                                                                                    $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
                                                                                    $modelCustomFieldData->setAttribute('object_id', $model->id);
                                                                                    $modelCustomFieldData->save();
                                                                                    unset($modelCustomFieldData);
                                                                                    $modelCustomFieldData = new CustomFieldData;
                                                                                    }else{ // S'il n'y a pas de donnees on ajoute
                                                                                    $modelCustomFieldData = new CustomFieldData;
                                                                                    $modelCustomFieldData->setAttribute('field_link', $cd->id);
                                                                                    $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
                                                                                    $modelCustomFieldData->setAttribute('object_id', $model->id);
                                                                                    $modelCustomFieldData->save();
                                                                                    unset($modelCustomFieldData);
                                                                                    $modelCustomFieldData = new CustomFieldData;
                                                                                    }
                                                                                }
                                                                            }
                                                                            // Terminer mise a jour des champs personalisables


                                                          if( ($modelStudentOtherInfo->apply_for_program!= null) && ($modelStudentOtherInfo->apply_shift != null) )
                                                            {
                                                            	
                                                            	    $new_modelStudentOtherInfo = SrcStudentOtherInfo::find()->where('student='.$model->id)->all();
                                                              if($new_modelStudentOtherInfo!=null)
                                                                {
                                                                	foreach($new_modelStudentOtherInfo as $otherInfo)
                                                                	   $id_other_inf = $otherInfo->id;
                                                                	   
                                                         /*       	  $command = Yii::$app->db->createCommand();
						                                               $command->update('student_other_info', ['apply_shift' => $modelStudentOtherInfo->apply_shift, 'apply_for_program'=>$modelStudentOtherInfo->apply_for_program ], ['student'=>$model->id ])
                                               ->execute(); */

                                                                 	//gad si student lan te gentan anwole(pran kou), pa dako update la
                                                                    $model_course = new SrcCourses;
                                                                    $stud_course = $model_course->getCourseByStudentIDAcad($model->id,$acad);
                                                                    if($stud_course==null)
                                                                      {
                                                                        
	                                                                	$new_studentOtherInfo = SrcStudentOtherInfo::findOne($id_other_inf);
	                                                                	$new_studentOtherInfo->setAttribute('apply_shift',$modelStudentOtherInfo->apply_shift);
	                                                                	$new_studentOtherInfo->setAttribute('apply_for_program',$modelStudentOtherInfo->apply_for_program);

                                                                          $new_studentOtherInfo->save();
                                                                      }
                                                                     else
                                                                       {
                                                                          $programShift_denied = true;	
                                                                       	}
                                                                      
                                                                  }
                                                               else
                                                                 {
                                                                 	   $modelStudentOtherInfo->setAttribute('student',$model->id);

                                                                          $modelStudentOtherInfo->save();
                                                                   }

                                                              }


                                                                  $explode_lastname=explode(" ",substr( trim($model->last_name),0));

														            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
														            else
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;



										                            $modelUser=new User;
                                                                    $user = $modelUser->findByPersonId($model->id);

									                         if($user!=null)// on fait 1 update
									                           {

									                              $command2 = Yii::$app->db->createCommand();
										                          $command2->update('user', ['username' => $username,   ], ['person_id'=>$model->id ])
									                                               ->execute();
									                            }
									                          else // enter 1 new record
									                            {  $command3 = Yii::$app->db->createCommand();

									                                $password = "password";

																	$new_user = new Signup();
																	$new_user->username = $username;
																	$new_user->person_id = $model->id;
																	$new_user->is_parent = 0;
																		if($model->email!='')
																			$new_user->email = $model->email;
																	$new_user->password=$password;

																	$new_user->signup_from_person();



									                              if($model->is_student==1)//si c etudiant
									                            	{
									                                     $modelNewUser=new User;
                                                                         $user = $modelNewUser->findByPersonId($model->id);

									                                    $command3->insert('auth_assignment', [
																				    'item_name'=>"guest",
																			'user_id'=>$user->id,
																			])->execute();

																	}

									                            }

										                  if(isset($_POST['SrcStudentLevel']['level']) && ($_POST['SrcStudentLevel']['level']!=null) )
												             {
												             	     $modelStudentLevel->load(Yii::$app->request->post());
												             	     $modelStudentLevel->student_id = $model->id;
												             	     $modelStudentLevel->save();

												               }


                                                                //$dbTrans->commit();
                                                             return $this->redirect(['/fi/persons/moredetailsstudent', 'id' => $model->id,'is_stud'=>1]);

                                                    }
                                                  else
                                                    { //   $dbTrans->rollback();

                                                     }


				              }
                                           else
                                           {
		                $last_name = trim($model->last_name );
                               $first_name = trim($model->first_name );
                                $model->setAttribute('first_name',$first_name );
                                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		            if ($model->save())
		                {
                                
                                      $modelCard = SrcIdcard::find()->where('person_id='.$model->id)->all();
                                    if($modelCard!=null)
                                    { foreach($modelCard as $card)
                                       {
                                           $newModelCard = SrcIdcard::findOne($card->id);
                                           $newModelCard->prenom = $model->first_name;
                                           $newModelCard->nom = $model->last_name;
                                           $newModelCard->sexe = $model->gender;
                                           $newModelCard->image_name = $model->image;
                                           
                                           $newModelCard->save();
                                        
                                       }
                                    }

						        // Demarrer mise a jours champs personalisables

						        $criteria_cf = ['field_related_to' => 'stud'];

						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        if(isset($_GET['id'])){$id_student = $_GET['id'];}
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData = $modelCustomFieldData->loadCustomFieldValue($id_student,$cd->id);
						                if($modelCustomFieldData!=null){ // S'il y a deja des donnees on fait la mise a jour
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }else{ // S'il n'y a pas de donnees on ajoute
						                $modelCustomFieldData = new CustomFieldData;
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }
						            }
						        }
						        // Terminer mise a jour des champs personalisables

						                   if( ($modelStudentOtherInfo->apply_for_program!= null) && ($modelStudentOtherInfo->apply_shift != null) )
                                                 {
                                                              $new_modelStudentOtherInfo = SrcStudentOtherInfo::find()->where('student='.$model->id)->all();
                                                              if($new_modelStudentOtherInfo!=null)
                                                                {
                                                                	foreach($new_modelStudentOtherInfo as $otherInfo)
                                                                	   $id_other_inf = $otherInfo->id;
                                                                    
                                                                    //gad si student lan te gentan anwole(pran kou), pa dako update la
                                                                    $model_course = new SrcCourses;
                                                                    $stud_course = $model_course->getCourseByStudentIDAcad($model->id,$acad);
                                                                    if($stud_course==null)
                                                                      {
	                                                                	$new_studentOtherInfo = SrcStudentOtherInfo::findOne($id_other_inf);
	                                                                	$new_studentOtherInfo->setAttribute('apply_shift',$modelStudentOtherInfo->apply_shift);
	                                                                	$new_studentOtherInfo->setAttribute('apply_for_program',$modelStudentOtherInfo->apply_for_program);
	
	
                                                                          $new_studentOtherInfo->save();
                                                                      }
                                                                    else
                                                                     {
                                                                     	  $programShift_denied =true;
                                                                     	}
                                                                      
                                                                  }
                                                               else
                                                                 {
                                                                 	   $modelStudentOtherInfo->setAttribute('student',$model->id);

                                                                          $modelStudentOtherInfo->save();
                                                                   }

                                                  }


                                                                 $explode_lastname=explode(" ",substr( trim($model->last_name),0));

														            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
														              $username= strtr( str_replace(" ","",strtolower( $explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
														            else
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;



										                            $modelUser=new User;
                                                                    $user = $modelUser->findByPersonId($model->id);

									                         if($user!=null)// on fait 1 update
									                           {

									                              $command2 = Yii::$app->db->createCommand();
										                          $command2->update('user', ['username' => $username,   ], ['person_id'=>$model->id ])
									                                               ->execute();
									                            }
									                          else // enter 1 new record
									                            {  $command3 = Yii::$app->db->createCommand();

									                                $password = "password";

																	$new_user = new Signup();
																	$new_user->username = $username;
																	$new_user->person_id = $model->id;
																	$new_user->is_parent = 0;
																		if($model->email!='')
																			$new_user->email = $model->email;
																	$new_user->password=$password;

																	$new_user->signup_from_person();



									                              if($model->is_student==1)//si c etudiant
									                            	{
									                                     $modelNewUser=new User;
                                                                         $user = $modelNewUser->findByPersonId($model->id);

									                                    $command3->insert('auth_assignment', [
																				    'item_name'=>"guest",
																			'user_id'=>$user->id,
																			])->execute();

																	}

									                            }

						                 if(isset($_POST['SrcStudentLevel']['level']) && ($_POST['SrcStudentLevel']['level']!=null) )
								             {
								             	     $modelStudentLevel->load(Yii::$app->request->post());
								             	     $modelStudentLevel->student_id = $model->id;
								             	     $modelStudentLevel->save();

								               }
						     
						     if($programShift_denied)
						       {
						       	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','{name} already enrolled course; program and shift cannot be updated.', ['name'=>$model->getFullName() ] ) ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
						       	}
				            //$dbTrans->commit();
		                	 return $this->redirect(['/fi/persons/moredetailsstudent', 'id' => $model->id,'is_stud'=>1]);

		                }
		              else
		                { //   $dbTrans->rollback();

		                 }

                             }

		           }
			     else
				  {    //Yii::$app->getSession()->setFlash(Yii::t('app','Error'), $age_message.Yii::t('app',' yr old'));
				       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($age_message.Yii::t('app',' yr old') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);

                   }

             }


        }

            return $this->render('update_student', [
                'model' => $model,
                'modelStudentOtherInfo' => $modelStudentOtherInfo,
                'modelCustomFieldData'=>$modelCustomFieldData,
                'modelStudentLevel'=>$modelStudentLevel,
                'disable_field'=>$disabled,
            ]);

      }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

 public function actionCreateemployee()
    {

        if(Yii::$app->user->can('fi-employee-create'))
        {
                  $acad = Yii::$app->session['currentId_academic_year'];
                    $ageCalculate= false;
                    $_hire_date='';
                    $_job_status='';
                    $_titles_id='';
                    $_program_id='';
                    $_department_id='';
                    
       $programs_leaders = infoGeneralConfig('programs_leaders');
       
        $model = new SrcPersons();
        $modelEmployeeInfo= new SrcEmployeeInfo();
        $modelCustomFieldData = new CustomFieldData();

         if(isset($_GET["from"])&&($_GET["from"]!=''))
		  {
			 if($_GET["from"]=='teach')
				{ //$criteria_cf= ['field_related_to' => 'teach'];
				}
			 elseif($_GET["from"]=='emp')
				{
					$modelDepartmentPerson = new SrcDepartmentInSchool();
			        $modelPersonTitles = new SrcTitles();
			        $modelProgram = new SrcProgram();

				 }
		   }




      if ($model->load(Yii::$app->request->post()) )
        {
                    $criteria_cf= ['field_related_to' => 'emp'];

			 if(isset($_GET["from"])&&($_GET["from"]!=''))
			   {
				  if($_GET["from"]=='teach')
					 {
					 	 $criteria_cf= ['field_related_to' => 'teach'];
					  }
				  elseif($_GET["from"]=='emp')
					 {
						  $criteria_cf= ['field_related_to' => 'emp'];


				            if(isset($_POST['SrcTitles']['id']) && ($_POST['SrcTitles']['id']!=null) )
				             {
				                $_titles_id = $_POST['SrcTitles']['id'];
				                
				                $modelPersonTitles->id = $_titles_id;


				              }

                          if(isset($_POST['SrcProgram']['id']) && ($_POST['SrcProgram']['id']!=null) )
				             {
				                $_program_id = $_POST['SrcProgram']['id'];
				                
				                $modelProgram->id = $_program_id;


				              }

				             if(isset($_POST['SrcDepartmentInSchool']['id']) && ($_POST['SrcDepartmentInSchool']['id']!=null) )
				             {
				                $_department_id = $_POST['SrcDepartmentInSchool']['id'];


				              }

					   }
				}

                  if(isset($_POST['SrcEmployeeInfo']['hire_date']) && ($_POST['SrcEmployeeInfo']['hire_date']!=null) )
				             {
				                $_hire_date = $_POST['SrcEmployeeInfo']['hire_date'];

				                $modelEmployeeInfo->hire_date = $_hire_date;
				              }

				            if(isset($_POST['SrcEmployeeInfo']['job_status']) && ($_POST['SrcEmployeeInfo']['job_status']!=null) )
				             {
				                $_job_status = $_POST['SrcEmployeeInfo']['job_status'];

				                $modelEmployeeInfo->job_status = $_job_status;
				              }
				              
				      if($model->file)
				         $model->file = UploadedFile::getInstance($model,'file');

             $dbTrans = Yii::$app->db->beginTransaction();

           if(isset($_POST['create']))
            {
		          $ok = false;




		           // if( (($_titles_id!='')&&($_GET["from"]=='emp')) ||($_GET["from"]=='teach') )
		           //  {
		               $automatic_code = infoGeneralConfig('automatic_code');

						if($model->active=='')
						 $model->setAttribute('active',2);

						$model->setAttribute('is_student',0);
						$model->setAttribute('date_created',date('Y-m-d'));
						$model->setAttribute('create_by',currentUser());

						   $model->file = UploadedFile::getInstance($model,'file');
						   $fileName = '';

		               if($model->file)
				              {
				              	$imageName = strtr( strtolower( trim(str_replace(" ","",$model->first_name) ) ), pa_daksan() ).strtr( strtolower( trim(str_replace(" ","",$model->last_name) ) ), pa_daksan() ).$model->id;


				              	  //save path la nan baz la
				              	  $model->image = $imageName.'.'.$model->file->extension;
				              	  $fileName = $model->image;

				              	  //resize imaj la


                                                  // Fn rezise imaj la

                                $last_name = trim($model->last_name );
                               $first_name = trim($model->first_name );
                                $model->setAttribute('first_name',$first_name );
                                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		         if( ($model->first_name!='')&&($model->last_name!='') ) 
		          {   
		            if ($model->save())
		                {
                             //code automatic
					     if($automatic_code ==1)
						   {
					        //if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
						     //{

								$cf='';
					         $cl='';

                               //first_name
							$explode_firstname=explode(" ",substr( trim($model->first_name) ,0));

				            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
							 {
						        $cf = strtoupper(  substr(strtr(  str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

							 }

							 //last_name
							$explode_lastname=explode(" ",substr( trim($model->last_name) ,0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
							 {
						        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

							 }
							else
							 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

							 }

							  $code_ = $cf.$cl.$model->id;


								  $model->setAttribute('id_number',$code_);

								  $model->save();
							  // }

						   }

                                $model->file->saveAs(Yii::getAlias('documents/photo_upload/'.$imageName.'.'.$model->file->extension));
		                	 if(($_hire_date!='')||($_job_status!=''))
		                	   {  //save in employee_info
		                	      $modelEmployeeInfo = new SrcEmployeeInfo();
		                	      $modelEmployeeInfo->hire_date = $_hire_date;
		                	      $modelEmployeeInfo->job_status = $_job_status;
		                	   	  $modelEmployeeInfo->employee = $model->id;
		                	   	  $modelEmployeeInfo->date_created = date('Y-m-d');
		                	   	  $modelEmployeeInfo->create_by = currentUser();
		                	   	  $modelEmployeeInfo->save();

		                	   	}

		                   if($_GET["from"]=='emp')
		                	 {
		                	  if(($_department_id!='')||($_department_id!=null))
		                	   {  //save in department_has_person
		                	      $modelDepartmentPerson = new DepartmentHasPerson();
		                	   	   $modelDepartmentPerson->department_id = $_department_id;
		                	   	   $modelDepartmentPerson->employee = $model->id;
		                	   	   $modelDepartmentPerson->academic_year = $acad;
		                	   	   $modelDepartmentPerson->date_created = date('Y-m-d');
		                	   	   $modelDepartmentPerson->created_by = currentUser();

		                	   	   if($modelDepartmentPerson->save())
		                	   	      $ok = true;

		                	   	}


		                	   if(($_titles_id!='')||($_titles_id!=null))
		                	   {  //gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
		                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
		                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
		                	   	    {
			                	   	   //save in persons_has_titles
			                	   	    $modelPersonTitles = new SrcPersonsHasTitles();
			                	   	    $modelPersonTitles->titles_id = $_titles_id;
			                	   	    $modelPersonTitles->persons_id = $model->id;
			                	   	    $modelPersonTitles->academic_year = $acad;
	
			                	   	    if($modelPersonTitles->save())
			                	   	      $ok = true;
			                	   	     else
			                	   	       $ok=false;
		                	   	    }
		                	   	    
		                	   	}
		                	   	
		                	   	
		                	   	if(($_program_id!='')||($_program_id!=null))
		                	   {  //save in programs_leaders
		                	   	    $modelProgramLeaders = new SrcProgramsLeaders();
		                	   	    $modelProgramLeaders->program_id = $_program_id;
		                	   	    $modelProgramLeaders->person_id = $model->id;
		                	   	    $modelProgramLeaders->date_created = date('Y-m-d');
		                	   	    $modelProgramLeaders->created_by = currentUser();

		                	   	    if($modelProgramLeaders->save())
		                	   	      $ok = true;
		                	   	     else
		                	   	       $ok=false;
		                	   	}


		                	   if( (($_titles_id=='')||($_titles_id==null))&&(($_program_id=='')||($_program_id==null))&&(($_department_id=='')||($_department_id==null)) )
		                	     $ok = true;
		                	 }
		                	  elseif($_GET["from"]=='teach')
		                	     $ok = true;

		                	 // Demarrer enregistrement champs personalisables
						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						            }
						        }
						        // Terminer Enregistrement champs personalisables


				            if($ok==true)
				              {
                                                $dbTrans->commit();

                                            $explode_lastname=explode(" ",substr( trim($model->last_name),0));

								            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
								              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
								            else
								              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;

				                            $password = "password";

				                            $user = new Signup();
				                            $user->username = $username;
								            $user->person_id = $model->id;
								            $user->is_parent = 0;
								               if($model->email!='')
								               $user->email = $model->email;

								            $user->password=$password;

								            $user->signup_from_person();


									         if($_GET["from"]=='teach')//si c teacher
									           {
									             $modelNewUser=new User;
                                                  $user = $modelNewUser->findByPersonId($model->id);
									                $command3 = Yii::$app->db->createCommand();
									              $command3->insert('auth_assignment', [
																'item_name'=>"teacher",
																'user_id'=>$user->id,
															])->execute();
												}


		                	      return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'teach']);
				               }
				             else
				                $dbTrans->rollback();

		                }
		              else
		                {    $dbTrans->rollback();
		                     $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName;
                                   if(file_exists($file_to_delete))
                                       unlink($file_to_delete);
		                 }

		            }

				              }else{

		               $last_name = trim($model->last_name );
                               $first_name = trim($model->first_name );
                                $model->setAttribute('first_name',$first_name );
                                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		         if( ($model->first_name!='')&&($model->last_name!='') ) 
		          {   
		            if ($model->save())
		                {
		                	    //code automatic
					     if($automatic_code ==1)
						   {
					        //if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
						     //{

								$cf='';
					         $cl='';

                               //first_name
							$explode_firstname=explode(" ",substr( trim($model->first_name),0));

				            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
							 {
						        $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

							 }

							 //last_name
							$explode_lastname=explode(" ",substr( trim($model->last_name),0));

				            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
							 {
						        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

							 }
							else
							 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

							 }

							  $code_ = $cf.$cl.$model->id;


								  $model->setAttribute('id_number',$code_);

								  $model->save();
							  // }

						   }

		                	 if(($_hire_date!='')||($_job_status!=''))
		                	   {  //save in employee_info
		                	      $modelEmployeeInfo = new SrcEmployeeInfo();
		                	      $modelEmployeeInfo->hire_date = $_hire_date;
		                	      $modelEmployeeInfo->job_status = $_job_status;
		                	   	  $modelEmployeeInfo->employee = $model->id;
		                	   	  $modelEmployeeInfo->date_created = date('Y-m-d');
		                	   	  $modelEmployeeInfo->create_by = currentUser();
		                	   	  $modelEmployeeInfo->save();

		                	   	}

		                   if($_GET["from"]=='emp')
		                	 {
		                	  if(($_department_id!='')||($_department_id!=null))
		                	   {  //save in department_has_person
		                	      $modelDepartmentPerson = new DepartmentHasPerson();
		                	   	   $modelDepartmentPerson->department_id = $_department_id;
		                	   	   $modelDepartmentPerson->employee = $model->id;
		                	   	   $modelDepartmentPerson->academic_year = $acad;
		                	   	   $modelDepartmentPerson->date_created = date('Y-m-d');
		                	   	   $modelDepartmentPerson->created_by = currentUser();

		                	   	   if($modelDepartmentPerson->save())
		                	   	      $ok = true;
		                	   	}

		                	   if(($_titles_id!='')||($_titles_id!=null))
		                	   {  
		                	   	//gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
		                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
		                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
		                	   	    {
			                	   	   //save in persons_has_titles
			                	   	    $modelPersonTitles = new SrcPersonsHasTitles();
			                	   	    $modelPersonTitles->titles_id = $_titles_id;
			                	   	    $modelPersonTitles->persons_id = $model->id;
			                	   	    $modelPersonTitles->academic_year = $acad;
	
			                	   	    if($modelPersonTitles->save())
			                	   	      $ok = true;
			                	   	    else
			                	   	      $ok = false;
		                	   	    }
		                	   	    
		                	   	}
	
		                	   	
		                	   	if(($_program_id!='')||($_program_id!=null))
		                	   {  //save in programs_leaders
		                	   	    $modelProgramLeaders = new SrcProgramsLeaders();
		                	   	    $modelProgramLeaders->program_id = $_program_id;
		                	   	    $modelProgramLeaders->person_id = $model->id;
		                	   	    $modelProgramLeaders->date_created = date('Y-m-d');
		                	   	    $modelProgramLeaders->created_by = currentUser();

		                	   	    if($modelProgramLeaders->save())
		                	   	      $ok = true;
		                	   	     else
		                	   	       $ok=false;
		                	   	}


		                	   if( (($_titles_id=='')||($_titles_id==null))&&(($_program_id=='')||($_program_id==null))&&(($_department_id=='')||($_department_id==null)) )
		                	     $ok = true;

		                	 }
		                	  elseif($_GET["from"]=='teach')
		                	     $ok = true;

		                	 // Demarrer enregistrement champs personalisables
						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						            }
						        }
						        // Terminer Enregistrement champs personalisables


				            if($ok==true)
				              {  $dbTrans->commit();

				                    $explode_lastname=explode(" ",substr( trim($model->last_name),0));

								            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
								              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
								            else
								              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;

				                            $password = "password";

				                            $user = new Signup();
				                            $user->username = $username;
								            $user->person_id = $model->id;
								            $user->is_parent = 0;
								               if($model->email!='')
								               $user->email = $model->email;

								            $user->password=$password;

								            $user->signup_from_person();


									         if($_GET["from"]=='teach')//si c teacher
									           {
									             $modelNewUser=new User;
                                                  $user = $modelNewUser->findByPersonId($model->id);
									                 $command3 = Yii::$app->db->createCommand();
									              $command3->insert('auth_assignment', [
																'item_name'=>"teacher",
																'user_id'=>$user->id,
															])->execute();
												}


		                	      return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'emp']);
				               }
				             else
				                $dbTrans->rollback();

		                }
		              else
		                {    $dbTrans->rollback();
		                     $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName;
                                   if(file_exists($file_to_delete))
                                       unlink($file_to_delete);
		                 }
		                 
				  }

                                              }
				  /*    }
		             else
				       {
				            Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',' Employee must have a title.') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				       }
				       */


             }


        }


        if(isset($_GET["from"])&&($_GET["from"]!=''))
		  {
			 if($_GET["from"]=='teach')
				{
					return $this->render('create_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				}
			 elseif($_GET["from"]=='emp')
				{
					return $this->render('create_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,

		            ]);
				 }
		   }



        }
      else
        {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);


          }
    }


  public function actionUpdateemployee($id)
    {
      if(Yii::$app->user->can('fi-employee-update'))
       {
	        $acad = Yii::$app->session['currentId_academic_year'];
	                    $ageCalculate= false;
	                    $_hire_date='';
	                    $_job_status='';
	                    $_titles_id='';
	                    $_program_id='';
	                    $_department_id='';

	                    $old_title ='';
	                    $old_program = '';
	                    
	          $programs_leaders = infoGeneralConfig('programs_leaders');

	        $model = new SrcPersons();
	        $modelEmployeeInfo= new SrcEmployeeInfo();
	        $modelCustomFieldData = new CustomFieldData();


	        $model = $this->findModel($id);

	        $fileName_old = $model->image;

	        $employeeInfo = SrcEmployeeInfo::find()->where(['employee'=>$id])->all();
	        if($employeeInfo==null)
	          $modelEmployeeInfo= new SrcEmployeeInfo();
	        else
	         {    foreach($employeeInfo as $empInfo)
                       $_job_status = $empInfo->job_status;
                       $_hire_date = $empInfo->hire_date ;

                       $modelEmployeeInfo = new SrcEmployeeInfo();
                       $modelEmployeeInfo->job_status = $_job_status;
                       $modelEmployeeInfo->hire_date = $_hire_date;

	           }



	         if(isset($_GET["from"])&&($_GET["from"]!=''))
			  {
				
						///Working department
						$personDepartment = DepartmentHasPerson::find()->where(['employee'=>$id, 'academic_year'=>$acad])->all();
				        if($personDepartment==null)
				           $modelDepartmentPerson = new SrcDepartmentInSchool();
				        else
				          {
				          	foreach($personDepartment as $department)
				          	   $_department_id = $department->department_id;

				          	  $modelDepartmentPerson = new SrcDepartmentInSchool();
				          	   $modelDepartmentPerson->id = $_department_id;
				           }

					  ///Titles
						$personTitles = SrcPersonsHasTitles::find()->where(['persons_id'=>$id, 'academic_year'=>$acad])->all();
				        if($personTitles==null)
				           $modelPersonTitles = new SrcTitles();
				        else
				          {
				          	foreach($personTitles as $title)
				          	   $_titles_id = $title->titles_id;

				          	  $old_title = $_titles_id;

				          	  $modelPersonTitles = new SrcTitles();
				          	   $modelPersonTitles->id = $_titles_id;
				           }
                        
                        //program
                        $personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1 ])->all();
				        if($personProgram==null)
				           $modelProgram = new SrcProgram();
				        else
				          {
				          	foreach($personProgram as $program)
				          	   $_program_id = $program->program_id;

				          	  $old_program = $_program_id;

				          	   $modelProgram = new SrcProgram();
				          	   $modelProgram->id = $_program_id;
				           }


					
			   }


      if ($model->load(Yii::$app->request->post()) )
        {
                    $criteria_cf= ['field_related_to' => 'emp'];

			 if(isset($_GET["from"])&&($_GET["from"]!=''))
			   {
				  if($_GET["from"]=='teach')
					 {
					 	 $criteria_cf= ['field_related_to' => 'teach'];
					  }
				  elseif($_GET["from"]=='emp')
					 {
						$criteria_cf= ['field_related_to' => 'emp'];
					 }

				            if(isset($_POST['SrcTitles']['id']) && ($_POST['SrcTitles']['id']!=null) )
				             {
				                $_titles_id = $_POST['SrcTitles']['id'];
				                
				                $modelPersonTitles->id = $_titles_id ;

				              }

				              if(isset($_POST['SrcProgram']['id']) && ($_POST['SrcProgram']['id']!=null) )
				             {
				                $_program_id = $_POST['SrcProgram']['id'];
				                 
				                 
				                 $modelProgram->id = $_program_id ;

				              }

				             if(isset($_POST['SrcDepartmentInSchool']['id']) && ($_POST['SrcDepartmentInSchool']['id']!=null) )
				             {
				                $_department_id = $_POST['SrcDepartmentInSchool']['id'];

				              }

					   
				}

                  if(isset($_POST['SrcEmployeeInfo']['hire_date']) && ($_POST['SrcEmployeeInfo']['hire_date']!=null) )
				             {
				                $_hire_date = $_POST['SrcEmployeeInfo']['hire_date'];

				                $modelEmployeeInfo->hire_date = $_hire_date;
				              }

				            if(isset($_POST['SrcEmployeeInfo']['job_status']) && ($_POST['SrcEmployeeInfo']['job_status']!=null) )
				             {
				                $_job_status = $_POST['SrcEmployeeInfo']['job_status'];

				                $modelEmployeeInfo->job_status = $_job_status;
				              }

             $dbTrans = Yii::$app->db->beginTransaction();

           if(isset($_POST['update']))
             {
		        $ageCalculate= false;
		        $ok = false;
		        if(ageCalculator($model->birthday)>=18)
  	                  $ageCalculate= true;

  	                 $age_message= Yii::t('app','Employee may have at least').' 18 ';



		        if((($model->birthday!='')&&($ageCalculate==true ))||($model->birthday==''))
				  {
		           
			            $automatic_code = infoGeneralConfig('automatic_code');
							//code automatic
						     if($automatic_code ==1)
							   {
						        //if(($model->create_by =='siges_migration_tool')&&($model->id_number==''))
							     //{

									$cf='';
						         $cl='';

	                               //first_name
								$explode_firstname=explode(" ",substr( trim($model->first_name),0));

					            if(isset($explode_firstname[1])&&($explode_firstname[1]!=''))
								 {
							        $cf = strtoupper(  substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_firstname[1]),pa_daksan() ), 0,1)  );

								 }
								else
								 {  $cf = strtoupper( substr(strtr( str_replace(" ","",$explode_firstname[0]),pa_daksan() ), 0,2)  );

								 }

								 //last_name
								$explode_lastname=explode(" ",substr( trim($model->last_name),0));

					            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
								 {
							        $cl = strtoupper(  substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,1).substr(strtr( str_replace(" ","",$explode_lastname[1]),pa_daksan() ), 0,1) );

								 }
								else
								 {  $cl = strtoupper( substr(strtr( str_replace(" ","",$explode_lastname[0]),pa_daksan() ), 0,2)  );

								 }

								  $code_ = $cf.$cl.$model->id;


									  $model->setAttribute('id_number',$code_);

								  // }

							   }


							if($model->active=='')
							 $model->setAttribute('active',2);

							$model->setAttribute('is_student',0);
							$model->setAttribute('date_created',date('Y-m-d'));
							$model->setAttribute('create_by',currentUser());

						   $model->file = UploadedFile::getInstance($model,'file');
						   $fileName = '';

		               if($model->file)
				              {

				              	$imageName = strtr( strtolower( trim(str_replace(" ","",$model->first_name) ) ), pa_daksan() ).strtr( strtolower( trim(str_replace(" ","",$model->last_name) ) ), pa_daksan() ).$model->id;


				              	  //save path la nan baz la
				              	  $model->image = $imageName.'.'.$model->file->extension;

				              	  //resize imaj la


                                                  // Fn resize imaj la

                           $_hire_date = $_POST['SrcEmployeeInfo']['hire_date'];
		                   $_job_status = $_POST['SrcEmployeeInfo']['job_status'];

		                 // if($_GET["from"]=='emp')
		                	// {
			                  if(isset($_POST['SrcTitles']['id']) )
			                     $_titles_id = $_POST['SrcTitles']['id'];
			                   if(isset($_POST['SrcDepartmentInSchool']['id']) )
			                     $_department_id = $_POST['SrcDepartmentInSchool']['id'];
			                  if(isset($_POST['SrcProgram']['id']) && ($_POST['SrcProgram']['id']!=null) )
				                 $_program_id = $_POST['SrcProgram']['id'];
		                	 //}

		                 $last_name = trim($model->last_name );
                               $first_name = trim($model->first_name );
                                $model->setAttribute('first_name',$first_name );
                                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		            if ($model->save())
		                {
		                    $modelCard = SrcIdcard::find()->where('person_id='.$model->id)->all();
                                    if($modelCard!=null)
                                    { foreach($modelCard as $card)
                                       {
                                           $newModelCard = SrcIdcard::findOne($card->id);
                                           $newModelCard->prenom = $model->first_name;
                                           $newModelCard->nom = $model->last_name;
                                           $newModelCard->sexe = $model->gender;
                                           $newModelCard->image_name = $model->image;
                                           
                                           $newModelCard->save();
                                        
                                       }
                                    }
                                            
                                         if(($_hire_date!='')||($_job_status!=''))
		                	   {  //save in employee_info
		                	      $employeeInfo = SrcEmployeeInfo::find()->where(['employee'=>$id])->all();
							        if($employeeInfo==null)
							         { //new record
							              $employeeInfo = new SrcEmployeeInfo();
				                	       if($_hire_date!='')
				                	         $employeeInfo->hire_date = $_hire_date;
				                	      if($_job_status!='')
				                	        $employeeInfo->job_status = $_job_status;
				                	   	  $employeeInfo->employee = $model->id;
				                	   	  $employeeInfo->date_created = date('Y-m-d');
				                	   	  $employeeInfo->create_by = currentUser();
				                	   	  $employeeInfo->save();
							          }
							        else
							         {  $id_emp_info = 0;
							            foreach($employeeInfo as $empInfo)
							              $id_emp_info = $empInfo->id;

							         	//load apropriate model
							               $modelEmployeeInfo = SrcEmployeeInfo::findOne($id_emp_info);

							            // update record

							                     if($_job_status!='')
							                       $modelEmployeeInfo->setAttribute('job_status', $_job_status);
							                     if($_hire_date!='')
										            $modelEmployeeInfo->setAttribute('hire_date',$_hire_date);
										         $modelEmployeeInfo->date_created = date('Y-m-d');
				                	   	         $modelEmployeeInfo->create_by = currentUser();
				                	   	         $modelEmployeeInfo->save();


							         	}







		                	   	}

		                 
		                	  if(($_department_id!='')||($_department_id!=null))
		                	   {  //save in department_has_person

		                	      ///Working department
									$personDepartment = DepartmentHasPerson::find()->where(['employee'=>$id, 'academic_year'=>$acad])->all();
							        if($personDepartment==null)
							          { //new record
							           	  $personDepartment = new DepartmentHasPerson();
				                	   	   $personDepartment->department_id = $_department_id;
				                	   	   $personDepartment->employee = $model->id;
				                	   	   $personDepartment->academic_year = $acad;
				                	   	   $personDepartment->date_created = date('Y-m-d');
				                	   	   $personDepartment->created_by = currentUser();

				                	   	   if($personDepartment->save())
							          	     $ok = true;


							           }
							        else
							          {   $id_pers_dep = 0;
							            foreach($personDepartment as $personDep)
							              $id_pers_dep = $personDep->id;

							         	//load apropriate model
							               $modelPersonDepartment = DepartmentHasPerson::findOne($id_pers_dep);

                                         //update record
							          	  $modelPersonDepartment->department_id = $_department_id;
							          	  $modelPersonDepartment->date_updated = date('Y-m-d');
				                	   	   $modelPersonDepartment->updated_by = currentUser();

							          	  if($modelPersonDepartment->save())
							          	     $ok = true;

							           }




		                	   	}

		                	   if(($_titles_id!='')||($_titles_id!=null))
		                	   {  //save in persons_has_titles
		                	   	   ///Titles
									$personTitles = SrcPersonsHasTitles::find()->where(['persons_id'=>$id, 'academic_year'=>$acad])->all();
							        if($personTitles==null)
							          { //new record
									      //gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
				                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
				                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
				                	   	    {
			                	   	       	    $personTitles = new SrcPersonsHasTitles();
									             $personTitles->titles_id = $_titles_id;
						                	   	    $personTitles->persons_id = $model->id;
						                	   	    $personTitles->academic_year = $acad;
		
						                	   	    if($personTitles->save())
						                	   	      $ok = true;
						                	   	    else
						                	   	      $ok = false;
						                	   	      
				                	   	    }
				                	   	    
				                	   	 //si se pa $programs_leaders vide $_program_id
				                	   	 if($modelTitl->title_name!=$programs_leaders)
				                	   	   {    $_program_id ='';
				                	   	       //sil te gen yon program,delete li
				                	   	       $personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
				                	   	       
				                	   	         if($personProgram!=null)
					                	   	       { 
					                	   	       	   $prog = '';
					                	   	       	   foreach($personProgram as $personProg)
					                	   	               $prog = $personProg->id;
					                	   	               
					                	   	            $modelProgramsLeaders = SrcProgramsLeaders::findOne($prog);
					                	   	             $modelProgramsLeaders->delete();
					                	   	        }
				                	   	   
				                	   	     }
				                	   	 
							           }
							        else
							          {   $id_tit = 0;
							              $academ=0;
							            foreach($personTitles as $personTit)
							              {$id_tit = $personTit->titles_id;
							                $academ= $personTit->academic_year;
							              }

							         	//load apropriate model
							               $modelPersonTitle = SrcPersonsHasTitles::findOne([$model->id,$id_tit,$academ]);

	                                        //gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
				                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
				                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
				                	   	    {
			                	   	         //update record
								          	 $modelPersonTitle->titles_id = $_titles_id;
								          	  if($modelPersonTitle->save())
					                	   	      $ok = true;
					                	   	    else
					                	   	      $ok = false;
				                	   	    }
				                	   	    
				                	   	    //si se pa $programs_leaders vide $_program_id
				                	   	    if($modelTitl->title_name!=$programs_leaders)
					                	   	   {    $_program_id ='';
					                	   	       //sil te gen yon program,delete li
					                	   	       $personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
					                	   	       
					                	   	       if($personProgram!=null)
					                	   	       { 
					                	   	       	   $prog = '';
					                	   	       	   foreach($personProgram as $personProg)
					                	   	               $prog = $personProg->id;
					                	   	               
					                	   	            $modelProgramsLeaders = SrcProgramsLeaders::findOne($prog);
					                	   	             $modelProgramsLeaders->delete();
					                	   	        }
					                	   	   
					                	   	     }

							           }





		                	   	}
		                	   	
		                	   	if(($_program_id!='')||($_program_id!=null))
		                	   {  //save in programs_leaders
		                	   	   ///Program
									$personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
							        if($personProgram==null)
							          { //new record
							          	$personProgram = new SrcProgramsLeaders();
							             $personProgram->program_id = $_program_id;
				                	   	    $personProgram->person_id = $model->id;
				                	   	    $personProgram->date_created = date('Y-m-d');
				                	   	    $personProgram->created_by = currentUser();

				                	   	    if($personProgram->save())
				                	   	      $ok = true;
				                	   	    else
				                	   	      $ok = false;
							           }
							        else
							          {   $id_pers_prog = 0;
							              
							            foreach($personProgram as $personProg)
							              {$id_pers_prog = $personProg->id;
							                
							              }

							         	//load apropriate model
							               $modelPersonProgram = SrcProgramsLeaders::findOne($id_pers_prog);

                                         //update record
							          	 $modelPersonProgram->setAttribute('program_id', $_program_id);
							          	 $modelPersonProgram->setAttribute('date_updated', date('Y-m-d') );
				                	   	    $modelPersonProgram->setAttribute('updated_by', currentUser());
							          	  if($modelPersonProgram->save())
				                	   	      $ok = true;
				                	   	    else
				                	   	      $ok = false;

							           }





		                	   	}

		                	   	if( (($_titles_id=='')||($_titles_id==null))&&(($_program_id=='')||($_program_id==null))&&(($_department_id=='')||($_department_id==null)) )
		                	     $ok = true;
		               
		                	  // Demarrer mise a jours champs personalisables

						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        if(isset($_GET['id'])){$id_pers = $_GET['id'];}
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData = $modelCustomFieldData->loadCustomFieldValue($id_pers,$cd->id);
						                if($modelCustomFieldData!=null){ // S'il y a deja des donnees on fait la mise a jour
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }else{ // S'il n'y a pas de donnees on ajoute
						                $modelCustomFieldData = new CustomFieldData;
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }
						            }
						        }
						        // Terminer mise a jour des champs personalisables



				            if($ok==true)
				              {
                                               $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName_old;
                                                if(file_exists($file_to_delete))
                                                unlink($file_to_delete);
				              	 $model->file->saveAs(Yii::getAlias('documents/photo_upload/'.$imageName.'.'.$model->file->extension));
                                              $dbTrans->commit();


                                                 $explode_lastname=explode(" ",substr( trim($model->last_name),0));

														            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
														            else
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;



										                            $modelUser=new User;
                                                                    $user = $modelUser->findByPersonId($model->id);

									                         if($user!=null)// on fait 1 update
									                           {

									                              $command2 = Yii::$app->db->createCommand();
										                          $command2->update('user', ['username' => $username,   ], ['person_id'=>$model->id ])
									                                               ->execute();
									                            }
									                          else // enter 1 new record
									                            {  $command3 = Yii::$app->db->createCommand();

									                                $password = "password";

																	$new_user = new Signup();
																	$new_user->username = $username;
																	$new_user->person_id = $model->id;
																	$new_user->is_parent = 0;
																		if($model->email!='')
																			$new_user->email = $model->email;
																	$new_user->password=$password;

																	$new_user->signup_from_person();



									                              if($_GET["from"]=='teach')//si c teacher
									                            	{
									                                     $modelNewUser=new User;
                                                                         $user = $modelNewUser->findByPersonId($model->id);

									                                    $command3->insert('auth_assignment', [
																				    'item_name'=>"teacher",
																			'user_id'=>$user->id,
																			])->execute();

																	}

									                            }

                                     if($_GET["from"]=='teach')//si c teacher
                                        return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'teach']);
                                     elseif($_GET["from"]=='emp')//si c employee
                                       return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'emp']);


				               }
				             else
				                $dbTrans->rollback();

		                }
		              else
		                {
                                  $dbTrans->rollback();

		                 }
				              }else{

		                   $_hire_date = $_POST['SrcEmployeeInfo']['hire_date'];
		                   $_job_status = $_POST['SrcEmployeeInfo']['job_status'];

		                   //if($_GET["from"]=='emp')
		                	// {
		                	 	if(isset($_POST['SrcTitles']['id']) )
		                	 	   $_titles_id = $_POST['SrcTitles']['id'];
		                        if(isset($_POST['SrcDepartmentInSchool']['id']) )
		                           $_department_id = $_POST['SrcDepartmentInSchool']['id'];
		                        if(isset($_POST['SrcProgram']['id']) && ($_POST['SrcProgram']['id']!=null) )
				                 $_program_id = $_POST['SrcProgram']['id'];
		                	// }

		                 $last_name = trim($model->last_name );
                               $first_name = trim($model->first_name );
                                $model->setAttribute('first_name',$first_name );
                                $model->setAttribute('last_name',strtr( strtoupper($last_name), capital_aksan() ) );

		            if ($model->save())
		                {
                                     $modelCard = SrcIdcard::find()->where('person_id='.$model->id)->all();
                                    if($modelCard!=null)
                                    { foreach($modelCard as $card)
                                       {
                                           $newModelCard = SrcIdcard::findOne($card->id);
                                           $newModelCard->prenom = $model->first_name;
                                           $newModelCard->nom = $model->last_name;
                                           $newModelCard->sexe = $model->gender;
                                           $newModelCard->image_name = $model->image;
                                           
                                           $newModelCard->save();
                                        
                                       }
                                    }
                                
		                	  if(($_hire_date!='')||($_job_status!=''))
		                	   {  //save in employee_info
		                	      $employeeInfo = SrcEmployeeInfo::find()->where(['employee'=>$id])->all();
							        if($employeeInfo==null)
							         { //new record
							              $employeeInfo = new SrcEmployeeInfo();
				                	       if($_hire_date!='')
				                	         $employeeInfo->hire_date = $_hire_date;
				                	      if($_job_status!='')
				                	        $employeeInfo->job_status = $_job_status;
				                	   	  $employeeInfo->employee = $model->id;
				                	   	  $employeeInfo->date_created = date('Y-m-d');
				                	   	  $employeeInfo->create_by = currentUser();
				                	   	  $employeeInfo->save();
							          }
							        else
							         {  $id_emp_info = 0;
							            foreach($employeeInfo as $empInfo)
							              $id_emp_info = $empInfo->id;

							         	//load apropriate model
							               $modelEmployeeInfo = SrcEmployeeInfo::findOne($id_emp_info);

							            // update record

							                     if($_job_status!='')
							                       $modelEmployeeInfo->setAttribute('job_status', $_job_status);
							                     if($_hire_date!='')
										            $modelEmployeeInfo->setAttribute('hire_date',$_hire_date);
										         $modelEmployeeInfo->date_created = date('Y-m-d');
				                	   	         $modelEmployeeInfo->create_by = currentUser();
				                	   	         $modelEmployeeInfo->save();


							         	}







		                	   	}

		                  
		                	  if(($_department_id!=''))
		                	   {  //save in department_has_person

		                	      ///Working department
									$personDepartment = DepartmentHasPerson::find()->where(['employee'=>$id, 'academic_year'=>$acad])->all();
							        if($personDepartment==null)
							          { //new record
							           	  $personDepartment = new DepartmentHasPerson();
				                	   	   $personDepartment->department_id = $_department_id;
				                	   	   $personDepartment->employee = $model->id;
				                	   	   $personDepartment->academic_year = $acad;
				                	   	   $personDepartment->date_created = date('Y-m-d');
				                	   	   $personDepartment->created_by = currentUser();

				                	   	   if($personDepartment->save())
							          	       $ok = true;

							           }
							        else
							          {   $id_pers_dep = 0;
							            foreach($personDepartment as $personDep)
							              $id_pers_dep = $personDep->id;

							         	//load apropriate model
							               $modelPersonDepartment = DepartmentHasPerson::findOne($id_pers_dep);

                                         //update record
							          	  $modelPersonDepartment->department_id = $_department_id;
							          	  $modelPersonDepartment->date_updated = date('Y-m-d');
				                	   	   $modelPersonDepartment->updated_by = currentUser();

							          	  if($modelPersonDepartment->save())
							          	     $ok = true;
							           }




		                	   	}

		                	   if(($_titles_id!=''))
		                	   {  //save in persons_has_titles
		                	   	   ///Titles
									$personTitles = SrcPersonsHasTitles::find()->where(['persons_id'=>$id, 'academic_year'=>$acad])->all();
							        if($personTitles==null)
							          { //new record
							          	//gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
				                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
				                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
				                	   	    {
			                	   	       	    $personTitles = new SrcPersonsHasTitles();
									             $personTitles->titles_id = $_titles_id;
						                	   	    $personTitles->persons_id = $model->id;
						                	   	    $personTitles->academic_year = $acad;
		
						                	   	    if($personTitles->save())
						                	   	      $ok = true;
						                	   	    else
						                	   	      $ok = false;
						                	   	      
				                	   	    }
				                	   	    
				                	   	 //si se pa $programs_leaders vide $_program_id
				                	   	 if($modelTitl->title_name!=$programs_leaders)
				                	   	   {    $_program_id ='';
				                	   	       //sil te gen yon program,delete li
				                	   	       $personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
				                	   	       
				                	   	       if($personProgram!=null)
					                	   	       { 
					                	   	       	   $prog = '';
					                	   	       	   foreach($personProgram as $personProg)
					                	   	               $prog = $personProg->id;
					                	   	               
					                	   	            $modelProgramsLeaders = SrcProgramsLeaders::findOne($prog);
					                	   	             $modelProgramsLeaders->delete();
					                	   	        }
				                	   	   
				                	   	     }

							           }
							        else
							          {   $id_tit = 0;
							              $academ=0;
							            foreach($personTitles as $personTit)
							              {$id_tit = $personTit->titles_id;
							                $academ= $personTit->academic_year;
							              }

							         	//load apropriate model
							               $modelPersonTitle = SrcPersonsHasTitles::findOne([$model->id,$id_tit,$academ]);
                                                
                                         //gade si tit la se $programs_leaders pou pa save sil pa chwazi pwogram lan
				                	   	  $modelTitl = SrcTitles::findOne($_titles_id);
				                	   	  if( ($modelTitl->title_name!=$programs_leaders)||(($modelTitl->title_name==$programs_leaders)&&(($_program_id!='')||($_program_id!=null) ) ) )
				                	   	    {
			                	   	         //update record
								          	 $modelPersonTitle->titles_id = $_titles_id;
								          	  if($modelPersonTitle->save())
					                	   	      $ok = true;
					                	   	    else
					                	   	      $ok = false;
				                	   	    }
				                	   	    
				                	   	    //si se pa $programs_leaders vide $_program_id
				                	   	    if($modelTitl->title_name!=$programs_leaders)
					                	   	   {    $_program_id ='';
					                	   	       //sil te gen yon program,delete li
					                	   	       $personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
					                	   	       
					                	   	       if($personProgram!=null)
					                	   	       { 
					                	   	       	   $prog = '';
					                	   	       	   foreach($personProgram as $personProg)
					                	   	               $prog = $personProg->id;
					                	   	               
					                	   	            $modelProgramsLeaders = SrcProgramsLeaders::findOne($prog);
					                	   	             $modelProgramsLeaders->delete();
					                	   	        }
					                	   	   
					                	   	     }


							           }





		                	   	}
		                	   	
		                	   if(($_program_id!=''))
		                	   {  //save in programs_leaders
		              	   	  
		                	   	   ///Program
									$personProgram = SrcProgramsLeaders::find()->where(['person_id'=>$id, 'old_new'=>1])->all();
							        if($personProgram==null)
							          { //new record
							          	$personProgram = new SrcProgramsLeaders();
							             $personProgram->program_id = $_program_id;
				                	   	    $personProgram->person_id = $model->id;
				                	   	    $personProgram->date_created = date('Y-m-d');
				                	   	     $personProgram->created_by = currentUser();

				                	   	    if($personProgram->save())
				                	   	      $ok = true;
				                	   	    else
				                	   	      $ok = false;
							           }
							        else
							          {   $id_pers_prog = 0;
							              
							            foreach($personProgram as $personProg)
							              {$id_pers_prog = $personProg->id;
							                
							              }

							         	//load apropriate model
							               $modelPersonProgram = SrcProgramsLeaders::findOne($id_pers_prog);
 
                                         //update record
							          	 $modelPersonProgram->setAttribute('program_id', $_program_id);
							          	 $modelPersonProgram->setAttribute('date_updated', date('Y-m-d') );
				                	   	    $modelPersonProgram->setAttribute('updated_by', currentUser());
							          	  if($modelPersonProgram->save())
				                	   	      $ok = true;
				                	   	    else
				                	   	      $ok = false;

							           }





		                	   	}
		                                         
		                	  // Demarrer mise a jours champs personalisables

						        $cfData = SrcCustomField::find()->where($criteria_cf)->all();
						        if(isset($_GET['id'])){$id_pers = $_GET['id'];}
						        foreach($cfData as $cd){
						            if(isset($_POST[$cd->field_name])){
						                $customFieldValue = $_POST[$cd->field_name];
						                $modelCustomFieldData = $modelCustomFieldData->loadCustomFieldValue($id_pers,$cd->id);
						                if($modelCustomFieldData!=null){ // S'il y a deja des donnees on fait la mise a jour
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }else{ // S'il n'y a pas de donnees on ajoute
						                $modelCustomFieldData = new CustomFieldData;
						                $modelCustomFieldData->setAttribute('field_link', $cd->id);
						                $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
						                $modelCustomFieldData->setAttribute('object_id', $model->id);
						                $modelCustomFieldData->save();
						                unset($modelCustomFieldData);
						                $modelCustomFieldData = new CustomFieldData;
						                }
						            }
						        }
						        // Terminer mise a jour des champs personalisables



				            if($ok==true)
				              {
                                                 $file_to_delete = Yii::getAlias('@web').'/documents/photo_upload/'.$fileName_old;
                                                    if(file_exists($file_to_delete))
                                                        unlink($file_to_delete);
                                                    if(isset($imageName)){
                                                        $model->file->saveAs(Yii::getAlias('documents/photo_upload/'.$imageName.'.'.$model->file->extension));
                                                    }else{
                                                        $model->file = $model->file;
                                                    }
                                                $dbTrans->commit();


                                                  $explode_lastname=explode(" ",substr( trim($model->last_name),0));

														            if(isset($explode_lastname[1])&&($explode_lastname[1]!=''))
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).'_'.strtr( strtolower( str_replace(" ","",$explode_lastname[1]) ), pa_daksan() ).$model->id;
														            else
														              $username= strtr( strtolower( str_replace(" ","",$explode_lastname[0]) ), pa_daksan() ).$model->id;



										                            $modelUser=new User;
                                                                    $user = $modelUser->findByPersonId($model->id);

									                         if($user!=null)// on fait 1 update
									                           {

									                              $command2 = Yii::$app->db->createCommand();
										                          $command2->update('user', ['username' => $username,   ], ['person_id'=>$model->id ])
									                                               ->execute();
									                            }
									                          else // enter 1 new record
									                            {  $command3 = Yii::$app->db->createCommand();

									                                $password = "password";

																	$new_user = new Signup();
																	$new_user->username = $username;
																	$new_user->person_id = $model->id;
																	$new_user->is_parent = 0;
																		if($model->email!='')
																			$new_user->email = $model->email;
																	$new_user->password=$password;

																	$new_user->signup_from_person();



									                              if($_GET["from"]=='teach')//si c teacher
									                            	{
									                                     $modelNewUser=new User;
                                                                         $user = $modelNewUser->findByPersonId($model->id);

									                                    $command3->insert('auth_assignment', [
																				    'item_name'=>"teacher",
																			'user_id'=>$user->id,
																			])->execute();

																	}

									                            }




		                	       if($_GET["from"]=='teach')//si c teacher
                                        return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'teach']);
                                     elseif($_GET["from"]=='emp')//si c employee
                                       return $this->redirect(['/fi/persons/moredetailsemployee', 'id' => $model->id,'is_stud'=>0,'from'=>'emp']);
				               }
				             else
				                $dbTrans->rollback();

		                }
		              else
		                {
                                  $dbTrans->rollback();

		                 }

                                }
		          
		           }
			     else
				  { //Yii::$app->getSession()->setFlash(Yii::t('app','Error'), $age_message.Yii::t('app',' yr old'));
				       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($age_message.Yii::t('app',' yr old') ),
										    'title' => Html::encode(Yii::t('app','warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
				   }


             }


        }


        if(isset($_GET["from"])&&($_GET["from"]!=''))
		  {
			 if($_GET["from"]=='teach')
				{
					return $this->render('update_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				}
			 elseif($_GET["from"]=='emp')
				{
					return $this->render('update_employee', [
		                'model' => $model,
		                'modelEmployeeInfo'=>$modelEmployeeInfo,
		                'modelPersonTitles'=>$modelPersonTitles,
		                'modelProgram'=>$modelProgram,
		                'modelDepartmentPerson'=>$modelDepartmentPerson,
		                'modelCustomFieldData'=>$modelCustomFieldData,
		            ]);
				 }
		   }


 
     }
  else
     {
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

        }

    }


   public function actionStudent(){
ini_set("memory_limit", "-1");
set_time_limit(0);

    if(Yii::$app->user->can('fi-student-list'))
     {
        //return program_id or null value
          $program = isProgramsLeaders(Yii::$app->user->identity->id);
        if($program==null)
          {
          	 
          	 if( (Yii::$app->session['profil_as'] ==1)||(isset($_GET['from']) &&($_GET['from']=='teach') )  )
              {  
              	$userModel = User::findOne(Yii::$app->user->identity->id);
    
				    $allStudents = SrcPersons::find()->alias('p')->select(['p.id','last_name','first_name','gender','birthday','phone','email','id_number','image','comment'])->joinWith(['studentOtherInfo0','courses0'])->where(['is_student'=>1,'active'=>[1,2],'teacher'=>$userModel->person_id ])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();
              }
             elseif( (Yii::$app->session['profil_as'] ==0)&&(!isset($_GET['from']) ) ) 
          	    $allStudents = SrcPersons::find()->alias('p')->select(['p.id','last_name','first_name','gender','birthday','phone','email','id_number','image','comment'])->joinWith(['studentOtherInfo0'])->where(['is_student'=>1,'active'=>[1,2] ])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();
                   
           
           
           }
         else //se yon responsab pwogram
           {
           	   
		$allStudents = SrcPersons::find()->alias('p')->select(['p.id','last_name','first_name','gender','birthday','phone','email','id_number','image','comment'])->joinWith(['studentOtherInfo0'])->where(['is_student'=>1,'apply_for_program'=>$program,'active'=>[1,2] ])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();

            } 

      
        return $this->render('student', [
           'allStudents'=>$allStudents,
        ]);
    }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                            'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                            'duration' =>120000,
                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
                            'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                            'title' => Html::encode(Yii::t('app','Unthorized access') ),
                            'positonY' => 'top',   //   top,//   bottom,//
                            'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }

    }


   public function actionTeacher(){

      if(Yii::$app->user->can('fi-teacher-list'))
     {
       $acad = Yii::$app->session['currentId_academic_year'];
       
        $searchModel = new SrcPersons();
        $dataProvider = $searchModel->searchGlobalTeacher(Yii::$app->request->queryParams);

      //  if(isset($_GET['SrcPersons']))
      //   {
       //         $allTeachers= $searchModel->searchGlobalTeacher(Yii::$app->request->queryParams);
       //          $allTeachers=$allTeachers->models;
       //  }
       // else
         $allTeachers=SrcPersons::find()
         ->innerJoinWith(['courses'])
         ->where(['is_student'=>0,'active'=>[1,2],'academic_year'=>$acad ])
         ->orderBy(['last_name'=>SORT_ASC])
         ->all();


        $dataProvider->sort = ['defaultOrder' => ['last_name' => 'ASC']];

        return $this->render('teacher', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'allTeachers'=>$allTeachers,
        ]);

 }else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }

    }

   public function actionEmployee(){

     if(Yii::$app->user->can('fi-employee-list'))
     {
       $acad = Yii::$app->session['currentId_academic_year'];
        $searchModel = new SrcPersons();
       // $dataProvider = $searchModel->searchGlobalEmployee(Yii::$app->request->queryParams);

       // if(isset($_GET['SrcPersons']))
       //  {
      //          $allEmployees= $searchModel->searchGlobalEmployee(Yii::$app->request->queryParams);
       //          $allEmployees=$allEmployees->models;
      //   }
     //  else
      //   {
/*/*pou rapo         
                    $query_teacher = SrcPersons::find()->select('persons.id')
                                                    ->innerJoinWith(['courses'])
                                                    ->where('is_student=0 AND active in(1,2) AND academic_year='.$acad )
                                                    ->orderBy(['last_name'=>SORT_ASC])
                                                    ->all();
                 $allEmployees=SrcPersons::find()->where('is_student=0 AND active in(1,2)')->andWhere(['not in','id',$query_teacher])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();
 */        	
         	//$allEmployees=SrcPersons::find()->joinWith(['personsHasTitles'])->where('is_student=0 AND active in(1,2) AND academic_year='.$acad)->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();

        //  }


       $allEmployees=SrcPersons::find()->where(' is_student=0 and active in(1,2)')->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->all();
         





        //$dataProvider->sort = ['defaultOrder' => ['last_name' => 'ASC']];

        return $this->render('employee', [
            'searchModel' => $searchModel,
          //  'dataProvider' => $dataProvider,
            'allEmployees'=>$allEmployees,
        ]);
 }else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }


    }
    
 
    public function actionGenderlist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_gender = [
            ['value'=>0,'text'=>Yii::t('app', 'Male')], 
            ['value'=>1,'text'=>Yii::t('app', 'Female')],
            ];
       
         $json_gender =json_encode($all_gender,false); 
         return $json_gender; 
       
       
    }
    
  public function actionGslist(){
       $acad = Yii::$app->session['currentId_academic_year'];
                          
       $all_gs = [
             [ 'value'=>'O+','text'=>Yii::t('app', 'O+')],
             ['value'=>'O-','text'=>Yii::t('app', 'O-')],
             ['value'=>'A+','text'=>Yii::t('app', 'A+')],
             ['value'=>'A-','text'=>Yii::t('app', 'A-')],
             ['value'=>'B+','text'=>Yii::t('app', 'B+')],
             ['value'=>'B-','text'=>Yii::t('app', 'B-')],
             ['value'=>'AB+','text'=>Yii::t('app', 'AB+')],
             ['value'=>'AB-','text'=>Yii::t('app', 'AB-')], 
                 
                 ];

       
               $json_gs =json_encode($all_gs,false); 
         return $json_gs; 
       
       
    }
    
public function actionSetInfoemployee(){
       $acad = Yii::$app->session['currentId_academic_year'];
       
       $modelEmployee = SrcPersons::findOne($_POST['pk']);
        
       
 if(isset($_POST)){
           //if(isset($_GET['is'])){
                
               //$model = SrcContactInfo::findOne($_POST['pk']); 
               $model = SrcPersons::findOne($_POST['pk']);
                
               if($_GET['set']=='email')
                   $model->email = $_POST['value'];
               elseif($_GET['set']=='phone')
                       $model->phone = $_POST['value'];
               elseif($_GET['set']=='adresse')
                       $model->adresse = $_POST['value'];
               elseif($_GET['set']=='sexe')
                       $model->gender = $_POST['value'];
               elseif($_GET['set']=='gs')
                       $model->blood_group = $_POST['value'];
               elseif($_GET['set']=='nif_cin')
                       $model->nif_cin = $_POST['value'];
               
               
                
                
                $model->update_by = Yii::$app->user->identity->username;
                $model->date_updated = date('Y-m-d H:i:s'); 
                $model->save(); 
           //}
       }
       
       
    } 
 
    
public function actionSaveContact($id_student,$nom_contact,$relation_name, $profession,$phone_contact,$email, $contact_address){
    if(Yii::$app->user->can('fi-student-detail'))
     {
        $contactStudent = SrcContactInfo::findAll(['person'=>$id_student]);
        $contact = new ContactInfo(); 
        $contact->person = $id_student;
        $contact->contact_name = $nom_contact; 
        $contact->contact_relationship = $relation_name;
        $contact->profession = $profession;
        $contact->phone = $phone_contact;
        $contact->email = $email;
        $contact->address = $contact_address; 
        if($contact->save()){
            return $this->renderAjax('contact-info',['id_student'=>$id_student,'contactStudent'=>$contactStudent]);
        }else{
            return 'Failled';
        } 
        
     }
     else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', 
                [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
		]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }
    
}

public function actionShowContact($id_student){
    $contactStudent = SrcContactInfo::findAll(['person'=>$id_student]);
    return $this->renderAjax('contact-info',['id_student'=>$id_student,'contactStudent'=>$contactStudent]);
}

public function  actionListRelation(){
         $all_relations = \app\modules\fi\models\Relations::findBySql("SELECT id AS 'value', relation_name AS 'text'  FROM relations ORDER BY id")->asArray()->all();
         $json_relations =json_encode($all_relations,false); 
         return $json_relations; 
    }


 public function actionMoredetailsstudent(){


        if(Yii::$app->user->can('fi-student-detail'))
     {
         $is_stud = $_GET['is_stud'];
        $modelContactInfo = new SrcContactInfo();//::findOne($is_stud);
        if($is_stud==1)
         {
	        $stud_id = $_GET['id'];

                /** Debut de la
                 *
                 */
                    if(Yii::$app->request->post('hasEditable')){
                        if(Yii::$app->request->post('contact_id')){
                        $contactId = Yii::$app->request->post('contact_id');
                        $contact = ContactInfo::findOne($contactId);
                        $out = Json::encode(['output'=>'','message'=>'']);
                        //$post = [];
                       // $posted = current($_POST['ContactInfo']);
                       // if($contact->load($posted)){
                            $contact->person = $stud_id;
                            $contact->contact_name = $_POST['ContactInfo']['contact_name'];
                            $contact->contact_relationship = $_POST['ContactInfo']['contact_relationship'];
                            $contact->profession = $_POST['ContactInfo']['profession'];
                            $contact->phone = $_POST['ContactInfo']['phone'];
                            $contact->address = $_POST['ContactInfo']['address'];
                            $contact->email = $_POST['ContactInfo']['email'];
                            $contact->save();
                       // }
                        }else{
                            $contact = new ContactInfo();
                            $out = Json::encode(['output'=>'','message'=>'']);
                            $contact->person = $stud_id;
                            $contact->contact_name = $_POST['ContactInfo']['contact_name'];
                            $contact->contact_relationship = $_POST['ContactInfo']['contact_relationship'];
                            $contact->profession = $_POST['ContactInfo']['profession'];
                            $contact->phone = $_POST['ContactInfo']['phone'];
                            $contact->address = $_POST['ContactInfo']['address'];
                            $contact->email = $_POST['ContactInfo']['email'];
                            $contact->save();
                        }
                        echo $out;
                        return;
                    }

                /** Fin de la chose
                 *
                 */
                
                     $all_students = loadAllStudentsJson();
                    $json_all_students = json_encode($all_students,false);
        
	        $modelStudent=SrcPersons::findOne($stud_id );
                $modelContactStudent = SrcContactInfo::findAll(['person'=>$stud_id]);
	        return $this->render('moredetailsstudent',[
                    'modelStudent'=>$modelStudent,
                    'modelContactInfo'=>$modelContactInfo,
                    'modelContactStudent'=>$modelContactStudent,
                    'json_all_students'=>$json_all_students,
                    ]);

         }

 }else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }

 }

 public function actionMoredetailsemployee(){

    if(Yii::$app->user->can('fi-employee-detail'))
     {

        $is_stud = $_GET['is_stud'];

        if($is_stud==0)
         {
         	$employee_id = $_GET['id'];
	        $modelEmployee=SrcPersons::findOne($employee_id );
	        return $this->render('moredetailsemployee',['modelEmployee'=>$modelEmployee]);

         	}

  }else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }
    }



    /**
     * Deletes an existing Persons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
 public function actionDelete($id)
   {
    if(Yii::$app->user->can('fi-persons-delete'))
     {

       try {
	       	 $this->findModel($id)->delete();

	           if($_GET['is_stud']==1)//student
	              return $this->redirect(['/reports/report/dashboardpedago']);
	           elseif( ($_GET['is_stud']==0)&&($_GET['from']=='teach') )//teacher
	               return $this->redirect(['persons/teacher']);
	              elseif( ($_GET['is_stud']==0)&&($_GET['from']=='emp') )//employee
	                 return $this->redirect(['persons/employee']);

        } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}


 }else{
        if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }
    }

    /**
     * Finds the Persons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Persons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcPersons::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new ContactInfo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ContactInfo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } elseif(Yii::$app->request->isAjax){

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }



    public function actionCreatecontact(){
            $model = ContactInfo::findOne(1); // your model can be loaded here

            /*
            if(isset($_GET['id'])){
                $idstud = $_GET['id'];
            }
             *
             */
            // Check if there is an Editable ajax request
            if (isset($_POST['hasEditable'])) {
                // use Yii's response format to encode output as JSON
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                // read your posted model attributes
                if ($model->load($_POST)) {
                    //$model->person = $idstud;
                    $contact_name = $_POST['ContactInfo']['contact_name'];
                    $contact_relationship = $_POST['ContactInfo']['contact_relationship'];
                    $profession = $_POST['ContactInfo']['profession'];
                    $phone = $_POST['ContactInfo']['phone'];
                    $address = $_POST['ContactInfo']['address'];
                    $email = $_POST['ContactInfo']['email'];

                    $model->setAttribute('person', 18);
                    $model->setAttribute('contact_name', $contact_name);
                    $model->setAttribute('contact_relationship', $contact_relationship);
                    $model->setAttribute('profession',$profession);
                    $model->setAttribute('phone', $phone);
                    $model->setAttribute('adsress', $address);
                    $model->setAttribute('email', $email);
                    $model->save();
                    // read or convert your posted information

                  // $value = $model->contact_name;

                    // return JSON encoded output in the below format
                  // return ['output'=>$value, 'message'=>''];

                    // alternatively you can return a validation error
                    // return ['output'=>'', 'message'=>'Validation error'];
                }
                // else if nothing to do always return an empty JSON encoded output
                else {
                    return ['output'=>'', 'message'=>''];
                }

            }

            // Else return to rendering a normal view
          // return $this->render('moredetailsstudent', ['model'=>$model]);
    }


    /*
     * Retourne la liste des etudiants qui suivent un cours
     */
    public function actionViewcourse(){
        //if(Yii::$app->user->can("fi-employee-viewcourse")){
            $searchModel = new SrcPersons();
            //$model = new Courses();
           // if(isset($_GET['id'])){
            $student = $_GET['id'];
            $dataProvider = $searchModel->coursesByStudent($student, Yii::$app->request->queryParams);
            if(isset($_GET['SrcPersons']))
                $dataProvider = $searchModel->coursesByStudent($student, Yii::$app->request->queryParams);
           // }
             if (isset($_GET['pageSize']))
         	 {
		   Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
		   unset($_GET['pageSize']);
                }
            return $this->renderAjax('viewcourse',
                    [
                     'searchModel'=>$searchModel,
                     'dataProvider'=>$dataProvider,
                    // 'model'=>$model,

                    ]
                    );
      /*  }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

        }
       */
    }


    public function actionGetStudentInfo($id){
        if(Yii::$app->user->can('fi-student-detail'))
     {
          $username ='';
        
        if(isset(Yii::$app->user->identity->username))
           $username = Yii::$app->user->identity->username;

            
            $acad = Yii::$app->session['currentId_academic_year'];
                $student = SrcPersons::findOne($id);
                // take some variable to build the student profil
                $condition_fee_status = ['in','fees_label.status', [0,1] ];
                $modelBilling = new SrcBillings();
                $studCourse = new SrcStudentHasCourses;
                //billing: si elev la gen balanse li pa ajou
                $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$student->id.' AND balance >0')->all();
                
                if($student->image!='')
                   $src_img= Url::to("@web/documents/photo_upload/$student->image");
                else
                  $src_img= Url::to("@web/img/no_pic.png");
                  
                if(isset($student->studentOtherInfo0[0]->applyForProgram->label)){
                $program= $student->studentOtherInfo0[0]->applyForProgram->label;
                }else{
                    $program = ""; 
                }
                //get info module
                 $total_module1= $studCourse->getTotalCourseForStudent($student->id); //$total_module;
                 $total_module_success1= $studCourse->getTotalCoursePassForStudent($student->id); //$total_module_success;
                 $percent_success1= 0;
                if( ($total_module1!='')&&($total_module1!=0) )
                  $percent_success1= ($total_module_success1 * 100 ) /  $total_module1;

                if( ($total_module1!=0)&&($total_module1!='') )
                 $info_module1 = $total_module_success1.'/'.$total_module1;
                else
                    $info_module1 = NULL;
                $module = Yii::t('app', 'Modules');
                $age_label = Yii::t('app', 'Age');
                if(ageCalculator($student->birthday)!=null)
                      $age = '('.ageCalculator($student->birthday).Yii::t('app',' yr old').')';
               else
                      $age= NULL;
               $level_label = Yii::t('app','Level');
               $level = "";
               $email_label = Yii::t('app', 'Email');
               $phone_label = Yii::t('app', 'Phone');
                $id_number_label = Yii::t('app', 'Id Number');
                $idNumber = $student->id_number;
               $schoolarship_ = $student->getIsScholarshipHolder($student->id,$acad);
               if(isset($student->studentLevel->level))
                              {
                                 $level = $student->studentLevel->level;
                              }

                if($schoolarship_==1)
                   $schoolarship = Yii::t('app','Yes');
                elseif($schoolarship_==0)
                 $schoolarship = Yii::t('app','No');
                $schoolarship_label = Yii::t('app', 'Schoolarship holder');
                $billing_label = Yii::t('app', 'Billing');
                //return a integer (id) or NULL
     /*      
           $last_bil_id = $modelBilling->getLastTransactionIDForStudentInfo($student->id, $condition_fee_status, $acad);
              if($last_bil_id==null)
                   $last_bil_id = 0;
                if($modelBalance_stud!=null)
                   { foreach($modelBalance_stud as $modelBalance)
                     {  if($modelBalance->balance > 0)
                            {
                                    $bal_id = $modelBalance->id;

                                    if($last_bil_id==0)
                                       $billing1 =  '<span class="fa fa-2x fa-thumbs-o-down" style="color:red"></span>';
                                 else
                                    $billing1 = '<span class="fa fa-2x fa-thumbs-o-down" style="color:red"></span>';
                            }
                      }

                   }
              else  
                { if($last_bil_id==0)
                        $billing1 ='-';
                   else
                     $billing1 = '<span class="fa fa-2x fa-thumbs-o-up" style="color:green"></span>';

                }
        */
                $note_label = Yii::t('app', 'Notes').' : ';
               // Construit le volet de droite du profil etudiant
                echo "<div class='col-lg-4'>"
                . "<div class='ibox'>"
                . "<div class='ibox-content'>"
                . "<div class='tab-content'>"
                . "<div id='contact'>"
                . "<div class='row m-b-lg' style='margin-bottom:10px;'>"
                . "<div class='row'>"
                . "<div class='col-lg-12 text-center'>"
                . "<h4>$student->fullName</h4>"
                . "</div>"
                . "<div class='row' style='margin: 0 auto;'>
                <div class='col-sm-2'></div>
                  <div class='col-sm-8'>
                         <div class='m-b-sm'>
                            <img alt='image' class='img-circle img-responsive' src='$src_img'/>
                        </div>
                   </div>
                <div class='col-sm-2'></div>
                </div>"
                . "<div class='row' style='margin: 0 auto;'>
                  <div class='col-lg-12 text-center'>
                     <div class='col-lg-12 text-center'>

                                        <strong>
                                        $program
                                        </strong>  <br/>
                    <div class='ibox-content' style='height:75px;'>
                    <div>
                        <div>
                            <span>$module</span>
                            <small class='pull-right'>$info_module1</small>
                        </div>

                        <div class='progress progress-small'>
                                <div style='width: $percent_success1%;' class='progress-bar progress-bar-striped'></div>
                        </div>
                     </div>
                    </div>

                       </div>
                    </div>

                </div>";
      
      
        if(Yii::$app->session['profil_as'] ==0)
          {
             if($username!='logipam')
              {

            echo "<div class='row' style='margin: 0 auto;' >
                 <div class='col-lg-12'> <div class='col-sm-2'></div>
                   ".Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'More details'), ['moredetailsstudent', 'id' =>$student->id,'is_stud'=>1], ['class' => 'btn btn-primary btn-sm'])." <div class='col-sm-2'></div>
               </div>

                </div>";
              }
     
          }
  

          
           echo "</div>"
                . "<div class='client-detail'>
                  <div class='full-height-scroll'>

                      <ul class='list-group clear-list'>
                           
                           <li class='list-group-item'>
                           <strong>     $id_number_label </strong>
                                <span class='pull-right'>$idNumber</span>
                               
                            </li>
                            
                            <li class='list-group-item'>
                              <strong>  $level_label </strong>
                                <span class='pull-right'> $level</span>
                                
                            </li>
                            <li class='list-group-item'>
                              <strong>  $email_label </strong>
                                <span class='pull-right'> $student->email</span>
                                
                            </li>";
                           
       
        if(Yii::$app->session['profil_as'] ==0)
          {
 
                
                        echo  "  <li class='list-group-item'>
                              <strong>  $phone_label </strong>
                                <span class='pull-right'>$student->phone</span>
                               
                            </li>

                            <li class='list-group-item'>
                               <strong>  $schoolarship_label </strong> 
                                <span class='pull-right'> $schoolarship </span>
                                
                            </li>
                         ";                           
           /*        <li class='list-group-item'>
                                <strong>  $billing_label </strong>
                                <span class='pull-right'>".Html::a(billing1, Yii::getAlias('@web').'/index.php/billings/billings/view?id='.student->id.'&wh=inc_bil&ri=0', [ 'title' =>'',
        ])."</span>
                                
                            </li>
              */        
                    echo "
                            <li class='list-group-item'>
                            <strong>$note_label</strong>

                            <p>$student->comment</p>
                            </li>";
              
          }
   


            echo "        </ul>
                      </div>
        </div>"
                . "</div>"
                . "</div>"
                . "</div>"
                . "</div>"
                . "</div>"
                . "</div>";


        }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                            'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                            'duration' =>120000,
                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
                            'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                            'title' => Html::encode(Yii::t('app','Unthorized access') ),
                            'positonY' => 'top',   //   top,//   bottom,//
                            'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }

    }
    
    
    public function actionGetStudentSearch($limit_stud){
        $allStudents = SrcPersons::find()->select(['id','last_name','first_name','gender','birthday','phone','email','id_number','image','comment'])->where(['is_student'=>1,'active'=>[1,2] ])->orderBy(['last_name'=>SORT_ASC,'first_name'=>SORT_ASC])->limit($limit_stud)->all();
        echo $this->renderAjax('student_ajax',['limit_stud'=>$limit_stud,'allStudents'=>$allStudents]); 
    }
    
    public function actionStudentNew(){
        if(Yii::$app->user->can('fi-student-list'))
     {
        $searchModel = new SrcPersons();
        //$dataProvider = $searchModel->searchGlobalStudent(Yii::$app->request->queryParams);
        $stud = SrcPersons::findBySql("SELECT id FROM persons WHERE is_student = 1 AND active IN (1,2) ORDER BY last_name ASC LIMIT 1")->all();  
        foreach($stud as $s){
            $stud_id = $s->id; 
        }
        
        $count =  Yii::$app->db->createCommand('
                    SELECT COUNT(*) FROM persons WHERE is_student=:status AND active IN (1,2)
                    ', [':status' => 1])->queryScalar();
         $sql = "SELECT DISTINCT p.id, p.last_name, p.first_name, if(p.gender=0,'Masculin','Féminin') as gender, p.birthday, p.phone, p.email, p.id_number, p.image, p.comment, r.room_name, s.shift_name, IF(sl.level = 1, 'Niveau-1','Niveau-2') as level, pr.label FROM persons p
 LEFT JOIN student_has_courses shc ON (p.id = shc.student)
 LEFT JOIN courses c ON (c.id = shc.course ) 
 RIGHT JOIN rooms r ON (r.id = c.room)
 LEFT JOIN shifts s ON (s.id = c.shift) 
 LEFT JOIN student_level sl ON (sl.student_id = p.id) 
 LEFT JOIN student_other_info soi ON (soi.student = p.id)
 LEFT JOIN program pr ON (pr.id = soi.apply_for_program)
 WHERE is_student = 1 AND active IN (1,2) ORDER BY last_name ASC";
         
         $dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    'pagination' => [ 'pageSize' => 100 ],
                    ]);
         
       return  $this->render("studentAjax",
               ['dataProvider'=>$dataProvider,
                   'searchModel'=>$searchModel,
                   'stud_id'=>$stud_id,
                   ]
               ); 
       }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {
              	  return $this->redirect(['/rbac/user/login']);
                }
             else
               {
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                            'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                            'duration' =>120000,
                            'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
                            'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                            'title' => Html::encode(Yii::t('app','Unthorized access') ),
                            'positonY' => 'top',   //   top,//   bottom,//
                            'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

    }
    }
    
    public function actionStudentAjaxSearch($search){
        $searchModel = new SrcPersons();
        
        
        $string_search =  "p.last_name LIKE '%$search%' OR p.first_name LIKE '%$search%' OR  p.id_number LIKE '%$search%' OR r.room_name LIKE '%$search%' OR s.shift_name LIKE '%$search%' OR pr.label LIKE '%$search%'";
        $sql_search = "SELECT count(DISTINCT(p.id)) FROM persons p
 LEFT JOIN student_has_courses shc ON (p.id = shc.student)
 LEFT JOIN courses c ON (c.id = shc.course ) 
 RIGHT JOIN rooms r ON (r.id = c.room)
 LEFT JOIN shifts s ON (s.id = c.shift) 
 INNER JOIN student_level sl ON (sl.student_id = p.id) 
 LEFT JOIN student_other_info soi ON (soi.student = p.id)
 LEFT JOIN program pr ON (pr.id = soi.apply_for_program)
 WHERE is_student = 1 AND active IN (1,2) AND ($string_search) ORDER BY last_name ASC";
       $count =  Yii::$app->db->createCommand($sql_search)->queryScalar(); 
       
       $sql = "SELECT DISTINCT p.id, p.last_name, p.first_name, if(p.gender=0,'Masculin','Féminin') as gender, p.birthday, p.phone, p.email, p.id_number, p.image, p.comment, r.room_name, s.shift_name, IF(sl.level = 1, 'Niveau-1','Niveau-2') as level, pr.label FROM persons p
 LEFT JOIN student_has_courses shc ON (p.id = shc.student)
 LEFT JOIN courses c ON (c.id = shc.course ) 
 RIGHT JOIN rooms r ON (r.id = c.room)
 LEFT JOIN shifts s ON (s.id = c.shift) 
 LEFT JOIN student_level sl ON (sl.student_id = p.id) 
 LEFT JOIN student_other_info soi ON (soi.student = p.id)
 LEFT JOIN program pr ON (pr.id = soi.apply_for_program)
 WHERE is_student = 1 AND active IN (1,2) AND ($string_search) ORDER BY last_name ASC";
       
       $dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => $sql,
                    'totalCount' => $count,
                    'pagination' => [ 'pageSize' => 100 ],
                    ]);
       $stud = SrcPersons::findBySql($sql)->all();  
        foreach($stud as $s){
            $stud_id = $s->id; 
        }
        
       return  $this->renderAjax("studentAjaxSearch",
             
               ['dataProvider'=>$dataProvider,
                   'searchModel'=>$searchModel,
                   'stud_id'=>$stud_id,
                   ]
                
               ); 
         
    }


}
