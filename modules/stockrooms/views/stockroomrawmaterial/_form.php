<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\stockrooms\models\SrcStockroomProductsLabel;

use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomRawMaterial */
/* @var $form yii\widgets\ActiveForm */


$automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
$array_=['maxlength' => true,];
$placeholder_code = '';

if($automatic_code_ ==1)
 { 
  $placeholder_code = Yii::t('app','Raw Material Code').' '.Yii::t('app',' automatic');
  $array_=['maxlength' => true, 'disabled'=>'', 'placeholder' => $placeholder_code ];
 }



?>

<div class="stockroom-raw-material-form">

    <?php $form = ActiveForm::begin(); ?>
       <div class="row">
        <div class="col-lg-3">

    			<?php
                    if(isset($_GET['id_prl'])&&($_GET['id_prl']!=''))
                       {
                       	    $product_id = preg_replace("/[^0-9]/","",$_GET['id_prl']);
                       	    
                       	    $model->product_label = [$product_id];
                       	 }
             ?>

    			<?= $form->field($model, 'product_label')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcStockroomProductsLabel::find()->where('category=0')->all(),'id','product_description' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select product  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>

        </div>
        <div class="col-lg-1" style="margin-left:-15px; margin-right:15px; margin-top:12px;">
         
          <?php 
            // echo '<a  onclick="addSubject()" class="btn btn-primary btn-sm" title= "'.Yii::t('app','Add subject').'" > <i class="fa fa-plus"></i></a>';
             ?>
          <?php echo Html::button('<i class="fa fa-plus"></i>', ['value' => Url::to(['../stockrooms/stockroomproductslabel/create','cat'=>0, 'wh'=>'rm','from1'=>'raw','from'=>'rm']), 'title' => 'Add product', 'class' => 'showModalButton btn btn-primary btn-sm']); ?>    
         
        </div>


        <div class="col-lg-4">
               <?= $form->field($model, 'code')->textInput($array_) ?>
        </div>
        <div class="col-lg-4">
               <?= $form->field($model, 'price')->textInput() ?>
         </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
              <?= $form->field($model, 'longueur')->textInput() ?>
         </div>
         <div class="col-lg-4">		
    			<?= $form->field($model, 'surface')->textInput() ?>
        </div>
        <div class="col-lg-4">
                <?= $form->field($model, 'volume')->textInput() ?>
        </div>
    </div>
   
   <div class="row">
        <div class="col-lg-4">
                <?= $form->field($model, 'nature')->textInput() ?>
        </div>
        <div class="col-lg-8">
              <?= $form->field($model, 'note')->textarea(['rows' => 1]) ?>
         </div>
    </div>    

    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 


    <?php ActiveForm::end(); ?>

</div>


<?php
         
    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Create Product Label').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md',
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            ]
            ); 
    echo '<div id="modalContent"><div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';
    
    Modal::end(); 
    
    ?>


