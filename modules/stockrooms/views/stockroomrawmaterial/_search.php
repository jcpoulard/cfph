<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\SrcStockroomRawMaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-raw-material-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'product_label') ?>

    <?= $form->field($model, 'weight') ?>

    <?= $form->field($model, 'longueur') ?>

    <?php // echo $form->field($model, 'square') ?>

    <?php // echo $form->field($model, 'volume') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'price') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
