<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\stockrooms\models\SrcLocal;

use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\Stockroom */
/* @var $form yii\widgets\ActiveForm */


$automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
$array_=['maxlength' => true,];
$placeholder_code = '';

if($automatic_code_ ==1)
 { $placeholder_code = Yii::t('app','Stockroom Code').' '.Yii::t('app',' automatic');
     $array_=['maxlength' => true, 'disabled'=>'', 'placeholder' => $placeholder_code ];
  
 }

?>

<div class="stockroom-form">

    <?php $form = ActiveForm::begin(); ?>

      <div class="row">
        <div class="col-lg-4">

    			<?= $form->field($model, 'stockroom_name')->textInput(['maxlength' => true]) ?>
           </div>
           
        <div class="col-lg-4">
    			 <?php
                    if(isset($_GET['id_loc'])&&($_GET['id_loc']!=''))
                       {
                       	    $local_id = preg_replace("/[^0-9]/","",$_GET['id_loc']);
                       	    
                       	    $model->stockroom_local = [$local_id];
                       	 }
             ?>

    			<?= $form->field($model, 'stockroom_local')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcLocal::find()->all(),'id','local_label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select local  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
            </div>
            <div class="col-lg-1" style="margin-left:-28px; margin-top:12px;">
         
          <?php 
            // echo '<a  onclick="addSubject()" class="btn btn-primary btn-sm" title= "'.Yii::t('app','Add subject').'" > <i class="fa fa-plus"></i></a>';
             ?>
          <?= Html::button('<i class="fa fa-plus"></i>', ['value' => Url::to(['../stockrooms/local/create','wh'=>'mod','from'=>'mod']), 'title' => 'Add local', 'class' => 'showModalButton btn btn-primary btn-sm']); ?>
         
        </div>
     </div>   
      <div class="row">  
         <div class="col-lg-4">
            <?= $form->field($model, 'stockroom_code')->textInput($array_) ?>
          </div>
        
     </div>
     
     
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 


    <?php ActiveForm::end(); ?>

</div>


<?php
    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Create New Local').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md',
                //keeps from closing modal with esc key or by clicking out of the modal.
			    // user must click cancel or X to close
			    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
            ]
            ); 
    echo '<div id="modalContent"><div style="text-align:center"><img  style="width: 370px;" class="img-circle" src="'. Url::to("@web/img/logipam_bg.jpeg").' "></div></div>';
    
    Modal::end(); 
    
    ?>


