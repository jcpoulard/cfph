<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomProductsLabel;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomRestock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-restock-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
	
	<div class="col-lg-4">
	
	   <?php
	          $product_id = 0;
	          
	        if( (isset($_GET['from']))&&($_GET['from']=='prodin') )
	          {
	          	    if(isset($_GET['product_id'])&&($_GET['product_id']!=''))
                          $product_id = preg_replace("/[^0-9]/","",$_GET['product_id']);
                          
                     $model->stockroom_product_id = $product_id;
                     
                     $option =['placeholder'=>Yii::t('app', ' --  select product  --'),
                                    'onchange'=>'submit()', 'disabled'=>'',
                         ];
	          	}
	         else
	           $option =['placeholder'=>Yii::t('app', ' --  select product  --'),
                                    'onchange'=>'submit()',
                         ];
	          	
	        
	   ?>
	    
	    <?= $form->field($model, 'stockroom_product_id')->widget(Select2::classname(), [
                       'data'=>loadProductsForRestock($_GET['strid']),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>$option,
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
    </div>
 
 

   <div class="col-lg-4">
	    <?= $form->field($model, 'quantity_restock')->textInput() ?>
	</div>

	<div class="col-lg-4">
	      <?php
                if($model->restock_date=='')
                  $model->restock_date = date('Y-m-d');
                  
                echo $form->field($model, 'restock_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>

	 </div>
</div>	
<div class="row"> 
    <div class="col-lg-12">	
	    <?= $form->field($model, 'comment')->textarea(['rows' => 1]) ?>
	</div>
</div>

     

<div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 



    <?php ActiveForm::end(); ?>

</div>
