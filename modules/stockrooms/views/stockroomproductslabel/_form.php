<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;



/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomProductsLabel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-products-label-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-4">

    			<?= $form->field($model, 'product_description')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-lg-4">

    			<?php
        
                //if(!isset($_GET['id']) ) 
	                   echo $form->field($model, 'category')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'Raw Material'), '1'=>Yii::t('app', 'Equipment'), '2'=>Yii::t('app', 'Furniture') ],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                  // 'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);

                  ?>
        </div>
        
     </div>
     
     
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 


    <?php ActiveForm::end(); ?>

</div>
