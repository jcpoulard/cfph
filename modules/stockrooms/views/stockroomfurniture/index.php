<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\stockrooms\models\SrcStockroomFurniture */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Stockroom Furnitures');

?>


  <?= $this->render('//layouts/stockroomSettingLayout'); ?>
  
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'set_fur'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content stockroom-furniture-index">
               <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Code'); ?></th>
				            <th><?= Yii::t('app','Product Label'); ?></th>
				            <th><?= Yii::t('app','Marque'); ?></th>
				            <th><?= Yii::t('app','Model'); ?></th>
				            <th><?= Yii::t('app','Serial'); ?></th>
				            <th><?= Yii::t('app','Price'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 
            


    $dataFurniture= $dataProvider->getModels();
    
              
          foreach($dataFurniture as $furniture)
           {
           	   echo '  <tr >
                                                    <td ><span data-toggle="tooltip" title="'.$furniture->description.'">'.$furniture->code.'</span> </td>
                                                    <td ><span data-toggle="tooltip" title="'.$furniture->description.'">'.$furniture->productLabel->product_description.'</span> </td>
                                                    <td >';
                                                      if( (isset($model->marque))&&($model->marque!='') )
                                                         echo $furniture->marque0->marque_label;
                                                      else
                                                          echo '';
                                                    echo ' </td>
                                                    <td >'.$furniture->model.' </td>
                                                    <td >'.$furniture->serial.' </td>
                                                    <td >'.$furniture->price.' </td>
                                                    
                                                    
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                     /*    if(Yii::$app->user->can('stockrooms-stockroomequipment-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomfurniture/view?id='.$furniture->id.'&wh=set_eq', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              */
                                                              
                                                         if(Yii::$app->user->can('stockrooms-stockroomfurniture-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomfurniture/update?id='.$furniture->id.'&wh=set_fur', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                         if(Yii::$app->user->can('stockrooms-stockroomfurniture-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomfurniture/delete?id='.$furniture->id.'&wh=set_fur', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>




</div>




<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Stockroom Furnitures List'},
                    {extend: 'pdf', title: 'Stockroom Furnitures  List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>


