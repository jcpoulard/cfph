<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\SrcStockroomHasProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-has-products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'stockroom_id') ?>

    <?= $form->field($model, 'stockroom_product') ?>

    <?= $form->field($model, 'quantity') ?>

    <?= $form->field($model, 'quantity_alert') ?>

    <?= $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'is_equipment') ?>

    <?php // echo $form->field($model, 'things_out') ?>

    <?php // echo $form->field($model, 'localisation') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
