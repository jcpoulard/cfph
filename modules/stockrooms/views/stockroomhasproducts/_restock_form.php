<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomEquipment;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomHasProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-has-products-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row" style="margin-top:-20px">
	<div class="col-lg-12">
	   <?php echo Yii::t('app', 'Click here to make an INVENTORY first.'); ?> 
	</div>
</div>
	
<br/>
  
 <div class="row">
	 <div class="col-lg-4">  
	    <?= $form->field($model, 'quantity')->textInput() ?>
	</div>
	<div class="col-lg-8">
	    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
	</div>    
</div>

     <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton( Yii::t('app', 'Save'), ['name' =>'update', 'class' =>'btn btn-primary']) ?>

                            </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 

    <?php ActiveForm::end(); ?>

</div>
