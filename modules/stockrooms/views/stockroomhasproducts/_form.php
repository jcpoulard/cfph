<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomFurniture;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomHasProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-has-products-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">

<?php
     if(!isset($_GET['id']) ) 
        {
?>

	<div class="col-lg-4">
	            <?php
	                 /*
	                  if(!isset($_GET['id']) )
	                     echo $form->field($model, 'is_equipment')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()','value'=>1 ]); 
	                     */
	                  
	                
	                   echo $form->field($model, 'is_equipment')->label(Yii::t('app', 'Item Type'))->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'Raw Material'), '1'=>Yii::t('app', 'Equipment'), '2'=>Yii::t('app', 'Furniture') ],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);

	                  
	             ?> 
	  </div>

<?php
        }
?>	
	 <div class="col-lg-4">
	 
	       <?php
	                   $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $data =  $stockroomRawMAterialModel->loadRawMaterialLabel();
	                    
	                   
	              if($model->is_equipment==1)
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $data =  $stockroomEquipmentModel->loadEquipmentLabel();
	               }
	              elseif($model->is_equipment==2)
	               {   $stockroomFurnitureModel = new SrcStockroomFurniture;
	                    $data =  $stockroomFurnitureModel->loadFurnitureLabel();
	               }
	               	 
	       ?>
	    <?= $form->field($model, 'stockroom_product')->widget(Select2::classname(), [
                       'data'=>$data,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select product  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
	   </div>
	 
	 <div class="col-lg-4">
	     <?= $form->field($model, 'localisation')->textInput(['maxlength' => true]) ?>
 </div>
  </div>
  
 <div class="row">
	<?php 
	         if($model->is_equipment==0)
	               {   $lg=3;
	?>
	
	 <div class="col-lg-3">  
	    <?= $form->field($model, 'quantity')->textInput() ?>
	</div>
	<div class="col-lg-3">
	    <?= $form->field($model, 'quantity_alert')->textInput() ?>
	</div>
	
	<?php 
	               }
	             else
	                $lg=4;
	?>

<?php 
	
	if($model->is_equipment!=0)
	  {   $lg=3;
	  
	     if( (isset($_GET['id']) )&&($model->things_out==1)  )
			 {  
						     //msg
						     Yii::$app->getSession()->setFlash('warning', [
												    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','When product is out, state and status cannot be updated here.') ),
												    'title' => Html::encode(Yii::t('app','Warning') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
						           
						      	 	
		   }
		else
		  {
	               
	?>
	
	<div class="col-lg-<?= $lg ?>">
	    <?php echo $form->field($model, 'state')->widget(Select2::classname(), [
			                                   'data'=>loadStateByProductType($model->is_equipment),  
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]); ?>
	</div>  
	
	<?php 
	    if($model->state!=0)
	       {
	?>
	<div class="col-lg-<?= $lg ?>">
	    <?php echo $form->field($model, 'status')->widget(Select2::classname(), [
			                                   'data'=>loadStatusByProductTypeAndState($model->is_equipment,$model->state),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]); ?>
	</div>   
	<?php 
	        
	       }
	       
	    }
	    
	  }
	  
	?>
	 
</div>

     <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 

    <?php ActiveForm::end(); ?>

</div>
