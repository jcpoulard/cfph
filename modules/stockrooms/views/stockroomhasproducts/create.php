<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2;

use app\modules\stockrooms\models\SrcStockroomProductsLabel;



/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomHasProducts */

$modelStockroom = $model->getStockroomInfo($stockroomID);
$this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'stockroom has products');

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>
<div class="row">
      
 <div class="col-lg-3">
       <?php $form = ActiveForm::begin(); ?>
       <?php   
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
										if($stockroomID!='')
                    $model->stockroom_id = $stockroomID;
                    
                    $user_id=0;
                    if(isset(Yii::$app->user->identity->id))
                       $user_id=Yii::$app->user->identity->id;
                    
						echo $form->field($model, 'stockroom_id')->label(false)->widget(Select2::classname(), [

                       'data'=>loadStockrooms($user_id),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select stockroom  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ], 
                       ]);  
		   
								   ?>
          <?php ActiveForm::end(); ?>
           
    </div>
    
</div>
<?= $this->render('//layouts/stockroomSubMenuLayout'); ?>


<div class="wrapper wrapper-content stockroom-has-products-create">

       <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


