<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomHasProducts */

$this->title = $model->stockroom_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stockroom Has Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stockroom-has-products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'stockroom_id',
            'stockroom_product',
            'quantity',
            'quantity_alert',
            'state',
            'is_equipment',
            'things_out',
            'localisation',
        ],
    ]) ?>

</div>
