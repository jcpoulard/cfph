<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomRawMaterial;


/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomHasProducts */

$modelStockroom = $model->getStockroomInfo($stockroomID);
$equipment_rawmaterial_name ='';
$equipment_rawmaterial ='';

if($model->is_equipment==0)
  {   $stockroomRawMaterialModel = new SrcStockroomRawMaterial;
	                    $equipment_rawmaterial_name =  $stockroomRawMaterialModel->getRawMaterialLabel($model->stockroom_product);
	    $equipment_rawmaterial = 'raw material';
	}
elseif($model->is_equipment==1)
  {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $equipment_rawmaterial_name =  $stockroomEquipmentModel->getEquipmentLabel($model->stockroom_product);
	     $equipment_rawmaterial = 'equipment';
	}

if( isset($_GET['from'])&&($_GET['from']=='res') )	               
    $this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'restock ').$equipment_rawmaterial_name;
else
   $this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'update ').$equipment_rawmaterial;

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>

<?= $this->render('//layouts/stockroomSubMenuLayout'); ?>

 
<div class="wrapper wrapper-content stockroom-has-products-update">

   <?php
           $form ='_form';
           if( isset($_GET['from'])&&($_GET['from']=='res') )
              $form ='_restock_form';
   ?>
   
    <?= $this->render($form, [
        'model' => $model,
    ]) ?>

</div>




