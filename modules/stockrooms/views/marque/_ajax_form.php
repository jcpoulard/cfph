<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\modules\fi\models\Subjects */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
if($error)
   echo '<br/>';
?>

<div class="marque-form">


    <?php $form = ActiveForm::begin([ 
          'options' => [
                    'id' => 'create-marque'
                ],
        //'type' => ActiveForm::TYPE_INLINE, 
        //'method' => 'GET',
        //'action' => Url::to(['model/action']), 
        'enableClientScript' => true, // default
        'validateOnSubmit'=>true,
    ]); ?>
    <div class="row">
        <div class="col-lg-12">

           <?= $form->field($model, 'marque_label')->textInput(['maxlength' => true]) ?>
</div>
        
     </div>
     
     
    <div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create_ajax' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                      
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 

    <?php ActiveForm::end(); ?>
    

   


    

</div>
