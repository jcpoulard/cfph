<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomHasProducts;

use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomStockUsage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-stock-usage-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
	
	<div class="col-lg-4">
        
       <?php
	        if( (isset($_GET['id']))&&($_GET['id']!='') ) 
	           $data = loadEquipmentFurnitureOrRawMaterialLabel($model->stockroom_product_id); 
	        else 
	             $data = loadEquipmentFurnitureAndRawMaterialLabel($_GET['strid']);      
	               	 
	       ?> 
	       
	       <?= $form->field($model, 'stockroom_product_id')->widget(Select2::classname(), [
                       'data'=> $data,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select product  --'),
                                     'onchange'=>'submit()',
                       ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) 
                    ?>
    </div>

 <?php
      $display = 1;
      $noreturn =0;
      
      if($model->stockroom_product_id!='')
      {
          $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
	            	 
	            	 if($modelStockroomHasProduct->is_equipment!=0)
	            	   $display = 0;
	            	 else
	            	    $noreturn =1;
	            	   
      }
      
      if($display == 1)
      {
 ?>  
    <div class="col-lg-4">
       <?= $form->field($model, 'quantity')->textInput() ?>
     </div>
<?php

      }
?>
    
    <div class="col-lg-4">
       <?= $form->field($model, 'product_user')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcPersons::find()->where('active in(1,2)')->all(),'id','fullName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select product user  --')],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>

    </div>
  
</div>  


<div class="row">
    <div class="col-lg-4">
      <?php
                if($model->taken_date=='')
                  $model->taken_date = date('Y-m-d');
                  
                echo $form->field($model, 'taken_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>

   </div>


<?php
       if(isset($_GET['id']) )
	     {
	     	if($noreturn != 1)
               {
?>
	<div class="col-lg-4">
       <?php
	                /*  if(isset($_GET['id']) )
	                     echo $form->field($model, 'is_return')->checkbox(['style'=>'display:block; margin-left:20px; margin-bottom:-35px;', 'onchange'=> 'submit()','value'=>1 ]); 
	                   */  
	                   
	                  
	                     echo $form->field($model, 'is_return')->widget(Select2::classname(), [
			                                   'data'=>['0'=>Yii::t('app', 'No'),'2'=>Yii::t('app', 'No, Lost'), '1'=>Yii::t('app', 'Yes') ],
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', 'Yes/No'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]);
	                  
	             ?> 
   </div>
   
        <?php   
               }
	     }
        
        if($model->is_return==1 )
                   {
        
        ?>
    <div class="col-lg-4">
         <?php
                if($model->return_date=='')
                  $model->return_date = date('Y-m-d');
                  
                echo $form->field($model, 'return_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>

     </div>
     
     <div class="col-lg-12">
                            <?= $form->field($model, 'comment')->textarea(['rows' => 1]) ?>
                        </div>
     
          <?php
                   }
          ?>
</div>




<div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 


    <?php ActiveForm::end(); ?>

</div>
