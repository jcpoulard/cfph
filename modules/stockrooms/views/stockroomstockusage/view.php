<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomStockUsage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Stockroom Stock Usages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stockroom-stock-usage-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'outp','from'=>'upd','strid'=>$_GET['strid'] ], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'wh'=>'outp','strid'=>$_GET['strid'] ], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'StockroomProductLabel',
            'quantity',
            'productUser.FullName',
            'stockkeeper0.FullName',
            'IsReturn',
            'quantity_recieved',
            'recieved_by',
            'taken_date:date',
            'return_date:date',
            'comment',
        ],
    ]) ?>

</div>
