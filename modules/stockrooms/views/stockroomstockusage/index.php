<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\modules\stockrooms\models\SrcStockroom;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\stockrooms\models\SrcStockroomStockUsage */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(isset($searchModel->stockroomProduct) )
  $modelStockroom = $searchModel->stockroomProduct->getStockroomInfo($stockroomID);
else
  {  $modelStockroom = SrcStockroom::findOne($_GET['strid']);
  	
  	}

$this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'stock usage');

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>
<div class="row">
      
 <div class="col-lg-3">
       <?php $form = ActiveForm::begin(); ?>
       <?php   
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
										if($stockroomID!='')
                    $searchModel->stockroom_id = $stockroomID;
                    
                    $user_id=0;
                    if(isset(Yii::$app->user->identity->id))
                       $user_id=Yii::$app->user->identity->id;
                    
						echo $form->field($searchModel, 'stockroom_id')->label(false)->widget(Select2::classname(), [

                       'data'=>loadStockrooms($user_id),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select stockroom  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ], 
                       ]);  
		   
								   ?>
          <?php ActiveForm::end(); ?>
           
    </div>
    
</div>
<?= $this->render('//layouts/stockroomSubMenuLayout'); ?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;margin-right:-20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'outp','strid'=>$_GET['strid'],'from'=>'' ], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= Yii::t('app','A new record'); ?></h3>
     </div>
        
     
        
</div> 


<div class="row">
 
<div class="wrapper wrapper-content stockroom-stock-usage-index">
       <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Product Label'); ?></th>
				            <th><?= Yii::t('app','Quantity Out'); ?></th>
				            <th><?= Yii::t('app','Product User'); ?></th>
				            <th><?= Yii::t('app','Taken Date'); ?></th>
				            <th><?= Yii::t('app','Quantity Recieved'); ?></th> 
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 
    
            
           
    $dataStockroomStockUsage= $dataProvider->getModels();
    
              
          foreach($dataStockroomStockUsage as $stockUsage)
           {
           	   $color='';
           	      if( ($stockUsage->quantity > $stockUsage->quantity_recieved)&&($stockUsage->is_return!=0) )
           	         $color = 'red';
           	         
           	   echo '  <tr style="color: '.$color.'; " >';
           	       
           	       if($stockUsage->stockroomProduct->is_equipment==2)
           	          echo '<td ><span data-toggle="tooltip" title="'.$stockUsage->stockroomProduct->state.'">'.$stockUsage->stockroomProduct->getFurnitureName($stockUsage->stockroomProduct->stockroom_product).'</span> </td>';
           	       elseif($stockUsage->stockroomProduct->is_equipment==1)
           	          echo '<td ><span data-toggle="tooltip" title="'.$stockUsage->stockroomProduct->state.'">'.$stockUsage->stockroomProduct->getEquipmentName($stockUsage->stockroomProduct->stockroom_product).'</span> </td>';
           	       elseif($stockUsage->stockroomProduct->is_equipment==0)
                      echo '<td ><span data-toggle="tooltip" title="'.$stockUsage->stockroomProduct->state.'">'.$stockUsage->stockroomProduct->getRawMaterialName($stockUsage->stockroomProduct->stockroom_product).'</span> </td>';
                      
                                      echo         '<td ><span data-toggle="tooltip" title="'.$stockUsage->stockroomProduct->state.'">'.$stockUsage->quantity.'</span> </td>
                                                    <td >'.$stockUsage->productUser->getFullName().' </td>
                                                   
                                                    <td >'.Yii::$app->formatter->asDate($stockUsage->taken_date).' </td>
                                                    <td >';
                                                            if($stockUsage->quantity_recieved == NULL)
                                                                 echo '0';
                                                            else
                                                                 echo $stockUsage->quantity_recieved;
                                                                 
                                                        echo ' </td>
                                                    
                                                    
                                                    
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                         if(Yii::$app->user->can('stockrooms-stockroomstockusage-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomstockusage/view?id='.$stockUsage->id.'&wh=outp&from=res&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                       
                                                       
                                                        if(Yii::$app->user->can('stockrooms-stockroomstockusage-update')) 
                                                              {
												                echo '&nbsp&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomstockusage/update?id='.$stockUsage->id.'&wh=outp&from=upd&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                              
                                                        
                                                              
                                                         if(Yii::$app->user->can('stockrooms-stockroomstockusage-delete')) 
                                                              {
												                echo '&nbsp&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroomstockusage/delete?id='.$stockUsage->id.'&wh=outp&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                              }
                                                         
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>


                
</div>
</div>



<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Stockroom Stock Usage List'},
                    {extend: 'pdf', title: 'Stockroom Stock Usage  List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>


