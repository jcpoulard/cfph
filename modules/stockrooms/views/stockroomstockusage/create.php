<?php

use yii\helpers\Html;

use app\modules\stockrooms\models\SrcStockroom;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomStockUsage */


if(isset($searchModel->stockroomProduct) )
  $modelStockroom = $searchModel->stockroomProduct->getStockroomInfo($stockroomID);
else
  {  $modelStockroom = SrcStockroom::findOne($_GET['strid']);
  	
  	}

$this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'stock usage');

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>


<?= $this->render('//layouts/stockroomSubMenuLayout'); ?>

 
<div class="wrapper wrapper-content stockroom-stock-usage-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
