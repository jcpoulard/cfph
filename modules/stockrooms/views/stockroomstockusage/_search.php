<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\SrcStockroomStockUsage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-stock-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'stockroom_product_id') ?>

    <?= $form->field($model, 'quantity') ?>

    <?= $form->field($model, 'product_user') ?>

    <?= $form->field($model, 'stockkeeper') ?>

    <?php // echo $form->field($model, 'is_return') ?>

    <?php // echo $form->field($model, 'quantity_recieved') ?>

    <?php // echo $form->field($model, 'recieved_by') ?>

    <?php // echo $form->field($model, 'taken_date') ?>

    <?php // echo $form->field($model, 'return_date') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
