<?php

use yii\helpers\Html;

use app\modules\stockrooms\models\SrcStockroom;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomInventory */
if(isset($searchModel->stockroomProduct) )
  $modelStockroom = $searchModel->stockroomProduct->getStockroomInfo($stockroomID);
else
  {  $modelStockroom = SrcStockroom::findOne($_GET['strid']);
  	
  	}

$this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'stock inventory');

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>

 
<div class="wrapper wrapper-content stockroom-inventory-update">

        <?= $this->render('_form', [
        'model' => $model,
        'is_equipment'=>$is_equipment,
    ]) ?>

</div>
