<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomHasProducts;

//use app\modules\fi\models\SrcPerson

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\StockroomInventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-inventory-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
	
	<div class="col-lg-4">
	    
	    <?php 
	              if( (isset($_GET['id'])&&($_GET['id']!='')) )
	                {
	                	echo $form->field($model, 'stockroom_product_id')->widget(Select2::classname(), [
		                       'data'=>loadEquipmentFurnitureOrRawMaterialLabelForInventory($model->stockroom_product_id), 
		                       'size' => Select2::MEDIUM,
		                       'theme' => Select2::THEME_CLASSIC,
		                       'language'=>'fr',
		                       'options'=>['placeholder'=>Yii::t('app', ' --  select product  --'),
		                                    'onchange'=>'submit()',
		                         ],
		                       'pluginOptions'=>[
		                             'allowclear'=>true,
		                         ],
		                       ]);

	                  }
	               else
	                {
	            	   echo $form->field($model, 'stockroom_product_id')->widget(Select2::classname(), [
		                       'data'=>loadEquipmentFurnitureAndRawMaterialLabel($_GET['strid']), //tout label nan depo a
		                       'size' => Select2::MEDIUM,
		                       'theme' => Select2::THEME_CLASSIC,
		                       'language'=>'fr',
		                       'options'=>['placeholder'=>Yii::t('app', ' --  select product  --'),
		                                    'onchange'=>'submit()',
		                         ],
		                       'pluginOptions'=>[
		                             'allowclear'=>true,
		                         ],
		                       ]);
		                       
	                    }
		                       
	             
	             
         ?>
    </div>
</div> 
 
<?php

if($is_equipment!=0)  //equipment-furniture
  {
  	  $modelStockProducts = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
?>
<div class="row"> 

<div class="col-lg-2" style="width:15%; margin-left:15px; margin-top:10px; text-align:center; background-color:#2784C5; color:#FFFFFF; ">
<?= Yii::t('app', 'Last inventory state: ').'<br/><b>'.$modelStockProducts->getState().'</b>'; ?>
</div>

<div class="col-lg-2">
	    <?php echo $form->field($model, 'state')->label(Yii::t('app', 'Current state'))->widget(Select2::classname(), [
			                                   'data'=>loadStateByProductType($is_equipment),  
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]); ?>
	</div>  


<div class="col-lg-2" style="width:17%; text-align:center; margin-top:10px; background-color:#2784C5; color:#FFFFFF; ">
 <?= Yii::t('app', 'Last inventory status: ').'<br/><b>'.$modelStockProducts->getStatus().'</b>'; ?>
</div>

	<div class="col-lg-2">
	    <?php echo $form->field($model, 'status')->label(Yii::t('app', 'Current status'))->widget(Select2::classname(), [
			                                   'data'=>loadInventoryStatusByProductTypeAndState($is_equipment,$model->state),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>['placeholder'=>Yii::t('app', '-- Select --'),
						                                   'onchange'=>'submit()',
						                         ],
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ]); ?>
	</div>   


<?php

  }
elseif($is_equipment==0)  //raw material
  {
  	
?>
 <div class="row">  
    <div class="col-lg-2">
	    <?= $form->field($model, 'opened_quantity')->textInput(['disabled'=>'' ]) ?>
	</div>
   
    
     <div class="col-lg-2">
	    <?= $form->field($model, 'available_quantity')->textInput(['id'=>'val0','disabled'=>'' ]) ?>
	</div>
   
    
    <div class="col-lg-2">
	    <?= $form->field($model, 'quantity_good')->textInput(['id'=>'val1']) ?>
	</div>
   
    <div class="col-lg-2">
	    <?= $form->field($model, 'quantity_bad')->textInput(['id'=>'val2']) ?>
	</div>
   
    <div class="col-lg-2">
	    <?= $form->field($model, 'quantity_bad_removed')->textInput(['id'=>'val3']) ?>
	</div>
   
    <div class="col-lg-2">
	
	    <?= $form->field($model, 'quantity_lost')->textInput(['id'=>'val4']) ?>
	</div>
   
   <div class="col-lg-2">
	    <?= $form->field($model, 'closed_quantity')->textInput(['id'=>'result', 'disabled'=>'' ]) ?>
	</div>


<?php

  }
  	
?>
	<div class="col-lg-4">
	      <?php
                if($model->inventory_date=='')
                  $model->inventory_date = date('Y-m-d');
                  
                echo $form->field($model, 'inventory_date')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>

	 </div>
	 
</div>
	
<div class="row"> 
    <div class="col-lg-12">	
	    <?= $form->field($model, 'comment')->textarea(['rows' => 1]) ?>
	</div>
</div>

	
   

<div class="row">
            <div class="col-lg-4">

            </div>
            <div class="col-lg-4">
               <div class="form-group">
               
                              
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
                </div>
            </div>
            <div class="col-lg-4">

            </div>
   </div> 



   <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS
      
   $(document).ready(function(){
        
        var val0 = $("#val0").val();
	    var val3 = $("#val3").val();
        var val4 = $("#val4").val();
       
        result = parseFloat(val0) - parseFloat(val3) - parseFloat(val4);
            $("#result").val(result);
         
        
        
        $("#val1").keyup(function(){
        var val0 = $("#val0").val();
	           if(val0 =='')
	              val0= 0;
	        var val1 = $("#val1").val();
           if(val1 =='')
              val1= 0;
        var val2 = $("#val2").val();
           if(val2 =='')
              val2= 0;
        var val3 = $("#val3").val();
           if(val3 =='')
              val3= 0;
        var val4 = $("#val4").val();
	           if(val4 =='')
	              val4= 0;
	        
        result = parseFloat(val0) - parseFloat(val3) - parseFloat(val4);
        $("#result").val(result);
            
        });
        
        $("#val2").keyup(function(){
            var val0 = $("#val0").val();
	           if(val0 =='')
	              val0= 0;
	        var val1 = $("#val1").val();
	           if(val1 =='')
	              val1= 0;
	        var val2 = $("#val2").val();
	           if(val2 =='')
	              val2= 0;
	        var val3 = $("#val3").val();
	           if(val3 =='')
	              val3= 0;
	        var val4 = $("#val4").val();
	           if(val4 =='')
	              val4= 0;
	       
        result = parseFloat(val0) - parseFloat(val3) - parseFloat(val4);
            $("#result").val(result);
        });
        
        $("#val3").keyup(function(){
            var val0 = $("#val0").val();
	           if(val0 =='')
	              val0= 0;
	        var val1 = $("#val1").val();
	           if(val1 =='')
	              val1= 0;
	        var val2 = $("#val2").val();
	           if(val2 =='')
	              val2= 0;
	        var val3 = $("#val3").val();
	           if(val3 =='')
	              val3= 0;
	        var val4 = $("#val4").val();
	           if(val4 =='')
	              val4= 0;
	        
        result = parseFloat(val0) - parseFloat(val3) - parseFloat(val4);
            $("#result").val(result);
        });
        
         $("#val4").keyup(function(){
            var val0 = $("#val0").val();
	           if(val0 =='')
	              val0= 0;
	        var val1 = $("#val1").val();
	           if(val1 =='')
	              val1= 0;
	        var val2 = $("#val2").val();
	           if(val2 =='')
	              val2= 0;
	        var val3 = $("#val3").val();
	           if(val3 =='')
	              val3= 0;
	        var val4 = $("#val4").val();
	           if(val4 =='')
	              val4= 0;
	        
        result = parseFloat(val0) - parseFloat(val3) - parseFloat(val4);
            $("#result").val(result);
        });
   });
   
       
  
JS;
$this->registerJs($script);

?>
