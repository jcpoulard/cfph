<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\widgets\Select2; 
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\modules\stockrooms\models\SrcStockroom;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\stockrooms\models\SrcStockroomInventory */
/* @var $dataProvider yii\data\ActiveDataProvider */

if(isset($searchModel->stockroomProduct) )
  $modelStockroom = $searchModel->stockroomProduct->getStockroomInfo($stockroomID);
else
  {  $modelStockroom = SrcStockroom::findOne($_GET['strid']);
  	
  	}

$this->title = $modelStockroom->stockroom_code.' ('.$modelStockroom->stockroom_name.'), '.Yii::t('app', 'stockroom inventory');

?>

 <div class="row">
     
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 

<p></p>
<div class="row">
      
 <div class="col-lg-3">
       <?php $form = ActiveForm::begin(); ?>
       <?php   
                /* Yii::$app->getSession()->setFlash('success', [
										    'type' => 'success', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 12000,
										    'icon' => 'glyphicon glyphicon-ok-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You have successfully created reportcard(s).').' '.Yii::t('app','But').', '.Yii::t('app','Students whose grades have not been validated do not have reportcard.') ),
										    'title' => Html::encode(Yii::t('app','Success').' & '.Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
							*/
										if($stockroomID!='')
                    $searchModel->stockroom_id = $stockroomID;
                    
                    $user_id=0;
                    if(isset(Yii::$app->user->identity->id))
                       $user_id=Yii::$app->user->identity->id;
                    
						echo $form->field($searchModel, 'stockroom_id')->label(false)->widget(Select2::classname(), [

                       'data'=>loadStockrooms($user_id),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select stockroom  --'),
                                    'onchange'=>'submit()',
                                    
                             ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ], 
                       ]);  
		   
								   ?>
          <?php ActiveForm::end(); ?>
           
    </div>
    
</div>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;margin-right:-20px;">
            <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'inv','strid'=>$_GET['strid'],'from'=>'' ], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?> 
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= Yii::t('app','New Inventory'); ?></h3>
     </div>
        
     
        
</div> 


<div class="row">
 
<div class="wrapper wrapper-content stockroom-inventory-index">
                 <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Product Label'); ?></th>
				            <th><?= Yii::t('app','Opened Quantity'); ?></th>
				            <th><?= Yii::t('app','Available Quantity'); ?></th>
				        <!--    <th><?= Yii::t('app','Quantity Good'); ?></th>
				            <th><?= Yii::t('app','Quantity Bad'); ?></th>
				            <th><?= Yii::t('app','Quantity Bad Removed'); ?></th>
				            <th><?= Yii::t('app','Quantity Lost'); ?></th>             -->
				            <th><?= Yii::t('app','Closed Quantity'); ?></th>
				     <!--       <th><?= Yii::t('app','State'); ?></th>           -->
				            <th><?= Yii::t('app','Status'); ?></th>
				            <th><?= Yii::t('app','Inventory Date'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 
   
            
           
    $dataStockroomProductInventory= $dataProvider->getModels();
    
              
          foreach($dataStockroomProductInventory as $productInventory)
           {
           	   echo '  <tr >';
           	       
           	       if($productInventory->stockroomProduct->is_equipment==2)
           	          echo '<td ><span data-toggle="tooltip" title="'.$productInventory->stockroomProduct->state.'">'.$productInventory->stockroomProduct->getFurnitureName($productInventory->stockroomProduct->stockroom_product).'</span> </td>';
           	       elseif($productInventory->stockroomProduct->is_equipment==1)
           	          echo '<td ><span data-toggle="tooltip" title="'.$productInventory->comment.'">'.$productInventory->stockroomProduct->getEquipmentName($productInventory->stockroomProduct->stockroom_product).'</span> </td>';
           	       elseif($productInventory->stockroomProduct->is_equipment==0)
                      echo '<td ><span data-toggle="tooltip" title="'.$productInventory->comment.'">'.$productInventory->stockroomProduct->getRawMaterialName($productInventory->stockroomProduct->stockroom_product).'</span> </td>';
                      
                                      echo         '<td ><span data-toggle="tooltip" title="'.$productInventory->comment.'">'.$productInventory->opened_quantity.'</span> </td>
                                                    <td >'.($productInventory->quantity_good + $productInventory->quantity_bad).' </td>
                                                <!--    <td >'.$productInventory->quantity_good.' </td>
                                                    <td >'.$productInventory->quantity_bad.' </td>
                                                    <td >'.$productInventory->quantity_bad_removed.' </td>
                                                    <td >'.$productInventory->quantity_lost.' </td>         -->
                                                    <td ><span data-toggle="tooltip" title="'.$productInventory->comment.'">'.$productInventory->closed_quantity.'</span> </td>
                                                <!--    <td >'.$productInventory->stockroomProduct->getState().' </td>    
                                                    <td >'.$productInventory->stockroomProduct->getStatus().' </td>    -->
                                                    <td >'.$productInventory->status.' </td>
                                                    <td >'.Yii::$app->formatter->asDate($productInventory->inventory_date).' </td>
                                                    
                                                    
                                                    
                                                    <td >'; 
                                                           
                                                          
                                                          
                                                         
                                                                                                            
                                                       
                                                        if(Yii::$app->user->can('stockrooms-stockroominventory-view')) 
                                                              {
												                echo '&nbsp&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroominventory/view?id='.$productInventory->id.'&wh=inv&from=upd&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              
                                                        
                                                        if(Yii::$app->user->can('stockrooms-stockroominventory-update')) 
                                                              {
												                echo '&nbsp&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroominventory/update?id='.$productInventory->id.'&wh=inv&from=upd&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'Update'),
                        ]); 
                                                              }
                                                              
                                                              
                                                        
                                                              
                                                        /* if(Yii::$app->user->can('stockrooms-stockroominventory-delete')) 
                                                        //      {
												                echo '&nbsp&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/stockrooms/stockroominventory/delete?id='.$productInventory->id.'&wh=inv&strid='.$_GET['strid'].'&upd', [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                                    'data-method' => 'post',
                        ]) ;
                                                         //     }
                                                         */
                                                         
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>




</div>

  </div>
  
  

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Stockroom Inventory List'},
                    {extend: 'pdf', title: 'Stockroom Inventory  List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>



