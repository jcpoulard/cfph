<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\stockrooms\models\SrcStockroomInventory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stockroom-inventory-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'stockroom_product_id') ?>

    <?= $form->field($model, 'quantity_new') ?>

    <?= $form->field($model, 'quantity_good') ?>

    <?= $form->field($model, 'quantity_used') ?>

    <?php // echo $form->field($model, 'quantity_bad') ?>

    <?php // echo $form->field($model, 'opened_quantity') ?>

    <?php // echo $form->field($model, 'closed_quantity') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'inventory_date') ?>

    <?php // echo $form->field($model, 'date_created') ?>

    <?php // echo $form->field($model, 'date_updated') ?>

    <?php // echo $form->field($model, 'is_bad_quantity_removed') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
