<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\stockrooms\models\StockroomStockUsage;
use app\modules\stockrooms\models\SrcStockroomStockUsage;
use app\modules\stockrooms\models\SrcStockroomHasProducts;


use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;


/**
 * StockroomstockusageController implements the CRUD actions for StockroomStockUsage model.
 */
class StockroomstockusageController extends Controller
{
    public $layout = "/inspinia";
    
     public $stockroom_id = 0;
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockroomStockUsage models.
     * @return mixed
     */
    public function actionIndex()
    {
       $strid = $_GET['strid'];//0;
       
     //  if(isset($_GET['strid'])&&($_GET['strid']!=''))
      //     $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
       
       if( (Yii::$app->user->can('stockrooms-stockroomstockusage-index')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

       
        $this->stockroom_id=0;
         
         if(isset($_POST['SrcStockroomStockUsage']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomStockUsage']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroomstockusage/index?wh=outp&strid='.$this->stockroom_id.'&from='));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroomstockusage/index?strid='.$this->stockroom_id.'&from='));
	       }

        $searchModel = new SrcStockroomStockUsage();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'stockroomID' => $this->stockroom_id,
        ]);
        
         }
      else
        {
        	if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            

        
        
    }

    /**
     * Displays a single StockroomStockUsage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
    	
    	if(  (Yii::$app->user->can('stockrooms-stockroomstockusage-view')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
         }
      else
        {
           if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

             if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            

        
    }

    /**
     * Creates a new StockroomStockUsage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
       
        if(  (Yii::$app->user->can('stockrooms-stockroomstockusage-create')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        
        $model = new SrcStockroomStockUsage();

        if ($model->load(Yii::$app->request->post()) ) 
	        {
                $dbTrans = Yii::$app->db->beginTransaction(); 
                
                if(isset($_POST['SrcStockroomStockUsage']['stockroom_product_id']))
				  {  $model->stockroom_product_id = $_POST['SrcStockroomStockUsage']['stockroom_product_id'];
				      
				  }

	           
	           if(isset($_POST['create']))
	            {
	            	 //verifye qt a
	            	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
	            	 
	            	 if($modelStockroomHasProduct->is_equipment!=0)
	            	   $model->quantity = 1;
	    
	            	 
	              if($model->quantity <= $modelStockroomHasProduct->quantity)
	               {
	                    $model->stockkeeper = Yii::$app->user->identity->id;
	                    $model->is_return = 0;
	                    
	                   /* $model->quantity_recieved = NULL;
			                    $model->recieved_by = NULL;
			                    $model->return_date = NULL;
			            */
	                    //$model->quantity_recieved =0;
	                    //$model->recieved_by = currentUser();
	                     	     							
		               if ($model->save()) 
				          {  
				          	 
			          	   
				          	   //si se yon ekipman note li deyo, sinon pa note anyen
						          
						                if($modelStockroomHasProduct->is_equipment!=0)
								          {
								            		 //$modelStockroomHasProduct->things_out = 1;
								            		 //$modelStockroomHasProduct->quantity = 1;
								            		 
								            	  //if($old_status ==3)
								          	       // $modelStockroomHasProduct->status = 1;
								          	       $command = Yii::$app->db->createCommand();
										           $result = $command->update('stockroom_has_products', ['things_out' => 1,   ], ['id'=>$model->stockroom_product_id ])
									                                               ->execute();
									               if ($result)
				                                    {
				                                    	
				                                    	    $dbTrans->commit(); 
				                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
				                                    	}
				                                    else
								          	          {    $dbTrans->rollback();
								                        }                                
								            		 
								            }
								          elseif($modelStockroomHasProduct->is_equipment==0)
								             {
								             	 $modelStockroomHasProduct->things_out = 0;
								             	 
								             	 //retire qt a nan depo 
									          	   $diff_qt = $modelStockroomHasProduct->quantity - $model->quantity;
									          	   $modelStockroomHasProduct->quantity = $diff_qt;
								             	 
								             	 //if($old_status ==3)
								          	       //$modelStockroomHasProduct->status = 1;
								          	       
								          	       if ($modelStockroomHasProduct->save())
				                                    {
				                                    	
				                                    	    $dbTrans->commit(); 
				                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
				                                    	}
				                                    else
								          	          {    $dbTrans->rollback();
								                        }
								               
								               }
								            					              
                                  
				          	   
	                                   
				                	
				                
				          }
				        else
				           {
				             
				           	 $dbTrans->rollback();
				           }
				           
	                }
	              else
	                  { 
					       	    
					       	    Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','Quantity not available.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
					       	 }
	               
	            }
	            
	            
	        }

        
            return $this->render('create', [
                'model' => $model,
            ]);
            
         }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
    
            
            
       
    }

    /**
     * Updates an existing StockroomStockUsage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      $strid = 0;
      $old_status = '';
      $old_quantity=0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
      
        if(  (Yii::$app->user->can('stockrooms-stockroomstockusage-update')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

            //gade si pwodwi sa gentan soti pou pi piti yon fwa anko
                //pou pa bay akse modifye l
                $modelCheck = $this->findModel($id);
                $modelStockUsage1 = new SrcStockroomStockUsage();
				$next_records = $modelStockUsage1->findNextRecords($modelCheck->stockroom_product_id,$modelCheck->taken_date);
            
            if($next_records!=null)
              {
              	   Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','Product already had new user(s) and cannot be modified.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);
															
					$this->redirect(Yii::$app->request->referrer);
                }
            else
             { 
	             $model = $this->findModel($id);
	        
	            $old_quantity = $model->quantity;
	            $r = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
	            //$stockroom_product_id = $model->stockroom_product_id;
	            $model->stockroom_product_id = $r->id;
        
        
          if ($model->load(Yii::$app->request->post()) ) 
				        {
			                if(isset($_POST['SrcStockroomStockUsage']['is_return']))
							  {  $model->is_return = $_POST['SrcStockroomStockUsage']['is_return'];
							      
							  }
							  
			                $dbTrans = Yii::$app->db->beginTransaction(); 
				           
				           if(isset($_POST['update']))
				            {
				            	 //verifye qt a
				            	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
				            	 
				            	 $old_status = $modelStockroomHasProduct->status;
				            
				              if($model->is_return == 0)  //no
				                {
				            	 
						              if($model->quantity <= $modelStockroomHasProduct->quantity)
						               {
						                    $model->stockkeeper = Yii::$app->user->identity->id;
						                    $model->is_return = 0;
						                    
						                    $model->quantity_recieved = NULL;
						                    $model->recieved_by = NULL;
						                    $model->return_date = NULL;
						                     	     							
							               if ($model->save()) 
									          {  
									          	 //retire qt a nan depo 
									          	   $diff_qt = $modelStockroomHasProduct->quantity - ($model->quantity -$old_quantity );
									          	   
									          	   $modelStockroomHasProduct->quantity = $diff_qt;
									          	   
									          	   //si se yon ekipman note li deyo, sinon pa note anyen
											         if($modelStockroomHasProduct->is_equipment!=0)
											            {
											            	$result1 = true;
											            		// $modelStockroomHasProduct->things_out = 1;
											            		 $command = Yii::$app->db->createCommand();
											            	  if($old_status ==3)
											          	       { //$modelStockroomHasProduct->status = 1;
											          	         $result1 = $command->update('stockroom_has_products', ['status' => 1,   ], ['id'=>$model->stockroom_product_id ])
												                                               ->execute();
											          	       }
											          	        
											          	      
													           $result = $command->update('stockroom_has_products', ['things_out' => 1,   ], ['id'=>$model->stockroom_product_id ])
												                                               ->execute();
												               if( ($result)&&($result1) )
							                                    {
							                                    	
							                                    	    $dbTrans->commit(); 
							                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
							                                    	}
							                                    else
											          	          {    $dbTrans->rollback();
											                        }                
											            		 
											            }
											          elseif($modelStockroomHasProduct->is_equipment==0)
											             {
											             	 $modelStockroomHasProduct->things_out = 0;
											             	 
											             	 //if($old_status ==3)
											          	       //$modelStockroomHasProduct->status = 1;
											               
											              
							                                  if ($modelStockroomHasProduct->save())
							                                    {
							                                    	    $dbTrans->commit(); 
							                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
							                                    	}
							                                    else
											          	          {    $dbTrans->rollback();
											                        }
											                        
											             }
									          	   
						                                   
									                	
									                
									          }
									        else
									           {    $dbTrans->rollback();
									           }
									           
						                }
						              else
						                  {  
										       	    
										       	    Yii::$app->getSession()->setFlash('warning', [
															    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
															    'duration' => 36000,
															    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
															    'message' => Html::encode( Yii::t('app','Quantity not available.') ),
															    'title' => Html::encode(Yii::t('app','Warning') ),
															    'positonY' => 'top',   //   top,//   bottom,//
															    'positonX' => 'center'    //   right,//   center,//  left,//
															]);
										       	 }
										       	 
				                 }
				               elseif($model->is_return == 1)  //yes
				                {
				                	  $model->quantity_recieved = 1;
						               			
							               	          $recieved_by = getPersonFullNameByUserId(Yii::$app->user->identity->id);
							               	          
							               	          if($recieved_by == '')
							               	             $recieved_by = currentUser();
							               	          
							               	          
								                    $model->is_return = 1;
								                    $model->recieved_by = $recieved_by;
								                     	     							
									               if ($model->save()) 
											          {  
											          	$command = Yii::$app->db->createCommand();
											          	$result = true;
											          	
											          	 if($old_status ==3)
											          	   {
											          	   	   //$modelStockroomHasProduct->status = 1;
											          	   	   $result = $command->update('stockroom_has_products', ['status' => 1,   ], ['id'=>$model->stockroom_product_id ])
												                                               ->execute();
											          	   	 }
											          	 
											          	 //ajoute qt a nan depo 
											          	  // $plus_qt = $modelStockroomHasProduct->quantity + $model->quantity_recieved;
											          	   
											          	   //$modelStockroomHasProduct->quantity = $plus_qt;
											          	  
											          	   
													           $result1 = $command->update('stockroom_has_products', ['things_out' =>0,   ], ['id'=>$model->stockroom_product_id ])
												                                               ->execute();
												            if( ($result)&&($result1) )
							                                    {
							                                    	    $dbTrans->commit(); 
							                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
							                                    	}
							                                    else
											          	          {    $dbTrans->rollback();
											                        }
											          	   //tcheke si tout quantite a antre deja
											          	   //$modelStockUsage = new SrcStockroomStockUsage();
											          	   //$totally_returned = $modelStockUsage->isEverythingReturned($model->id, $model->stockroom_product_id);
											          	   
											          	   //si se yon ekipman note li deyo, sinon pa note anyen
													      /*   if($modelStockroomHasProduct->is_equipment==1)
													          	{
													          		if($totally_returned==1)//tout remet
													          		   $modelStockroomHasProduct->things_out = 0;
													          		  
													          	}
													          
											              
							                                  if ($modelStockroomHasProduct->save())
							                                    {
							                                    	    $dbTrans->commit(); 
							                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
							                                    	}
							                                    else
											          	          {    $dbTrans->rollback();
											                        }
											          	   */
								                                   
											                	
											                
											          }
											        else
											           {    $dbTrans->rollback();
											           }
										        
									       
									            
						              
							               
										     
									   
			
			
				                  }
				               elseif($model->is_return == 2) //no, lost
				                {
				                	
				                	
						                	//chanje statu prodwi a
						                	//$modelStockroomHasProduct->status = 3;  //lost
						                	
						                	$command = Yii::$app->db->createCommand();
						                	
						                	$result = $command->update('stockroom_stock_usage', ['is_return' => 2,   ], ['id'=>$model->id ])
														                                               ->execute();
											
											$result1 = $command->update('stockroom_has_products', ['status' => 3,   ], ['id'=>$model->stockroom_product_id ])
														                                               ->execute();
													          	         
													              
									                                  if( ($result)&&($result1) )
									                                    {
									                                    	    $dbTrans->commit(); 
									                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
									                                    	}
									                                    else
													          	          {    $dbTrans->rollback();
													                        }
				                	 
				                	
				                 }
				               
				               
				               
				            }
				            
				            
				        }

                    
       
	            return $this->render('update', [
	                'model' => $model,
	            ]);
             
             }
            
         }
      else
        {
           if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

             if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
    
            
            
        
    }

    /**
     * Deletes an existing StockroomStockUsage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
     
        if(  (Yii::$app->user->can('stockrooms-stockroomstockusage-delete')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        
        try
          {
          	   $model2delete = $this->findModel($id);
          	   
          	   $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model2delete->stockroom_product_id);
          	   
          	   if($model2delete->is_return==0) //retounen qt a nan depo
          	    { 
				   $plus_qt = $modelStockroomHasProduct->quantity + $model2delete->quantity;
									          	   
				   $modelStockroomHasProduct->quantity = $plus_qt;
				   
          	     }
          	   else  // yo retounen deja, verifye si se te tout ki te retounen
          	     {
          	     	  if($model2delete->quantity_recieved < $model2delete->quantity) //retounen res la nan depo
          	     	   {
          	     	   	    $plus_qt = $modelStockroomHasProduct->quantity + ($model2delete->quantity - $model2delete->quantity_recieved );
									          	   
				              $modelStockroomHasProduct->quantity = $plus_qt;
          	     	   	}
          	     	   	
          	       }
								          	   
								          	   //tcheke si tout quantite a antre deja
								          	   $modelStockUsage = new SrcStockroomStockUsage();
								          	   $totally_returned = $modelStockUsage->isEverythingReturned($model2delete->id, $model2delete->stockroom_product_id);
								          	   
								          	   //si se yon ekipman note li deyo, sinon pa note anyen
										         if($modelStockroomHasProduct->is_equipment==1)
										          	{
										          		if($totally_returned==1)//tout remet
										          		   $modelStockroomHasProduct->things_out = 0;
										          		  
										          	}
										          else
										             $modelStockroomHasProduct->things_out = 0;
										             
										             
								              
				                                  if ($modelStockroomHasProduct->save())
				                                    {
				                                    	    $model2delete->delete();
				                                    	    return $this->redirect(['/stockrooms/stockroomstockusage/index?wh=outp&strid='.$_GET['strid'].'&']);
				                                    	    
				                                    	}
				
              
          
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}


 }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            



    }

    /**
     * Finds the StockroomStockUsage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockroomStockUsage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomStockUsage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
