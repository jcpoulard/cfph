<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\stockrooms\models\StockroomHasProducts;
use app\modules\stockrooms\models\SrcStockroomHasProducts;
use yii\web\Controller;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;


/**
 * StockroomhasproductsController implements the CRUD actions for StockroomHasProducts model.
 */
class StockroomhasproductsController extends Controller
{
    public $layout = "/inspinia";
    
    public $stockroom_id = 0;
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDashboard()
    {
        if(Yii::$app->user->can('stockrooms-stockroomhasproducts-dashboard'))
         {
              //selon "role" la kite l nan dash la osinon redirect li 
           if(  (Yii::$app->user->can('superadmin') )||(Yii::$app->user->can('direction') )||(Yii::$app->user->can('Magasinier manager') )  )
		     {
		        $searchModel = new SrcStockroomHasProducts();
		        
		        if(isset($_POST['SrcStockroomHasProducts']['stockroom_id']))
				  {  $this->stockroom_id = $_POST['SrcStockroomHasProducts']['stockroom_id'];
				      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
				      
				      return $this->redirect(array('/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$this->stockroom_id.'&from=dash'));
				  }
				else
			     {
			     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
			     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
			     	   //return $this->redirect(array('/stockrooms/stockroomhasproducts/index?strid='.$this->stockroom_id.'&from=dash'));
			       }
		
		        return $this->render('dashboard', [
		            'model' => $searchModel,
		            'stockroomID' => $this->stockroom_id,
		        ]);
		        
		      }
		   else    //redirect
		     {    //cheche yon default depo
		          $stockrooms_array = array();
	                $stockrooms_query = '';
	
	 
		               $stockrooms_query_auth = Yii::$app->db->createCommand('SELECT item_name FROM auth_assignment where user_id='.Yii::$app->user->identity->id.' and item_name like("stockrooms-stockroom%")')->queryAll();
					      
					      if($stockrooms_query_auth!=null)
					       {
					       	   foreach($stockrooms_query_auth as $stockrooms_q)
					       	     {
					       	     	$stockroomID = '';
					       	     	
					       	     	$stockroomStr = substr($stockrooms_q['item_name'],20);
					       	     	
					       	     	$explode_name=explode("-",substr($stockroomStr,0));
					       	     	
					       	     	if(isset($explode_name[0])&&($explode_name[0]!=''))
					       	     	  {
					       	     	  	  
					       	     	  	  if (preg_match('/^[0-9]*$/', $explode_name[0])) 
					       	     	  	       $stockroomID = $explode_name[0];
					       	     	  
					       	     	  }
					       	     	
					       	        if($stockroomID!='' )
					       	     	  {  $this->stockroom_id = $stockroomID;
					       	     	      break;
					       	     	   }
					       	     	
					       	      }
					       	     
					       	     
					       	}
					    /*   
					      if($stockrooms_array!=null)
					        {
					        	$condition='';
					        	$pase=0;
					        	foreach($stockrooms_array as $id)
					                {  $condition= $condition.$id.',';
					                   if($pase==0)
					                      $condition= $condition.$id;
					                   else
					                      $condition= $condition.$id.',';
					                      
					                   $pase=1;
					                   
					                }
					                  
					           $stockrooms_query = Yii::$app->db->createCommand('SELECT id, stockroom_code, stockroom_name FROM stockroom where id in('.$condition.')')->queryAll();
					         }
					         
					         
					    if( ($stockrooms_query!='')&&($stockrooms_query!=null)  )
					    { 
					    	foreach($stockrooms_query as $stockrooms)
					    	  {
					    	  	$code[$stockrooms['id']]= $stockrooms['stockroom_code'].' ('.$stockrooms['stockroom_name'].')';
					    	  	
					    	  	}
					     }
		           */
		     	 
		     	 if($this->stockroom_id!='')
		     	   return $this->redirect(array('/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$this->stockroom_id.'&from=dash'));
		         else
			        {
			            if(Yii::$app->session['currentId_academic_year']=='')
			              {   
			              	  return $this->redirect(['/rbac/user/login']); 
			                }
			             else
			               {  
			              //throw new ForbiddenHttpException;
			              Yii::$app->getSession()->setFlash('Error', [
													    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' =>120000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
													    'title' => Html::encode(Yii::t('app','Unthorized access') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
			              $this->redirect(Yii::$app->request->referrer);
			               }
			
			          }
		      
		      
		       }
        
       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
        
    }



    /**
     * Lists all StockroomHasProducts models.
     * @return mixed
     */
    public function actionIndex()
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
                      

       
       if( (Yii::$app->user->can('stockrooms-stockroomhasproducts-index')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

       
         $this->stockroom_id=0;
         
         if(isset($_POST['SrcStockroomHasProducts']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomHasProducts']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$this->stockroom_id.'&from=dash'));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroomhasproducts/index?strid='.$this->stockroom_id.'&from=dash'));
	       }
            
	     $searchModel = new SrcStockroomHasProducts();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchByStockroomID($this->stockroom_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'stockroomID' => $this->stockroom_id,
        ]);
        
        }
      else
        {
           if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

             if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
        
        
    }

    /**
     * Displays a single StockroomHasProducts model.
     * @param integer $stockroom_product
     * @param string $state
     * @param integer $is_equipment
     * @return mixed
     */
    public function actionView($id)
    {
       
       
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
        
    }

    /**
     * Creates a new StockroomHasProducts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
           
        if(  (Yii::$app->user->can('stockrooms-stockroomhasproducts-create')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        
         $model = new SrcStockroomHasProducts();
         
         $model->is_equipment =0;
        
        $this->stockroom_id=0;
         if(isset($_POST['SrcStockroomHasProducts']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomHasProducts']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$this->stockroom_id.'&from=dash'));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroomhasproducts/index?strid='.$this->stockroom_id.'&from=dash'));
	     	   $model->stockroom_id =$this->stockroom_id;
	       }

       if ($model->load(Yii::$app->request->post()) ) 
	        {
                if(isset($_POST['SrcStockroomHasProducts']['is_equipment']))
				  {  $model->is_equipment = $_POST['SrcStockroomHasProducts']['is_equipment'];
				      
				  }


	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
					if( ($model->is_equipment==1)||($model->is_equipment==2) )
	                  {  
	                  	$model->quantity =1;
	                  	$model->quantity_alert =0;
	                  }
	                
					
				    if($model->state==0)
				      {
				      	  $model->status=NULL;
				      	}
				      	
				      			
	               if ($model->save()) 
			          {   
			          	           
			               //$dbTrans->commit();  	
			                return $this->redirect(['/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$model->stockroom_id.'&']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }

            return $this->render('create', [
                'model' => $model,
                'stockroomID' => $this->stockroom_id,
            ]);
            
        }
      else
        {
           if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

             if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }     
        
    }

    /**
     * Updates an existing StockroomHasProducts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $stockroom_product
     * @param string $state
     * @param integer $is_equipment
     * @return mixed
     */
    public function actionUpdate($id)
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
       
        if( (Yii::$app->user->can('stockrooms-stockroomhasproducts-update')) || (Yii::$app->user->can('stockrooms-stockroomhasproducts-restock')) )
         {

        
        $model = new SrcStockroomHasProducts();
        
        $model_new = $this->findModel($id);
        
        $this->stockroom_id = $model_new->stockroom_id;
        
        
        $model->stockroom_id = $model_new->stockroom_id;
        
        if( isset($_GET['from'])&&($_GET['from']=='res') )
		  {
				$model->is_equipment = $model_new->is_equipment;
				$model->stockroom_product = $model_new->stockroom_product;
				$model->stockroom_id = $model_new->stockroom_id;
				$model->state = $model_new->state;	 
				$model->quantity = 	0;
			}
		else
		  {
		  	   $model = $model_new;
		  	}
        

        if ($model->load(Yii::$app->request->post()) ) 
	        {
                 
               if($model->state==0)
				      {
				      	  $model->status=NULL;
				      	}
				
			    // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
					       if ($model->save()) 
					          {   //$dbTrans->commit();  	
					                return $this->redirect(['/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$model->stockroom_id.'&']);
					          }
					        else
					           { //   $dbTrans->rollback();
					           }
						 
							               
	            }
	            
	            
	        }
	    
	    
            return $this->render('update', [
                'model' => $model,
                'stockroomID' => $this->stockroom_id,
            ]);
         }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
        
    }

    /**
     * Deletes an existing StockroomHasProducts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $stockroom_product
     * @param string $state
     * @param integer $is_equipment
     * @return mixed
     */
    public function actionDelete($id)
    {
      $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
      
      if(  (Yii::$app->user->can('stockrooms-stockroomhasproducts-delete')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

      
       try
         { 
         	 $this->findModel($id)->delete();

             return $this->redirect(['index?wh=lisp&strid='.$_GET['strid'].'&']);
             
          
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}


 }
      else
        {
           if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

             if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the StockroomHasProducts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $stockroom_product
     * @param string $state
     * @param integer $is_equipment
     * @return StockroomHasProducts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomHasProducts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
