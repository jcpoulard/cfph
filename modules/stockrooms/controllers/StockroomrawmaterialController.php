<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\stockrooms\models\StockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomHasProducts;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;


/**
 * StockroomrawmaterialController implements the CRUD actions for StockroomRawMaterial model.
 */
class StockroomrawmaterialController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockroomRawMaterial models.
     * @return mixed
     */
    public function actionIndex()
    {
    	
    	if(Yii::$app->user->can('stockrooms-stockroomrawmaterial-index'))
         {

        $searchModel = new SrcStockroomRawMaterial();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
        
    }

    /**
     * Displays a single StockroomRawMaterial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
       
      

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
        
    }

    /**
     * Creates a new StockroomRawMaterial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('stockrooms-stockroomrawmaterial-create'))
         {

       
        $model = new SrcStockroomRawMaterial();
        
        

         if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
	               $automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
	                
	                if($automatic_code_ ==1)
	                   $model->setAttribute('code','RM-');
							
	               if ($model->save()) 
			          {   
			          	  
	                    if($automatic_code_ ==1)
						   {
					        
								$cp='';
					           
							$explode_name=explode(" ",substr($model->productLabel->product_description,0));

				            if(isset($explode_name[1])&&($explode_name[1]!=''))
							 {
						        $cp = strtoupper(  substr(strtr($explode_name[0],pa_daksan() ), 0,2).substr(strtr($explode_name[1],pa_daksan() ), 0,4)  );

							 }
							else
							 {  $cp = strtoupper( substr(strtr($explode_name[0],pa_daksan() ), 0,4)  );

							 }
							 
							
							
						
							 

							  $code_ = Yii::t('app', 'RM-').$cp.'-'.$model->id;


								  $model->setAttribute('code',$code_);
								  
								  $model->save();

							}
			          	  
			          	   
                                   
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_rm']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }
	        
            return $this->render('create', [
                'model' => $model,
            ]);
            
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
        
    }

    /**
     * Updates an existing StockroomRawMaterial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('stockrooms-stockroomrawmaterial-update'))
         {

        
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
						$automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
	                    if($automatic_code_ ==1)
						   {
					        
								$cp='';
					           
							$explode_name=explode(" ",substr($model->productLabel->product_description,0));

				            if(isset($explode_name[1])&&($explode_name[1]!=''))
							 {
						        $cp = strtoupper(  substr(strtr($explode_name[0],pa_daksan() ), 0,2).substr(strtr($explode_name[1],pa_daksan() ), 0,4)  );

							 }
							else
							 {  $cp = strtoupper( substr(strtr($explode_name[0],pa_daksan() ), 0,4)  );

							 }
							 
							
							
						
							 

							  $code_ = Yii::t('app', 'RM-').$cp.'-'.$model->id;


								  $model->setAttribute('code',$code_);
								  
							

							}
	
	               if ($model->save()) 
			          {           
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_rm']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }

            return $this->render('update', [
                'model' => $model,
            ]);
            
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
        
    }

    /**
     * Deletes an existing StockroomRawMaterial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       if(Yii::$app->user->can('stockrooms-stockroomrawmaterial-delete'))
         {

       
       try
        { 
        	$model2delete = $this->findModel($id);
        	
        	//gad dil gentan nan yon magazen pou pa delete li
        	$prod_stock = SrcStockroomHasProducts::find()->where('stockroom_product='.$id.' and is_equipment=0')->all();
        	
        	if($prod_stock!=null)
        	  {  
        	  	Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
        	  	$this->redirect(Yii::$app->request->referrer);//return $this->redirect(['index','wh'=>'set_rm']);
        	  }
        	else
        	  { $model2delete->delete();

                  return $this->redirect(['index','wh'=>'set_rm']);
        	  }
        
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}
			
			
			
			 }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the StockroomRawMaterial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockroomRawMaterial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomRawMaterial::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
