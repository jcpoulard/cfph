<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\stockrooms\models\Stockroom;
use app\modules\stockrooms\models\SrcStockroom;

use app\modules\rbac\models\AuthItem;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * StockroomController implements the CRUD actions for Stockroom model.
 */
class StockroomController extends Controller
{
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stockroom models.
     * @return mixed
     */
    public function actionIndex()
    {
        //Yii::$app->session['stockroom_id'] ='';
        
        $searchModel = new SrcStockroom();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stockroom model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stockroom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SrcStockroom();
        
        
	        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
	               $automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
	               
	                if($automatic_code_ ==1)
	                      $model->setAttribute('stockroom_code','MAG-');
							
	               if ($model->save()) 
			          {   
			          	  
	                if($automatic_code_ ==1)
						   {
					        
								$cm='';
					          
							$explode_name=explode(" ",substr($model->stockroom_name,0));

				            if(isset($explode_name[1])&&($explode_name[1]!=''))
							 {
						        $cm = strtoupper(  substr(strtr($explode_name[0],pa_daksan() ), 0,2).substr(strtr($explode_name[1],pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cm = strtoupper( substr(strtr($explode_name[0],pa_daksan() ), 0,3)  );

							 }

							 

							  $code_ = 'MAG-'.$cm.'-'.$model->id;


								  $model->setAttribute('stockroom_code',$code_);
								  
								  $model->save();

							}
			          	  
			          	   $command3 = Yii::$app->db->createCommand();
                           $command3->insert('auth_item', ['name'=>"stockrooms-stockroom".$model->id, 'type'=>2, ])->execute();
                           $command3->insert('auth_item', ['name'=>"stockrooms-stockroom".$model->id."-index", 'type'=>2, ])->execute();
                           $command3->insert('auth_item', ['name'=>"stockrooms-stockroom".$model->id."-create", 'type'=>2, ])->execute();
                           $command3->insert('auth_item', ['name'=>"stockrooms-stockroom".$model->id."-update", 'type'=>2, ])->execute();
                           $command3->insert('auth_item', ['name'=>"stockrooms-stockroom".$model->id."-delete", 'type'=>2, ])->execute();
                           
                           //bay manager magazen tout pemisyon sa yo
                           $command3->insert('auth_item_child', ['parent'=>"Magasinier manager", 'child'=>"stockrooms-stockroom".$model->id, ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"Magasinier manager", 'child'=>"stockrooms-stockroom".$model->id."-index", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"Magasinier manager", 'child'=>"stockrooms-stockroom".$model->id."-create", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"Magasinier manager", 'child'=>"stockrooms-stockroom".$model->id."-update", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"Magasinier manager", 'child'=>"stockrooms-stockroom".$model->id."-delete", ])->execute();
                           
                           //bay direction tout pemisyon sa yo
                           $command3->insert('auth_item_child', ['parent'=>"direction", 'child'=>"stockrooms-stockroom".$model->id, ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"direction", 'child'=>"stockrooms-stockroom".$model->id."-index", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"direction", 'child'=>"stockrooms-stockroom".$model->id."-create", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"direction", 'child'=>"stockrooms-stockroom".$model->id."-update", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"direction", 'child'=>"stockrooms-stockroom".$model->id."-delete", ])->execute();
                           
                           //bay superadmin tout pemisyon sa yo
                           $command3->insert('auth_item_child', ['parent'=>"superadmin", 'child'=>"stockrooms-stockroom".$model->id, ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"superadmin", 'child'=>"stockrooms-stockroom".$model->id."-index", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"superadmin", 'child'=>"stockrooms-stockroom".$model->id."-create", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"superadmin", 'child'=>"stockrooms-stockroom".$model->id."-update", ])->execute();
                           $command3->insert('auth_item_child', ['parent'=>"superadmin", 'child'=>"stockrooms-stockroom".$model->id."-delete", ])->execute();
                           
                           
                                   
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_str']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }
	        
	        
            return $this->render('create', [
                'model' => $model,
            ]);
        
    }

    /**
     * Updates an existing Stockroom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
	               
	                $automatic_code_ = infoGeneralConfig('stockroom_automaticCode');
	                if($automatic_code_ ==1)
						   {
					        
								$cm='';
					           //first_name
							$explode_name=explode(" ",substr($model->stockroom_name,0));

				            if(isset($explode_name[1])&&($explode_name[1]!=''))
							 {
						        $cm = strtoupper(  substr(strtr($explode_name[0],pa_daksan() ), 0,2).substr(strtr($explode_name[1],pa_daksan() ), 0,1)  );

							 }
							else
							 {  $cm = strtoupper( substr(strtr($explode_name[0],pa_daksan() ), 0,3)  );

							 }

							 

							  $code_ = 'MAG-'.$cm.'-'.$model->id;


								  $model->setAttribute('stockroom_code',$code_);
								  
								  

							}

							
	               if ($model->save()) 
			          {            
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_str']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }
	        


            return $this->render('update', [
                'model' => $model,
            ]);
        
    }

    /**
     * Deletes an existing Stockroom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
       
      try
       {  
        $model = $this->findModel($id);
        $model->delete();
         
         //(auth_item_child) retire tout pemission ki te sou magazen sa
				
		//jwenn tout paran item sa
        Yii::$app->db->createCommand()
			        ->delete(
				        'auth_item_child',
				        'child LIKE :name',
				        [
				            'name' => 'stockrooms-stockroom'.$id.'%',
				        ]
			    	)->execute();
			    	
	      Yii::$app->db->createCommand()
			        ->delete(
				        'auth_item',
				        'name LIKE :name',
				        [
				            'name' => 'stockrooms-stockroom'.$id.'%',
				        ]
			    	)->execute();
			    	
		 Yii::$app->db->createCommand()
			        ->delete(
				        'auth_assignment',
				        'item_name LIKE :name',
				        [
				            'name' => 'stockrooms-stockroom'.$id.'%',
				        ]
			    	)->execute();		 	          	                        
                                       
          return $this->redirect(['index','wh'=>'set_str']);
          
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}
    }

    /**
     * Finds the Stockroom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stockroom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stockroom::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
