<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;
use app\modules\stockrooms\models\StockroomInventory;
use app\modules\stockrooms\models\SrcStockroomInventory;
use app\modules\stockrooms\models\SrcStockroomHasProducts;
use app\modules\stockrooms\models\SrcStockroomStockUsage;
use app\modules\stockrooms\models\SrcStockroomRestock;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * StockroominventoryController implements the CRUD actions for StockroomInventory model.
 */
class StockroominventoryController extends Controller
{
    public $layout = "/inspinia";
    
     public $stockroom_id = 0;
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

  
      public function actionDashboard()
    {
        if(Yii::$app->user->can('stockrooms-stockroominventory-dashboard'))
         {

        $searchModel = new SrcStockroomHasProducts();
        
        if(isset($_POST['SrcStockroomHasProducts']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomHasProducts']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroominventory/index?wh=inv&strid='.$this->stockroom_id.'&from=dash'));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroomhasproducts/index?strid='.$this->stockroom_id.'&from=dash'));
	       }

        return $this->render('dashboard', [
            'model' => $searchModel,
            'stockroomID' => $this->stockroom_id,
        ]);
        
       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }  
        
    }



    /**
     * Lists all StockroomInventory models.
     * @return mixed
     */
    public function actionIndex()
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
       
       if(  (Yii::$app->user->can('stockrooms-stockroominventory-index')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

       
        $this->stockroom_id=0;
         
         if(isset($_POST['SrcStockroomInventory']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomInventory']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroominventory/index?wh=inv&strid='.$this->stockroom_id.'&from='));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroominventory/index?strid='.$this->stockroom_id.'&from='));
	       }


        $searchModel = new SrcStockroomInventory();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'stockroomID' => $this->stockroom_id,
        ]);
        
         }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            

        
    }

    /**
     * Displays a single StockroomInventory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
      
       if(  (Yii::$app->user->can('stockrooms-stockroominventory-view')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

       
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
 
        
    }

    /**
     * Creates a new StockroomInventory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       
       $strid = 0;
       $is_equipment = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
       
       if(  (Yii::$app->user->can('stockrooms-stockroominventory-create')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {
       
        $model = new SrcStockroomInventory();
        $model->quantity_bad_removed =0;
        $model->quantity_lost =0;
        $model->quantity_good =0;
        $model->quantity_bad =0;
        $model->opened_quantity =0;
        $model->available_quantity=0;
        $model->closed_quantity =0;
        
         
        
         if ($model->load(Yii::$app->request->post()) ) 
	        {
                $dbTrans = Yii::$app->db->beginTransaction(); 
                
                 if(($_POST['SrcStockroomInventory']['stockroom_product_id']!=''))
				  {  
				  	   $model->stockroom_product_id = $_POST['SrcStockroomInventory']['stockroom_product_id'];
				     
				      $modelStockProducts = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
				      
				    //gade denye fwa yo reyapwovizyone pwodwi sa
				      $data_restock = SrcStockroomRestock::find()->where('stockroom_product_id ='.$model->stockroom_product_id.' AND restock_date=(select MAX(restock_date) from stockroom_restock where stockroom_product_id ='.$model->stockroom_product_id.') ')->all();
				      
				      $quantity_out=0;
				      $is_equipment = $modelStockProducts->is_equipment;
				      
				      if($data_restock == NULL)
				       {
					      //jwenn ki kantite ki deyo 
					      
	    	   	          $data = SrcStockroomStockUsage::find()->where('stockroom_product_id ='.$model->stockroom_product_id.' AND is_return =0 ')->all();
	   	    
					   	    if($data!=null)
					   	      {
					   	      	foreach($data as $r)
					   	      	  {
					   	      	  	   if(isset($r->stockroom_product_id) )
					   	      	  	     {  
					   	      	  	     	$quantity_out= $quantity_out + $r->quantity;
					   	      	  	       
					   	      	  	     }
					   	      	  	}
					   	      	}
					   	     else
					   	       $quantity_out=0;
					   	       
					   	 $model->opened_quantity = $modelStockProducts->quantity + $quantity_out;
					   	        
				       }
				     else //$data_restock pa vid
				       {
				       	   
					   	      	foreach($data_restock as $r)
					   	      	  {
					   	      	  	   if(isset($r->stockroom_product_id) )
					   	      	  	     {  
					   	      	  	     	$quantity_out= $quantity_out + $r->quantity_after;
					   	      	  	       
					   	      	  	     }
								   	  // else
								   	  //     $quantity_out=0;
					   	      	  	
					   	      	}
					   	      	
					   	      	 $model->opened_quantity = $quantity_out;
				       	}
		   	       
				     
				      
				      $model->available_quantity=$modelStockProducts->quantity;
				     
				      
				  }
				  
		 if($is_equipment!=0)
		   {
		   	 if(isset($_POST['SrcStockroomInventory']['state']))
				  {  $model->state = $_POST['SrcStockroomInventory']['state'];
				      
				  }
				  
		     if(isset($_POST['SrcStockroomInventory']['status']))
				  {  $model->status = $_POST['SrcStockroomInventory']['status'];
				      
				  }
		   	
		   	}
	           
	    if(isset($_POST['create']))
	            {
	            	$pass=0;
	            	
	             if($is_equipment!=0)
		          {
		          	$model->opened_quantity = 1;
		          	$model->closed_quantity = 1;
		          	$model->available_quantity = 1;
		          	$model->quantity_good = 1;
		          	$model->quantity_lost =0;
		          	
		          	   if($model->state==1)  //good
		          	    {
		          	    	  if($model->status==1)   //in use
				          	    {
				          	    	$model->available_quantity =1;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
				          	      }
				          	   elseif($model->status==3)   //lost
				          	     {
				          	     	   $model->available_quantity =0;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0)  //removed
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }
		          	       
		          	       
		          	      }
		          	   elseif($model->state==2)  //damaged
		          	     {
		          	     	   if($model->status==1)   //in use
				          	    {
				          	    	 $model->available_quantity =1;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
				          	      }
				          	   elseif($model->status==2)   //in repair
				          	     {
				          	     	$model->available_quantity =1;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
					          	        
				          	       }
				          	     elseif($model->status==3)  //lost
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0)  //removed
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }

		          	       }
		          	     elseif($model->state==0)  //bad
		          	     {
		          	     	    if($model->status==3)  //lost
				          	     {
				          	     	$model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0) // removed
				          	     {
				          	     	   $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }

		          	       }
		          	
		          	
		          	
		          	     
		          	  
		          	   
		          	   
		          	            $model->date_created=date('Y-m-d');
		                        $model->created_by=Yii::$app->user->identity->id;
		                          
		                     $pass=1;
		          	    
		          	
		          }
		        else
		          {
	            	
	            	$model->closed_quantity = $model->available_quantity - $model->quantity_bad_removed - $model->quantity_lost;
	            	$closed_quantity = $model->closed_quantity;
	            	$check_good= $closed_quantity - ($model->quantity_bad - $model->quantity_bad_removed);
	                
	                //$check_close = $model->opened_quantity - $model->quantity_bad_removed - $model->quantity_lost;
	               
			       	         
			            if( $model->quantity_bad_removed<= $model->quantity_bad )
			             { 
			               	
			               	if( $model->quantity_good== $check_good )
			                 { 
			                
			                      $model->date_created=date('Y-m-d');
		                          $model->created_by=Yii::$app->user->identity->id;
			                   
			                      $pass=1;
						              
						      
			               }
			              else
			                  { 
							       	   					       	    
							       	    Yii::$app->getSession()->setFlash('warning', [
												    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','Please review the numbers for GOOD, BAD, and BAD REMOVED quantities, they do not reflect the closing quantity.') ),
												    'title' => Html::encode(Yii::t('app','Warning') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
							       	 }
			               
			               
			               }
			              else
			                  { 
							       	   					       	    
							       	    Yii::$app->getSession()->setFlash('warning', [
												    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
												    'duration' => 36000,
												    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
												    'message' => Html::encode( Yii::t('app','"Quantity bad removed" cannot be higher than "quantity bad".') ),
												    'title' => Html::encode(Yii::t('app','Warning') ),
												    'positonY' => 'top',   //   top,//   bottom,//
												    'positonX' => 'center'    //   right,//   center,//  left,//
												]);
							       	 }
			               
                           
		          }
	               
	               
	                      if($pass==1)
	                        {
	                        	
	                              if ($model->save()) 
						          {  
				
						          	 //update qt ki nan depo a
						          	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);  
						          	   //$modelStockroomHasProduct->quantity = $model->closed_quantity;
						          	   
						          	    if($is_equipment!=0)
						          	      {
						          	      	   $modelStockroomHasProduct->state = $model->state;
						          	      	   
						          	      	   $modelStockroomHasProduct->status = $model->status;
						          	      	}
						          	     else
						          	        $modelStockroomHasProduct->quantity = $model->closed_quantity;
		          
						          	   
						          	    if ($modelStockroomHasProduct->save())
		                                    {
		                                    	    $dbTrans->commit(); 
		                                    	    return $this->redirect(['/stockrooms/stockroominventory/index?wh=inv&strid='.$_GET['strid'].'&']);
		                                    	}
		                                    else
						          	          {    $dbTrans->rollback();
						                        }
						          	   
			                                   
						                	
						                
						            }
						         else
						            {    $dbTrans->rollback();
						              }
						              	
	                          }
	               
	               
	               
	               
	               
	               
	               
	             }
	             
	         

						                
						                
               } 
               
               
            return $this->render('create', [
                'model' => $model,
                'is_equipment'=>$is_equipment,
            ]);
            
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
     
            
        
    }

    /**
     * Updates an existing StockroomInventory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $strid = 0;
        $is_equipment = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
           
           
        
        if(  (Yii::$app->user->can('stockrooms-stockroominventory-update')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        
        $model = $this->findModel($id);
        
        $stockroom_product_id = $model->stockroom_product_id;
        
        
        $modelStockProducts0 = SrcStockroomHasProducts::findOne($stockroom_product_id);
        
        $is_equipment = $modelStockProducts0->is_equipment;

        
        if ($model->load(Yii::$app->request->post()) ) 
	        {
                $dbTrans = Yii::$app->db->beginTransaction(); 
                
                 if(isset($_POST['SrcStockroomInventory']['stockroom_product_id']))
				  {  
				  	   $model->stockroom_product_id = $_POST['SrcStockroomInventory']['stockroom_product_id'];
				     
				      $modelStockProducts12 = SrcStockroomHasProducts::findOne($stockroom_product_id);
				      
				      $is_equipment = $modelStockProducts12->is_equipment;
				      
				      $model->opened_quantity = $modelStockProducts12->quantity;
				      
				      
				  }
				  
				  
	            if($is_equipment!=0)
				   {
				   	 if(isset($_POST['SrcStockroomInventory']['state']))
						  {  $model->state = $_POST['SrcStockroomInventory']['state'];
						      
						  }
						  
				     if(isset($_POST['SrcStockroomInventory']['status']))
						  {  $model->status = $_POST['SrcStockroomInventory']['status'];
						      
						  }
				   	
				   	}
				   	
				   	
	           if(isset($_POST['update']))
	            {
	                $pass=0;
	            	
		             if($is_equipment!=0)
			          {
			          	$model->opened_quantity = 1;
		          	$model->closed_quantity = 1;
		          	$model->available_quantity = 1;
		          	$model->quantity_good = 1;
		          	$model->quantity_lost =0;
		          	
		          	   if($model->state==1)  //good
		          	    {
		          	    	  if($model->status==1)   //in use
				          	    {
				          	    	$model->available_quantity =1;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
				          	      }
				          	   elseif($model->status==3)   //lost
				          	     {
				          	     	   $model->available_quantity =0;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0)  //removed
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 1;
					          	       $model->quantity_bad =0;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }
		          	       
		          	       
		          	      }
		          	   elseif($model->state==2)  //damaged
		          	     {
		          	     	   if($model->status==1)   //in use
				          	    {
				          	    	 $model->available_quantity =1;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
				          	      }
				          	   elseif($model->status==2)   //in repair
				          	     {
				          	     	$model->available_quantity =1;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 1;
					          	        
				          	       }
				          	     elseif($model->status==3)  //lost
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0)  //removed
				          	     {
				          	     	  $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }

		          	       }
		          	     elseif($model->state==0)  //bad
		          	     {
		          	     	    if($model->status==3)  //lost
				          	     {
				          	     	$model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =1;
		          	     	           $model->quantity_bad_removed =0;
					          	        $model->closed_quantity = 0;
		          	        
				          	       }
				          	      elseif($model->status==0) // removed
				          	     {
				          	     	   $model->available_quantity =0;
					          	       $model->quantity_good = 0;
					          	       $model->quantity_bad =1;
		          	     	           $model->quantity_lost =0;
		          	     	           $model->quantity_bad_removed =1;
					          	        $model->closed_quantity = 0;
				          	       }

		          	       }
		          				          	   
			          	   
			          	   $model->date_updated=date('Y-m-d');
			                          $model->updated_by=Yii::$app->user->identity->id;
			                          
			                     $pass=1;
			          	    
			          	
			          }
			        else
			          {
		            	
		            	$model->closed_quantity = $model->available_quantity - $model->quantity_bad_removed - $model->quantity_lost;
		            	$closed_quantity = $model->closed_quantity;
		            	$check_good= $closed_quantity - ($model->quantity_bad - $model->quantity_bad_removed);
		                
		                //$check_close = $model->opened_quantity - $model->quantity_bad_removed - $model->quantity_lost;
		               
				       	         
				            if( $model->quantity_bad_removed<= $model->quantity_bad )
				             { 
				               	
				               	if( $model->quantity_good== $check_good )
				                 { 
				                
				                      $model->date_created=date('Y-m-d');
			                          $model->created_by=Yii::$app->user->identity->id;
				                   
				                      $pass=1;
							              
							      
				               }
				              else
				                  { 
								       	   					       	    
								       	    Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','Please review the numbers for GOOD, BAD, and BAD REMOVED quantities, they do not reflect the closing quantity.') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
								       	 }
				               
				               
				               }
				              else
				                  { 
								       	   					       	    
								       	    Yii::$app->getSession()->setFlash('warning', [
													    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
													    'duration' => 36000,
													    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
													    'message' => Html::encode( Yii::t('app','"Quantity bad removed" cannot be higher than "quantity bad".') ),
													    'title' => Html::encode(Yii::t('app','Warning') ),
													    'positonY' => 'top',   //   top,//   bottom,//
													    'positonX' => 'center'    //   right,//   center,//  left,//
													]);
								       	 }
				               
	                           
			          }	     
			          
			          
			         
	                      if($pass==1)
	                        {
	                        	
	                              if ($model->save()) 
						          {  
				
						          	 //update qt ki nan depo a
						          	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);  
						          	   //$modelStockroomHasProduct->quantity = $model->closed_quantity;
						          	   
						          	    if($is_equipment!=0)
						          	      {
						          	      	   $modelStockroomHasProduct->state = $model->state;
						          	      	   
						          	      	   $modelStockroomHasProduct->status = $model->status;
						          	      	}
						          	     else
						          	        $modelStockroomHasProduct->quantity = $model->closed_quantity;
		          
						          	   
						          	    if ($modelStockroomHasProduct->save())
		                                    {
		                                    	    $dbTrans->commit(); 
		                                    	    return $this->redirect(['/stockrooms/stockroominventory/index?wh=inv&strid='.$_GET['strid'].'&']);
		                                    	}
		                                    else
						          	          {    $dbTrans->rollback();
						                        }
						          	   
			                                   
						                	
						                
						            }
						         else
						            {    $dbTrans->rollback();
						              }
						              	
	                          }
	                          
	                          
	                                        
	               
	               
	             }
	             
	         

						                
						                
               } 
               

            return $this->render('update', [
                'model' => $model,
                'is_equipment'=>$is_equipment,
            ]);
        
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
 
        
        
        
    }

    /**
     * Deletes an existing StockroomInventory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
           
      
      if( (Yii::$app->user->can('stockrooms-stockroominventory-delete')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

      
        try
          {
          	  $this->findModel($id)->delete();

               return $this->redirect(['index?wh=invp&strid='.$_GET['strid'].'&']);
        
          
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}



 }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            


    }

    /**
     * Finds the StockroomInventory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockroomInventory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomInventory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
