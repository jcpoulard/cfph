<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\stockrooms\models\StockroomProductsLabel;
use app\modules\stockrooms\models\SrcStockroomProductsLabel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;


/**
 * StockroomproductslabelController implements the CRUD actions for StockroomProductsLabel model.
 */
class StockroomproductslabelController extends Controller
{
    public $layout = "/inspinia";
    
     public $is_ajax=false;

    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockroomProductsLabel models.
     * @return mixed
     */
    public function actionIndex()
    {
       if(Yii::$app->user->can('stockrooms-stockroomproductslabel-index'))
         {

       
        $searchModel = new SrcStockroomProductsLabel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
        
        
    }

    /**
     * Displays a single StockroomProductsLabel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
              
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
        
    }

    /**
     * Creates a new StockroomProductsLabel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       if(Yii::$app->user->can('stockrooms-stockroomproductslabel-create'))
         {

       
        $model = new SrcStockroomProductsLabel();

        $ok=false;
		        
		
		       
		        
				             if(isset($_POST['create_ajax']))
									            {
									                $this->is_ajax=true;
									                $model->load(Yii::$app->request->post());
									                
									                 if(isset($_GET['cat']) &&($_GET['cat']!=''))
									                   {
									                   	    if($_GET['cat']==0) 
									                   	       $model->category = 0;
									                   	    elseif($_GET['cat']==1) 
									                   	       $model->category = 1;
									                   	    elseif($_GET['cat']==2) 
									                   	       $model->category = 2;
									                   	}
									                 
											               if ($model->save()) 
													          {   
													               //$dbTrans->commit();
													               	if(isset($_GET['from']))
													               	  {
													               	  	 if($_GET['from']=='eq')
													                        return $this->redirect(['stockroomequipment/create','id_prl'=>$model->id,'from1'=>'pro','wh'=>'set_eq',]);
													                     elseif($_GET['from']=='fu')
													                        {
													                        	    
													                        	return $this->redirect(['stockroomfurniture/create','id_prl'=>$model->id,'from1'=>'pro', 'wh'=>'set_fur',]);  
													                        	
													                        }
													                      elseif($_GET['from']=='rm')
													                        {
													                        	    
													                        	return $this->redirect(['stockroomrawmaterial/create','id_prl'=>$model->id,'from1'=>'pro', 'wh'=>'set_rm',]);  
													                        	
													                        }
													                      
													               	  }
													              
													          }
													        else
													           { //   $dbTrans->rollback();
													           }
													           
									               }
								               

        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['create']))
	            {
							
	               if ($model->save()) 
			          {           
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_spl']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }
       
       
         if ( (Yii::$app->request->isAjax) )
				{  
			
					 $this->is_ajax=true;
					 
					return $this->renderAjax('_ajax_form', [
							                'model' => $model,
							                'error'=>false,
							            ]);
                 }
		       elseif ( $this->is_ajax==true )
				{  
					 $this->is_ajax=true;
					 
					return $this->render('_ajax_form', [
							                'model' => $model,
							                'error'=>true,
							            ]);
                 }
                 else 
		         { 
		            return $this->render('create', [
		                'model' => $model,
		            ]);
		            
		         }

        
         }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        

    }

    /**
     * Updates an existing StockroomProductsLabel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
       if(Yii::$app->user->can('stockrooms-stockroomproductslabel-update'))
         {
       
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
	        {
	            // $dbTrans = Yii::app->db->beginTransaction(); 
	           
	           if(isset($_POST['update']))
	            {
							
	               if ($model->save()) 
			          {           
			               //$dbTrans->commit();  	
			                return $this->redirect(['index','wh'=>'set_spl']);
			          }
			        else
			           { //   $dbTrans->rollback();
			           }
	               
	            }
	            
	            
	        }

            return $this->render('update', [
                'model' => $model,
            ]);
            
        }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }     
    }

    /**
     * Deletes an existing StockroomProductsLabel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      if(Yii::$app->user->can('stockrooms-stockroomproductslabel-delete'))
       {

        try
        {  
        	$this->findModel($id)->delete();

           return $this->redirect(['index','wh'=>'set_spl']);
        
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}
			
			 }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }

    }

    /**
     * Finds the StockroomProductsLabel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockroomProductsLabel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomProductsLabel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
