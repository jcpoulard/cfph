<?php

namespace app\modules\stockrooms\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Html;

use app\modules\stockrooms\models\StockroomRestock;
use app\modules\stockrooms\models\SrcStockroomRestock;
use app\modules\stockrooms\models\SrcStockroomHasProducts;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * StockroomrestockController implements the CRUD actions for StockroomRestock model.
 */
class StockroomrestockController extends Controller
{
    public $layout = "/inspinia";
    
     public $stockroom_id = 0;
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockroomRestock models.
     * @return mixed
     */
    public function actionIndex()
    {
                $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
       
       if(  (Yii::$app->user->can('stockrooms-stockroomrestock-index')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

       
        $this->stockroom_id=0;
         
         if(isset($_POST['SrcStockroomRestock']['stockroom_id']))
		  {  $this->stockroom_id = $_POST['SrcStockroomRestock']['stockroom_id'];
		      //Yii::$app->session['stockroom_id'] = $this->stockroom_id;
		      
		      return $this->redirect(array('/stockrooms/stockroomrestock/index?wh=rest&strid='.$this->stockroom_id.'&from='));
		  }
		else
	     {
	     	 if(isset($_GET['strid'])&&($_GET['strid']!='') ) //if(isset(Yii::$app->session['stockroom_id'])&&(Yii::$app->session['stockroom_id']!='') )
	     	   $this->stockroom_id = $_GET['strid'];  //Yii::$app->session['stockroom_id']; 
	     	   //return $this->redirect(array('/stockrooms/stockroomrestock/index?strid='.$this->stockroom_id.'&from='));
	       }


        $searchModel = new SrcStockroomRestock();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'stockroomID' => $this->stockroom_id,
        ]);
        
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
    }

    /**
     * Displays a single StockroomRestock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StockroomRestock model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
       $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
       
       if(  (Yii::$app->user->can('stockrooms-stockroomrestock-create')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {
              $model = new SrcStockroomRestock();

             if ($model->load(Yii::$app->request->post()) ) 
	        {
                $dbTrans = Yii::$app->db->beginTransaction(); 
                
                 if(isset($_POST['SrcStockroomRestock']['stockroom_product_id']))
				  {  
				  	   $model->stockroom_product_id = $_POST['SrcStockroomRestock']['stockroom_product_id'];
				     
				      $modelStockProducts = SrcStockroomHasProducts::findOne($model->stockroom_product_id);
				      
				      //$model->opened_quantity = $modelStockProducts->quantity;
				      
				      
				  }
	           
	           if(isset($_POST['create']))
	            {
	            	      if( (isset($_GET['from']))&&($_GET['from']=='prodin') )
					          {
					          	    if(isset($_GET['product_id'])&&($_GET['product_id']!=''))
				                          $product_id = preg_replace("/[^0-9]/","",$_GET['product_id']);
				                          
				                     $model->stockroom_product_id = $product_id;
				                    
					          	}
	            	
	                      $model->created_date=date('Y-m-d');
                          $model->created_by=Yii::$app->user->identity->id;
	                   
	                   if ($model->save()) 
				          {  
				          	 //update qt ki nan depo a
				          	 
				          	 
				          	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id);  
				          	   
				          	  $model->quantity_before = $modelStockroomHasProduct->quantity;
				          	  $model->quantity_after = $modelStockroomHasProduct->quantity + $model->quantity_restock; 
				          	  
				          	  $modelStockroomHasProduct->quantity = $modelStockroomHasProduct->quantity + $model->quantity_restock;
				          	   
				          	  
				          	if ($model->save()) 
				             {
				          	    if ($modelStockroomHasProduct->save())
                                    {
                                    	    $dbTrans->commit(); 
                                    	    if( (isset($_GET['from']))&&($_GET['from']=='prodin') )
                                    	       return $this->redirect(['/stockrooms/stockroomhasproducts/index?wh=lis&strid='.$_GET['strid'].'&']);
                                    	    else
                                    	        return $this->redirect(['/stockrooms/stockroomrestock/index?wh=rest&strid='.$_GET['strid'].'&']);
                                    	}
                                    else
				          	          {    $dbTrans->rollback();
				                        }
				               }
                              else
				          	    {    $dbTrans->rollback();
				                 }
	                                   
				                	
				                
				            }
				         else
				            {    $dbTrans->rollback();
				              }
				              
				      
	             }
	             
	         

						                
						                
               }
               
               
            return $this->render('create', [
                'model' => $model,
            ]);
            
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
     
    
               
        
    }

    /**
     * Updates an existing StockroomRestock model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $strid = 0;
        $old_qt = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
           
           
        
        if(  (Yii::$app->user->can('stockrooms-stockroomrestock-update')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

        
        $model = $this->findModel($id);

         $old_qt = $model->quantity_restock;
         
        if ($model->load(Yii::$app->request->post()) ) 
	        {
                $dbTrans = Yii::$app->db->beginTransaction(); 
                
                 if(isset($_POST['SrcStockroomRestock']['stockroom_product_id']))
				  {  
				  	   $model->stockroom_product_id = $_POST['SrcStockroomRestock']['stockroom_product_id'];
				     
				      
				  }
	           
	           if(isset($_POST['update']))
	            {
	               
	                
	                      $model->updated_date=date('Y-m-d');
                          $model->updated_by=Yii::$app->user->identity->id;
	                   
	                   if ($model->save()) 
				          {  
				          	 //update qt ki nan depo a
				          	 $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model->stockroom_product_id); 
				          	 
				          	   $model->quantity_before = $modelStockroomHasProduct->quantity - $old_qt;
				          	   $model->quantity_after = $modelStockroomHasProduct->quantity - $old_qt + $model->quantity_restock; 
				          	   
				          	   $modelStockroomHasProduct->quantity = $modelStockroomHasProduct->quantity - $old_qt + $model->quantity_restock;
				          	   
				          	   
				          	if ($model->save()) 
				             {  
				          	    if ($modelStockroomHasProduct->save())
                                    {
                                    	    $dbTrans->commit(); 
                                    	    return $this->redirect(['/stockrooms/stockroomrestock/index?wh=rest&strid='.$_GET['strid'].'&']);
                                    	}
                                    else
				          	          {    $dbTrans->rollback();
				                        }
				          	   
	                           }
                              else
				          	      {    $dbTrans->rollback();
				                   }     
				                	
				                
				            }
				         else
				            {    $dbTrans->rollback();
				              }
				              
				         	               
	               
	             }
	             
	         

						                
						                
               } 
               

            return $this->render('update', [
                'model' => $model,
            ]);
        
        }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            
 
        
        
    }

    /**
     * Deletes an existing StockroomRestock model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $strid = 0;
       
       if(isset($_GET['strid'])&&($_GET['strid']!=''))
           $strid = preg_replace("/[^0-9]/","",$_GET['strid']);
           
           
      
      if( (Yii::$app->user->can('stockrooms-stockroomrestock-delete')) && (Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
         {

      
        try
          {
          	    $dbTrans = Yii::$app->db->beginTransaction(); 
          	    
          	   $model2delete = $this->findModel($id);
          	   
          	   $modelStockroomHasProduct = SrcStockroomHasProducts::findOne($model2delete->stockroom_product_id); 
          	   
          	   $new_qt = $modelStockroomHasProduct->quantity - $model2delete->quantity_restock;
          	   
          	   if( $new_qt >= 0  )
          	   {
          	   	   $model2delete->delete();
          	   	   
          	   	     //update qt ki nan depo a
          	   	     $modelStockroomHasProduct->quantity = $new_qt;
          	   	     
          	   	     if ($modelStockroomHasProduct->save()) 
          	   	       {
          	   	       	    $dbTrans->commit(); 
                                    	    return $this->redirect(['/stockrooms/stockroomrestock/index?wh=rest&strid='.$_GET['strid'].'&']);
                        }
                      else
				          {   
				          	 $dbTrans->rollback();
				            
				            }

          	   }
          	  else
          	     {
          	     	Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Operation failed. Stock quantity cannot be < 0.') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
										
						$dbTrans->rollback();
          	       }
          	   
          	   
          	   				              
          
         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }


			}



 }
      else
        {
            if( !(Yii::$app->user->can('stockrooms-stockroom'.$strid))  )
        	   return $this->goHome();

            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }    
            

    }

    /**
     * Finds the StockroomRestock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockroomRestock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SrcStockroomRestock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
