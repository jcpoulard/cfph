<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "stockroom_equipment".
 *
 * @property integer $id
 * @property string $code
 * @property integer $product_label
 * @property integer $marque
 * @property string $model
 * @property string $serial
 * @property string $description
 * @property double $price
 *
 * @property Marque $marque0
 * @property StockroomProductsLabel $productLabel
 */
class StockroomEquipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'product_label', 'marque'], 'required'],
            [['product_label', 'marque'], 'integer'],
            [['price'], 'number'],
            [['code'], 'string', 'max' => 200],
            [['model', 'serial', 'description'], 'string', 'max' => 255],
            [['marque'], 'exist', 'skipOnError' => true, 'targetClass' => SrcMarque::className(), 'targetAttribute' => ['marque' => 'id']],
            [['product_label'], 'exist', 'skipOnError' => true, 'targetClass' => SrcStockroomProductsLabel::className(), 'targetAttribute' => ['product_label' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'product_label' => Yii::t('app', 'Product Label'),
            'marque' => Yii::t('app', 'Marque'),
            'model' => Yii::t('app', 'Model'),
            'serial' => Yii::t('app', 'Serial'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarque0()
    {
        return $this->hasOne(SrcMarque::className(), ['id' => 'marque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLabel()
    {
        return $this->hasOne(SrcStockroomProductsLabel::className(), ['id' => 'product_label']);
    }
}
