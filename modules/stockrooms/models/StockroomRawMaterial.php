<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "stockroom_raw_material".
 *
 * @property integer $id
 * @property string $code
 * @property integer $product_label
 * @property string $nature
 * @property double $longueur
 * @property double $surface
 * @property double $volume
 * @property string $note
 * @property double $price
 *
 * @property StockroomProductsLabel $productLabel
 */
class StockroomRawMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_raw_material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'product_label'], 'required'],
            [['product_label'], 'integer'],
            [['longueur', 'surface', 'volume', 'price'], 'number'],
            [['note'], 'string'],
            [['code'], 'string', 'max' => 255],
            [['nature'], 'string', 'max' => 100],
            [['product_label'], 'exist', 'skipOnError' => true, 'targetClass' => StockroomProductsLabel::className(), 'targetAttribute' => ['product_label' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'product_label' => Yii::t('app', 'Product Label'),
            'nature' => Yii::t('app', 'Nature'),
            'longueur' => Yii::t('app', 'Longueur'),
            'surface' => Yii::t('app', 'Surface'),
            'volume' => Yii::t('app', 'Volume'),
            'note' => Yii::t('app', 'Note'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLabel()
    {
        return $this->hasOne(StockroomProductsLabel::className(), ['id' => 'product_label']);
    }
}
