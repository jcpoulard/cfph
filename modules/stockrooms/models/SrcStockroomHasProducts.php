<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\SrcStockroom;
use app\modules\stockrooms\models\SrcStockroomHasProducts;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomFurniture;
use app\modules\stockrooms\models\SrcStockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomStockUsage;

/**
 * SrcStockroomHasProducts represents the model behind the search form about `app\modules\stockrooms\models\StockroomHasProducts`.
 */
class SrcStockroomHasProducts extends StockroomHasProducts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'stockroom_id', 'stockroom_product', 'quantity', 'quantity_alert','state', 'status',  'is_equipment', 'things_out'], 'integer'],
            [['localisation'], 'safe'],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroomHasProducts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stockroom_id' => $this->stockroom_id,
            'stockroom_product' => $this->stockroom_product,
            'quantity' => $this->quantity,
            'quantity_alert' => $this->quantity_alert,
            'state' => $this->state,
            'status' => $this->status,
            'is_equipment' => $this->is_equipment,
            'things_out' => $this->things_out,
        ]);

        $query->andFilterWhere(['like', 'localisation', $this->localisation]);

        return $dataProvider;
    }
    
    
    public function searchByStockroomID($stockroom_id)
    {
        $query = SrcStockroomHasProducts::find()->where('stockroom_id='.$stockroom_id)->orderBy(['is_equipment'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        
       
        // grid filtering conditions
        $query->andFilterWhere([
            'stockroom_id' => $this->stockroom_id,
            'stockroom_product' => $this->stockroom_product,
            'quantity' => $this->quantity,
            'quantity_alert' => $this->quantity_alert,
            'state' => $this->state,
            'status' => $this->status,
            'is_equipment' => $this->is_equipment,
            'things_out' => $this->things_out,
        ]);

        $query->andFilterWhere(['like', 'localisation', $this->localisation]);

        return $dataProvider;
    }
    
    
    public function getStockroomInfo($stockroom_id)
    {
    	 $query = SrcStockroom::findOne($stockroom_id);
    	 return $query;
    }
 
 public function getState()
   {
   	   if($this->state==NULL)
	      return Yii::t('app','N/A');
	   elseif($this->state==0)
    	  return Yii::t('app','Bad');
        elseif($this->state==1)
           return Yii::t('app','Good');
            elseif($this->state==2)
               return Yii::t('app','Damaged');
             
   	}
   	
  public function getStatus()
   {
   	     if($this->status==NULL)
	        return Yii::t('app','N/A');
	    elseif($this->status==0)
    	  return Yii::t('app','Removed from stock');
        elseif($this->status==1)
           return Yii::t('app','In use');
            elseif($this->status==2)
               return Yii::t('app','In repair');
	               elseif($this->status==3)
	                 return Yii::t('app','Lost');
	                 
   	}
   	
   	
 //bay kantite ki deyo
  public function getThingsOut()
    {
    	 $result= 0;     	 
    	 if($this->things_out==0)
    	    { 
    	    	if( ($this->is_equipment==0)||($this->is_equipment==2) )
    	    	  return Yii::t('app','N/A');
    	    	else
    	    	return Yii::t('app','No'); //0; // 
    	    
    	    
    	    }
    	elseif($this->things_out==1)
    	   {
    	   	 /*  $result= 0;
    	   	   
    	   	   //jwenn ki kantite ki deyo pou retounen l
    	   	    $data = SrcStockroomStockUsage::find()->where('stockroom_product_id ='.$this->id.' AND is_return =0 ')->all();
   	    
		   	    if($data!=null)
		   	      {
		   	      	foreach($data as $r)
		   	      	  {
		   	      	  	   if(isset($r->stockroom_product_id) )
		   	      	  	     {  
		   	      	  	     	$result= $result + $r->quantity;
		   	      	  	       
		   	      	  	     }
		   	      	  	}
		   	      	}
                */
    	   	  return Yii::t('app','Yes'); //$result; // 
    	   	  
    	   }
    	     
    }
  
  public function getEquipmentName($stockroom_product)
  {
  	        $query = SrcStockroomEquipment::find()->joinWith(['productLabel'])->where('stockroom_equipment.id='.$stockroom_product)->all();
  	        
  	        foreach($query as $q)
  	          {
  	          	   return $q->productLabel->product_description.' ('.$q->code.')';
  	          	}
  	}
 
  public function getFurnitureName($stockroom_product)
  {
  	        $query = SrcStockroomFurniture::find()->joinWith(['productLabel'])->where('stockroom_furniture.id='.$stockroom_product)->all();
  	        
  	        foreach($query as $q)
  	          {
  	          	   return $q->productLabel->product_description.' ('.$q->code.')';
  	          	}
  	}
   
  public function getRawMaterialName($stockroom_product)
  {
  	       $query = SrcStockroomRawMaterial::find()->joinWith(['productLabel'])->where('stockroom_raw_material.id='.$stockroom_product)->all();
  	       
  	        foreach($query as $q)
  	          {
  	          	   return $q->productLabel->product_description.' ('.$q->code.')';
  	          	}
  	}
    
    
}
