<?php

namespace app\modules\stockrooms\models;

use Yii;

use app\modules\stockrooms\models\SrcStockroomHasProducts;

use app\modules\rbac\models\User;

/**
 * This is the model class for table "stockroom_restock".
 *
 * @property integer $id
 * @property integer $stockroom_product_id
 * @property integer $quantity_before 
 * @property integer $quantity_restock
 * @property integer $quantity_after
 * @property string $comment
 * @property string $restock_date
 * @property string $created_date
 * @property string $updated_date
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $updatedBy
 * @property User $createdBy
 * @property StockroomHasProducts $stockroomProduct
 */
class StockroomRestock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_restock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stockroom_product_id', 'quantity_restock', 'restock_date'], 'required'],
            [['stockroom_product_id', 'quantity_before', 'quantity_restock', 'quantity_after', 'created_by', 'updated_by'], 'integer'],
            [['restock_date', 'created_date', 'updated_date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['stockroom_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcStockroomHasProducts::className(), 'targetAttribute' => ['stockroom_product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stockroom_product_id' => Yii::t('app', 'Stockroom Product ID'),
            'quantity_before' => Yii::t('app', 'Quantity Before'), 
		    'quantity_restock' => Yii::t('app', 'Quantity Restock'),
		    'quantity_after' => Yii::t('app', 'Quantity After'), 
		    'comment' => Yii::t('app', 'Comment'),
            'restock_date' => Yii::t('app', 'Restock Date'),
            'created_date' => Yii::t('app', 'Created Date'),
            'updated_date' => Yii::t('app', 'Updated Date'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomProduct()
    {
        return $this->hasOne(SrcStockroomHasProducts::className(), ['id' => 'stockroom_product_id']);
    }
}
