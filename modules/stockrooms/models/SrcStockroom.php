<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\Stockroom;
use app\modules\stockrooms\models\SrcStockroom;

/**
 * SrcStockroom represents the model behind the search form about `app\modules\stockrooms\models\Stockroom`.
 */
class SrcStockroom extends Stockroom
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'stockroom_local'], 'integer'],
            [['stockroom_code', 'stockroom_name'], 'safe'],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroom::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stockroom_local' => $this->stockroom_local,
        ]);

        $query->andFilterWhere(['like', 'stockroom_code', $this->stockroom_code])
            ->andFilterWhere(['like', 'stockroom_name', $this->stockroom_name]);

        return $dataProvider;
    }
}
