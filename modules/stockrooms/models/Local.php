<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "local".
 *
 * @property integer $id
 * @property string $local_label
 *
 * @property Stockroom[] $stockrooms
 */
class Local extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'local';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['local_label'], 'required'],
            [['local_label'], 'unique'],
            [['local_label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'local_label' => Yii::t('app', 'Local Label'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockrooms()
    {
        return $this->hasMany(Stockroom::className(), ['stockroom_local' => 'id']);
    }
}
