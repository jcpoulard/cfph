<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "stockroom_products_label".
 *
 * @property integer $id
 * @property string $product_description
 * @property integer $category
 *
 * @property StockroomEquipment[] $stockroomEquipments
 * @property StockroomFurniture[] $stockroomFurnitures
 * @property StockroomRawMaterial[] $stockroomRawMaterials
 */
class StockroomProductsLabel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_products_label';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_description'], 'required'],
            [['category', ], 'integer'],
            [['product_description'], 'string', 'max' => 255],
            [['product_description'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_description' => Yii::t('app', 'Product Description'),
            'category' => Yii::t('app', 'Category'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomEquipments()
    {
        return $this->hasMany(SrcStockroomEquipment::className(), ['product_label' => 'id']);
    }
    
     /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStockroomFurnitures()
		   {
		       return $this->hasMany(SrcStockroomFurniture::className(), ['product_label' => 'id']);
		   }
		

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomRawMaterials()
    {
        return $this->hasMany(SrcStockroomRawMaterial::className(), ['product_label' => 'id']);
    }
    
    
    
}
