<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomFurniture;

/**
 * SrcStockroomFurniture represents the model behind the search form about `app\modules\stockrooms\models\StockroomFurniture`.
 */
class SrcStockroomFurniture extends StockroomFurniture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'product_label', 'marque'], 'integer'],
            [['code', 'model', 'serial', 'description'], 'safe'],
            [['price'], 'number'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StockroomFurniture::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_label' => $this->product_label,
            'marque' => $this->marque,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'serial', $this->serial])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
    
        public function loadFurnitureLabel()
       {
       	    $code= array();
       	    
       	    $data = SrcStockroomFurniture::find()->joinWith(['productLabel'])->all();
       	    
       	    foreach($data as $r)
       	      {
       	      	$code[$r->id]= $r->productLabel->product_description.' ('.$r->code.')';
       	      	}
       	    return $code;
       	}
    
 
   public function getFurnitureLabel($stockroom_product) 
    {
       	    $code= '';
       	    
       	    $data = SrcStockroomFurniture::findOne($stockroom_product);
       	    
       	    if($data!=null)
       	     $code = $data->productLabel->product_description.' ('.$data->code.')';
       	     
       	    return $code;
       	} 
    
   
}
