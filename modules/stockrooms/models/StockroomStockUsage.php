<?php

namespace app\modules\stockrooms\models;

use Yii;

use app\modules\stockrooms\models\SrcStockroomHasProducts;

use app\modules\fi\models\SrcPersons;




/**
 * This is the model class for table "stockroom_stock_usage".
 *
 * @property integer $id
 * @property integer $stockroom_product_id
 * @property integer $quantity
 * @property integer $product_user
 * @property integer $stockkeeper
 * @property integer $is_return
 * @property integer $quantity_recieved
 * @property string $recieved_by
 * @property string $taken_date
 * @property string $return_date
 *
 * @property Persons $stockkeeper0
 * @property Persons $productUser
 * @property StockroomHasProducts $stockroomProduct
 */
class StockroomStockUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_stock_usage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['stockroom_product_id', 'quantity', 'product_user', 'stockkeeper'], 'required'],
            [['stockroom_product_id', 'quantity', 'product_user', 'stockkeeper', 'is_return', 'quantity_recieved'], 'integer'],
            [['taken_date', 'return_date'], 'safe'],
            [['recieved_by','comment'], 'string', 'max' => 255],
            [['stockkeeper'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['stockkeeper' => 'id']],
            [['product_user'], 'exist', 'skipOnError' => true, 'targetClass' => SrcPersons::className(), 'targetAttribute' => ['product_user' => 'id']],
            [['stockroom_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcStockroomHasProducts::className(), 'targetAttribute' => ['stockroom_product_id' => 'id']],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stockroom_product_id' => Yii::t('app', 'Stockroom Product ID'),
            'quantity' => Yii::t('app', 'Quantity Out'),
            'product_user' => Yii::t('app', 'Product User'),
            'stockkeeper' => Yii::t('app', 'Stockkeeper'),
            'is_return' => Yii::t('app', 'Is Return'),
            'quantity_recieved' => Yii::t('app', 'Quantity Recieved'),
            'recieved_by' => Yii::t('app', 'Recieved By'),
            'taken_date' => Yii::t('app', 'Taken Date'),
            'return_date' => Yii::t('app', 'Return Date'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockkeeper0()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'stockkeeper']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductUser()
    {
        return $this->hasOne(SrcPersons::className(), ['id' => 'product_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomProduct()
    {
        return $this->hasOne(SrcStockroomHasProducts::className(), ['id' => 'stockroom_product_id']);
    }
}
