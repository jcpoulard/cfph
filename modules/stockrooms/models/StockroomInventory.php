<?php

namespace app\modules\stockrooms\models;

use Yii;


use app\modules\stockrooms\models\SrcStockroomHasProducts;

use app\modules\rbac\models\User;

/**
 * This is the model class for table "stockroom_inventory".
 *
 * @property integer $id
 * @property integer $stockroom_product_id
 * @property integer $quantity_lost
 * @property integer $quantity_good
 * @property integer $quantity_bad
 * @property integer $opened_quantity
 * @property integer $closed_quantity
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $inventory_date
 * @property string $date_created
 * @property string $date_updated
 * @property integer $is_bad_quantity_removed
 * @property string $comment
 *
 * @property User $updatedBy 
 * @property StockroomHasProducts $stockroomProduct
 * @property User $createdBy 
 */
class StockroomInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stockroom_product_id', 'quantity_lost', 'quantity_good', 'quantity_bad', 'opened_quantity', 'closed_quantity', 'inventory_date', 'quantity_bad_removed'], 'required'],
            [['stockroom_product_id', 'quantity_lost', 'quantity_good', 'quantity_bad', 'opened_quantity', 'closed_quantity','created_by', 'updated_by', 'quantity_bad_removed'], 'integer'],
            [['inventory_date', 'date_created', 'date_updated'], 'safe'],
            [['comment'], 'string', 'max' => 255],
            [['stockroom_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcStockroomHasProducts::className(), 'targetAttribute' => ['stockroom_product_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stockroom_product_id' => Yii::t('app', 'Stockroom Product ID'),
            'quantity_lost' => Yii::t('app', 'Quantity Lost'),
            'quantity_good' => Yii::t('app', 'Quantity Good'),
            'quantity_bad' => Yii::t('app', 'Quantity Bad'),
            'opened_quantity' => Yii::t('app', 'Opened Quantity'),
            'closed_quantity' => Yii::t('app', 'Closed Quantity'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'inventory_date' => Yii::t('app', 'Inventory Date'),
            'date_created' => Yii::t('app', 'Date Created'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'quantity_bad_removed' => Yii::t('app', 'Quantity Bad Removed'),
            'comment' => Yii::t('app', 'Comment'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomProduct()
    {
        return $this->hasOne(SrcStockroomHasProducts::className(), ['id' => 'stockroom_product_id']);
    }
    
    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getUpdatedBy() 
		   { 
		       return $this->hasOne(User::className(), ['id' => 'updated_by']); 
		   } 
		   
		    /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getCreatedBy()
		   {
		       return $this->hasOne(User::className(), ['id' => 'created_by']);
		   }
}
