<?php

namespace app\modules\stockrooms\models;

use Yii;
use app\modules\stockrooms\models\SrcStockroom;
use app\modules\stockrooms\models\SrcStockroomInventory;
use app\modules\stockrooms\models\SrcStockroomStockUsage;
use app\modules\stockrooms\models\SrcStockroomRestock;

/**
 * This is the model class for table "stockroom_has_products".
 *
 * @property integer $id
 * @property integer $stockroom_id
 * @property integer $stockroom_product
 * @property integer $quantity
 * @property integer $quantity_alert
 * @property integer $state
 * @property integer $status
 * @property integer $is_equipment
 * @property integer $things_out
 * @property string $localisation
 *
 * @property Stockroom $stockroom
 * @property StockroomInventory[] $stockroomInventories
 * @property StockroomRestock[] $stockroomRestocks
 * @property StockroomStockUsage[] $stockroomStockUsages
 */
class StockroomHasProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom_has_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stockroom_id', 'stockroom_product', 'quantity',  'is_equipment', 'localisation'], 'required'],
            [['stockroom_id', 'stockroom_product', 'quantity','state', 'status', 'quantity_alert', 'is_equipment', 'things_out'], 'integer'],
            [['state', 'localisation'], 'string', 'max' => 255],
            [['stockroom_id', 'stockroom_product', 'is_equipment'], 'unique', 'targetAttribute' => ['stockroom_id', 'stockroom_product', 'is_equipment'], 'message' => Yii::t('app', 'The combination of Stockroom ID, Stockroom Product and Is Equipment has already been taken.') ],
            [['stockroom_id'], 'exist', 'skipOnError' => true, 'targetClass' => SrcStockroom::className(), 'targetAttribute' => ['stockroom_id' => 'id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'), 
            'stockroom_id' => Yii::t('app', 'Stockroom ID'),
            'stockroom_product' => Yii::t('app', 'Stockroom Product'),
            'quantity' => Yii::t('app', 'Quantity'),
            'quantity_alert' => Yii::t('app', 'Quantity Alert'),
            'state' => Yii::t('app', 'State'),
            'status' => Yii::t('app', 'Status'),
            'is_equipment' => Yii::t('app', 'Is Equipment'),
            'things_out' => Yii::t('app', 'Things Out'),
            'localisation' => Yii::t('app', 'Localisation'),
        ];
    }
    
    
     /** 
		    * @return \yii\db\ActiveQuery 
		    */ 
		   public function getStockroom() 
		   { 
		       return $this->hasOne(SrcStockroom::className(), ['id' => 'stockroom_id']); 
		   } 
		   
		
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStockroomInventories()
		   {
		       return $this->hasMany(SrcStockroomInventory::className(), ['stockroom_product_id' => 'id']);
		   }
		   
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStockroomRestocks()
		   {
		       return $this->hasMany(SrcStockroomRestock::className(), ['stockroom_product_id' => 'id']);
		   }
	
		   /**
		    * @return \yii\db\ActiveQuery
		    */
		   public function getStockroomStockUsages()
		   {
		       return $this->hasMany(SrcStockroomStockUsage::className(), ['stockroom_product_id' => 'id']);
		   }
    
}
