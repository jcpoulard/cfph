<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomStockUsage;
use app\modules\stockrooms\models\SrcStockroomStockUsage;

/**
 * SrcStockroomStockUsage represents the model behind the search form about `app\modules\stockrooms\models\StockroomStockUsage`.
 */
class SrcStockroomStockUsage extends StockroomStockUsage
{
    public $stockroom_id;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'stockroom_id', 'stockroom_product_id', 'quantity', 'product_user', 'stockkeeper', 'is_return', 'quantity_recieved'], 'integer'],
            [['recieved_by', 'taken_date', 'return_date'], 'safe'],
        ] );
    }
    
    
    
    public function attributeLabels()
    {
        return array_merge(
		    	parent::attributeLabels(), [
            'stockroom_id' => Yii::t('app', 'Stockroom ID'),
            
        ] );
    }



    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroomStockUsage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stockroom_product_id' => $this->stockroom_product_id,
            'quantity' => $this->quantity,
            'product_user' => $this->product_user,
            'stockkeeper' => $this->stockkeeper,
            'is_return' => $this->is_return,
            'quantity_recieved' => $this->quantity_recieved,
            'taken_date' => $this->taken_date,
            'return_date' => $this->return_date,
        ]);

        $query->andFilterWhere(['like', 'recieved_by', $this->recieved_by]);
        

        return $dataProvider;
    }
    
    
    
  public function getIsReturn()
    {
        switch($this->is_return)
          {
          	case 0: return Yii::t('app', 'No');
          	
          	          break;
          	case 1: return Yii::t('app', 'Yes');
          	
          	          break;
          	          	
          }
        
        
       
    }
    
  public function getStockroomProductLabel()
    {
        //$this->stockroom_product_id
        $label='';
        $data = SrcStockroomStockUsage::findOne($this->id);
         
         
	              if($data->stockroomProduct->is_equipment==1)
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $label =  $stockroomEquipmentModel->getEquipmentLabel($data->stockroomProduct->stockroom_product).' '.Yii::t('app', '(EQ)');
	               }
	              elseif($data->stockroomProduct->is_equipment==0)
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($data->stockroomProduct->stockroom_product).' '.Yii::t('app', '(RM)');

	                  }
	      	               	 
	       
       	    return $label;
        
    }
   
   


    
  
 public function isEverythingReturned($usage_id,$stockroom_product_id)
   {
   	    $result=1;
   	    
   	    $data = SrcStockroomStockUsage::find()->where('stockroom_product_id ='.$stockroom_product_id.' AND is_return =0 AND id NOT IN('.$usage_id.') ')->all();
   	    
   	    if($data!=null)
   	      {
   	      	foreach($data as $r)
   	      	  {
   	      	  	   if(isset($r->stockroom_product_id) )
   	      	  	     {  $result=0;
   	      	  	        break;
   	      	  	     }
   	      	  	}
   	      	}
   	    
   	    return $result;
   	}
 

 public function findNextRecords($stockroom_product_id,$taken_date) 
   {
   	   
   	    
   	    $data = SrcStockroomStockUsage::find()->where('stockroom_product_id ='.$stockroom_product_id.' AND taken_date >\''.$taken_date.'\' ')->all();
   	    
   	    return $data;
   	      	  
  }
 
    
    
    
    
    
    
}
