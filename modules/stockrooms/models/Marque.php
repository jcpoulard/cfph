<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "marque".
 *
 * @property integer $id
 * @property string $marque_label
 *
 * @property StockroomEquipment[] $stockroomEquipments
 */
class Marque extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marque';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['marque_label'], 'required'],
            [['marque_label'], 'unique'],
            [['marque_label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'marque_label' => Yii::t('app', 'Marque Label'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomEquipments()
    {
        return $this->hasMany(StockroomEquipment::className(), ['marque' => 'id']);
    }
}
