<?php

namespace app\modules\stockrooms\models;

use Yii;

/**
 * This is the model class for table "stockroom".
 *
 * @property integer $id
 * @property string $stockroom_code
 * @property string $stockroom_name
 * @property integer $stockroom_local
 *
 * @property Local $stockroomLocal
 */
class Stockroom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stockroom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stockroom_code', 'stockroom_name', 'stockroom_local'], 'required'],
            [['stockroom_local'], 'integer'],
            [['stockroom_code'], 'string', 'max' => 100],
            [['stockroom_name'], 'string', 'max' => 255],
            [['stockroom_local'], 'exist', 'skipOnError' => true, 'targetClass' => Local::className(), 'targetAttribute' => ['stockroom_local' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'stockroom_code' => Yii::t('app', 'Stockroom Code'),
            'stockroom_name' => Yii::t('app', 'Stockroom Name'),
            'stockroom_local' => Yii::t('app', 'Stockroom Local'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockroomLocal()
    {
        return $this->hasOne(Local::className(), ['id' => 'stockroom_local']);
    }
}
