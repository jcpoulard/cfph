<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomInventory;

/**
 * SrcStockroomInventory represents the model behind the search form about `app\modules\stockrooms\models\StockroomInventory`.
 */
class SrcStockroomInventory extends StockroomInventory
{
    public $stockroom_id;
    public $available_quantity;
    public $state;
    public $status;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'stockroom_id', 'stockroom_product_id', 'available_quantity', 'quantity_lost', 'quantity_good', 'quantity_bad', 'opened_quantity', 'closed_quantity', 'created_by', 'updated_by','quantity_bad_removed'], 'integer'],
            [['created_by', 'updated_by', 'inventory_date', 'date_created', 'date_updated', 'comment'], 'safe'],
        ] );
    }


  
    public function attributeLabels()
    {
        return array_merge(
		    	parent::attributeLabels(), [
            'stockroom_id' => Yii::t('app', 'Stockroom ID'),
            'state'=> Yii::t('app', 'State'),
            'status'=> Yii::t('app', 'Status'),
            
        ] );
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroomInventory::find()->orderBy(['inventory_date'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stockroom_product_id' => $this->stockroom_product_id,
            'quantity_lost' => $this->quantity_lost,
            'quantity_good' => $this->quantity_good,
            'quantity_bad' => $this->quantity_bad,
            'opened_quantity' => $this->opened_quantity,
            'closed_quantity' => $this->closed_quantity,
            'created_by' => $this->created_by, 
		    'updated_by' => $this->updated_by, 
		    'inventory_date' => $this->inventory_date,
            'date_created' => $this->date_created,
            'date_updated' => $this->date_updated,
            'quantity_bad_removed' => $this->quantity_bad_removed,
        ]);

        $query->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'updated_by', $this->updated_by])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
    
    
    
 public function getState()
       {
       	   switch($this->state)  
	        {   
	          case 0:  return Yii::t('app', 'Bad');
	                 break;
	          case 1:  return Yii::t('app', 'Good');
	                 break;
	          case 2:  return Yii::t('app', 'Damaged');
	                	break;
	        }   
     
       }
 
 public function getStatus()
       {
       	   switch($this->status)  
	        {   
	          case 0:  return Yii::t('app', 'Removed from stock');
	                 break;
	          case 1:  return Yii::t('app', 'In use');
	                 break;
	          case 2:  return Yii::t('app', 'In repair');
	                	break;
	          case 3:  return Yii::t('app', 'Lost');
	                	break;
	        }   
     
       }	

    
    
    
}
