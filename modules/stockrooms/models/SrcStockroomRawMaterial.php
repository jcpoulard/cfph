<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomRawMaterial;
use app\modules\stockrooms\models\SrcStockroomRawMaterial;

/**
 * SrcStockroomRawMaterial represents the model behind the search form about `app\modules\stockrooms\models\StockroomRawMaterial`.
 */
class SrcStockroomRawMaterial extends StockroomRawMaterial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'product_label'], 'integer'],
            [['code', 'note','nature'], 'safe'],
            [[ 'longueur', 'surface', 'volume', 'price'], 'number'],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroomRawMaterial::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_label' => $this->product_label,
            'longueur' => $this->longueur,
            'surface' => $this->surface,
            'volume' => $this->volume,
            'price' => $this->price,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'nature', $this->nature])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
    
    public function loadRawMaterialLabel()
       {
       	    $code= array();
       	    
       	    $data = SrcStockroomRawMaterial::find()->joinWith(['productLabel'])->all();
       	    
       	    foreach($data as $r)
       	      {
       	      	$code[$r->id]= $r->productLabel->product_description.' ('.$r->code.')';
       	      	}
       	    return $code;
       	}
    
 
   public function getRawMaterialLabel($stockroom_product) 
    {
       	    $code= '';
       	    
       	    $data = SrcStockroomRawMaterial::findOne($stockroom_product);
       	    
       	    if($data!=null)
       	     $code = $data->productLabel->product_description.' ('.$data->code.')';
       	     
       	    return $code;
       	} 
    
    
    
    
    
}
