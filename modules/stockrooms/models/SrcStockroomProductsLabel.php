<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomProductsLabel;
use app\modules\stockrooms\models\SrcStockroomProductsLabel;

/**
 * SrcStockroomProductsLabel represents the model behind the search form about `app\modules\stockrooms\models\StockroomProductsLabel`.
 */
class SrcStockroomProductsLabel extends StockroomProductsLabel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'category'], 'integer'],
            [['product_description'], 'unique'],
            [['product_description'], 'safe'],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcStockroomProductsLabel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
        ]);

        $query->andFilterWhere(['like', 'product_description', $this->product_description]);

        return $dataProvider;
    }
    
    
    
 public function getCategory()
   {
   	   if(($this->category==NULL)&&($this->category!=0) )
	      return Yii::t('app','N/A');
	   elseif($this->category==0)
    	  return Yii::t('app','Raw Material');
        elseif($this->category==1)
           return Yii::t('app','Equipment');
            elseif($this->category==2)
               return Yii::t('app','Furniture');
               
             
   	}






    
    
    
    
    
}


