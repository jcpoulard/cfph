<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\StockroomRestock;

/**
 * SrcStockroomRestock represents the model behind the search form about `app\modules\stockrooms\models\StockroomRestock`.
 */
class SrcStockroomRestock extends StockroomRestock
{
    public $stockroom_id;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(),[
            [['id', 'stockroom_id', 'stockroom_product_id', 'quantity_before', 'quantity_restock', 'quantity_after', 'created_by', 'updated_by'], 'integer'],
            [['comment', 'restock_date', 'created_date', 'updated_date'], 'safe'],
        ]);
    }
    
     public function attributeLabels()
    {
        return array_merge(
		    	parent::attributeLabels(), [
            'stockroom_id' => Yii::t('app', 'Stockroom ID'),
            
        ] );
    }



    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StockroomRestock::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'stockroom_product_id' => $this->stockroom_product_id,
            'quantity_before' => $this->quantity_before,
            'quantity_restock' => $this->quantity_restock,
            'quantity_after' => $this->quantity_after,
            'restock_date' => $this->restock_date,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
