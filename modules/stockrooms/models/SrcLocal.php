<?php

namespace app\modules\stockrooms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\stockrooms\models\Local;
use app\modules\stockrooms\models\SrcLocal;

/**
 * SrcLocal represents the model behind the search form about `app\modules\stockrooms\models\Local`.
 */
class SrcLocal extends Local
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
		    	parent::rules(), [
            [['id'], 'integer'],
            [['local_label'], 'unique'],
            [['local_label'], 'safe'],
        ] );
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SrcLocal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'local_label', $this->local_label]);

        return $dataProvider;
    }
}
