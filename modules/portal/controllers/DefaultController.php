<?php

namespace app\modules\portal\controllers;


use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\portal\models\CmsBankImg; 
use app\modules\portal\models\CmsProgramExtra; 
use app\modules\portal\models\CmsCarrousel; 
use app\modules\portal\models\CmsArticle;
use app\modules\portal\models\CmsAlbumCat; 
use app\modules\portal\models\CmsImageAlbum; 
use app\modules\portal\models\CmsEvents;
use app\modules\portal\models\CmsMenu;
use app\modules\portal\models\CmsConfig;

/**
 * Default controller for the `portal` module
 */
class DefaultController extends Controller
{
    
     public $layout = "/inspinia";
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    public function actionUpload(){
        $fileName = 'file';
        $uploadPath = 'web_uploads';
        
        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);

            //Print file data
           // print_r($file);

            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database
                    $model = new CmsBankImg(); 
                    $model->image_name = $file->name;
                    $model->is_in_use = 0; 
                    $model->create_date = date('Y-m-d h:m:s');
                    $model->create_by = currentUser();
                    $model->save();
                    
                echo \yii\helpers\Json::encode($file);
            }
    }else{
        return $this->render('index'); 
    }

    return false;
    }
    
    
    public function actionViewImage(){
        $all_image = CmsBankImg::find()->all();
        return $this->renderAjax('view_image',['all_image'=>$all_image]); 
    }
    
    public function actionManageContent(){
        $model = new \app\modules\portal\models\CmsMenu();
        return $this->render('manage-content',['model'=>$model]);
    }
    
    public function actionKonfigirasyon(){
        $model = new \app\modules\portal\models\CmsMenu();
        return $this->render('konfigirasyon',['model'=>$model]);
    }
    
    public function actionCreateProgramExtra(){
        $program_extra = new \app\modules\portal\models\CmsProgramExtra(); 
        $program = new \app\modules\fi\models\Program(); 
        return $this->renderAjax('create-program-extra',['program'=>$program,'program_extra'=>$program_extra]); 
    }
    
    public function  actionUpdateProgramExtra($id){
        $program_extra = CmsProgramExtra::findOne($id);
        $program = new \app\modules\fi\models\Program();
        return $this->renderAjax('update-program-extra',['program'=>$program,'program_extra'=>$program_extra,'id'=>$id]);
    }
    
    public function actionListContent(){
        return $this->renderAjax('list-content',['test'=>$this->correctionUrlImage('http://localhost/cfph/web/web_uploads/images/478.jpg')]);
    }
    
    public function actionSaveProgramExtra($program_id,$image,$description_program,$pre_requis,$why,$is_publish){
        $program_extra = new \app\modules\portal\models\CmsProgramExtra(); 
        $program_extra->program_id = $program_id;
        $program_extra->image = $this->correctionUrlImage($image);
        $program_extra->description_programme = $description_program;
        $program_extra->pre_requis = $pre_requis;
        $program_extra->why = $why; 
        $program_extra->is_publish = $is_publish;
        $program_extra->create_by = currentUser();
        $program_extra->create_date = date('Y-m-d h:m:s');
        if($program_extra->save()){
            return 1;
        }else{
            return 0;
        }
    }
    
      public function actionModifyeProgramExtra($id,$program_id,$image,$description_program,$pre_requis,$why,$is_publish){
        $program_extra = CmsProgramExtra::findOne($id); 
        $program_extra->program_id = $program_id;
        $program_extra->image = $this->correctionUrlImage($image);
        $program_extra->description_programme = $description_program;
        $program_extra->pre_requis = $pre_requis;
        $program_extra->why = $why; 
        $program_extra->is_publish = $is_publish;
        $program_extra->update_by = currentUser();
        $program_extra->update_date = date('Y-m-d h:m:s');
        if($program_extra->save()){
            return 1;
        }else{
            return 0;
        }
    }
    
    public function actionSetPublishProgramExtra($id,$state){
         $program_extra = CmsProgramExtra::findOne($id); 
         $program_extra->is_publish = $state;
         $program_extra->update_by = currentUser();
         $program_extra->update_date = date('Y-m-d h:m:s');
         if($program_extra->save()){
            return $this->renderAjax('list-program-extra');
         }else{
            return 0;
         }
         
    }
    
    public function actionCreateCarrousel(){
        $carrousel = new CmsCarrousel();
        return $this->renderAjax('create-carrousel',['carrousel'=>$carrousel]); 
    }
    
    public function actionCreateArticle(){
        $article = new CmsArticle();
        return $this->renderAjax('create-article',['article'=>$article]); 
    }
    
    public function actionListCarrousel(){
         return $this->renderAjax('list-carrousel');
    }
    
    public function actionListGallerie(){
         return $this->renderAjax('list-gallerie');
    }
    
    public function actionCreateEvent(){
        $event = new CmsEvents();
        $all_menu = CmsMenu::findBySql("SELECT nom_menu AS 'name', code AS 'id' FROM cms_menu  WHERE is_publish = 1 AND can_have_article = 1 ORDER BY nom_menu")->asArray()->all();
        $json_menu = json_encode($all_menu,false); 
        $all_location = CmsEvents::findBySql("SELECT DISTINCT(location) AS 'name', location AS 'id' FROM cms_events   ORDER BY location")->asArray()->all();
        if(empty($all_location)){
            $all_location = [['name'=>'Auditorium','id'=>'Auditorium'],];
            $json_location = json_encode($all_location,false); 
        }else{
            $json_location = json_encode($all_location,false);
        }
        return $this->renderAjax('create-event',['event'=>$event,'json_menu'=>$json_menu,'json_location'=>$json_location]); 
    }
    
    public function actionUpdateEvent($id){
        $event = CmsEvents::findOne($id);
        $all_menu = CmsMenu::findBySql("SELECT nom_menu AS 'name', code AS 'id' FROM cms_menu  WHERE is_publish = 1 AND can_have_article = 1 ORDER BY nom_menu")->asArray()->all();
        $json_menu = json_encode($all_menu,false); 
        $all_location = CmsEvents::findBySql("SELECT DISTINCT(location) AS 'name', location AS 'id' FROM cms_events   ORDER BY location")->asArray()->all();
        if(empty($all_location)){
            $all_location = [['name'=>'Auditorium','id'=>'Auditorium'],];
            $json_location = json_encode($all_location,false); 
        }else{
            $json_location = json_encode($all_location,false);
        }
        return $this->renderAjax('update-event',['event'=>$event,'json_menu'=>$json_menu,'json_location'=>$json_location]); 
    }
    
    public function actionListEvent(){
         return $this->renderAjax('list-event');
    }
    
    public function actionSaveCarrousel($titre_image, $image,$short_description,$is_publish){
        $carrousel = new CmsCarrousel(); 
        $carrousel->titre_image = $titre_image;
        $carrousel->image = $this->correctionUrlImage($image);
        $carrousel->short_description = $short_description;
        $carrousel->is_publish = $is_publish;
        $carrousel->create_by = currentUser();
        $carrousel->create_date = date('Y-m-d h:m:s');
        if($carrousel->save()){
            return 1;
        }else{
            return 0;
        }
    }
    
   public function  actionUpdateCarrousel($id){
        $carrousel = CmsCarrousel::findOne($id);
        return $this->renderAjax('update-carrousel',['carrousel'=>$carrousel,'id'=>$id]);
    } 
    
    public function actionModifyeCarrousel($id, $titre_image, $image,$short_description,$is_publish){
        $carrousel = CmsCarrousel::findOne($id); 
        $carrousel->titre_image = $titre_image;
        $carrousel->image = $this->correctionUrlImage($image);
        $carrousel->short_description = $short_description;
        $carrousel->is_publish = $is_publish;
        $carrousel->update_by = currentUser();
        $carrousel->update_date = date('Y-m-d h:m:s');
        if($carrousel->save()){
            return 1;
        }else{
            return 0;
        }
    }
    
    public function actionSetPublishCarrousel($id,$state){
         $carrousel = CmsCarrousel::findOne($id); 
         $carrousel->is_publish = $state;
         $carrousel->update_by = currentUser();
         $carrousel->update_date = date('Y-m-d h:m:s');
         if($carrousel->save()){
            return $this->renderAjax('list-carrousel');
         }else{
            return 0;
         }
         
    }
    
    public function actionSetVisibleLabel($id,$state){
         $carrousel = CmsCarrousel::findOne($id); 
         $carrousel->label_visible = $state;
         $carrousel->update_by = currentUser();
         $carrousel->update_date = date('Y-m-d h:m:s');
         if($carrousel->save()){
            return $this->renderAjax('list-carrousel');
         }else{
            return 0;
         }
         
    }
    
    public function actionDeleteProgramExtra($id){
        $program_extra = CmsProgramExtra::findOne($id); 
        if($program_extra->delete()){
            return $this->renderAjax('list-program-extra');
         }else{
            return 0;
         }
    }
    
    public function actionDeleteCarrousel($id){
        $carrousel = CmsCarrousel::findOne($id); 
        if($carrousel->delete()){
            return $this->renderAjax('list-carrousel');
         }else{
            return 0;
         }
    }
    
    public function actionDeleteGallerie($id){
        $gallerie = CmsImageAlbum::findOne($id); 
        if($gallerie->delete()){
            return $this->renderAjax('list-gallerie');
         }else{
            return 0;
         }
    }
    
    public function actionDeleteEvent($id){
        $event = CmsEvents::findOne($id); 
        if($event->delete()){
            return $this->renderAjax('list-event');
         }else{
            return 0;
         }
    }
    
    public function actionDeleteArticle($id){
        $article = CmsArticle::findOne($id); 
        if($article->delete()){
            return $this->renderAjax('list-article');
         }else{
            return 0;
         }
    }
    
    public function actionListProgramExtra(){
        return $this->renderAjax('list-program-extra');
    }
    
    public function actionListArticle(){
        return $this->renderAjax('list-article');
    }
    
    public function actionSaveArticle(){
        // $menu_link, $titre, $image,$description,$is_publish
        if(isset($_REQUEST['menu_link']) && isset($_REQUEST['titre']) && isset($_REQUEST['image']) && isset($_REQUEST['description']) && isset($_REQUEST['is_publish'])){
            $menu_link = $_REQUEST["menu_link"]; 
            $image = $_REQUEST["image"]; 
            $description = $_REQUEST["description"];
            $titre = $_REQUEST["titre"];
            $is_publish = $_REQUEST["is_publish"];
            $article = new CmsArticle(); 
            $article->menu_link = $menu_link;
            $article->featured_image = $this->correctionUrlImage($image);
            $article->description = $description;
            $article->titre = $titre;
            $article->is_publish = $is_publish;
            $article->create_by = currentUser();
            $article->create_date = date('Y-m-d h:m:s');
            if($article->save()){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    
    public function actionSaveAlbum(){
        if(isset($_REQUEST['nom_album']) && isset($_REQUEST['album_description'])){
            $nom_album = $_REQUEST['nom_album'];
            $album_description = $_REQUEST['album_description'];
            $album = new CmsAlbumCat(); 
            $album->nom_gallerie = $nom_album;
            $album->description = $album_description; 
            $album->is_publish = 1;
            $album->create_by = currentUser();
            $album->create_date = date('Y-m-d h:m:s');
            if($album->save()){
                return $album->id;
            }else{
                return 0;
            }      
        }else{
            return 0;
        }
    }
    
    public function actionSaveGallerie(){
        if(isset($_REQUEST['image'])&&isset($_REQUEST['image_title'])&&isset($_REQUEST['album_id'])&&isset($_REQUEST['album_description'])&&isset($_REQUEST['is_publish'])){
            $image = $_REQUEST['image']; 
            $image_title = $_REQUEST['image_title'];
            $album_description = $_REQUEST['album_description'];
            $album_id = $_REQUEST['album_id']; 
            $is_publish = $_REQUEST['is_publish']; 
            $gallerie = new CmsImageAlbum(); 
            $gallerie->image_name = $this->correctionUrlImage($image);
            $gallerie->album_id = $album_id; 
            $gallerie->image_title = $image_title; 
            $gallerie->description = $album_description; 
            $gallerie->is_publish = $is_publish;
            $gallerie->create_by = currentUser();
            $gallerie->create_date = date('Y-m-d h:m:s');
            if($gallerie->save()){
                return 1;
            }else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    
    public function actionSaveEvent(){
        
        if(isset($_REQUEST)){
            $titre_event = $_REQUEST['titre_event']; 
            $date_start = $_REQUEST['date_start'];
            $date_end = $_REQUEST['date_end'];
            $time_start = $_REQUEST['time_start']; 
            $time_end = $_REQUEST['time_end']; 
            $one_day_event = $_REQUEST['one_day_event']; 
            $location = $_REQUEST['location']; 
            $description = $_REQUEST['description']; 
            $menu_link = $_REQUEST['menu_link'];
            if($menu_link==''){
                $menu_link = null;
            }else{
                $menu_link = $menu_link;
            }
            if($date_end==''){
                $date_end = null;
            }else{
                $date_end = date("Y-m-d",strtotime($date_end));
            }
            $is_publish = $_REQUEST['is_publish'];
            $event = new CmsEvents(); 
            $event->titre_event = $titre_event;
            $event->date_start = date("Y-m-d",strtotime($date_start)); 
            $event->date_end = $date_end;
            $event->time_start = date("H:i",strtotime($time_start)); 
            $event->time_end = date("H:i",strtotime($time_end));
            $event->one_day_event = $one_day_event;
            $event->location = $location; 
            $event->description = $description; 
            $event->menu_link = $menu_link;
            $event->is_publish = $is_publish;
            $event->create_by = currentUser();
            $event->create_date = date('Y-m-d h:i:s');
            
            if($event->save()){
                return 1;
            }else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    
    public function actionModifyeEvent(){
        
        if(isset($_REQUEST)){
            
            $id = $_REQUEST['id'];
            $titre_event = $_REQUEST['titre_event']; 
            $date_start = $_REQUEST['date_start'];
            $date_end = $_REQUEST['date_end'];
            $time_start = $_REQUEST['time_start']; 
            $time_end = $_REQUEST['time_end']; 
            $one_day_event = $_REQUEST['one_day_event']; 
            $location = $_REQUEST['location']; 
            $description = $_REQUEST['description']; 
            $menu_link = $_REQUEST['menu_link'];
            
            if($menu_link==''){
                $menu_link = null;
            }else{
                $menu_link = $menu_link;
            }
            if($date_end==''){
                $date_end = null;
            }else{
                $date_end = date("Y-m-d",strtotime($date_end));
            }
            $is_publish = $_REQUEST['is_publish'];
            $event = CmsEvents::findOne($id); 
             
            $event->titre_event = $titre_event;
            $event->date_start = date("Y-m-d",strtotime($date_start)); 
            $event->date_end = $date_end;
            $event->time_start = date("H:i",strtotime($time_start)); 
            $event->time_end = date("H:i",strtotime($time_end));
            $event->one_day_event = $one_day_event;
            $event->location = $location; 
            $event->description = $description; 
            $event->menu_link = $menu_link;
            $event->is_publish = $is_publish;
            $event->update_by = currentUser();
            $event->update_date = date('Y-m-d h:i:s');
            
            if($event->save()){
                return 1;
            }else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    
    public function actionModifyeGallerie(){
        if(isset($_REQUEST['id']) && isset($_REQUEST['image'])&&isset($_REQUEST['image_title'])&&isset($_REQUEST['album_id'])&&isset($_REQUEST['album_description'])&&isset($_REQUEST['is_publish'])){
            $image = $_REQUEST['image']; 
            $image_title = $_REQUEST['image_title'];
            $album_description = $_REQUEST['album_description'];
            $album_id = $_REQUEST['album_id']; 
            $is_publish = $_REQUEST['is_publish']; 
            $id = $_REQUEST['id'];
            $gallerie = CmsImageAlbum::findOne($id); 
            $gallerie->image_name = $this->correctionUrlImage($image);
            $gallerie->album_id = $album_id; 
            $gallerie->image_title = $image_title; 
            $gallerie->description = $album_description; 
            $gallerie->is_publish = $is_publish;
            $gallerie->update_by = currentUser();
            $gallerie->update_date = date('Y-m-d h:m:s');
            if($gallerie->save()){
                return 1;
            }else{
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    
    public function  actionUpdateGallerie($id){
        $album = CmsImageAlbum::findOne($id);
         $all_album = CmsAlbumCat::findBySql("SELECT nom_gallerie AS 'name', id AS 'id' FROM cms_album_cat ORDER BY nom_gallerie")->asArray()->all();
        $json_album = json_encode($all_album,false); 
        return $this->renderAjax('update-gallerie',['album'=>$album,'id'=>$id,'json_album'=>$json_album]);
    } 
    
    public function  actionUpdateArticle($id){
        $article = CmsArticle::findOne($id);
        return $this->renderAjax('update-article',['article'=>$article,'id'=>$id]);
    } 
    
    public function actionModifyeArticle(){
        // $id, $menu_link, $titre, $image,$description,$is_publish
        if(isset($_REQUEST['id']) && isset($_REQUEST['menu_link']) && isset($_REQUEST['titre']) && isset($_REQUEST['image']) && isset($_REQUEST['description']) && isset($_REQUEST['is_publish'])){
            $id = $_REQUEST['id'];
            $menu_link = $_REQUEST['menu_link']; 
            $image = $_REQUEST['image']; 
            $description = $_REQUEST['description'];
            $titre = $_REQUEST['titre'];
            $is_publish = $_REQUEST['is_publish'];
            $article = CmsArticle::findOne($id); 
            $article->menu_link = $menu_link;
            $article->featured_image = $this->correctionUrlImage($image);
            $article->description = $description;
            $article->titre = $titre;
            $article->is_publish = $is_publish;
            $article->update_by = currentUser();
            $article->update_date = date('Y-m-d h:m:s');
        if($article->save()){
            return 1;
        }else{
            return 0;
        }
        }else{
            return 0;
        }
    }
    
    public function actionSetPublishArticle($id,$state){
         $article = CmsArticle::findOne($id); 
         $article->is_publish = $state;
         $article->update_by = currentUser();
         $article->update_date = date('Y-m-d h:m:s');
         if($article->save()){
            return $this->renderAjax('list-article');
         }else{
            return 0;
         }
         
    }
    
    public function actionSetPublishGallerie($id,$state){
         $article = CmsImageAlbum::findOne($id); 
         $article->is_publish = $state;
         $article->update_by = currentUser();
         $article->update_date = date('Y-m-d h:m:s');
         if($article->save()){
            return $this->renderAjax('list-gallerie');
         }else{
            return 0;
         }
         
    }
    
    public function actionSetConfigBoolean($id,$state){
         $config = CmsConfig::findOne($id); 
         $config->parameter_value = $state;
         $config->update_by = currentUser();
         $config->update_date = date('Y-m-d h:m:s');
         if($config->save()){
            return $this->renderAjax('refresh-konfig');
         }else{
            return 0;
         }
         
    }
    
    public function actionSetVale(){
        if(isset($_POST['pk'])){
            $id = $_POST['pk'];
            $config = CmsConfig::findOne($id);
            $config->parameter_value = $_POST['value'];
            $config->update_by = currentUser();
            $config->update_date = date('Y-m-d h:m:s');
            if($config->save()){
            return $this->renderAjax('refresh-konfig');
            }else{
                return 0;
            }
        }
    }
    
    public function actionSetPublishEvent($id,$state){
         $event = CmsEvents::findOne($id); 
         $event->is_publish = $state;
         $event->update_by = currentUser();
         $event->update_date = date('Y-m-d h:m:s');
         if($event->save()){
            return $this->renderAjax('list-event');
         }else{
            return 0;
         }
         
    }
    
    
    public function actionCreateGallerie(){
        $album = new CmsImageAlbum(); 
        $album_cat = New CmsAlbumCat(); 
        $all_album = CmsAlbumCat::findBySql("SELECT nom_gallerie AS 'name', id AS 'id' FROM cms_album_cat ORDER BY nom_gallerie")->asArray()->all();
        $json_album = json_encode($all_album,false); 
        return $this->renderAjax('create-gallerie',['album'=>$album,'album_cat'=>$album_cat,'json_album'=>$json_album]);
    }
    
    public function correctionUrlImage($image){
        $image_arr = explode("/", $image);
        if($image_arr[0]=='http:' || $image_arr[0]=='https:'){
        $image_str = "";
        for($i=5;$i<sizeof($image_arr);$i++){
            $image_str.='/'.$image_arr[$i];
        }
        return $image_str; 
        }else{
            return $image;
        }
        
    }
    
}
