<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_config".
 *
 * @property integer $id
 * @property string $parameter_name
 * @property string $parameter_label
 * @property string $parameter_value
 * @property string $parameter_type
 * @property string $parameter_warning
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 */
class CmsConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parameter_name', 'parameter_label', 'parameter_type', 'create_by', 'create_date'], 'required'],
            [['parameter_value', 'parameter_warning'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['parameter_name', 'parameter_label', 'parameter_type'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parameter_name' => Yii::t('app', 'Parameter Name'),
            'parameter_label' => Yii::t('app', 'Parameter Label'),
            'parameter_value' => Yii::t('app', 'Parameter Value'),
            'parameter_type' => Yii::t('app', 'Parameter Type'),
            'parameter_warning' => Yii::t('app', 'Parameter Warning'),
            'create_by' => Yii::t('app', 'Create By'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }
}
