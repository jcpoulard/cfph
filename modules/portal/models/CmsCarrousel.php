<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_carrousel".
 *
 * @property integer $id
 * @property string $titre_image
 * @property string $short_description
 * @property string $image
 * @property integer $is_publish
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 */
class CmsCarrousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_carrousel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titre_image', 'short_description', 'image', 'is_publish'], 'required'],
            [['titre_image', 'short_description', 'image'], 'string'],
            [['is_publish'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'titre_image' => Yii::t('app', 'Titre Image'),
            'short_description' => Yii::t('app', 'Short Description'),
            'image' => Yii::t('app', 'Image'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'create_by' => Yii::t('app', 'Create By'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }
}
