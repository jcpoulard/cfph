<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_bank_img".
 *
 * @property integer $id
 * @property string $image_name
 * @property integer $is_in_use
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 */
class CmsBankImg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_bank_img';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_name', 'is_in_use'], 'required'],
            [['is_in_use'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['image_name', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image_name' => Yii::t('app', 'Image Name'),
            'is_in_use' => Yii::t('app', 'Is In Use'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
