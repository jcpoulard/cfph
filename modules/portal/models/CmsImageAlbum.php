<?php

namespace app\modules\portal\models;


use Yii;
use app\modules\portal\models\CmsAlbumCat;

/**
 * This is the model class for table "cms_image_album".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $image_name
 * @property string $description
 * @property string $image_title
 * @property integer $is_publish
 * @property string $publish_at
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property CmsAlbumCat $album
 */
class CmsImageAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_image_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_id', 'image_name', 'description', 'image_title', 'is_publish', 'create_date', 'create_by'], 'required'],
            [['album_id', 'is_publish'], 'integer'],
            [['description'], 'string'],
            [['publish_at', 'create_date', 'update_date'], 'safe'],
            [['image_name', 'image_title'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 64],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => CmsAlbumCat::className(), 'targetAttribute' => ['album_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'album_id' => Yii::t('app', 'Album ID'),
            'image_name' => Yii::t('app', 'Image Name'),
            'desctiption' => Yii::t('app', 'Desctiption'),
            'image_title' => Yii::t('app', 'Image Title'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'publish_at' => Yii::t('app', 'Publish At'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(CmsAlbumCat::className(), ['id' => 'album_id']);
    }
}
