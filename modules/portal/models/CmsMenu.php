<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_menu".
 *
 * @property integer $id
 * @property string $code
 * @property string $nom_menu
 * @property integer $is_publish
 */
class CmsMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'nom_menu'], 'required'],
            [['is_publish'], 'integer'],
            [['code'], 'string', 'max' => 12],
            [['nom_menu'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['nom_menu'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'nom_menu' => Yii::t('app', 'Nom Menu'),
            'is_publish' => Yii::t('app', 'Is Publish'),
        ];
    }
}
