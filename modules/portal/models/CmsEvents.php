<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_events".
 *
 * @property integer $id
 * @property string $titre_event
 * @property string $date_start
 * @property string $date_end
 * @property string $time_start
 * @property string $time_end
 * @property integer $one_day_event
 * @property string $location
 * @property string $description
 * @property string $publish_at
 * @property string $menu_link
 * @property integer $is_publish
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property CmsMenu $menuLink
 */
class CmsEvents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titre_event', 'date_start', 'time_start', 'time_end', 'one_day_event', 'location', 'description', 'is_publish', 'create_date', 'create_by'], 'required'],
            [['date_start', 'date_end', 'time_start', 'time_end', 'publish_at', 'create_date', 'update_date'], 'safe'],
            [['one_day_event', 'is_publish'], 'integer'],
            [['description'], 'string'],
            [['titre_event', 'location'], 'string', 'max' => 255],
            [['menu_link'], 'string', 'max' => 32],
            [['create_by', 'update_by'], 'string', 'max' => 64],
            [['menu_link'], 'exist', 'skipOnError' => true, 'targetClass' => CmsMenu::className(), 'targetAttribute' => ['menu_link' => 'code']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'titre_event' => Yii::t('app', 'Titre Event'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'one_day_event' => Yii::t('app', 'One Day Event'),
            'location' => Yii::t('app', 'Location'),
            'description' => Yii::t('app', 'Description'),
            'publish_at' => Yii::t('app', 'Publish At'),
            'menu_link' => Yii::t('app', 'Menu Link'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuLink()
    {
        return $this->hasOne(CmsMenu::className(), ['code' => 'menu_link']);
    }
    
    public function getMenu(){
        $menu_link = $this->menu_link; 
        if(isset($menu_link)){
            return $this->menuLink->nom_menu; 
        }else{
            return '';
        }
    }
}
