<?php

namespace app\modules\portal\models;

use Yii;

/**
 * This is the model class for table "cms_article".
 *
 * @property integer $id
 * @property string $titre
 * @property string $featured_image
 * @property string $description
 * @property string $menu_link
 * @property integer $is_publish
 * @property string $publish_at
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 */
class CmsArticle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titre', 'featured_image', 'description', 'is_publish', 'create_by', 'create_date'], 'required'],
            [['titre', 'featured_image','description'], 'string'],
            [['is_publish'], 'integer'],
            [['publish_at', 'create_date', 'update_date'], 'safe'],
            [['menu_link'], 'string', 'max' => 12],
            [['create_by', 'update_by'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'titre' => Yii::t('app', 'Titre'),
            'featured_image' => Yii::t('app', 'Featured Image'),
            'description' => Yii::t('app', 'Description'),
            'menu_link' => Yii::t('app', 'Menu Link'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'publish_at' => Yii::t('app', 'Publish At'),
            'create_by' => Yii::t('app', 'Create By'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_by' => Yii::t('app', 'Update By'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }
}
