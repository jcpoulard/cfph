<?php

namespace app\modules\portal\models;

use Yii;
use app\modules\portal\models\CmsImageAlbum;
/**
 * This is the model class for table "cms_album_cat".
 *
 * @property integer $id
 * @property string $nom_gallerie
 * @property string $description
 * @property integer $is_publish
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property CmsImageAlbum[] $cmsImageAlbums
 */
class CmsAlbumCat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_album_cat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_gallerie', 'description', 'is_publish', 'create_date', 'create_by'], 'required'],
            [['description'], 'string'],
            [['is_publish'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['nom_gallerie'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 64],
            [['nom_gallerie'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nom_gallerie' => Yii::t('app', 'Nom Gallerie'),
            'description' => Yii::t('app', 'Description'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCmsImageAlbums()
    {
        return $this->hasMany(CmsImageAlbum::className(), ['album_id' => 'id']);
    }
}
