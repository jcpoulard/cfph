<?php

namespace app\modules\portal\models;


use Yii;
use app\modules\fi\models\Program;

/**
 * This is the model class for table "cms_program_extra".
 *
 * @property integer $id
 * @property integer $program_id
 * @property string $image
 * @property string $description_programme
 * @property string $pre_requis
 * @property string $why
 * @property integer $is_publish
 * @property string $create_date
 * @property string $create_by
 * @property string $update_date
 * @property string $update_by
 *
 * @property Program $program
 */
class CmsProgramExtra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_program_extra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['program_id'], 'required'],
            [['program_id', 'is_publish'], 'integer'],
            [['description_programme', 'pre_requis', 'why'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['image', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => Program::className(), 'targetAttribute' => ['program_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'program_id' => Yii::t('app', 'Program ID'),
            'image' => Yii::t('app', 'Image'),
            'description_programme' => Yii::t('app', 'Description Programme'),
            'pre_requis' => Yii::t('app', 'Pre Requis'),
            'why' => Yii::t('app', 'Why'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Program::className(), ['id' => 'program_id']);
    }
}
