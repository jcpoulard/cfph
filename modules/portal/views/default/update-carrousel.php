<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
 
?>
<div class="clear">
</div>
<div>
<?php $form = ActiveForm::begin(
            
       ); 

?>

<div class='row-fluid'>
    <div class='col-lg-6'>
        <?php echo $form->field($carrousel,'image')->textInput()->label(Yii::t('app','Choose carrousel image')); ?>
        <input type="hidden" id="id-carrousel" value="<?= $id;?>">
    </div>
    <div class='col-lg-6'>
        <?php echo $form->field($carrousel,'titre_image')->textInput()->label(Yii::t('app','Image title')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class='col-lg-12'>
        <?php echo $form->field($carrousel,'short_description')->textArea(['name'=>'short_description'])->label(Yii::t('app','Short description')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class='col-lg-4'>
        
    </div>
    <div class='col-lg-4'>
        <a class='btn btn-primary' id='btn-update'><?= Yii::t('app','Update Draft');?></a>
        <?php               
             if( Yii::$app->user->can("portal-save_publish") ){
         ?>
        <a class='btn btn-success' id='btn-update-publish'><?= Yii::t('app','Update & Publish');?></a>
        
        <?php
             }
        ?>
        <a class='btn btn-danger' id='btn-cancel'><?= Yii::t('app','Cancel');?></a>
    </div>
    <div class='col-lg-4'>
        
    </div>
</div>

<?php ActiveForm::end(); ?>
</div>
<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
       
var featured_image = document.getElementById( 'cmscarrousel-image' );
        
        
featured_image.onclick = function() {
       selectFileWithCKFinder( 'cmscarrousel-image' );
};


function selectFileWithCKFinder( elementId ) {
	CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
	} );
}
        
// Enregistrer sans publier  
  $('#btn-update').click(function(){
        var id = $('#id-carrousel').val(); 
        var image = featured_image.value;
        var titre_image = $('#cmscarrousel-titre_image').val();
        var short_description = $('#cmscarrousel-short_description').val();
        var is_validate = true;
        if(image == ''){
            is_validate = false;
            }
        if(titre_image == ''){
            is_validate = false;
            }
        if(short_description == ''){
            is_validate = false;
            }
        
        if(is_validate == true){
        $.get('$baseUrl/index.php/portal/default/modifye-carrousel',{
            id:id,
            titre_image:titre_image, 
            image:image,
            short_description:short_description,
            is_publish:0
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-carrousel',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });  
    
   // Enregistrer et publier 
   $('#btn-update-publish').click(function(){
        var id = $('#id-carrousel').val(); 
        var image = featured_image.value;
        var titre_image = $('#cmscarrousel-titre_image').val();
        var short_description = $('#cmscarrousel-short_description').val();
        var is_validate = true;
        if(image == ''){
            is_validate = false;
            }
        if(titre_image == ''){
            is_validate = false;
            }
        if(short_description == ''){
            is_validate = false;
            }
        
        if(is_validate == true){
        $.get('$baseUrl/index.php/portal/default/modifye-carrousel',{
            id:id,
            titre_image:titre_image, 
            image:image,
            short_description:short_description,
            is_publish:1
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-carrousel',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });
          
    // Annuler 
    $('#btn-cancel').click(function(){
                
        featured_image.value="";
        $('#cmscarrousel-titre_image').val('');
        $('#cmscarrousel-short_description').val('');        
                
    });      
        

  
JS;
$this->registerJs($script,$this::POS_END);        

?>