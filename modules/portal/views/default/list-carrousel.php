<?php
use app\modules\portal\models\CmsCarrousel; 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$carrousel = CmsCarrousel::find()->all(); 
$count_publish_carrousel = CmsCarrousel::find()->where(['is_publish'=>1])->count();  

?>

<div class="row-fluid col-lg-12">
    <p>&nbsp;</p>
<table class="table table-bordered table-responsive table-striped karou">
    <thead>
        <tr>
            <th><?= Yii::t('app','#')?></th>
            <th><?= Yii::t('app','Carrousel title');?></th>
            <th><?= Yii::t('app','Description');?></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach($carrousel as $c){
                
                ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= $c->titre_image; ?></td>
            <td><a title="<?= strip_tags($c->short_description); ?>"><?= substr(strip_tags($c->short_description),0,80);?> ...</a></td>
            <td>
                <a class="modifye" data-id="<?= $c->id; ?>"><i class="fa fa-edit"></i></a>
            </td>
            <td>
               
                <?php if($c->is_publish == 1 ) { ?>
                 <a title="<?= Yii::t('app','Make unpublish') ?>" class="publish" data-id="<?= $c->id; ?>">
                <i class="fa fa-toggle-on" style="color: green"></i>
                 </a>
                <?php }else{ ?>
                 <a title="<?= Yii::t('app','Make publish');?>" class="un-publish" data-id="<?= $c->id; ?>">
                <i class="fa fa-toggle-off" style="color: red"></i>
                 </a>
                <?php } ?>
               
            </td>
            <td>
               
                <?php if($c->label_visible == 1 ) { ?>
                 <a title="<?= Yii::t('app','Hide labels ') ?>" class="visible-label" data-id="<?= $c->id; ?>">
                <i class="fa fa-eye" style="color: green"></i>
                 </a>
                <?php }else{ ?>
                 <a title="<?= Yii::t('app','Show labels');?>" class="invisible-label" data-id="<?= $c->id; ?>">
                <i class="fa fa-eye-slash" style="color: red"></i>
                 </a>
                <?php } ?>
               
            </td>
            <td><?php               
                     if( Yii::$app->user->can("portal-delete") ){
                 ?>
                <a class="efase" data-id="<?= $c->id; ?>"><i class="fa fa-trash"></i></a>
                <?php
                     }
                ?>
            </td>
        </tr>
        <?php
        $i++;
            }
        ?>
        
    </tbody>
</table>
</div>
<input type="hidden" id="konte-carrou" value="<?= $count_publish_carrousel; ?>">
<?php
$src_txt = Yii::t('app','Search');
$baseUrl = Yii::$app->request->BaseUrl;
$message_depublier = Yii::t('app','Do you really want to unpublish this content?');
$message_publier = Yii::t('app','Do you really want to publish this content?');
$message_delete = Yii::t('app','Do you really want to delete this content ?');
$message_visible = Yii::t('app','Do you really want to show labels ?');
$message_invisible = Yii::t('app','Do you really want to hide labels ?');
$script = <<< JS
        $('.modifye').click(function(){
                var id = $(this).data().id;
                 $.get('$baseUrl/index.php/portal/default/update-carrousel',{id:id},function(data){
                    $('#plas-kontni').html(data);
                });
            });
        
        $('.publish').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_depublier");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-publish-carrousel',{id:id,state:0},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.un-publish').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_publier");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-publish-carrousel',{id:id,state:1},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.visible-label').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_invisible");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-visible-label',{id:id,state:0},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.invisible-label').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_visible");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-visible-label',{id:id,state:1},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.efase').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_delete");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/delete-carrousel',{id:id},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        var konte_carrou = $('#konte-carrou').val(); 
        if(konte_carrou > 12){
                $('#create-carrou').hide();
            }else{
                    $('#create-carrou').show();
                }
        
   // JDatatable 
       $('.karou').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   
                ]

            });

        
JS;
$this->registerJs($script,$this::POS_END);        

?>