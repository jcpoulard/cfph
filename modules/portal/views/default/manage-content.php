<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\portal\models\CmsMenu; 

?>

<div class="row">
    <?= $this->render('//layouts/cmsLayout'); ?>
</div>
<?php $form = ActiveForm::begin(
            
            ); ?>
<div id="zon-pam">
    <div class="row-fluid">
        <div class="col-lg-10 dropdown"> <!-- dropdown -->
                  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                      <?= Yii::t('app','Choose menu or site web interface block to view contents'); ?>
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                      <?php
                      $menu = CmsMenu::find()->where(['is_system'=>1,'is_publish'=>1])->all();
                      foreach($menu as $m){
                          ?>
                      <li><a id="<?= $m->code; ?>" class="meni" data-titre="<?= $m->nom_menu; ?>" data-code="<?= $m->code; ?>"><?= $m->nom_menu; ?></a></li>
                      <?php
                      }
                      ?>
                </ul>
        </div>
        <div class="col-lg-2" id="espas-tit">
            
        </div>
    </div>    

<div class="clear">
    
</div>
<div class='row-fluid'>
    <div id='plas-kontni'>
        
    </div>
</div>
</div>    
<?php ActiveForm::end(); ?>


<?php

$baseUrl = Yii::$app->request->BaseUrl;

$script = <<< JS
$(document).ready(function(){
    $('#cmsmenu-id').change(function(){
            var code_menu = $('#cmsmenu-id').val();
            switch(code_menu){
                    case 'prog': {
                           $.get('$baseUrl/index.php/portal/default/create-program-extra',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
                            }
                    break; 
                    case 'carrou' :{
                            $.get('$baseUrl/index.php/portal/default/create-carrousel',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
                        }    
        
                }
            });
        
        $('.meni').click(function(){
                $('#espas-tit').html('<button  type="button" id="create-'+$(this).data().code+'" class="btn btn-primary"><i class="fa fa-plus"></i> '+$(this).data().titre+'</button>');
                var code_menu = $(this).data().code;
                switch(code_menu){
                        case 'carrou':{
                            $.get('$baseUrl/index.php/portal/default/list-carrousel',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
                            }
                            break; 
                        case 'prog' : {
                            $.get('$baseUrl/index.php/portal/default/list-program-extra',{},function(data){
                                $('#plas-kontni').html(data);
                            });
                            }
                        break;
                        case 'art' : {
                            $.get('$baseUrl/index.php/portal/default/list-article',{},function(data){
                                $('#plas-kontni').html(data);
                            });    
                            }
                        break;
                        case 'gal' : {
                            $.get('$baseUrl/index.php/portal/default/list-gallerie',{},function(data){
                                $('#plas-kontni').html(data);
                            });    
                            }
                        break;
                        case 'event' : {
                            $.get('$baseUrl/index.php/portal/default/list-event',{},function(data){
                                $('#plas-kontni').html(data);
                            });    
                            }
                        break;
                    }
                
            });
        
        $(document).on('click','#create-carrou',function(){
            $.get('$baseUrl/index.php/portal/default/create-carrousel',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
            });
        
        $(document).on('click','#create-prog',function(){
            $.get('$baseUrl/index.php/portal/default/create-program-extra',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
            });
        
        $(document).on('click','#create-art',function(){
            $.get('$baseUrl/index.php/portal/default/create-article',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
            });
        
        $(document).on('click','#create-gal',function(){
            $.get('$baseUrl/index.php/portal/default/create-gallerie',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
            });
        
      $(document).on('click','#create-event',function(){
            $.get('$baseUrl/index.php/portal/default/create-event',{},function(data){
                                   $('#plas-kontni').html(data);     
                                });
            });
        
   });        
          
        
        $(document).ready(function(){
                $('#espas-tit').html('<button  type="button" id="create-art" class="btn btn-primary"><i class="fa fa-plus"></i> Article </button>');
                $.get('$baseUrl/index.php/portal/default/list-article',{},function(data){
                    $('#plas-kontni').html(data);
                });
            });
        
        
JS;
$this->registerJs($script,$this::POS_END);        

?>