<?php
use app\modules\portal\models\CmsEvents; 
use app\modules\portal\models\CmsMenu;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$event = CmsEvents::find()->orderBy('id DESC')->all(); 

?>

<div class="row-fluid col-lg-12">
    <p>&nbsp;</p>
<table class="table table-bordered table-responsive table-striped event">
    <thead>
        <tr>
            <th><?= Yii::t('app','#')?></th>
            <th><?= Yii::t('app','Title');?></th>
            <th><?= Yii::t('app','Date event');?></th>
            <th><?= Yii::t('app','Menu categorie');?></th>
            <th><?= Yii::t('app','Description');?></th>
            <th></th>
            <th></th>
            <th></th>
            
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach($event as $e){
                
                ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= $e->titre_event; ?></td>
            <td>
            <?php
            Yii::$app->formatter->locale = 'fr-FR';
            echo Yii::$app->formatter->format($e->date_start, 'date');
            ?>
            </td>
            <td>
                <?= $e->menu; ?>
                
            </td>
            <td><a><?= substr(strip_tags($e->description),0,80);?> ...</a></td>
            <td>
                <a class="modifye" data-id="<?= $e->id; ?>"><i class="fa fa-edit"></i></a>
            </td>
            <td>
               
                <?php if($e->is_publish == 1 ) { ?>
                 <a class="publish" data-id="<?= $e->id; ?>">
                <i class="fa fa-toggle-on" style="color: green"></i>
                 </a>
                <?php }else{ ?>
                 <a class="un-publish" data-id="<?= $e->id; ?>">
                <i class="fa fa-toggle-off" style="color: red"></i>
                 </a>
                <?php } ?>
               
            </td>
            <td><?php               
                     if( Yii::$app->user->can("portal-delete") ){
                 ?>
                <a class="efase" data-id="<?= $e->id; ?>"><i class="fa fa-trash"></i></a>
                <?php
                     }
                ?>
            </td>
        </tr>
        <?php
        $i++;
            }
        ?>
        
    </tbody>
</table>
</div>

<?php
$src_txt = Yii::t('app','Search');
$baseUrl = Yii::$app->request->BaseUrl;
$message_depublier = Yii::t('app','Do you really want to unpublish this content?');
$message_publier = Yii::t('app','Do you really want to publish this content?');
$message_delete = Yii::t('app','Do you really want to delete this content ?');
$script = <<< JS
        $('.modifye').click(function(){
                var id = $(this).data().id;
                 $.get('$baseUrl/index.php/portal/default/update-event',{id:id},function(data){
                    $('#plas-kontni').html(data);
                });
            });
        
        $('.publish').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_depublier");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-publish-event',{id:id,state:0},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.un-publish').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_publier");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-publish-event',{id:id,state:1},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        $('.efase').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_delete");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/delete-event',{id:id},function(data){
                    $('#plas-kontni').html(data);
                });
                }
            });
        
        
   // JDatatable 
       $('.event').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   
                ]

            });

        
JS;
$this->registerJs($script,$this::POS_END);        

?>