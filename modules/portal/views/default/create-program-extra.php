<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\fi\models\Program; 
?>
<?php $form = ActiveForm::begin(
            
       ); ?>
<div class='row-fluid'>
    <div class="col-lg-6">
        <?= $form->field($program, 'id')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(Program::find()->all() ,'id','label' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select program  --'),
                                    ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Choose a program')); 
         ?> 
    </div>
    <div class='col-lg-6'>
        <?php echo $form->field($program_extra,'image')->textInput()->label(Yii::t('app','Choose featured image')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class='col-lg-12'>
        <?php echo $form->field($program_extra,'description_programme')->textArea(['name'=>'description_program'])->label(Yii::t('app','Description programme')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class='col-lg-12'>
        <?php echo $form->field($program_extra,'pre_requis')->textArea()->label(Yii::t('app','Pr&eacute;requis')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class='col-lg-12'>
        <?php echo $form->field($program_extra,'why')->textArea()->label(Yii::t('app','Pourquoi ?')); ?>
    </div>
</div>

<div class='row-fluid'>
    <div class='col-lg-2'>
        
    </div>
    <div class='col-lg-8' style="text-align: center">
        <a class='btn btn-primary' id='btn-save'><?= Yii::t('app','Save Draft');?></a>
        <?php               
             if( Yii::$app->user->can("portal-save_publish") ){
         ?>
        <a class='btn btn-success' id='btn-save-publish'><?= Yii::t('app','Save & Publish');?></a>
        <?php
             }
        ?>
        <a class='btn btn-danger' id='btn-cancel'><?= Yii::t('app','Cancel');?></a>
    </div>
    <div class='col-lg-2'>
        
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
var featured_image = document.getElementById( 'cmsprogramextra-image' );
var description; 
var prerequis;
var why_space;
        
featured_image.onclick = function() {
       selectFileWithCKFinder( 'cmsprogramextra-image' );
};


function selectFileWithCKFinder( elementId ) {
	CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
	} );
}
        
ClassicEditor.create( document.querySelector( '#cmsprogramextra-description_programme' ), {
                ckfinder: {
                            uploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    },
                language: 'fr'
            })
		.then( editor => {
			window.editor = editor;
                        description = editor; 
		} )
		.catch( err => {
			console.error( err.stack );
		} ); 
                    
ClassicEditor.create( document.querySelector( '#cmsprogramextra-pre_requis' ), {
                ckfinder: {
                            uploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    },
                language: 'fr'
            })
		.then( editor => {
			window.editor = editor;
                        prerequis = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );

 ClassicEditor.create( document.querySelector( '#cmsprogramextra-why' ), {
                ckfinder: {
                            uploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    },
                language: 'fr'
            })
		.then( editor => {
			window.editor = editor;
                        why_space = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} ); 
        
// Enregistrer extra information pour programme 

  
  // Enregistrer sans publier  
  $('#btn-save').click(function(){
        var program_id = $('#program-id').val();
        var image = featured_image.value;
        var description_program = description.getData();
        var pre_requis = prerequis.getData();
        var why = why_space.getData();
        var is_validate = true;
        if(program_id == ''){
            is_validate = false;
            }
        if(description_program == '' || description_program == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(pre_requis=='' || pre_requis == '<p>&nbsp;</p>' ){
                is_validate = false;
            }
        if(why == '' || why == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(is_validate == true){
        $.get('$baseUrl/index.php/portal/default/save-program-extra',{
            program_id:program_id, 
            image:image,
            description_program:description_program,
            pre_requis:pre_requis,
            why:why,
            is_publish:0
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-program-extra',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });
     
   // Enregistrer et publier 
    $('#btn-save-publish').click(function(){
        var program_id = $('#program-id').val();
        var image = featured_image.value;
        var description_program = description.getData();
        var pre_requis = prerequis.getData();
        var why = why_space.getData();
        var is_validate = true;
        if(program_id == ''){
            is_validate = false;
            }
        if(description_program == '' || description_program == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(pre_requis=='' || pre_requis == '<p>&nbsp;</p>' ){
                is_validate = false;
            }
        if(why == '' || why == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(is_validate == true){
        $.get('$baseUrl/index.php/portal/default/save-program-extra',{
            program_id:program_id, 
            image:image,
            description_program:description_program,
            pre_requis:pre_requis,
            why:why,
            is_publish:1
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-program-extra',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      }); 
     
   // Annuler 
    $('#btn-cancel').click(function(){
                $('#program-id').empty();
                featured_image.value="";
                description.setData('');
                prerequis.setData('');
                why_space.setData('');
          });     
  
JS;
$this->registerJs($script,$this::POS_END);        

?>