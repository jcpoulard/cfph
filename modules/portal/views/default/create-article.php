<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\fi\models\Program; 
use app\modules\portal\models\CmsMenu;

?>
<div>
<?php $form = ActiveForm::begin(
            
       ); ?>

<div class='row-fluid'>
    <div class="col-lg-4">
        <?= $form->field($article, 'menu_link')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(CmsMenu::find()->where(['can_have_article'=>1,'is_publish'=>1])->all() ,'code','nom_menu' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select menu  --'),
                                    ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Choose a menu')); 
         ?> 
    </div>
    <div class='col-lg-4'>
        <?php echo $form->field($article,'featured_image')->textInput()->label(Yii::t('app','Choose featured image')); ?>
    </div>
    <div class='col-lg-4'>
        <?php echo $form->field($article,'titre')->textInput()->label(Yii::t('app','Title')); ?>
    </div>
</div>

<div class='row-fluid'>
    <div class='col-lg-12'>
        <?php echo $form->field($article,'description')->textArea(['name'=>'description'])->label(Yii::t('app','Text article')); ?>
    </div>
</div>


<div class='row-fluid'>
    <div class='col-lg-2'>
        
    </div>
    <div class='col-lg-8' style="text-align: center">
        <a class='btn btn-primary' id='btn-save'><?= Yii::t('app','Save Draft');?></a>
        <?php               
             if( Yii::$app->user->can("portal-save_publish") ){
         ?>
        <a class='btn btn-success' id='btn-save-publish'><?= Yii::t('app','Save & Publish');?></a>
        <?php
             }
        ?>
        <a class='btn btn-danger' id='btn-cancel'><?= Yii::t('app','Cancel');?></a>
    </div>
    <div class='col-lg-2'>
        
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS

$(document).ready(function(){
        var featured_image = document.getElementById( 'cmsarticle-featured_image' );
var description; 

      
featured_image.onclick = function() {
       selectFileWithCKFinder( 'cmsarticle-featured_image' );
};


function selectFileWithCKFinder( elementId ) {
	CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
	} );
}
        
ClassicEditor.create( document.querySelector( '#cmsarticle-description' ), {
                ckfinder: {
                            uploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    },
                language: 'fr',
               
                
            })
		.then( editor => {
			window.editor = editor;
                        description = editor; 
		} )
		.catch( err => {
			console.error( err.stack );
		} ); 
 
        
    // Enregistrer et sans publier 
     
  $('#btn-save').click(function(){
        var menu_link = $('#cmsarticle-menu_link').val();
        var titre = $('#cmsarticle-titre').val();
        var image = featured_image.value;
        var text_article = description.getData();
        var is_validate = true;
        if(menu_link == ''){
            is_validate = false;
            }
        if(text_article == '' || text_article == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(image==''){
                is_validate = false;
            }
        
        if(is_validate == true){
        $.post('$baseUrl/index.php/portal/default/save-article',{
            menu_link:menu_link,
            titre:titre,
            image:image,
            description:text_article,
            is_publish:0
            },function(data){
                if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-article',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
            });
        
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });
     
   // Enregistrer et publier 
     
  $('#btn-save-publish').click(function(){
        var menu_link = $('#cmsarticle-menu_link').val();
        var titre = $('#cmsarticle-titre').val();
        var image = featured_image.value;
        var text_article = description.getData();
        var is_validate = true;
        if(menu_link == ''){
            is_validate = false;
            }
        if(text_article == '' || text_article == '<p>&nbsp;</p>'){
                is_validate = false;
            }
        if(image==''){
                is_validate = false;
            }
        
        if(is_validate == true){
        $.post('$baseUrl/index.php/portal/default/save-article',{
            menu_link:menu_link,
            titre:titre,
            image:image,
            description:text_article,
            is_publish:1
            },function(data){
                if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-article',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
            });
        
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });
     
   // Annuler 
    $('#btn-cancel').click(function(){
                $('#cmsarticle-menu_link').empty();
                $('#cmsarticle-titre').val('');
                featured_image.value="";
                description.setData('');
                
          });     
  
    });   
      

JS;
$this->registerJs($script,$this::POS_END);        

?>