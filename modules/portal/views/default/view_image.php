<?php 
  use yii\helpers\Url;
  
?>
<div class="espas_browser">
   <?php 
    foreach($all_image as $am){
        ?>
    <div class="col-lg-1" tabindex="0"
            role="button" 
            contenteditable="" accesskey="" data-html="true" 
            contextmenu="" data-toggle="popover" 
            data-trigger="click" 
            
            data-placement="bottom"
            data-content="<div><i class='fa fa-pencil'></i> | <i class='fa fa-trash'></i></div>">
        <img  style="width: 30px; height: 30px;" class="ti-imaj" src="<?= Url::to("@web/web_uploads/$am->image_name") ?>" alt=""/>
        <p><?= $am->image_name?></p>
    </div>
    <?php 
    }
   ?>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>