<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
 
?>
<div class="clear">
</div>
<div>
<?php $form = ActiveForm::begin(
            
       ); 

?>
<div class='row-fluid'>
    <div class='col-lg-6'>
        <?php echo $form->field($album,'image_name')->textInput()->label(Yii::t('app','Choose image for album')); ?>
        <input type="hidden" id="id-gallerie" value="<?= $id; ?>">
    </div>
    <div class='col-lg-6'>
        <?php echo $form->field($album,'image_title')->textInput()->label(Yii::t('app','Image title')); ?>
    </div>
</div>
<div class='row-fluid'>
    <div class="col-lg-6">
        <label for="album-name"><?= Yii::t('app','Choose album')?> <span id="error-album" style="color: #FF0000;"></span></label>
        <div class="input-group">
            <input class="form-control" type="text" id="album-name" name="album-name" data-selectedid="" value="<?= $album->album->nom_gallerie; ?>"> 
            <!--  Place the id of album here -->
            <input type="hidden" id="album-id"  value="<?= $album->id ?>"/>
            <span class="input-group-btn"> 
                <button type="button" class="btn btn-primary" id="add-album">
                    <i class="fa fa-plus"></i>
                    </button> 
            </span>

        </div>
                            
                            
    </div>
    <div class='col-lg-6'>
        <?php echo $form->field($album,'description')->textArea(['name'=>'description'])->label(Yii::t('app','Description')); ?>
    </div>
</div>  
<div class='row-fluid'>
    <div class='col-lg-4'>
        
    </div>
    <div class='col-lg-4'>
        <a class='btn btn-primary' id='btn-update'><?= Yii::t('app','Update Draft');?></a>
        <?php               
             if( Yii::$app->user->can("portal-save_publish") ){
         ?>
        <a class='btn btn-success' id='btn-update-publish'><?= Yii::t('app','Update & Publish');?></a>
        <?php
             }
        ?>
        <a class='btn btn-danger' id='btn-cancel'><?= Yii::t('app','Cancel');?></a>
    </div>
    <div class='col-lg-4'>
        
    </div>
</div>    
<?php ActiveForm::end(); ?>    

</div>   

<!-- Modal pour ajouter les artistes -->
 
 <!-- Modal -->
  <div class="modal fade" id="modal-album" role="dialog">
    <div class="modal-dialog lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add Album') ?></h4>
        </div>
        <div class="modal-body">
            <div class='row'>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nom-album"><?= Yii::t('app','Album name') ?></label>
                            <input type="text" class="form-control" id="nom-album" name="nom-album">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="album-description"><?= Yii::t('app','Album description') ?></label>
                            <input type="text" class="form-control" id="album-description" name="album-description">
                    </div>
                </div>
            </div>
            
            
            
        </div>
          
        <div class="modal-footer">
          <button type="button" id='save-album' class='btn btn-success'><?= Yii::t('app','Save & Publish')?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>
 
 <?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script = <<< JS
      
var image = document.getElementById( 'cmsimagealbum-image_name' );
        
image.onclick = function() {
       selectFileWithCKFinder('cmsimagealbum-image_name');
};


function selectFileWithCKFinder( elementId ) {
	CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
	} );
}
        
// Enregistrer album categorie in the table cms_album_cat  
        $('#save-album').click(function(){
            var nom_album = $('#nom-album').val();
            var album_description = $('#album-description').val();
            $.post('$baseUrl/index.php/portal/default/save-album',{nom_album:nom_album, album_description:album_description}, function(data){
              $('#album-id').val(data); 
              $('#album-name').val(nom_album); 
              $('#nom-album').val('');
              $('#album-description').val('');
              $('#modal-album').modal('toggle');
            });
            
        });
            
 // Liste de choix des album 
           $('#album-name').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_album; // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#album-id').val(map[item].id);
                return item;
            }
                
                
            });       
        
// Faire afficher modal album 
        $('#add-album').click(function(){
            $("#modal-album").modal();
            $('#modal-album').on('hidden.bs.modal', function () {
               $('#nom-album').val('');
               $('#album-description').val('');
              
              });
        });        
       
// Enregistrer sans publier  
  $('#btn-update').click(function(){
        var imagej = image.value;
        var image_title = $('#cmsimagealbum-image_title').val();
        var album_id = $('#album-id').val();
        var album_description = $('#cmsimagealbum-description').val();
        var id_gallerie = $('#id-gallerie').val();
        var is_validate = true;
        if(imagej == ''){
            is_validate = false;
            }
        if(image_title == ''){
            is_validate = false;
            }
        if(album_description == ''){
            is_validate = false;
            }
        if(album_id == ''){
            is_validate = false;
        }
        
        if(is_validate == true){
        $.post('$baseUrl/index.php/portal/default/modifye-gallerie',{
            id:id_gallerie,
            image:imagej, 
            image_title:image_title,
            album_description:album_description,
            album_id:album_id,
            is_publish:0
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-gallerie',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });  
    
  // Enregistrer et publier  
  $('#btn-update-publish').click(function(){
        var imagej = image.value;
        var image_title = $('#cmsimagealbum-image_title').val();
        var album_id = $('#album-id').val();
        var album_description = $('#cmsimagealbum-description').val();
        var id_gallerie = $('#id-gallerie').val();
        var is_validate = true;
        if(imagej == ''){
            is_validate = false;
            }
        if(image_title == ''){
            is_validate = false;
            }
        if(album_description == ''){
            is_validate = false;
            }
        if(album_id == ''){
            is_validate = false;
        }
        
        if(is_validate == true){
        $.post('$baseUrl/index.php/portal/default/modifye-gallerie',{
            id:id_gallerie,
            image:imagej, 
            image_title:image_title,
            album_description:album_description,
            album_id:album_id,
            is_publish:1
                },function(data){
                    if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-gallerie',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
                    
                    });
        }else{
                alert("Tous les champs sont obligatoires !");
            }
      });  
          
    // Annuler 
    $('#btn-cancel').click(function(){
                
        image.value="";
        $('#cmsimagealbum-image_title').val('');
        $('#cmsimagealbum-description').val(''); 
        $('#album-id').val('');    
                
    });
        
   
        

  
JS;
$this->registerJs($script,$this::POS_END);        

?>