<?php 
    use app\modules\portal\models\CmsConfig;
    $config = CmsConfig::find()->all(); 
?>

<div class="row">
    <?= $this->render('//layouts/cmsLayout'); ?>
</div>

<div class='row'>
    <div class='col-lg-1'></div>
    <div class='col-lg-10' id="konf">
    <table class="table table-bordered table-responsive table-striped konfig">
        <thead>
            <th>#</th>
            <th><?= Yii::t('app','Properties') ?></th>
            <th><?= Yii::t('app','Values')?></th>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach($config as $c){
                ?>
            <tr>
                <td><?= $i; ?></td>
                <td><?= $c->parameter_label ?></td>
                <td>
                    <?php 
                        switch($c->parameter_type){
                            case 'boolean':{
                                ?>
                            <?php if($c->parameter_value == 1 ) { ?>
                             <a class="vre <?= $c->parameter_type; ?>" data-id="<?= $c->id; ?>" data-msg='<?= $c->boolean_warning ?>'>
                                <i class="fa fa-toggle-on" style="color: green"></i>
                             </a>
                            <?php }else{ ?>
                             <a class="fo <?= $c->parameter_type; ?>" data-id="<?= $c->id; ?>" data-msg='<?= $c->parameter_warning ?>'>
                                <i class="fa fa-toggle-off" style="color: red"></i>
                             </a>
                            <?php } ?>    
                    <?php 
                            }
                            break; 
                            case 'textarea' :{
                    ?>
                    <span style="float: left">
                        <a href="#" class="zonteks" style="float: left" data-name="<?= $c->parameter_name; ?>" data-type="textarea" data-pk="<?= $c->id; ?>"  data-url="set-vale" data-original-title="<?= Yii::t('app','Update {name}',['name'=>$c->parameter_label]);?>" title="">
                                <?= $c->parameter_value; ?>
                            </a>
                    </span>
                                
                    <?php
                                
                            }
                        }
                    ?>
                    
                </td>
            </tr>
            <?php
            $i++;
            }
            ?>
        </tbody>
    </table>
</div>
    <div class='col-lg-1'></div>
</div>

<?php
$src_txt = Yii::t('app','Search');
$baseUrl = Yii::$app->request->BaseUrl;
$message_depublier = Yii::t('app','Do you really want to unpublish this content?');
$message_publier = Yii::t('app','Do you really want to publish this content?');
$message_delete = Yii::t('app','Do you really want to delete this content ?');
$message_empty = Yii::t('app','Add');
$script = <<< JS
    $(document).ready(function(){
            $('.modifye').click(function(){
                var id = $(this).data().id;
                 $.get('$baseUrl/index.php/portal/default/update-article',{id:id},function(data){
                    $('#plas-kontni').html(data);
                });
            });
        
        $('.vre').click(function(){
                var id = $(this).data().id;
                var message = $(this).data().msg;
                konfimasyon = confirm(message);
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-config-boolean',{id:id,state:0},function(data){
                    //alert(data);
                    $('#konf').html(data);
                });
                }
            });
        
        $('.fo').click(function(){
                var id = $(this).data().id;
                var message = $(this).data().msg;
                konfimasyon = confirm(message);
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/set-config-boolean',{id:id,state:1},function(data){
                    //alert(data);
                    $('#konf').html(data);
                });
                }
            });
        
        $('.efase').click(function(){
                var id = $(this).data().id;
                konfimasyon = confirm("$message_delete");
                if(konfimasyon){
                 $.get('$baseUrl/index.php/portal/default/delete-article',{id:id},function(data){
                    //$(this).html(data);
                });
                }
            });
        
        
   // JDatatable 
       $('.konfig').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                   
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   
                ]

            });
        
        $('.zonteks').editable(
                {
                'emptytext' : '$message_empty',
                //'mode'  : 'inline',
                showbuttons: true,
                
            }
            );
        });    
        

        
JS;
$this->registerJs($script,$this::POS_END);        

?>