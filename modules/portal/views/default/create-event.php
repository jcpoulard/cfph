<?php
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\fi\models\Program; 
use app\modules\portal\models\CmsMenu;
use kartik\widgets\DatePicker;
use kartik\widgets\TimePicker;

?>
<?php $form = ActiveForm::begin(
            
       ); ?>
<div class='row-fluid'>
    
    <div class='col-lg-4'>
        <?php echo $form->field($event,'titre_event')->textInput()->label(Yii::t('app','Event title')); ?>
    </div>
    <div class='col-lg-4'>
        <?php
                echo $form->field($event, 'date_start')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] )->label(Yii::t('app','Date event'));
               
            ?>
    </div>
    <div class='col-lg-4'>
        <?php
                echo $form->field($event, 'date_end')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] )->label(Yii::t('app','End date event'));
               
            ?>
    </div>
</div>
<div class='row-fluid'>
    
    
    <div class='col-lg-3'>
    <?php
        echo '<label>'.Yii::t('app','Time start').'</label>';    
        echo TimePicker::widget([
            'name' => 'time_start', 
            'id'=>'time_start',
            'pluginOptions' => [
            'minuteStep' => 1,
            'showSeconds' => false,
            'showMeridian' => false
        ]
        ]);
         
    ?>
    </div>
    <div class='col-lg-3'>
        <?php
        echo '<label>'.Yii::t('app','Time end').'</label>';    
        echo TimePicker::widget([
            'name' => 'time_end',
            'id'=>'time_end',
            'pluginOptions' => [
            'minuteStep' => 1,
            'showSeconds' => false,
            'showMeridian' => false
        ]
        ]);
         
    ?>
    </div>
    <div class='col-lg-3'>
        <div class="form-group">
        <label for="album-name"><?= Yii::t('app','Event category')?> <span id="error-album" style="color: #FF0000;"></span></label>
        <div class="input-group">
            <input class="form-control" type="text" id="event-category" name="event-category" data-selectedid=""> 
            <input type="hidden" id="menu-code"  value=""/>
            <!--  Place the id of album here -->
            <input type="hidden" id="menu-link"  value=""/>
            
        </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for=event-location><?= Yii::t('app','Event location')?> </label>
        <div class="input-group">
            <input class="form-control lokasyon" type="text" id="event-location" name="event-location" data-selectedid=""> 
             <input type="hidden" id="location-id"  value=""/>
           </div>
    </div>
</div>

<div class='row-fluid'>
    
    <div class='col-lg-12'>
        <?php echo $form->field($event,'description')->textArea(['name'=>'description'])->label(Yii::t('app','Event description')); ?>
    </div>
</div>


<div class='row-fluid'>
    <div class='col-lg-2'>
        
    </div>
    <div class='col-lg-8' style="text-align: center">
        <a class='btn btn-primary' id='btn-save'><?= Yii::t('app','Save Draft');?></a>
        <?php               
             if( Yii::$app->user->can("portal-save_publish") ){
         ?>
        <a class='btn btn-success' id='btn-save-publish'><?= Yii::t('app','Save & Publish');?></a>
        <?php
             }
        ?>
        <a class='btn btn-danger' id='btn-cancel'><?= Yii::t('app','Cancel');?></a>
    </div>
    <div class='col-lg-2'>
        
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$message_date = Yii::t('app','Date Start need to be before date end !');
$message_time = Yii::t('app','Time Start need to be before time end !');
$script = <<< JS
      
$(document).ready(function(){
         // Enregistrer et sans publier 
     
  $('#btn-save').click(function(){
        var event_title = $('#cmsevents-titre_event').val();
        var date_start = $('#cmsevents-date_start').val(); 
        var date_end = $('#cmsevents-date_end').val();
        var one_day_event = 0;
        if( (new Date(date_start).getTime() > new Date(date_end).getTime()))
            {
                    alert('$message_date');
                    exit();
        }
        var time_start = $('#time_start').val();
        var time_end = $('#time_end').val();
        if( (new Date("July 22, 1979 " + time_start).getTime() > new Date("July 22, 1979 " + time_end).getTime()))
            {
                    alert('$message_time');
                    exit();
        }
        var menu_code = $('#menu-code').val(); 
        var event_location = $('#event-location').val();
        var event_description = $('#cmsevents-description').val();
        
        var is_validate = true;
        if(event_title == ''){
            $('#cmsevents-titre_event').addClass('obligatwa');
            is_validate = false;
        }
        
        if(date_start == ''){
            $('#cmsevents-date_start').addClass('obligatwa');
            is_validate = false;
        }
        
        if(time_start == ''){
            $('#time_start').addClass('obligatwa');
            is_validate = false;
        }
        if(time_end == ''){
            $('#time_end').addClass('obligatwa');
            is_validate = false;
        }
        
        if(event_location == ''){
            $('#event-location').removeClass('lokasyon').addClass('obligatwa');
            //$('#event-location').addClass('obligatwa');
            alert('eske se ou');
            is_validate = false;
        }
        if(event_description == ''){
            $('#cmsevents-description').addClass('obligatwa');
            is_validate = false;
        }
        if(date_end==''){
            one_day_event = 1;
            }
        
        if(is_validate == true){
        
        $.post('$baseUrl/index.php/portal/default/save-event',{
            titre_event:event_title,
            date_start:date_start,
            date_end:date_end,
            time_start:time_start,
            time_end:time_end,
            one_day_event: one_day_event,
            location:event_location,
            description:event_description,
            menu_link:menu_code,
            is_publish:0
            },function(data){
                if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-event',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
            });
        
        }else{
                alert("Tous les champs en rouge sont obligatoires!");
            }
      });
     
   // Enregistrer et publier 
     
  $('#btn-save-publish').click(function(){
        var event_title = $('#cmsevents-titre_event').val();
        var date_start = $('#cmsevents-date_start').val(); 
        var date_end = $('#cmsevents-date_end').val();
        var one_day_event = 0;
        if( (new Date(date_start).getTime() > new Date(date_end).getTime()))
            {
                    alert('$message_date');
                    exit();
        }
        var time_start = $('#time_start').val();
        var time_end = $('#time_end').val();
        if( (new Date("July 22, 1979 " + time_start).getTime() > new Date("July 22, 1979 " + time_end).getTime()))
            {
                    alert('$message_time');
                    exit();
        }
        var menu_code = $('#menu-code').val(); 
        var event_location = $('#event-location').val();
        var event_description = $('#cmsevents-description').val();
        
        var is_validate = true;
        if(event_title == ''){
            $('#cmsevents-titre_event').addClass('obligatwa');
            is_validate = false;
        }
        
        if(date_start == ''){
            $('#cmsevents-date_start').addClass('obligatwa');
            is_validate = false;
        }
        
        if(time_start == ''){
            $('#time_start').addClass('obligatwa');
            is_validate = false;
        }
        if(time_end == ''){
            $('#time_end').addClass('obligatwa');
            is_validate = false;
        }
        
        if(event_location == ''){
            $('#event-location').removeClass('lokasyon').addClass('obligatwa');
            //$('#event-location').addClass('obligatwa');
            
            is_validate = false;
        }
        if(event_description == ''){
            $('#cmsevents-description').addClass('obligatwa');
            is_validate = false;
        }
        if(date_end==''){
            one_day_event = 1;
            }
        
        if(is_validate == true){
        
        $.post('$baseUrl/index.php/portal/default/save-event',{
            titre_event:event_title,
            date_start:date_start,
            date_end:date_end,
            time_start:time_start,
            time_end:time_end,
            one_day_event: one_day_event,
            location:event_location,
            description:event_description,
            menu_link:menu_code,
            is_publish:1
            },function(data){
                if(data==1){
                            $.get('$baseUrl/index.php/portal/default/list-event',{},function(data){
                                    $('#plas-kontni').html(data);
                                });
                        }else{
                            $('#plas-kontni').html(data);
                            }
            });
        
        }else{
                alert("Tous les champs en rouge sont obligatoires!");
            }
      });
     
   // Annuler 
    $('#btn-cancel').click(function(){
                $('#cmsarticle-menu_link').empty();
                $('#cmsarticle-titre').val('');
                featured_image.value="";
                description.setData('');
                
          }); 
              
        
// Liste de choix des menu qui jouent le role de categorie
    $('#event-category').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_menu; // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
                updater: function(item) {
                    $('#menu-code').val(map[item].id);
        
                return item;
            }
    }); 
        
$('#event-location').typeahead(
            {
                name: "result",
                displayKey: "name",
                source: function(query, process) {
                objects = [];
                map = {};
                var data = $json_location; // Or get your JSON dynamically and load it into this variable
                $.each(data, function(i, object) {
                 map[object.name] = object;
                 objects.push(object.name);
                });
                process(objects);
                },
           updater: function(item) {
                    $('#event-location').val(map[item].id);
        
                return item;
            }
       
    });       
    });
    
     
  
JS;
$this->registerJs($script,$this::POS_END);        

?>