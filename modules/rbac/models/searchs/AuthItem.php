<?php

namespace app\modules\rbac\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\rbac\Item;



/**
 * AuthItemSearch represents the model behind the search form about AuthItem.
 * 
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class AuthItem extends Model
{
    const TYPE_ROUTE = 101;

    public $name;
    public $type;
    public $description;
    public $ruleName;
    public $data;
    public $globalSearch;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ruleName', 'description','globalSearch'], 'safe'],
            [['type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'item_name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'ruleName' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
        ];
    }

    /**
     * Search authitem
     * @param array $params
     * @return \yii\data\ActiveDataProvider|\yii\data\ArrayDataProvider
     */
    public function search($params)
    {
        /* @var \yii\rbac\Manager $authManager */
        $authManager = Yii::$app->getAuthManager();
        if ($this->type == Item::TYPE_ROLE) 
         {
              $user_role = getRoleByUserId(Yii::$app->user->identity->id);
              
              if(Yii::$app->user->can('superadmin'))
                  $items = $authManager->getRoles();
              elseif(Yii::$app->user->can('direction'))  
                    $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy"]);
              elseif(Yii::$app->user->can('manager'))  
                    $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy",'direction'=>"xy",]);
              else
                 $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy",'direction'=>"xy",'manager'=>"xy",$user_role=>"xy",]);   //retire role user ankour lan
          
          
           
           //array_diff_key($authManager->getRoles(),['direction'=>"xy"]) );
               	 // $items = $authManager->getRoles();
          } 
        else 
        {
            $items = array_filter($authManager->getPermissions(), function($item) {
                return $this->type == Item::TYPE_PERMISSION xor strncmp($item->name, '/', 1) === 0;
            });
        }
        
        
        $this->load($params);
        if ($this->validate()) {
            $search = strtolower(trim($this->globalSearch));
            $desc = strtolower(trim($this->globalSearch));
            $ruleName = $this->globalSearch;
            foreach ($items as $name => $item) {
                $f = (empty($search) || strpos(strtolower($item->name), $search) !== false) &&
                    (empty($desc) || strpos(strtolower($item->description), $desc) !== false) &&
                    (empty($ruleName) || $item->ruleName == $ruleName);
                if (!$f) {
                    unset($items[$name]);
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $items,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
    }
    
      public function searchGlobal($params)
    {
        /* @var \yii\rbac\Manager $authManager */
       $authManager = Yii::$app->getAuthManager();
        if ($this->type == Item::TYPE_ROLE) 
         {
              $user_role = getRoleByUserId(Yii::$app->user->identity->id);
              
              if(Yii::$app->user->can('superadmin'))
                  $items = $authManager->getRoles();
              elseif(Yii::$app->user->can('direction'))  
                    $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy"]);
              elseif(Yii::$app->user->can('manager'))  
                    $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy",'direction'=>"xy",]);
              else
                 $items = array_diff_key($authManager->getRoles(),['superadmin'=>"xy",'direction'=>"xy",'manager'=>"xy",$user_role=>"xy",]);   //retire role user ankour lan
          
          
           
           //array_diff_key($authManager->getRoles(),['direction'=>"xy"]) );
               	 // $items = $authManager->getRoles();
          } 
        else 
        {
        	 $items = array_filter($authManager->getPermissions(), function($item) {
                return $this->type == Item::TYPE_PERMISSION xor strncmp($item->name, '/', 1) === 0;
            });
            
           
        }
        
        
        $this->load($params);
        if ($this->validate()) {
            $search = strtolower(trim($this->globalSearch));
            $desc = strtolower(trim($this->globalSearch));
            $ruleName = $this->globalSearch;
            foreach ($items as $name => $item) {
                $f = (empty($search) || strpos(strtolower($item->name), $search) !== false) ||
                    (empty($desc) || strpos(strtolower($item->description), $desc) !== false) ||
                    (empty($ruleName) || $item->ruleName == $ruleName);
                if (!$f) {
                    unset($items[$name]);
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $items,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);
    }



}
