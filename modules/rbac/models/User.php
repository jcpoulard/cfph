<?php

namespace app\modules\rbac\models; 

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\rbac\components\Configs; 
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcContactInfo;


/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $person_id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property UserProfile $profile
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 10;

   

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Configs::instance()->userTable;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' =>self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
           
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);    //return NULL si user a pa aktif
    }


   public static function findIdentityOutStatus($id)
    {
        return static::findOne(['id' => $id, ]);  
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);    //return NULL si user a pa aktif
    }
    
    
    public static function findByPersonId($person_id)
    {
        return static::findOne(['person_id' => $person_id, 'status' => self::STATUS_ACTIVE]);    //return NULL si user a pa aktif
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                'password_reset_token' => $token,
                'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

  public function getFullNameByUserId($user_id)
    {
        $full_name = '';

        //$modelUser = User::findIdentityOutStatus($user_id);  //return NULL if not found
        

        //$modelUser = User::findIdentity($user_id);
        $modelUser = User::findOne($user_id); 

        if($modelUser->person_id==NULL)
            $full_name = $modelUser->person_id;//$modelUser->username;
        else
           { if($modelUser->is_parent==1) //c yon paran
               {  
               	$modelContact= new SrcContactInfo;
                 
                     $person=$modelContact->findOne($modelUser->person_id);
                 	
                 	$full_name = $person->contact_name;  
                 	  
               	  }
              else
                 {  $modelPerson= new SrcPersons;
                 
                     $person=$modelPerson->findOne($modelUser->person_id);
                 	
                 	$full_name = $person->getFullName(); 
                 	
                   }
           
            }
        
        
        return $full_name;
    }



    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getPersonId()
    {
        return $this->person_id;
    }
    
    /**
     * @inheritdoc
     */
    public function getIsParent()
    {
        return $this->is_parent;
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function getStatus()
    {
        return $this->status == 0 ? Yii::t('app','Inactive') : Yii::t('app','Active');
        
        
    }


    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
