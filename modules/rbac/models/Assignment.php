<?php

namespace app\modules\rbac\models; 

use Yii;
use yii\base\Object;
use app\modules\rbac\components\Helper;

/**
 * Description of Assignment
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 2.5
 */
class Assignment extends Object
{
    /**
     * @var integer User id
     */
    public $id;
    /**
     * @var \yii\web\IdentityInterface User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function __construct($id, $user = null, $config = array())
    {
        $this->id = $id;
        $this->user = $user;
        parent::__construct($config);
    }

    /**
     * Grands a roles from a user.
     * @param array $items
     * @return integer number of successful grand
     */
    public function assign($items)
    {
        $manager = Yii::$app->getAuthManager();
        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ? : $manager->getPermission($name);
                $manager->assign($item, $this->id);
                $success++;
            } catch (\Exception $exc) {
                Yii::error($exc->getMessage(), __METHOD__);
            }
        }
        Helper::invalidate();
        return $success;
    }

    /**
     * Revokes a roles from a user.
     * @param array $items
     * @return integer number of successful revoke
     */
    public function revoke($items)
    {
        $manager = Yii::$app->getAuthManager();
        $success = 0;
        foreach ($items as $name) {
            try {
                $item = $manager->getRole($name);
                $item = $item ? : $manager->getPermission($name);
                $manager->revoke($item, $this->id);
                $success++;
            } catch (\Exception $exc) {
                Yii::error($exc->getMessage(), __METHOD__);
            }
        }
        Helper::invalidate();
        return $success;
    }

    /**
     * Get all avaliable and assigned roles/permission
     * @return array
     */
    public function getItems()
    {
        $manager = Yii::$app->getAuthManager();
        $avaliable = [];
        
        $user_role = getRoleByUserId(Yii::$app->user->identity->id);
        
         
         
          if(Yii::$app->user->can('superadmin'))
               {
               	  $role_items = $manager->getRoles();
               }
              elseif(Yii::$app->user->can('direction'))
               {
                   $role_items = array_diff_key($manager->getRoles(),['superadmin'=>"xy"]);
                 }
                elseif(Yii::$app->user->can('manager'))
               {
                   $role_items = array_diff_key($manager->getRoles(),['superadmin'=>"xy",'admin'=>"xy",]);
                 }
               else
               {
               	     $role_items = array_diff_key($manager->getRoles(),['superadmin'=>"xy",'direction'=>"xy",'manager'=>"xy",$user_role=>"xy",]);
                 }
                 
        foreach (array_keys($role_items) as $name) 
          {
              $avaliable[$name] = 'role';
                 

            }

        foreach (array_keys($manager->getPermissions()) as $name) 
          {
	          if( (Yii::$app->user->can('superadmin'))||(Yii::$app->user->can('direction')) )
               {
               	    if ($name[0] != '/') 
		              {
		                $avaliable[$name] = 'permission';
		              }
		              
                 }
            
           }

        $assigned = [];
        foreach ($manager->getAssignments($this->id) as $item) 
          {
	        
                  $assigned[$item->roleName] = $avaliable[$item->roleName];
	               unset($avaliable[$item->roleName]);
	               
               
        
           }

        return[
            'avaliable' => $avaliable,
            'assigned' => $assigned
        ];
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if ($this->user) {
            return $this->user->$name;
        }
    }
    
    
    public function getRoleByUserId($user_id)
    {
        $sql='SELECT item_name FROM auth_assignment WHERE user_id='.$user_id ;

	    $result = Yii::$app->db->createCommand($sql)->queryAll();
           
         foreach($result as $r)
            return $r['item_name'];
 	
       
    } 
    
    
}
