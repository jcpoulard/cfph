<?php


namespace app\modules\rbac\controllers; 

use app\modules\rbac\components\ItemController; 
use app\modules\rbac\models\AuthItem;
use app\modules\rbac\models\searchs\AuthItem as AuthItemSearch;
use yii\rbac\Item;
use Yii;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;


/**
 * PermissionController implements the CRUD actions for AuthItem model.
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class PermissionController extends ItemController
{
   public $layout = "/inspinia";

    /**
     * @inheritdoc
     */
    public function labels()
    {
        return[
            'Item' => 'Permission',
            'Items' => 'Permissions',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return Item::TYPE_PERMISSION;
    }
    
    public function actionCreate()
    {
        
        if(Yii::$app->user->can("rbac-permission-create")){
            
            $model = new AuthItem(null);
            $model->type = $this->type;
            if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
                
                return $this->redirect(['view', 'id' => $model->name,'wh'=>'per']);
            } else {
                return $this->render('create', ['model' => $model]);
            }
        }
        else
        {
            
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }
          }
    }
    
    public function actionIndex()
    {
        if(Yii::$app->user->can("rbac-permission-index")){
         $searchModel = new AuthItemSearch(['type' => $this->type]);
         $dataProvider = $searchModel->searchGlobal(Yii::$app->request->getQueryParams());
           
           if (isset($_GET['pageSize'])) 
		         	 {
				        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
				          unset($_GET['pageSize']);
					   }

          return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            
          ]);
        }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }
        }
    }
    
     public function actionView($id)
    {
         if(Yii::$app->user->can("rbac-permission-view")){
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
         }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }
        }
    }
     
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can("rbac-permission-update")){
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            
            return $this->redirect(['view', 'id' => $model->name,'wh'=>'per']);
        }

        return $this->render('update', ['model' => $model]);
        }else{
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              return $this->redirect(Yii::$app->request->referrer);
               }
        }
    }
}
