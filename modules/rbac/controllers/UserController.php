<?php


namespace app\modules\rbac\controllers; 

use Yii;
use yii\helpers\Html;
use app\modules\rbac\models\form\PasswordResetRequest;
use app\modules\rbac\models\form\ResetPassword;
use app\modules\rbac\models\form\Signup;
use app\modules\rbac\models\form\ChangePassword;
use app\modules\rbac\models\form\PasswordReset;
use app\modules\rbac\models\form\Login; 
use app\modules\rbac\models\User;
use app\modules\rbac\models\searchs\User as UserSearch;
use app\modules\rbac\models\searchs\Assignment as AssignmentSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\base\UserException;
use yii\mail\BaseMailer;

use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcScholarshipHolder;

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * User controller
 */
class UserController extends Controller
{
    private $_oldMailPath;
    
     public $message=false;
	 
	 
	  public $full_scholarship=false;
	  public $internal;
          
          public $username_liste_select=1;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['signup', 'reset-password', 'login', 'request-password-reset'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout', 'change-password', 'index', 'view', 'delete', 'activate'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                    'activate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->has('mailer') && ($mailer = Yii::$app->getMailer()) instanceof BaseMailer) {
                /* @var $mailer BaseMailer */
                $this->_oldMailPath = $mailer->getViewPath();
                $mailer->setViewPath('@mdm/admin/mail');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($this->_oldMailPath !== null) {
            Yii::$app->getMailer()->setViewPath($this->_oldMailPath);
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all Active User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('rbac-user-index'))
         {
             ini_set("memory_limit", "-1");
        set_time_limit(0);
        
         	  $searchModel = new UserSearch();
		        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		        				   
			   return $this->render('index', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    
    public function actionUsernamelist()
    {
        if(Yii::$app->user->can('rbac-user-usernamelist'))
         {
         	  $searchModel = new UserSearch();
		  
                if( (Yii::$app->user->can("Administration études"))|| (Yii::$app->user->can('Administration recettes')) )
                  {
                      //students  
                     $dataProvider = $searchModel->searchStudentUserForList();
                      $for='stud';
                  
                  }
                elseif( (Yii::$app->user->can("superadmin")) ||(Yii::$app->user->can("direction")) ||(Yii::$app->user->can("Administration économat")) )
                 {
                    if(isset($_POST['User']['username_list']))
		       $this->username_liste_select = $_POST['User']['username_list'];
                    
                      $searchModel->username_list = $this->username_liste_select;
                   
                      if($this->username_liste_select==0)
                        {
                         //staff
                         $dataProvider = $searchModel->searchStaffUserForList();
                          $for='staff';
                        }
                      elseif($this->username_liste_select==1)
                        {
                          //students  
                            $dataProvider = $searchModel->searchStudentUserForList();
                             $for='stud';
                      
                         }
                    
                  }
		        				   
			   return $this->render('usernamelist', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
                               'for'=>$for,
                               'username_liste'=>$this->username_liste_select
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

 /**
     * Lists all Inactive User models.
     * @return mixed
     */
    public function actionUsersdisabled()
    {
        if(Yii::$app->user->can('rbac-user-usersdisabled'))
         {
         	  $searchModel = new UserSearch();
		        $dataProvider = $searchModel->searchUsersDisabled(Yii::$app->request->queryParams);
		
		         if (isset($_GET['pageSize'])) 
				         	 {
						        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
						          unset($_GET['pageSize']);
							   }
							   
			   return $this->render('index0', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('rbac-user-view'))
         {
					 return $this->render('view', [
		                'model' => $this->findModel($id),
		        ]);
		     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('rbac-user-delete'))
         {
         	try{
         		 $user_id = $id;
         		 
				  $modelUser = $this->findModel($id);
				  
				   $person_id = $modelUser->person_id;
			        
			        $modelUser->delete();
			        
			        //retire tout role user sa
			           
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                       //return item_name in array  
                                                $items =  $modelUserAssign->searchByUserid($user_id);	
                          
						          	   if($items!=null)
						          	     {
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $modelAssignment->revoke($items);
						          	       
						          	     }
						  
			              
		        return $this->redirect(['index','wh'=>'use']);
	        
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
			
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
       
       $this->layout = "/login";
       
       $modelPers = new SrcPersons();
       
       $user_profil = -1;
       $student_parent_guest=0;
       
        if (!Yii::$app->getUser()->isGuest) {
        	return $this->goHome();
        }

        $model = new Login();
        if($model->load(Yii::$app->getRequest()->post()) && $model->login()) 
          {
              $modelAcad = new SrcAcademicperiods;
              $current_acad=$modelAcad->searchCurrentAcademicYear();
                   
                  if( $current_acad['date_end']>=date('Y-m-d') )
                    {  
                    	//set current academic variable session
					   Yii::$app->session['currentId_academic_year']=$current_acad['id'];
					   Yii::$app->session['currentName_academic_year']=$current_acad['name_period'];
					   
					    $person_id = 0;
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id==NULL)
				           {  $user_profil = 0;
				              $person_id = 0;
                                              
                                              
				           }
				        else
				           { if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
                                                    
                                                    //gade si se yon elev
                                                   $modelPers = SrcPersons::findOne($person_id);
				                      if($modelPers->is_student==1)
                                                          $student_parent_guest=1;
				                  }
                                              elseif( ($modelUser->is_parent==1) ) //c yon paran
                                                 {
                                                  
                                                    $student_parent_guest=1;
                                                 }
				           
				            }
				        

					   //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
                        $user_profil = $modelPers->isEmployeeOrTeacher($person_id, $current_acad['id']);
                    	
                    	Yii::$app->session['user_profil'] = $user_profil;
                    	
                    	if( ($user_profil==-1) || ($user_profil==0) || ($user_profil==2) )
                    	   Yii::$app->session['profil_as'] =0;
                        elseif( ($user_profil==1) )
                    	   Yii::$app->session['profil_as'] =1;	 
                    	   
                       
                        
					  
		//&&&&&&&&&&&&&&&&   calcule pour automatiser "balance a payer"
					     $model_fees= new SrcFees;
					     $fee_academic_period = 0;
					     $data_fees_datelimit = $model_fees->checkDateLimitPayment( date('Y-m-d'), $current_acad['id'] );//date_du_jour >= date_limt_payment AND checked=0
		
					     if(isset($data_fees_datelimit)&&($data_fees_datelimit!=null))
					       { 
						   
					       	 $data_fees_datelimit=$data_fees_datelimit->getModels();
					       	 
					       	 foreach($data_fees_datelimit as $date_limit)
					       	   {  
					       	   	$fee_name = NULL;
						       	
						       	
						       	 $fee_period_id=$date_limit->id;
						         $program=$date_limit->program;
						         $level=$date_limit->level;
						         $amount1=$date_limit->amount;
						         $amount=$date_limit->amount;
								 $amount_to_pay = $date_limit->amount;
								 $fee_academic_period = $date_limit->academic_period;
	                          
	                          if(isset($date_limit->fee0->fee_label))
	                             $fee_name = $date_limit->fee0->fee_label;
	                            
                             if($fee_name!='Pending balance')
                             	{
                             	  $pass=false;
						         
						         $fee_status = $model_fees->getFeeStatus($fee_period_id); 
  
                                  
						        $modelPers = new SrcPersons();					         
					      $personsBillings = $modelPers->getStudentsForBillings($fee_period_id,$program,$level);
	 				      

					           if(isset($personsBillings)&&($personsBillings!=null))
							     {   
							     	    $personsBillings=$personsBillings->getModels();
							     	    $modelBillings= new SrcBillings; 
							     	    
							     	     $modelCurrency = getDefaultCurrency(); 
							     	     if($modelCurrency!=null)
							     	         $currency_symbol = $modelCurrency->id; 
							     	     else
							     	        $currency_symbol = null;
							     	    
							     	foreach($personsBillings as $stud)
							           {  $percentage_pay = 0;
							           	 
							           	  $this->full_scholarship=false;
										  $porcentage_level = 0; //mwens ke 100%
	                                  
                                         $modelFee = SrcFees::findOne($fee_period_id);
                                         //check if student is scholarship holder
                                          
                                         $currency_symbol = $modelFee->devise0->id;
                                         
											$model_scholarship = $modelPers->getIsScholarshipHolder($stud->id,$current_acad['id']);    
														           	  
														           	  $konte =0;
																	  $amount = 0;	
																		$percentage_pay = 0;
																	$partner_repondant = NULL;																			
																	  $premye_fee = NULL;
																			        if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																							   $modelScholarshipH = new SrcScholarshipHolder();
																							   $notNullFee = $modelScholarshipH->feeNotNullByStudentId($stud->id,$current_acad['id']);
																			           	      $full=1;
																			           	      
																						   if($notNullFee!=NULL)
																							{																								
																							  foreach($notNullFee as $scholarship)
																			           	    	{ $konte++;
																			           	    	  
																			           	    	  if(isset($scholarship->fee) )
																			           	    	   {
																			           	    	     	$full=0;
																			           	    	   	
																			           	    	     if($scholarship->fee == $modelFee->id)
																			           	    	      { 
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																										$partner_repondant = $scholarship->partner;
																		           	                    
																									   if(($partner_repondant==NULL))
																									    {	$amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																		           	                    
																											 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																									     
																										  }
																										 else
																											  $amount = $modelFee->amount;
																			           	    	        }
																			           	    	    }
																			           	    	  else
																			           	    	    {
																			           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	       	        $partner_repondant = $scholarship->partner;
																			           	    	      }
																								   
																			           	    	 }
																			           	    	 
																			           	     if($full==1)
																			           	       {
																			           	       	 if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 $partner_repondant = $scholarship->partner;
																											  }
																			           	       	 
																			           	       	 }
																								 
																								if((($konte>0)&&($amount==0))&&(!$this->full_scholarship))
																				           	    	  $amount = $modelFee->amount;
																							}
																						   else
																						     {
																								 // $this->full_scholarship = true;
																								 
																								 //fee ka NULL tou
																								  $check_partner=$modelScholarshipH->getScholarshipPartnerByStudentIdFee($stud->id,NULL,$current_acad['id']);
																								  //$porcentage_level = 1; //100%
																								  //$percentage_pay=100;
																								  
																								   if($check_partner!=NULL)
																								    {  
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								     }
																								   
																								       if(($percentage_pay==100))
																											  { $this->full_scholarship = true;
																										          $porcentage_level = 1; //100%
																												   $this->internal=1;
																												 
																											  }
																								       else
																								       {			  

																								   if(($partner_repondant==NULL))
																								      {  $amount = $modelFee->amount - (($modelFee->amount * $percentage_pay)/100) ;
																								         $this->internal=1;
																									  }
																								   else
																								      $amount = $modelFee->amount;
																								       }
																							 }
																			           	    	 
																			           	    
																			           	 }
																			           elseif($model_scholarship == 0)   //se pa yon bousye
																					     $amount = $modelFee->amount;
																					   
                                        
										
							           	  
							        //billings record for each stud
							           	   unset($modelBillings); 
							           	   $modelBillings= new SrcBillings;
							           	   
							           	   $ok=0;
							       
							     if(($this->full_scholarship)&&($partner_repondant==NULL)) //nap record le l full e se pa lekol la ki repondan, poul pa fose etafinansye a
									    { 
										   
										    if($porcentage_level == 0) //mwens ke 100%
											    {	 
													$modelBillings->setAttribute('academic_year',$current_acad['id']);
													 
													   
													$modelBillings->setAttribute('student',$stud->id);
													$modelBillings->setAttribute('fee_period',$fee_period_id);
													
													$modelBillings->setAttribute('amount_to_pay',$amount); 
													$modelBillings->setAttribute('amount_pay',0); 
													$modelBillings->setAttribute('date_pay',date('Y-m-d'));
													$modelBillings->setAttribute('comments', Yii::t('app','Full scholarship holder'));
													$modelBillings->setAttribute('balance', $amount);
													$modelBillings->setAttribute('created_by', "SIGES");
													$modelBillings->setAttribute('date_created', date('Y-m-d')); 
													
													$ok=1;
												}
									       
																				       	
							       	  }
								   else 
									  { 	  
							           	 	$modelBillings->setAttribute('academic_year',$current_acad['id']);
													
											
							           	 	$modelBillings->setAttribute('student',$stud->id);
											$modelBillings->setAttribute('fee_period',$fee_period_id);
											$modelBillings->setAttribute('amount_to_pay',$amount); 
											$modelBillings->setAttribute('amount_pay',0); 
											$modelBillings->setAttribute('balance', $amount);
											$modelBillings->setAttribute('created_by', "SIGES");
											$modelBillings->setAttribute('date_created', date('Y-m-d')); 
											
											$ok=1;
							           }
							           
							   if($ok==1)
							     {
							     	$ok=0;
  
					                if($modelBillings->save())
					                   {           
					                         
							           	   //balance record for each stud
							           	   $modelBalance_= new SrcBalance;
							           	   
							           	   $modelBalance_=SrcBalance::find()->where('student='.$stud->id.' AND devise='.$currency_symbol)->all();
									     							           	  
							           	  if(isset($modelBalance_)&&($modelBalance_!=null))
							           	    {  //update this model
							           	      foreach($modelBalance_ as $modelBalance)
							           	      {	
							           	    	if(($this->full_scholarship)&&($partner_repondant==NULL))
												  { 
											        if($porcentage_level==0) //mwens ke 100%
         											  $balance1=$modelBalance->balance + $amount;
												  
												  }
							                    else
							                       $balance1=$modelBalance->balance + $amount;
							                       
							           	    	 $modelBalance->setAttribute('balance',$balance1);
							           	    	 
							           	    	   if($modelBalance->save())
							           	               $pass=true;
							           	               
							           	        }
							           	    }
							           	  else
							           	    { //create new model
							           	          unset($modelBalance); 
					                              $modelBalance= new SrcBalance;
					                              
												  if( (($this->full_scholarship)&&($partner_repondant!=NULL)) || (!$this->full_scholarship) )
					                               {  $modelBalance->setAttribute('balance',$amount); 
							                          $modelBalance->setAttribute('devise',$currency_symbol);
														  $modelBalance->setAttribute('student',$stud->id);
														  $modelBalance->setAttribute('date_created',date('Y-m-d'));
														  
														  if($modelBalance->save())
																$pass=true;
													}
							           	     
							           	    }
							           	  
					                    } 
					                   
					                   
							           }
					                    
							           
							           	    
							            }//fen foreach
							            
							     }//fen if(isset($personsBillings)&&($personsBillings!=null))
							     
							    if($pass) 
							      {   //udate this fees model, checked=1
						       	     $modelFees=SrcFees::findOne($date_limit->id);
						       	  
						       	      $modelFees->setAttribute('checked',1);
						             $modelFees->save();
						             
							      }

					       	   }//fen sil pa "pending balance"
							     					           
					           }//fen foreach($data_fees_datelimit as $date_limit)
					           
					        }//fen if(isset($data_fees_datelimit)&&($data_fees_datelimit!=null))
					      
					      //delete balance kote balanse 0 yo
					      $command = Yii::$app->db->createCommand();
						  $command->delete('balance', ['balance'=>0])
                                   ->execute();  

					   
					   
					    //CURRENCY --  (default)
					    //Extract devise name and symbol 
					    $currency_name_symbol = defaultCurrency();
					    
					    $explode_currency_name_symbol= explode("/",substr($currency_name_symbol, 0));
					    
					    $currency_name = $explode_currency_name_symbol[0];
					    $currency_symbol = $explode_currency_name_symbol[1];
					    $currency_id = $explode_currency_name_symbol[2];
					    Yii::$app->session['currencyName']=$currency_name;
					    Yii::$app->session['currencySymbol']=$currency_symbol;
					    Yii::$app->session['currencyId']=$currency_id;
					   
                     
                     
                   if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))  )
               	     {
               	         return $this->redirect('../../reports/report/dashboardpedago');//student'); //$this->goBack();
               	         
               	     }
                   elseif( (Yii::$app->user->can("Administration économat"))  )
               	     {
               	         return $this->redirect('../../reports/report/dashboardbilling');//student'); //$this->goBack();
               	         
               	     }
                   elseif( (Yii::$app->user->can("Administration recettes"))  )
               	     {
               	         return $this->redirect('../../reports/report/dashboardpedago');//return $this->redirect('../../billings/billings/index?ri=0&part=rec');//student'); //$this->goBack();
               	         
               	     }
                   elseif( (Yii::$app->user->can("Administration dépenses"))  )
               	     {
               	         return $this->redirect('../../billings/chargepaid/index?di=2&from=b');//student'); //$this->goBack();
               	         
               	     }
                   elseif( (Yii::$app->user->can("Magasinier manager"))||(Yii::$app->user->can("Magasinier"))  )
               	     {
               	         return $this->redirect('../../stockrooms/stockroomhasproducts/dashboard?wh=set_str');//student'); //$this->goBack();
               	         
               	     }
                   elseif( (Yii::$app->user->can("teacher"))  )
               	     {
               	         return $this->redirect('../../reports/report/dashboardpedago'); //$this->goBack();
               	         
               	     }
                   elseif(Yii::$app->user->can("guest")){
                       //si se paran ou elev
                       if($student_parent_guest==1)
                         {  return $this->redirect('../../guest/academic/academic');
                         
                         }
                       elseif( Yii::$app->user->can("portal-menu") )
                        {
                          return $this->redirect('../../portal/default/manage-content?wh=pla_pv');
                          
                        }
                       else
                        { 
                             return $this->goHome();
                        }
                   }else
                        return $this->goHome(); 
                         
                         
                     }
                  else				
                   {    //$modelAcad = new AcademicPeriods;
						  //alert to create the new one
						if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) )
		                      return $this->redirect('../../fi/academicperiods/create?from=gol');
						 else
						   Yii::$app->getSession()->setFlash('Warning', 'Check your Admin to set the new academic period.');
						  
					 }

             
            
           } 
        else 
           {
              return $this->render('login', [
                    'model' => $model,
                ]);
           }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
       // Yii::$app->getUser()->logout();
         $last_Activity = '';
        $user_id = '';
		
		if(isset(Yii::$app->user->identity->id))
		 { $user_id = Yii::$app->user->identity->id;
		 
		    //return datetime (last_activity)
             $last_Activity = isUserConnected($user_id);
		 }
		else
		  $user_id = '';
		  
		 if(($last_Activity=='')||($last_Activity==NULL))
		  {
		
				unset(Yii::$app->session['currentId_academic_year']);
				unset(Yii::$app->session['currentName_academic_year']);
				
				unset(Yii::$app->session['user_profil']);
		        unset(Yii::$app->session['profil_as']);
		        unset(Yii::$app->session['currencyName']);
				unset(Yii::$app->session['currencySymbol']);
				unset(Yii::$app->session['currencyId']);

		         
		
	        }
	      else
	       { 
	         	//delete session user sa
	         	$result = Yii::$app->db->createCommand("DeLETE FROM session WHERE user_id=\"$user_id\" AND last_activity=\"$last_Activity\"")
              ->execute();
	         	
	         	unset(Yii::$app->session['currentId_academic_year']);
				unset(Yii::$app->session['currentName_academic_year']);
				
				unset(Yii::$app->session['user_profil']);
		        unset(Yii::$app->session['profil_as']);
		        unset(Yii::$app->session['currencyName']);
				unset(Yii::$app->session['currencySymbol']);
				unset(Yii::$app->session['currencyId']);
				
	             Yii::$app->getUser()->logout();
	         
	         }

        return $this->goHome();
    }

    /**
     * Signup new user
     * @return string
     */
    public function actionSignup()
    {
	     if(Yii::$app->user->can('rbac-user-signup'))
         {
              $model = new Signup();
	        if ($model->load(Yii::$app->getRequest()->post())) {
	            if ($user = $model->signup()) {
	                return $this->goHome();
	            }
	        }
	
	        return $this->render('signup', [
	                'model' => $model,
	        ]);
	       }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                 return $this->redirect(Yii::$app->request->referrer);
               }

          }     
	        
    }

public function actionViewonlineusers()
 {
 	
     if( (Yii::$app->user->can('rbac-user-viewonlineusers'))||(Yii::$app->user->can('superadmin')) )
         {
         	$user = new UserSearch();
            return $this->render('onlineusers',['model'=>$user]);
            
         }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }

          }
}
 

    /**
     * Request reset password
     * @return string
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequest();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPassword($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                'model' => $model,
        ]);
    }

    /**
     * Change password
     * @return string
     */
    public function actionChangepassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        return $this->render('change-password', [
                'model' => $model,
        ]);
    }
    
    
     /**
     * Reset password
     * @return string
     */
    public function actionPasswordreset()
    {
        $model = new PasswordReset();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->reset($_GET['id'], $model->newPassword)) 
        {
              if(Yii::$app->user->can('rbac-user-index'))
                  return $this->redirect(['/rbac/user/index?wh=use&']);
               elseif(Yii::$app->user->can('rbac-user-usernamelist'))
                   return $this->redirect(['/rbac/user/usernamelist?wh=usel&']);
                   else
                   return $this->goHome();
        }

        return $this->render('password_reset', [
                'model' => $model,
        ]);
    }


    /**
     * Change profil
     * @return string
     */
    public function actionChangeprofil()
    {
        Yii::$app->session['profil_as'] = $_GET['profil'];
        
        //$this->redirect(Yii::$app->request->referrer);
        //$this->redirect('../../../index.php');
        return $this->redirect('../../reports/report/dashboardpedago');
        
    }
    

    /**
     * Activate new user
     * @param integer $id
     * @return type
     * @throws UserException
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        /* @var $user User */
        if(Yii::$app->user->can('rbac-user-activate'))
         {
		   if(Yii::$app->user->identity->id!=$id)
		     {
		       $user = $this->findModel($id);
		        if ($user->status == User::STATUS_INACTIVE) {
		            $user->status = User::STATUS_ACTIVE;
		            if ($user->save()) 
		             {
		            	//enable associate person
		            	if($user->person_id!=NULL)
		            	  {  $modelPerson = SrcPersons::findOne($user->person_id);
		            	  	  
		            	  	  $modelPerson->active =1;
		            	  	   
		            	  	   $modelPerson->save();
		            	  	}
		            	
		                return $this->redirect('index?wh=use');
		            } else {
		                $errors = $user->firstErrors;
		                throw new UserException(reset($errors));
		            }
		        }elseif ($user->status == User::STATUS_ACTIVE) {
		            $user->status = User::STATUS_INACTIVE;
		            if ($user->save()) 
		             {
		             	//disable associate person
		             	if($user->person_id!=NULL)
		            	  {  $modelPerson = SrcPersons::findOne($user->person_id);
		            	  	  
		            	  	  $modelPerson->active =0;
		            	  	   
		            	  	   $modelPerson->save();
		            	  	}

		             	
		                return $this->redirect('index?wh=use');
		            } else {
		                $errors = $user->firstErrors;
		                throw new UserException(reset($errors));
		            }
		        }
		        
		      }
		    else 
		      {
		      	  Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You are not allowed to enable/disable yourself.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
		      	  return $this->redirect(Yii::$app->request->referrer);
                          
                      }
		      	
		      	
		        return $this->redirect('index?wh=use');
		        
		        
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                   return $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * Creer un utilisateur dans le système
     * 
     */
    public function actionCreate(){
       if(Yii::$app->user->can('rbac-user-create'))
         {
		    $model = new Signup();
		        if ($model->load(Yii::$app->getRequest()->post())) {
		            if ($user = $model->signup()) {
		                return $this->goHome();
		            }
		        }
		
		        return $this->render('create', [
		                'model' => $model,
		        ]);
		        
    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                 return $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }
    
}
