<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\rbac\components\Helper;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\User */

$this->title = $model->username;


$controllerId = $this->context->uniqueId . '/';

$action = 'index';

if(isset($_GET['wh'])&&($_GET['wh']=='use0') )
    $action = 'usersdisabled';
?>


<div class="row">
    <?= $this->render("//layouts/rbacLayout") ?>
</div>
<div class="row">
    <div class="col-lg-7">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
<p>
        <?php //echo Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id,'wh'=>'use'], ['class' => 'btn btn-success btn-sm']); ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), [$action, 'wh'=>$_GET['wh'] ], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
    
</div>

<div class="user-view">


    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            'created_at:date',
            'Status',
            
        ],
    ])
    ?>

</div>
