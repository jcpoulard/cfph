<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('app', 'Login');

$school_acronym = infoGeneralConfig('school_acronym');

?>
<div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div style="margin-bottom:-40px; ">

                <h1 class="logo-name"><img alt="<?= Yii::t('app', 'logo') ?>" style="width: 170px;" class="img-circle" src=" <?= Url::to("@web/img/Logo_Cana_Reel_G.gif") ?> " /></h1>

            </div>
            <h3><?= Yii::t('app', 'Welcome to ').$school_acronym ?></h3>
            <p><?= Html::encode($this->title) ?></p>
            
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            
            <form class="m-t" role="form" action="index.html">
                <div class="form-group">
                    <?= $form->field($model, 'username')->label(false)->textInput(['placeholder'=> Yii::t('app', 'Username'), 'class' => 'input form-control']) ?> <!-- <input type="email" class="form-control" placeholder="Username" required="">  -->
                </div>
                <div class="form-group">
                    <?= $form->field($model, 'password')->label(false)->passwordInput(['placeholder'=> Yii::t('app', 'Password'), 'class' => 'input form-control']) ?> <!-- <input type="password" class="form-control" placeholder="Password" required=""> -->
                </div>
                <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?> <!-- <button type="submit" class="btn btn-primary block full-width m-b">Login</button>  -->
<br/><br/>
               <?php // echo Html::a('Forgot password?', ['user/request-password-reset']); ?> <!--  <a href="#"><small>Forgot password?</small></a>  -->
      <!--           <p class="text-muted text-center"><small><?php // echo Yii::t('app', 'New user?')/ ?> ( <?php // echo Html::a('signup', ['user/signup']); ?> )</small></p>     -->
                 <!--  <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a>  -->
            </form>
            <p class="m-t"> <small><?= Yii::t('app', 'Powered by LOGIPAM') ?> &copy; 2017</small> </p>
            
             <?php ActiveForm::end(); ?>
             
        </div>
    </div>

    <!-- Mainly scripts 
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    -->
    
<br/>

