<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ChangePassword */

$this->title = Yii::t('app', 'Changing my password');

?>

 <div class="row">
        
        <div class="col-lg-5">
             <h3><?= $this->title; ?></h3>
        </div>
 </div>

<div class="wrapper wrapper-content password-change">


            <?php $form = ActiveForm::begin(['id' => 'form-change']); ?>
         <div class="row">
        <div class="col-lg-6">    
                <?= $form->field($model, 'oldPassword')->passwordInput() ?>
          
                
         </div>
    </div>
               
        <div class="row">
        <div class="col-lg-6">      
                <?= $form->field($model, 'newPassword')->passwordInput() ?>
        </div>
        
        <div class="col-lg-6">
             <?= $form->field($model, 'retypePassword')->passwordInput() ?>
              
          </div>
    </div>
            
  <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">

                    <?= Html::submitButton(Yii::t('app', 'Change'), ['class' => 'btn btn-primary', 'name' => 'change-button']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
            <?php ActiveForm::end(); ?>
       
</div>
