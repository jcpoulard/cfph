<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\rbac\components\Helper; 

use app\modules\fi\models\SrcPersons;
use app\modules\rbac\models\User;
use app\modules\rbac\models\searchs\User as UserSearch;

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */



$acad = Yii::$app->session['currentId_academic_year'];


$this->title = Yii::t('app','Username list');

?>



 
  <?= $this->render('//layouts/rbacLayout'); ?>
  
 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            
        
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 
<div class="wrapper wrapper-content user-index">
       
   <?php $form = ActiveForm::begin(); ?>
    
   <?php
   if( (Yii::$app->user->can("superadmin")) ||(Yii::$app->user->can("direction")) ||(Yii::$app->user->can("Administration économat")) )
       {
   ?>
    
   <div class="row">
             
             <div class="col-lg-2">
                 
                  <?php 
					 
					 
						
          
			 echo $form->field($searchModel, 'username_list')->widget(Select2::classname(), [
			                                   'data'=>array('1'=>Yii::t('app','Students'),'0'=>Yii::t('app','Staff') ),
			                                   'size' => Select2::MEDIUM,
			                                   'theme' => Select2::THEME_CLASSIC,
			                                   'language'=>'fr',
			                                   'options'=>[ //'placeholder'=>Yii::t('app', ' --  select username list  --'),
			                                                  'onchange'=>'submit()',
			                                           ],
			                                                
			                                   'pluginOptions'=>[
			                                         'allowclear'=>true,
			                                     ],
			                                   ])->label(false);			
						
					 
	           ?>  
             </div>   
   </div>
    
  <?php
  
         }
  
  ?>  
    <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Username'); ?></th>
         <?php  if($for=='stud') 
                     {
             
             ?>
				            <th><?= Yii::t('app','Program'); ?></th>
                                            <th><?= Yii::t('app','Shift'); ?></th>
                                            <th><?= Yii::t('app','Room'); ?></th>
                                            <th><?= Yii::t('app','Level'); ?></th>
               <?php
                     }
                  elseif($for=='staff') 
                     {
               ?>  
                                            <th><?= Yii::t('app','Title'); ?></th>
               <?php
                     }
                  
                     
                     if(Yii::$app->user->can('rbac-user-passwordreset')) 
                           {
               ?>  
                                            <th></th>
		<?php
                            }
                  ?>		            
				           </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataPersons = $dataProvider->getModels();
    
          foreach($dataPersons as $pers)
           {
               echo '  <tr >
                    <td > '.$pers->first_name.' </td>
                        <td > '.$pers->last_name.' </td>
                            <td > '.$pers->users[0]->username.' </td>';
              
               if($for=='stud') 
                  {
                 echo '
                        <td >'.$pers->studentOtherInfo0[0]->applyForProgram->short_name.' </td>
                            <td >'.$pers->studentOtherInfo0[0]->applyShift->shift_name.' </td>
                                 <td >';
                                     if( isset($pers->studentLevel->room)&&($pers->studentLevel->room!=0)  )
                                         echo $pers->studentLevel->room0->room_name;
                                     else
                                          echo '';
                                     
                               echo' </td>
                                   <td >';
                                     if( isset($pers->studentLevel->level)&&($pers->studentLevel->level!=0)  )
                                         echo $pers->studentLevel->level;
                                     else
                                          echo '';
                                     
                               echo' </td>';
                   }
                elseif($for=='staff') 
                  {  
                                    echo'<td >';
                                    
                                                 $modelPers = new SrcPersons();
                                              $poste = $modelPers->getTitles($pers->id, $acad);
                                     if( $poste!=''  )
                                         echo $poste;
                                     elseif( isset($pers->courses0->teacher)&&($pers->courses0->teacher!=0)  )
                                         echo Yii::t('app', 'Teacher');
                                         else
                                          echo '';
                                     
                               echo' </td>';
                    
                     }               
                     
                     if(Yii::$app->user->can('rbac-user-passwordreset')) 
                                                              {
                     echo'<td >';
                               $user_id='';
                               if(isset($pers->users[0]->id) )
                                  $user_id = $pers->users[0]->id;
				echo '&nbsp'.Html::a('<span class="fa fa-refresh"></span>', Yii::getAlias('@web').'/index.php/rbac/user/passwordreset?id='.$user_id.'&wh=use', [
                                    'title' => Yii::t('app', 'Password reset'),
                        ]); 
                                                              
                    
                   echo ' </td>';
                                                               }
                     echo '  </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>
				    
            
 <?php ActiveForm::end(); ?>
    
</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'User(s) Online List'},
                    {extend: 'pdf', title: 'User(s) Online List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>