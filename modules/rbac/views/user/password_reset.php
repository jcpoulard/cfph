<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\modules\rbac\models\User;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ChangePassword */

$this->title = Yii::t('app', 'Reset password');

$modelUser = new User;
$name = $modelUser->getFullNameByUserId($_GET['id']);
?>



 <div class="row">
        <div class="col-lg-1">
            <p>
            <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index','wh'=>'use'], ['class' => 'btn btn-info btn-sm']) ?>
            </p>
        </div>
        <div class="col-lg-5">
             <h3><?= $this->title.' '.$name; ?></h3>
        </div>
 </div>

<div class="wrapper wrapper-content password-reset">

            <?php $form = ActiveForm::begin(); ?>

               
  <div class="row">
        <div class="col-lg-4">      
                <?= $form->field($model, 'newPassword')->passwordInput() ?>
        </div>
        
        <div class="col-lg-4">
             <?= $form->field($model, 'retypePassword')->passwordInput() ?>
              
          </div>
    </div>
            
  <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">

                    <?= Html::submitButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-primary', 'name' => 'change-button']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>
            <?php ActiveForm::end(); ?>
       
</div>
