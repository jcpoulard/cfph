<?php

namespace app\modules\planning\models;

use Yii;
use app\modules\fi\models\Persons; 
use app\modules\fi\models\Courses; 
use app\modules\fi\models\Academicperiods; 
use app\modules\fi\models\Shifts; 

/**
 * This is the model class for table "student_attendance".
 *
 * @property string $id
 * @property integer $student
 * @property integer $course
 * @property integer $lesson
 * @property string $date_attendance
 * @property integer $attendance_status
 * @property integer $academic_year
 * @property string $notes
 * @property string $time_start
 * @property string $time_end
 * @property integer $shift
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * @property Persons $student0
 * @property Courses $course0
 * @property Events $lesson0
 * @property Academicperiods $academicYear
 * @property Shifts $shift0
 */
class StudentAttendance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student_attendance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student', 'course', 'lesson', 'date_attendance', 'attendance_status', 'academic_year', 'shift'], 'required'],
            [['student', 'course', 'lesson', 'attendance_status', 'academic_year', 'shift'], 'integer'],
            [['date_attendance', 'time_start', 'time_end', 'create_date', 'update_date'], 'safe'],
             [['student','course','lesson'],'unique','targetAttribute'=>['student','course','lesson'],'message'=>Yii::t('app','Attendance already take for the lesson !')],
            [['notes'], 'string'],
            [['create_by', 'update_by'], 'string', 'max' => 64],
            [['student'], 'exist', 'skipOnError' => true, 'targetClass' => Persons::className(), 'targetAttribute' => ['student' => 'id']],
            [['course'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course' => 'id']],
            [['lesson'], 'exist', 'skipOnError' => true, 'targetClass' => Events::className(), 'targetAttribute' => ['lesson' => 'id']],
            [['academic_year'], 'exist', 'skipOnError' => true, 'targetClass' => Academicperiods::className(), 'targetAttribute' => ['academic_year' => 'id']],
            [['shift'], 'exist', 'skipOnError' => true, 'targetClass' => Shifts::className(), 'targetAttribute' => ['shift' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'student' => Yii::t('app', 'Student'),
            'course' => Yii::t('app', 'Course'),
            'lesson' => Yii::t('app', 'Lesson'),
            'date_attendance' => Yii::t('app', 'Date Attendance'),
            'attendance_status' => Yii::t('app', 'Attendance Status'),
            'academic_year' => Yii::t('app', 'Academic Year'),
            'notes' => Yii::t('app', 'Notes'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'shift' => Yii::t('app', 'Shift'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent0()
    {
        return $this->hasOne(Persons::className(), ['id' => 'student']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse0()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson0()
    {
        return $this->hasOne(Events::className(), ['id' => 'lesson']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(Academicperiods::className(), ['id' => 'academic_year']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShift0()
    {
        return $this->hasOne(Shifts::className(), ['id' => 'shift']);
    }
}
