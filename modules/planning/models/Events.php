<?php

namespace app\modules\planning\models;

use Yii;
use app\modules\fi\models\Courses; 
use app\modules\fi\models\Rooms;


/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property integer $course
 * @property string $course_date_start
 * @property string $course_date_end
 * @property integer $weekday
 * @property string $description
 * @property string $date_start
 * @property string $date_end
 * @property string $time_start
 * @property string $time_end
 * @property integer $repeat_each_day
 * @property string $color
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * @property Courses $course0
 */
class Events extends \yii\db\ActiveRecord
{
    public $hour_start; 
    public $hour_end; 
    public $minute_start; 
    public $minute_end;
    
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course', 'weekday', 'repeat_each_day','lesson_complete'], 'integer'],
            [['course_date_start', 'course_date_end','date_start','weekday','course', 'time_start','time_end'], 'required'],
            //[['time_end','time_start'],'time'],
            ['course_date_end','compare','compareAttribute'=>'course_date_start','operator'=>'>=','message'=>Yii::t('app','Date course end must be after date course start !')],
            //['time_end','compare','compareAttribute'=>'time_start','operator'=>'>=','message'=>Yii::t('app','Time end must be after time start !')],
            [['course_date_start', 'course_date_end','date_start','weekday','create_by', 'date_end', 'time_start', 'time_end', 'create_date', 'update_date'], 'safe'],
            [['description'], 'string'],
            [['color', 'create_by', 'update_by'], 'string', 'max' => 32],
            [['course'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'course' => Yii::t('app', 'Course'),
            'course_date_start' => Yii::t('app', 'Course Date Start'),
            'course_date_end' => Yii::t('app', 'Course Date End'),
            'weekday' => Yii::t('app', 'Weekday'),
            'description' => Yii::t('app', 'Description'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'repeat_each_day' => Yii::t('app', 'Repeat Each Day'),
            'color' => Yii::t('app', 'Color'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse0()
    {
        return $this->hasOne(Courses::className(), ['id' => 'course']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom0()
    {
        return $this->hasOne(Rooms::className(), ['id' => 'room']);
    }
    
    
}
