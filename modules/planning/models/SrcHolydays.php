<?php

namespace app\modules\planning\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\planning\models\Holydays;

/**
 * SrcHolydays represents the model behind the search form about `app\modules\planning\models\Holydays`.
 */
class SrcHolydays extends Holydays
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_no_planning', 'all_day', 'repeat_each_day'], 'integer'],
            [['name', 'description', 'description_no_planning', 'date_start', 'date_end', 'color', 'create_date', 'update_date', 'create_by', 'update_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Holydays::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_no_planning' => $this->is_no_planning,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'all_day' => $this->all_day,
            'repeat_each_day' => $this->repeat_each_day,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'description_no_planning', $this->description_no_planning])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
