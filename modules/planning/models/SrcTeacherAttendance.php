<?php

namespace app\modules\planning\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\planning\models\TeacherAttendance;
use app\modules\planning\models\SrcStudentAttendance; 

/**
 * SrcTeacherAttendance represents the model behind the search form about `app\modules\planning\models\TeacherAttendance`.
 */
class SrcTeacherAttendance extends TeacherAttendance
{
    public $program; 
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'teacher','program', 'course', 'lesson', 'attendance_status', 'academic_year', 'shift'], 'integer'],
            
            [['date_attendance', 'time_arrival', 'notes', 'time_start', 'time_end', 'create_by', 'create_date', 'update_by', 'update_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeacherAttendance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'teacher' => $this->teacher,
            'course' => $this->course,
            'lesson' => $this->lesson,
            'date_attendance' => $this->date_attendance,
            'time_arrival' => $this->time_arrival,
            'attendance_status' => $this->attendance_status,
            'academic_year' => $this->academic_year,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
            'shift' => $this->shift,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    public function getAttendanceByDay($teacher, $day, $month, $year){
        $date = strtotime($year.'-'.$month.'-'.$day);
        $date_sql = $year.'-'.$month.'-'.$day; 
        $sql = "SELECT id, attendance_status FROM teacher_attendance WHERE teacher = $teacher AND date_attendance = '$date_sql'";
        $data = SrcTeacherAttendance::findBySql($sql)->all(); 
        $string_retour = "";
        
        foreach($data as $d){
            if($d->attendance_status == 0){
               
                $string_retour.="<span title='".Yii::t('app','Present')."' class='prezans' data-idprezans='".$d->id."' style='color: #0000FF'>".Yii::t('app','P'). " </span>";
            }
            if($d->attendance_status == 1 || $d->attendance_status == 2){
                if($d->attendance_status==1)
                     $string_retour.="<span title='".Yii::t('app','Non motivated absence')."' class='prezans' data-idprezans='".$d->id."' style='color: #FF0000'>".Yii::t('app','A'). " </span>";
                else
                    $string_retour.="<span title='".Yii::t('app','Motivated absence')."' class='prezans' data-idprezans='".$d->id."' style='color: #FF0000'>".Yii::t('app','A'). " </span>";
               }
            if($d->attendance_status == 3 || $d->attendance_status ==4){
                if($d->attendance_status==3)
                     $string_retour.="<span title='".Yii::t('app','Non motivated late')."' class='prezans' data-idprezans='".$d->id."' style='color: #000000'>".Yii::t('app','R'). " </span>";
                else
                    $string_retour.="<span title='".Yii::t('app','Motivated late')."' class='prezans' data-idprezans='".$d->id."' style='color: #000000'>".Yii::t('app','R'). " </span>";
                
            }
        }
        return $string_retour;
    }
    
    public function getAttendanceStudByDay($student, $day, $month, $year){
       // $date = strtotime($year.'-'.$month.'-'.$day);
        $date_sql = $year.'-'.$month.'-'.$day; 
        $sql = "SELECT id, attendance_status FROM student_attendance WHERE student = $student AND date_attendance = '$date_sql'";
        $data = SrcStudentAttendance::findBySql($sql)->all(); 
        $string_retour = "";
        
        foreach($data as $d){
            if($d->attendance_status == 0){
               
                $string_retour.="<span title='".Yii::t('app','Present')."' class='prezans' data-idprezans='".$d->id."' style='color: #0000FF'>".Yii::t('app','P'). " </span>";
            }
            if($d->attendance_status == 1 || $d->attendance_status == 2){
                if($d->attendance_status==1)
                     $string_retour.="<span title='".Yii::t('app','Non motivated absence')."' class='prezans' data-idprezans='".$d->id."' style='color: #FF0000'>".Yii::t('app','A'). " </span>";
                else
                    $string_retour.="<span title='".Yii::t('app','Motivated absence')."' class='prezans' data-idprezans='".$d->id."' style='color: #FF0000'>".Yii::t('app','A'). " </span>";
               }
            if($d->attendance_status == 3 || $d->attendance_status ==4){
                if($d->attendance_status==3)
                     $string_retour.="<span title='".Yii::t('app','Non motivated late')."' class='prezans' data-idprezans='".$d->id."' style='color: #000000'>".Yii::t('app','R'). " </span>";
                else
                    $string_retour.="<span title='".Yii::t('app','Motivated late')."' class='prezans' data-idprezans='".$d->id."' style='color: #000000'>".Yii::t('app','R'). " </span>";
                
            }
        }
        return $string_retour;
    }
    
    public function getCountStudentAttendance($id_student,$acad,$status, $month){
        $sql_str = "SELECT student FROM student_attendance WHERE student = $id_student AND academic_year = $acad AND attendance_status IN $status AND MONTH(date_attendance) = $month";
        $count_stud = SrcStudentAttendance::findBySql($sql_str)->count(); 
        return $count_stud; 
    }
    
    public function getCountTeacherAttendance($id_student,$acad,$status,$month){
        $sql_str = "SELECT teacher FROM teacher_attendance WHERE teacher = $id_student AND academic_year = $acad AND attendance_status IN $status AND MONTH(date_attendance) = $month";
        $count_teach = SrcTeacherAttendance::findBySql($sql_str)->count(); 
        return $count_teach; 
    }
    
    
}
