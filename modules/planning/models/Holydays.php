<?php

namespace app\modules\planning\models;

use Yii;

/**
 * This is the model class for table "holydays".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $is_no_planning
 * @property string $description_no_planning
 * @property string $date_start
 * @property string $date_end
 * @property integer $all_day
 * @property string $color
 * @property integer $repeat_each_day
 * @property string $create_date
 * @property string $update_date
 * @property string $create_by
 * @property string $update_by
 */
class Holydays extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'holydays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date_start', 'date_end'], 'required'],
            [['description', 'description_no_planning'], 'string'],
            [['is_no_planning', 'all_day', 'repeat_each_day'], 'integer'],
            [['date_start', 'date_end', 'create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['color', 'create_by', 'update_by'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'is_no_planning' => Yii::t('app', 'Is No Planning'),
            'description_no_planning' => Yii::t('app', 'Description No Planning'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'all_day' => Yii::t('app', 'All Day'),
            'color' => Yii::t('app', 'Color'),
            'repeat_each_day' => Yii::t('app', 'Repeat Each Day'),
            'create_date' => Yii::t('app', 'Create Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
