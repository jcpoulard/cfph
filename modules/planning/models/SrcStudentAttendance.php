<?php

namespace app\modules\planning\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\planning\models\StudentAttendance;

/**
 * SrcStudentAttendance represents the model behind the search form about `app\modules\planning\models\StudentAttendance`.
 */
class SrcStudentAttendance extends StudentAttendance
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'student', 'course', 'lesson', 'attendance_status', 'academic_year', 'shift'], 'integer'],
            [['date_attendance', 'notes', 'time_start', 'time_end', 'create_by', 'update_by', 'create_date', 'update_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = StudentAttendance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'student' => $this->student,
            'course' => $this->course,
            'lesson' => $this->lesson,
            'date_attendance' => $this->date_attendance,
            'attendance_status' => $this->attendance_status,
            'academic_year' => $this->academic_year,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
            'shift' => $this->shift,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    
    
    
    
}
