<?php

namespace app\modules\planning\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\planning\models\Events;
use app\modules\fi\models\SrcCourses; 

/**
 * SrcEvents represents the model behind the search form about `app\modules\planning\models\Events`.
 */
class SrcEvents extends Events
{
    public $globalSearch;
    public $total_hour;
    public $jour; 
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'course', 'repeat_each_day','lesson_complete'], 'integer'],
            
            [['description', 'date_start', 'date_end', 'color', 'create_by', 'update_by', 'create_date', 'update_date'], 'safe'],
        ];
    }
    
   

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'course' => $this->course,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'repeat_each_day' => $this->repeat_each_day,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    
    
    public function searchGlobal($params)
    {     
        //$this->globalSearch = $params;
        $this->load($params);
        $sql = "SELECT e.course, m.code, m.duration, e.date_end, s.subject_name, e.course_date_start, e.course_date_end, TIME_TO_SEC(SUM(UNIX_TIMESTAMP(time_end) - UNIX_TIMESTAMP(time_start))/1000) as total_hour FROM events e INNER JOIN courses c ON (c.id = e.course) 
    INNER JOIN module m ON (m.id = c.module)
    INNER JOIN subjects s ON (s.id = m.subject)
    group by e.course HAVING e.date_end <= NOW()";    
    $query = SrcEvents::findBySql($sql);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
            'pageSize' => Yii::$app->session->get('pageSize',Yii::$app->params['defaultPageSize']),
        ],
    ]);

    

    if (!$this->validate()) {
        // uncomment the following line if you do not want to any records when validation fails
        // $query->where('0=1');
        return $dataProvider;
    }
   
    $query->joinWith(['course0', 'course0.module0.subject0', 'course0.teacher0',]);
    
    $query->orFilterWhere(['like', 'Subjects.subject_name', $this->globalSearch])
           ->orFilterWhere(['like', 'Subjects.short_subject_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.first_name', $this->globalSearch])
            ->orFilterWhere(['like', 'Persons.last_name', $this->globalSearch]);
    
    return $dataProvider;
    }
    /*
     * 
     */
    public function getDayCourse($course){
        $sql = "SELECT distinct(weekday) as jour FROM `events` where course = $course"; 
        $data = SrcEvents::findBySql($sql)->all(); 
        $day_value = [];
        $day_string = ""; 
        $j = 0;
        foreach($data as $d){
            switch($d->jour){
                case 0:
                    $day_value[$j] = Yii::t('app','Sun');
                    break; 
                case 1: 
                    $day_value[$j] = Yii::t('app','Mon');
                    break; 
                case 2: 
                    $day_value[$j] = Yii::t('app','Tue');
                    break; 
                case 3: 
                    $day_value[$j] = Yii::t('app','Wed');
                    break; 
                case 4:
                    $day_value[$j] = Yii::t('app','Thu');
                    break; 
                case 5: 
                    $day_value[$j] = Yii::t('app','Fri');
                    break; 
                case 6: 
                    $day_value[$j] = Yii::t('app','Sat');
                    break; 
            }
            $j++; 
        }
        for($i=0; $i<count($day_value); $i++){
         $day_string = $day_string . " ".$day_value[$i];
        }
        return $day_string; 
    }
    
    public function getDateStart($course){
        $sql = "SELECT course_date_start FROM `events` where course = $course"; 
        $data = SrcEvents::findBySql($sql)->all();
        $date_start = null;
        foreach($data as $d){
            $date_start = $d->course_date_start; 
        } 
        return $date_start; 
    }
    
    public function getDateEnd($course){
        $sql = "SELECT course_date_end FROM `events` where course = $course"; 
        $data = SrcEvents::findBySql($sql)->all();
        $date_end = null;
        foreach($data as $d){
            $date_end = $d->course_date_end; 
        } 
        return $date_end; 
    }
    
    
    public function getDay($day){
        switch($day){
                case 0:
                   return Yii::t('app','Sunday');
                    break; 
                case 1: 
                    return Yii::t('app','Monday');
                    break; 
                case 2: 
                    return Yii::t('app','Tuesday');
                    break; 
                case 3: 
                    return Yii::t('app','Wednesday');
                    break; 
                case 4:
                    return Yii::t('app','Thursday');
                    break; 
                case 5: 
                    return Yii::t('app','Friday');
                    break; 
                case 6: 
                    return Yii::t('app','Saturday');
                    break; 
            }
          }
    
    
    
    public function getEventByCourse($course){
        $query = SrcEvents::findByCondition(['course'=>$course]); 
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        return $dataProvider; 
    }
    
    public function getLessoncomplete(){
        switch ($this->lesson_complete){
            case 1: 
                return Yii::t('app','Lesson complete');
                break; 
            case 0:
                return Yii::t('app','Lesson incomplete'); 
                break; 
            case 2: 
                return Yii::t('app','Lesson postpone');
                break; 
            default :
                return Yii::t('app',"It's a holiday");
                break;
        }
    }
    
    public function getCompleteCourseDuration($course){
        // Take all the lesson who already complete 
        $sql_str = "SELECT e.id, e.course, m.code, m.duration, e.date_start, e.date_end, e.lesson_complete, s.subject_name, e.course_date_start, e.course_date_end FROM events e INNER JOIN courses c ON (c.id = e.course) INNER JOIN module m ON (m.id = c.module) INNER JOIN subjects s ON (s.id = m.subject) WHERE e.lesson_complete = 1 AND e.course = $course ";
        $data = Events::findBySql($sql_str)->all();
        $duration_ = []; 
        $j = 0;
        // calcul all the duration between two dates 
        foreach($data as $d){
                $ts1 = strtotime($d->date_start);
                $ts2 = strtotime($d->date_end);     
                $seconds_diff = $ts2 - $ts1;                            
                $duration_[$j] = ($seconds_diff/3600);
            
           // $duration_[$j] = date('H:i',abs(strtotime($d->date_start)-strtotime($d->date_end)));  
            $j++; 
        }
        return array_sum($duration_);
        /*
        $hrs = 0; 
        $mins = 0;
        foreach($duration_ as $du){
            list($hours,$minutes) = explode(":",$du);
            $hrs += (int) $hours;
            $mins += (int) $minutes;
            // Convert each 60 minutes to an hour
            if ($mins >= 60) {
                $hrs++;
                $mins -= 60;
            }
            
        }
        if($mins<10){
            $mins = "0".$mins; 
        }
        return $hrs.":".$mins; 
         * 
         */
        
    }
    
}
