<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\fi\models\SrcCourses; 
use app\modules\fi\models\Courses; 
use app\modules\planning\models\SrcEvents; 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\ColorInput;


if(isset($_GET['course'])){
    $id_course = $_GET['course']; 
}
if(isset($_GET['id'])){
    $id = $_GET['id']; 
}

$date_lesson = SrcEvents::findOne($id)->date_start; 
$time_start  = SrcEvents::findOne($id)->time_start; 
$time_end = SrcEvents::findOne($id)->time_end; 
?>

<div class="row">
     <div class="col-lg-1"></div>
     <div class="col-lg-10">
         <h3><?= Yii::t('app','Move {courseName} from {dateLesson} at {timeStart} to {timeEnd} to a new date and time',['courseName'=>SrcCourses::findOne($id_course)->courseName,'dateLesson'=>Yii::$app->formatter->asDate($date_lesson),'timeStart'=>Yii::$app->formatter->asTime($time_start),'timeEnd'=>Yii::$app->formatter->asTime($time_end)]);  ?></h3>
     </div>
     <div class="col-lg-1"></div>    
</div>


<?php $form = ActiveForm::begin(
            
            ); ?>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group courses">
        <label class="control-label" for="courses">
            
            <?php echo Yii::t('app','Course'); ?>
        </label>
        <br/>
        <?= SrcCourses::findOne($id_course)->courseName; ?>
        </div>
    </div>
    <div class="col-lg-6">
             <?php
                echo $form->field($model, 'date_start')->widget(DatePicker::classname(), [
                     'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        <input id="weekday1" name="weekday" type="hidden">
        </div>
</div>
<div class="row">
    <div id="time_start1" name="time_start1" class="col-lg-6">
            
        <div class="input-group bootstrap-timepicker timepicker">
            <?php echo $form->field($model,'time_start')->input('text',['id'=>'debi1','name'=>'debi1']); ?>
        </div>
    </div>
    <div id="time_end1" name="time_end1" class="col-lg-6">
           
        <div  class="input-group bootstrap-timepicker timepicker">
             <?php echo $form->field($model,'time_end')->input('text',['id'=>'fen1','name'=>'fen1']); ?>

        </div>
    </div>
</div>
<p></p>
<div class="row">
        <div class="col-lg-3">
            
        </div>    
    <div class="form-group col-lg-6">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','id'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
    </div>
        <div class="col-lg-3">
            
        </div>
</div>

  <?php ActiveForm::end(); ?>

    
<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$message_time_end_before = Yii::t('app','Time end must after time start !');
$script = <<< JS
$(document).ready(function(){ 
        
 $("#events-course_date_start").change(function() {
    var v = document.getElementById("events-date_start").value;
    var n = v.split('-');
    var y = n[0];
    var m = n[1]-1;
    var d = n[2];
    var g = new Date(y,m,d);
    document.getElementById('weekday1').value = g.getDay();
   
   });       
        
        
       
 $('#debi1').timepicker({
            // template: 'modal',
             showMeridian: false,
             minuteStep: 10,
             explicitMode: true,
                          
   });
$('#fen1').timepicker({
    // template: 'modal',    
     showMeridian: false,
     minuteStep: 10,
     explicitMode: true,
      
});

$('#fen1').change(function(){
    var startTime = document.getElementById("debi1").value;
    var endTime = document.getElementById("fen1").value;
    var regExp = /(\d{1,2})\:(\d{1,2})\:(\d{1,2})/;
    if(parseInt(endTime .replace(regExp, "$1$2$3")) > parseInt(startTime .replace(regExp, "$1$2$3"))){
             document.getElementById("btnCreate").disabled = false;
    }else{
          alert("$message_time_end_before");
          document.getElementById("btnCreate").disabled = true; 
        }
        
        
   });   
        
});        
JS;
$this->registerJs($script);
?>
    