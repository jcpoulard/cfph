<?php
use yii\helpers\Html;
use app\modules\fi\models\SrcCourses; 
use app\modules\planning\models\SrcEvents;
?>
<div class="row">
    <div class="col-lg-2">
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Schedule'), ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="col-lg-2">
     <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update schedules'), ['#'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="col-lg-2">
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'Summary courses timing'), ['coursesummary'], ['class' => 'btn btn-info']) ?>
    </div>
</div>
<p>
    
</p>
<div class="row">
    <div class="col-lg-12">
           <!-- <?php // echo $this->render('_globalSearch', ['model' => $searchModel]); ?> -->
    </div>
</div>

<p>
<?php 
    $courseModel = new SrcCourses(); 
     if(isset($_GET['course'])){
        $course = $_GET['course'];
    }
    $courseName = $courseModel->findOne(['id'=>$course])->courseName; 
    
?>
<h4><?= Yii::t('app','Schedule for {courseName}',['courseName'=>$courseName]); ?></h4>
</p>

<table class="table-responsive table-striped table-condensed">
    <thead>
        <tr>
            <th>
                <?= Yii::t('app','Day'); ?>
            </th>
            <th>
                <?= Yii::t('app','Start'); ?>
            </th>
            <th>
                <?= Yii::t('app','End'); ?>
            </th>
            <th>
                <?= Yii::t('app','Lesson complete ?'); ?>
            </th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <?php 
    if(isset($_GET['course'])){
        $course = $_GET['course'];
    }
    $modelEvent = new SrcEvents(); 
    
    $data = $modelEvent->getEventByCourse($course)->getModels(); 
    
    foreach ($data as $d){
    ?>
    <tr>
        <td><?= $modelEvent->getDay($d->weekday); ?></td>
        <td> <?= Yii::$app->formatter->asDateTime($d->date_start); ?></td>
        <td> <?= Yii::$app->formatter->asDateTime($d->date_end); ?></td>
        <td><?= Yii::$app->formatter->asBoolean($d->lesson_complete); ?></td>
        <td><?= Html::a('<i class="fa fa-undo"></i>', ['moveday','id'=>$d->id], ['class' => 'btn btn-warning btn-sm',
                'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to move this course to next free week ?'),
                'method' => 'post',
                ]]         
                ) ?></td>
        <td><?= Html::a('<i class="fa fa-edit"></i>', ['update','id'=>$d->id], ['class' => 'btn btn-success btn-sm']) ?></td>
        <td><?= Html::a('<i class="fa fa-trash"></i>', ['delete','id'=>$d->id], ['class' => 'btn btn-danger btn-sm',
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this schedule?'),
                'method' => 'post',
                ]
            ]) ?></td>
    </tr>
    <?php 
    }
    ?>
</table>
<br/><br/><br/><br/><br/>