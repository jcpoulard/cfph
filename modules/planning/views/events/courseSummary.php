<?php
use yii\helpers\Html;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\SrcEvents;
use app\modules\fi\models\Courses;
use yii\bootstrap\Modal;
use app\modules\fi\models\Shifts;
?>

<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
</div>

<?php
    $all_vacation = Shifts::find()->all();


?>





<div class="row">
    <div class="col-md-2">
         <?php
             if(Yii::$app->session['profil_as'] ==0)  //se pa yon pwof
                echo Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>'pla_sct'], ['class' => 'btn btn-primary','title'=>Yii::t('app','Add schedule'),'id'=>'addSchedule']); ?>
        <div class="list-group">
            <a class="list-group-item <?php if(!isset($_GET['shi']) ||  (isset($_GET['shi']) && $_GET['shi']==0)) echo "active"; ?>" href="coursesummary?wh=pla_sct&shi=0"><?= Yii::t('app','All'); ?></a>
        <?php
            foreach($all_vacation as $av){
                ?>

                <a class="list-group-item <?php if(isset($_GET['shi']) && $_GET['shi']==$av->id) echo "active";  ?>" href="coursesummary?wh=pla_sct&shi=<?= $av->id; ?>"><?= $av->shift_name; ?></a>

        <?php
            }
        ?>

        </div>
    </div>


    <?php
        $acad = Yii::$app->session['currentId_academic_year'];
        if(!isset($_GET['shi']) || (isset($_GET['shi']) && $_GET['shi']==0)){
        $sql_str = "SELECT c.id, m.code, m.duration, c.module, c.weight FROM courses c INNER JOIN module m ON (m.id = c.module) WHERE c.academic_year = $acad ";
    }elseif(isset($_GET['shi']) && $_GET['shi'] > 0){
        $vacation_id = $_GET['shi'];
        $sql_str = "SELECT c.id, m.code, m.duration, c.module, c.weight FROM courses c INNER JOIN module m ON (m.id = c.module) WHERE c.academic_year = $acad AND c.shift = $vacation_id";
    }


        $dataCourse = SrcCourses::findBySql($sql_str)->all();
        $eventModel = new SrcEvents();
    ?>

    <div class="col-md-10 table-responsive">
        <table class='table table-striped table-bordered table-hover dataTables-example'>
         <thead>
            <tr>
            <th><?= Yii::t('app','Code'); ?></th>
            <th><?= Yii::t('app','Module'); ?></th>
            <th><?= Yii::t('app','Duration'); ?></th>
            <th><?= Yii::t('app','Day'); ?></th>
            <th><?= Yii::t('app','Start'); ?></th>
            <th><?= Yii::t('app','End'); ?></th>
            <th><?= Yii::t('app','% complete'); ?></th>
            <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dataCourse as $dc){

            if($eventModel->getDateStart($dc->id)!=null)
            	{
            ?>
            <tr>
                <td><a href="#" class='popopKou' data-idcourse="<?= $dc->id; ?>"><?= $dc->module0->code; ?></a></td>
            <td><?= $dc->courseName; ?></td>
            <td><?= $dc->module0->duration ?></td>
            <td><?= $eventModel->getDayCourse($dc->id); ?></td>
            <td><?= Yii::$app->formatter->asDate($eventModel->getDateStart($dc->id)); ?></td>
            <td><?= Yii::$app->formatter->asDate($eventModel->getDateEnd($dc->id)); ?></td>
            <?php
            $duree_cours_complete = $eventModel->getCompleteCourseDuration($dc->id);
            $hours = $duree_cours_complete;  //explode(":",$duree_cours_complete);
            $total_min = 0;
            $duree_en_minute = 0;
            $percentage = 0;
            /*
            if((int)$hours[1]>0){
                $total_min = $hours[0]*60+$hours[1];
            }else{
                $total_min = $hours[0]*60;
            }
             * 
             */
            $total_min = $hours*60;
            $duree_en_minute = (int)$dc->module0->duration*60;

            if($duree_en_minute>0){
                $percentage = ($total_min/$duree_en_minute)*100;
            }else{
                $percentage = 0;
            }

            ?>
            <td class="project-completion">
                <small title="<?php echo Yii::t('app','{total_complete} of {duree_cours}',['total_complete'=>$duree_cours_complete,'duree_cours'=>$dc->module0->duration]) ?>"><?= Yii::$app->formatter->asInteger($percentage)."%"; ?></small>
                <div class="progress progress-mini">
                    <div  style="width: <?= Yii::$app->formatter->asInteger($percentage)."%"; ?>;" class="progress-bar "></div>
                </div>
           </td>
           <td>
               <?php if(Yii::$app->user->can('planning-events-delete') && $total_min==0) { ?>
                <?= Html::a('<i class="fa fa-trash"></i>', ["delete?id=$dc->id"], ['class' => 'btn btn-danger', 'id'=>'holidayDelete','title'=>Yii::t('app','Delete Course Schedule'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete all schedules related to this course ?'),
                'method' => 'post',
                ]
            ]) ?>
               <?php } ?>
           </td>
            </tr>
            <?php }
               }
            ?>
        </tbody>
        </table>


    </div>

<?php
            if(Yii::$app->session['profil_as'] ==0)
              {
       ?>    
    
<?php
              }
       ?>
</div>

<?php

    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Detail course').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md'

            ]
            );
    echo '<div id="modalContent"></div>';

    Modal::end();



    ?>

<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [

                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>

<?php

$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(function(){
    $(document).on('click','.popopKou',function(){
        var idcourse = $(this).attr('data-idcourse');
        $.get('$baseUrl/index.php/fi/courses/view',{'id':idcourse},function(data){

            $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });

    });

});
JS;
$this->registerJs($script2);

?>
