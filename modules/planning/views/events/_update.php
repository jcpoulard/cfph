<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\ColorInput;
use kartik\widgets\DateTimePicker;
use app\modules\fi\models\SrcCourses;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Events */
/* @var $form yii\widgets\ActiveForm */

 //$acad = Yii::$app->session['currentId_academic_year'];

 $weekdays = [0=>Yii::t('app','Sunday'),1=>Yii::t('app','Monday'),
            2=>Yii::t('app','Tuesday'),3=>Yii::t('app','Wednesday'),4=>Yii::t('app','Thursday'),
            5=>Yii::t('app','Friday'),6=>Yii::t('app','Saturday')];
 $hours = [];
 for($i=0;$i<24;$i++){
     if($i<10){
         $hours[$i]= '0'.$i;
     }else{
         $hours[$i] = $i; 
     }
 }
 
 $minutes = [];
 for($i=0;$i<60;$i++){
     if($i<10){
         $minutes[$i]= '0'.$i;
     }else{
         $minutes[$i] = $i; 
     }
 }

?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
          <div class="alert alert-danger alert-dismissable">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
          <h4><i class="icon fa fa-check"></i><?= Yii::t('app',"Error : Date time incorrect"); ?></h4>
          <?= Yii::$app->session->getFlash('error') ?>
          </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-6">
            
            <?= $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcCourses::findAll(['academic_year'=>1]) ,'id','courseName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
            ?>  
          
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'description')->textarea(['rows' =>2]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
             <?php
                echo $form->field($model, 'course_date_start')->widget(DatePicker::classname(), [
                     'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        </div>
        <div class="col-lg-3">
             <?php
                echo $form->field($model, 'course_date_end')->widget(DatePicker::classname(), [
                     'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        </div>
        <div class="col-lg-3">
            <?php 
            echo '<label class="control-label">'.Yii::t('app','Date start').'</label>';
            echo DateTimePicker::widget([
                'name' => 'date_start',
                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                'value' =>$model->date_start,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]);
            
            ?>
        </div>
        <div class="col-lg-3">
            <?php 
            echo '<label class="control-label">'.Yii::t('app','Date end').'</label>';
            echo DateTimePicker::widget([
                'name' => 'date_end',
                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                'value' =>$model->date_end,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]);
            
            ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-lg-4">
            <?php 
            
              echo $form->field($model, 'weekday')->dropDownList(
                    $weekdays,          
                [
                    'prompt'=>Yii::t('app','Select a day'),
                    'value'=>$model->weekday,
                    'id'=>'weekday',
                    'name'=>'weekday',
                    ]    
            );
         ?>
        </div>
        <div class="col-lg-4">
             <?php
            echo $form->field($model, 'color')->widget(ColorInput::classname(), [
                'options' => ['placeholder' =>Yii::t('app','Select color ...')],
        ]);
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'lesson_complete')->checkbox() ?>
        </div>
    </div>
    
    
        
     

    
    <p></p>
    <div class="row">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>

<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$msg_confirm = Yii::t('app','Are you sure want to remove new day and time ?');
$script = <<< JS
$(document).ready(function(){ 
    $("#field_add").attr('value',1);
    $("#btnAddNewField").click(function(){
         var num     = $('.clonedInput').length;
         var newNum  = new Number(num + 1);
         
       var newElem = $("#entry"+num).clone(true).prop({id: "entry"+newNum,class: "row clonedInput clonedEntry"}).appendTo("#toClone");
        newElem.find("#weekday"+num).attr('id', "weekday"+newNum).attr('name', "weekday"+newNum).val('');
        newElem.find("#hour_start"+num).attr('id', "hour_start"+newNum).attr('name', "hour_start"+newNum).val('');
        newElem.find("#minute_start"+num).attr('id', "minute_start"+newNum).attr('name', "minute_start"+newNum).val('');
        newElem.find("#minute_end"+num).attr('id', "minute_end"+newNum).attr('name', "minute_end"+newNum).val('');
        newElem.find("#hour_end"+num).attr('id', "hour_end"+newNum).attr('name', "hour_end"+newNum).val('');
        $("#field_add").attr('value',newNum); 
       
        $('#btnRemoveField').attr('disabled', false);
    });
        
    $("#btnRemoveField").click(function(){
        if (confirm("$msg_confirm"))
            {
                var num = $('.clonedInput').length;
                $('#entry' + num).slideUp('slow',function(){
                    $(this).remove(); 
                    if(num - 1 === 1)
                    $('#btnRemoveField').attr('disabled', true);
                    $("#field_add").attr('value',num-1);
                });
        }
    });    
        
 });
JS;
$this->registerJs($script);

?>




