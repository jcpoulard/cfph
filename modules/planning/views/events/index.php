<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\SrcEvents;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\planning\models\SrcEvents */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>

<?php
    $me = new SrcEvents();
   // print_r($me->getCompleteCourseDuration(4));
?>

<div class="row">

    <div class="col-xs-12 col-md-2 col-lg-2">
        <!-- New format -->
       <?php
            if(!Yii::$app->user->can("teacher"))
              {
       ?>
       
        <h4>
            <a href="create?wh=pla_sct"><i class="fa fa-plus btn btn-primary btn-sm" title="<?= Yii::t('app','Create Schedule')?>"></i> </a>
            <?= Yii::t('app','Closest Courses'); ?>
        </h4>
      <?php
              }
       ?>
        <div class="ibox float-e-margins">
                
                <div class="ibox-content">
                    <div id="external-events">

        <?php
            $i=1;
            foreach($courseProche as $cp){
        ?>
        <div  title="<?= SrcCourses::findOne(['id'=>$cp->course])->courseName.' ['.Yii::$app->formatter->asTime($cp->time_start).Yii::t('app',' to ').Yii::$app->formatter->asTime($cp->time_end).']'; ?>" class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: <?= $cp->color ?>">

                <span class="pull-right">
                 <?= Yii::$app->formatter->asDate($cp->date_start) ?>

                </span>
                <?= SrcCourses::findOne(['id'=>$cp->course])->module0->code; ?>

        </div>
            <?php

            $i++;
            } ?>
          </div>

                    </div>
                </div>

        <div class="row">

              <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h4>
                        <?= Yii::t('app','Legend'); ?>
                    </h4>

                </div>
                <div class="ibox-content">
                    <div id="external-events">


        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #FF9900">

                <span class="pull-right">
                </span>
                <?= Yii::t('app','Lesson complete') ?>

        </div>
        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #EE82EE">

                <span class="pull-right">
                </span>
                <?= Yii::t('app','Lesson postpone') ?>

        </div>
        <div class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: #980000 ">

                <span class="pull-right">
                </span>
                <?= Yii::t('app',"It's a holiday") ?>

        </div>

          </div>

                    </div>
                </div>
        </div>

            </div>
        <!-- End new format -->




    <div class="col-xs-12 col-md-10 col-lg-10">
        <div id='calendar'></div>

    </div>

</div>


<!-- Modal pour la visualisation des evenements  -->

<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?= Yii::t('app','Close') ?></span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <span><b><?= Yii::t('app','Date start'); ?></b> : </span>
                <span id='dateStart'></span><br/>
                <span><b><?= Yii::t('app','Date end'); ?></b> : </span>
                <span id='dateEnd'></span><br/>
                <span class='kacheFet'><b><?= Yii::t('app','Lesson status')?></b> : </span>
                <span id='lessonStatus' class='kacheFet'></span><br/>
                <span class="kacheFet"><b><?= Yii::t('app','Program'); ?></b> : </span>
                <span id="program" class="kacheFet"></span><br/>
                <span class="kacheFet"><b><?= Yii::t('app','Room'); ?></b> : </span>
                <span id="room" class="kacheFet"></span><br/>
                <span class="kacheFet"><b><?= Yii::t('app','Shift'); ?></b> : </span>
                <span id="shift" class="kacheFet"></span><br/>
                <span class='rapote'><b><?= Yii::t('app','Report to ')?></b> : </span>
                <span id='dateReport' class='rapote'></span><br/>
            </div>

            <div class="modal-footer">
                <div class="row kacheFet">
                    <div class="col-lg-2">
                     <button type="button" class="btn btn-default fenmen" data-dismiss="modal"><?= Yii::t('app','Close'); ?></button>
                    </div>

                    <div class="col-lg-2 efase">
                     <?= Html::a('<i class="fa fa-trash"></i>', [''], ['class' => 'btn btn-info', 'id'=>'eventDelete','title'=>Yii::t('app','Delete event'),
                        'data'=>[
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this lesson ?'),
                        'method' => 'post',
                        ]
                    ]) ?>
                    </div>
               <div class="col-lg-8">
               <?= Html::a('<i class="fa fa-graduation-cap"></i>', [''], ['class' => 'btn btn-info kacheFet fiti', 'id'=>'eventStudentAttendance','title'=>Yii::t('app','Take students attendance.'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to take students attendance for this lesson ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-user"></i>', [''], ['class' => 'btn btn-primary kacheFet fiti', 'id'=>'eventTeacherAttendance','title'=>Yii::t('app','Take teacher attendance.'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to take teacher attendance for this lesson ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-check"></i>', [''], ['class' => 'btn btn-warning  fiti', 'id'=>'eventComplete','title'=>Yii::t('app','Mark as lesson complete'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to mark this lesson as complete ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-thumb-tack"></i>', [''], ['class' => 'btn btn-success maske', 'id'=>'eventMoveChoose','title'=>Yii::t('app','Postpone lesson to choosen date'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to postpone this lesson to specific date you choose ?'),
                //'method' => 'post',
                ]
            ]) ?>
                <?= Html::a('<i class="fa fa-arrow-right"></i>', [''], ['class' => 'btn btn-danger maske', 'id'=>'eventMove','title'=>Yii::t('app','Postpone lesson'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to postpone this lesson to the next avalaible date ?'),
                //'method' => 'post',
                ]
            ]) ?>
               </div>
                </div>
                </div>
                </div>
        </div>
    </div>





<?php

$baseUrl = Yii::$app->request->BaseUrl;

?>

<?php
        $str_json = "";
        foreach($allEvents as $e){
         $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
        }
$language = Yii::$app->params["language"];
$script2 = <<< JS
$('#calendar').fullCalendar({
			locale: '$language',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,listMonth'
			},
			defaultDate: Date.now(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true,
			events: [
                                $str_json
                            ],
                        eventClick:  function(event, jsEvent, view) {
                           $('.efase').show();
                           $('#eventDelete').show();
                           $('#modalTitle').html(event.title);
                           $("#dateStart").html(moment(event.start).format('Do MMMM YYYY h:mm A'));
                           $("#dateEnd").html(moment(event.end).format('Do MMMM YYYY h:mm A'));
                           $("#lessonStatus").html(event.lessonStatus);
                           $("#program").html(event.program);
                           $("#room").html(event.room);
                           $("#shift").html(event.shift);
                           $("#dateReport").html(moment(event.report_to).format('Do MMMM YYYY h:mm A'));
                           $('.rapote').hide();
                           var lesson_complete = event.lesson_complete;
                           if(lesson_complete == -1){
                               $('.kacheFet').hide();
                               $('.maske').hide();

                            }else{
                                $('.kacheFet').show();
                                $('.maske').show();
                                }
                           if(lesson_complete==1 || lesson_complete==2){
                                $('.kacheFet').show();
                                $('.maske').hide();
                                
                                }else if(lesson_complete!=-1){

                                    $('.maske').show();

                                }
                            if(lesson_complete==2){
                                    $('.rapote').show();
                                }
                           var jodia = new Date();
                           if(event.start >= jodia){
                                 
                                if(lesson_complete == -1){
                                    $('.kacheFet').hide();
                                    $('.maske').hide();
                                }
                               $('.fiti').hide();
                               $('fenmen').show();
                            }else{
                                $('.efase').hide();
                                if(lesson_complete == -1){
                                    $('.kacheFet').hide();
                                    $('.maske').hide();
                                }
                                $('.fiti').show();
                                $('fenmen').show();
                                }
                            var initial_date_course_end = event.initial_date_course_end;
                            if(initial_date_course_end=='anyen'){
                                //$('#eventDelete').hide();
                                }else{
                                    $('.efase').show();
                                    $('#eventDelete').show();
                                }
                                
                           $('#eventComplete').attr('href',event.url_complete);
                           $('#eventMove').attr('href',event.url_move);
                           $('#eventMoveChoose').attr('href',event.url_move_choose);
                           $('#eventTeacherAttendance').attr('href',event.url_attendance_teacher);
                           $('#eventDelete').attr('href',event.url_delete_event);
                           $('#eventStudentAttendance').attr('href',event.url_attendance_student);
                           $('#fullCalModal').modal();
                    }
		});
JS;
$this->registerJs($script2);

?>
