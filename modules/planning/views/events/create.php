<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Events */

$this->title = Yii::t('app', 'Create Schedule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>
<div class="events-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelCourse'=>$modelCourse,
    ]) ?>

</div>
