<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\ColorInput;
use kartik\widgets\TimePicker;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcProgram; 
use app\modules\fi\models\Program; 
use app\modules\fi\models\Shifts; 
use app\modules\fi\models\SrcShifts; 
use app\modules\fi\models\Rooms; 
use app\modules\fi\models\SrcRooms;
use kartik\select2\Select2Asset;

Select2Asset::register($this);

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Events */
/* @var $form yii\widgets\ActiveForm */

 $acad = Yii::$app->session['currentId_academic_year'];

 $weekdays = [0=>Yii::t('app','Sunday'),1=>Yii::t('app','Monday'),
            2=>Yii::t('app','Tuesday'),3=>Yii::t('app','Wednesday'),4=>Yii::t('app','Thursday'),
            5=>Yii::t('app','Friday'),6=>Yii::t('app','Saturday')];
 $hours = [];
 for($i=0;$i<24;$i++){
     if($i<10){
         $hours[$i]= '0'.$i;
     }else{
         $hours[$i] = $i; 
     }
 }
 
 $minutes = [];
 for($i=0;$i<60;$i++){
     if($i<10){
         $minutes[$i]= '0'.$i;
     }else{
         $minutes[$i] = $i; 
     }
 }

?>

<div class="events-form">

    <?php $form = ActiveForm::begin(
            
            ); ?>
    


    <div class="row">
        <div class="col-lg-3">
            
            <?= $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcCourses::findAll(['academic_year'=>$acad]) ,'id','completeCourseName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
            ?>  
         
         
        </div>
        <div class="col-lg-3">
            
            <?= $form->field($model, 'room')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcRooms::find()->all(),'id','room_name' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select room  --'),
                                    ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ])->label(Yii::t('app','Room')); 
            ?>  
         
         
        </div>
        <div class="col-lg-3">
             <?php
                echo $form->field($model, 'course_date_start')->widget(DatePicker::classname(), [
                     'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        </div>
        
        <div class="col-lg-3">
             <?php
            echo $form->field($model, 'color')->widget(ColorInput::classname(), [
                'options' => ['placeholder' =>Yii::t('app','Select color ...')],
            ]);
            ?>
        </div>
       
    </div>
    
    
    <div id="toClone">  
    <div id="entry1" class="row clonedInput">
        
        <div class="col-lg-4 selek2">
          <?php 
            
              echo $form->field($model, 'weekday')->dropDownList(
                    $weekdays,          
                [
                    'prompt'=>Yii::t('app','Select a day'),
                    'value'=>1,
                    'id'=>'weekday1',
                    'name'=>'weekday1',
                    ]    
            );
         ?>
        </div>
        <div id="time_start1" name="time_start1" class="col-lg-3">
            
        <div class="input-group bootstrap-timepicker timepicker">
            <?php echo $form->field($model,'time_start')->input('text',['id'=>'debi1','name'=>'debi1']); ?>
        </div>
        </div>
        <div id="time_end1" name="time_end1" class="col-lg-3">
           
        <div  class="input-group bootstrap-timepicker timepicker">
             <?php echo $form->field($model,'time_end')->input('text',['id'=>'fen1','name'=>'fen1']); ?>
         
       </div>
        </div>
        <div class="col-lg-1">
             <div class="input-group">
            <label>&nbsp;</label>
            <button type="button" id="btnAddNewField" class="btn btn-warning form-control" name="addNewField"><i class="fa fa-plus"></i></button>
            </div>
        </div>    
        <div class="col-lg-1">
            <div class="input-group">
            <label>&nbsp;</label>
            <button type="button" style="display: none;" id="btnRemoveField" class="btn btn-danger form-control" name="removeField"><i class="fa fa-trash"></i></button>
            </div>
            </div>
        
    </div>
        
</div>
    <input type="hidden" id="field_add" name="field_add" value="1">
    <div class="row">
        
        
    </div>
    <p></p>
    <div class="row">
        <div class="col-lg-3">
            
        </div>    
    <div class="form-group col-lg-6">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>

            <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
    </div>
        <div class="col-lg-3">
            
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>

<!-- 
        TESTING THINK
-->


<?php 
$baseUrl = Yii::$app->request->BaseUrl;
$msg_confirm = Yii::t('app','Are you sure want to remove new day and time ?');
$script = <<< JS
$(document).ready(function(){ 
   
    $("#field_add").attr('value',1);
    $("#btnAddNewField").click(function(){
         var num     = $('.clonedInput').length;
         var newNum  = new Number(num + 1);
         
       var newElem = $("#entry"+num).clone(true).prop({id: "entry"+newNum,class: "row clonedInput clonedEntry"}).appendTo("#toClone");
        newElem.find("#weekday"+num).attr('id', "weekday"+newNum).attr('name', "weekday"+newNum).val('');
        $('#weekday1').prop('disabled', false);
        newElem.find("#time_start"+num).attr('id', "time_start"+newNum).attr('name', "time_start"+newNum).val('');
        newElem.find("#time_end"+num).attr('id', "time_end"+newNum).attr('name', "time_end"+newNum).val('');
        newElem.find("#debi"+num).attr('id',"debi"+newNum); 
       
        $("#debi"+newNum).timepicker({
             template: 'modal',
             showMeridian: false,
             minuteStep: 10,
             explicitMode: true,
             
             
        }); 
        newElem.find("#fen"+num).attr('id',"fen"+newNum); 
        $("#fen"+newNum).timepicker({
             template: 'modal',
             showMeridian: false,
             minuteStep: 10,
             explicitMode: true,
             
             
        });
        newElem.find("#btnRemoveField").attr('style',"display: inline");    
        $("#field_add").attr('value',newNum); 
       
    });
        
    $("#btnRemoveField").click(function(){
        if (confirm("$msg_confirm"))
            {
                var num = $('.clonedInput').length;
                $('#entry' + num).slideUp('slow',function(){
                    $(this).remove(); 
                    if(num - 1 === 1)
                   // $('#btnRemoveField'+newNum).attr('style', "display: none");
                    $("#field_add").attr('value',num-1);
                });
        }
    }); 
   
   $("#events-course_date_start").change(function() {
    var v = document.getElementById("events-course_date_start").value;
    var n = v.split('-');
    var y = n[0];
    var m = n[1]-1;
    var d = n[2];
    var g = new Date(y,m,d);
    document.getElementById('weekday1').value = g.getDay();
   
   });
   
        
 });
        
 $('#debi1').timepicker({
             template: 'modal',
             showMeridian: false,
             minuteStep: 10,
             explicitMode: true,
                          
   });
$('#fen1').timepicker({
     template: 'modal',    
     showMeridian: false,
     minuteStep: 10,
     explicitMode: true,
      
});
        
       
        
JS;
$this->registerJs($script);
?>





