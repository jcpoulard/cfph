<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcCourses; 

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Events */
$courseModel = new SrcCourses;

$courseName = $courseModel->findOne(['id'=>$model->course])->courseName; 
$this->title = Yii::t('app', 'Update schedule for {modelClass}', [
    'modelClass' =>$courseName,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="events-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
