<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Holydays */

$this->title = Yii::t('app', 'Update {modelClass} ', [
    'modelClass' => 'Holydays',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Holydays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="holydays-update">
   

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
