<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Holydays */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Holydays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holydays-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'is_no_planning',
            'description_no_planning:ntext',
            'date_start',
            'date_end',
            'all_day',
            'color',
            'repeat_each_day',
            'create_date',
            'update_date',
            'create_by',
            'update_by',
        ],
    ]) ?>

</div>
