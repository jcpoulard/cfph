<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Holydays */

$this->title = Yii::t('app', 'Create Holydays');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Holydays'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holydays-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
