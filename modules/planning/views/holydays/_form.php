<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\ColorInput;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\Holydays */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="holydays-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
             <?= $form->field($model, 'is_no_planning')->checkbox(['id'=>'noPlanning']) ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'description_no_planning')->textarea(['rows' =>2,'disabled'=>'disabled','id'=>'descNoPlanning'])?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6">
           
            <?php
                echo $form->field($model, 'date_start')->widget(DatePicker::classname(), [
                     'type' => DatePicker::TYPE_INPUT,
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
           
        </div>
        <div class="col-lg-6">
           <?php
                echo $form->field($model, 'date_end')->widget(DatePicker::classname(), [
                    'options'=>['placeholder'=>'' ],
                    'language'=>'fr',
                    'pluginOptions'=>[
                         'autoclose'=>true, 
                           'format'=>'yyyy-mm-dd', 
                        ],

                    ] );
               
            ?>
        </div>
    </div>
  
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'all_day')->checkbox() ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'repeat_each_day')->checkbox() ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS
    $('#noPlanning').click(function() {
    $('#descNoPlanning').attr('disabled',! this.checked)
});
JS;
$this->registerJs($script);
?>

