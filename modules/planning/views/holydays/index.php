<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal; 
use app\modules\planning\models\Holydays;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\ColorInput;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\planning\models\SrcHolydays */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Holydays');
$this->params['breadcrumbs'][] = $this->title;
$model = new Holydays();
?>
<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
    
</div>


<div class="row">
    <div class="col-lg-3">
        <!-- New format -->
        <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h4>
                        <?= Yii::t('app','Closest Holydays'); ?>
                    </h4>
                    
                </div>
                <div class="ibox-content">
                    <div id="external-events">
                        
        <?php
            $i=1;
           foreach($holydaysProche as $hp){
        ?>
        <div  title="<?= $hp->description; ?>" class="external-event navy-bg ui-draggable ui-draggable-handle alert alert-info" style="position: relative; background-color: <?= $hp->color; ?>">
           
                <span class="pull-right">
                 <?= Yii::$app->formatter->asDate($hp->date_start.' EST','long'); ?>   
                
                </span>
                <?= $hp->name; ?>
           
        </div>
            <?php 
            
            $i++; 
            } ?>     
          </div>
                        
                    </div>
                </div>
            </div>
        <!-- End new format -->
     
    <div class="col-lg-9">
        
        <div id='calendar'></div>
    </div>
</div>

<?php 
//$baseUrl = Yii::$app->request->BaseUrl;
//$model = new Holydays();
// echo $this->renderAjax('create',['date'=>'2018-10-17','model'=>$model]); 

?>
<?php

    Modal::begin(
            [
                'header'=>'<h4>'.Yii::t('app','Holidays').'</h4>',
                'id'=>'modal',
                'size'=>'modal-md'
                
            ]
            ); 
    echo '<div id="modalContent"></div>';
    
    Modal::end(); 

    
    ?>


<div class="holydays-index">

   
     <div class="row">
        <div class="col-lg-2">
            
        </div>
        <div class="col-lg-10">
            
        </div>
    </div>
  
   
</div>

<!-- Modal de creation des musees -->
 <div class="modal fade lg" id="modal-ferie" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Create Holydays');?></h4>
        </div>
        <div class="modal-body">
            <div class="holydays-form">

    
    <div class="row">
         <div class="col-xs-6">
            <div class="form-group">
                <label for="nom-holiday"><?= Yii::t('app','Holiday') ?></label>
                    <input type="text" class="form-control" id="nom-holiday" name="nom-holiday">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="description"><?= Yii::t('app','Description') ?></label>
                    <input type="text" class="form-control" id="description" name="description">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
             
        </div>
        <div class="col-lg-6">
            
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6">
           
            
           
        </div>
        <div class="col-lg-6">
           
        </div>
    </div>
  
    <div class="row">
        <div class="col-lg-6">
           
        </div>
        <div class="col-lg-6">
             
        </div>
    </div>
            
            </div>    
            
            <div id='value-modal'>
                
            </div>
                
        </div>
            
        
            
        <div class="modal-footer">
            <div class='row'>
                <div class='col-xs-2'>
                    
                </div>
                <div class='col-xs-4'>
                     <div class="form-group">
                        <button type="button" id='save-musee' class='btn btn-success'>Enregistrer</button>
                    </div> 
                </div>
                <div class='col-xs-4'>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
                <div class='col-xs-2'>
                    
                </div>
            </div>
          
          
        </div>
           </div>
      </div>
      
    </div>



<!-- Modal pour la visualisation des evenements  -->
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?= Yii::t('app','Close') ?></span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <span><b><?= Yii::t('app','Date start'); ?></b> : </span>
                <span id='dateStart'></span><br/>
                <span><b><?= Yii::t('app','Date end'); ?></b> : </span>
                <span id='dateEnd'></span><br/>
               
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a id="eventUpdate" data-dismiss="modal" data-id='' target="_blank" class='btn btn-warning' title="<?= Yii::t('app','Update') ?>"> <i class="fa fa-edit"></i></a>
                
                <?= Html::a('<i class="fa fa-trash"></i>', [''], ['class' => 'btn btn-danger', 'id'=>'holidayDelete','title'=>Yii::t('app','Delete holiday'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this holiday ?'),
                'method' => 'post',
                ]
            ]) ?>
                
                
                </div>
        </div>
    </div>
</div>


<?php
        $str_json = "";
        foreach($events as $e){
         $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
        }
      
       
$script2 = <<< JS
$('#calendar').fullCalendar({
			locale: 'fr',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek'
			},
			defaultDate: Date.now(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, 
			events: [
                                $str_json
                            ],
                        eventClick:  function(event, jsEvent, view) {
                           $('#modalTitle').html(event.title);
                           $("#dateStart").html(moment(event.start).format('Do MMMM YYYY h:mm A'));
                           $("#dateEnd").html(moment(event.end).format('Do MMMM YYYY h:mm A'));
                           $('#holidayDelete').attr('href',event.url_delete);
                           $('#eventUpdate').attr('target','_self');
                           //$('#eventUpdate').attr('href',event.url_update);
                           $('#eventUpdate').attr('data-id',event.id);
                           $('#fullCalModal').modal();
                    } 
		});	
JS;
$this->registerJs($script2);
?>

<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script3 = <<< JS
    $(function(){
    $(document).on('click','#eventUpdate',function(){
        
        var id = $(this).attr('data-id');
        $.get('$baseUrl/index.php/planning/holydays/update',{'id':id},function(data){
            
            $('#modal').modal('show')
                   .find('#modalContent')
                   .html(data);
        });
      
    });
    
});
JS;
$this->registerJs($script3);

?>



<?php 

$alert_ms = Yii::t('app','You are about to pass in week mode, in week mode you will not be able to add new data.');
$script1 = <<< JS
$(function(){
       $(document).on('click','.fc-agendaWeek-button',function(){
            alert("$alert_ms");
        }); 
    });
JS;
$this->registerJs($script1);
 
?>

<?php 

$baseUrl = Yii::$app->request->BaseUrl;
$script4 = <<< JS
        $(document).ready(function(){
             // Faire afficher modal musee
        $('.fc-day').click(function(){
           var date = $(this).attr('data-date');
           $(location).attr('href', '$baseUrl/index.php/planning/holydays/create?date='+date);

        });
        });
        
        $(document).on('click','.fc-day',function(){
           var date = $(this).attr('data-date');
           $(location).attr('href', '$baseUrl/index.php/planning/holydays/create?date='+date);
        });
        
JS;
$this->registerJs($script4);
 
?>






