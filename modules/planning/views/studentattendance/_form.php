<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\StudentAttendance */
/* @var $form yii\widgets\ActiveForm */



$status_presence = [0=>Yii::t('app','Present'),
    1=>Yii::t('app','Non motivated absence'),
    2=>Yii::t('app','Motivated absence'),
    3=>Yii::t('app','Non motivated late'),
    4=>Yii::t('app','Motivated late')];


?>

<div class="student-attendance-form">
<?php $form = ActiveForm::begin(); ?>
    
    <?= 
                        $form->field($model, 'attendance_status')
                            ->dropDownList(
                                $status_presence
                                 
                            );
                    ?>
    <div class="row">
        <div class="col-lg-2">
            
        </div>
    <div class="form-group col-lg-2">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <div class="form-group col-lg-2">
           <?= Html::submitButton(Yii::t('app', 'Cancel'),['name'=>'btnCancel','class'=>'btn btn-warning']) ?>  
    </div> 
        <?php if(Yii::$app->user->can('planning-studentattendance-delete')) { ?>
        <?php
            if($model->id != null){
                ?>
        <div class="col-lg-2">
            
                <?= Html::a(Yii::t('app','Delete'), ["delete?id=$model->id"], ['class' => 'btn btn-danger', 'id'=>'attendanceDelete','title'=>Yii::t('app','Delete Attendance'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this student attendance ?'),
                'method' => 'post',
                ]
            ]) ?>
               <?php } ?>
        </div>
        <?php
            }
        ?>
    <div class="form-group col-lg-2">   
        <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
    </div>
        <div class="col-lg-2">
            
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
