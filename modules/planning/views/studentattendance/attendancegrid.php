<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\fi\models\SrcProgram;
use yii\helpers\ArrayHelper;
use app\modules\fi\models\SrcPersons;
use app\modules\planning\models\SrcTeacherAttendance;
use app\modules\fi\models\SrcAcademicperiods;
use yii\bootstrap\Modal;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\StudentAttendance;
use app\modules\planning\models\SrcStudentAttendance;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planning\models\SrcTeacherAttendance */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Teacher Attendances');
$this->params['breadcrumbs'][] = $this->title;
 $acad = Yii::$app->session['currentId_academic_year'];
 $model = new SrcTeacherAttendance();

 $sql_course = "SELECT *  FROM courses where academic_year = $acad";
 $data_course = SrcCourses::findBySql($sql_course)->all();
 $baseUrl = Yii::$app->request->BaseUrl;

?>

<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>

<div class="teacher-attendance-index">
    <div class="row" id="teacher-attendance">
        <div class="col-lg-2">
         <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'course')->widget(Select2::classname(), [
                       'data'=>ArrayHelper::map(SrcCourses::findBySql($sql_course)->all(),'id','courseName' ),
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'options'=>['placeholder'=>Yii::t('app', ' --  select course  --'),
                                    'onchange'=>'submit()',
                                   ],
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]) ?>

        <?php ActiveForm::end(); ?>
         </div>

            <?php
                if(isset($_GET['course'])){
                    $course_id = $_GET['course'];
                    $course_name = SrcCourses::findOne($course_id)->courseName;
                    $sql_str_all = "SELECT DISTINCT p.id, p.last_name, p.first_name FROM persons p "
                            . "INNER JOIN student_has_courses shc ON (shc.student = p.id) "
                            . "WHERE shc.course = $course_id ORDER BY p.last_name";
                }else{
                    $course_name  = null;
                    $sql_str_all = "SELECT DISTINCT p.id, p.last_name, p.first_name FROM persons p "
                            . "INNER JOIN student_has_courses shc ON (shc.student = p.id) "
                            . "ORDER BY p.last_name";
                }
            ?>
            <div class="col-lg-10">
                <?php if($course_name != null){ ?>
                <h4><?= Yii::t('app','Student attendance summary for {coursename}',['coursename'=>$course_name]); ?></h4>
                <?php }else { ?>
                <?php

                    ?>
                <h4><?= Yii::t('app','All students attendance'); ?></h4>
                <?php
                }?>
            </div>

            </div>
    </div>
    <?php
        // Retrieve all the students and the students by programs
        $data_student = SrcPersons::findBySql($sql_str_all)->all();
        $sql_attendance = "SELECT DISTINCT MONTH(sa.date_attendance) FROM student_attendance sa WHERE sa.academic_year = $acad";
        $data_attendance = SrcStudentAttendance::findBySql($sql_attendance)->all();
        $i = 0;
        //$month_ = 0;
        $current_month = date("n",  strtotime(date('Y-m-d')));
        $current_year = date("Y",strtotime(date('Y-m-d')));
        $class = "";
        $month_aca_start = date("n",strtotime(SrcAcademicperiods::findOne($acad)->date_start));
        $month_aca_end = date("n",strtotime(SrcAcademicperiods::findOne($acad)->date_end));
        $year_aca_start = date("y",  strtotime(SrcAcademicperiods::findOne($acad)->date_start));
        $year_aca_end = date("y",  strtotime(SrcAcademicperiods::findOne($acad)->date_end));


     ?>

      <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-tabs nav-justified nav-tabs-sm">
                    <?php
                       if(isset($_GET['course'])){
                           $string_link = "&course=".$_GET['course'];
                       }else{
                           $string_link = "";
                       }
                            for($month_= $month_aca_start; $month_<= 12; $month_++){

                                    if($month_!=$current_month){
                                        $class = "";
                                    }
                                    else{
                                    $class = "active";
                                    }

                                ?>
                    <li class="<?php if(isset($_GET['month']) && $_GET['month']==$month_){ echo "active"; } else{ echo "";} ?>">
                        <a href="attendancegrid?wh=st_att&month=<?= $month_ ?>&year=<?= $year_aca_start.$string_link?>">
                            <?= getShortMonth($month_).' '.$year_aca_start; ?>
                        </a>
                    </li>
                    <?php
                            }

                            for($month_= 1; $month_< $month_aca_start; $month_++){

                                    if($month_!=$current_month){
                                        $class = "";
                                    }
                                    else{
                                    $class = "active";
                                    }

                                ?>
                    <li class="<?php if(isset($_GET['month']) && $_GET['month']==$month_){ echo "active"; } else{ echo "";} ?>">
                        <a href="attendancegrid?wh=st_att&month=<?= $month_ ?>&year=<?= $year_aca_end.$string_link?>">
                            <?= getShortMonth($month_).' '.$year_aca_end; ?>
                        </a>
                    </li>
                    <?php
                            }


                    ?>
                </ul>
            </div>
        </div>
<?php
    if(isset($_GET['month'])){
        $month_id = $_GET['month'];
    }else{
        $month_id = $current_month;
    }

    if(isset($_GET['year'])){
        $year_id = $_GET['year'];
    }else{
        $year_id  = $current_year;
    }
?>



<p>

</p>
    <div class="col-lg-12 table-responsive">
    <table class='table table-striped table-bordered table-hover dataTables-example'>
        <thead>
            <tr>
                <th><a><?= Yii::t('app','First name') ?></a></th>
                <th><a><?= Yii::t('app','Last name') ?></a></th>
                <?php
                    for($i=1; $i<32; $i++){

                        ?>
                <th><?= $i ?></th>
                <?php
                    }
                ?>
                <th title="<?= Yii::t('app','Present') ?>"><?= Yii::t('app','P') ?></th>
                <th title="<?= Yii::t('app','Absent') ?>"><?= Yii::t('app','A') ?></th>
                <th title="<?= Yii::t('app','Tardy') ?>"><?= Yii::t('app','R') ?></th>

            </tr>
        </thead>
        <tbody>
            <?php
                foreach($data_student as $dt){
            ?>
            <tr>
                <td>
                    <a href="<?= $baseUrl ?>/index.php/fi/persons/moredetailsstudent?id=<?= $dt->id ?>&is_stud=1"><?= $dt->first_name; ?></a>
                </td>
                <td>
                    <a href="<?= $baseUrl ?>/index.php/fi/persons/moredetailsstudent?id=<?= $dt->id ?>&is_stud=1"> <?= $dt->last_name; ?></a>
                </td>
                <?php
                    for($i=1; $i<32; $i++){

                        ?>
                <td><?= $model->getAttendanceStudByDay($dt->id,$i,$month_id,$year_id); ?></td>
                <?php
                    }
                ?>
                <td><?= $model->getCountStudentAttendance($dt->id, $acad, "(0)", $month_id) ?></td>
                <td><?= $model->getCountStudentAttendance($dt->id, $acad, "(1,2)",$month_id) ?></td>
                <td><?= $model->getCountStudentAttendance($dt->id, $acad, "(3,4)",$month_id) ?></td>
            </tr>
                <?php  } ?>
        </tbody>
    </table>
    </div>


<?php
    $script = <<< JS
    $(document).ready(function(){

            $('.dataTables-example').DataTable({
               'columnDefs': [{ 'orderable': false, 'targets': [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35] }],
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                      sortAscending:  ": activer pour trier la colonne par ordre croissant",
                      sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    {extend: 'excel', title: 'cfphAttendanceGrid'},
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '8px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });


        });

JS;
$this->registerJs($script);

?>

<?php

$baseUrl = Yii::$app->request->BaseUrl;
$script2 = <<< JS
    $(function(){
    $(document).on('click','.prezans',function(){
        var idprezans = $(this).attr('data-idprezans');
        location.href='$baseUrl/index.php/planning/studentattendance/update?id='+idprezans+'&wh=st_att';
    });

});
JS;
$this->registerJs($script2);

?>

<?php

$this->registerJs('
$("#srcteacherattendance-course").change(function() {
    var text = $("#srcteacherattendance-course option:selected").val();
    $("#w0").attr("action", "'.$baseUrl.'/index.php/planning/studentattendance/attendancegrid?wh=st_att&month='.$month_id.'&year='.$year_id.'&course="+text);
});
', \yii\web\View::POS_END);
?>
