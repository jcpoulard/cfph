<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\SrcEvents;
use app\modules\fi\models\SrcStudentHasCourses; 
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\StudentAttendance */

$this->title = Yii::t('app', 'Create Student Attendance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Student Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$status_presence = [0=>Yii::t('app','Present'),
    1=>Yii::t('app','Non motivated absence'),
    2=>Yii::t('app','Motivated absence'),
    3=>Yii::t('app','Non motivated late'),
    4=>Yii::t('app','Motivated late')];

if(isset($_GET['id'])){
    $lesson_id = $_GET['id'];
    $lesson_date = SrcEvents::findOne($lesson_id)->date_start;
    $lesson_time_start = SrcEvents::findOne($lesson_id)->time_start;
    $lesson_time_end = SrcEvents::findOne($lesson_id)->time_end; 
}


if(isset($_GET['course'])){
    $course_id = $_GET['course']; 
    $course_name = SrcCourses::findOne($course_id)->courseName; 
    $sql = "SELECT shc.id, shc.student, shc.course, p.first_name, p.last_name FROM student_has_courses shc INNER JOIN persons p ON (p.id = shc.student) where shc.course = $course_id ORDER BY p.last_name ASC";
    $data_course = SrcStudentHasCourses::findBySql($sql)->all(); 
    $i = 1; 
}


?>
<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>
<div class="student-attendance-create">
    
    
    <div class="row">
        <div class="col-lg-12">
            <h3>
                <?= Yii::t('app','Take attendance for student in  {course} on {date} from {time1} to {time2}',['course'=>$course_name,'date'=>Yii::$app->formatter->asDate($lesson_date),'time1'=>Yii::$app->formatter->asTime($lesson_time_start),'time2'=>Yii::$app->formatter->asTime($lesson_time_end)]); ?>
            </h3>
        </div>
        
    </div>
    
    
    
    <div class="row">
        <div class="col-lg-12">
     <?php $form = ActiveForm::begin(); ?>
            
            <div class="row">
        <?php
             $errors = $model->errors;
             if(!empty($errors)){
                 ?>
        <div class="col-lg-12 alert alert-dismissable alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="<?=Yii::t('app','Close')?>">&times;</a>
            <?php  echo $errors['student'][0]; ?><br/>
            <?= Yii::t('app','Please click on cancel to return to the calendar !'); ?>
        </div>
        <?php 
            }
            
        ?>
        
    </div>
     
  <div class="row">
                <div class="col-lg-3">
                    
                </div>
                <div class="col-lg-2">
                     <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','id'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <div class="col-lg-2">
                     
                    <?= Html::submitButton(Yii::t('app', 'Cancel'),['name'=>'btnCancel','class'=>'btn btn-warning']) ?>  
                    
                </div>
                <div class="col-lg-2">
                    <?php //back button   
                    $url=Yii::$app->request->referrer; 
                    echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

                    ?>
                </div>
                 <div class="col-lg-3">
                    
                </div>
            </div>          
    
    <table class='table table-striped table-bordered table-hover dataTables-example'>
        <thead>
        <th><?= Yii::t('app','#'); ?></th>
        <th><?= Yii::t('app','First name'); ?></th>
        <th><?= Yii::t('app','Last name'); ?></th>
        <th><?= Yii::t('app','Presence status');  ?></th>
        </thead>
        <tbody>
        <?php
            foreach($data_course as $dc){
                ?>
            <tr>
                <td><?= $i; ?></td>
                <td><?= $dc->first_name;  ?></td>
                <td><?= $dc->last_name; ?></td>
                <td>
                    <?= 
                        $form->field($model, 'attendance_status')
                            ->dropDownList(
                                $status_presence,
                                 ['name'=>'presence_status'.$dc->student]
                            );
                    ?>
                    <input type="hidden" value="<?= $dc->student; ?>" name="student<?= $dc->student; ?>" />
                </td>
            </tr>
            <?php
                $i++;
            }
        ?>
        </tbody>
    </table>
            
            <div class="row">
                <div class="col-lg-3">
                    
                </div>
                <div class="col-lg-2">
                     <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','id'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
                <div class="col-lg-2">
                     
                    <?= Html::submitButton(Yii::t('app', 'Cancel'),['name'=>'btnCancel','class'=>'btn btn-warning']) ?>  
                    
                </div>
                <div class="col-lg-2">
                    <?php //back button   
                    $url=Yii::$app->request->referrer; 
                    echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

                    ?>
                </div>
                 <div class="col-lg-3">
                    
                </div>
            </div>
    
    <?php ActiveForm::end(); ?>
        </div>
    </div>
    

</div>

<br/><br/><br/><br/><br/>