<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\StudentAttendance */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Student Attendance',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Student Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
</div>

<div class="student-attendance-update">
    
    <?php 
    
        $student_name = SrcPersons::findOne($model->student)->fullName; 
    ?>
    <h3><?= Yii::t('app','Update attendance for {student}',['student'=>$student_name]) ?></h3>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
