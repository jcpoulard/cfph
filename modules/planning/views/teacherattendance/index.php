<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\modules\fi\models\SrcProgram; 
use yii\helpers\ArrayHelper;
use app\modules\fi\models\SrcPersons; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\planning\models\SrcTeacherAttendance */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Teacher Attendances');
$this->params['breadcrumbs'][] = $this->title;
 $acad = Yii::$app->session['currentId_academic_year'];
?>
<div class="teacher-attendance-index">
    <div class="row">
       
           <div class="col-lg-2 dropdown">
                  <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                      <?= Yii::t('app','Choose a program'); ?>
                  <span class="caret"></span></button>
                  <ul class="dropdown-menu">
                      <?php
                      $program = SrcProgram::find()->all();
                      foreach($program as $p){
                          ?>
                      <li><a href="index?wh=te_att&prog=<?= $p->id; ?>"><?= $p->label;  ?></a></li>
                      <?php 
                      }
                      ?>
                      <li><a href="index?wh=te_att"><?= Yii::t('app','Show all program')?></a></li>
                    
                  </ul>
            </div>
            <?php
                if(isset($_GET['prog'])){
                    $id_prog = $_GET['prog'];
                    $program_name = SrcProgram::findOne($id_prog)->label; 
                    $sql_str_all = "SELECT DISTINCT p.id, p.last_name, p.first_name FROM persons p "
                            . "INNER JOIN courses c ON (c.teacher = p.id) "
                            . "INNER JOIN module m ON (m.id = c.module) "
                            . "INNER JOIN program pr ON (pr.id = m.program) where pr.id = $id_prog  AND c.academic_year = $acad ORDER BY p.last_name ASC";
                }else{
                    $program_name  = null;
                    $sql_str_all = "SELECT DISTINCT p.id, p.last_name, p.first_name FROM persons p INNER JOIN courses c ON (c.teacher = p.id)
                        INNER JOIN module m ON (m.id = c.module) INNER JOIN program pr ON (pr.id = m.program)
                        WHERE c.academic_year = $acad
                        ORDER BY p.last_name ASC";
                }
            ?>
            <div class="col-lg-10">
                <?php if($program_name != null){ ?>
                <h4><?= Yii::t('app','Teacher attendance summary for {program}',['program'=>$program_name]); ?></h4>
                <?php }else { ?>
                <?php
                    
                    ?>
                <h4><?= Yii::t('app','All teachers attendance'); ?></h4>
                <?php 
                }?>
            </div>
            
            </div>
    </div>
    <?php
        // Retrieve all the teacher and the teacher by programs 
        $data_teacher = SrcPersons::findBySql($sql_str_all)->all();
     ?>
    <div class="col-lg-10 table-responsive">
    <table class='table table-striped table-bordered table-hover dataTables-example'>
        <thead>
            <tr>
                <th><?= Yii::t('app','First name') ?></th>
                <th><?= Yii::t('app','Last name') ?></th>
                <th><?= Yii::t('app','Present') ?></th>
                <th><?= Yii::t('app','Absence') ?></th>
                <th><?= Yii::t('app','Late') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($data_teacher as $dt){
            ?>
            <tr>
                <td>
                    <?= $dt->first_name; ?>
                </td>
                <td>
                    <?= $dt->last_name; ?>
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
                <td>
                    
                </td>
            </tr>
                <?php  } ?>
        </tbody>
    </table>
    </div>
<?php
    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>
