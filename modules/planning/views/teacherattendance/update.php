<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons; 

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\TeacherAttendance */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Teacher Attendance',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teacher Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>
<div class="teacher-attendance-update">
    <?php 
    
        $teacher_name = SrcPersons::findOne($model->teacher)->fullName; 
    ?>
    
    
    
    <h3><?= Yii::t('app','Update attendance for {teacher}',['teacher'=>$teacher_name]) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
