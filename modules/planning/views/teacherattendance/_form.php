<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\TeacherAttendance */
/* @var $form yii\widgets\ActiveForm */

$status_presence = [0=>Yii::t('app','Present'),
    1=>Yii::t('app','Non motivated absence'),
    2=>Yii::t('app','Motivated absence'),
    3=>Yii::t('app','Non motivated late'),
    4=>Yii::t('app','Motivated late')];

?>

<div class="teacher-attendance-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php
             $errors = $model->errors;
             if(!empty($errors)){
                 ?>
        <div class="col-lg-12 alert alert-dismissable alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="<?=Yii::t('app','Close')?>">&times;</a>
            <?php  echo $errors['teacher'][0]; ?><br/>
            <?= Yii::t('app','Please click on cancel to return to the calendar !'); ?>
        </div>
        <?php 
            }
            
        ?>
        
    </div>
    <div class="row">
        <div class="col-lg-6">
           <?= $form->field($model, 'attendance_status')->widget(Select2::classname(), [
                       'data'=>$status_presence,
                       'size' => Select2::MEDIUM,
                       'theme' => Select2::THEME_CLASSIC,
                       'language'=>'fr',
                       'pluginOptions'=>[
                             'allowclear'=>true,
                         ],
                       ]); 
            ?>  
          
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'time_arrival')->textInput() ?>
        </div>
        <div class="row">
            <div class="col-lg-12">
             <?= $form->field($model, 'notes')->textarea(['rows' => 3]) ?>
        </div>
        </div>
        
    </div>
    
    <?php if($model->id!=null) {?>
    
    <div class="row">
        <div class="col-lg-3">
        </div>
    <div class="form-group col-lg-2">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','id'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     <div class="form-group col-lg-2">
           <?= Html::submitButton(Yii::t('app', 'Cancel'),['name'=>'btnCancel','class'=>'btn btn-warning']) ?>  
    </div> 
        <?php if(Yii::$app->user->can('planning-teacherattendance-delete')) { ?>
        <?php
            if($model->id != null){
                ?>
        <div class="col-lg-2">
            
                <?= Html::a(Yii::t('app','Delete'), ["delete?id=$model->id"], ['class' => 'btn btn-danger', 'id'=>'attendanceDelete','title'=>Yii::t('app','Delete Attendance'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this teacher attendance ?'),
                'method' => 'post',
                ]
            ]) ?>
               <?php } ?>
        </div>
        <?php
            }
        ?>
    <div class="form-group col-lg-2">   
        <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
    </div>
        
        <div class="col-lg-3">
        </div>
    </div>
    
    <?php }else { ?>
        <div class="row">
        <div class="col-lg-3">
        </div>
    <div class="form-group col-lg-2">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['name'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','id'=>$model->isNewRecord ? 'btnCreate':'btnUpdate','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     <div class="form-group col-lg-2">
           <?= Html::submitButton(Yii::t('app', 'Cancel'),['name'=>'btnCancel','class'=>'btn btn-warning']) ?>  
    </div> 
        
    <div class="form-group col-lg-2">   
        <?php //back button   
              $url=Yii::$app->request->referrer; 
              echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';

            ?>
    </div>
        
        <div class="col-lg-3">
        </div>
    </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>


<?php 
$absence = Yii::t('app','Absent'); 
$script = <<< JS
$(document).ready(function(){ 
       
 $('#teacherattendance-time_arrival').timepicker({
            // template: 'modal',
             showMeridian: false,
             minuteStep: 10,
             explicitMode: true,
                          
   });

$('#teacherattendance-attendance_status').change(function(){
    var presence_status = document.getElementById("teacherattendance-attendance_status").value;
    if(presence_status == 1 || presence_status == 2){
        document.getElementById("teacherattendance-time_arrival").disabled = true;
        
        }else{
            document.getElementById("teacherattendance-time_arrival").disabled = false;
            }    
   });        
        
});        
JS;
$this->registerJs($script);
?>
