<?php

use yii\helpers\Html;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcCourses;
use app\modules\planning\models\SrcEvents; 


/* @var $this yii\web\View */
/* @var $model app\modules\planning\models\TeacherAttendance */

$this->title = Yii::t('app', 'Create Teacher Attendance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teacher Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if(isset($_GET['teacher'])){
    $teacher_id = $_GET['teacher'];
    $teacher_name = SrcPersons::findOne($teacher_id)->fullName; 
}
if(isset($_GET['course'])){
    $course_id = $_GET['course'];
    $course_name = SrcCourses::findOne($course_id)->courseName; 
}
if(isset($_GET['id'])){
    $lesson_id = $_GET['id'];
    $lesson_date = SrcEvents::findOne($lesson_id)->date_start;
    $lesson_time_start = SrcEvents::findOne($lesson_id)->time_start;
    $lesson_time_end = SrcEvents::findOne($lesson_id)->time_end; 
}


?>

<div class="row">
    <?= $this->render('//layouts/planningLayout'); ?>
 </div>

<div class="teacher-attendance-create">
    <div class="row">
        <div class="col-lg-12">
            <h3>
                <?= Yii::t('app','Take attendance for {course} on {date} from {time1} to {time2}',['course'=>$course_name,'date'=>Yii::$app->formatter->asDate($lesson_date),'time1'=>Yii::$app->formatter->asTime($lesson_time_start),'time2'=>Yii::$app->formatter->asTime($lesson_time_end)]); ?>
            </h3>
        </div>
        
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
