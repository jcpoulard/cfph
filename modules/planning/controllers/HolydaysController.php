<?php

namespace app\modules\planning\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\planning\models\Holydays;
use app\modules\planning\models\SrcHolydays;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\fi\models\Academicperiods; 
use yii\web\ForbiddenHttpException;

use yii\db\IntegrityException;
/**
 * HolydaysController implements the CRUD actions for Holydays model.
 */
class HolydaysController extends Controller
{
    
     public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Holydays models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('planning-holidays-index')){
        $searchModel = new SrcHolydays();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $holydays = Holydays::find()->all(); 
        $events = [];
        $j = 0; 
        foreach($holydays as $holyday){
            $events[$j]['id'] = $holyday->id; 
            $events[$j]['title'] = $holyday->name;
            $events[$j]['start'] = $holyday->date_start;
            $events[$j]['end'] = date("Y-m-d",strtotime($holyday->date_end."+1 day"));
            $events[$j]['color'] = $holyday->color; 
            $events[$j]['allDay'] = 1; 
            $events[$j]['url_delete'] = "delete?id=$holyday->id";
            $events[$j]['url_update'] = "update?id=$holyday->id&wh=pla_ho";
            $j++; 
        }
        
       
        $sql = "SELECT * FROM `holydays` where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 10";
        $holydaysProche = Holydays::findBySql($sql)->all(); 
        return $this->render('index', [
            'events'=>$events,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'holydaysProche'=>$holydaysProche,
        ]);
         }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Displays a single Holydays model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('planning-holidays-view')){
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                'duration' =>120000,
                'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                'title' => Html::encode(Yii::t('app','Unthorized access') ),
                'positonY' => 'top',   //   top,//   bottom,//
                'positonX' => 'center'    //   right,//   center,//  left,//
            ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Creates a new Holydays model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($date)
    {
       
        if(Yii::$app->user->can('planning-holidays-create')){
        $acad = Yii::$app->session['currentId_academic_year'];
        $model = new Holydays();
        $model->date_start = $date;
        $model->date_end = $date;
        $model->all_day = True;
        if ($model->load(Yii::$app->request->post())){
            if($model->repeat_each_day == 1){
                $week_day = date('w', strtotime($model->date_start));
                $date1 = Academicperiods::findOne($acad)->date_start; 
                $date2 = Academicperiods::findOne($acad)->date_end; 
                $date_all = getDateForSpecificDayBetweenDates($date1, $date2, $week_day); 
                for($i=0; $i<count($date_all);$i++){
                    $model = new Holydays();
                    $model->name = $_POST['Holydays']['name'];
                    $model->description =$_POST['Holydays']['description']; 
                    $model->is_no_planning = $_POST['Holydays']['is_no_planning'];
                    if(isset($_POST['Holydays']['description_no_planning']))
                    $model->description_no_planning = $_POST['Holydays']['description_no_planning'];  
                    $model->all_day =  $_POST['Holydays']['all_day'];  
                    $model->color = "#980000"; //$_POST['Holydays']['color'];  
                    $model->repeat_each_day = $_POST['Holydays']['repeat_each_day'];  
                    $model->date_start = $date_all[$i];
                    $model->date_end = $date_all[$i];
                    $model->save();
                    $model = new Holydays(); 
                }
                return $this->redirect(['index','wh'=>'pla_ho']);
            }else{
                $model->color = "#980000";
                $model->save();
                return $this->redirect(['index','wh'=>'pla_ho']);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                'duration' =>120000,
                'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                'title' => Html::encode(Yii::t('app','Unthorized access') ),
                'positonY' => 'top',   //   top,//   bottom,//
                'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing Holydays model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('planning-holidays-update')){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'wh'=>'pla_ho']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                'duration' =>120000,
                'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                'title' => Html::encode(Yii::t('app','Unthorized access') ),
                'positonY' => 'top',   //   top,//   bottom,//
                'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing Holydays model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
     if(Yii::$app->user->can('planning-holidays-delete')){   
	      try{
	        $this->findModel($id)->delete();
	
	        return $this->redirect(['index','wh'=>'pla_ho']);
            } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
                ]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Finds the Holydays model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Holydays the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Holydays::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
