<?php

namespace app\modules\planning\controllers;

use Yii;
use app\modules\planning\models\Events;
use app\modules\planning\models\SrcEvents;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\planning\models\Holydays; 
use app\modules\fi\models\SrcCourses; 
use app\modules\fi\models\Courses; 
use app\modules\fi\models\Module; 
use app\modules\fi\models\Program; 
use yii\helpers\Html; 
use yii\web\ForbiddenHttpException;
 
use yii\db\IntegrityException;
/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    
    public $layout = "/inspinia";
    public $weekdays = [];
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        
       if(Yii::$app->user->can('planning-events-index')){ 
        $searchModel = new SrcEvents();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $holydays = Holydays::find()->all(); 
        $eventsHolydays = [];
        $schedules = SrcEvents::find()->all(); 
        $schedulesEvent = [];
        $course = new SrcCourses(); 
        $j = 0; 
        $k = 0; 
        foreach($holydays as $holyday){
            $eventsHolydays[$j]['id']  = $holyday->id; 
            $eventsHolydays[$j]['title'] = $holyday->name; 
            $eventsHolydays[$j]['start'] = $holyday->date_start; 
            $eventsHolydays[$j]['end'] = date("Y-m-d",strtotime($holyday->date_end."+1 day"));
            $eventsHolydays[$j]['color'] = $holyday->color;
            $eventsHolydays[$j]['allDay'] = 1; 
            $eventsHolydays[$j]['type'] = "holiday";
            $eventsHolydays[$j]['lesson_complete'] = -1;
            $eventsHolydays[$j]['lessonStatus'] = Yii::t('app','Holiday'); 
            $eventsHolydays[$j]['editable'] = false;
            $j++; 
            
        }
        foreach($schedules as $schedule){
            $schedulesEvent[$k]['id'] = $schedule->id; 
            $schedulesEvent[$k]['title'] = $course::findOne(['id'=>$schedule->course])->CourseName;
            $schedulesEvent[$k]['room'] = $schedule->room0->room_name;
            $schedulesEvent[$k]['desciption'] = $schedule->description; 
            $schedulesEvent[$k]['start'] = $schedule->date_start; 
            $schedulesEvent[$k]['end'] = $schedule->date_end; 
            $schedulesEvent[$k]['color'] = $schedule->color; 
            $schedulesEvent[$k]['allDay'] = 0; 
            $schedulesEvent[$k]['type'] = "schedule";
            $schedulesEvent[$k]['lessonStatus'] = $schedule->lessoncomplete; 
            $schedulesEvent[$k]['lesson_complete'] = $schedule->lesson_complete;
            $schedulesEvent[$k]['report_to'] = $schedule->report_to; 
            $schedulesEvent[$k]['url_complete'] = "markascomplete?id=$schedule->id";
            $schedulesEvent[$k]['url_move'] = "movelesson?id=$schedule->id&course=$schedule->course";
            $schedulesEvent[$k]['program'] = $schedule->course0->module0->program0->label;
            $schedulesEvent[$k]['shift'] = $schedule->course0->shift0->shift_name; 
            $schedulesEvent[$k]['url_move_choose'] = "movelessonchoose?id=$schedule->id&course=$schedule->course";
            $schedulesEvent[$k]['url_attendance_teacher'] = "../teacherattendance/create?id=$schedule->id&course=$schedule->course&teacher=".$schedule->course0->teacher."&wh=te_att&shift=".$schedule->course0->shift0->id;
            $schedulesEvent[$k]['url_attendance_student'] = "../studentattendance/create?id=$schedule->id&course=$schedule->course&shift=".$schedule->course0->shift0->id."&wh=st_att"; 
            $schedulesEvent[$k]['url_delete_event'] = "deleteevent?id=$schedule->id";
            if($schedule->initial_date_course_end!=null){
            $schedulesEvent[$k]['initial_date_course_end'] = $schedule->initial_date_course_end;
            }else{
                $schedulesEvent[$k]['initial_date_course_end'] = "";
            }
            $k++; 
            
        }
        
        $allEvents = array_merge($eventsHolydays, $schedulesEvent); 
        $sql = "SELECT * FROM events where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS(DATEDIFF( date_start, NOW())), date_start DESC LIMIT 10";
        $courseProche = Events::findBySql($sql)->all(); 
        return $this->render('index', [
            'allEvents'=>$allEvents,
            'eventsHolydays'=>$eventsHolydays,
            'schedulesEvent'=>$schedulesEvent,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'courseProche'=>$courseProche,
        ]);
       }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       
    }
    
    
    public function actionMovelessonchoose($id, $course){
       if(Yii::$app->user->can('planning-events-movelessonchoose')){
           $modelPrime  = SrcEvents::findOne($id); 
           $model = new Events();
           $is_holiday = 0;
           $event_exist = 0;
           if(isset($_POST['btnCreate'])){
           if(isset($_POST['weekday'])){
               $weekday = $_POST['weekday']; 
           }
           if(isset($_POST['Events']['date_start'])){
               $date_start = $_POST['Events']['date_start'];
           }
           if(isset($_POST['Events']['color'])){
               $color = $_POST['Events']['color'];
           }
           if(isset($_POST['debi1'])){
               $time_start = $_POST['debi1']; 
           }
           if(isset($_POST['fen1'])){
               $time_end = $_POST['fen1']; 
           }
           $date_time_end  = $date_start.' '.$time_end; 
           $date_time_start = $date_start.' '.$time_start; 
           // Verifie les dates générées ne sont pas des jours de conges ou des intervalle de congés 
            $sql = "SELECT id FROM holydays WHERE date_start <= '".$date_start."' AND date_end >='".$date_start."'";
            $is_holiday = Holydays::findBySql($sql)->count();
            // Si la date est un jour de congé on ne fait rien 
           if($is_holiday>0){
               /**
                     * Start flash 
                     */
                    Yii::$app->getSession()->setFlash('danger', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' => 36000,
                        'icon' => 'fa fa-exclamation-triangle',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode( Yii::t('app','This may be an holiday !') ),
                        'title' => Html::encode(Yii::t('app','Error') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
                    /*
                     * End Flash 
                     */
            }else{
               // Verifie si n'importe quel cours n'est pas programmé à cette date
                $sql_events = "SELECT * FROM events WHERE date_start <= '".$date_time_start."' AND date_end >='".$date_time_end."'";
                $event_exist = Events::findBySql($sql_events)->count();
                if($event_exist>0){
                    /**
                     * Start flash 
                     */
                    Yii::$app->getSession()->setFlash('danger', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' => 36000,
                        'icon' => 'fa fa-exclamation-triangle',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode( Yii::t('app','This date is not available, a lesson is given at this same date/time!') ),
                        'title' => Html::encode(Yii::t('app','Error') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
                    /*
                     * End Flash 
                     */
                }else{
                   $model->course = $course;
                   $model->course_date_start = $modelPrime->course_date_start; 
                   $model->course_date_end = $modelPrime->course_date_end; 
                   $model->weekday = $weekday;
                   $model->date_start = $date_time_start; 
                   $model->date_end = $date_time_end; 
                   $model->time_start = $time_start; 
                   $model->time_end = $time_end; 
                   $model->repeat_each_day = 0;
                   $model->color = $color;
                   $model->lesson_complete = 0; 
                   $model->initial_date_course_end = $modelPrime->course_date_end; 
                   $model->create_by = currentUser(); 
                   $model->create_date = date('Y-m-d');
                   $model->save(); 
                   $modelPrime->color = "#EE82EE";
                   $modelPrime->lesson_complete = 2;
                   $modelPrime->report_to = $date_time_start; 
                   $modelPrime->update_by = currentUser(); 
                   $modelPrime->save();
                   $this->redirect(['index','wh'=>'pla_pv']);
                }
           }
           }
           
           // POur afficher le calendrier a cote de la page de creation 
           $holydays = Holydays::find()->all(); 
        $eventsHolydays = [];
        $schedules = SrcEvents::find()->all(); 
        $schedulesEvent = [];
        $course = new SrcCourses(); 
        $j = 0; 
        $k = 0; 
        foreach($holydays as $holyday){
            $eventsHolydays[$j]['id']  = $holyday->id; 
            $eventsHolydays[$j]['title'] = $holyday->name; 
            $eventsHolydays[$j]['start'] = $holyday->date_start; 
            $eventsHolydays[$j]['end'] = date("Y-m-d",strtotime($holyday->date_end."+1 day"));
            $eventsHolydays[$j]['color'] = $holyday->color;
            $eventsHolydays[$j]['allDay'] = 1; 
            $eventsHolydays[$j]['type'] = "holiday";
            $eventsHolydays[$j]['lesson_complete'] = -1;
            $eventsHolydays[$j]['lessonStatus'] = Yii::t('app','Holiday'); 
            $eventsHolydays[$j]['editable'] = false;
            $j++; 
            
        }
        foreach($schedules as $schedule){
            $schedulesEvent[$k]['id'] = $schedule->id; 
            $schedulesEvent[$k]['title'] = $course::findOne(['id'=>$schedule->course])->CourseName;
            $schedulesEvent[$k]['desciption'] = $schedule->description; 
            $schedulesEvent[$k]['start'] = $schedule->date_start; 
            $schedulesEvent[$k]['end'] = $schedule->date_end; 
            $schedulesEvent[$k]['color'] = $schedule->color; 
            $schedulesEvent[$k]['allDay'] = 0; 
            $schedulesEvent[$k]['type'] = "schedule";
            $schedulesEvent[$k]['lessonStatus'] = $schedule->lessoncomplete; 
            $schedulesEvent[$k]['lesson_complete'] = $schedule->lesson_complete;
            $schedulesEvent[$k]['url_complete'] = "markascomplete?id=$schedule->id";
            $schedulesEvent[$k]['url_move'] = "movelesson?id=$schedule->id&course=$schedule->course";
            $schedulesEvent[$k]['program'] = $schedule->course0->module0->program0->label;
            $schedulesEvent[$k]['shift'] = $schedule->course0->shift0->shift_name; 
            $schedulesEvent[$k]['url_move_choose'] = "movelessonchoose?id=$schedule->id&course=$schedule->course";
           
            $k++; 
            
        }
        
        $allEvents = array_merge($eventsHolydays, $schedulesEvent);
           
           return $this->render('movelessonchoose',
                   ['model'=>$model,
                    'allEvents'=>$allEvents,
                    'eventsHolydays'=>$eventsHolydays,
                    'schedulesEvent'=>$schedulesEvent,   
                       ]
                   );
           
       }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          } 
    }
    

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCoursesummary(){
        if(Yii::$app->user->can('planning-events-coursesummary')){
        $searchModel = new SrcEvents();
        $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
       
        if(isset($_GET['SrcEvents']))
	          $dataProvider = $searchModel->searchGlobal(Yii::$app->request->queryParams);
        return $this->render(
                'courseSummary',
               [
                'searchModel'=>$searchModel,
                'dataProvider'=>$dataProvider, 
                 
               ]
                );
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    public function actionList(){
        if(Yii::$app->user->can('planning-events-list')){
        $modelEvent = new SrcEvents(); 
        
        
        return $this->render(
                'list',
                ['modelEvent'>$modelEvent]
                );
        }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }
    

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('planning-events-create'))
         {
        
        $acad = Yii::$app->session['currentId_academic_year'];
        $model = new Events();
        $modelCourse = new Courses(); 
        $value_not_set = False; 
        $weekday_post = [];
        $date_time_start_post = []; 
        $date_time_end_post = []; 
        $duration_choose = [];
        $time_start_post = [];
        $time_end_post =[];
        $is_save = 0; 
        if (isset($_POST['btnCreate'])){
            // Recupère les donnees les du formulaire de setup du planning 
            if(isset($_POST['Events']['course_date_start'])){
                $course_date_start =  date('Y-m-d',strtotime($_POST['Events']['course_date_start']));
            }else{
                $value_not_set = True; 
            }
            if(isset($_POST['Events']['color'])){
                $color = $_POST['Events']['color'];
            }
            /*
            if(isset($_POST['Events']['course_date_end'])){
                $course_date_end = $_POST['Events']['course_date_end'];
            }else{
                $value_not_set = True; 
            }
             * 
             */
            if(isset($_POST['Events']['course'])){
                $course =  $_POST['Events']['course'];
            }else{
                $value_not_set = True; 
            }
            if(isset($_POST['Events']['room'])){
                $room = $_POST['Events']['room'];
            }else{
                $value_not_set = True;
            }
            
            // Recupere la durée 
            $module_id = Courses::findOne(['id'=>$course])->module; 
            $duree_cours = Module::findOne(['id'=>$module_id])->duration;
            // Recupere le id du prof
            $teacher_id = Courses::findOne(['id'=>$course])->teacher;
            
            if(isset($_POST['field_add'])){
                $field_add = $_POST['field_add']; 
            }
            // Pour les champs dynamique d'ajout des jours et heures de cours; 
            for($i=1; $i<=$field_add; $i++){
                if(isset($_POST["weekday$i"])){
                    $weekday_post[$i] = $_POST["weekday$i"]; 
                }else{
                    $value_not_set = True; 
                }
                if(isset($_POST["debi$i"])){
                    $start_hour = $_POST["debi$i"];
                }else{
                    $value_not_set = True;
                }
                
                $time_start_post[$i] = $start_hour.':00'; 
                
                if(isset($_POST["fen$i"])){
                    $end_hour = $_POST["fen$i"];
                }else{
                    $value_not_set = True;
                }
                
                $time_end_post[$i] = $end_hour.':00';
                
            }
            $duration_time = [];
            $total_duration = null; 
            // Calcul du nombre total d'heure par semaine
            for($k=1; $k<=$field_add; $k++){
                $ts1 = strtotime($time_start_post[$k]);
                $ts2 = strtotime($time_end_post[$k]);     
                $seconds_diff = $ts2 - $ts1;                            
                $duration_time[$k] = ($seconds_diff/3600);
               // $duration_time[$k] = //date('H:i:s',abs(strtotime($time_start_post[$k])-strtotime($time_end_post[$k])));
            }
            
            
        $hrs = 0; 
        $mins = 0;
        /*
        foreach($duration_time as $du){
            list($hours,$minutes,$secondes) = explode(":",$du);
            $hrs += (int) $hours;
            $mins += (int) $minutes;
            // Convert each 60 minutes to an hour
            if ($mins >= 60) {
                $hrs++;
                $mins -= 60; // $mins -= 60;
            }
            
        }
         * 
         */
        
        /*
        if($mins<60){
            $mins = 0+"".$mins; 
        }
         * 
         */
        $total_duration_minutes = array_sum($duration_time)*60; //$hrs*60+Yii::$app->formatter->asInteger($mins); 
        if($duree_cours!=0){
            $nombre_semaine = Yii::$app->formatter->asInteger(($duree_cours*60)/$total_duration_minutes); 
        }
        
        $course_date_end = date('Y-m-d', strtotime($course_date_start . " +$nombre_semaine weeks"));
        // Fin du calcul de la date de fin du cours 
        
        $all_date_time = []; 
            $fatal_logic_error = True; 
           
            // ON itilialise la transaction 
          $dbTrans = Yii::$app->db->beginTransaction(); 
                for($j=1;$j<=$field_add;$j++){
                    $all_date_time[$j] = getDateForSpecificDayBetweenDates($course_date_start,$course_date_end,$weekday_post[$j]);
                    foreach($all_date_time[$j] as $adt){
                        // Verifie les dates générées ne sont pas des jours de conges ou des intervalle de congés 
                        $sql = "SELECT id FROM holydays WHERE date_start <= '".$adt."' AND date_end >='".$adt."'";
                        $is_holyday = Holydays::findBySql($sql)->count();
                        // Si la date est un jour de congé on ne fait rien 
                        if($is_holyday > 0){
                             
                        }else{
                        // Verifie si le cours est déja en
                            // AND room = $room
                        $sql_events = "SELECT * FROM events WHERE course = $course AND room = $room  AND date_start <= '".$adt.' '.$time_start_post[$j]."' AND date_end >='".$adt.' '.$time_end_post[$j]."'";
                        $event_exist = Events::findBySql($sql_events)->count();
                        if($event_exist>0){
                            $fatal_logic_error = False; 
                        }else{
                            $model->course = $course;
                            $model->room = $room;
                            $model->course_date_start = $course_date_start;
                            $model->course_date_end = $course_date_end; 
                            $model->weekday = (int)$weekday_post[$j]; 
                            $model->description = json_encode($all_date_time); 
                            $model->date_start = $adt.' '.$time_start_post[$j];
                            $model->date_end = $adt.' '.$time_end_post[$j];
                            $model->time_start = $time_start_post[$j]; 
                            $model->time_end = $time_end_post[$j];
                            $model->repeat_each_day = 0;  
                            $model->color = $color; 
                            $model->create_by = currentUser();
                            $model->create_date = date('Y-m-d');
                            $model->lesson_complete = 0; 
                            $model->save(); 
                            $model = new Events();
                        }
                        }
                    
                    }
                }
                
                if($fatal_logic_error===False){
                    
                  
                    /**
                     * Start flash 
                     */
                    Yii::$app->getSession()->setFlash('success', [
                        'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                        'duration' => 36000,
                        'icon' => 'glyphicon glyphicon-remove-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                        'message' => Html::encode( Yii::t('app','Error may be the date is not available !') ),
                        'title' => Html::encode(Yii::t('app','Error') ),
                        'positonY' => 'top',   //   top,//   bottom,//
                        'positonX' => 'center'    //   right,//   center,//  left,//
                    ]);
                    /*
                     * End Flash 
                     */
                    $dbTrans->rollback();
                    return $this->redirect(['create','wh'=>'pla_cs']);
                }else{
                    $dbTrans->commit();
                    return $this->redirect(['index','wh'=>'pla_pv']);  
                }
           // }
            
             
             
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelCourse'=>$modelCourse,
                
            ]);
        }
        
         }else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
       
        
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      if(Yii::$app->user->can('planning-events-update')){  
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
        }
        else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('planning-events-delete')){
           
           try{
	            // Delete all schedules for a specific course 
	            Events::deleteAll('course = :id',[':id'=>$id]);    
	            return $this->redirect(['coursesummary','wh'=>'pla_sct']);
	            
	            } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
        }
        else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    public function actionDeleteevent($id)
    {
        if(Yii::$app->user->can('planning-events-deleteevent')){
         $this->findModel($id)->delete();

        return $this->redirect(['index','wh'=>'pla_pv']);
        }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    public function actionMarkascomplete($id){
         if(Yii::$app->user->can('planning-events-markascomplete')){ 
            $event = $this->findModel($id); 
            $event->lesson_complete = 1; 
            $event->update_by = currentUser();
            $event->color = "#FF9900";
            $event->save(); 
            return $this->redirect(['index','wh'=>'pla_pv']); 
         }
        else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCourseinroom($room,$shift,$program,$acad){
       
         $countCourses = Courses::find()->alias('c')->select(['c.id','module','teacher'])
                         ->joinWith(['module0'])
                         ->where(['room'=>$room,'shift'=>$shift,'program'=>$program,'academic_year'=> $acad])
                         ->count();
                          
        $allCourses = Courses::find()->alias('c')->select(['c.id','module','teacher'])
                         ->joinWith(['module0'])
                         ->where(['room'=>$room,'shift'=>$shift,'program'=>$program,'academic_year'=> $acad])
                         ->all();
       //return  $allCourses;    
       
         if($countCourses > 0)
          {
         	   echo "<option ></option>"; 
         	   
         	   foreach($allCourses as $course)
         	    {
         	   	     echo "<option value='".$course->id."'>".$course->module0->subject0->subject_name." (".$course->teacher0->getFullName().")</option>";
         	   	    
         	   	 }
           }
         else
            echo "<option ></option>"; 
         
          
    }
    
    public function actionChrono(){
        if(Yii::$app->user->can('planning-events-chrono')){ 
            return $this->render('chrono');
            }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    
    public function actionMovelesson($id,$course){
    if(Yii::$app->user->can('planning-events-movelesson')){
       $event = $this->findModel($id);
      // $date_fin_cours = $event->course_date_end; 
       
      $date_fin_cours = $event->course_date_end;

    // add 7 days to the date above
    $new_date_fin_cours = date('Y-m-d', strtotime($date_fin_cours . " +1 week"));
    //echo $NewDate; 
       
     //  $new_date_fin_cours = date('Y-m-d', $time_unix);  //strtotime(strtotime($date_fin_cours), "+1 Week");
       $newEvent = New Events(); 
       $newEvent->course = $event->course; 
       $newEvent->room = $event->room;
       $newEvent->course_date_start = $event->course_date_start; 
       $newEvent->initial_date_course_end = $event->course_date_end; 
       $newEvent->course_date_end = $new_date_fin_cours;
       $newEvent->weekday = $event->weekday; 
       $newEvent->description = $event->description;
       $newEvent->date_start = $new_date_fin_cours.' '.$event->time_start; 
       $newEvent->date_end = $new_date_fin_cours.' '.$event->time_end; 
       $newEvent->time_start = $event->time_start; 
       $newEvent->time_end = $event->time_end; 
       $newEvent->repeat_each_day = $event->repeat_each_day; 
       $newEvent->color = "#45818E";
       $newEvent->lesson_complete = 0; 
       
       $newEvent->create_by = currentUser(); 
       $newEvent->create_date = date('Y-m-d');
       $newEvent->save(); 
       $event->color = "#EE82EE";
       $event->lesson_complete = 2;
       $event->report_to = $new_date_fin_cours.' '.$event->time_start;
       $event->update_by = currentUser();
       $event->save(); 
      // UPDATE all the lesson for this course to change the course_end_date
      $connection = Yii::$app->db;
      $connection->createCommand()->update('events', ['course_date_end' =>$new_date_fin_cours], "course = $course")->execute();
      return $this->redirect(['index','wh'=>'pla_pv']);
    }
    
    else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    
    }
    
}
