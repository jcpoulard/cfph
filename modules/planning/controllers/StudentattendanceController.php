<?php

namespace app\modules\planning\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\planning\models\StudentAttendance;
use app\modules\planning\models\SrcStudentAttendance;
use app\modules\fi\models\SrcStudentHasCourses; 
use app\modules\planning\models\SrcEvents; 

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

use yii\db\IntegrityException;
/**
 * StudentattendanceController implements the CRUD actions for StudentAttendance model.
 */
class StudentattendanceController extends Controller
{
    
    public $layout = "/inspinia";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StudentAttendance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SrcStudentAttendance();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
public function actionAttendancegrid(){
    if(Yii::$app->user->can('planning-studentattendance-attendancegrid')){
    return $this->render('attendancegrid'); 
    }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
}

    /**
     * Displays a single StudentAttendance model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StudentAttendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('planning-studentattendance-create')){
        $acad = Yii::$app->session['currentId_academic_year'];    
        $model = new StudentAttendance();
        $course_id = $_GET['course']; 
        $lesson_id = $_GET['id']; 
        $shift_id = $_GET['shift']; 
        $modelEvent = SrcEvents::findOne($lesson_id); 
        $date_attendance = SrcEvents::findOne($lesson_id)->date_start; 
        $time_start = SrcEvents::findOne($lesson_id)->time_start;
        $time_end = SrcEvents::findOne($lesson_id)->time_end;
        $sql = "SELECT shc.id, shc.student, shc.course, p.first_name, p.last_name FROM student_has_courses shc INNER JOIN persons p ON (p.id = shc.student) where shc.course = $course_id ORDER BY p.last_name ASC";
        $data_course = SrcStudentHasCourses::findBySql($sql)->all();
        $is_model_save = FALSE;
        if(isset($_POST['btnCreate'])){
            foreach($data_course as $dc){
               // print_r($_POST['presence_status'.$dc->student]);
                if(isset($_POST['presence_status'.$dc->student])){
                    $presence_status = $_POST['presence_status'.$dc->student];
                }else{
                  // $presence_status = "";
                }
                $model->student = $dc->student; 
                $model->course = $course_id; 
                $model->lesson = $lesson_id; 
                $model->date_attendance = $date_attendance; 
                $model->attendance_status = $presence_status; 
                $model->academic_year = $acad; 
                $model->time_start = $time_start;
                $model->time_end = $time_end; 
                $model->shift = $shift_id; 
                $model->create_by = currentUser();
                $model->create_date = date('Y-m-d');
                if($model->save()){
                    $is_model_save = TRUE; 
                    $model = new StudentAttendance(); 
                }else{
                    $is_model_save = FALSE; 
                } 
                        
                }
                if($is_model_save){
                    $modelEvent->lesson_complete = 1;
                    $modelEvent->color = "#FF9900";
                    $modelEvent->save();
                    $this->redirect(['events/index','wh'=>'pla_pv']); 
                }else{
                    
                }
        }
        
        if(isset($_POST['btnCancel'])){
            $this->redirect(['events/index','wh'=>'pla_pv']);
        }
        
            return $this->render('create', [
                'model' => $model,
            ]);
      
            }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Updates an existing StudentAttendance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('planning-studentattendance-update')){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['attendancegrid', 'wh' =>'st_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))]);
    
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
         }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing StudentAttendance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('planning-studentattendance-delete')){
		   try{
		        $this->findModel($id)->delete();
		
		       return $this->redirect(['attendancegrid','wh'=>'st_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))]);
		       
		        } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
    }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Finds the StudentAttendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StudentAttendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StudentAttendance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
