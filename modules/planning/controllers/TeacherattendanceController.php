<?php

namespace app\modules\planning\controllers;

use Yii;
use yii\helpers\Html;
use app\modules\planning\models\TeacherAttendance;
use app\modules\planning\models\SrcTeacherAttendance;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\planning\models\SrcEvents; 
use yii\web\ForbiddenHttpException;

use yii\db\IntegrityException;
/**
 * TeacherattendanceController implements the CRUD actions for TeacherAttendance model.
 */
class TeacherattendanceController extends Controller
{
    
    public $layout = "/inspinia";
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TeacherAttendance models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SrcTeacherAttendance();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAttendancegrid()
    {
        if(Yii::$app->user->can('planning-teacherattendance-attendancegrid')){
        $searchModel = new SrcTeacherAttendance();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('attendancegrid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }
    
    /**
     * Displays a single TeacherAttendance model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TeacherAttendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->can('planning-teacherattendance-create')){
        $model = new TeacherAttendance();
        $acad = Yii::$app->session['currentId_academic_year'];
        // Recupere les données necessaire à l'enregistrement 
        if(isset($_POST['btnCreate'])){
          if(isset($_POST['TeacherAttendance']['attendance_status'])){
              $attendance_status = $_POST['TeacherAttendance']['attendance_status'];
          }
          if(isset($_POST['TeacherAttendance']['time_arrival'])){
              $time_arrival = $_POST['TeacherAttendance']['time_arrival']; 
          }else{
              $time_arrival = NULL; 
          }
          if(isset($_POST['TeacherAttendance']['notes'])){
              $notes = $_POST['TeacherAttendance']['notes']; 
          }
          if(isset($_GET['id'])){
              $lesson_id = $_GET['id']; 
          }
          if(isset($_GET['course'])){
              $course_id = $_GET['course']; 
          }
          if(isset($_GET['teacher'])){
              $teacher_id = $_GET['teacher']; 
          }
          if(isset($_GET['shift'])){
              $shift_id = $_GET['shift']; 
          }
          
          $date_attendance = SrcEvents::findOne($lesson_id)->date_start; 
          $time_start = SrcEvents::findOne($lesson_id)->time_start;
          $time_end = SrcEvents::findOne($lesson_id)->time_end;
          $modelEvent = SrcEvents::findOne($lesson_id);
          
          $model->teacher = $teacher_id; 
          $model->course = $course_id; 
          $model->lesson = $lesson_id; 
          $model->date_attendance = $date_attendance; 
          $model->time_arrival = $time_arrival; 
          $model->attendance_status = $attendance_status; 
          $model->academic_year = $acad; 
          $model->notes = $notes; 
          $model->time_start = $time_start; 
          $model->time_end = $time_end; 
          $model->shift = $shift_id; 
          $model->create_by = currentUser();
          $model->create_date = date('Y-m-d');
          if($model->save()){
              $modelEvent->lesson_complete = 1;
              $modelEvent->color = "#FF9900";
              $modelEvent->save();
              $this->redirect(['events/index','wh'=>'pla_pv']); 
          }else{
              
          } 
        }
        if(isset($_POST['btnCancel'])){
            $this->redirect(['events/index','wh'=>'pla_pv']);
        }
        /*
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
         * 
         */
            return $this->render('create', [
                'model' => $model,
            ]);
       // }
        }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }

    /**
     * Updates an existing TeacherAttendance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->can('planning-teacherattendance-update')){
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['attendancegrid', 'wh' =>'te_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
         }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing TeacherAttendance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('planning-teacherattendance-delete')){
	      try{
	      	
	         $this->findModel($id)->delete();
	
	        return $this->redirect(['attendancegrid','wh'=>'te_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))]);
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
        }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Finds the TeacherAttendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TeacherAttendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeacherAttendance::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
       /*
         * Get month from a date 
         */
        public function getMonthAttendance($date){
            $time = strtotime($date);
                         $month=date("n",$time);
                         
            return $month;
        }
    
    
}
