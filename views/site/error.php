<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = Yii::t('app','Not authorized Access');//$name;
?>


<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            
        
            
    </div>
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content site-error">

    <h4><?php echo Yii::t('app','Please contact the administration'); ?></h4>

    <?php 
       if($message!=null)
        { 
	         echo '<div class="alert alert-danger">';
	       
	             echo nl2br(Html::encode($message));
	             
	         echo '</div>';
       
        }
       
     ?>

<!--    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>
-->
</div>
