<?php
use yii\helpers\Url;

?>

<div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"><i class='fa fa-user'></i> Info perso</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false"><i class='fa fa-book'></i> Modules</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false"><i class='fa fa-calendar'></i> Horaire</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i class='fa fa-dollar'></i> Économat</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-5" aria-expanded="false"><i class='fa fa-briefcase'></i> Expériences</a></li>
                        </ul>
                        <div class="tab-content">
                           
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                 <div class="col-lg-9">
                                    <table class='table table-responsive'>
                                        <tr>
                                            <td><b>Prenom:</b> </td>
                                            <td><?= $modelStudent->first_name ?></td>
                                            <td><b>Nom:</b> </td>
                                            <td><?= $modelStudent->last_name ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Date de naissance:</b> </td>
                                            <td><?= $modelStudent->birthday ?></td>
                                            <td><b>Sexe:</b> </td>
                                            <td><?= $modelStudent->gender ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Programme: </b></td>
                                            <td>Electromécanique</td>
                                            <td><b>Adresse:</b> </td>
                                            <td><?= $modelStudent->adresse ?></td>
                                        </tr>
                                    </table>
                                  </div>
                                <div class="col-lg-3">
                            <div class="widget-head-color-box navy-bg p-lg text-center">
                            
                            <img src="<?= Url::to("@web/img/a$modelStudent->id.jpg")?>" class="img-circle circle-border m-b-md" alt="profile">
                            
                            </div>
                        
                        </div>
                         </div>       
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <table class="table table-responsive">
                                        
                                        <thead>
                                         <th>
                                            Modules
                                        </th>
                                        <th>
                                            Code
                                        </th>
                                        <th>
                                            Durée
                                        </th>
                                        <th>
                                            Seuil de réussite
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Résultats
                                        </th>
                                        </thead>
                                        <tr>
                                            <td>Aglais technique</td>
                                            <td>TRI-04</td>
                                            <td>30 hrs</td>
                                            <td>75%</td>
                                            <td>95</td>
                                            <td>Succès</td>
                                        </tr>
                                        <tr>
                                            <td>Recherche d'information</td>
                                            <td>TRI-07</td>
                                            <td>15 hrs</td>
                                            <td>80%</td>
                                            <td>85</td>
                                            <td>Succès</td>
                                        </tr>
                                        <tr>
                                            <td>Programmation des scripts</td>
                                            <td>TRI-14</td>
                                            <td>90 hrs</td>
                                            <td>70%</td>
                                            <td>50</td>
                                            <td>Echec</td>
                                        </tr>
                                        
                                    </table>
                                </div>
                            </div>
                           <div id="tab-3" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Horaire</strong>

                                    <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                        and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                        sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Economat</strong>

                                    <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                        and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                        sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </div>
                            </div>
                            <div id="tab-5" class="tab-pane">
                                <div class="panel-body">
                                    <strong>Experience</strong>

                                    <p>Thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects
                                        and flies, then I feel the presence of the Almighty, who formed us in his own image, and the breath </p>

                                    <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite
                                        sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet.</p>
                                </div>
                            </div>
                        </div>


 </div>