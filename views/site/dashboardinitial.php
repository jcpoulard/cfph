<div class="p-w-md m-t-sm">
                    <div class="row">

                        <div class="col-sm-4">
                            <h1 class="m-b-xs">
                                1,500
                            </h1>
                            <small>
                                Étudiants
                            </small>
                            <div id="sparkline1" class="m-b-sm"><canvas style="display: inline-block; width: 323px; height: 50px; vertical-align: top;" width="323" height="50"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">Electrotechnique</small>
                                    <h4>500</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Réseau informatique</small>
                                    <h4>400</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Electromécanique</small>
                                    <h4>600</h4>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <h1 class="m-b-xs">
                                50
                            </h1>
                            <small>
                                Professeurs
                            </small>
                            <div id="sparkline2" class="m-b-sm"><canvas style="display: inline-block; width: 323px; height: 50px; vertical-align: top;" width="323" height="50"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">Temps plein</small>
                                    <h4>20</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Temps partiel</small>
                                    <h4>25</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Contrats</small>
                                    <h4>5</h4>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-4">

                            <div class="row m-t-xs">
                                <div class="col-xs-6">
                                    <h5 class="m-b-xs">Income last month</h5>
                                    <h1 class="no-margins">160,000</h1>
                                    <div class="font-bold text-navy">98% <i class="fa fa-bolt"></i></div>
                                </div>
                                <div class="col-xs-6">
                                    <h5 class="m-b-xs">Sals current year</h5>
                                    <h1 class="no-margins">42,120</h1>
                                    <div class="font-bold text-navy">98% <i class="fa fa-bolt"></i></div>
                                </div>
                            </div>


                            <table class="table small m-t-sm">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>142</strong> Projects

                                    </td>
                                    <td>
                                        <strong>22</strong> Messages
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <strong>61</strong> Comments
                                    </td>
                                    <td>
                                        <strong>54</strong> Articles
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>154</strong> Companies
                                    </td>
                                    <td>
                                        <strong>32</strong> Clients
                                    </td>
                                </tr>
                                </tbody>
                            </table>



                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="small pull-left col-md-3 m-l-lg m-t-md">
                                <strong>Sales char</strong> have evolved over the years sometimes.
                            </div>
                            <div class="small pull-right col-md-3 m-t-md text-right">
                                <strong>There are many</strong> variations of passages of Lorem Ipsum available, but the majority have suffered.
                            </div>
                            <div class="flot-chart m-b-xl">
                                <div class="flot-chart-content" id="flot-dashboard5-chart" style="padding: 0px; position: relative;"><canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1029px; height: 200px;" width="1029" height="200"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div style="position: absolute; max-width: 114px; top: 184px; left: 16px; text-align: center;" class="flot-tick-label tickLabel">0</div><div style="position: absolute; max-width: 114px; top: 184px; left: 142px; text-align: center;" class="flot-tick-label tickLabel">2</div><div style="position: absolute; max-width: 114px; top: 184px; left: 267px; text-align: center;" class="flot-tick-label tickLabel">4</div><div style="position: absolute; max-width: 114px; top: 184px; left: 393px; text-align: center;" class="flot-tick-label tickLabel">6</div><div style="position: absolute; max-width: 114px; top: 184px; left: 518px; text-align: center;" class="flot-tick-label tickLabel">8</div><div style="position: absolute; max-width: 114px; top: 184px; left: 641px; text-align: center;" class="flot-tick-label tickLabel">10</div><div style="position: absolute; max-width: 114px; top: 184px; left: 766px; text-align: center;" class="flot-tick-label tickLabel">12</div><div style="position: absolute; max-width: 114px; top: 184px; left: 892px; text-align: center;" class="flot-tick-label tickLabel">14</div><div style="position: absolute; max-width: 114px; top: 184px; left: 1017px; text-align: center;" class="flot-tick-label tickLabel">16</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div style="position: absolute; top: 171px; left: 8px; text-align: right;" class="flot-tick-label tickLabel">0</div><div style="position: absolute; top: 137px; left: 8px; text-align: right;" class="flot-tick-label tickLabel">5</div><div style="position: absolute; top: 103px; left: 2px; text-align: right;" class="flot-tick-label tickLabel">10</div><div style="position: absolute; top: 69px; left: 2px; text-align: right;" class="flot-tick-label tickLabel">15</div><div style="position: absolute; top: 35px; left: 2px; text-align: right;" class="flot-tick-label tickLabel">20</div><div style="position: absolute; top: 1px; left: 2px; text-align: right;" class="flot-tick-label tickLabel">25</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1029px; height: 200px;" width="1029" height="200"></canvas></div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">



                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label" for="product_name">Project Name</label>
                                                <input id="product_name" name="product_name" value="" placeholder="Project Name" class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="price">Name</label>
                                                <input id="price" name="price" value="" placeholder="Name" class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label class="control-label" for="quantity">Company</label>
                                                <input id="quantity" name="quantity" value="" placeholder="Company" class="form-control" type="text">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label" for="status">Status</label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="1" selected="">Completed</option>
                                                    <option value="0">Pending</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table table-striped">

                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Master project</td>
                                                <td>Patrick Smith</td>
                                                <td>$892,074</td>
                                                <td>Inceptos Hymenaeos Ltd</td>
                                                <td><strong>20%</strong></td>
                                                <td>Jul 14, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Alpha project</td>
                                                <td>Alice Jackson</td>
                                                <td>$963,486</td>
                                                <td>Nec Euismod In Company</td>
                                                <td><strong>40%</strong></td>
                                                <td>Jul 16, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Betha project</td>
                                                <td>John Smith</td>
                                                <td>$996,824</td>
                                                <td>Erat Volutpat</td>
                                                <td><strong>75%</strong></td>
                                                <td>Jul 18, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Gamma project</td>
                                                <td>Anna Jordan</td>
                                                <td>$105,192</td>
                                                <td>Tellus Ltd</td>
                                                <td><strong>18%</strong></td>
                                                <td>Jul 22, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Alpha project</td>
                                                <td>Alice Jackson</td>
                                                <td>$674,803</td>
                                                <td>Nec Euismod In Company</td>
                                                <td><strong>40%</strong></td>
                                                <td>Jul 16, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Master project</td>
                                                <td>Patrick Smith</td>
                                                <td>$174,729</td>
                                                <td>Inceptos Hymenaeos Ltd</td>
                                                <td><strong>20%</strong></td>
                                                <td>Jul 14, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Gamma project</td>
                                                <td>Anna Jordan</td>
                                                <td>$823,198</td>
                                                <td>Tellus Ltd</td>
                                                <td><strong>18%</strong></td>
                                                <td>Jul 22, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Project <small>This is example of project</small></td>
                                                <td>Patrick Smith</td>
                                                <td>$778,696</td>
                                                <td>Inceptos Hymenaeos Ltd</td>
                                                <td><strong>20%</strong></td>
                                                <td>Jul 14, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Alpha project</td>
                                                <td>Alice Jackson</td>
                                                <td>$861,063</td>
                                                <td>Nec Euismod In Company</td>
                                                <td><strong>40%</strong></td>
                                                <td>Jul 16, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Betha project</td>
                                                <td>John Smith</td>
                                                <td>$109,125</td>
                                                <td>Erat Volutpat</td>
                                                <td><strong>75%</strong></td>
                                                <td>Jul 18, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Gamma project</td>
                                                <td>Anna Jordan</td>
                                                <td>$600,978</td>
                                                <td>Tellus Ltd</td>
                                                <td><strong>18%</strong></td>
                                                <td>Jul 22, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Alpha project</td>
                                                <td>Alice Jackson</td>
                                                <td>$150,161</td>
                                                <td>Nec Euismod In Company</td>
                                                <td><strong>40%</strong></td>
                                                <td>Jul 16, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Project <small>This is example of project</small></td>
                                                <td>Patrick Smith</td>
                                                <td>$160,586</td>
                                                <td>Inceptos Hymenaeos Ltd</td>
                                                <td><strong>20%</strong></td>
                                                <td>Jul 14, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Gamma project</td>
                                                <td>Anna Jordan</td>
                                                <td>$110,612</td>
                                                <td>Tellus Ltd</td>
                                                <td><strong>18%</strong></td>
                                                <td>Jul 22, 2015</td>
                                                <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>


<script>
        $(document).ready(function() {

            var sparklineCharts = function(){
                $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 52], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline2").sparkline([32, 11, 25, 37, 41, 32, 34, 42], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline3").sparkline([34, 22, 24, 41, 10, 18, 16,8], {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1C84C6',
                    fillColor: "transparent"
                });
            };

            var sparkResize;

            $(window).resize(function(e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineCharts, 500);
            });

            sparklineCharts();




            var data1 = [
                [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,20],[11,10],[12,13],[13,4],[14,7],[15,8],[16,12]
            ];
            var data2 = [
                [0,0],[1,2],[2,7],[3,4],[4,11],[5,4],[6,2],[7,5],[8,11],[9,5],[10,4],[11,1],[12,5],[13,2],[14,5],[15,2],[16,0]
            ];
            $("#flot-dashboard5-chart").length && $.plot($("#flot-dashboard5-chart"), [
                        data1,  data2
                    ],
                    {
                        series: {
                            lines: {
                                show: false,
                                fill: true
                            },
                            splines: {
                                show: true,
                                tension: 0.4,
                                lineWidth: 1,
                                fill: 0.4
                            },
                            points: {
                                radius: 0,
                                show: true
                            },
                            shadowSize: 2
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,

                            borderWidth: 2,
                            color: 'transparent'
                        },
                        colors: ["#1ab394", "#1C84C6"],
                        xaxis:{
                        },
                        yaxis: {
                        },
                        tooltip: false
                    }
            );

        });
    </script>