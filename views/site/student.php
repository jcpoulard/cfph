<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

use app\modules\fi\models\Persons;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\fi\models\SrcAcademicperiods */
/* @var $dataProvider yii\data\ActiveDataProvider */


?>

<div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row">
                <div class="col-sm-8">
                    <div class="ibox">
                        <div class="ibox-content">
                            <h2>Étudiants</h2>

                            <div class="input-group col-lg-12">
                               <div class="col-lg-12"style="float: left;">
                                   <div style="float: left;margin-right:30px;">
                                        <a href="../fi/persons/create" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <?php  echo Yii::t("app","Add");?></a>
                                   </div>
                                   <div class="col-lg-9"style="float: left; padding-left:0px;">
                                      <div class="col-lg-9" style="float:left; padding-left:0px;padding-right:0px;">
                                          <input type="text" placeholder="Rechercher étudiant " class="input form-control" style="height:30px;">
                                      </div>

                                        <button type="button" class="btn btn btn-primary btn-sm"> <i class="fa fa-search"></i> Rechercher</button>
                                    </div>
                                </div>
                            </div>
                            <div class="clients-list">

                            <ul class="nav nav-tabs">
                                <span class="pull-right small text-muted"> <?= sizeOf($allStudents); ?> students </span>
                            </ul>

               <!-- konntene kontni tab yo -->
                            <div class="tab-content">
                <!-- kontni tab yo -->
                                <div id="tab-1" class="tab-pane active">
                                    <div class="full-height-scroll">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover">
                                                <tbody>


                                   <?php
                                    //  $all_students=Persons::find()->all();
                                    //voye tout done nap bezwen nan detail.php nan sa :onclick="getSummary('.$stud->id.')"

                                      $cmpt=0;
                                        foreach($allStudents as $stud)
                                          {
                                          	   if($cmpt==0)
                                          	   {  $stud_id = $stud->id;
                                          	      $full_name1=$stud->first_name." ".$stud->last_name;
                                                  $src_img1= Url::to("@web/img/a$stud->id.jpg");


                                          	   	  $cmpt=1;
                                          	   	}

                                              $full_name=$stud->first_name." ".$stud->last_name;
                                              $src_img= Url::to("@web/img/a$stud->id.jpg");

                                              echo '  <tr>
                                                    <td class="client-avatar"><img alt="image" src="'.$src_img.'"> </td>
                                                    <td><a  onclick="getSummary(\''.$stud->id.'\',\''.$full_name.'\',\''.$src_img.'\')" class="client-link">'.$full_name.'</a></td>
                                                    <td>Electromécanique</td>
                                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                                    <td> '.$stud->email.'</td>
                                                    <td class="client-status"><span class="label label-primary">'.$stud->getActive().'</span></td>

                                                </tr>';

                                          }
/*
4 fason pou afiche imaj
   1- echo '<img src="'.Url::to('@web/img/a3.jpg').'"  />';
   2- Html::img('@web/img/a2.jpg')
   3- <img src="<?= Yii::$app->request->baseUrl.'/img/a2.jpg' ?>" >
   4-(database) <img src="<?php echo Yii::getAlias('@web').'/'.$dataProvider->models[0]->id_image; ?>">
*/


                                          ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
               <!-- fen kontni tab yo -->


                            </div>
                 <!-- fen konntene kontni tab yo -->

                            </div>
                        </div>
                    </div>
                </div>


     <!-- fen kolonn adwat la (detay sou...) -->
                <div class="col-lg-4">
                    <div class="ibox ">

                        <div class="ibox-content">
                            <div class="tab-content">

                    <!-- kolonn adwat la pou 1e tab la(detay sou yon moun) -->
                                <div id="contact" >
                                  <div class="row m-b-lg">

                                        <div class="col-lg-4 text-center">
                                            <h2><?php echo $full_name1; ?> </h2>

                                            <div class="m-b-sm">
                                                <img alt="image" class="img-circle" src="<?php echo $src_img1; ?>"
                                                     style="width: 62px">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <strong>
                                                Progression
                                            </strong>

                                            <p>
                            <div class="ibox-content">
                                            <div>
                                                <div>
                                                    <span>Modules</span>
                                                    <small class="pull-right">12/28 </small>
                                                </div>
                                                <div class="progress progress-small">
                                                    <div style="width: 43%;" class="progress-bar progress-bar-striped"></div>
                                                </div>

                                                <div>
                                                    <span>Nombre d'heure</span>
                                                    <small class="pull-right">360 h</small>
                                                </div>
                                                <div class="progress progress-small">
                                                    <div style="width: 60%;" class="progress-bar progress-bar-warning"></div>
                                                </div>
                                                 </div>
                            </div>

                                        </div>
                                 <div class="col-lg-10">
                                       <div style="float: left;margin-right:30px;">
                                           <a href="../fi/persons/update?id= <?php echo $stud_id; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil"></i>update </a>
                                       </div>
                                       <div style="float: left; padding-left:0px;">
                                            <a href="studentdetails?id=<?php echo $stud_id ?>" type="button" class="btn btn-primary btn-sm btn-block"><i
                                                    class="fa fa-plus"></i> Voir détails
                                            </a>
                                      </div>
                                 </div>
                              </div>
                                    <div class="client-detail">
                                    <div class="full-height-scroll">

                                        <strong>Informations</strong>

                                        <ul class="list-group clear-list">
                                            <li class="list-group-item fist-item">
                                                <span class="pull-right"> Lycée Pétion </span>
                                                École fréquentée
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> Port-au-Prince </span>
                                                Zone de provenance
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> Oui </span>
                                                En règle avec l'économat
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> 25 ans </span>
                                                Age
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> jacques12 </span>
                                                Nom utilisateur
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> Oui </span>
                                                Boursier
                                            </li>
                                            <li class="list-group-item">
                                                <span class="pull-right"> Matin </span>
                                                Vacation
                                            </li>
                                        </ul>


                                    </div>
                                    </div>
                           </div>
                                </div>
       <!-- fen kolonn adwat la pou 1e tab la(detay sou yon moun) -->




       <!-- kolonn adwat la pou 2e tab la(detay sou yon moun) -->
                                <div id="company-1" class="tab-pane">
                                    <div class="m-b-lg">
                                            <h2>Tellus Institute</h2>

                                            <p>
                                                Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,written in 45 BC. This book is a treatise on.
                                            </p>
                                            <div>
                                                <small>Active project completion with: 48%</small>
                                                <div class="progress progress-mini">
                                                    <div style="width: 48%;" class="progress-bar"></div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="client-detail">
                                        <div class="full-height-scroll">

                                            <strong>Last activity</strong>

                                            <ul class="list-group clear-list">
                                                <li class="list-group-item fist-item">
                                                    <span class="pull-right"> <span class="label label-primary">NEW</span> </span>
                                                    The point of using
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="pull-right"> <span class="label label-warning">WAITING</span></span>
                                                    Lorem Ipsum is that it has
                                                </li>
                                                <li class="list-group-item">
                                                    <span class="pull-right"> <span class="label label-danger">BLOCKED</span> </span>
                                                    If you are going
                                                </li>
                                            </ul>
                                            <strong>Notes</strong>
                                            <p>
                                                Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
                                            </p>
                                            <hr/>
                                            <strong>Timeline activity</strong>
                                            <div id="vertical-timeline" class="vertical-container dark-timeline">
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon gray-bg">
                                                        <i class="fa fa-coffee"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>Conference on the sales results for the previous year.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                                    </div>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon gray-bg">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>Many desktop publishing packages and web page editors now use Lorem.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                                    </div>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon gray-bg">
                                                        <i class="fa fa-bolt"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>There are many variations of passages of Lorem Ipsum available.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 06:10 pm - 11.03.2014 </span>
                                                    </div>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon navy-bg">
                                                        <i class="fa fa-warning"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>The generated Lorem Ipsum is therefore.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 02:50 pm - 03.10.2014 </span>
                                                    </div>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon gray-bg">
                                                        <i class="fa fa-coffee"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>Conference on the sales results for the previous year.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 2:10 pm - 12.06.2014 </span>
                                                    </div>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                    <div class="vertical-timeline-icon gray-bg">
                                                        <i class="fa fa-briefcase"></i>
                                                    </div>
                                                    <div class="vertical-timeline-content">
                                                        <p>Many desktop publishing packages and web page editors now use Lorem.
                                                        </p>
                                                        <span class="vertical-date small text-muted"> 4:20 pm - 10.05.2014 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
          <!-- fen kolonn adwat la pou 2e tab la(detay sou yon moun) -->






                            </div>
                        </div>
                    </div>
                </div>
           <!-- fen kolonn adwat la (detay sou...) -->



            </div>
        </div>

<script>

function getSummary(id,full_name,src_img)
{


   $.ajax({

     type: "GET",
     url: "<?= Yii::$app->request->baseUrl ?>/../views/site/pers_detail.php",
     data: "id=" + id + "&full_name=" + full_name + "&src_img=" + src_img, // appears as $_GET['id'] @ your backend side
     success: function(data) {
           // data is ur summary
          $('#contact').html(data);
    }


   });

}
</script>
