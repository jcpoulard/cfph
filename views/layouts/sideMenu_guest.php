<li><a title="<?= Yii::t('app', 'Academic') ?>" style="<?php if($controller=="academic") echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/guest/academic/academic">
        <i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Academic') ?></span></a>
</li>

 <li><a title="<?= Yii::t('app', 'Module') ?>" style="<?php if( $controller == "course" ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/guest/course/course">
         <i class="fa fa-book"></i> <span class="nav-label"><?= Yii::t('app', 'Module') ?></span></a>
 </li>


 <li><a title="<?= Yii::t('app', 'Schedule') ?>" style="<?php if( $controller == "schedule" ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/guest/schedule/schedule">
         <i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Schedule') ?></span></a>
 </li>
