<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
    
 <?php
       
 
 ?>  
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='vains') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Validate inscrition').'</span>', [ '../inscription/postulant/validate','wh'=>'vains'], ['title'=>Yii::t('app','Validate Inscription')]) ?></li>
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='ins') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Inscrits').'</span>', [ '../inscription/postulant/index','wh'=>'ins'], ['title'=>Yii::t('app','Inscrit')]) ?></li>
   
  
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='exam') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Entrance examination grades').'</span>', [ '../inscription/postulant/exam','wh'=>'exam'], ['title'=>Yii::t('app','Entrance examination grades')]) ?></li>
   
  
    
<?php
        
 ?>   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='dec') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Decision').'</span>', [ '../inscription/postulant/decision','wh'=>'dec'], ['title'=>Yii::t('app','Decision')]) ?></li>
   
   

<?php
        
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='adm') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Admis').'</span>', [ '../inscription/postulant/admis','wh'=>'adm'],['title'=>Yii::t('app','Admis')]) ?></li>
   
  

<?php
       
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='nadm') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Non admis').'</span>', [ '../inscription/postulant/nonadmis','wh'=>'nadm'],['title'=>Yii::t('app','Non admis')]) ?></li>
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='rpt') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Report').'</span>', [ '../inscription/postulant/report','wh'=>'rpt'],['title'=>Yii::t('app','Report')]) ?></li>
   
  

</ul>