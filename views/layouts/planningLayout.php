<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
   
<?php
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("Magasinier manager"))||(Yii::$app->user->can("Magasinier"))||(Yii::$app->user->can("teacher"))  )
      {
?>    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='pla_sct') echo "active"; ?>">
    <?= Html::a('<span>'.Yii::t('app','Planning').'</span>', ['events/coursesummary','wh'=>'pla_sct'],['title'=>Yii::t('app','Planning')]) ?>
    </li>

<?php
      }
?>    

<?php
    if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("Magasinier manager"))||(Yii::$app->user->can("Magasinier"))||(Yii::$app->user->can("teacher"))  )
      {
?>    
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='pla_pv') echo "active"; ?>">
    <?= Html::a('<span>'.Yii::t('app','Calendar').'</span>', ['events/index','wh'=>'pla_pv'], ['title'=>Yii::t('app','Calendar')]) ?>
    </li>

<?php
      }
?>    

<?php
    if( ( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Magasinier manager"))||(Yii::$app->user->can("Magasinier"))  )&& (Yii::$app->session['profil_as'] ==0)  )
      {
?>       
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='te_att') echo "active"; ?>">
    <?= Html::a('<span>'.Yii::t('app','Teacher attendance').'</span>', ['teacherattendance/attendancegrid','wh'=>'te_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))], ['title'=>Yii::t('app','Teacher attendance')]) ?>
    </li>

<?php
      }
?>    

<?php
    if( ( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Magasinier manager"))||(Yii::$app->user->can("Magasinier"))  )&&(Yii::$app->session['profil_as'] ==0)  )
      {
?>       
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='st_att') echo "active"; ?>">
    <?= Html::a('<span>'.Yii::t('app','Student attendance').'</span>', ['studentattendance/attendancegrid','wh'=>'st_att','month'=>date("n",strtotime(date("Y-m-d"))),'year'=>date("y",strtotime(date("Y-m-d")))], ['title'=>Yii::t('app','Teacher attendance')]) ?>
    </li>

<?php
      }
?>    

   
   
   
</ul>