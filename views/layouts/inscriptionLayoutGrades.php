<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
    
 
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='exam') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Entrance examination grades').'</span>', [ '../inscription/postulant/exam','wh'=>'exam'], ['title'=>Yii::t('app','Entrance examination grades')]) ?></li>
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='dec') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Decision').'</span>', [ '../inscription/postulant/decision','wh'=>'dec'], ['title'=>Yii::t('app','Decision')]) ?></li>
   
   

   
</ul>