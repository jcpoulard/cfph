<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use app\modules\rbac\models\User;

AppAsset::register($this);

$school_acronym = infoGeneralConfig('school_acronym');

$modelUser = new User;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Object du template INSPINIA  -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <?= Html::csrfMetaTags() ?>
    <title>Sytème Gestion Canado Technique</title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
 
 
 
    
<!-- Debbut du template INSPINIA  -->   


   <div  class="gray-bg dashbard-1">
         
      
      
       
<!-- Debut Widget 2 -->    
        <div class="row">
            <div class="col-lg-12">
                <div class=""> <!-- wrapper wrapper-content -->
                
                <!-- Get all flash messages and loop through them  -->
<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
            <?php
            echo \kartik\widgets\Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
                'showSeparator' => true,
                'delay' => 1, //This delay is how long before the message shows
                'pluginOptions' => [
                    'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            ?>
        <?php endforeach; ?> 
        
                    <?= $content; ?>
                 </div>
             <br/>
                
            </div>
      </div>
<div class="clear"></div>
<div class="row">
      <div class="">
    <br/><br/><br/><br/><br/> <br/><br/><br/><br/><br/>   
    </div>
</div>
        
 </div>  

            




<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'><div style=\"text-align:center\"><img src=\"../img/angular_logo.png\"></div></div>";
yii\bootstrap\Modal::end();
?>
