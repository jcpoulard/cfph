<?php

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;

?>


        
  
                     <li class="<?php if( ( ($module=='reports')&&($action!='dashboardbilling')&&(! isset($_GET['from']))&&(! isset($_GET['from1'])) )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Dashboard') ?>" style="<?php if( ( ($action=='dashboardpedago')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dashboardpedago"><i class="fa fa-th-large"></i><span class="nav-label"> <?= Yii::t('app', 'Dashboard') ?> </span></a>

                    </li>
                    
                    <!-- Formation initiale  -->
                    <li class="<?php if( ( ($module=='fi')&&(in_array($action,array_merge($action_teach,$action_stud)) )  )|| ( ($module=='fi')&&(in_array($controller,$control_fi) )  )  ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Initial education') ?>" href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Initial education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                           <!-- <li><a style="<?php if( (in_array($action,$action_teach) )||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/teacher"><?= Yii::t('app', 'Teachers') ?></a></li>
                            <li><a style="<?php if(in_array($action,$action_stud) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/student"><?= Yii::t('app', 'Students') ?></a></li>
                           -->
                           <li><a style="<?php if($controller=='courses') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/courses/index"><?= Yii::t('app', 'Courses') ?></a></li>
                            <!-- A ajouter lors du developpement
                            <li><a class="<?php if($action=='fi') echo $li_active; else echo ''; ?>"href="#"><?= Yii::t('app', 'Attendance') ?></a></li>
                            -->
                            <li><a style="<?php if( ($controller=='grades')&&(in_array($action,$grade_array) ) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/index"><?= Yii::t('app', 'Grades') ?></a></li>
                            <li><a style="<?php if( in_array($action,['reprise','viewreprise']) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/reprise"><?= Yii::t('app', 'Reprise') ?></a></li>
                            <li><a style="<?php if($action=='listbyroom') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/listbyroom"><?= Yii::t('app', 'Grade sheet') ?></a></li>
                          <!-- A a reactive apres leur developpement
                            <li><a href="#"><?= Yii::t('app', 'Admission') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Budget') ?></a></li>
                            -->

 <?php
                    //return program_id or null value
		             $program = isProgramsLeaders(Yii::$app->user->identity->id);
				     if($program==null)
				       {
 ?>

                            <li><a style="<?php if($action=='reportcard') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/reportcard?wh=rpt_gra"><?= Yii::t('app', 'Reportcard') ?></a></li>
                            <li><a style="<?php if( (in_array($action,['transcriptnotes']) ) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/transcriptnotes"><?= Yii::t('app', 'Transcript of notes') ?></a></li>
                           <li><a style="<?php if( ($controller=='studenthascourses')&&(in_array($action,['decision','decisionlist'])) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studenthascourses/decision"><?= Yii::t('app', 'Decision') ?></a></li>
                           
                           
<?php
				       }
?>                           
                           <li><a style="<?php if(($controller=='studenthascourses')&&(! in_array($action,['decision','decisionlist'])) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studenthascourses/index"><?= Yii::t('app', 'Manage room') ?></a></li>
                            
                         <li><a style="<?php if($controller=='studentlevel') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studentlevel/index"><?= Yii::t('app', 'Manage degree') ?></a></li>
                        </ul>
                    </li>
                    <!-- Fin formation initiale -->

                    <!-- Education permanante -->
   <!--                 <li>
                        <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label"><?= Yii::t('app', 'Permanent education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Orientation & Placement') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Business services') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training') ?> continue</a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training in school') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Partners') ?></a></li>

                        </ul>
                    </li>
       -->
                    <!-- Fin Education permanante -->

              <li class="<?php if( ($module=='fi')&&(in_array($action, array_merge($action_emp,$action_teach) )  )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?>" style="<?php if( (in_array($action, array_merge($action_emp,$action_teach) )  )||(( ($action=='moredetailsemployee') &&($_GET['from']=='emp'))||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) )  ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/employee"><i class="fa fa-user"></i><span class="nav-label"> <?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?> </span></a>

                    </li>
                    
                    <li class="<?php if($module=='inscription') echo $li_active; else echo '';?>" title="<?= Yii::t('app', 'Admission') ?>">
                        
                        <a href="">
                            <!-- <?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/index?wh=ins -->
                          <i class="fa fa-sort-amount-asc"></i>
                             <span class="nav-label">
                                    <?= Yii::t('app', 'Admission') ?>
                            </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                             
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='vains') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/validate?wh=vains">
                                         <?= Yii::t('app', 'Validate inscription') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='exam') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/exam?wh=exam">
                                         <?= Yii::t('app', 'Grades & decision') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='rpt') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/report?wh=rpt">
                                         <?= Yii::t('app', 'Reports') ?>
                                 </a>
                             </li>
                             
                         </ul>
                     </li>

                  <!-- Finances -->
                    <li  class="<?php if(  ( ( ($module=='billings')||($module=='stockrooms') )&&(($controller!='partners')&&($controller!='paymentmethod')&&($controller!='devises') ) )||( ($module=='reports')&&($controller=='report')&&((in_array($action,$action_rpt))&&(isset($_GET['from'])&&($_GET['from']=='bil') ) ) )  ) echo $li_active; else echo ''; ?>">
                        <a  title="<?= Yii::t('app','Finance'); ?>" href="#"><i class="fa fa-dollar"></i> <span class="nav-label"><?= Yii::t('app', 'Finance') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                            <li class="<?php if($module=='billings') echo $li_active; else echo ''; ?>">
                                    <a style="<?php if(in_array($controller, $billing_array) ) echo $a_active_style1; else echo ''; ?>" href="#"><span class="nav-label"><?= Yii::t('app', 'Billing') ?></span><span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level collapse">
			                             <?php
                       //payroll individyel
                       $person_id = 0;
                       $payroll_id = 0;
                       $month=0;
                       $year =0;
                       $payroll_date = '';
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id!=NULL)
				           {  
				             if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
				                 	
				                  }
				           
				            }
				        

                       //last payroll pou moun sa
                      $modelPayroll = new SrcPayroll;
                      
                       $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$payroll_id = $p->id;
						     	$month = $p->payroll_month;
						     	$payroll_date = $p->payroll_date;
						     	
						     	$year = getYear($payroll_date);
						     	 break;
						     	 
						     }
						     
						} 
				 }
				 
				  
			
               
               ?>
               <li><a style="<?php if($controller=='payroll') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll_id.'&from=pay&id_='.$payroll_id.'&month_='.$month.'&year_='.$year.'&di=1"'; ?> ><i class="fa fa-arrow-left"></i><?= Yii::t('app', 'Payroll') ?></a></li>
               
               
			                        </ul>
                            </li>
   <!--                         <li><a href="site/magasin"><?= Yii::t('app', 'Store') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Inventory') ?></a></li>
    -->
                        </ul>
                    </li>
                    <!-- Fin Finances -->

            

                    <!-- Planning -->
                    <li>

                        <a title="<?= Yii::t('app', 'Planning') ?>" style="<?php if( ($module=='planning')&&($controller!='holydays') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/events/coursesummary?wh=pla_sct"><i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Planning') ?></span></a>

                    </li>

 <!-- 
                    <li>
                        <a title="<?= Yii::t('app', 'Partners') ?>" style="<?php if($controller=='partners') echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/partners/index"><i class="fa fa-handshake-o"></i><span class="nav-label"> <?= Yii::t('app', 'Partners') ?> </span></a>

                    </li>
   -->
                     <?php               
                  //       if( Yii::$app->user->can("portal-menu") ){
                     ?>
                  <!--  <li>
                        <a title="<?= Yii::t('app', 'Site Web') ?>" style="<?php if( ($module=='portal') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/manage-content?wh=pla_pv"><i class="fa fa-globe"></i> <span class="nav-label"> <?= Yii::t('app', 'Site Web') ?> </span></a>
                    </li>
                  -->
                    <?php
                   //      }
                    ?>
                    
                    <!-- Fin Planning -->

                    <!-- Rapports -->
                    <li>
                        <a title="<?= Yii::t('app', 'Reports') ?>" style="<?php if( ($module=='reports')&&(isset($_GET['from1'])) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/list?cat=peda&from1=rpt&"><i class="fa fa-line-chart"></i> <span class="nav-label"> <?= Yii::t('app', 'Reports') ?> </span></a>
                    </li>
      
                    <!-- Fin Rapports -->

    <!--                <li>
                        <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label"><?= Yii::t('app', 'Mail') ?> </span><span class="label label-warning pull-right">16/24</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Inbox') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'See email') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Compose email') ?></a></li>
                        </ul>
                    </li>
      -->
 <?php
                    //return program_id or null value
		             $program = isProgramsLeaders(Yii::$app->user->identity->id);
				     if($program==null)
				       {
 ?>
                      <li  class="<?php if( (($module=='fi')||($module=='planning')||($module=='billings'))&&(in_array($controller, $setting_array) ) ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Settings') ?>" style="<?php if( (($module=='fi')||($module=='planning'))&&(in_array($controller, $setting_array) ) ) echo $a_active_style1; else echo ''; ?>" href="#"><i class="fa fa-gears"></i> <span class="nav-label"><?= Yii::t('app', 'Settings') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                              <li ><a style="<?php if(in_array($controller, $setting_general_array) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/generalconfig/index?wh=gc"><i class=""></i><?= Yii::t('app', 'General') ?></a></li>

                                        <li ><a style="<?php if($controller=='program') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/program/index"><i class=""></i><?= Yii::t('app', 'Program') ?></a></li>

			                             <li><a style="<?php if($controller=='rooms') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/rooms/index"><i class=""></i><?= Yii::t('app', 'Room') ?></a></li>

			                             <li><a style="<?php if($controller=='module') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/module/index"><i class=""></i><?= Yii::t('app', 'Module') ?></a></li>

			                             <li><a style="<?php if(($controller=='subjects') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/subjects/index"><i class=""></i><?= Yii::t('app', 'Module label') ?></a></li>
                               <?php
                                      if((Yii::$app->user->can("fi-margeechec-index")) ||(Yii::$app->user->can("fi-margeechec-create"))||(Yii::$app->user->can("fi-margeechec-update"))||(Yii::$app->user->can("fi-margeechec-delete")) )
                                         {
                               ?>
                                                     <li><a style="<?php if(($controller=='margeechec') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/margeechec/index"><i class=""></i><?= Yii::t('app', 'Marge Echecs') ?></a></li>
                               <?php   }   ?>
			                             <li><a style="<?php if(($controller=='holydays') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/holydays/index"><i class=""></i><?= Yii::t('app', 'Manage Holidays') ?></a></li>
                                                    <li><a style="<?php if(($controller=='settings') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/settings/yearmigrationcheck"><i class=""></i><?= Yii::t('app', 'New year data migration') ?></a></li>


                        </ul>
                    </li>


           <?php
       }
    	  if((Yii::$app->user->can("rbac-user-usernamelist")) )
             {
        
    	?>
                    <li  >

                        <a title="<?= Yii::t('app', 'User management') ?>" style="<?php if( ($module=='rbac') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/usernamelist?wh=usel"><i class="fa fa-user-plus"></i> <span class="nav-label"> <?= Yii::t('app', 'User management') ?> </span></a>
                    </li>
                 

        <?php
                 }
                 
                   
                      ?>  
                    <li  >

                        <a title="<?= Yii::t('app', 'ID cards') ?>" style="<?php if( ($module=='idcards') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/idcards/idcard/index?wh=prt"><i class="fa fa-id-card"></i> <span class="nav-label"> <?= Yii::t('app', 'ID cards') ?> </span></a>
                    </li>
                    
                    <?php               
             
                if( Yii::$app->user->can("data-migration")){
                    
                    ?>
                    <li>
                        <a title="<?= Yii::t('app', 'Data migration') ?>" style="<?php if( ($module=='migration') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/migration/migration/upload?wh=migr"><i class="fa fa-database"></i> <span class="nav-label"> <?= Yii::t('app', 'Data migration') ?> </span></a>
                    </li>
        <?php 
                }  
               
             ?>  

        <?php

          
             ?>  
               
           
                   