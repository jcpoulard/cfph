<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Url;
use yii\helpers\Html;
//use Yii; 

?>

<ul class="nav nav-tabs col-lg-12">
   <?php 
    if( (Yii::$app->user->can("superadmin")) || (Yii::$app->user->can("direction")) )
       {
  ?>
   <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='useon') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-globe"></span> <span >'.Yii::t('app','User(s) online').'</span>', ['user/viewonlineusers','wh'=>'useon'],['title'=>Yii::t('app','User(s) online')]) ?></li>
      <?php } ?>
      
   <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='usel') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-list"></span><span >'.Yii::t('app','Username list').'</span>', ['user/usernamelist','wh'=>'usel'],['title'=>Yii::t('app','Username list')]) ?></li>
 
  <?php 
   if( !((Yii::$app->user->can("Administration études"))|| (Yii::$app->user->can("Administration recettes")) ) )
       {
  ?>
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='use') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-check-square-o"></span><span >'.Yii::t('app','Users').'</span>', ['user/index','wh'=>'use'],['title'=>Yii::t('app','Users')]) ?></li>
    
 <!--   <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='asi') echo "active"; ?>"><?= Html::a('<i class="fa fa-key"></i><span>'.Yii::t('app','Asign permission').'</span>', ['assignment/index','wh'=>'asi'], ['title'=>Yii::t('app','Asign permission')]) ?></li>
   -->
    <?php } ?>
  <?php 
    if(Yii::$app->user->can("superadmin")|| (Yii::$app->user->can("direction")) ){
  ?>
  <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='per') echo "active"; ?>"><?= Html::a('<i class="fa fa-ban"></i><span>'.Yii::t('app','Permission').'</span>', ['permission/index','wh'=>'per'],['title'=>Yii::t('app','Permission')]) ?>    

  </li>
      <?php } ?>
 
  <?php 
    if( !((Yii::$app->user->can("Administration études"))|| (Yii::$app->user->can("Administration recettes")) ) )
       {
  ?>
   <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='rol') echo "active"; ?>"><?= Html::a('<i class="fa fa-lock"></i><span>'.Yii::t('app','Role').'</span>', ['role/index','wh'=>'rol'],['title'=>Yii::t('app','Role')]) ?></li>
 
<!-- <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='use0') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-times-circle-o"></span><span>'.Yii::t('app','Users Disabled').'</span>', ['user/usersdisabled','wh'=>'use0'],['title'=>Yii::t('app','Users Disabled')]) ?></li>
-->
    
 <?php } ?>

</ul>
