<?php

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;

?>


        
                    <li class="<?php if( ( ($module=='reports')&&($action!='dashboardbilling')&&(! isset($_GET['from']))&&(! isset($_GET['from1'])) )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Dashboard') ?>" style="<?php if( ( ($action=='dashboardpedago')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dashboardpedago"><i class="fa fa-th-large"></i><span class="nav-label"> <?= Yii::t('app', 'Dashboard') ?> </span></a>

                    </li>
  
                    
                    <!-- Formation initiale  -->
                    <li class="<?php if( ( ($module=='fi')&&(in_array($action,array_merge($action_teach,$action_stud)) )  )|| ( ($module=='fi')&&(in_array($controller,$control_fi) )  )  ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Initial education') ?>" href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Initial education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                           <!-- <li><a style="<?php if( (in_array($action,$action_teach) )||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/teacher"><?= Yii::t('app', 'Teachers') ?></a></li>
                            <li><a style="<?php if(in_array($action,$action_stud) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/student"><?= Yii::t('app', 'Students') ?></a></li>
                           -->
                           <li><a style="<?php if($controller=='courses') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/courses/index"><?= Yii::t('app', 'Courses') ?></a></li>
                            <!-- A ajouter lors du developpement
                            <li><a class="<?php if($action=='fi') echo $li_active; else echo ''; ?>"href="#"><?= Yii::t('app', 'Attendance') ?></a></li>
                            -->
                            <li><a style="<?php if( ($controller=='grades')&&(in_array($action,$grade_array) ) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/create"><?= Yii::t('app', 'Grades') ?></a></li>
                            <li><a style="<?php if($action=='listbyroom') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/listbyroom"><?= Yii::t('app', 'Grade sheet') ?></a></li>
                          
                            <li><a href="#"><?= Yii::t('app', 'Admission') ?></a></li>
                            <!-- A a reactive apres leur developpement
                            <li><a href="#"><?= Yii::t('app', 'Budget') ?></a></li>
                            -->
                            <li><a style="<?php if($action=='reportcard') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/reportcard?wh=rpt_gra"><?= Yii::t('app', 'Reportcard') ?></a></li>
                           
                            <li><a style="<?php if($controller=='studenthascourses') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studenthascourses/index"><?= Yii::t('app', 'Manage room') ?></a></li>
                            <li><a style="<?php if($controller=='studentlevel') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studentlevel/index"><?= Yii::t('app', 'Manage degree') ?></a></li>
                        </ul>
                    </li>
                    <!-- Fin formation initiale -->

                    <!-- Education permanante -->
   <!--                 <li>
                        <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label"><?= Yii::t('app', 'Permanent education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Orientation & Placement') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Business services') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training') ?> continue</a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training in school') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Partners') ?></a></li>

                        </ul>
                    </li>
       -->
                    <!-- Fin Education permanante -->

                    <li class="<?php if( ($module=='fi')&&(in_array($action, array_merge($action_emp,$action_teach) )  )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?>" style="<?php if( (in_array($action, array_merge($action_emp,$action_teach) )  )||(( ($action=='moredetailsemployee') &&($_GET['from']=='emp'))||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) )  ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/employee"><i class="fa fa-user"></i><span class="nav-label"> <?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?> </span></a>

                    </li>
                    
                    <li class="<?php if($module=='inscription') echo $li_active; else echo '';?>" title="<?= Yii::t('app', 'Admission') ?>">
                        
                        <a href="">
                            <!-- <?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/index?wh=ins -->
                          <i class="fa fa-sort-amount-asc"></i>
                             <span class="nav-label">
                                    <?= Yii::t('app', 'Admission') ?>
                            </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                             
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='vains') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/validate?wh=vains">
                                         <?= Yii::t('app', 'Validate inscription') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='exam') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/exam?wh=exam">
                                         <?= Yii::t('app', 'Grades & decision') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='rpt') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/report?wh=rpt">
                                         <?= Yii::t('app', 'Reports') ?>
                                 </a>
                             </li>
                             
                         </ul>
                     </li>


                    <!-- Finances -->
                    <li  class="<?php if( ( ($module=='billings')||($module=='stockrooms') )&&(($controller!='partners')&&($controller!='paymentmethod')&&($controller!='devises') ) ) echo $li_active; else echo ''; ?>">
                        <a  title="<?= Yii::t('app','Finance'); ?>" href="#"><i class="fa fa-dollar"></i> <span class="nav-label"><?= Yii::t('app', 'Finance') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                           
                            <!-- Billings  -->
                            <li class="<?php if($module=='billings') echo $li_active; else echo ''; ?>">
                                    <a style="<?php if(in_array($controller, $billing_array) ) echo $a_active_style1; else echo ''; ?>" href="#"><span class="nav-label"><?= Yii::t('app', 'Billing') ?></span><span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level collapse">
			                            <li ><a style="<?php if($controller=='billings') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/billings/index?ri=0&part=rec"><i class="fa fa-arrow-right"></i><?= Yii::t('app', 'Income') ?></a></li>

			                             <?php
                       //payroll individyel
                       $person_id = 0;
                       $payroll_id = 0;
                       $month=0;
                       $year =0;
                       $payroll_date = '';
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id!=NULL)
				           {  
				             if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
				                 	
				                  }
				           
				            }
				        

                       //last payroll pou moun sa
                      $modelPayroll = new SrcPayroll;
                      
                       $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$payroll_id = $p->id;
						     	$month = $p->payroll_month;
						     	$payroll_date = $p->payroll_date;
						     	
						     	$year = getYear($payroll_date);
						     	 break;
						     	 
						     }
						     
						} 
				 }
				 
				  
			
               
               ?>
               <li><a style="<?php if($controller=='payroll') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll_id.'&from=pay&id_='.$payroll_id.'&month_='.$month.'&year_='.$year.'&di=1"'; ?> ><i class="fa fa-arrow-left"></i><?= Yii::t('app', 'Payroll') ?></a></li> 
			                             
			                             <li><a style="<?php if(($controller=='scholarshipholder')&&( ($action=='index')&&(isset($_GET['from'])) ) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/scholarshipholder/index?from=oth"><i class="fa fa-credit-card"></i><?= Yii::t('app', 'Scholarship holder') ?></a></li>

			                         <!--   <li><a style="<?php if(($controller=='scholarshipholder')&&(in_array($action, ['index_exempt','exempt']) ) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/scholarshipholder/index_exempt?from=bil"><i class="fa fa-credit-card"></i><?= Yii::t('app', 'Exemption') ?></a></li>  -->

			                            
			                        </ul>
                            </li>
   
                         
                         <!-- Stockroom  -->
                    <li  class="<?php if( ($module=='stockrooms') ) echo $li_active; else echo ''; ?>">
                        <a   href="#"> <span class="nav-label"><?= Yii::t('app', 'Stockrooms') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                            <li ><a style="<?php if(  ($controller=='stockroominventory' )  )  echo $a_active_style2; else echo '';  ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroominventory/dashboard?wh="><i class="fa fa-pencil"></i><?= Yii::t('app', 'Inventory') ?></a> </li>
                            
                            <li ><a style="<?php if(  (in_array($controller, $stockroom_array) )  )  echo $a_active_style2; else echo '';  ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroomhasproducts/dashboard?wh=set_str"><i class="fa fa-home"></i><?= Yii::t('app', 'Stockrooms') ?></a> </li> 
                            
                            
			                <li><a style="<?php if(  (in_array($controller, $stockrooms_setting) )  )  echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroom/index?wh=set_str"><i class="fa fa-wrench"></i><?= Yii::t('app', 'Setting') ?></a></li>
			                       
			                   
                        </ul>
                    </li>
                    <!-- Fin Stockroom    -->

   
                        </ul>
                    </li>
                    <!-- Fin Finances -->

                    <!-- Planning -->
                    <li>

                        <a title="<?= Yii::t('app', 'Planning') ?>" style="<?php if( ($module=='planning')&&($controller!='holydays') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/events/coursesummary?wh=pla_sct"><i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Planning') ?></span></a>

                    </li>

                    <li>
                        <a title="<?= Yii::t('app', 'Partners') ?>" style="<?php if($controller=='partners') echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/partners/index"><i class="fa fa-handshake-o"></i><span class="nav-label"> <?= Yii::t('app', 'Partners') ?> </span></a>

                    </li>
                    
                    <?php               
                 //        if( Yii::$app->user->can("portal-menu") ){
                     ?>
                  <!--  <li>
                        <a title="<?= Yii::t('app', 'Site Web') ?>" style="<?php if( ($module=='portal') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/manage-content?wh=pla_pv"><i class="fa fa-globe"></i> <span class="nav-label"> <?= Yii::t('app', 'Site Web') ?> </span></a>
                    </li>
                  -->
                    <?php
                    //     }
                    ?>
                    <!-- Fin Planning -->

                    <!-- Rapports -->
                    <li>
                        <a title="<?= Yii::t('app', 'Reports') ?>" style="<?php if( ($module=='reports')&&(isset($_GET['from1'])) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/list?cat=peda&from1=rpt&"><i class="fa fa-line-chart"></i> <span class="nav-label"> <?= Yii::t('app', 'Reports') ?> </span></a>
                    </li>
      
                    <!-- Fin Rapports -->

    <!--                <li>
                        <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label"><?= Yii::t('app', 'Mail') ?> </span><span class="label label-warning pull-right">16/24</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Inbox') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'See email') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Compose email') ?></a></li>
                        </ul>
                    </li>
      -->

                      <li  class="<?php if( (($module=='fi')||($module=='planning')||($module=='billings'))&&(in_array($controller, $setting_array) ) ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Settings') ?>" style="<?php if( (($module=='fi')||($module=='planning'))&&(in_array($controller, $setting_array) ) ) echo $a_active_style1; else echo ''; ?>" href="#"><i class="fa fa-gears"></i> <span class="nav-label"><?= Yii::t('app', 'Settings') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                              <li ><a style="<?php if(in_array($controller, $setting_general_array) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/generalconfig/index?wh=gc"><i class=""></i><?= Yii::t('app', 'General') ?></a></li>

                                        <li ><a style="<?php if($controller=='program') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/program/index"><i class=""></i><?= Yii::t('app', 'Program') ?></a></li>

			                             <li><a style="<?php if($controller=='rooms') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/rooms/index"><i class=""></i><?= Yii::t('app', 'Room') ?></a></li>

			                             <li><a style="<?php if($controller=='module') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/module/index"><i class=""></i><?= Yii::t('app', 'Module') ?></a></li>

			                             <li><a style="<?php if(($controller=='subjects') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/subjects/index"><i class=""></i><?= Yii::t('app', 'Module label') ?></a></li>

			                             <li><a style="<?php if(($controller=='holydays') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/holydays/index"><i class=""></i><?= Yii::t('app', 'Manage Holidays') ?></a></li>
                                                     <li><a style="<?php if(($controller=='setupmail') ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/setupmail/index"><i class=""></i><?= Yii::t('app', 'Setup Mail') ?></a></li>

                        </ul>
                    </li>


           <?php
    
    	?>
                    <li  >

                        <a title="<?= Yii::t('app', 'User management') ?>" style="<?php if( ($module=='rbac') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/index?wh=use"><i class="fa fa-user-plus"></i> <span class="nav-label"> <?= Yii::t('app', 'User management') ?> </span></a>
                    </li>

        <?php
                     
                      ?>  
                    <li  >

                        <a title="<?= Yii::t('app', 'ID cards') ?>" style="<?php if( ($module=='idcards') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/idcards/idcard/index?wh=prt"><i class="fa fa-id-card"></i> <span class="nav-label"> <?= Yii::t('app', 'ID cards') ?> </span></a>
                    </li>

        <?php

        ?>


         <?php               
             
               
               
             ?>  
               
           
                   