<?php

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;

?>


        
                    <li class="<?php if( ( ($module=='reports')&&($action!='dashboardbilling')&&(! isset($_GET['from']))&&(! isset($_GET['from1'])) )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Dashboard') ?>" style="<?php if( ( ($action=='dashboardpedago')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dashboardpedago"><i class="fa fa-th-large"></i><span class="nav-label"> <?= Yii::t('app', 'Dashboard') ?> </span></a>

                    </li>
  
                    <!-- Formation initiale  -->
                   <!-- <li class="<?php if( ( ($module=='fi')&&(in_array($action,array_merge($action_teach,$action_stud)) )  )|| ( ($module=='fi')&&(in_array($controller,$control_fi) )  )  ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Initial education') ?>" href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Initial education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            
                            <li><a style="<?php if( (in_array($action,$action_teach) )||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/teacher"><?= Yii::t('app', 'Teachers') ?></a></li>
                           --> <!-- A ajouter lors du developpement
                            <li><a class="<?php if($action=='fi') echo $li_active; else echo ''; ?>"href="#"><?= Yii::t('app', 'Attendance') ?></a></li>
                            -->
                            
                            
                            
                    <!--    </ul>
                    </li>
                    -->
                    <!-- Fin formation initiale -->

                    <!-- Education permanante -->
   <!--                 <li>
                        <a href="#"><i class="fa fa-briefcase"></i> <span class="nav-label"><?= Yii::t('app', 'Permanent education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Orientation & Placement') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Business services') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training') ?> continue</a></li>
                            <li><a href="#"><?= Yii::t('app', 'Training in school') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Partners') ?></a></li>

                        </ul>
                    </li>
       -->
                    <!-- Fin Education permanante -->

                    <li class="<?php if( ($module=='fi')&&(in_array($action, array_merge($action_emp,$action_teach) )  )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?>" style="<?php if( (in_array($action, array_merge($action_emp,$action_teach) )  )||(( ($action=='moredetailsemployee') &&($_GET['from']=='emp'))||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) )  ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/employee"><i class="fa fa-user"></i><span class="nav-label"> <?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?> </span></a>

                    </li>
                    
                    
                    <li class="<?php if($module=='inscription') echo $li_active; else echo '';?>" title="<?= Yii::t('app', 'Admission') ?>">
                        
                        <a href="">
                            <!-- <?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/index?wh=ins -->
                          <i class="fa fa-sort-amount-asc"></i>
                             <span class="nav-label">
                                    <?= Yii::t('app', 'Admission') ?>
                            </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse">
                             
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='vains') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/validate?wh=vains">
                                         <?= Yii::t('app', 'Validate inscription') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='exam') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/exam?wh=exam">
                                         <?= Yii::t('app', 'Grades & decision') ?>
                                 </a>
                             </li>
                             <li>
                                 <a style="<?php if(isset($_GET['wh'])&&$_GET['wh']=='rpt') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/inscription/postulant/report?wh=rpt">
                                         <?= Yii::t('app', 'Reports') ?>
                                 </a>
                             </li>
                             
                         </ul>
                     </li>



                    <!-- Finances -->
                    <li  class="<?php if( ($module=='billings')&&($controller!='partners') ) echo $li_active; else echo ''; ?>">
                        <a  title="<?= Yii::t('app','Finance'); ?>" href="#"><i class="fa fa-dollar"></i> <span class="nav-label"><?= Yii::t('app', 'Finance') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                            <li class="<?php if( ($module=='billings')||(in_array($action,$action_rpt)&&(isset($_GET['from'])&&($_GET['from']=='bil') ) )   ) echo $li_active; else echo ''; ?>">
                                    <a style="<?php if(in_array($controller, $billing_array) ) echo $a_active_style1; else echo ''; ?>" href="#"><span class="nav-label"><?= Yii::t('app', 'Billing') ?></span><span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level collapse">
			                           
 <?php
                       //payroll individyel
                       $person_id = 0;
                       $payroll_id = 0;
                       $month=0;
                       $year =0;
                       $payroll_date = '';
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id!=NULL)
				           {  
				             if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
				                 	
				                  }
				           
				            }
				        

                       //last payroll pou moun sa
                      $modelPayroll = new SrcPayroll;
                      
                       $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$payroll_id = $p->id;
						     	$month = $p->payroll_month;
						     	$payroll_date = $p->payroll_date;
						     	
						     	$year = getYear($payroll_date);
						     	 break;
						     	 
						     }
						     
						} 
				 }
				 
				  
			
               
               ?>
                                           <li><a style="<?php if($controller=='payroll') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll_id.'&from=pay&id_='.$payroll_id.'&month_='.$month.'&year_='.$year.'&di=1"'; ?> ><i class="fa fa-arrow-left"></i><?= Yii::t('app', 'Payroll') ?></a></li>
               
			                             <li><a style="<?php if(in_array($controller, ['payroll','chargepaid']) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/chargepaid/index?di=2&from=b"><i class="fa fa-arrow-left"></i><?= Yii::t('app', 'Expenses') ?></a></li>

			                             <li><a style="<?php if($controller=='loanofmoney') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/loanofmoney/index?part=pay&from=stud"><i class="fa fa-university"></i><?= Yii::t('app', 'Loan') ?></a></li>

			                            <!-- <li><a style="<?php if(  ($module=='reports')&&($controller=='report')&&($action=='')   )  echo $a_active_style2; else echo ''; ?>" href="#"><i class="fa fa-line-chart"></i><?= Yii::t('app', 'Report') ?></a></li>  -->           
                                 
                                  
			                            
			                        </ul>
                            </li>
   <!--                         <li><a href="site/magasin"><?= Yii::t('app', 'Store') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Inventory') ?></a></li>
    -->
                        </ul>
                    </li>
                    <!-- Fin Finances -->

                    <!-- Planning -->
                    <li>

                        <a title="<?= Yii::t('app', 'Planning') ?>" style="<?php if( ($module=='planning')&&($controller!='holydays') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/events/coursesummary?wh=pla_sct"><i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Planning') ?></span></a>

                    </li>

                    <li>
                        <a title="<?= Yii::t('app', 'Partners') ?>" style="<?php if($controller=='partners') echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/billings/partners/index"><i class="fa fa-handshake-o"></i><span class="nav-label"> <?= Yii::t('app', 'Partners') ?> </span></a>

                    </li>
                    <!-- Fin Planning -->
                    
                        
                       
                    <li  >

                        <a title="<?= Yii::t('app', 'ID cards') ?>" style="<?php if( ($module=='idcards') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/idcards/idcard/index?wh=prt"><i class="fa fa-id-card"></i> <span class="nav-label"> <?= Yii::t('app', 'ID cards') ?> </span></a>
                    </li>

        

                    <!-- Rapports -->
                    <li>
                        <a title="<?= Yii::t('app', 'Reports') ?>" style="<?php if( ($module=='reports')&&(isset($_GET['from1'])) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/list?cat=peda&from1=rpt&"><i class="fa fa-line-chart"></i> <span class="nav-label"> <?= Yii::t('app', 'Reports') ?> </span></a>
                    </li>
      
                    <!-- Fin Rapports -->

    <!--                <li>
                        <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label"><?= Yii::t('app', 'Mail') ?> </span><span class="label label-warning pull-right">16/24</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Inbox') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'See email') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Compose email') ?></a></li>
                        </ul>
                    </li>
      -->

                    


           <?php
    
    	?>
                   

        <?php
                 

        ?>


         <?php               
             
               
               
             ?>  
               
         
                    