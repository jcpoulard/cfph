<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use app\modules\fi\models\SrcPersons;

use app\modules\rbac\models\User;
use app\modules\rbac\models\searchs\User as UserSearch;


AppAsset::register($this);

$school_acronym = infoGeneralConfig('school_acronym');

$modelUser = new User;


 $person_id = 0;
 $student_parent_guest=0;
 $modelUser = User::findOne(Yii::$app->user->identity->id); 

    if($modelUser->person_id==NULL)
       {  $person_id = 0;


       }
    else
       { if( ($modelUser->is_parent==0) ) //c pa yon paran
             {  $person_id = $modelUser->person_id;

                //gade si se yon elev
               $modelPers = SrcPersons::findOne($person_id);
                  if($modelPers->is_student==1)
                      $student_parent_guest=1;
              }
          elseif( ($modelUser->is_parent==1) ) //c yon paran
             {

                $student_parent_guest=1;
             }

        }





?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Object du template INSPINIA  -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?= Html::csrfMetaTags() ?>
    <title>Sytème Gestion Canado Technique</title>
    <?php $this->head() ?>
</head>
<!--
/*  for the collapse side menu */

    <body class="mini-navbar">

-->

<!--for the none collapse side menu just use body -->

<body>
<?php $this->beginBody() ?>

<?php

$li_active='active';
$a_active_style1="background-color:#4492AB;";
$a_active_style2="background-color:#4399FC;";


$current_url = "$_SERVER[REQUEST_URI]";
$explode_url= explode("/",substr($_SERVER['REQUEST_URI'], 1));

 $module = '';

 $controller = '';

 $action_ = '';

if(isset($explode_url[3]))
 $module = $explode_url[3];

if(isset($explode_url[4]))
$controller = $explode_url[4];

if(isset($explode_url[5]))
$action_ = $explode_url[5];

$explode_action_= explode("?",substr($action_, 0));

$action = '';
if(isset($explode_action_[0]))
  $action = $explode_action_[0];

$action_stud = ['student','student#','moredetailsstudent','updatestudent','createstudent',];
$action_teach = ['teacher','teacher#','updateteaher','createteacher'];
$action_emp = ['employee','employee#','updateemployee','createemployee'];

$action_rpt = ['dashboardbilling','dashboardpedago','list','onataxes','dgitaxes'];

$control_fi = ['courses','studenthascourses','grades','studentlevel'];

$billing_setting = ['feeslabel','payrollsettings','fees','taxes',];
$billing_array = ['billings','billingsservices','payroll','loanofmoney', 'payrollplusvalue', 'scholarshipholder','feeslabel','payrollsettings','fees','taxes','chargepaid',];
$setting_array = ['academicperiods','shifts','holydays','rooms','program','module','subjects','margeechec','settings','departmentinschool','titles','relations','jobstatus','customfield','paymentmethod','devises'];
$setting_general_array = ['generalconfig','academicperiods','shifts','departmentinschool','titles','relations','jobstatus','customfield','paymentmethod','devises'];

$grade_array = ['create','update','index',];

$stockrooms_setting = ['stockroom','stockroomproductslabel','stockroomequipment','stockroomrawmaterial','stockroomfurniture','marque','local'];
$stockroom_array = ['stockroomhasproducts','stockroomstockusage','stockroomrestock'];
$action_stockroom = ['create',];

?>


<!-- Debut du template INSPINIA  -->
<div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
           <div class="sidebar-collapse"><!-- sidebar-collapse -->
               <ul class="nav metismenu" id="side-menu">

                   <li class="nav-header">

                    <div class="dropdown profile-element">
                           <span >
              <img alt="<?= Yii::t('app', 'logo') ?>" style="width: 130px;" class="img-circle" src=" <?= Url::to("@web/img/Logo_Cana_Reel_G.png") ?> " />
                          </span>
                          
                      </div>
       
                        <div class="logo-element">
                            <h2> <?php echo 'CFPH';//echo $school_acronym; ?> </h2>
                        </div>
                     
                    </li>

         <?php  
            if(isset(Yii::$app->user->identity)&&( (isset(Yii::$app->session['currentId_academic_year']))&&(Yii::$app->session['currentId_academic_year']!='') ) )
               {
               	   if( (Yii::$app->user->can("superadmin")) || ( (Yii::$app->user->can("superadmin"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_super.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("direction")) || ( (Yii::$app->user->can("direction"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_direction.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Administrateur systeme")) || ( (Yii::$app->user->can("Administrateur systeme"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_adminSystem.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Direction des études")) || ( (Yii::$app->user->can("Direction des études"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_directionEtudes.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Administration études")) || ( (Yii::$app->user->can("Administration études"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_administrationEtudes.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Administration économat")) || ( (Yii::$app->user->can("Administration économat"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_administrationBillings.php';
               	         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Administration recettes")) || ( (Yii::$app->user->can("Administration recettes"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_administrationIncome.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';
               	     }
               	   elseif( (Yii::$app->user->can("Administration dépenses")) || ( (Yii::$app->user->can("Administration dépenses"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_administrationExpenses.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';

               	     }
               	   elseif( (Yii::$app->user->can("Magasinier")||Yii::$app->user->can("Magasinier manager")) || ( (Yii::$app->user->can("Magasinier")||Yii::$app->user->can("Magasinier manager"))&&(Yii::$app->user->can("teacher")) )  )
               	     {
               	         if(Yii::$app->session['profil_as']==0)//se yon employee
                              include 'sideMenu_magasinier.php';
                         elseif(Yii::$app->session['profil_as']==1)//se yon teacher
                             include 'sideMenu_teacher.php';

               	     }
               	   elseif(Yii::$app->user->can("teacher"))
               	     {
               	         include 'sideMenu_teacher.php';
               	     }
               	    elseif(Yii::$app->user->can("guest"))
               	     {
               	         //si se paran ou elev
                          if($student_parent_guest==1)
                             include 'sideMenu_guest.php';
                          
               	     }
                   
               	       
                   include 'sideMenu_portal.php';
                  


               }
             ?>


                </ul>

            </div>
        </nav>



  </div>
<!-- Fin du menu lateral -->




   <div id="page-wrapper" class="gray-bg dashbard-1">
     <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">

        <!-- Zone de recherche -->
       <div class="navbar-header">
            <?php
               if(isset(Yii::$app->user->identity)&&( (isset(Yii::$app->session['currentId_academic_year']))&&(Yii::$app->session['currentId_academic_year']!='') ) )
                 {
             ?>
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <?php
                 }
            ?>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <!-- <input type="text" placeholder="Rechercher..." class="form-control" name="top-search" id="top-search">  -->
                    <?php
			             $url=Yii::$app->request->referrer; //Yii::app()->request->urlReferrer;//(<-yii1)
			                // $explode_url= explode("php",substr($url,0));
							 echo ' <div style="margin-top:13px;margin-left:13px;"><a class=" btn btn-default" style="height:32px;" href="'.$url.'" class="btn btn-default"> Back </a></div>';
			           ?>
                </div>
            </form>
        </div>

        <div class="navbar-middle">

    <!--      <span>
              <img alt="<?= Yii::t('app', 'logo') ?>" style="width: 130px;" class="img-circle" src=" <?= Url::to("@web/img/Logo_Cana_Reel_G.png") ?> " />
               </span>
      -->

       </div>

            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message"><?= Yii::t('app', 'Welcome to ').$school_acronym ?></span>
                </li>
                <?php 
        
                if(Yii::$app->session['user_profil']==2)//alafwa employee-teacher
                    {
                       if( (  ( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("Magasinier")||Yii::$app->user->can("Magasinier manager")) )&&(Yii::$app->user->can("teacher")) )  )
                         {
                   ?>
                <li >
                    <span class="m-r-sm text-muted welcome-message">
                       <i>    <?php echo Yii::t('app', 'View as').': '; ?>
                              <span> <a style="padding:0px 0px; color:red; background-color:#DBDBDC;" class="dropdown-toggle count-info" data-toggle="dropdown" href="#"><i class="caret"></i>
                           <?php   if(Yii::$app->session['profil_as']==0)
                                       echo Yii::t('app', 'Employee');
                                   elseif(Yii::$app->session['profil_as']==1)
                                       echo Yii::t('app', 'Teacher');

                            ?></a>

                           <ul class="dropdown-menu animated fadeInRight s-t-ms" style="padding:0px;">
                              <li class="divider" style="margin-top:0px; margin-bottom:-3px; padding:0px;"></li>
                              <?php if(Yii::$app->session['profil_as']==0)//se yon employee, bal opsyon teacher
                                      {
                                  ?>
                              <li style="padding:0px;"><a href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/changeprofil?profil=1"><?= Yii::t('app', 'Teacher') ?></a></li>
                              <?php  }
                                   elseif(Yii::$app->session['profil_as']==1)//se yon teacher, bal opsyon employee
                                      {
                                  ?>
                              <li style="padding:0px;"><a href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/changeprofil?profil=0"><?= Yii::t('app', 'Employee') ?></a></li>
                              <?php }
                               ?>
                                <li class="divider" style="margin-top:-3px; margin-bottom:1px; padding:0px;"></li>

                            </ul>

                            </span>
                       </i>
                    </span>
                </li>
                <?php     }
                         elseif( (  ( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("Magasinier")||Yii::$app->user->can("Magasinier manager")) ) )  )       
                          {
                             Yii::$app->session['profil_as']=0;
                           }
                        elseif( (Yii::$app->user->can("teacher")) ) 
                          {
                                 Yii::$app->session['profil_as']=1;
                             }
                          
                
                      }
                
                
                     ?>

     <!--
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
             -->



               <li>
                 <?php
                        if(isset(Yii::$app->user->identity)&&( (isset(Yii::$app->session['currentId_academic_year']))&&(Yii::$app->session['currentId_academic_year']!='') ) )
                        {
                 ?>
                   <div class="dropdown profile-element">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                  <b><?php echo $modelUser->getFullNameByUserId(Yii::$app->user->identity->id); ?></b> <b class="caret"></b>  </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <?php
                                      if(Yii::$app->session['profil_as']==1)
                                       {
                                ?>
                                <li><a href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/moredetailsemployee?id=<?=$person_id ?>&is_stud=0&from=emp"><?= Yii::t('app', 'Update personal info') ?></a></li>  
                                <?php
                                       }
                                ?>
                                <li><a href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/changepassword"><?= Yii::t('app', 'Change password') ?></a></li>
                              <!--    <li class="dropdown" ><a class="dropdown-toggle count-info" href="#"><i class="fa fa-envelope"></i><?= Yii::t('app', 'Mail') ?>  <span class="label label-warning">16</span></a></li>     -->
                                <li class="divider"></li>
        <?php
                if(Yii::$app->user->isGuest)
                  echo '<li>'.Html::a('<i class="fa fa-sign-in">&nbsp'.Yii::t('app','Login').'</i>', ['/rbac/user/login'],['data-method' => 'post']).'</li>';
                else
                   echo '<li>'.Html::a('<i class="fa fa-sign-out">&nbsp'.Yii::t('app','Logout').'('.Yii::$app->user->identity->username.')</i>', ['/rbac/user/logout'],['data-method' => 'post']).'</li>';
             ?>
                              <!--   <li><a href="#"><?= Yii::t('app', 'Logout') ?></a></li> -->
                            </ul>
                        </div>
                <?php
                        }
                       else
                         {
                         	if(isset(Yii::$app->user->identity) )
                         	    echo '<li>'.Html::a('<i class="fa fa-sign-out">&nbsp'.Yii::t('app','Logout').'('.Yii::$app->user->identity->username.')</i>', ['/rbac/user/logout'],['data-method' => 'post']).'</li>';
                         	else
                         	    echo '<li>'.Html::a('<i class="fa fa-sign-in">&nbsp'.Yii::t('app','Login').'</i>', ['/rbac/user/login'],['data-method' => 'post']).'</li>';

                         }
                 ?>

                </li>

          <!--       <li>

                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Déconnecter
                    </a>


                     <?php
                if(Yii::$app->user->isGuest)
                  echo '<li>'.Html::a('<i class="fa fa-sign-in">&nbsp'.Yii::t('app','Login').'</i>', ['/rbac/user/login'],['data-method' => 'post']).'</li>';
                else
                   echo '<li>'.Html::a('<i class="fa fa-sign-out">&nbsp'.Yii::t('app','Logout').'</i>', ['/rbac/user/logout'],['data-method' => 'post']).'</li>';
             ?>
                </li>
               -->
           <?php
            if(isset(Yii::$app->user->identity)&&( (isset(Yii::$app->session['currentId_academic_year']))&&(Yii::$app->session['currentId_academic_year']!='') ) )
               {
          ?>
               <li>
                    <a class="right-sidebar-toggle">
                       <!-- <i class="fa fa-tasks"></i> -->
                    </a>
                </li>
            <?php
               }
            ?>
            </ul>

        </nav>
    </div>




<!-- Debut Widget 2 -->
        <div class="row">
            <div class="col-lg-12">
                <div class=""> <!-- wrapper wrapper-content -->

                <!-- Get all flash messages and loop through them  -->
<?php foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
            <?php
            echo \kartik\widgets\Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Message Not Set!',
                'showSeparator' => true,
                'delay' => 1, //This delay is how long before the message shows
                'pluginOptions' => [
                    'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            ?>
        <?php endforeach; ?>

                    <?= $content; ?>
                 </div>
             <br/>

            </div>
      </div>
<div class="clear"></div>
<div class="row">
      <div class="footer">
        <div class="pull-right">
           <!-- 10GB de <strong>250GB</strong> disponible.-->
        </div>
        <div>
            <strong>Tous droits réservés</strong> LOGIPAM &copy; 2017
        </div>

<?php
$count_user_online = 0;

$users = new UserSearch();
foreach($users->getOnlineUsers() as $user){
    $count_user_online++;
    
}

?>        
        
             <div class="pull-right" style="margin-top:-50px;text-align:right;"> 
                           <?php  if(Yii::$app->user->can('superadmin')||Yii::$app->user->can('direction')){ ?> 
                            <a style="color: #EE642E" href="<?php echo Yii::getAlias('@web').'/index.php/rbac/user/viewonlineusers?wh=useon' ?>"><i class="fa fa-user text-success"></i> <?php echo Yii::t('app',' {count} user(s) online', ['count'=>$count_user_online]) ?></a>
                           <?php  } ?> 
             </div>
    </div>
</div>

 </div>


<!-- Fin du template INSPINIA -->

<!-- JavaScript des données de test du template INSPINIA -->



<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
<?php
/*
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'><div style=\"text-align:center\"><img src=\"../img/angular_logo.png\"></div></div>";
yii\bootstrap\Modal::end();
 * 
 */

/*
$_SESSION['loggedAt']=time();
if(time() - $_SESSION['loggedAt'] > 30) { //subtract new timestamp from the old one
    echo"<script>alert('15 Minutes over!');</script>";
    unset($_SESSION['loggedAt']);
    //$_SESSION['logged_in'] = false;
    header("Location: " . index.php); //redirect to index.php
    exit;
} else {
    $_SESSION['loggedAt'] = time(); //set new timestamp
}
*/ 
?>


