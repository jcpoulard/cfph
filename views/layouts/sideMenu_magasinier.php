<?php

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;

?>


        
                   <li class="<?php if( ( ($module=='reports')&&($action!='dashboardbilling')&&(! isset($_GET['from']))&&(! isset($_GET['from1'])) )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Dashboard') ?>" style="<?php if( ( ($action=='dashboardpedago')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dashboardpedago"><i class="fa fa-th-large"></i><span class="nav-label"> <?= Yii::t('app', 'Dashboard') ?> </span></a>

                    </li>
                    
                    <!-- Formation initiale  -->
                    <li class="<?php if( ( ($module=='fi')&&(in_array($action,array_merge($action_teach,$action_stud)) )  )|| ( ($module=='fi')&&(in_array($controller,$control_fi) )  )  ) echo $li_active; else echo ''; ?>">
                        <a title="<?= Yii::t('app', 'Initial education') ?>" href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Initial education') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                           <!-- <li><a style="<?php if( (in_array($action,$action_teach) )||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/teacher"><?= Yii::t('app', 'Teachers') ?></a></li>
                            
                              <li><a style="<?php if(in_array($action,$action_stud) ) echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/student"><?= Yii::t('app', 'Students') ?></a></li>
                            -->
                            <li><a style="<?php if($controller=='studentlevel') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/studentlevel/index"><?= Yii::t('app', 'Manage degree') ?></a></li>
                        </ul>
                    </li>
                    <!-- Fin formation initiale -->

                    <li class="<?php if( ($module=='fi')&&(in_array($action, array_merge($action_emp,$action_teach) )  )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?>" style="<?php if( (in_array($action, array_merge($action_emp,$action_teach) )  )||(( ($action=='moredetailsemployee') &&($_GET['from']=='emp'))||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) )  ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/employee"><i class="fa fa-user"></i><span class="nav-label"> <?= Yii::t('app', 'Employees').'/'.Yii::t('app', 'Teachers') ?> </span></a>

                    </li>




                    <!-- Finances -->
                    <li  class="<?php if( ( ($module=='billings')||($module=='stockrooms') )&&($controller!='partners') ) echo $li_active; else echo ''; ?>">
                        <a  title="<?= Yii::t('app','Finance'); ?>" href="#"><i class="fa fa-dollar"></i> <span class="nav-label"><?= Yii::t('app', 'Finance') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                          
                          <!-- Billings -->
                            <li class="<?php if($module=='billings') echo $li_active; else echo ''; ?>">
                                    <a style="<?php if(in_array($controller, $billing_array) ) echo $a_active_style1; else echo ''; ?>" href="#"><span class="nav-label"><?= Yii::t('app', 'Billing') ?></span><span class="fa arrow"></span></a>
                                  <ul class="nav nav-second-level collapse">
			                     <?php
                       //payroll individyel
                       $person_id = 0;
                       $payroll_id = 0;
                       $month=0;
                       $year =0;
                       $payroll_date = '';
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id!=NULL)
				           {  
				             if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
				                 	
				                  }
				           
				            }
				        

                       //last payroll pou moun sa
                      $modelPayroll = new SrcPayroll;
                      
                       $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$payroll_id = $p->id;
						     	$month = $p->payroll_month;
						     	$payroll_date = $p->payroll_date;
						     	
						     	$year = getYear($payroll_date);
						     	 break;
						     	 
						     }
						     
						} 
				 }
				 
				  
			
               
               ?>
               <li><a style="<?php if($controller=='payroll') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll_id.'&from=pay&id_='.$payroll_id.'&month_='.$month.'&year_='.$year.'&di=1"'; ?> ><i class="fa fa-arrow-left"></i><?= Yii::t('app', 'Payroll') ?></a></li>
			                             
			                        </ul>
                            </li>
                       
                      
                         <!-- Stockroom  -->
                    <li  class="<?php if( ($module=='stockrooms') ) echo $li_active; else echo ''; ?>">
                        <a   href="#"><span class="nav-label"> <?= Yii::t('app', 'Stockrooms') ?></span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">

                            <li ><a style="<?php if(  ($controller=='stockroominventory' )  )  echo $a_active_style2; else echo '';  ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroominventory/dashboard?wh="><i class="fa fa-pencil"></i><?= Yii::t('app', 'Inventory') ?></a> </li>
                            
                            <li ><a style="<?php if(  (in_array($controller, $stockroom_array) )  )  echo $a_active_style2; else echo '';  ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroomhasproducts/dashboard?wh=set_str"><i class="fa fa-home"></i><?= Yii::t('app', 'Stockrooms') ?></a> </li> 
                            
                            
			                <li><a style="<?php if(  (in_array($controller, $stockrooms_setting) )  )  echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/stockrooms/stockroom/index?wh=set_str"><i class="fa fa-wrench"></i><?= Yii::t('app', 'Setting') ?></a></li>
			                       
			                   
                        </ul>
                    </li>
                    <!-- Fin Stockroom    -->
 
                       
                       
                        </ul>
                    </li>
                    <!-- Fin Finances -->

                    <!-- Planning -->
                    <li>

                        <a title="<?= Yii::t('app', 'Planning') ?>" style="<?php if( ($module=='planning')&&($controller!='holydays') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/events/coursesummary?wh=pla_sct"><i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Planning') ?></span></a>

                    </li>
                    
                  
                    
                   
                        </ul>
                    </li>


           
               
               