<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
    
 <!--   <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_sh') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Scholarship holder').'</span>', ['../billings/scholarshipholder/index','wh'=>'set_sh'], ['title'=>Yii::t('app','Scholarship holder')]) ?></li>
   -->
   <?php
            if( (!Yii::$app->user->can("Administrateur systeme"))&&(!Yii::$app->user->can("Direction des études"))  )
              {
       ?>
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_pa') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Payroll').'</span>', ['../billings/payrollsettings/index','wh'=>'set_pa'], ['title'=>Yii::t('app','Payroll')]) ?></li>
   
   <?php
              }
   ?>
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_ta') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Taxes').'</span>', ['../billings/taxes/index','wh'=>'set_ta'], ['title'=>Yii::t('app','Taxes')]) ?></li>

       <?php
            if( isset(Yii::$app->user->identity->username)&&(Yii::$app->user->identity->username=='_super_') ) 
              {
       ?>
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_ba') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','IRI').'</span>', ['../billings/bareme/index','wh'=>'set_ba'], ['title'=>Yii::t('app','IRI')]) ?></li>
        <?php
                }
        ?>
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_id') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Other income description').'</span>', ['../billings/otherincomesdescription/index','wh'=>'set_id'], ['title'=>Yii::t('app','Other income description')]) ?></li>
    
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_cd') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Charge description').'</span>', ['../billings/chargedescription/index','wh'=>'set_cd'], ['title'=>Yii::t('app','Charge description')]) ?></li>
 
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_fl') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Fees description').'</span>', ['../billings/feeslabel/index','wh'=>'set_fl'], ['title'=>Yii::t('app','Fees description')]) ?></li>
    
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_f') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Fees').'</span>', ['../billings/fees/index','wh'=>'set_f'], ['title'=>Yii::t('app','Fees')]) ?></li>
    
    

    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_fs') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Fee Services').'</span>', ['../billings/feeservices/index','wh'=>'set_fs'], ['title'=>Yii::t('app','Fee Services')]) ?></li>
    
    

    
    
</ul>