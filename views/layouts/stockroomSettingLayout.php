<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>  

<ul class="nav nav-tabs">
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_loc') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Local').'</span>', ['../stockrooms/local/index','wh'=>'set_loc'], ['title'=>Yii::t('app','Local')]) ?></li>
   
   <?php
            
       ?>
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_str') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Stockroom').'</span>', ['../stockrooms/stockroom/index','wh'=>'set_str'], ['title'=>Yii::t('app','Stockroom')]) ?></li>
   
   <?php
             
      ?>
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_eq') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Equipment').'</span>', ['../stockrooms/stockroomequipment/index','wh'=>'set_eq'], ['title'=>Yii::t('app','Equipment')]) ?></li>
    
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_rm') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Raw material').'</span>', ['../stockrooms/stockroomrawmaterial/index','wh'=>'set_rm'], ['title'=>Yii::t('app','Raw material')]) ?></li>
 
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_fur') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Furniture').'</span>', ['../stockrooms/stockroomfurniture/index','wh'=>'set_fur'], ['title'=>Yii::t('app','Furniture')]) ?></li>
    
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_spl') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Products label').'</span>', ['../stockrooms/stockroomproductslabel/index','wh'=>'set_spl'], ['title'=>Yii::t('app','Products label')]) ?></li>

       <?php
            
       ?>
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_mar') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Marque').'</span>', ['../stockrooms/marque/index','wh'=>'set_mar'], ['title'=>Yii::t('app','Marque')]) ?></li>
        <?php
                
        ?>
        
    
</ul>