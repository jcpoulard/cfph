<?php

use app\modules\rbac\models\User;

use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcPayroll;

use app\modules\billings\models\SrcLoanOfMoney;



$acad=Yii::$app->session['currentId_academic_year'];
?>


                     <li class="<?php if( ( ($module=='reports')&&($action!='dashboardbilling')&&(! isset($_GET['from']))&&(! isset($_GET['from1'])) )  ) echo $li_active; else echo ''; ?>"><a title="<?= Yii::t('app', 'Dashboard') ?>" style="<?php if( ( ($action=='dashboardpedago')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/dashboardpedago"><i class="fa fa-th-large"></i><span class="nav-label"> <?= Yii::t('app', 'Dashboard') ?> </span></a>

                    </li>
                    
                    <!-- Formation initiale  -->
                    
                            <li><a title="<?= Yii::t('app', 'Teachers') ?>" style="<?php if( (in_array($action,$action_teach) )||( ($action=='moredetailsemployee')&&($_GET['from']=='teach')) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/teacher"> <i class="fa fa-user"></i> <span class="nav-label"><?= Yii::t('app', 'Teachers') ?></span></a></li>
                            <li><a title="<?= Yii::t('app', 'Students') ?>" style="<?php if(in_array($action,$action_stud) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/persons/student?from=teach"><i class="fa fa-graduation-cap"></i> <span class="nav-label"><?= Yii::t('app', 'Students') ?></span></a></li>
                            
                            
                            <li><a title="<?= Yii::t('app', 'Grades') ?>" style="<?php if( ($controller=='grades')&&(in_array($action,$grade_array) ) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/grades/create"> <i class="fa fa-pencil"></i> <span class="nav-label"><?= Yii::t('app', 'Grades') ?></span></a></li>
                            
                         
                    <!--    </ul>  -->
                    
                    <!-- Fin formation initiale -->

                    <!-- Education permanante -->

                    <!-- Fin Education permanante -->


                  <!-- Finances -->
        <?php
                       //payroll individyel
                       $person_id = 0;
                       $payroll_id = 0;
                       $month=0;
                       $year =0;
                       $payroll_date = '';
					    $modelUser = User::findOne(Yii::$app->user->identity->id); 

				        if($modelUser->person_id!=NULL)
				           {  
				             if( ($modelUser->is_parent==0) ) //c pa yon paran
				                 {  $person_id = $modelUser->person_id;
				                 	
				                  }
				           
				            }
				        

                       //last payroll pou moun sa
                      $modelPayroll = new SrcPayroll;
                      
                       $last_payroll_info=$modelPayroll->getInfoLastPayrollByPerson($person_id);
		  
			
				if(isset($last_payroll_info)&&($last_payroll_info!=null))
				 {
					  $payroll=$last_payroll_info->getModels();//return a list of ... objects
				           
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						     {  
						     	$payroll_id = $p->id;
						     	$month = $p->payroll_month;
						     	$payroll_date = $p->payroll_date;
						     	
						     	$year = getYear($payroll_date);
						     	 break;
						     	 
						     }
						     
						} 
				 }
				 
				  
			
               
               ?>
               <li><a style="<?php if($controller=='payroll') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/payroll/view?id='.$payroll_id.'&from=pay&id_='.$payroll_id.'&month_='.$month.'&year_='.$year.'&di=1"'; ?> ><i class="fa fa-dollar"></i><?= Yii::t('app', 'Payroll') ?></a></li>
               
              <!-- <li><a style="<?php if($controller=='loanofmoney') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/loanofmoney/index?from=teach"'; ?> ><i class="fa fa-dollar"></i><?= Yii::t('app', 'Loan') ?></a></li>
               -->
               
               <?php
                       $person_id = 0; 
                       $loan_id = 0;
                       
          	               $modelUser = User::findOne(Yii::$app->user->identity->id); 

				     if($modelUser->person_id!=NULL)
				         $person_id = $modelUser->person_id;
                                
                                    $searchModel = new SrcLoanOfMoney();
                                     
                              if($person_id!=0)
                                {
                                 $datas = $searchModel->searchForOne($acad,$person_id)->getModels();
                                  foreach($datas as $dat)
                                    {
                                       $loan_id = $dat->id;     
                                    }
                                    
                                }
                                
                                
               if($loan_id != 0)
                {
               ?>
               
               
               <li><a style="<?php if($controller=='loanofmoney') echo $a_active_style2; else echo ''; ?>" href="<?php echo Yii::getAlias('@web').'/index.php/billings/loanofmoney/view?id='.$loan_id.'&from=teach"'; ?> ><i class="fa fa-dollar"></i><?= Yii::t('app', 'Loan') ?></a></li>
               
	        <?php
                
                    }
                
                ?>
               
			                                           <!-- Fin Finances -->

                    
                    
                    <!-- Planning -->
                    <li>

                        <a title="<?= Yii::t('app', 'Planning') ?>" style="<?php if( ($module=='planning')&&($controller!='holydays') ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/planning/events/coursesummary?wh=pla_sct"><i class="fa fa-calendar"></i> <span class="nav-label"><?= Yii::t('app', 'Planning') ?></span></a>

                    </li>

                    
                    <!-- Fin Planning -->

                    <!-- Rapports -->
   <!--                 <li>
                        <a title="<?= Yii::t('app', 'Reports') ?>" style="<?php if( ($module=='reports')&&(isset($_GET['from1'])) ) echo $a_active_style1; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/reports/report/list?cat=peda&from1=rpt&"><i class="fa fa-line-chart"></i> <span class="nav-label"> <?= Yii::t('app', 'Reports') ?> </span></a>
                    </li>
      -->
                    <!-- Fin Rapports -->

    <!--                <li>
                        <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label"><?= Yii::t('app', 'Mail') ?> </span><span class="label label-warning pull-right">16/24</span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="#"><?= Yii::t('app', 'Inbox') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'See email') ?></a></li>
                            <li><a href="#"><?= Yii::t('app', 'Compose email') ?></a></li>
                        </ul>
                    </li>
      -->

                      <li><a title="<?= Yii::t('app', 'Courses') ?>" style="<?php if($controller=='courses') echo $a_active_style2; else echo ''; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/fi/courses/index"><i class="fa fa-list-alt"></i><span class="nav-label"><?= Yii::t('app', 'Courses') ?></span></a></li>

			        

           <?php
    
    	?>
                   

        <?php
                 

        ?>


         <?php               
             
               
               
             ?>  
               
               