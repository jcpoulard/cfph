<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
    
 <?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='ap') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Academic year').'</span>', [ '../fi/academicperiods/index','wh'=>'ap'], ['title'=>Yii::t('app','Academic year')]) ?></li>
   
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='shi') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Shift').'</span>', [ '../fi/shifts/index','wh'=>'shi'],['title'=>Yii::t('app','Shifts')]) ?></li>
   
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='dep') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Department in school').'</span>', [ '../fi/departmentinschool/index','wh'=>'dep'],['title'=>Yii::t('app','Department in school')]) ?></li>
   
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='tit') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Titles').'</span>', [ '../fi/titles/index','wh'=>'tit'],['title'=>Yii::t('app','Title')]) ?></li>
   
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='job') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Job Status').'</span>', [ '../fi/jobstatus/index','wh'=>'job'],['title'=>Yii::t('app','Job Status')]) ?></li>
    
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='rel') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Relations').'</span>', [ '../fi/relations/index','wh'=>'rel'],['title'=>Yii::t('app','Relations')]) ?></li>
    
    <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Direction des études"))||(Yii::$app->user->can("Administration études"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
   
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='cus') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Custom field').'</span>', [ '../fi/customfield/index','wh'=>'cus'],['title'=>Yii::t('app','Custom field')]) ?></li>
    
    
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_pm') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Payment method').'</span>', [ '../billings/paymentmethod/index','wh'=>'set_pm'], ['title'=>Yii::t('app','Payment method')]) ?></li>
    
   <?php
          }
   ?>

<?php
        if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Administrateur systeme"))||(Yii::$app->user->can("Administration économat"))||(Yii::$app->user->can("Administration recettes"))||(Yii::$app->user->can("Administration dépenses"))||(Yii::$app->user->can("manager"))  )
          {
 
 ?>   
    
    <li class="<?php if(isset($_GET['wh']) && $_GET['wh']=='set_dev') echo "active"; ?>"><?= Html::a('<span>'.Yii::t('app','Devises').'</span>', [ '../billings/devises/index','wh'=>'set_dev'], ['title'=>Yii::t('app','Devises')]) ?></li>
    
   <?php
          }
   ?>

  
    
    
</ul>