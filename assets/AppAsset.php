<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    // Ajout les elements du CSS ici pour utiliser le template
    public $css = [
        
        'css/site.css',
        'css/bootstrap.min.css',
        'https://use.fontawesome.com/releases/v5.5.0/css/all.css',
        'css/font-awesome/css/font-awesome.css',
        'css/plugins/toastr/toastr.min.css',
        'js/plugins/gritter/jquery.gritter.css',
        // Ajout de step 
        //'css/plugins/iCheck/custom.css',
        //'css/plugins/steps/jquery.steps.css',
        'css/animate.css',
        'css/style.css',
        'css/plugins/footable/footable.core.css',
        
        //'css/tableur.css',
        
        // Ajouter full calendar
        // 'fullcalendar/fullcalendar.css',
        'fullcalendar/fullcalendar.min.css',
        'fullcalendar/lib/cupertino/jquery-ui.min.css',
        'css/plugins/dataTables/datatables.min.css',
        'css/plugins/select2/select2.min.css',
        'css/plugins/datapicker/datepicker3.css',
        'css/bootstrap-timepicker.min.css',
        
        'css/steps.css',
        
        'css/editable/css/bootstrap-editable.css',
        // Css pour les graphes 
        'css/plugins/morris/morris-0.4.3.min.css',
        'css/plugins/chartist/chartist.min.css',
        'css/AdminLTE.min.css',
        'css/custom.css',
        
       
    
  
        
         
        
    ];
    public $js = [
        
        //"js/jquery-3.1.1.min.js",    //sa anpeche Select2 yo afiche, yo still ap woule
        "js/ajax-modal-popup.js",
        "js/bootstrap.min.js",
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        "js/plugins/flot/jquery.flot.js",
        "js/plugins/flot/jquery.flot.tooltip.min.js",
        "js/plugins/flot/jquery.flot.spline.js",
        "js/plugins/flot/jquery.flot.resize.js",
        "js/plugins/flot/jquery.flot.pie.js",
        "js/plugins/flot/jquery.flot.symbol.js",
        "js/plugins/flot/jquery.flot.time.js",
        
        "js/plugins/peity/jquery.peity.min.js",
        
        "js/plugins/morris/raphael-2.1.0.min.js",
        "js/plugins/morris/morris.js",
        
        "js/plugins/chartist/chartist.min.js",
        
        "js/highcharts.js",
        
        
        
         
        // "js/demo/peity-demo.js",
        
        "js/inspinia.js",
         
        "js/plugins/pace/pace.min.js",
        
        "js/plugins/jquery-ui/jquery-ui.min.js",
        
        "js/plugins/gritter/jquery.gritter.min.js",
        
        "js/plugins/sparkline/jquery.sparkline.min.js",
         
        
        // "js/demo/sparkline-demo.js",
        
        "js/plugins/chartJs/Chart.min.js",
         
        "js/plugins/toastr/toastr.min.js",
         
       // "js/plugins/footable/footable.all.min.js",
         
        //   <!-- jqGrid -->
       "js/plugins/jqGrid/i18n/grid.locale-en.js",
       "js/plugins/jqGrid/jquery.jqGrid.min.js",

           
       
       
        //Ajouter full calendar 
        
        //"fullcalendar/gcal.min.js",
        "fullcalendar/locale-all.js",
        "fullcalendar/lib/moment.min.js",
        "fullcalendar/fullcalendar.js",
        "fullcalendar/locale/fr.js",
        "js/plugins/dataTables/datatables.min.js",
        "js/plugins/select2/select2.full.min.js",
        "js/plugins/datapicker/bootstrap-datepicker.js",
        "js/bootstrap-timepicker.min.js",
        
        'js/plugins/steps/jquery.steps.min.js',
        'js/plugins/validate/jquery.validate.min.js',
        
        'js/plugins/typehead/bootstrap3-typeahead.min.js',
        'js/editable/js/bootstrap-editable.min.js',
        /* Espace pour chager l'editeur WYSIWYG */
        'ckeditor5-build-classic/build/ckfinder/ckfinder.js',
        'ckeditor5-build-classic/build/ckeditor.js',
        'ckeditor5-build-classic/build/translations/fr.js',
        
          
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}




   




    