<?php

use yii\helpers\ArrayHelper;

use yii\data\ActiveDataProvider;
use app\modules\fi\models\GeneralConfig;
use app\modules\portal\models\CmsConfig;

use app\modules\rbac\models\User;
use app\modules\rbac\models\Assignment;
use app\modules\fi\models\SrcStudentOtherInfo;
use app\modules\fi\models\SrcStudentHasCourses;
use app\modules\fi\models\SrcPersons;
use app\modules\fi\models\SrcStudentLevel;
use app\modules\fi\models\SrcCourses;
use app\modules\fi\models\SrcModule;
use app\modules\fi\models\YearMigrationCheck;
use app\modules\inscription\models\SrcPostulant;
use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcDevises;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcLoanOfMoney;
use app\modules\billings\models\SrcPayroll;
use app\modules\billings\models\SrcBareme;
use app\modules\billings\models\SrcFeesLabel;
use app\modules\billings\models\SrcFeeServices;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcScholarshipholder;

use app\modules\stockrooms\models\SrcStockroomHasProducts;
use app\modules\stockrooms\models\SrcStockroomInventory;
use app\modules\stockrooms\models\SrcStockroomEquipment;
use app\modules\stockrooms\models\SrcStockroomFurniture;
use app\modules\stockrooms\models\SrcStockroomRawMaterial;

use app\modules\fi\models\SrcMargeEchec;

// auto-loading




	function numberAccountingFormat($number)
	{
		//string number_format ( float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = ",” )
       // english notation with thousands separator
		$format_number = preg_replace( '/(-)([\d\.\,]+)/ui','($2)', number_format($number, 2, '.', ',')  );
		// french notation with thousands separator
		//$format_number = number_format($number, 2, ',', '.');
		return $format_number;
	}
	
	
function pa_daksan()
	{
		 return ['Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' ];

    
    }

function capital_aksan()
	{
		 return ['Š'=>'Š', 'š'=>'Š', 'Ž'=>'Ž', 'ž'=>'Ž', 'À'=>'À', 'Á'=>'Á', 'Â'=>'Â', 'Ã'=>'Ã', 'Ä'=>'Ä', 'Å'=>'Å', 'Æ'=>'Æ', 'Ç'=>'Ç', 'È'=>'È', 'É'=>'É', 'Ê'=>'Ê', 'Ë'=>'Ë', 'Ì'=>'Ì', 'Í'=>'Í', 'Î'=>'Î', 'Ï'=>'Ï', 'Ñ'=>'Ñ', 'Ò'=>'Ò', 'Ó'=>'Ó', 'Ô'=>'Ô', 'Õ'=>'Õ', 'Ö'=>'Ö', 'Ø'=>'Ø', 'Œ'=>'Œ', 'Ù'=>'Ù', 'Ú'=>'Ú', 'Û'=>'Û', 'Ü'=>'Ü', 'Ū'=>'Ū', 'Ý'=>'Ý', 'Ÿ'=>'Ÿ', 'Þ'=>'B', 'à'=>'À', 'á'=>'Á', 'â'=>'Â', 'ã'=>'Ã', 'ä'=>'Ä', 'å'=>'Å', 'æ'=>'Æ', 'ç'=>'Ç', 'è'=>'È', 'é'=>'É', 'ê'=>'Ê', 'ë'=>'Ë', 'ì'=>'Ì', 'í'=>'Í', 'î'=>'Î', 'ï'=>'Ï', 'ñ'=>'Ñ', 'ò'=>'Ò', 'ó'=>'Ó', 'ô'=>'Ô', 'õ'=>'Õ', 'ö'=>'Ö', 'ø'=>'Ø', 'œ'=>'Œ', 'ù'=>'Ù', 'ú'=>'Ú', 'û'=>'Û', 'ü'=>'Ü', 'ū'=>'Ū', 'ý'=>'Ý', 'ÿ'=>'Ÿ' ];

    
    }
 
 
 	function gender_array()
	{
		 return ['0'=>Yii::t('app', 'Male'),'1'=>Yii::t('app', 'Female') ];

    }
    
 function blood_array()
	{
		 return ['O+'=>Yii::t('app', 'O+'),'O-'=>Yii::t('app', 'O-'),'A+'=>Yii::t('app', 'A+'),'A-'=>Yii::t('app', 'A-'),'B+'=>Yii::t('app', 'B+'),'B-'=>Yii::t('app', 'B-'),'AB+'=>Yii::t('app', 'AB+'),'AB-'=>Yii::t('app', 'AB-') ];

    }
    
 function active_array()
	{
		 return ['0'=>Yii::t('app', 'Inactive'), '1'=>Yii::t('app', 'Active'), '2'=>Yii::t('app', 'New') ];

    }
    
 function active2add_array() //for create and update operation
	{
		 return ['1'=>Yii::t('app', 'Active'), '2'=>Yii::t('app', 'New') ];

    }

 function ageCalculator($dob)
 {
	if(!empty($dob)&&($dob!='0000-00-00'))
	 {
		$birthdate = new DateTime($dob);
		$today   = new DateTime('today');
		$age = $birthdate->diff($today)->y;
		return $age;
	  }
	else
	  {
	      return null;
	    }
 
  }
  
function infoGeneralConfig($param_value){
        $item_value=null;
                
       $query = GeneralConfig::find()
                ->where(['item_name'=>$param_value])
                ->all();

         if(isset($query)&&($query!=null))
           { foreach($query as $q)
                $item_value=$q->item_value;
           }
            
            return $item_value;
               
   }
   
function infoCmsConfig($param_value){
        $item_value=null;
                
       $query = CmsConfig::find()
                ->where(['parameter_name'=>$param_value])
                ->all();

         if(isset($query)&&($query!=null))
           { foreach($query as $q)
                $item_value=$q->parameter_value;
           }
            
            return $item_value;
               
   }

 
function headerLogo_freres()
  { 
    $string_add_logo = '<img alt="'. Yii::t("app", "") .'" style="width: 70px;" class="img-circle" src="img/Logo-freres.png" /> ';
     
     return $string_add_logo;
  
   }
   
   
 function headerLogo()
  { 
    $string_add_logo = '<img alt="'. Yii::t("app", "") .'" style="width: 170px;" class="img-circle" src="img/Logo_Cana_Reel_G.gif" /> ';
     
     return $string_add_logo;
  
   }
   
  
function pdfHeading()
  { 
    $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
     
$school_name_school_acronym = $school_name; 

if($school_acronym!='')
   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	


	$hearder = '<table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:22%" ><span style="padding-top:0px; "> '.headerLogo().'
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px;" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span>  
					     
					       <hr style=" color:#000; margin-top:5px; width:85%;" />
					     </td>
					    
					     <td rowspan="2" style="width:22%" ></td>
					 </tr>
				 </table>
		';
			
     return $hearder;
  
   }

 
function pdfHeading_reportcard()
  { 
    $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
     $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
     $school_email_address = infoGeneralConfig('school_email_address');
     
     $school_siteweb = infoGeneralConfig('school_site_web');

     $school_acronym = infoGeneralConfig('school_acronym');
     
     $school_name_school_acronym = $school_name; 

               	          	


	$hearder = '<table  style="width:100%; margin-bottom:-17px; " >
				  <tr>
					     <td rowspan="2" style="width:13%" ><span style="padding-top:0px; "> '.headerLogo_freres().'
                             </span></td>
					     <td colspan="4" style="text-align:center; " > <br/><span style="font-size:18px; font-weight:bold;" >'.$school_name.'</span> <br/><span style="font-size:14px; font-weight:bold;" >Congrégation des Frères du Sacré-Coeur</span> <br/> <span style="font-size:11px;" >'.$school_address.'</span> <br/>  <span style="font-size:10px;" > '.$school_phone_number.' -  '.$school_siteweb.'</span>  
					     
					       
					     </td>
					    
					     <td rowspan="2" style="width:23%" ><span style="padding-top:0px; "> '.headerLogo().'
                             </span></td>
					 </tr>
				 </table>
                       <br/>
		';
			
     return $hearder;
  
   }
   
   
   
   
function headerLogo1()
  { 
    $string_add_logo = '<img style="width: 70px;" class="img-circle" src="img/Logo_Cana_Reel_G.gif" /> ';
     
     return $string_add_logo;
  
   }
   
   
 function pdfHeading1()
  { 
    $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
     
$school_name_school_acronym = $school_name; 

if($school_acronym!='')
   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	


	$hearder = '<div  ><div style="float:left; width:80px; margin-right:200px;"> '.headerLogo1().'
                             </div>	     </div>';
			
     return $hearder;
  
   }
   
function getDateForSpecificDayBetweenDates($startDate, $endDate, $weekdayNumber)
{
    ini_set( 'date.timezone', 'America/Port-au-Prince' ); 
    $startDate = strtotime($startDate);
    $endDate = strtotime($endDate);

    $dateArr = [];

    do
    {
        if(date("w", $startDate) != (int)$weekdayNumber)
        {
            $startDate += 24 * 3600; // add 1 day
        }
    } while(date("w", $startDate) != (int)$weekdayNumber);


    while($startDate <= $endDate)
    {
        $dateArr[] = date('Y-m-d', $startDate);
        $startDate += 7 * 24 * 3600; // add 7 days
    }

    return($dateArr);
}   
   
   
 function getRoleByUserId($user_id)
  {
        $modelAssignment= new Assignment(0,0,[]);
              $user_role = $modelAssignment->getRoleByUserId($user_id); 
              
       return $user_role;  
              
   }
  
  function getPersonFullNameByUserId($user_id)
    {
        $full_name = '';

        //$modelUser = User::findIdentityOutStatus($user_id);  //return NULL if not found
        

        //$modelUser = User::findIdentity($user_id);
        $modelUser = User::findOne($user_id); 

        if($modelUser->person_id==NULL)
            $full_name = $modelUser->person_id;//$modelUser->username;
        else
           { if($modelUser->is_parent==1) //c yon paran
               {  
               	$modelContact= new SrcContactInfo;
                 
                     $person=$modelContact->findOne($modelUser->person_id);
                 	
                 	$full_name = $person->contact_name;  
                 	  
               	  }
              else
                 {  $modelPerson= new SrcPersons;
                 
                     $person=$modelPerson->findOne($modelUser->person_id);
                 	
                 	$full_name = $person->getFullName(); 
                 	
                   }
           
            }
        
        
        return $full_name;
    }    

//return datetime (last_activity)
function isUserConnected($user_id){  
    
    
	$command= Yii::$app->db->createCommand('SELECT id, last_activity FROM session  WHERE user_id='.$user_id ); 
	
	$data_ = $command->queryAll();
    
    foreach($data_ as $d){
    	         return $d['last_activity']; 
                    break;
                      
         }
         
         
}

function currentUser()
  {
  	   if( (Yii::$app->user->identity->username=="_super_")||(Yii::$app->user->identity->username=="logipam") )
                  return "LOGIPAM";
        else
           return Yii::$app->user->identity->username;
  	}

function defaultCurrency()	
 {
 	$data_devises = SrcDevises::find()
 	             ->where(['is_default'=>1])
				 ->orderBy(['id'=>SORT_ASC])
				 ->all();

 	   if($data_devises!=null)
 	     {
 	     	foreach($data_devises as $devises)
 	     	  { return ($devises->devise_name.'/'.$devises->devise_symbol.'/'.$devises->id);
 	     	     break;
 	     	  }
 	     	  
 	      } 
 	    else
 	      return ('?/?');
   }
   
function getDefaultCurrency()	
 {
 	$data_devises = SrcDevises::find()
 	             ->where(['is_default'=>1])
				 ->orderBy(['id'=>SORT_ASC])
				 ->all();

 	   if($data_devises!=null)
 	     {
 	     	foreach($data_devises as $devises)
 	     	  { return $devises;
 	     	     
 	     	  }
 	     	  
 	      } 
 	    else
 	      return null;
   }
   	    
function getDeviseNameSymbol($devise_object)	
 {
 	
 	   if($devise_object!=null)
 	     {
 	     	return ($devise_object->devise_name.'/'.$devise_object->devise_symbol);
 	     	     	     	  
 	      } 
 	    else
 	      return ('?/?');
   }
 
 function getDeviseNameSymbolById($devise)	
 {
 	$data_devises = SrcDevises::findOne($devise);

 	   if($data_devises!=null)
 	     {
 	     	  	return $data_devises;
 	     	
 	      } 
 	    else
 	      return null;
 	      

   }
   
   
function getDevises()	
 {
 	$data_devises = SrcDevises::find()
				 ->orderBy(['id'=>SORT_ASC])
				 ->all();
 
 $code= array();
 
 	   if($data_devises!=null)
 	     {
 	     	foreach($data_devises as $devises)
 	     	  $code[$devises->id]= getDeviseNameSymbol($devises);
 	     	     
 	     	  
 	     	  
 	      } 
 	  
 	  return $code;  
 	    
   }



	//************************  loadFeeServices ******************************/
function loadFeeServices()
	{    
	   	    
	       	$code= array();
	    
           $modelFeeServices = SrcFeeServices::find()
                       ->joinWith(['serviceDescription',])
                       ->where('old_new =1')
                       ->all(); 
		   
		  
		    foreach($modelFeeServices as $fee_services)
		     {
		     	$devise_symbol = getDeviseNameSymbolById($fee_services->devise)->devise_symbol;
		     	
			    $code[$fee_services->id]= $fee_services->serviceDescription->income_description.' ( '.$devise_symbol.' '.$fee_services->price.' )';
		           
		      }
		    
		return $code;
         
	}


    

	//************************  loadFeeName ******************************/
function loadFeeName($status,$program,$level,$acad,$student)
	{    
	   	    $currency_name = Yii::$app->session['currencyName'];
	       $currency_symbol = Yii::$app->session['currencySymbol'];
	       
	       	$code= array();
	    /*   	
	       $previous_year= SrcAcademicperiods::model()->getPreviousAcademicYear($acad);
	   
	   	//gad si elev la gen balans ane pase ki poko peye
	   	$modelPendingBal=SrcPendingBalance::model()->findAll(array('select'=>'id, balance',
												 'condition'=>'student=:stud AND is_paid=0 AND academic_year=:acad',
												 'params'=>array(':stud'=>$student,':acad'=>$previous_year),
										   ));
			//si gen pending, ajoutel nan lis apeye a			
					if(isset($modelPendingBal)){
						  foreach($modelPendingBal as $bal)
						     {  
						     	$criteria1 = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.fee_label LIKE("Pending balance") AND fl.status='.$status.' AND level='.$program.' AND academic_period='.$acad));
						     	
						     	$model_feesPend_program = SrcFees::model()->findAll($criteria1);
						     	
						     	foreach($model_feesPend_program as $model_feesPend_program_)
						     	  $code[$model_feesPend_program_->id]= Yii::t('app',$model_feesPend_program_->fee0->fee_label).' '.$model_feesPend_program_->label.' '.$currency_symbol.' '.numberAccountingFormat($bal->balance);
						     	 						     
						     }
					    } 
		  */
           $modelFeeName = SrcFees::find()
                       ->joinWith(['fee0','academicPeriod','program0'])
                       ->Where(['not like', 'fee_label', 'Pending balance'])
                       ->andWhere(['fees_label.status'=>$status])
                       ->andWhere(['program.id'=>$program])
                       ->andWhere(['level'=>$level])
                       ->andWhere(['academicperiods.id'=>$acad])
                       ->all(); 
		   
		  //$criteria = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.fee_label NOT LIKE("Pending balance") AND fl.status='.$status.' AND level='.$program.' AND academic_period='.$acad));
		  
		 // $modelFeeName=SrcFees::model()->findAll($criteria);
            //$code[null]= Yii::t('app','-- Please select fee name ---');
		    foreach($modelFeeName as $fee_name){
			    $code[$fee_name->id]= $fee_name->getFeeName();
		           
		      }
		    
		return $code;
         
	}



	//************************  loadFeeForAdmis ******************************/
function loadFeeForAdmis()
	{    
	   	    $currency_name = Yii::$app->session['currencyName'];
	       $currency_symbol = Yii::$app->session['currencySymbol'];
	       
	       	$code= array();
	  
           $modelFee = SrcFeesLabel::find()
                       ->Where(['in', 'id', [4,5]])
                       ->all(); 
		   
		  foreach($modelFee as $fee_name){
			    $code[$fee_name->id]= $fee_name->fee_label;
		           
		      }
		    
		return $code;
         
	}
        
        
	//************************  loadFeeNameByProgramLevel ******************************/
function loadFeeNameByProgramLevel($program,$level,$acad)
	{    
	   	 	       	$code= array();
	    
           $modelFeeName = SrcFees::find()
                       ->joinWith(['fee0','academicPeriod','program0'])
                       ->Where(['not like', 'fee_label', 'Pending balance'])
                       ->andWhere(['program.id'=>$program])
                       ->andWhere(['level'=>$level])
                       ->andWhere(['academicperiods.id'=>$acad])
                       ->all(); 
		   
		  
		    foreach($modelFeeName as $fee_name){
			    $code[$fee_name->id]= $fee_name->getFeeName();
		           
		      }
		    
		return $code;
         
	}
 
	//************************  loadFeeNameByProgramLevelForScholarship ******************************/
function loadFeeNameByProgramLevelForScholarship($program,$level,$student,$acad)
	{    
	   	 	       	$code= array();
	    
              //fee ki nan billing kote amout_pay = 0      
               $query_fee_in_billing = SrcBillings::find()->select('fee_period')
	                ->joinWith(['feePeriod', 'feePeriod.program0'])
	                ->where(['program.id'=>$program])
	                ->andWhere(['fees.level'=>$level])
	                ->andWhere(['student'=>$student])
                        ->andWhere(['<>','amount_pay',0])
	                ->andWhere(['academic_year'=>$acad]);
               
               //fee ki nan Scholarshipholder      
               $query_fee_in_scholarshipholder = SrcScholarshipholder::find()->select('fee')
	                ->andWhere(['student'=>$student])
                        ->andWhere(['academic_year'=>$acad]);
	                
           $modelFeeName = SrcFees::find()
                       ->joinWith(['fee0','academicPeriod','program0'])
                       ->Where(['not like', 'fee_label', 'Pending balance'])
                       ->andWhere(['program.id'=>$program])
                       ->andWhere(['level'=>$level])
                       ->andWhere(['academicperiods.id'=>$acad])
                       ->andWhere(['not in','fees.id',$query_fee_in_billing])
                       ->andWhere(['not in','fees.id',$query_fee_in_scholarshipholder])
                       ->all(); 
		   
		  
		    foreach($modelFeeName as $fee_name){
			    $code[$fee_name->id]= $fee_name->getFeeName();
		           
		      }
		    
		return $code;
         
	}


	//************************  loadModule ******************************/
function loadModule()
	{    
	   	 	       	$code= array();
	     
	     //return program_id or null value
		             $program = isProgramsLeaders(Yii::$app->user->identity->id);
				     if($program==null)
				       {
				       	  $modelModuleName = SrcModule::find()
						                       ->joinWith(['subject0','program0'])
						                       ->all(); 
				       }
				     else
				       {
				       	   $modelModuleName = SrcModule::find()
						                       ->joinWith(['subject0','program0'])
						                       ->where('program='.$program)
						                       ->all(); 
				       	}
				       	
      	  
		    foreach($modelModuleName as $module){
			    $code[$module->id]= $module->subject0->subject_name.' ('.$module->program0->short_name.')';
		           
		      }
		    
		return $code;
         
	}
 


//************************  getProgramByStudentId($id) ******************************/
//return the last program id
function getProgramByStudentId($id)
	{
		 
		  
		  
		  $modelStudInfo = SrcStudentOtherInfo::find()
		                  ->select('apply_for_program')
		                  ->orderBy(['id'=>SORT_DESC])
		                  ->where(['student'=>$id])->all();
		
	  if(isset($modelStudInfo)&&($modelStudInfo!=null))	
		{ foreach($modelStudInfo as $studInfo)
		    {  return $studInfo->apply_for_program;
		       break;
		    }
		}
	  else
	     return null;
			
	}


//************************  getLevelByStudentId($id) ******************************/
//return the level
function getLevelByStudentId($id)
	{  
		  $modelStudLevel = SrcStudentLevel::findOne($id);
		                  
		
	  if(isset($modelStudLevel)&&($modelStudLevel!=null))	
		{ 
			return $modelStudLevel->level;
		}    		
	  else
	     return null;
			
	}

	

function getStudentFullName()
 {	
      $code= array();
      $modelStudent = SrcPersons::find()
                      ->joinWith(['studentOtherInfo0','studentOtherInfo0.applyForProgram'])
                      ->select(['program.label as program_label','program.short_name as program_short_name','persons.id','first_name','last_name'])
                      ->orderBy(['last_name'=>SORT_ASC])
                      ->where(['is_student'=>1])
                      ->andWhere(['in','active',[1,2] ])
                      ->all();
            
                     
      foreach($modelStudent as $students)
         { 
			    $code[$students->id]= $students->getFullName().' ('.$students->program_short_name.')';
			    
		  }
		    
		return $code;
      
  }


function getStudentFullNameForServices()
 {	
      $code= array();
      $modelStudent = SrcPersons::find()
                      ->joinWith(['studentOtherInfo0','studentOtherInfo0.applyForProgram'])
                      ->select(['program.label as program_label','program.short_name as program_short_name','persons.id','first_name','last_name'])
                      ->orderBy(['last_name'=>SORT_ASC])
                      ->where(['is_student'=>1])
                      ->all();
            
                     
      foreach($modelStudent as $students)
         { 
			    $code[$students->id]= $students->getFullName().' ('.$students->program_short_name.')';
			    
		  }
		    
		return $code;
      
  }



function getStudentToExemptFee()
 {	
    $acad=Yii::$app->session['currentId_academic_year'];
    
      $code= array();
      
      $modelStudent = SrcPersons::find()
                      ->joinWith(['studentOtherInfo0','studentOtherInfo0.applyForProgram'])
                      ->select(['program.label as program_label','persons.id','first_name','last_name','id_number'])
                      ->orderBy(['last_name'=>SORT_ASC])
                      ->where('persons.active in(1,2) AND ( (persons.id in (select student from balance b  where balance >0) ) OR  (persons.id in (select student from student_has_courses shc inner join courses c on(c.id=shc.course) inner join module m on(m.id=c.module) inner join fees f on(f.program=m.program) where (checked=0 AND (f.fee not in(select fee from scholarship_holder sh where fee IS NOT NULL AND partner IS NULL AND academic_year='.$acad.' AND percentage_pay=100 )  )  ) and academic_year='.$acad.') )  )')
                      ->all();
            
                     
      foreach($modelStudent as $students)
         { 
			    $code[$students->id]= $students->getFullName().' ('.$students->id_number.') ['.$students->program_label.']';
			    
		  }
		    
		return $code;
      
  }


 
 //************************  loadStockrooms ******************************/
function loadStockrooms($user_id)
	{     $code= array();
	//$code[0]= Yii::t('app','N/A');
	     $stockrooms_array = array();
	$stockrooms_query = '';
		 //select tout stockroom li gen dwa sou yo  
		if( (Yii::$app->user->can("superadmin"))||(Yii::$app->user->can("direction"))||(Yii::$app->user->can("Magasinier manager"))   ) 
		     $stockrooms_query = Yii::$app->db->createCommand('SELECT id, stockroom_code, stockroom_name FROM stockroom')->queryAll();
		else
		  { $stockrooms_query_auth = Yii::$app->db->createCommand('SELECT item_name FROM auth_assignment where user_id='.$user_id.' and item_name like("stockrooms-stockroom%")')->queryAll();
		      
		      if($stockrooms_query_auth!=null)
		       {
		       	   foreach($stockrooms_query_auth as $stockrooms_q)
		       	     {
		       	     	$stockroomStr = substr($stockrooms_q['item_name'],20);
		       	     	
		       	     	$explode_name=explode("-",substr($stockroomStr,0));
		       	     	
		       	     	if(isset($explode_name[0])&&($explode_name[0]!=''))
		       	     	  {
		       	     	  	  
		       	     	  	  if (preg_match('/^[0-9]*$/', $explode_name[0])) 
		       	     	  	       $stockroomID = $explode_name[0];
		       	     	  
		       	     	  }
		       	     	
		       	      if(! in_array($stockroomID, $stockrooms_array) )
		       	     	$stockrooms_array[]= $stockroomID;
		       	      }
		       	     
		       	     
		       	}
		      
		      if($stockrooms_array!=null)
		        {
		        	$condition='';
		        	$pase=0;
		        	foreach($stockrooms_array as $id)
		                {  $condition= $condition.$id.',';
		                   if($pase==0)
		                      $condition= $condition.$id;
		                   else
		                      $condition= $condition.$id.',';
		                      
		                   $pase=1;
		                   
		                }
		                  
		           $stockrooms_query = Yii::$app->db->createCommand('SELECT id, stockroom_code, stockroom_name FROM stockroom where id in('.$condition.')')->queryAll();
		         }
		         
		  }
		  
     	  
		  
		  if( ($stockrooms_query!='')&&($stockrooms_query!=null)  )
		    { 
		    	foreach($stockrooms_query as $stockrooms)
		    	  {
		    	  	$code[$stockrooms['id']]= $stockrooms['stockroom_code'].' ('.$stockrooms['stockroom_name'].')';
		    	  	
		    	  	}
		     }
		return $code;
         
	}
 

	
//************************  loadRecettesItems ******************************/
function loadRecettesItems()
	{     $code= array();
		   
		  // $code[0]= Yii::t('app','Tuition fees');
		  //$code[1]= Yii::t('app','Other fees');
		   $code[5]= Yii::t('app','Services'); 
		    $code[2]= Yii::t('app','Manage other income');
		   /* $code[3]= Yii::t('app','Enrollment fee');  
		   $code[4]= Yii::t('app','Reservation');        
		  */  		   
		  
		  
		return $code;
         
	}
 

//************************  loadRecettesItemsSummary ******************************/
function loadRecettesItemsSummary()
	{     $code= array();
		   
		  // $code[0]= Yii::t('app','Tuition fees');
		   //$code[1]= Yii::t('app','Other fees');
		    $code[5]= Yii::t('app','Services'); 
		   $code[2]= Yii::t('app','Manage other income');
		 // $code[3]= Yii::t('app','Enrollment fee');
		  // $code[4]= Yii::t('app','Reservation'); 
		           
		    		   
		return $code;
         
	}


//************************  loadDepensesItems ******************************/
function loadDepensesItems()
	{     $code= array();
	
	
		   if( Yii::$app->user->can('Administrateur systeme') )
		    {
		    	$code[2]= Yii::t('app','Charge');
		    }
          else
           { 
           	 $code[1]= Yii::t('app','Payroll');
		     $code[2]= Yii::t('app','Charge');
           }

		           
		    		   
		return $code;
         
	}

	

function lastBillingTransactionID($tud, $acad)
  { 
  	$fee_status = ['in','fees_label.status', [0,1] ];  
  	 $mod_bil = new SrcBillings();
     $last_bill_id =0;
          $result = $mod_bil->getLastTransactionID($tud,$fee_status, $acad);
          if($result!=null)
            $last_bill_id = $result;
        
        return $last_bill_id;
  
   }

   
   
   function lastBillingTransactionIDWithoutDevise($tud, $acad)
  { 
  	$fee_status = ['in','fees_label.status', [0,1] ];  
  	 $mod_bil = new SrcBillings();
     $last_bill_id =0;
          $result = $mod_bil->getLastTransactionIDWithoutDevise($tud,$fee_status, $acad);
          if($result!=null)
            $last_bill_id = $result;
        
        return $last_bill_id;
  
   }

	//************************  loadLevels ******************************/
function loadLevels()
	{    
	   	 $max_level_cycle = infoGeneralConfig('max_level_cycle');
	   	 	       
	   	 	       	$code= array();
	    
         		  
		    for($i=1; $i<=$max_level_cycle; $i++){
			    $code[$i]= Yii::t('app','Level')." ".$i;
		           
		      }
		    
		return $code;
         
	}	
	

        
//************************  loadTranscriptItems ******************************/
function loadTranscriptItems()
	{     $code= array();
		   
		   $code[0]= Yii::t('app','Transcript of notes');
		   //$code[1]= Yii::t('app','Certificate');
                
                   if((Yii::$app->user->can("fi-grades-update_validatedG")) )
                    { $code[2]= Yii::t('app','Delayed grades');
                        $code[3]= Yii::t('app','Reportcard');
                     }
		    		   
		return $code;
         
	}	

//---


//************************  isPayrollDoneForOne($month,$pers,$acad)  ******************************/	
function isPayrollDoneForOne($month,$pers,$acad)
	{    
             $modelPayroll= new SrcPayroll();
                $bool=false;
               
		 if($month!='')
		   {
			  $payroll= $modelPayroll->isDoneForOne($month,$pers,$acad);
				
				 if(isset($payroll)&&($payroll!=null))
				  {        
				      $payroll = $payroll->getModels();      
				      
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						       $bool=true;
						}
						  
				   }
				   
				//check if there is any after
		/*		if(!$bool)
				  {
				  	 $any_after = Payroll::model()->anyAfterDoneForOnes($month,$pers,$acad);
  
				  	 if(isset($any_after)&&($any_after!=null))
						  {  //$any_after_ = $any_after->getData();//return a list of  objects
						           
						      foreach($any_after as $p)
						       {			   
								  
								  if($p['id']!=null)
								     {
						     	
								     	  $bool=true;
								     }
								}  
						   }
				  	
				  	}
			*/				 
		     }
		     
		     
		return $bool;
         
	}


//************************  isPayrollDone($month)  ******************************/	
function isPayrollDone($month)
	{    
                $modelPayroll= new SrcPayroll();
                
                 $bool=false;
                 
		 if($month!='')
		   {
			  $payroll=$modelPayroll->isDone($month);
				
				 if(isset($payroll)&&($payroll!=null))
				  { 
				  	$payroll = $payroll->getModels();      
				      
				      foreach($payroll as $p)
				       {			   
						  if($p->id!=null)
						       $bool=true;
						}  
				   }	 					 
		     }
		     
		     
		return $bool;
         
	}

//return program_id or null value
function isProgramsLeaders($user_id){  
    
    $program = null;
    
    $userModel = User::findOne($user_id);
    
    if($userModel->person_id!='')
     {
    
			$command= Yii::$app->db->createCommand('SELECT id, program_id FROM programs_leaders WHERE person_id='.$userModel->person_id.' and old_new=1' ); 
			
			$data_ = $command->queryAll();
		    
		   if($data_!=null)
		     { foreach($data_ as $d)
		        {
		    	         $program = $d['program_id']; 
		                    break;
		                      
		         }
		         
		     }
      }
    return $program;
         
         
}



//return an objects of type Payroll
//************************  infoLastPayrollDone()  ******************************/	
function infoLastPayrollDone()
	{    
          $modelPayroll= new SrcPayroll(); 
                
		  $payroll= $modelPayroll->getInfoLastPayroll();
		  
		  	 if(isset($payroll)&&($payroll!=null))
			  {   
			  	$payroll = $payroll->getModels();
			  	     
			      foreach($payroll as $p)
			       {			   
					  if($p->id!=null)
					     {  
					     	return $p;
					     	 break;
					     	 
					     }
					     
					}  
			   }	 					 
			
		return null;
         
	}


//return an objects of type Payroll
//************************  infoLastPayrollDoneForOne($pers)  ******************************/	
function infoLastPayrollDoneForOne($pers)
	 {    
           $modelPayroll= new SrcPayroll();
                
		  $payroll=$modelPayroll->getInfoLastPayrollForOne($pers);
		  
			 if(isset($payroll)&&($payroll!=null))
			  {   
			  	$payroll = $payroll->getModels();     
			      foreach($payroll as $p)
			       {			   
					  if($p->id!=null)
					     {  
					     	return $p;
					     	 break;
					     	 
					     }
					     
					}  
			   }	 					 
			
		return null;
         
	}
//return an array of month
//************************  infoAllPayrollDoneForOne($pers,$acad)  ******************************/	
function infoAllPayrollDoneForOne($pers,$acad)
	 {    
           $modelPayroll= new SrcPayroll();
           
           $month_array = array();
                
		  $payroll=$modelPayroll->getInfoAllPayrollForOne($pers,$acad);
			  
			 if(isset($payroll)&&($payroll!=null))
			  {   
			  	$payroll = $payroll->getModels();     
			      foreach($payroll as $p)
			       {			   
					  if($p->id!=null)
					     {  
					     	$month_array[] =$p->payroll_month;
				     	 
					     }
					     
					}  
			   }	 					 
			
		return $month_array;
         
	}


//************************  getSelectedLongMonthValueForOne($pers)  ******************************/	
/*  //old
function getSelectedLongMonthValueForOne($pers)
	{    
          $modelLoan = new SrcLoanOfMoney();
          
          $code = array(); 
          
          $nbre_payroll = infoGeneralConfig('total_payroll');  
               
		  $last_payroll_info= infoLastPayrollDoneForOne($pers);
			
			if(isset($last_payroll_info)&&($last_payroll_info!=null))
			 {
				  if($last_payroll_info->payroll_month == $nbre_payroll) //denye payroll fet deja
				    {
				    	for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name= $modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
				     }    
				  else
					{  for($i=($last_payroll_info->payroll_month + 1); $i<=$nbre_payroll; $i++)
					   {			   
						   $month_name= $modelLoan->getSelectedLongMonth($i);
						   
						   $code[$i]= $month_name; 	  
							     
					     }
					}  
			     
			  }
			else
			  {   //poko gen payroll ki fet
			  	 
			  	 
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name= $modelLoan->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  	}
			  	 	
		return $code;
         
	}
*/
function getSelectedLongMonthValueForOne($pers)
	{    
          $acad=Yii::$app->session['currentId_academic_year'];
          
          $modelLoan = new SrcLoanOfMoney();
          
          $code = array(); 
          
          $nbre_payroll = infoGeneralConfig('total_payroll');  
               
		  $last_payroll_info= infoAllPayrollDoneForOne($pers,$acad);  
			
			if(isset($last_payroll_info)&&($last_payroll_info!=null))
			 {
				    	for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   //depi li pa nan rezilta denye payroll yo afiche l
							   if(! in_array($i,$last_payroll_info) )
							    {
									   $month_name= $modelLoan->getSelectedLongMonth($i);
									   
									   $code[$i]= $month_name; 
							    }	  
								     
						     }  
				     
			  }
			else  
			   {   // poko gen payroll ditou
			   	    for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   
							   $month_name= $modelLoan->getSelectedLongMonth($i);
									   
									   $code[$i]= $month_name; 
							     
								     
						     } 
			   	}
			
			  	 	
		return $code;
         
	}

//************************  getSelectedLongMonthValue()  ******************************/	
function getSelectedLongMonthValue()
	{    
          $modelLoan = new SrcLoanOfMoney();
          
          $code = array();       
		  
		  $nbre_payroll = infoGeneralConfig('total_payroll');  
               
		  $last_payroll_info= infoLastPayrollDone();
			
			if(isset($last_payroll_info)&&($last_payroll_info!=null))
			 {
				  if($last_payroll_info->payroll_month == $nbre_payroll)
				    {
				    	for($i=1; $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name= $modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
				     }
				  else
					{  for($i=($last_payroll_info->payroll_month + 1); $i<=$nbre_payroll; $i++)
						   {			   
							   $month_name= $modelLoan->getSelectedLongMonth($i);
							   
							   $code[$i]= $month_name; 	  
								     
						     }  
					}
			     
			  }
			else
			  {
			  	 for($i=1; $i<=$nbre_payroll; $i++)
				   {			   
					   $month_name= $modelLoan->getSelectedLongMonth($i);
					   
					   $code[$i]= $month_name; 	  
						     
				     }  
			  	}
			  	 	
		return $code;
         
	}


			   
//************************  allLoanPaid($person)  ******************************/	
function allLoanPaid($person)
	{    
               $modelLoan = new SrcLoanOfMoney();
                $bool=true;
		  $loan=$modelLoan->isPaid($person);
			
			 if(isset($loan)&&($loan!=null))
			  {        
			      foreach($loan_paid as $l)
			       {			   
					  if($l->paid == 0)
					       $bool=false;
					}  
			   }	 					 
			
		return $bool;
         
	}

  	
  	

//************************  personHasLoan($person,$month)  ******************************/	
//return the total amount for this month
function personHasLoan($person,$month)
	{    
                $modelLoan = new SrcLoanOfMoney();
                $sum=0;
		  $loan=$modelLoan->hasLoan($person,$month);
			
			 if(isset($loan)&&($loan!=null))
			  {        
			      foreach($loan as $l)
			       {			   
					   $sum= $sum + $l->amount;
					}  
			   }	 					 
			
		return $sum;
         
	}
	  


/**  from base de donnees **/   
//return 0:false, 1:true
function isDateInAcademicRange($dat,$acad){  
    
    $result = 0;
    
	$command= Yii::$app->db->createCommand('SELECT id FROM academicperiods a  WHERE id ='.$acad.' AND (date_start <\''.$dat.'\' AND date_end >\''.$dat.'\')'); 
	
	$data_ = $command->queryAll();

  
    if($data_ !=null)
      { foreach($data_ as $d){
    	        $result = 1;
 
                  break;             
         }
         
       }
         
    return $result;     
} 
	


//************************  loadEmployerEmployee() ******************************/
function loadEmployerEmployee()
	{
		  $code= array();
		   
		  $code[null]= Yii::t('app','-- Select --');
		  $code[0]= Yii::t('app','Employee');
          $code[1]= Yii::t('app','Employer');
		           
		       
		   
		return $code;
			
	}



function loadRoomForTeacher($person_id)
 {     $code= array();
	$acad=Yii::$app->session['currentId_academic_year'];
	
		  $moduleCourse = SrcCourses::find()->select(['id'])
                         ->where(['teacher'=>$person_id,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct(true);

		  $moduleStudent = SrcStudentHasCourses::find()
		                 ->distinct(true)
                         ->select(['student'])
                         ->where(['in','course',$moduleCourse]);
     	 
          
                 
     	  $moduleRoom = SrcStudentlevel::find()->distinct(true)
                         ->distinct(true)
                         ->select(['room'])
                         ->joinWith(['room0'])
                         ->where(['in','student_id',$moduleStudent])
                         ->all();
		  
		  if( ($moduleRoom!='')&&($moduleRoom!=null)  )
		    { 
		    	foreach($moduleRoom as $room)
		    	  {
		    	  	  if($room->room!=0)
		    	  	     $code[$room->room]= $room->room0->room_name;
		    	  	
		    	  	}
		     }

		return $code;
		
 }


function loadCoursesForTeacher($person_id,$acad)
 {     $code= array();
	
	
		  $moduleCourse = SrcCourses::find()
                         ->joinWith(['module0'])
                         ->where(['teacher'=>$person_id,'academic_year'=>$acad,'old_new'=>1])
                         ->distinct()
                         ->all();

		  
     	  
		  
		  if( ($moduleCourse!='')&&($moduleCourse!=null)  )
		    { 
		    	foreach($moduleCourse as $course)
		    	  {
		    	  	$code[$course->id]= $course->module0->subject0->subject_name." (".$course->module0->code.")"." [".$course->shift0->shift_name."]";
		    	  	
		    	  	}
		     }
		return $code;
		
 }


	
//************************  loadCategoryExpenses ******************************/
function loadCategoryExpenses()
	{    
	    
		$acad=Yii::$app->session['currentId_academic_year']; 
		 
           $code= array();
		   
		    $sql='select id, category from label_category_for_billing where income_expense=\'di\'';
	
         $result= Yii::$app->db->createCommand($sql)->queryAll();

    
		           
		 $code[null]= Yii::t('app','-- Select --');
		    foreach($result as $r){
			    $code[$r['id']]= Yii::t('app',$r['category']);
		           
		      }
		   
		return $code;
         
	}

	//************************  loadCategoryIncome ******************************/
function loadCategoryIncome()
	{    
	    
		$acad=Yii::$app->session['currentId_academic_year']; 
		 
           $code= array();
		   
		    $sql='select id, category from label_category_for_billing where income_expense=\'ri\'';
	
         $result= Yii::$app->db->createCommand($sql)->queryAll();

    
		           
		 $code[null]= Yii::t('app','-- Select --');
		    foreach($result as $r){
			    $code[$r['id']]= Yii::t('app',$r['category']);
		           
		      }
		   
		return $code;
         
	}

function isBillingStudent_Ajou($last_bil_id,$student)
{
    $pass=true;
 
 if( ($last_bil_id!=0) )
 {
    $modelLastBill = SrcBillings::findOne($last_bil_id);
    $modelFee_ = SrcFees::findOne($modelLastBill->fee_period);
    if(date('Y-m-d') >= $modelFee_->date_limit_payment)//gad si gen balans
      {
         if($modelLastBill->balance >0 )
             $pass=false;
         else 
           {
             $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$student.' AND balance >0')->all();
              if( $modelBalance_stud!=null)
                $pass=false;
             }
       }
       else
         {
           if($modelLastBill->balance >0 )
               $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$student.' AND balance >0 AND balance <>'.$modelLastBill->balance )->all();
           else 
             $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$student.' AND balance >0')->all();
           
           if( $modelBalance_stud!=null)
              $pass=false;
           
       }
 }
 else 
  {
      $modelBalance_stud = SrcBalance::find()->select(['balance','id'])->where('student='.$student.' AND balance >0')->all();
      if( $modelBalance_stud!=null)
          $pass=false;
   }
   
   return $pass;
}
        
 function loadAllStudentsJson()
 {
      $allStudents = SrcPersons::findBySql("SELECT CONCAT(first_name,' ',last_name,' ','(',id_number,')') AS 'name', p.id from persons p inner join student_other_info soi on(soi.student=p.id) where is_student=1 and active in(1,2) order by last_name ASC, first_name ASC")->asArray()->all();
 
        return $allStudents;
 } 
 
 
 function loadAllPostulantsJson($date_start)
 {
      //  $allPostulants = SrcPostulant::findBySql("SELECT CONCAT(first_name,' ',last_name,' ','(',birthday,')[ ',short_name,' ]') AS 'name', p.id from postulant p inner join program pr on(pr.id=p.apply_for_program) where p.date_created >= '".$date_start."' AND p.id NOT IN(SELECT DISTINCT postulant FROM postulant_income where fee IS NOT NULL) order by last_name ASC, first_name ASC")->asArray()->all();
     $allPostulants = SrcPostulant::findBySql("SELECT CONCAT(first_name,' ',last_name,' ','(',birthday,')[ ',short_name,' ]') AS 'name', p.id from postulant p inner join program pr on(pr.id=p.apply_for_program) where p.date_created >= '".$date_start."' order by last_name ASC, first_name ASC")->asArray()->all();
 
        return $allPostulants;
 } 
        
        
 function loadAllStudentsBillingJson()
 {
      $allStudents = SrcPersons::findBySql("SELECT CONCAT(first_name,' ',last_name,' ','(',id_number,')') AS 'name', p.id from persons p inner join billings b on(b.student=p.id) where is_student=1 and active in(1,2) order by last_name ASC, first_name ASC")->asArray()->all();
 
        return $allStudents;
 }        
        

 
  function decisionSelectedOption($select,$nbr_echec,$program,$poudesizyon)
  {
     $option = '';
     $borne_inf=null;
     $borne_sup=null;
     $acad=Yii::$app->session['currentId_academic_year'];
     
     $modelMargeEchec = SrcMargeEchec::find()->where('program='.$program.' AND academic_year='.$acad)->all();
     
     if($modelMargeEchec!=NULL)
     {
         foreach ($modelMargeEchec as $margeEchec)
         {
             $borne_inf= $margeEchec->quantite_module;
                     $borne_sup= $margeEchec->borne_sup;
         }
     
     $rencontredirection = infoGeneralConfig('rencontredirection_dateheure');
     
     if($poudesizyon==NULL)
      {
            if($select==true)
            {
              if($nbr_echec!=null)
              {
                if($nbr_echec==0)
                 {
                    $option ='<option selected="selected" value="1">'.Yii::t('app','Licensed to higher level').'</option>'
                                    . '<option  value="2">'.Yii::t('app','Licensed to higher level conditionally').'</option>'
                                    . '<option  value="3">'.Yii::t('app','Maintained at the current level for all modules').'</option>'
                                    . '<option  value="4">'.Yii::t('app','Maintained at the current level for some modules').'</option>';
                  }
               elseif(($nbr_echec >=1)&&($nbr_echec <= $borne_inf))
                     { 
                        $option ='<option  value="1">'.Yii::t('app','Licensed to higher level').'</option>'
                                        . '<option selected="selected" value="2">'.Yii::t('app','Licensed to higher level conditionally').'</option>'
                                        . '<option  value="3">'.Yii::t('app','Maintained at the current level for all modules').'</option>'
                                        . '<option  value="4">'.Yii::t('app','Maintained at the current level for some modules').'</option>';
                       }
                    elseif(($nbr_echec > $borne_inf)&&($nbr_echec <=$borne_sup))
                             {
                                $option ='<option  value="1">'.Yii::t('app','Licensed to higher level').'</option>'
                                                . '<option  value="2">'.Yii::t('app','Licensed to higher level conditionally').'</option>'
                                                . '<option  value="3">'.Yii::t('app','Maintained at the current level for all modules').'</option>'
                                                . '<option selected="selected" value="4">'.Yii::t('app','Maintained at the current level for some modules').'</option>';
                               }
                            elseif($nbr_echec>$borne_sup)
                                    {
                                       $option ='<option  value="1">'.Yii::t('app','Licensed to higher level').'</option>'
                                                       . '<option  value="2">'.Yii::t('app','Licensed to higher level conditionally').'</option>'
                                                       . '<option selected="selected" value="3">'.Yii::t('app','Maintained at the current level for all modules').'</option>'
                                                       . '<option  value="4">'.Yii::t('app','Maintained at the current level for some modules').'</option>';
                                      }
                                      
                 }
               else
               {
                   $option ='<option ></option>';
               }
            }
           elseif($select==false)  //pou bulten yo
           {
             
               if(($nbr_echec >=0)&&($nbr_echec <=$borne_inf))
                     { 
                        $option ='EST AUTORISÉ(E) À POURSUIVRE AVEC LE PROGRAMME';
                       }
                    elseif($nbr_echec >$borne_inf)
                             {
                                $option ='DIRECTION À RENCONTRER LE '.$rencontredirection;
                               }
              

           }
           
        }
      elseif($poudesizyon!=NULL) //  nan bulten an pou voye done nan student_level_history
      {
         
          if(($nbr_echec >=0)&&($nbr_echec <=$borne_inf))
                     { 
                        $option =1;
                       }
                    elseif($nbr_echec >$borne_inf)
                             {
                                $option =0;
                               }
      }
      
     }
     
   
     return $option;
     
     
  }
 

function isCourseEvaluated($course,$level)
	{    
       	  
                $bool=false;
		
                 $command= Yii::$app->db->createCommand('SELECT grade_value FROM grades g INNER JOIN student_level sl ON(g.student=sl.student_id) WHERE course='.$course.' and level='.$level ); 
                 $data1 = $command->queryAll();
                
                 if($data1!=NULL)
                 {
                    foreach($data1 as $d)
                     {
                         $bool=true;
                     }  
                 }
                	 					 
		      
			
		return $bool;
         
	} 
 
/*    
 function acad_sess()
  {
		$acad_sess = 0;
			
	$siges_structure = infoGeneralConfig('siges_structure_session');
	     
	   if($siges_structure==1)
	    {
	         $sess=Yii::app()->session['currentId_academic_session'];  
             $sess_name=Yii::app()->session['currentName_academic_session'];	
	      }

$acad=Yii::app()->session['currentId_academic_year']; 
		
 if($siges_structure==1)
 {  if( $sess=='')
        $acad_sess = 0;
    else 
		$acad_sess = $sess;
 }
 elseif($siges_structure==0)
   $acad_sess = $acad;


		
		 return $acad_sess;
 
    }
  

  
  


function sumPeriodWeight($pastp,$current_p)
  {
  	  if($pastp!=null)
  	    {
  	    	 $sum_ = 0;
  	    	 $weight_null=false;
  	    	 $weight_100=true;
  	    	 
  	    	 foreach($pastp as $pId)
  	    	   {
  	    	   	 //jwenn peryod eval sa ye
  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($pId);
  	    	   	  
  	    	   	   foreach($p_acad as $p_weight)
  	    	   	     {  
  	    	   	     	if($p_weight['weight']!=null)
  	    	   	          { $sum_ =  $sum_ + $p_weight['weight'];
  	    	   	             
  	    	   	             if($p_weight['weight']!=100)
  	    	   	               $weight_100=false;
  	    	   	                
  	    	   	          }
  	    	   	        else
  	    	   	           $weight_null=true;
  	    	   	          
  	    	   	     }
  	    	   	  
  	    	   	 
  	    	   	}
  	    	   
  	    	if($current_p!='')
  	    	  {
  	    	  	   //jwenn peryod eval sa ye
  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($current_p);
  	    	   	  
  	    	   	   foreach($p_acad as $p_weight)
  	    	   	     {  
  	    	   	     	if($p_weight['weight']!=null)
  	    	   	          { $sum_ =  $sum_ + $p_weight['weight'];
  	    	   	             
  	    	   	             if($p_weight['weight']!=100)
  	    	   	               $weight_100=false;
  	    	   	                
  	    	   	          }
  	    	   	        else
  	    	   	           $weight_null=true;
  	    	   	          
  	    	   	     }
  	    	  	}
  	    	   	
  	    	   	if( ($sum_==0) && ($weight_null==true) ) //tout peryod yo gen menm koyefisyan
  	    	   	  return -1;   
  	    	   	elseif( ($sum_!=0) && ($weight_null==true) )
  	    	   	       return $sum_;
  	    	   	    elseif( ($sum_!=0) && ($weight_null==false) )
  	    	   	        {
  	    	   	        	if( ($weight_100==true) )//tout peryod yo gen menm koyefisyan
  	    	   	        	  return -1;
  	    	   	        	else
  	    	   	        	   return $sum_;
  	    	   	          }
  	    	
  	      }
  	  else
  	     return 0;
  	
  	}
  	
 	
	

 
 */
        
        
         
//-1: migration not yet done; 1: migration is not completed 2: migration done; 0: no migration to do  
function getYearMigrationCheck($acad)
{
    $result=2;
    $YearMigrationCheck = new YearMigrationCheck;
    $data = $YearMigrationCheck->getValueYearMigrationCheck($acad);
    
         if($data!=null)
           {  foreach($data as $d){
    	         if( ($d['marge_echec']==0)&&($d['payroll_setting']==0)&&($d['postulant']==0)&&($d['course']==0)&&($d['fees']==0)&&($d['taxes']==0)&&($d['pending_balance']==0) )
    	           {
    	           	  $result=-1; 
    	           	  break;
    	           }
    	         else
    	           {
		    	         if( ($d['marge_echec']==0)||($d['payroll_setting']==0)||($d['postulant']==0)||($d['course']==0)||($d['fees']==0)||($d['taxes']==0)||($d['pending_balance']==0) )
		    	         $result=1; 
		                    break;
    	           }
                      
              }
           }
         else
            $result = 0;
          
           
	return $result;
       
  }
  
  
         // Get month from a date 
         
      function getMonth($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                         $month=date("n",$time);
                         
            return $month;
              }
           else
              return null;
        }
       
       
       
        //  Get day from a date 
         
      function getDay($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                         $day=date("j",$time);
                         
            return $day;
              }
           else
              return null;
        }
        
     
        
        //  Get year form a date 
         
      function getYear($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                        $year=date("Y",$time);
                         
                return $year;
              }
           else
              return null;
        }
        
      function getYear2($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                        $year=date("y",$time);
                         
                return $year;
              }
           else
              return null;
        }
        
      
      function ChangeDateFormat($d){
            if(($d!='')&&($d!='0000-00-00'))
              { $time = strtotime($d);
                         $month=date("m",$time);
                         $year=date("Y",$time);
                         $day=date("j",$time);
                         
               return $day.'/'.$month.'/'.$year; 
               }
             else
                return ''; 
        }

  
        
        // Return the name of a month in long format 
  function getLongMonth($mois){
    
    if($mois!='')
    {
     switch ($mois){
        case 0:
            return Yii::t('app','BONIS');
            break;
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
        case 13:
            return Yii::t('app','13e');
            break;
        case 14:
            return Yii::t('app','14e');
            break;
       }
       
      }
      
      
   }
    
    
  function getShortMonth($mois){
    
    if($mois!='')
    {
      switch ($mois){
        case 0:
            return Yii::t('app','BON');
            break;
        case 1:
            return Yii::t('app','Jan');
            break;
        case 2:
            return Yii::t('app','Feb');
            break;
        case 3:
            return Yii::t('app','Mar');
            break;
        case 4:
            return Yii::t('app','Apr');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','Jun');
            break;
        case 7:
            return Yii::t('app','Jul');
            break;
        case 8:
            return Yii::t('app','Aug');
            break;
        case 9:
            return Yii::t('app','Sep');
            break;
        case 10:
            return Yii::t('app','Oct');
            break;
        case 11:
            return Yii::t('app','Nov');
            break;
        case 12:
            return Yii::t('app','Dec');
            break;
        case 13:
            return Yii::t('app','13e');
            break;
        case 14:
            return Yii::t('app','14e');
            break;
       }
       
     }
    
    
   }

	
  		
	function getLongDay($day){
		switch($day){
				case 1:
					return Yii::t('app','Monday');
				     break;
				case 2:
					return Yii::t('app','Tuesday');
					break;
				case 3:
					return Yii::t('app','Wednesday');	
					break;
				case 4:
					return Yii::t('app','Thursday');
					break;
				case 5:
					return Yii::t('app','Friday');
					break;
				case 6:
					return Yii::t('app','Saturday');
					break;	
				case 7:
					return Yii::t('app','Sunday');
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}

	
    function getShortDay($day){
		switch($day){
				case 1:
					return Yii::t('app','Mon');
				     break;
				case 2:
					return Yii::t('app','Tue');
					break;
				case 3:
					return Yii::t('app','Wed');	
					break;
				case 4:
					return Yii::t('app','Thu');
					break;
				case 5:
					return Yii::t('app','Fri');
					break;
				case 6:
					return Yii::t('app','Sat');
					break;	
				case 7:
					return Yii::t('app','Sun');
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}

	
    function getDayNumberByShortDay($day){
		switch($day){
				case Yii::t('app','Mon'):
					return 1;
				     break;
				case Yii::t('app','Tue'):
					return 2;
					break;
				case Yii::t('app','Wed'):
					return 3;	
					break;
				case Yii::t('app','Thu'):
					return 4;
					break;
				case Yii::t('app','Fri'):
					return 5;
					break;
				case Yii::t('app','Sat'):
					return 6;
					break;	
				case Yii::t('app','Sun'):
					return 7;
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}



//return iri deduction over total_gross_salary
function getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary)
  {
     $deduct_iri=false;
     $deduction=0;
    
  if($total_gross_salary!=0)
     { 
     if($id_payroll_set2==null)
       {     $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set;
        
        }
     elseif($id_payroll_set2!=null)
	     {  $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set IN('.$id_payroll_set.','.$id_payroll_set2.')';
	     
	       }
			           	      
		$command__ = Yii::$app->db->createCommand($sql__);
		$result__ = $command__->queryAll(); 
			
																					       	   
		  if($result__!=null) 
			{ foreach($result__ as $r)
				{ 
				    $tx_des='';
					$sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];
																				
					$command_tx_des = Yii::$app->db->createCommand($sql_tx_des);
					$result_tx_des = $command_tx_des->queryAll(); 
																								       	   
					foreach($result_tx_des as $tx_desc)
					 {   $tx_des= $tx_desc['taxe_description'];
						 
					  }
											     	 
					if( ($tx_des=='IRI') ) //c iri,
						$deduct_iri=true; 					     	      			                        
		 
				}
				
			}
			            	 
		
		if($deduct_iri)
		  {
	          $bareme = array();
	          
	          $modelBareme = new SrcBareme;
	          $modelPayroll = new SrcPayroll;
	        
	        //pran vale barem lan nan baz la
	        //return an array  
	         $potential_bareme = $modelBareme->getBaremeInUse();
	           if($potential_bareme!=null)
	             { $i =0;
	                
	                foreach($potential_bareme as $bar_)
	                 { 
	                    $bareme[$i] = array($bar_['min_value'], $bar_['max_value'], $bar_['percentage']);
	                     $i++;
	  				  }
			     
	             
		           // Salaire mensuel net
				   // Deduction IRI mensuel
				   // Salaire annuel
				   // Deduction IRI annuel
				   $salaire = $modelPayroll->getIriCharge_new($total_gross_salary,$bareme);
										              
					$deduction = $salaire['month_iri'];
					
												         
				   }
				      				   
	  	  
		      }
		      
         }
	  	     
  	  return $deduction;
  	
  	
  	}
  	
  	
  	//####################### STOCKROOM ###########################
 
      
  function loadStatusByProductType($type)
       {
       	    $code= array();
       	    
       	    	       
	              if($type==2)  //furniture
	               {   
	               	  if(!isset($_GET['id']))
	               	   {
	               	    $code[1]= Yii::t('app', 'In use');
	               	    $code[2]= Yii::t('app', 'In repair');
	                	
	               	   }
	               	 else
	               	    {
	               	    	$code[1]= Yii::t('app', 'In use');
		               	    $code[2]= Yii::t('app', 'In repair');
		                	$code[0]= Yii::t('app', 'Removed from stock');
		                	$code[3]= Yii::t('app', 'Lost');
	               	     }
	               }
	              elseif($type==1)  //equipment
	               {   
	               	 if(!isset($_GET['id']))
	               	   {
	               	    $code[1]= Yii::t('app', 'In use');
	               	    $code[2]= Yii::t('app', 'In repair');
	                	
	               	   }
	               	 else
	               	   {
	               	   	$code[1]= Yii::t('app', 'In use');
	               	    $code[2]= Yii::t('app', 'In repair');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	                	$code[3]= Yii::t('app', 'Lost');
	               	   	
	               	   	} 
	               	   
	               	   
	               }
	              elseif($type==0)   //raw material
	                {
	                  if(!isset($_GET['id']))
	               	   {
	               	   	$code[1]= Yii::t('app', 'In use');
	                	
	               	   }
	               	 else
	               	  {
	               	  	$code[1]= Yii::t('app', 'In use');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	               	   }
	                	
	                	

	                  }
	               	 
	       
	            
       	    return $code;
       	}
       	
       	 
  function loadStatusByProductTypeAndState($type,$state)
       {
       	    $code= array();
       	    
       	    	       
	              if($type==2)  //furniture
	               {   
	               	 if($state!=null)
                     {	               	 
	               	     
	               	  if(!isset($_GET['id']))
	               	   {
	               	       switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	                	
	               	   }
	               	 else
	               	    {
	               	    	 switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	        $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	                     $code[0]= Yii::t('app', 'Removed from stock');
		                	              $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }

	               	    	/*$code[1]= Yii::t('app', 'In use');
		               	    $code[2]= Yii::t('app', 'In repair');
		                	$code[0]= Yii::t('app', 'Removed from stock');
		                	$code[3]= Yii::t('app', 'Lost');
		                	*/
	               	     }
	                  
	                  }
	                  
	               }
	              elseif($type==1)  //equipment
	               {   
	               	 if($state!=null)
                       {	               	 
	               	     
	               	 
	               	 if(!isset($_GET['id']))
	               	   {
	               	     switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	                	
	               	   }
	               	 else
	               	   {
	               	   	  switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	        $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	                     $code[0]= Yii::t('app', 'Removed from stock');
		                	              $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	               	           
	               	   	/*$code[1]= Yii::t('app', 'In use');
	               	    $code[2]= Yii::t('app', 'In repair');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	                	$code[3]= Yii::t('app', 'Lost');
	                	*/
	               	      }
	               	   	
	               	   	} 
	               	   
	               	   
	               }
	              elseif($type==0)   //raw material
	                {
	                  if(!isset($_GET['id']))
	               	   {
	               	   	$code[1]= Yii::t('app', 'In use');
	                	
	               	   }
	               	 else
	               	  {
	               	  	if($state!=null)
                          {	               	 
	               	     
	               	  	    switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	         $code[0]= Yii::t('app', 'Removed from stock');
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        //$code[1]= Yii::t('app', 'In use');
	               	                     //$code[2]= Yii::t('app', 'In repair');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	               	  	
	               	  	/*$code[1]= Yii::t('app', 'In use');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	                	*/
                           }
                          
                          
	               	   }
	                	
	                	

	                  }
	               	 
	       
	            
       	    return $code;
       	}


       	
  function loadInventoryStatusByProductTypeAndState($type,$state) 
    {
       	    $code= array();
       	    
       	    	       
	              if($type==2)  //furniture
	               {   
	               	 if($state!=null)
                       {	               	 
	               	       switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	       $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	                     $code[0]= Yii::t('app', 'Removed from stock');
	               	                      $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	               	

	               	    	/*$code[1]= Yii::t('app', 'In use');
		               	    $code[2]= Yii::t('app', 'In repair');
		                	$code[0]= Yii::t('app', 'Removed from stock');
		                	$code[3]= Yii::t('app', 'Lost');
		                	*/
		                	
                         }
                         
	               	   
	               }
	              elseif($type==1)  //equipment
	               {   
	               	 if($state!=null)
                       {	               	 
	               	     switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	       $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[0]= Yii::t('app', 'Removed from stock');
		                	             $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        $code[1]= Yii::t('app', 'In use');
	               	                     $code[2]= Yii::t('app', 'In repair');
	               	                     $code[0]= Yii::t('app', 'Removed from stock');
	               	                      $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	    
	               	         	default: 
	               	         	       break;
	               	         	       
	               	       
	               	           }
	                        
	                        }
	               	           
	               	   	/*$code[1]= Yii::t('app', 'In use');
	               	    $code[2]= Yii::t('app', 'In repair');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	                	$code[3]= Yii::t('app', 'Lost');
	                	*/
	               	   	 
	               	   
	               	   
	               }
	              elseif($type==0)   //raw material
	                {
	                  if(!isset($_GET['id']))
	               	   {
	               	   	$code[1]= Yii::t('app', 'In use');
	                	
	               	   }
	               	 else
	               	  {
	               	    if($state!=null)
                         {	               	 
	               	       switch($state)
	               	         {          //bad
	               	         	case 0:
	               	         	         $code[0]= Yii::t('app', 'Removed from stock');
	               	         	         $code[3]= Yii::t('app', 'Lost');
	               	         	    break;
	               	         	          //good
	               	         	case 1:
	               	         	       $code[1]= Yii::t('app', 'In use');
	               	         	       $code[0]= Yii::t('app', 'Removed from stock');
	               	         	       $code[3]= Yii::t('app', 'Lost');
	               	         	      break;
	               	         	           //damaged
	               	         	case 2:
	               	         	        //$code[1]= Yii::t('app', 'In use');
	               	                     //$code[2]= Yii::t('app', 'In repair');
	               	         	    break;
	               	         	    
	               	         	default:
	               	         	       break;
	               	         	       
	               	       
	               	           }
	               	  	
	               	  	/*$code[1]= Yii::t('app', 'In use');
	                	$code[0]= Yii::t('app', 'Removed from stock');
	                	
	                	*/
	                	
                         }
                         
                         
	               	   }
	                	
	                	

	                  }
	               	 
	       
	            
       	    return $code;
       	}
       	
       	
  function loadStateByProductType($type)
       {
       	    $code= array();
       	    
       	    	       
	              if($type==2)  //furniture
	               {   
	               	    $code[1]= Yii::t('app', 'Good');
	               	    $code[2]= Yii::t('app', 'Damaged');
	                	$code[0]= Yii::t('app', 'Bad');
	               }
	              elseif($type==1)  //equipment
	               {   
	               	  if(!isset($_GET['id']))
	               	   {
	               	    $code[1]= Yii::t('app', 'Good');
	               	    $code[2]= Yii::t('app', 'Damaged');
	                	$code[0]= Yii::t('app', 'Bad');
	               	   }
	               	 else
	               	   {
	               	   	  $code[1]= Yii::t('app', 'Good');
	               	    $code[2]= Yii::t('app', 'Damaged');
	                	$code[0]= Yii::t('app', 'Bad');
	               	   	}
	               }
	              elseif($type==0)   //raw material
	                {
	                  if(!isset($_GET['id']))
	               	   {
	               	   	 $code[1]= Yii::t('app', 'Good');
	               	    $code[2]= Yii::t('app', 'Damaged');
	                	$code[0]= Yii::t('app', 'Bad');
	                	}
	               	 else
	               	   {
	               	   	   $code[1]= Yii::t('app', 'Good');
	               	    $code[2]= Yii::t('app', 'Damaged');
	                	$code[0]= Yii::t('app', 'Bad');
	               	   	}

	                	
	                 }
	               	 
	       
	            
       	    return $code;
       	}

  
  function loadEquipmentFurnitureAndRawMaterialLabel($stockroom_id)
       {
       	    $code= array();
       	    
       	    $data = SrcStockroomHasProducts::find()->where('stockroom_id ='.$stockroom_id.' AND quantity >0 and (status is null or status=1)')->all();
       	    
       	    foreach($data as $r)
       	      {
       	      	   $label = '';
       	      	       
	              if($r->is_equipment==2)  //furniture
	               {   
	               	  //pa pran l si li an sikilasyon
	               	  if($r->things_out==0)
	               	    { $stockroomFurnitureModel = new SrcStockroomFurniture;
	                      $label =  $stockroomFurnitureModel->getFurnitureLabel($r->stockroom_product);//.' '.Yii::t('app', '(FU)');
	                       $code[$r->id]= $label;
	               	    }
	               }
	              elseif($r->is_equipment==1)  //equipment
	               {   
	                 	//pa pran l si li an sikilasyon
	               	    if($r->things_out==0)
	               	      { $stockroomEquipmentModel = new SrcStockroomEquipment;
	                        $label =  $stockroomEquipmentModel->getEquipmentLabel($r->stockroom_product);//.' '.Yii::t('app', '(EQ)');
	                         $code[$r->id]= $label;
	               	      }
	               }
	              elseif($r->is_equipment==0)   //raw material
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($r->stockroom_product);//.' '.Yii::t('app', '(RM)');
	                     $code[$r->id]= $label;

	                  }
	               	 
	       
	            
       	      	}
       	    return $code;
       	}
        	
  function loadEquipmentFurnitureOrRawMaterialLabel($stockroom_product_id)
       {
       	    $code= array();
       	    
       	    $r = SrcStockroomHasProducts::findOne($stockroom_product_id);
       	    
       	    
       	   // foreach($data as $r)
       	     // {
       	      	   $label = '';
       	      	       
	              if($r->is_equipment==2)  //furniture
	               {   $stockroomFurnitureModel = new SrcStockroomFurniture;
	                    $label =  $stockroomFurnitureModel->getFurnitureLabel($r->stockroom_product);//.' '.Yii::t('app', '(FU)');
	               }
	              elseif($r->is_equipment==1)   //equipment
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $label =  $stockroomEquipmentModel->getEquipmentLabel($r->stockroom_product);//.' '.Yii::t('app', '(EQ)');
	               }
	              elseif($r->is_equipment==0)    //raw material
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($r->stockroom_product);//.' '.Yii::t('app', '(RM)');

	                  }
	               	 
	       
	             $code[$r->id]= $label;
       	      //	}
       	    return $code;
       	}
       	
    function loadEquipmentFurnitureOrRawMaterialLabelForInventory($stockroom_id) //tout label nan depo a
       {
       	    $code= array();
       	    
       	    $inv = SrcStockroomInventory::findOne($stockroom_product_id);
       	    
       	    $r = SrcStockroomHasProducts::findOne($inv->stockroom_product_id);
       	    
       	   // foreach($data as $r)
       	     // {
       	      	   $label = '';
       	      	       
	              if($r->is_equipment==2)  //furniture
	               {   $stockroomFurnitureModel = new SrcStockroomFurniture;
	                    $label =  $stockroomFurnitureModel->getFurnitureLabel($r->stockroom_product);//.' '.Yii::t('app', '(FU)');
	               }
	              elseif($r->is_equipment==1)   //equipment
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $label =  $stockroomEquipmentModel->getEquipmentLabel($r->stockroom_product);//.' '.Yii::t('app', '(EQ)');
	               }
	              elseif($r->is_equipment==0)    //raw material
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($r->stockroom_product);//.' '.Yii::t('app', '(RM)');

	                  }
	               	 
	       
	             $code[$r->id]= $label;
       	      //	}
       	    return $code;
       	}
 
  function loadEquipmentFurnitureAndRawMaterialLabelForRestock($stockroom_id)
       {
       	    $code= array();
       	    
       	    $data = SrcStockroomHasProducts::find()->where('stockroom_id ='.$stockroom_id)->all();
       	    
       	    foreach($data as $r)
       	      {
       	      	   $label = '';
       	      	       
	               if($r->is_equipment==2)  //furniture
	               {   $stockroomFurnitureModel = new SrcStockroomFurniture;
	                    $label =  $stockroomFurnitureModel->getFurnitureLabel($r->stockroom_product);//.' '.Yii::t('app', '(FU)');
	               }
	              elseif($r->is_equipment==1)  //equipment
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $label =  $stockroomEquipmentModel->getEquipmentLabel($r->stockroom_product);//.' '.Yii::t('app', '(EQ)');
	               }
	              elseif($r->is_equipment==0)  //raw material
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($r->stockroom_product);//.' '.Yii::t('app', '(RM)');

	                  }
	               	 
	       
	             $code[$r->id]= $label;
       	      	}
       	    return $code;
       	}
           	
 
   function loadProductsForRestock($stockroom_id)
       {
       	    $code= array();
       	    
       	    $data = SrcStockroomHasProducts::find()->where('is_equipment=0 and stockroom_id ='.$stockroom_id)->all();
       	    
       	    foreach($data as $r)
       	      {
       	      	   $label = '';
       	      	       
	               if($r->is_equipment==2)  //furniture
	               {   $stockroomFurnitureModel = new SrcStockroomFurniture;
	                    $label =  $stockroomFurnitureModel->getFurnitureLabel($r->stockroom_product);//.' '.Yii::t('app', '(FU)');
	               }
	              elseif($r->is_equipment==1)  //equipment
	               {   $stockroomEquipmentModel = new SrcStockroomEquipment;
	                    $label =  $stockroomEquipmentModel->getEquipmentLabel($r->stockroom_product);//.' '.Yii::t('app', '(EQ)');
	               }
	              elseif($r->is_equipment==0)  //raw material
	                {
	                	 $stockroomRawMAterialModel = new SrcStockroomRawMaterial;
	                    $label =  $stockroomRawMAterialModel->getRawMaterialLabel($r->stockroom_product);//.' '.Yii::t('app', '(RM)');

	                  }
	               	 
	       
	             $code[$r->id]= $label;
       	      	}
       	    return $code;
       	}
           	
      	
       	

/*  
   
	
//return datetime (last_activity)
function isUserConnected($user_id){  
    
    
	$command= Yii::app()->db->createCommand('SELECT id, last_activity FROM session  WHERE user_id='.$user_id ); 
	
	$data_ = $command->queryAll();
    
    foreach($data_ as $d){
    	         return $d['last_activity']; 
                    break;
                      
         }
         
         
}


function standard_deviation($aValues, $bSample = false)
        {
            $fMean = array_sum($aValues) / count($aValues);
            $fVariance = 0.0;
            foreach ($aValues as $i)
            {
                $fVariance += pow($i - $fMean, 2);
            }
            $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
            return (float) sqrt($fVariance);
        }
        
        
        





 // Retourne le total d'absence pour un un eleve pour l'annee academique 
 function getTotalAbsenceByStudent($student, $aca_year){
            $model = new RecordPresence;
            $stud_ = $model->findAll(array('select'=>'presence_type, date_record',
                                      'condition'=>'student=:std AND academic_period=:acad',
                                      'params'=>array(':std'=>$student,':acad'=>$aca_year),
               ));
        $absence_number = 0;
            foreach($stud_ as $sp){
                if($sp->presence_type == 1 || $sp->presence_type == 2){
                      $absence_number++;
                  }  
             }
            return $absence_number;
        }
 
 
  // Retourne le total retard pour un un eleve pour l'annee academique 
 function getTotalRetardByStudent($student, $aca_year){
            $model = new RecordPresence;
            $stud_ = $model->findAll(array('select'=>'presence_type, date_record',
                                      'condition'=>'student=:std AND academic_period=:acad',
                                      'params'=>array(':std'=>$student,':acad'=>$aca_year),
               ));
       $retard_number = 0;
            foreach($stud_ as $sp){
                if($sp->presence_type == 3 || $sp->presence_type == 4){
                      $retard_number++;
                  }  
             }
            return $retard_number;
        }
  
   // Total of infraction par annee
    
 function numberOfInfraction($student,$acad){
            $total = 0;
            $criteria = new CDbCriteria;
            $criteria->condition='student=:student AND academic_period=:academic_period';
            $criteria->params=array(':student'=>$student,':academic_period'=>$acad);
            $infractions = RecordInfraction::model()->findAll($criteria);
            foreach($infractions as $in){
                $total++;
            }
            return $total;
        }       
 
 
 
//############ subject average  #################
function course_average($course_id,$eval_id)
{
         $grade_array = array();
	   	   $i=0;
		   $class_average_value =0;
		   
		   $grade=Grades::model()->searchByRoom($course_id, $eval_id);
						             
				if(isset($grade)&&($grade!=null))
				 { 
				    $grade___=$grade->getData();//return a list of  objects
		                               
		               if(($grade___!=null))
						{ 
							
		           		  foreach($grade___ as $g) 
							{
								if($g->grade_value=='')
							       $grade_array[$i] = 0;
							    else
							       $grade_array[$i] = $g->grade_value;
							        	     
							
							   $i++;
							}
							
						}
						
				 }

                         if($grade_array!=null)
							$class_average_value = round(average_for_array($grade_array),2);
   
   

    return $class_average_value;
}

 */ 	

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

  	
	//####################  VARIANCE  ###################  

  
//(note: variance function uses the average function...)
function average_for_array($arr)
{
    if (!count($arr)) return 0;

    $sum = 0;
    for ($i = 0; $i < count($arr); $i++)
    {
        $sum += $arr[$i];
    }

    return $sum / count($arr);
}

function variance($arr)
{
    if (!count($arr)) return 0;

    
    $sum = 0;
    for ($i = 0; $i < count($arr); $i++)
    {
        $sum += $arr[$i];
    }

    $mean =  $sum / count($arr);
    
    $sos = 0;    // Sum of squares
    for ($i = 0; $i < count($arr); $i++)
    {
        $sos += ($arr[$i] - $mean) * ($arr[$i] - $mean);
    }

    return $sos / (count($arr)-1);  // denominator = n-1; i.e. estimating based on sample
                                    // n-1 is also what MS Excel takes by default in the
                                    // VAR function
}



 //####################  FIN VARIANCE  ###################   
 /**
  * Return a design depends on what program you pass on argument 
  * @param type $short_name
  * @return type
  */
function getDesignByProgram($short_name){
        $dizay = []; 
        switch($short_name){
            case 'TRI':{
                $dizay['fa'] = 'fa fa-wifi';
                $dizay['koule'] = 'bg-aqua';
            }
            break;
            case 'TEL':{
                $dizay['fa'] = 'fa fa-flash';
                $dizay['koule'] = 'bg-yellow';
            }
            break;
            case 'ELM':{
                $dizay['fa'] = 'fa fa-gear';
                $dizay['koule'] = 'bg-green';
            }
            break;
            case 'MEI':{
                $dizay['fa'] = 'fa fa-gears';
                $dizay['koule'] = 'bg-red';
            }
            break;
            default : {
                $dizay['fa'] = 'fa fa-building';
                $dizay['koule'] = 'bg-blue';
            }
                
         
        }
        return $dizay; 
    }
    
function kode($data, $sessionKey){
    $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
    $keySize = mcrypt_get_key_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
    
    $iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_URANDOM);
    $key = substr (hash('sha256', $sessionKey), 0, $keySize);
    $encryptedData = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_CBC, $iv);
    $encryptedB64Data = base64_encode($iv.$encryptedData);
    
    $vale_tounen = []; 
    $vale_tounen['ivSize'] = $ivSize; 
    $vale_tounen['key'] = $key; 
    $vale_tounen['encodeb64'] = $encryptedB64Data; 
    
    return $vale_tounen; 
} 

function dekode($encryptedB64Data, $sessionKey,$ivSize){
    $data = base64_decode($encryptedB64Data, true);
    $keySize = mcrypt_get_key_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
    $key = substr (hash('sha256', $sessionKey), 0, $keySize);
    $iv = substr ($data, 0, $ivSize);
    $data = substr ($data, $ivSize);
    $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_CBC, $iv);
    $decodedData =  rtrim($data, "\0");
    return $decodedData; 
}

function voyeEmail($from,$to,$subject,$body){
    try{
        Yii::$app->mailer->compose()
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject) 
            ->setHtmlBody($body)
            ->send();
    }
    catch (Exception $e){
        echo $e->getMessage();
    }
}








	

