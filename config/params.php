<?php

return [
    'adminEmail' => 'admin@example.com',
    'defaultPageSize'=>Yii::$app->session['pageSize'] ? Yii::$app->session['pageSize'] : 50,
    'language'=>'fr',
];
