<?php
require_once( __DIR__ . '/../assets/GlobalCustomFunction.php');

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'fr',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'S3CR3t',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
       
       /*
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
         * 
         */
        'user' => [
            'identityClass' =>'mdm\admin\models\User', //'mdm\admin\models\User',
            'loginUrl' => ['rbac/user/login'],
        ],
      
       'session' => [
            'class' => 'yii\web\DbSessionLogipam', // DbSession',
             'db' => 'db', //'mydb',  // the application component ID of the DB connection. Defaults to 'db'.
             'sessionTable' => '{{%session}}', // session table name. Defaults to 'session'.
             'userTableName' => 'user'   //add by LOGIPAM
        ],   
        
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' =>'mdm\admin\components\DbManager', // or use 'yii\rbac\DbManager'
            'defaultRoles'=>['guest'],
        ],
        'i18n' => [
        'translations' => [
            'app*' => [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => '@app/messages',
                'sourceLanguage' => 'en',
                'fileMap' => [
                    'app' => 'app.php',
                    'app/error' => 'error.php',
                ],
            ],
        ],
    ],
        
    ],
    'modules' => [
        'fi' => [
            'class' => 'app\modules\fi\Module',
        ],
        
        'billings' => [
            'class' => 'app\modules\billings\module',
        ],
        
        // Transfert du module yii2-admin vers un module rbac 
        'rbac' => [
            'class' => 'app\modules\rbac\Module',
            'layout'=>'/inspinia',
        ],
        'planning' => [
            'class' => 'app\modules\planning\Module',
        ],
        
        'stockrooms' => [
            'class' => 'app\modules\stockrooms\Module',
        ],
        
        'migration' => [
            'class' => 'app\modules\migration\Module',
        ],
        'inscription' => [
            'class' => 'app\modules\inscription\Module',
        ],
        
        'idcards' => [
            'class' => 'app\modules\idcards\Module',
        ],
        
        'reports' => [
            'class' => 'app\modules\reports\Module',
        ],
        'portal' => [
            'class' => 'app\modules\portal\Module',
        ],
        
        'admin' => [
        'class' => 'mdm\admin\Module',
        'layout' =>'left-menu',//'/inspinia', //'left-menu', // it can be '@path/to/your/layout'.
        'controllerMap' => [
            'assignment' => [
                'class' => 'mdm\admin\controllers\AssignmentController',
                'userClassName' => 'mdm\admin\models\User',
                'idField' => 'user_id'
            ],
            'other' => [
                'class' => 'path\to\OtherController', // add another controller
            ],
        ],
        'menus' => [
            'assignment' => [
                'label' => 'Grand Access' // change label
            ],
            'route' => null, // disable menu route
        ]
	],
	       
	       'guest' => [
            'class' => 'app\modules\guest\Module',
        ],
        

    ],
    
  /*  'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'admin/*',
            'fi/rooms/*',
            'fi/module/*',
            'fi/subjects/*',
            'fi/courses/*',
            'fi/persons/student',
            'fi/studenthascourses/create',
            'gii/*',
             //'fi/persons/*',
            'user/*',
            'gii/*',
           // 'some-controller/some-action',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
     */
     
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
