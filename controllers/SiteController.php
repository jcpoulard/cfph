<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;


use app\modules\fi\models\Persons;
use app\modules\fi\models\SrcPersons;



class SiteController extends Controller
{
    
    public $title = null;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    { $this->layout = "inspinia";
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //$this->layout = "inspinia";
        $this->title = "Sytème Gestion Canado Technique";
         
         if( isset(Yii::$app->user->identity->username) )
            return $this->render('index');
         else
            return $this->redirect('index.php/rbac/user/login');
    }
    
    public function actionTest()
    {
        $this->layout = "inspinia";
        
        return $this->render('test');
    }
    
  
    public function actionMagasin(){
        $this->layout = "inspinia";
        return $this->render('magasin');
    }
    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        
        
        $last_Activity = '';
        $user_id = '';
		
		if(isset(Yii::$app->user->identity->id))
		 { $user_id = Yii::$app->user->identity->id;
		 
		    //return datetime (last_activity)
             $last_Activity = isUserConnected($user_id);
		 }
		else
		  $user_id = '';
		  
		 if(($last_Activity=='')||($last_Activity==NULL))
		  {
		
				unset(Yii::app()->session['currentId_academic_year']);
				unset(Yii::app()->session['currentName_academic_year']);
				
				unset(Yii::$app->session['user_profil']);
		        unset(Yii::$app->session['profil_as']);
		        unset(Yii::$app->session['currencyName']);
				unset(Yii::$app->session['currencySymbol']);
				unset(Yii::$app->session['currencyId']);

		         
		
	        }
	      else
	       { 
	         	//delete session user sa
	         	$result = $this->db->createCommand("DeLETE FROM session WHERE user_id=\"$user_id\"")
              ->execute();
	         	
	         	unset(Yii::app()->session['currentId_academic_year']);
				unset(Yii::app()->session['currentName_academic_year']);
				
				unset(Yii::$app->session['user_profil']);
		        unset(Yii::$app->session['profil_as']);
		        unset(Yii::$app->session['currencyName']);
				unset(Yii::$app->session['currencySymbol']);
				unset(Yii::$app->session['currencyId']);
				
	             Yii::$app->user->logout();
	         
	         }
		  
		  
         
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
